package com.happyfresh.snowflakes.hoverfly.config

/**
 * Created by ifranseda on 24/12/2016.
 */

object Config {
    val Origin = "http://api-stage.happyfresh.com"

    @JvmStatic
    val BaseUrl = Origin + "/api/"

    @JvmStatic
    val ClientToken = "0115f406e71219ec9ea58e2eaaa4480ef966bdc42e245ec4bf601b23f07bd48e"

    @JvmStatic
    val SegmentWriteKey = "JHHPiyvqTPcxNFTkgZSF22UeY6n3LlSq"

    @JvmStatic
    val StockCheckerUrl = Origin + "/snd/item_checklist"

    @JvmStatic
    val brazeSenderId = "538857391278"

    @JvmStatic
    val SendbirdAppId = "3497E92B-C9C9-4031-ABF0-CE7AF12B39CB"
}