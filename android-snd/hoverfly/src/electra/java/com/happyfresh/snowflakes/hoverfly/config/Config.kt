package com.happyfresh.snowflakes.hoverfly.config

/**
 * Created by ifranseda on 24/12/2016.
 */

object Config {
    val Origin = "http://api-electra.happyfresh.com"

    @JvmStatic
    val BaseUrl = Origin + "/api/"

    @JvmStatic
    val ClientToken = "electra-uwN4zRDAG4jq8S12fzbcbk0JggXVAnOMYmzTRHcb"

    @JvmStatic
    val SegmentWriteKey = "JHHPiyvqTPcxNFTkgZSF22UeY6n3LlSq"

    @JvmStatic
    val StockCheckerUrl = Origin + "/snd/item_checklist"

    @JvmStatic
    val brazeSenderId = "264410800530"
}