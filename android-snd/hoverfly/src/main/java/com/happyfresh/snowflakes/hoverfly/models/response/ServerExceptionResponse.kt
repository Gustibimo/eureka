package com.happyfresh.snowflakes.hoverfly.models.response

import com.google.gson.annotations.SerializedName

/**
 * Created by ifranseda on 30/12/2016.
 */

class ServerExceptionResponse {

    @SerializedName("type")
    var type: String? = null

    @SerializedName("exception")
    var exception: String? = null

    @SerializedName("message")
    var message: String? = null
}