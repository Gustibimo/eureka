package com.happyfresh.snowflakes.hoverfly.models

/**
 * Created by kharda on 06/07/18.
 */
class GrabExpressCancelReason {

    var key: Int = 0

    var value: String? = null

    override fun toString(): String {
        return value!!
    }
}