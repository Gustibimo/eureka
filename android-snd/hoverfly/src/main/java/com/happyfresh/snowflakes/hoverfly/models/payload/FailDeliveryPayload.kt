package com.happyfresh.snowflakes.hoverfly.models.payload

/**
 * Created by rsavianto on 4/28/15.
 */

class FailDeliveryPayload : SerializablePayload() {

    var reason: String? = null
}
