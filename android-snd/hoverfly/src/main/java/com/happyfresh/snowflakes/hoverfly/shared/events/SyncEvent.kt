package com.happyfresh.snowflakes.hoverfly.shared.events

import com.happyfresh.snowflakes.hoverfly.models.payload.SerializablePayload

/**
 * Created by ifranseda on 12/12/17.
 */

class SyncEvent(data: List<SerializablePayload>) : BaseEvent<List<SerializablePayload>>(data)

class SyncFailedEvent(data: Throwable) : BaseEvent<Throwable>(data)