package com.happyfresh.snowflakes.hoverfly.models.payload

import com.google.gson.annotations.SerializedName

/**
 * Created by ifranseda on 11/27/14.
 */

class ListItemShopPayload : SerializablePayload() {

    @SerializedName("items")
    var items: List<ItemShopPayload>? = null
}
