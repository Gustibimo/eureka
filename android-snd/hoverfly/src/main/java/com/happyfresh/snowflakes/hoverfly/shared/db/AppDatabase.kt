package com.happyfresh.snowflakes.hoverfly.shared.db

import com.happyfresh.snowflakes.hoverfly.models.ProductProperty
import com.raizlabs.android.dbflow.annotation.ConflictAction
import com.raizlabs.android.dbflow.annotation.Database
import com.raizlabs.android.dbflow.annotation.Migration


/**
 * Created by ifranseda on 30/12/2016.
 */

@Database(name = AppDatabase.NAME, version = AppDatabase.VERSION, insertConflict = ConflictAction.IGNORE, updateConflict = ConflictAction.REPLACE)
object AppDatabase {
    const val NAME = "HF13"
    const val VERSION = 4

    @Migration(version = 2, database = AppDatabase::class)
    class Migration2 : FixPrimaryKeyMigration<ProductProperty>() {
        override fun getTableClass(): Class<ProductProperty> {
            return ProductProperty::class.java
        }
    }
}
