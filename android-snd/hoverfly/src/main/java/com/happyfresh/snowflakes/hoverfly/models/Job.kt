package com.happyfresh.snowflakes.hoverfly.models

import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.shared.db.AppDatabase
import com.raizlabs.android.dbflow.annotation.*
import com.raizlabs.android.dbflow.config.FlowManager
import com.raizlabs.android.dbflow.sql.language.SQLite
import com.raizlabs.android.dbflow.structure.BaseModel
import org.parceler.Parcel
import java.io.Serializable
import java.util.*

/**
 * Created by ifranseda on 11/13/14.
 */

@Parcel
@Table(database = AppDatabase::class)
class Job : BaseModel(), Serializable {

    object JobType {
        @JvmStatic
        val SHOPPING = "shopping"

        @JvmStatic
        val DELIVERY = "delivery"
    }

    object Progress {
        @JvmStatic
        val BOOKED = "initial"

        @JvmStatic
        val STARTED = "started"

        @JvmStatic
        val ACCEPTED = "accepted"

        @JvmStatic
        val DELIVERING = "delivering"

        @JvmStatic
        val FOUND_ADDRESS = "found_address"

        @JvmStatic
        val FINISHED = "finished"

        @JvmStatic
        val FAILED = "failed"

        @JvmStatic
        val FINALIZE = "finalizing"

        @JvmStatic
        val PICK_LAST_ITEMS = "last_item_picked"
    }

    @SerializedName("id")
    @Column
    @Unique(unique = true)
    @PrimaryKey
    var remoteId: Long? = null

    @SerializedName("shipment_id")
    @Column
    var shipmentId: Long? = null

    @SerializedName("start_time")
    @Column
    var startTime: Date? = null

    @SerializedName("end_time")
    @Column
    var endTime: Date? = null

    @Column
    var note: String? = null

    @Column
    var state: String? = null

    @Column
    @ForeignKey(saveForeignKeyModel = true)
    var user: User? = null

    @SerializedName("job_type")
    @Column
    var jobType: String? = null

    @SerializedName("driver_notified_at")
    @Column
    var driverNotifiedAt: Date? = null

    @SerializedName("states")
    var states: List<JobState>? = ArrayList()

    @OneToMany(methods = arrayOf(OneToMany.Method.ALL), variableName = "states", isVariablePrivate = false)
    fun states(): List<JobState>? {
        if (states?.isEmpty() == true) {
            states = SQLite.select().from(JobState::class.java).where(JobState_Table.jobId.eq(remoteId)).queryList()
        }

        return states
    }

    val isInitial: Boolean
        get() = Progress.BOOKED.equals(this.state, ignoreCase = true)

    val isStarted: Boolean
        get() = Progress.STARTED.equals(this.state, ignoreCase = true)

    val isAccepted: Boolean
        get() = Progress.ACCEPTED.equals(this.state, ignoreCase = true)

    val isDelivering: Boolean
        get() = Progress.DELIVERING.equals(this.state, ignoreCase = true)

    val isFoundAddress: Boolean
        get() = Progress.FOUND_ADDRESS.equals(this.state, ignoreCase = true)

    val isFinished: Boolean
        get() = Progress.FINISHED.equals(this.state, ignoreCase = true)

    val isPickingLastItem: Boolean
        get() = (jobType == JobType.SHOPPING) && (states()?.any { it.state == Progress.PICK_LAST_ITEMS } == true)

    val isFinalizing: Boolean
        get() = Progress.FINALIZE.equals(this.state, ignoreCase = true)

    val isActive: Boolean
        get() = Progress.DELIVERING.equals(this.state, ignoreCase = true) || Progress.FOUND_ADDRESS.equals(this.state, ignoreCase = true)

    fun stillShopping(): Boolean {
        return if (JobType.SHOPPING.equals(jobType!!, ignoreCase = true)) {
            Progress.STARTED.equals(this.state, ignoreCase = true)
        }
        else false
    }

    fun hasStartedShopping(): Boolean {
        return (jobType == JobType.SHOPPING) && (states()?.any { it.state == Progress.STARTED } == true)
    }

    fun arrivedAt(): Date {
        var date: Date? = null
        if (isFoundAddress) {
            date = states()?.first { it.isFoundAddress }?.startTime
        }

        return date!!
    }

    fun arrivalDuration(): Long {
        var tmp: Long = 0

        if (isFoundAddress) {
            var foundAddress = System.currentTimeMillis()
            var accepted = System.currentTimeMillis()

            for (state in states()!!) {
                if (state.isFoundAddress) {
                    foundAddress = state.startTime!!.time
                }
                else if (state.isAccepted) {
                    accepted = state.startTime!!.time
                }
            }

            tmp = foundAddress - accepted
        }

        return tmp
    }

    override fun save(): Boolean {
        val saved = super.save()

        if (this.states?.isNotEmpty() == true) {
            this.states?.forEach {
                it.jobId = this.remoteId
                it.save()
            }
        }

        return saved
    }

    companion object {
        @JvmStatic
        fun findById(jobId: Long?): Job? {
            return SQLite.select().from(Job::class.java).where(Job_Table.remoteId.eq(jobId)).querySingle()
        }

        @JvmStatic
        fun findByShipment(shipmentId: Long?): Job? {
            return SQLite.select().from(Job::class.java).where(Job_Table.shipmentId.eq(shipmentId)).querySingle()
        }
    }
}
