package com.happyfresh.snowflakes.hoverfly.models.response

import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.models.Chat

/**
 * Created by kharda on 18/9/18
 */

class ChatResponse {

    @SerializedName("chat")
    var chat: Chat? = null
}