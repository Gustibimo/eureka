package com.happyfresh.snowflakes.hoverfly

import android.app.Application
import android.content.Context
import com.happyfresh.snowflakes.hoverfly.services.BaseService
import com.happyfresh.snowflakes.hoverfly.shared.db.AppDatabase
import com.happyfresh.snowflakes.hoverfly.stores.BaseStore
import com.raizlabs.android.dbflow.config.FlowConfig
import com.raizlabs.android.dbflow.config.FlowManager
import java.lang.reflect.Constructor
import java.util.*

/**
 * Created by ifranseda on 31/12/2016.
 */

class Sprinkles {

    companion object {

        internal lateinit var sContext: Context

        private val services = HashMap<Class<*>, BaseService<*>>()

        @JvmStatic
        fun initialize(context: Application) {
            sContext = context

            resetDatabase()

            RestClient.init(context)
        }

        @JvmStatic
        fun resetDatabase() {
            FlowManager.reset()
            FlowManager.destroy()

            val config = FlowConfig.Builder(sContext).build()

            FlowManager.init(config)
        }

        @JvmStatic
        fun <T : BaseService<*>> service(service: Class<T>): T {
            try {
                return if (services.containsKey(service)) {
                    services[service] as T
                }
                else {
                    service.newInstance()
                }
            }
            catch (e: InstantiationException) {
                throw RuntimeException("Cannot instantiate " + service.canonicalName + " class. " + e.localizedMessage)
            }
            catch (e: IllegalAccessException) {
                throw RuntimeException("Cannot access " + service.canonicalName + " class. " + e.localizedMessage)
            }
        }

        @JvmStatic
        fun <U : BaseStore> stores(store: Class<U>): U? {
            return try {
                val c = Class.forName(store.name).getDeclaredConstructor(Context::class.java)!! as Constructor<U>
                c.isAccessible = true
                c.newInstance(sContext)
            }
            catch (e: Exception) {
                e.printStackTrace()
                null
            }
        }
    }
}