package com.happyfresh.snowflakes.hoverfly.utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import com.happyfresh.snowflakes.hoverfly.config.Constant

/**
 * Created by ifranseda on 11/6/15.
 */
class PermissionUtils {

    companion object {

        @JvmStatic
        fun accessLocation(context: Context?): Boolean {
            if (context == null) {
                return false
            }

            return ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
        }

        @JvmStatic
        fun callPhone(context: Context?): Boolean {
            if (context == null) {
                return false
            }

            return ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED
        }

        @JvmStatic
        fun accessStorage(context: Context?): Boolean {
            if (context == null) {
                return false
            }

            return ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
        }

        @JvmStatic
        fun alertWindow(context: Context?): Boolean {
            if (context == null) {
                return false
            }

            return ContextCompat.checkSelfPermission(context, Manifest.permission.SYSTEM_ALERT_WINDOW) == PackageManager.PERMISSION_GRANTED
        }

        @JvmStatic
        fun requestPermissionIfNeeded(activity: Activity?) {
            if (activity == null) {
                return
            }

            if (accessLocation(activity) && callPhone(activity) && accessStorage(activity)) {
                return
            }

            val permissions = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,
                                      Manifest.permission.ACCESS_COARSE_LOCATION,
                                      Manifest.permission.GET_ACCOUNTS,
                                      Manifest.permission.CALL_PHONE,
                                      Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                      Manifest.permission.READ_EXTERNAL_STORAGE)

            ActivityCompat.requestPermissions(activity, permissions, Constant.PERMISSIONS_REQUEST_CODE)
        }
    }
}