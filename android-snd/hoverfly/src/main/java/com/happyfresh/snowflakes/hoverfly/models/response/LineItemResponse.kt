package com.happyfresh.snowflakes.hoverfly.models.response

import com.happyfresh.snowflakes.hoverfly.models.LineItem

/**
 * Created by ifranseda on 11/20/14.
 */

class LineItemResponse {

    var items: List<LineItem>? = null
}
