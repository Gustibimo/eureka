package com.happyfresh.snowflakes.hoverfly.models

import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.models.response.PriceChangeResponse
import com.happyfresh.snowflakes.hoverfly.shared.db.AppDatabase
import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import com.raizlabs.android.dbflow.sql.language.Select
import com.raizlabs.android.dbflow.structure.BaseModel
import org.parceler.Parcel

/**
 * Created by anton on 12/11/14.
 */

@Parcel
@Table(database = AppDatabase::class)
class PriceAmendment : BaseModel() {

    @SerializedName("id")
    @PrimaryKey
    var remoteId: Long? = null

    @SerializedName("variant_id")
    @Column
    var variantId: Long? = null

    @SerializedName("order_id")
    @Column
    var orderId: Long? = null

    @SerializedName("stock_location_id")
    @Column
    var stockLocationId: Long? = null

    @SerializedName("replacement_id")
    @Column
    var replacementId: Long? = null

    @SerializedName("user_id")
    @Column
    var userId: String? = null

    @Column
    var variant: String? = null

    @Column
    var replacement: String? = null

    companion object {
        @JvmStatic
        fun findById(id: Long?): PriceAmendment? {
            id?.let {
                return Select().from(PriceAmendment::class.java).where(PriceAmendment_Table.remoteId.eq(id)).querySingle()
            }

            return null
        }

        @JvmStatic
        fun findByLineItem(lineItem: LineItem): List<PriceAmendment>? {
            return Select().from(PriceAmendment::class.java)
                    .where(PriceAmendment_Table.variantId.eq(lineItem.variantId))
                    .and(PriceAmendment_Table.orderId.eq(lineItem.orderId))
                    .and(PriceAmendment_Table.stockLocationId.eq(lineItem.stockLocationId))
                    .queryList()
        }

        @JvmStatic
        fun createFromResponse(response: PriceChangeResponse): PriceAmendment {
            findById(response.remoteId)?.let {
                return it
            }

            return PriceAmendment().apply {
                remoteId = response.remoteId
                variantId = response.variantId
                orderId = response.orderId
                stockLocationId = response.stockLocationId
                userId = response.userId
                replacementId = response.replacementId

                if (replacementId != null) {
                    replacement = response.newPrice
                }
                else {
                    variant = response.newPrice
                }

                save()
            }
        }
    }
}
