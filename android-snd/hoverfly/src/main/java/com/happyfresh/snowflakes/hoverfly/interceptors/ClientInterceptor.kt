package com.happyfresh.snowflakes.hoverfly.interceptors

import com.happyfresh.snowflakes.hoverfly.Sprinkles
import com.happyfresh.snowflakes.hoverfly.config.Config
import com.happyfresh.snowflakes.hoverfly.config.Spree
import com.happyfresh.snowflakes.hoverfly.stores.UserStore
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException
import java.util.*

/**
 * Created by ifranseda on 25/12/2016.
 */

class ClientInterceptor : Interceptor {

    private var credential: String? = null

    internal var multipart: Boolean = false

    private val userStore = Sprinkles.stores(UserStore::class.java)

    @Throws(IOException::class) override fun intercept(chain: Interceptor.Chain): Response {
        val requestBuilder = chain.request().newBuilder().header(Spree.ClientToken, Config.ClientToken)

        if (!multipart) {
            requestBuilder.header("Content-Type", "application/json")
        }

        if (userStore?.hasToken!!) {
            requestBuilder.header(Spree.UserToken, userStore.token)
        }

        this.credential?.let {
            if (it.isNotEmpty()) {
                requestBuilder.header("Authorization", "Basic " + this.credential)
            }
        }

        val locale = Locale.getDefault()
        val language = if (locale.country.toUpperCase() == "ID") "id" else locale.language
        language?.let {
            requestBuilder.addHeader("Locale", it)

            val url = chain.request().url().newBuilder()
                    .addQueryParameter("Locale", it)
                    .build()

            requestBuilder.url(url)
        }

        return chain.proceed(requestBuilder.build())
    }
}
