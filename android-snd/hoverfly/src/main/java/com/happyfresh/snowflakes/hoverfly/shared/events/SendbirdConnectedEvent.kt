package com.happyfresh.snowflakes.hoverfly.shared.events

/**
 * Created by kharda on 24/8/18
 */
class SendbirdConnectedEvent(data: Boolean) : BaseEvent<Boolean>(data)