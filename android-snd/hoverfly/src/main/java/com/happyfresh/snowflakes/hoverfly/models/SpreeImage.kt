package com.happyfresh.snowflakes.hoverfly.models

import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.shared.db.AppDatabase
import com.raizlabs.android.dbflow.annotation.*
import com.raizlabs.android.dbflow.structure.BaseModel
import org.parceler.Parcel
import java.util.*

/**
 * Created by ifranseda on 11/14/14.
 */

@Parcel
@Table(database = AppDatabase::class)
class SpreeImage : BaseModel() {

    @SerializedName("id")
    @Column
    @PrimaryKey
    var remoteId: Long? = null

    @ForeignKey(stubbedRelationship = true)
    var variant: Variant? = null

    @Column
    var position: Int = 0

    @SerializedName("attachment_content_type")
    @Column(name = "attachment_content_type")
    var contentType: String? = null

    @SerializedName("attachment_file_name")
    @Column(name = "attachment_file_name")
    var fileName: String? = null

    @Column
    var type: String? = null

    @SerializedName("attachment_updated_at")
    @Column(name = "attachment_updated_at")
    var updatedAt: Date? = null

    @SerializedName("attachment_width")
    @Column(name = "attachment_width")
    var width: Double? = null

    @SerializedName("attachment_height")
    @Column(name = "attachment_height")
    var height: Double? = null

    @Column
    var alt: String? = null

    @SerializedName("viewable_type")
    @Column
    var viewableType: String? = null

    @SerializedName("viewable_id")
    @Column
    var viewableId: Long? = null

    @SerializedName("mini_url")
    @Column
    var miniUrl: String? = null

    @SerializedName("small_url")
    @Column
    var smallUrl: String? = null

    @SerializedName("product_url")
    @Column
    var productUrl: String? = null

    @SerializedName("large_url")
    @Column
    var largeUrl: String? = null

    @SerializedName("original_url")
    @Column
    var originalUrl: String? = null
}
