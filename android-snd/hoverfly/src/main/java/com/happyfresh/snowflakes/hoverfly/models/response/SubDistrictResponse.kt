package com.happyfresh.snowflakes.hoverfly.models.response

import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.models.SubDistrict
import java.util.*

/**
 * Created by rsavianto on 2/7/15.
 */

class SubDistrictResponse {

    @SerializedName("sub_districts")
    var subDistricts: List<SubDistrict> = ArrayList()
}
