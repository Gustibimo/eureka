package com.happyfresh.snowflakes.hoverfly.models


import com.google.gson.annotations.SerializedName
import org.parceler.Parcel

/**
 * Created by rsavianto on 2/7/15.
 */

@Parcel
class State {

    @SerializedName("id")
    var remoteId: Long = 0

    var name: String? = null

    var abbr: String? = null

    @SerializedName("country_id")
    var countryId: Long? = null
}
