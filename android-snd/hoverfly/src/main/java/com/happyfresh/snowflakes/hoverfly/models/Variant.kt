package com.happyfresh.snowflakes.hoverfly.models

import android.text.TextUtils
import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.models.ProductProperty.PropertyName.*
import com.happyfresh.snowflakes.hoverfly.shared.db.AppDatabase
import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.OneToMany
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import com.raizlabs.android.dbflow.sql.language.SQLite
import com.raizlabs.android.dbflow.structure.BaseModel
import org.parceler.Parcel
import java.util.*

/**
 * Created by ifranseda on 11/14/14.
 */

@Parcel
@Table(database = AppDatabase::class)
class Variant : BaseModel() {

    @SerializedName("id")
    @PrimaryKey
    var remoteId: Long = 0L

    @SerializedName("product_id")
    @Column
    var productId: Long? = null

    @Column
    var name: String? = null

    @Column
    var sku: String? = null

    @Column
    var slug: String? = null

    @Column
    var description: String? = null

    @SerializedName("options_text")
    @Column
    var optionsText: String? = null

    @SerializedName("display_price")
    @Column
    var displayPrice: String? = null

    @SerializedName("display_cost_price")
    @Column
    var displayCostPrice: String? = null

    @SerializedName("display_supermarket_unit_cost_price")
    @Column
    var displaySupermarketUnitCostPrice: String? = null

    @SerializedName("display_unit")
    @Column
    var displayUnit: String? = null

    @SerializedName("display_average_weight")
    @Column
    var displayAverageWeight: String? = null

    @SerializedName("natural_average_weight")
    @Column
    var naturalAverageWeight: Double? = null

    @SerializedName("actual_weight_adjustment_total")
    @Column
    var actualWeightAdjustmentTotal: Double? = null

    @SerializedName("display_actual_weight")
    @Column
    var displayActualWeight: String? = null

    @SerializedName("supermarket_unit")
    @Column
    var supermarketUnit: String? = null

    @Column
    var price: Double? = null

    @Column
    var weight: Double? = null

    @Column
    var height: Double? = null

    @Column
    var width: Double? = null

    @Column
    var depth: Double? = null

    @SerializedName("cost_price")
    @Column
    var costPrice: Double? = null

    @SerializedName("supermarket_unit_cost_price")
    @Column
    var supermarketUnitCostPrice: Double? = null

    @SerializedName("is_master")
    @Column
    var isMaster: Boolean = false

    @SerializedName("track_inventory")
    @Column
    var isTrackInventory: Boolean = false

    @SerializedName("in_stock")
    @Column
    var isInStock: Boolean = false

    @SerializedName("option_values")
    var optionValues: List<String> = ArrayList()

    var images: List<SpreeImage>? = ArrayList()

    @OneToMany(methods = arrayOf(OneToMany.Method.ALL), variableName = "images")
    fun images(): List<SpreeImage>? {
        if (images?.isEmpty() == true) {
            images = SQLite.select().from(SpreeImage::class.java).where(SpreeImage_Table.variant_remoteId.eq(remoteId)).queryList()
        }

        return images
    }

    @SerializedName("product_properties")
    var properties: List<ProductProperty>? = ArrayList()

    @OneToMany(methods = arrayOf(OneToMany.Method.ALL), variableName = "properties")
    fun properties(): List<ProductProperty>? {
        if (properties?.isEmpty() == true) {
            properties = SQLite.select().from(ProductProperty::class.java).where(ProductProperty_Table.variant_remoteId.eq(remoteId)).queryList()
        }

        return properties
    }

    val units: String
        get() {
            var pack: ProductProperty? = null
            var size: ProductProperty? = null
            var unit: ProductProperty? = null

            properties()?.filter { it.type != null }?.forEach {
                when (it.type) {
                    PACKAGE          -> pack = it
                    SIZE             -> size = it
                    SUPERMARKET_UNIT -> unit = it
                }
            }

            val builder = StringBuilder()

            if (size != null && unit != null) {
                if (size?.value != "1") {
                    builder.append(size?.value)
                    builder.append(" ")
                }

                builder.append(unit?.value)
            }

            if (!builder.isEmpty() && pack?.value?.toInt()!! > 1) {
                builder.insert(0, " x ")
                builder.insert(0, pack?.value)
            }

            return builder.toString()
        }

    fun hasNaturalUnit(): Boolean {
        return !TextUtils.isEmpty(displayAverageWeight)
    }

    val averageWeight: String?
        get() {
            return properties()?.firstOrNull { it.type === AVG_WEIGHT && it.value != null }?.value
        }

    fun getTotalNaturalAverageWeight(count: Int): Double? {
        return if (hasNaturalUnit()) count * naturalAverageWeight!! else 0.0
    }

    override fun save(): Boolean {
        val saved = super.save()

        if (saved) {
            images?.forEach {
                it.variant = this
                it.save()
            }

            properties?.forEach {
                it.variant = this
                it.save()
            }
        }

        return saved
    }
}
