package com.happyfresh.snowflakes.hoverfly.models.response

import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.models.CashTransfer

/**
 * Created by ifranseda on 5/6/15.
 */

class CashHandoverResponse {

    @SerializedName("cash_transfers")
    var cashTransfers: List<CashTransfer>? = null

    var count: Int = 0

    @SerializedName("total_count")
    var total: Int = 0

    @SerializedName("current_page")
    var currentPage: Int = 0

    @SerializedName("per_page")
    var perPage: Int = 0

    @SerializedName("pages")
    var page: Int = 0
}
