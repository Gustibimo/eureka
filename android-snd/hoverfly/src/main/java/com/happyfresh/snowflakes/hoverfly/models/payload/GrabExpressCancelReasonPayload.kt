package com.happyfresh.snowflakes.hoverfly.models.payload

import com.google.gson.annotations.SerializedName


/**
 * Created by kharda on 06/07/18.
 */
class GrabExpressCancelReasonPayload : SerializablePayload() {

    @SerializedName("cancel_reason")
    var cancelReason: Int = 0
}