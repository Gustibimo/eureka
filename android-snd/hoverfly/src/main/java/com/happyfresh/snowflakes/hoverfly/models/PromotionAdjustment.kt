package com.happyfresh.snowflakes.hoverfly.models

import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.shared.db.AppDatabase
import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import com.raizlabs.android.dbflow.structure.BaseModel
import org.parceler.Parcel
import java.io.Serializable

/**
 * Created by kharda on 16/12/16.
 */

@Parcel
@Table(database = AppDatabase::class)
class PromotionAdjustment : BaseModel(), Serializable {

    @Column
    @PrimaryKey
    var orderId: Long? = null

    @SerializedName("eligible")
    @Column
    var isEligible: Boolean = false

    @SerializedName("credit_card_promotion")
    @Column
    var isCreditCardPromotion: Boolean = false
}
