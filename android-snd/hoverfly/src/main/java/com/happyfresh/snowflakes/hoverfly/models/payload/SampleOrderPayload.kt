package com.happyfresh.snowflakes.hoverfly.models.payload

import com.google.gson.annotations.SerializedName

/**
 * Created by numannaufal on 9/20/16.
 */

class SampleOrderPayload(
        @SerializedName("user_id")
        var userId: Long,

        @SerializedName("order_number")
        var orderNumber: String,

        @SerializedName("order_id")
        var orderId: Long,

        @SerializedName("stock_location_id")
        var stockLocationId: Long,

        @SerializedName("campaigns")
        var sampleCampaignPayloads: List<SampleCampaignPayload>)
