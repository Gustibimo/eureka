package com.happyfresh.snowflakes.hoverfly

import android.content.Context
import android.text.TextUtils
import com.google.gson.GsonBuilder
import com.happyfresh.snowflakes.hoverfly.config.Config
import com.happyfresh.snowflakes.hoverfly.interceptors.ClientInterceptor
import com.happyfresh.snowflakes.hoverfly.serializers.DateDeserializer
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.io.IOException
import java.lang.reflect.Modifier
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created by ifranseda on 24/12/2016.
 */

class RestClient internal constructor() {

    private var _context: Context? = null
    internal var context: Context?
        get() = _context
        set(value) {
            _context = value
        }

    private var clientBuilder: OkHttpClient.Builder

    private var retrofitBuilder: Retrofit.Builder

    private var API_CONNECT_TIME_OUT = 60 * 1000

    private var API_READ_TIME_OUT = 60 * 1000

    private var API_CACHE_SIZE = 10 * 1024 * 1024 // 10 MB

    private var API_CACHE_NAME = "hf_cache"

    init {
        this.clientBuilder = OkHttpClient.Builder()
                .connectTimeout(API_CONNECT_TIME_OUT.toLong(), TimeUnit.MILLISECONDS)
                .writeTimeout(API_READ_TIME_OUT.toLong(), TimeUnit.MILLISECONDS)
                .readTimeout(API_READ_TIME_OUT.toLong(), TimeUnit.MILLISECONDS)
                .followRedirects(true)
//                .proxy(Proxy(Proxy.Type.HTTP, InetSocketAddress("10.1.100.23", 8888)))

        this.retrofitBuilder = createRetrofitBuilder()
    }

    private fun createRetrofitBuilder(): Retrofit.Builder {
        if (TextUtils.isEmpty(Config.BaseUrl)) {
            throw RuntimeException("Base URL is empty")
        }

        val gson = GsonBuilder().registerTypeAdapter(Date::class.java, DateDeserializer())
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .create()

        return Retrofit.Builder().baseUrl(Config.BaseUrl)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
    }

    @Throws(IOException::class)
    fun <T> convert(body: ResponseBody, tClass: Class<T>): T {
        val converter = createAdapter().responseBodyConverter<T>(tClass, arrayOfNulls<Annotation>(0))
        return converter.convert(body)
    }

    @JvmOverloads
    fun createAdapter(multipart: Boolean = false): Retrofit {
        clientBuilder.interceptors().clear()

        val interceptor = ClientInterceptor()
        interceptor.multipart = multipart

        val client = clientBuilder.addInterceptor(interceptor).build()

        return retrofitBuilder.client(client).build()
    }

    private fun cache(): Cache {
        val file = File(this.context?.cacheDir, API_CACHE_NAME)
        return Cache(file, API_CACHE_SIZE.toLong())
    }

    object Service {
        fun <T> createMultipart(cls: Class<T>): T {
            return getInstance().createAdapter(true).create(cls)
        }

        fun <T> create(cls: Class<T>): T {
            return getInstance().createAdapter().create(cls)
        }
    }

    companion object {
        internal var sInstance: RestClient? = null

        fun reset() {
            init(sInstance?.context!!)
        }

        fun getInstance(): RestClient {
            if (sInstance == null) {
                throw RuntimeException("No sInstance created")
            }

            return sInstance as RestClient
        }

        fun init(context: Context) {
            sInstance = RestClient()
            sInstance?.context = context
        }
    }
}