package com.happyfresh.snowflakes.hoverfly.utils

import android.content.Context
import android.widget.Toast
import com.happyfresh.happychat.ChatRouter
import com.happyfresh.snowflakes.hoverfly.Sprinkles
import com.happyfresh.snowflakes.hoverfly.models.Batch
import com.happyfresh.snowflakes.hoverfly.models.Chat
import com.happyfresh.snowflakes.hoverfly.models.Order
import com.happyfresh.snowflakes.hoverfly.models.Shipment
import com.happyfresh.snowflakes.hoverfly.services.ShipmentService
import com.happyfresh.snowflakes.hoverfly.shared.events.ChatStartedEvent
import com.happyfresh.snowflakes.hoverfly.shared.events.EventBus
import com.happyfresh.snowflakes.hoverfly.shared.events.SendbirdConnectedEvent
import com.happyfresh.snowflakes.hoverfly.shared.events.UnreadChatFetchedEvent
import com.happyfresh.snowflakes.hoverfly.shared.subscribers.ServiceSubscriber
import com.happyfresh.snowflakes.hoverfly.stores.StockLocationStore
import com.sendbird.android.*
import rx.android.schedulers.AndroidSchedulers

/**
 * Created by kharda on 20/8/18
 */
class ChatUtils {

    companion object {

        @JvmStatic
        fun fetchTotalUnreadChatCountInBatch(batchId: Long?) {
            val batch = Batch.findById(batchId)

            if (batch != null) {
                for (shipment in batch.shipments()) {
                    val chat = shipment.order?.chat
                    chat?.channelUrl?.let { channelUrl ->
                        GroupChannel.getChannel(channelUrl) { channel, exception ->
                            if (exception == null) {
                                channel.refresh { exception ->
                                    if (exception == null) {
                                        saveUnreadChatCountAndNotify(batchId, chat, channel.unreadMessageCount)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        @JvmStatic
        fun fetchUnreadChatCountInChannel(batchId: Long?, channelUrl: String?) {
            GroupChannel.getChannel(channelUrl) { channel, exception ->
                if (exception == null) {
                    val chat = Chat.findByChannelUrl(channelUrl)
                    chat?.let {
                        saveUnreadChatCountAndNotify(batchId, chat, channel.unreadMessageCount)
                    }
                }
            }
        }

        @JvmStatic
        fun openChatScreen(context: Context?, shopperId: String, shipment: Shipment) {
            shipment.order?.let { order ->
                order.load()
                if (order.chat?.channelUrl == null) {
                    initiateAndOpenChatRoom(context, shopperId, shipment)
                } else {
                    openChatRoom(context, shopperId, "", shipment.order)
                }
            }
        }

        @JvmStatic
        fun addChatHandler(batchId: Long?, connectionHandlerId: String, channelHandlerId: String, shopperId: String) {
            ChatUtils.addConnectionManagementHandler(connectionHandlerId, shopperId,
                    object : ChatUtils.Companion.ConnectionManagementHandler {
                        override fun onConnected(reconnect: Boolean) {
                            ChatUtils.fetchTotalUnreadChatCountInBatch(batchId)
                            EventBus.handler.post(SendbirdConnectedEvent(true))
                        }
                    })

            SendBird.addChannelHandler(channelHandlerId,
                    object : SendBird.ChannelHandler() {
                        override fun onMessageReceived(channel: BaseChannel?, message: BaseMessage?) {
                            ChatUtils.fetchUnreadChatCountInChannel(batchId, channel?.url)
                        }
                    })
        }

        fun removeChatHandler(connectionHandlerId: String, channelHandlerId: String) {
            SendBird.removeConnectionHandler(connectionHandlerId)
            SendBird.removeChannelHandler(channelHandlerId)
        }

        @JvmStatic
        private fun saveUnreadChatCountAndNotify(batchId: Long?, chat: Chat, unreadMessageCount: Int) {
            chat.unreadChatCount = unreadMessageCount
            chat.save()
            EventBus.handler.post(UnreadChatFetchedEvent(batchId!!))
        }

        @JvmStatic
        private fun addConnectionManagementHandler(handlerId: String, shopperId: String, handler: ConnectionManagementHandler?) {
            SendBird.addConnectionHandler(handlerId, object : SendBird.ConnectionHandler {
                override fun onReconnectStarted() {}

                override fun onReconnectSucceeded() {
                    handler?.onConnected(true)
                }

                override fun onReconnectFailed() {}
            })

            if (SendBird.getConnectionState() == SendBird.ConnectionState.OPEN) {
                handler?.onConnected(false)
            } else if (SendBird.getConnectionState() == SendBird.ConnectionState.CLOSED) {
                SendBird.connect(shopperId) { _, exception ->
                    if (exception == null) {
                        handler?.onConnected(false)
                    }
                }
            }
        }

        @JvmStatic
        private fun initiateAndOpenChatRoom(context: Context?, shopperId: String, shipment: Shipment) {
            val customerId = shipment.order?.userId.toString()
            SendBird.connect(customerId) { _, exception ->
                if (exception == null) {
                    SendBird.updateCurrentUserInfo(shipment.customerName, null, null)

                    val userIds = listOf(shopperId, customerId)
                    val params = GroupChannelParams()
                            .addUserIds(userIds)
                            .setName(shipment.order?.number.toString())
                            .setOperatorUserIds(listOf(shopperId))
                            .setDistinct(false)

                    GroupChannel.createChannel(params) { groupChannel, exception ->
                        if (exception == null) {
                            val stockLocationStore = Sprinkles.stores(StockLocationStore::class.java)
                            val storeName = stockLocationStore?.currentStoreName

                            val metaData = hashMapOf("order_number" to shipment.order?.number, "store_name" to storeName)
                            groupChannel.createMetaData(metaData) { _, exception ->
                                if (exception == null) {
                                    startChat(context, shopperId, shipment.order, groupChannel.url)
                                } else {
                                    Toast.makeText(context, exception.message, Toast.LENGTH_SHORT).show()
                                }
                            }
                        } else {
                            Toast.makeText(context, exception.message, Toast.LENGTH_SHORT).show()
                        }
                    }
                } else {
                    Toast.makeText(context, exception.message, Toast.LENGTH_SHORT).show()
                }
            }
        }

        @JvmStatic
        private fun startChat(context: Context?, shopperId: String, order: Order?, channelUrl: String) {
            val shipmentService = ShipmentService()
            val observable = shipmentService.startChat(order?.remoteId!!, channelUrl).observeOn(AndroidSchedulers.mainThread())
            val subscriber = object : ServiceSubscriber<Chat>() {
                override fun onSuccess(data: Chat) {
                    order.load()
                    val shopperName = data.shopperName ?: ""
                    openChatRoom(context, shopperId, shopperName, order)
                    EventBus.handler.post(ChatStartedEvent(true))
                }

                override fun onFailure(exception: Throwable) {
                    Toast.makeText(context, exception.message, Toast.LENGTH_SHORT).show()
                }
            }

            observable.subscribe(subscriber)
        }

        @JvmStatic
        private fun openChatRoom(context: Context?, shopperId: String, shopperName: String, order: Order?) {
            val chatRouter = ChatRouter(context)
            chatRouter.putUseCustomHeader(true)
            chatRouter.putUpdateUserInfo(shopperName)
            chatRouter.open(shopperId, order?.chat?.channelUrl)
        }

        @JvmStatic
        fun finishChatRooms(batchId: Long?) {
            val batch = Batch.findById(batchId)

            if (batch != null) {
                for (shipment in batch.shipments()) {
                    shipment.order?.chat?.let { chat ->
                        freezeChatRoom(chat)
                        if (chat.isActive()) {
                            ShipmentService().finishChat(shipment.order?.remoteId!!)
                        }
                    }
                }
            }
        }

        @JvmStatic
        fun freezeChatRoom(shipmentId: Long) {
            val shipment = Shipment.findById(shipmentId)
            shipment?.order?.chat?.let { chat ->
                freezeChatRoom(chat)
            }
        }

        @JvmStatic
        fun freezeChatRoom(chat: Chat) {
            chat.channelUrl?.let { channelUrl ->
                GroupChannel.getChannel(channelUrl) { channel, exception ->
                    if (exception == null) {
                        if (!channel.isFrozen) {
                            channel.freeze { }
                        }
                    }
                }
            }
        }

        interface ConnectionManagementHandler {
            /**
             * A callback for when connected or reconnected to refresh.
             *
             * @param reconnect Set false if connected, true if reconnected.
             */
            fun onConnected(reconnect: Boolean)
        }
    }
}