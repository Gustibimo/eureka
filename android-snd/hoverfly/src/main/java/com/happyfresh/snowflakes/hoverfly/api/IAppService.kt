package com.happyfresh.snowflakes.hoverfly.api

import com.happyfresh.snowflakes.hoverfly.models.UserLocation
import com.happyfresh.snowflakes.hoverfly.models.payload.ClockingPayload
import com.happyfresh.snowflakes.hoverfly.models.response.PauseResponse
import com.happyfresh.snowflakes.hoverfly.models.response.FeedbackTypeResponse
import com.happyfresh.snowflakes.hoverfly.models.response.StatusResponse
import com.happyfresh.snowflakes.hoverfly.models.response.VersionUpdate
import retrofit.client.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query
import rx.Observable



/**
 * Created by aldi on 6/22/17.
 */

interface IAppService {

    @GET("check_update")
    fun checkUpdate(@Query("current_version") currentVersion: Int): Observable<VersionUpdate>

    @POST("snd/clock_in")
    fun clockIn(@Body sndResponse: ClockingPayload): Observable<UserLocation>

    @POST("snd/clock_out")
    fun clockOut(): Observable<StatusResponse>

    @GET("snd/status")
    fun status(): Observable<StatusResponse>

    @POST("snd/pause")
    fun pause(): Observable<PauseResponse>

    @POST("snd/resume")
    fun resume(): Observable<Response>

    @GET("snd/feedback_types")
    fun feedbackTypes(@Query("variant_id") variantId: Long): Observable<FeedbackTypeResponse>
}
