package com.happyfresh.snowflakes.hoverfly.config

/**
 * Created by ifranseda on 19/07/2017.
 */
object Spree {
    const val ClientToken = "X-Spree-Client-Token"

    const val UserToken = "X-Spree-Token"

    const val OrderToken = "X-Spree-Order-Token"
}