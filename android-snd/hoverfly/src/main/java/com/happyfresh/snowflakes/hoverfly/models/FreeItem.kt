package com.happyfresh.snowflakes.hoverfly.models

import com.google.gson.annotations.SerializedName
import org.parceler.Parcel

/**
 * Created by kharda on 7/19/16.
 */

@Parcel
class FreeItem {

    @SerializedName("total_free_item")
    var totalFreeItem: Int = 0

    @SerializedName("total_rejected_free_item")
    var totalRejectedFreeItem: Int = 0
}
