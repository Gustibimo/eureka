package com.happyfresh.snowflakes.hoverfly.models.response

import com.happyfresh.snowflakes.hoverfly.models.Shipment

/**
 * Created by ifranseda on 11/13/14.
 */

class ShipmentResponse {

    var shipments: List<Shipment>? = null
}
