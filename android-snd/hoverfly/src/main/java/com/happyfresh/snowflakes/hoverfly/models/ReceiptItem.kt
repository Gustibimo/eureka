package com.happyfresh.snowflakes.hoverfly.models

import android.os.Parcelable
import android.text.TextUtils
import com.happyfresh.snowflakes.hoverfly.utils.FileUtils
import org.parceler.Parcel
import org.parceler.Parcels
import retrofit.mime.TypedFile
import java.io.File
import java.io.FileNotFoundException
import java.io.Serializable

/**
 * Created by ifranseda on 11/25/15.
 */

@Parcel
class ReceiptItem : Serializable, Cloneable {

    companion object {
        fun toListParcel(dataList: List<ReceiptItem>?): ArrayList<Parcelable> {
            val parcelableList = ArrayList<Parcelable>()

            dataList?.forEach {
                parcelableList.add(Parcels.wrap(it))
            }

            return parcelableList
        }

        fun fromListParcel(parcelableList: List<Parcelable>?): ArrayList<ReceiptItem> {
            val dataList = ArrayList<ReceiptItem>()

            parcelableList?.forEach {
                dataList.add(Parcels.unwrap(it))
            }

            return dataList
        }
    }

    var requireNumber: Boolean = false

    var position: Int = 0

    var number: String? = null

    var amount: String? = null

    var tax: String? = null

    var photoReceiptItems: List<PhotoReceiptItem> = ArrayList()

    constructor()

    constructor(requireNumber: Boolean) {
        this.requireNumber = requireNumber
    }

    constructor(requireNumber: Boolean, photoReceiptItems: List<PhotoReceiptItem>) {
        this.requireNumber = requireNumber
        this.photoReceiptItems = photoReceiptItems
    }

    constructor(requireNumber: Boolean, number: String, amount: String, tax: String, photoReceiptItems: List<PhotoReceiptItem>) {
        this.requireNumber = requireNumber
        this.number = number
        this.amount = amount
        this.tax = tax
        this.photoReceiptItems = photoReceiptItems
    }

    @Throws(FileNotFoundException::class, FileUtils.FileEmptyException::class)
    fun getAttachments(): List<TypedFile> {
        val imageFiles = getFiles()
        val tmp = ArrayList<TypedFile>()

        imageFiles.forEach {
            tmp.add(TypedFile("image/jpg", it))
        }

        return tmp
    }

    @Throws(FileNotFoundException::class, FileUtils.FileEmptyException::class)
    fun getFiles(): List<File> {
        val tmp = ArrayList<File>()
        photoReceiptItems?.forEach {
            if (!TextUtils.isEmpty(it.imagePath)) {
                tmp.add(FileUtils.getFile(it.imagePath))
            }
        }

        return tmp
    }

    fun completed(): Boolean {
        return if (requireNumber) {
            hasNumber() && hasAmount() && hasTax()
        } else {
            hasAmount() && hasTax()
        }
    }

    fun completedWithPicture(): Boolean {
        var hasPicture = false

        photoReceiptItems?.forEach {
            if (!TextUtils.isEmpty(it.imagePath)) {
                hasPicture = true
                return@forEach
            }
        }

        return hasPicture && completed()
    }

    private fun hasNumber(): Boolean {
        val numberRegex = "[a-zA-Z0-9-./]+".toRegex()
        return !TextUtils.isEmpty(number) && number!!.matches(numberRegex)
    }

    private fun hasAmount(): Boolean {
        return !TextUtils.isEmpty(amount) && amount?.toDouble() != 0.0
    }

    private fun hasTax(): Boolean {
        return !TextUtils.isEmpty(tax)
    }
}
