package com.happyfresh.snowflakes.hoverfly.models.response

import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.models.Batch

/**
 * Created by galuh on 7/5/17.
 */

class BatchesResponse(
        @SerializedName("batches")
        var batches: List<Batch> = ArrayList())
