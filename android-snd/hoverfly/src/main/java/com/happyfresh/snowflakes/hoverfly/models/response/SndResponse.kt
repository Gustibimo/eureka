package com.happyfresh.snowflakes.hoverfly.models.response

import com.google.gson.annotations.SerializedName

/**
 * Created by rezarachman8932 on 11/11/15.
 */

class SndResponse {

    @SerializedName("lat")
    var latitude: Double? = null

    @SerializedName("lon")
    var longitude: Double? = null

    @SerializedName("stock_location_id")
    var stockLocationId: Long? = null
}
