package com.happyfresh.snowflakes.hoverfly.models.response

import com.google.gson.annotations.SerializedName

/**
 * Created by ifranseda on 10/20/15.
 */

class VersionUpdate {

    @SerializedName("require_update")
    var required: Boolean = false

    @SerializedName("version")
    var version: String? = null

    @SerializedName("apk")
    var apkUrl: String? = null
}
