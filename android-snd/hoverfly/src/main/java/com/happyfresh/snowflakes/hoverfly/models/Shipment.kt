package com.happyfresh.snowflakes.hoverfly.models

import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.config.Constant
import com.happyfresh.snowflakes.hoverfly.shared.db.AppDatabase
import com.raizlabs.android.dbflow.annotation.*
import com.raizlabs.android.dbflow.kotlinextensions.and
import com.raizlabs.android.dbflow.sql.language.SQLite
import com.raizlabs.android.dbflow.structure.BaseModel
import org.parceler.Parcel
import java.io.Serializable
import java.util.*

/**
 * Created by ifranseda on 11/13/14.
 */

@Parcel
@Table(database = AppDatabase::class)
class Shipment : BaseModel(), IServiceModel, Serializable {

    @SerializedName("id")
    @Column
    @Unique(unique = true)
    @Index
    @PrimaryKey
    var remoteId: Long? = 0L

    @Column
    var state: String? = null

    @Column
    @ForeignKey(saveForeignKeyModel = true)
    @Index
    var order: Order? = null

    @Column
    @ForeignKey(saveForeignKeyModel = true)
    var slot: Slot? = null

    @Column
    @ForeignKey(saveForeignKeyModel = true)
    var address: Address? = null

    @SerializedName("total_items")
    @Column
    var totalItems: Int = 0

    @SerializedName("shopping_batch_status")
    @Column
    var shoppingStatus: String? = null

    @SerializedName("estimated_shopping_time")
    @Column
    var estimatedTime: Double? = null

    @SerializedName("shopping_job")
    @Column
    @ForeignKey(saveForeignKeyModel = false)
    var shoppingJob: Job? = null

    @SerializedName("delivery_job")
    @Column
    @ForeignKey(saveForeignKeyModel = false)
    var deliveryJob: Job? = null

    @SerializedName("user_id")
    @Column
    var userId: Long? = null

    @ForeignKey(stubbedRelationship = true)
    var batch: Batch? = null

    @Column
    @Index
    var colorResource: Int = 0

    @Column
    var isCancelled: Boolean = false

    @SerializedName("delivery_order")
    @Column
    var deliveryOrder: Int? = null

    @SerializedName("delivery_fleet_type")
    @Column(name = "delivery_fleet_type")
    var deliveryFleetType: String? = null

    @get:Synchronized
    var deliveryItems: List<LineItem> = ArrayList()

    val allItems: List<LineItem>
        get() {
            return SQLite.select().from(LineItem::class.java)
                    .where(LineItem_Table.shipment_remoteId.eq(remoteId))
                    .orderBy(LineItem_Table.taxonOrder.desc())
                    .orderBy(LineItem_Table.taxonName.asc())
                    .queryList()
        }

    val user: User?
        get() {
            if (userId == null) return null
            return SQLite.select().from(User::class.java).where(User_Table.remoteId.eq(userId)).querySingle()
        }

    val customerName: String
        get() {
            return when {
                (this.order?.companyId != 0L)                -> this.order?.companyName ?: ""
                (!this.order?.userFirstName.isNullOrBlank()) -> this.order?.userFirstName!!
                else                                         -> this.order?.userFullName ?: ""
            }
        }

    val recipientName: String
        get() {
            return when {
                (this.order?.companyId != 0L) -> this.order?.companyName ?: ""
                else                          -> this.address?.fullName ?: ""
            }
        }

    val shoppedCount: Int
        get() {
            return allItems.sumBy {
                if (it.totalFlagged > 0 && it.flag?.equals("out_of_stock") == true) {
                    it.totalShopped + it.totalFlagged
                }
                else {
                    it.totalShopped
                }
            }
        }

    val outOfStockItems: List<LineItem>
        get() {
            val outOfStocks = ArrayList<LineItem>()
            for (item in allItems.filter { (it.total - it.bundleQuantity) > it.totalShopped || it.totalReplaced > 0}) {
                item.replacementType?.let {
                    if (it.isNotEmpty() && !it.equals(Constant.DONT_REPLACE, ignoreCase = true)) {
                        val remaining = item.totalReplacementRemaining()
                        if (remaining > 0 && item.flag.equals("out_of_stock") || item.totalReplacedShopper > 0) {
                            outOfStocks.add(item)
                        }
                    }
                }
            }

            Collections.sort(outOfStocks, CompareBatchList())

            return outOfStocks
        }

    val pendingCount: Int
        get() = totalItems - shoppedCount

    fun deleteAllItems() {
        SQLite.delete().from(LineItem::class.java).where(LineItem_Table.shipment_remoteId.eq(remoteId)).execute()
    }

    private inner class CompareBatchList : Comparator<LineItem> {
        override fun compare(lhs: LineItem, rhs: LineItem): Int {
            return lhs.replacementType!!.compareTo(rhs.replacementType!!)
        }
    }

    override fun save(): Boolean {
        val saved = super.save()

        if (shoppingJob != null) {
            shoppingJob?.apply { jobType = Job.JobType.SHOPPING }?.save()
        }
        else {
            SQLite.delete().from(Job::class.java).where(Job_Table.shipmentId.eq(this.remoteId)).and(
                    Job_Table.jobType.eq(Job.JobType.SHOPPING)).execute()
        }

        if (deliveryJob != null) {
            deliveryJob?.apply { jobType = Job.JobType.DELIVERY }?.save()
        }
        else {
            SQLite.delete().from(Job::class.java).where(Job_Table.shipmentId.eq(this.remoteId)).and(
                    Job_Table.jobType.eq(Job.JobType.DELIVERY)).execute()
        }

        return saved
    }

    companion object {

        @JvmStatic
        fun findById(remoteId: Long?): Shipment? {
            return SQLite.select().from(Shipment::class.java).where(Shipment_Table.remoteId.eq(remoteId)).querySingle()
        }

        @JvmStatic private fun findStarted(): List<Shipment> {
            val jobs = SQLite.select().from(Job::class.java).where(
                    Job_Table.jobType.eq(Job.JobType.SHOPPING).and(Job_Table.state.eq("started"))).queryList()

            val tmp = ArrayList<Long>()
            for (job in jobs) {
                tmp.add(job.shipmentId!!)
            }

            return SQLite.select().from(Shipment::class.java).where(Shipment_Table.remoteId.`in`(tmp)).queryList()
        }

        @JvmStatic private fun findAll(): List<Shipment> {
            return SQLite.select().from(Shipment::class.java).queryList()
        }

        @JvmStatic
        val size: Int
            get() = findAll().size

        @JvmStatic private fun hasActiveJob(): Boolean {
            return findStarted().isNotEmpty()
        }

        @JvmStatic private fun findActiveJob(): Shipment? {
            val jobs = SQLite.select().from(Job::class.java).where(
                    Job_Table.jobType.eq(Job.JobType.SHOPPING).and(Job_Table.state.eq("started"))).queryList()

            val tmp = jobs.map { it.shipmentId!! }
            return SQLite.select().from(Shipment::class.java).where(Shipment_Table.remoteId.`in`(tmp)).querySingle()
        }

        @JvmStatic
        fun findByShoppingJob(jobId: Long?): Shipment? {
            val job = SQLite.select().from(Job::class.java).where(Job_Table.remoteId.eq(jobId)).querySingle()
            return SQLite.select().from(Shipment::class.java).where(Shipment_Table.remoteId.eq(job?.shipmentId)).querySingle()
        }
    }
}
