package com.happyfresh.snowflakes.hoverfly.models.payload

import com.google.gson.annotations.SerializedName

/**
 * Created by ifranseda on 4/23/15.
 */

class RejectionPayload : SerializablePayload() {

    @SerializedName("variant_id")
    var variantId: Long? = null

    var quantity: Int = 0

    var reason: String? = null

    @SerializedName("replacement_id")
    var replacementId: Long? = null
}
