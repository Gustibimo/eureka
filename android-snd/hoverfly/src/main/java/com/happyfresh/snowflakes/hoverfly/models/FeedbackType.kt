package com.happyfresh.snowflakes.hoverfly.models

import com.google.gson.annotations.SerializedName
import org.parceler.Parcel

/**
 * Created by kharda on 23/2/17.
 */

@Parcel
class FeedbackType {

    enum class Type {
        @SerializedName("oos")
        OOS,
        @SerializedName("price_discrepancy")
        PRICE_DISCREPANCY,
        @SerializedName("product_mismatch")
        PRODUCT_MISMATCH,
        @SerializedName("weight_discrepancy")
        WEIGHT_DISCREPANCY
    }

    @SerializedName("type_id")
    var typeId: Int? = null

    @SerializedName("display_name")
    var displayName: String? = null

    @SerializedName("need_detail")
    var needDetail: Boolean = false

    @SerializedName("feedback_type")
    var feedbackType: Type? = null

    override fun toString(): String {
        return displayName!!
    }
}
