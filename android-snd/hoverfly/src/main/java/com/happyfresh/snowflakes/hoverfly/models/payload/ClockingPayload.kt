package com.happyfresh.snowflakes.hoverfly.models.payload

import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.models.UserLocation

class ClockingPayload(@SerializedName("snd") internal var userLocation: UserLocation)
