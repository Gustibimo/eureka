package com.happyfresh.snowflakes.hoverfly.models

import com.happyfresh.snowflakes.hoverfly.utils.FileUtils
import retrofit.mime.TypedFile
import java.io.File

/**
 * Created by rsavianto on 11/14/17.
 */

class PhotoItem(val imagePath: String) {

    val attachment: TypedFile
        get() = TypedFile("image/jpg", file)

    val file: File = FileUtils.getFile(imagePath)
}