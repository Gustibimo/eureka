package com.happyfresh.snowflakes.hoverfly.utils

import android.util.Log

/**
 * Created by ifranseda on 11/17/14.
 */

class LogUtils {

    companion object {

        private val LOG_PREFIX = "HappyFresh:"
        private val LOG_PREFIX_LENGTH = LOG_PREFIX.length
        private val MAX_LOG_TAG_LENGTH = 23

        @JvmStatic
        fun tagLogger(str: String): String {
            if (str.length > MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH) {
                // Returns end of class name
                return LOG_PREFIX + str.substring(MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH)
            }

            return LOG_PREFIX + str
        }

        @JvmStatic
        fun LOG(message: String?) {
            val className = Throwable().stackTrace[1].className
            val tag = tagLogger(className)

            if (message != null) {
                Log.d(tag, message)
            }
        }

        @JvmStatic
        fun WTF(message: String?) {
            val className = Throwable().stackTrace[1].className
            val tag = tagLogger(className)

            if (message != null) {
                Log.wtf(tag, message)
            }
        }

        @JvmStatic
        @JvmOverloads
        fun logEvent(name: String, message: String? = null) {
            LogUtils.LOG(name + "\n" + message)
        }
    }
}
