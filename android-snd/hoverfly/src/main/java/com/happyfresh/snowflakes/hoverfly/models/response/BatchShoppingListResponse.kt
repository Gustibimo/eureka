package com.happyfresh.snowflakes.hoverfly.models.response

import com.happyfresh.snowflakes.hoverfly.models.Batch

/**
 * Created by ifranseda on 9/9/15.
 */

class BatchShoppingListResponse {

    var batch: Batch? = null

    var data: List<ShoppingListResponse>? = null
}
