package com.happyfresh.snowflakes.hoverfly.exceptions

import org.apache.commons.lang3.StringUtils

/**
 * Created by ifranseda on 30/12/2016.
 */

class ServerException : Throwable {

    var code: Int = 0

    constructor(message: String) : super(message)

    constructor(code: Int, message: String) : super(message) {
        this.code = code
    }

    override val message: String?
        get() = StringUtils.capitalize(super.message)
}
