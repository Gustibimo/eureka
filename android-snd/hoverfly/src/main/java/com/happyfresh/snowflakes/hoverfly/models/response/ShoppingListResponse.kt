package com.happyfresh.snowflakes.hoverfly.models.response

import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.models.LineItem
import com.happyfresh.snowflakes.hoverfly.models.Shipment

/**
 * Created by ifranseda on 9/9/15.
 */

class ShoppingListResponse {

    @SerializedName("shipment_id")
    var shipmentId: Long? = null

    @SerializedName("order_number")
    var orderNumber: String? = null

    val shipment: Shipment?
        get() = Shipment.findById(shipmentId)

    var items: List<LineItem>? = null
        get() {
            field?.map { it.shipment = this.shipment }
            return field
        }
}
