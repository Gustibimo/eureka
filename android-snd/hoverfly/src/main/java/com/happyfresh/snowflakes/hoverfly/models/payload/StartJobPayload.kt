package com.happyfresh.snowflakes.hoverfly.models.payload

import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.models.Job

/**
 * Created by ifranseda on 11/25/14.
 */

class StartJobPayload @JvmOverloads constructor(
        @SerializedName("shipment_id")
        var shipmentId: Long?,

        @SerializedName("job_type")
        var jobType: String = Job.JobType.SHOPPING) : SerializablePayload()
