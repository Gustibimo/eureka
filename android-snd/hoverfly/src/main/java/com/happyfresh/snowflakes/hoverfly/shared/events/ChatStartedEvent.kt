package com.happyfresh.snowflakes.hoverfly.shared.events

/**
 * Created by kharda on 17/9/18
 */
class ChatStartedEvent(data: Boolean) : BaseEvent<Boolean>(data)