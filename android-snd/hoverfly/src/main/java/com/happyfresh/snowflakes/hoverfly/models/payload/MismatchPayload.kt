package com.happyfresh.snowflakes.hoverfly.models.payload

import com.google.gson.annotations.SerializedName

/**
 * Created by ifranseda on 07/04/2017.
 */

class MismatchPayload : SerializablePayload() {

    @SerializedName("variant_id")
    var variantId: Long = 0L

    @SerializedName("order_id")
    var orderId: Long = 0L

    @SerializedName("stock_location_id")
    var stockLocationId: Long = 0L
}
