package com.happyfresh.snowflakes.hoverfly.models.payload

import com.google.gson.annotations.SerializedName

/**
 * Created by ifranseda on 4/23/15.
 */

class ListItemRejectionPayload : SerializablePayload() {

    @SerializedName("items")
    var items: List<RejectionPayload>? = null
}

