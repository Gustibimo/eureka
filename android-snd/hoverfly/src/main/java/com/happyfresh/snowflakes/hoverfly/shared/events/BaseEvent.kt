package com.happyfresh.snowflakes.hoverfly.shared.events

/**
 * Created by ifranseda on 12/10/2017.
 */

abstract class BaseEvent<out T>(val data: T)