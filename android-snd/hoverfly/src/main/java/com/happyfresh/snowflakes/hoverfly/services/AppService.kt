package com.happyfresh.snowflakes.hoverfly.services

import com.happyfresh.snowflakes.hoverfly.api.IAppService
import com.happyfresh.snowflakes.hoverfly.models.UserLocation
import com.happyfresh.snowflakes.hoverfly.models.payload.ClockingPayload
import com.happyfresh.snowflakes.hoverfly.models.response.FeedbackTypeResponse
import com.happyfresh.snowflakes.hoverfly.models.response.PauseResponse
import com.happyfresh.snowflakes.hoverfly.models.response.StatusResponse
import com.happyfresh.snowflakes.hoverfly.models.response.VersionUpdate
import retrofit.client.Response
import rx.Observable

/**
 * Created by aldi on 6/22/17.
 */

class AppService : BaseService<IAppService>() {

    override fun serviceClass(): Class<IAppService> {
        return IAppService::class.java
    }

    fun checkUpdate(currentVersion: Int): Observable<VersionUpdate> {
        return observe(api().checkUpdate(currentVersion))
    }

    fun clockIn(clockingPayload: ClockingPayload): Observable<UserLocation> {
        return observe(api().clockIn(clockingPayload))
    }

    fun status(): Observable<StatusResponse> {
        return observe(api().status())
    }

    fun pause(): Observable<PauseResponse> {
        return observe(api().pause())
    }

    fun resume(): Observable<Response> {
        return observe(api().resume())
    }

    fun clockOut(): Observable<StatusResponse> {
        return observe(api().clockOut())
    }

    fun feedbackTypes(variantId: Long): Observable<FeedbackTypeResponse> {
        return observe(api().feedbackTypes(variantId))
    }
}
