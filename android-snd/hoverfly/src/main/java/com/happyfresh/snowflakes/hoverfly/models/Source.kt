package com.happyfresh.snowflakes.hoverfly.models

import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.shared.db.AppDatabase
import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import com.raizlabs.android.dbflow.annotation.Unique
import com.raizlabs.android.dbflow.structure.BaseModel
import org.parceler.Parcel

/**
 * Created by ifranseda on 11/14/14.
 */

@Parcel
@Table(database = AppDatabase::class)
class Source : BaseModel() {

    @SerializedName("id")
    @Column
    @Unique
    @PrimaryKey
    var remoteId: Long? = null

    @Column
    var month: Int = 0

    @Column
    var year: Int = 0

    @Column
    var ccType: String? = null

    @Column
    var lastDigits: Int = 0

    @Column
    var name: String? = null

    @Column
    var gatewayCustomerProfileId: Long? = null

    @Column
    var gatewayPaymentProfileId: Long? = null
}
