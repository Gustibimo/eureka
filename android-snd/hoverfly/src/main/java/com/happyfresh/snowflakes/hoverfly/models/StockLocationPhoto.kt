package com.happyfresh.snowflakes.hoverfly.models

import com.google.gson.annotations.SerializedName
import org.parceler.Parcel

/**
 * Created by ifranseda on 1/26/16.
 */

@Parcel
class StockLocationPhoto {

    @SerializedName("url")
    var original: String? = null

    @SerializedName("medium_url")
    var mediumUrl: String? = null

    @SerializedName("thumb_url")
    var thumbnail: String? = null
}
