package com.happyfresh.snowflakes.hoverfly.api

import com.happyfresh.snowflakes.hoverfly.models.User
import retrofit2.http.Header
import retrofit2.http.POST
import rx.Observable

/**
 * Created by aldi on 6/13/17.
 */

interface ILoginService {

    @POST("login")
    fun login(@Header("Authorization")
              auth: String,
              @Header("X-Spree-Client-Token")
              token: String): Observable<User>
}
