package com.happyfresh.snowflakes.hoverfly.models.response

import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.models.PendingHandover

/**
 * Created by kharda on 28/06/18.
 */

class PendingHandoverListResponse(
        @SerializedName("pending_handovers")
        var pendingHandovers: List<PendingHandover> = ArrayList())