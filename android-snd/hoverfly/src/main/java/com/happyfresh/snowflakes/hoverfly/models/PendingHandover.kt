package com.happyfresh.snowflakes.hoverfly.models

import com.google.gson.annotations.SerializedName
import com.raizlabs.android.dbflow.structure.BaseModel
import org.parceler.Parcel
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by kharda on 28/06/18.
 */

@Parcel
class PendingHandover : BaseModel(), IServiceModel, Serializable {
    @SerializedName("batch_id")
    var batchId: Long? = null

    @SerializedName("order_number")
    var orderNumber: String? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("pin")
    var pin: String? = null

    @SerializedName("driver_name")
    var driverName: String? = null

    @SerializedName("driver_phone")
    var driverPhone: String? = null

    @SerializedName("driver_photo_url")
    var driverPhotoUrl: String? = null

    @SerializedName("created_at")
    var bookingTime: String? = null

    @SerializedName("slot_start_time")
    var slotStartTime: String? = null

    @SerializedName("slot_end_time")
    var slotEndTime: String? = null

    var indicatorColor: Int? = null

    fun getBookingTIme(): String? {
        if (bookingTime == null) {
            return null
        }

        val bookingTime = parseDate(this.bookingTime!!)

        val hourFormat = SimpleDateFormat("HH:mm:ss")
        return hourFormat.format(bookingTime)
    }

    fun getSlotTimeDisplay(): String? {
        if (slotStartTime == null) {
            return null
        }

        val slotStartTime = parseDate(this.slotStartTime!!)
        val slotEndTime = parseDate(this.slotEndTime!!)

        val hourFormat = SimpleDateFormat("HH:mm")
        return String.format("%s - %s", hourFormat.format(slotStartTime), hourFormat.format(slotEndTime))
    }

    private fun parseDate(date: String): Date {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        dateFormat.timeZone = TimeZone.getTimeZone("UTC")

        return dateFormat.parse(date)
    }

    fun isActive(): Boolean {
        return status.equals("ALLOCATING") || status.equals("PICKING_UP")
    }
}