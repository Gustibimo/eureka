package com.happyfresh.snowflakes.hoverfly.models.response

import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.models.FeedbackType

/**
 * Created by kharda on 23/2/17.
 */

class FeedbackTypeResponse {

    @SerializedName("snd_feedbacks")
    var feedbackTypes: List<FeedbackType> = ArrayList()
}