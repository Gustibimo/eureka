package com.happyfresh.snowflakes.hoverfly.services

import com.happyfresh.snowflakes.hoverfly.api.ILoginService
import com.happyfresh.snowflakes.hoverfly.models.User

import rx.Observable

/**
 * Created by aldi on 6/13/17.
 */

class LoginService : BaseService<ILoginService>() {
    override fun serviceClass(): Class<ILoginService> {
        return ILoginService::class.java
    }

    fun login(basicAuth: String, clientToken: String): Observable<User> {
        return observe(api().login(basicAuth, clientToken))
    }
}
