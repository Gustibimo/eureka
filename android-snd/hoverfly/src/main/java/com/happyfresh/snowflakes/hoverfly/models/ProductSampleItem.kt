package com.happyfresh.snowflakes.hoverfly.models

import android.content.Context
import android.support.v4.content.ContextCompat
import android.text.TextUtils

import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.R
import com.happyfresh.snowflakes.hoverfly.shared.db.AppDatabase
import com.raizlabs.android.dbflow.annotation.*
import com.raizlabs.android.dbflow.sql.language.SQLite
import com.raizlabs.android.dbflow.structure.BaseModel
import org.parceler.Parcel

/**
 * Created by kharda on 9/9/16.
 */

@Parcel
@Table(database = AppDatabase::class)
class ProductSampleItem : BaseModel() {

    @SerializedName("campaign_id")
    @Column
    var campaignId: Int = 0

    @SerializedName("rule_id")
    @Column
    var ruleId: Int = 0

    @SerializedName("remote_id")
    @Column
    @PrimaryKey
    var remoteId: Long? = null

    @SerializedName("id")
    @Column
    var productId: Long? = null

    @SerializedName("sku")
    @Column
    var sku: String? = null

    @Column
    var total: Int = 0

    @SerializedName("total_shopped")
    @Column
    var totalShopped: Int = 0

    @SerializedName("total_flagged")
    @Column
    var totalFlagged: Int = 0

    @SerializedName("flag")
    @Column
    var flag: String? = null

    @Column
    var price: Double = 0.toDouble()

    @SerializedName("cost_price")
    @Column
    var costPrice: Double = 0.toDouble()

    @SerializedName("supermarket_unit_cost_price")
    @Column
    var supermarketUnitCostPrice: Double = 0.toDouble()

    @SerializedName("display_cost_price")
    @Column
    var displayCostPrice: String? = null

    @SerializedName("display_supermarket_unit_cost_price")
    @Column
    var displaySupermarketUnitCostPrice: String? = null

    @SerializedName("total_cost_price")
    @Column
    var totalCostPrice: Double = 0.toDouble()

    @SerializedName("display_total_cost_price")
    @Column
    var displayTotalCostPrice: String? = null

    @SerializedName("single_display_amount")
    @Column
    var singleDisplayAmount: String? = null

    @SerializedName("display_amount")
    @Column
    var displayAmount: String? = null

    @Column
    var unit: String? = null

    @SerializedName("display_unit")
    @Column
    var displayUnit: String? = null

    @SerializedName("display_average_weight")
    @Column
    var displayAverageWeight: String? = null

    @SerializedName("natural_average_weight")
    @Column
    var naturalAverageWeight: Double? = null

    @SerializedName("supermarket_unit")
    @Column
    var supermarketUnit: String? = null

    @SerializedName("display_actual_weight")
    @Column
    var displayActualWeight: String? = null

    @SerializedName("taxon_name")
    @Column
    @Index
    var taxonName: String? = null

    @SerializedName("taxon_order")
    @Column
    @Index
    var taxonOrder: Int = 0

    @SerializedName("out_of_store_f")
    @Column(name = "out_of_store_f")
    @Index
    var isOutOfStore: Boolean = false

    @Column
    @Index
    @ForeignKey(saveForeignKeyModel = false)
    var variant: Variant? = null

    @Column
    var batchId: Long? = null

    @Column
    var orderNumber: String? = null

    @Column
    var customerName: String? = null

    val flagName: String
        get() {
            var flagName = ""
            if (this.flag != null) {
                flagName = this.flag!!.replace("_", " ").toUpperCase()
            }
            return flagName
        }

    val purchasedAmount: String
        get() {
            return totalShopped.toString() + " of " + total.toString()
        }

    fun getTotalNaturalAverageWeight(count: Int): Double? {
        return naturalAverageWeight!! * count
    }

    val totalNaturalAverageWeight: Double?
        get() {
            if (!hasNaturalUnit()) {
                return 0.0
            }

            var count = total
            if (totalShopped > 0) {
                count = totalShopped
            }

            return getTotalNaturalAverageWeight(count)
        }

    val status: PurchaseStatus
        get() {
            if (flag != null && totalShopped == 0) {
                return PurchaseStatus.OUT_OF_STOCK
            }

            if (totalShopped > 0) {
                return PurchaseStatus.COMPLETED
            }

            return PurchaseStatus.NOT_STARTED
        }

    fun hasNaturalUnit(): Boolean {
        /* No other field to indicate the item has natural unit or not */
        return !TextUtils.isEmpty(displayAverageWeight)
    }

    override fun save(): Boolean {
        val saved = super.save()
        this.variant?.save()

        return saved
    }

    enum class PurchaseStatus {
        COMPLETED,
        OUT_OF_STOCK,
        NOT_STARTED;

        fun statusName(context: Context): String {
            if (this == COMPLETED) {
                return context.resources.getString(R.string.purchase_status_completed)
            }

            if (this == OUT_OF_STOCK) {
                return context.resources.getString(R.string.purchase_status_not_found)
            }

            return ""
        }

        fun backgroundColor(context: Context): Int {
            if (this == COMPLETED) {
                return ContextCompat.getColor(context, R.color.purchase_status_completed)
            }

            if (this == OUT_OF_STOCK) {
                return ContextCompat.getColor(context, R.color.purchase_status_not_found)
            }

            return ContextCompat.getColor(context, android.R.color.white)
        }

        fun textColor(context: Context): Int {
            if (this == COMPLETED) {
                return ContextCompat.getColor(context, R.color.purchase_status_text_completed)
            }

            return ContextCompat.getColor(context, android.R.color.white)
        }
    }

    companion object {

        @JvmStatic
        fun findById(id: Long): ProductSampleItem? {
            return SQLite.select().from(ProductSampleItem::class.java).where(ProductSampleItem_Table.remoteId.eq(id)).querySingle()
        }

        @JvmStatic
        fun findByProductIdAndOrderNumber(id: Long, orderNumber: String): ProductSampleItem? {
            return SQLite.select().from(ProductSampleItem::class.java).where(ProductSampleItem_Table.productId.eq(id)).and(
                    ProductSampleItem_Table.orderNumber.eq(orderNumber)).querySingle()
        }

        @JvmStatic
        fun findByProductId(id: Long): ProductSampleItem? {
            return SQLite.select().from(ProductSampleItem::class.java).where(ProductSampleItem_Table.productId.eq(id)).querySingle()
        }

        @JvmStatic
        fun findByBatchId(batchId: Long): List<ProductSampleItem> {
            return SQLite.select().from(ProductSampleItem::class.java).where(ProductSampleItem_Table.batchId.eq(batchId)).queryList()
        }

        @JvmStatic
        fun findByBatchIdAndFilterByInStore(batchId: Long): List<ProductSampleItem> {
            return findByBatchIdAndFilterByIsOutOfStore(batchId, false)
        }

        @JvmStatic
        fun findByBatchIdAndFilterByOutStore(batchId: Long): List<ProductSampleItem> {
            return findByBatchIdAndFilterByIsOutOfStore(batchId, true)
        }

        @JvmStatic private fun findByBatchIdAndFilterByIsOutOfStore(batchId: Long, isOutOfStore: Boolean): List<ProductSampleItem> {
            return SQLite.select().from(ProductSampleItem::class.java).where(ProductSampleItem_Table.batchId.eq(batchId)).and(
                    ProductSampleItem_Table.out_of_store_f.eq(isOutOfStore)).queryList()
        }

        @JvmStatic
        fun isThereInStoreSample(batchId: Long): Boolean {
            val list = findByBatchIdAndFilterByInStore(batchId)
            return list.isNotEmpty()
        }

        @JvmStatic
        fun isThereOutStoreSample(batchId: Long): Boolean {
            val list = findByBatchIdAndFilterByOutStore(batchId)
            return list.isNotEmpty()
        }
    }
}
