package com.happyfresh.snowflakes.hoverfly.models.payload

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by rsavianto on 2/9/15.
 */
class PlacementPayload(@SerializedName("stock_location_ids") var stockLocationIds: List<Long> = ArrayList())
