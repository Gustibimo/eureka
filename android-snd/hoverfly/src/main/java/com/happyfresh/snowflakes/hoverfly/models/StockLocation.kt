package com.happyfresh.snowflakes.hoverfly.models

import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.shared.db.AppDatabase
import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import com.raizlabs.android.dbflow.annotation.Unique
import com.raizlabs.android.dbflow.sql.language.SQLite
import com.raizlabs.android.dbflow.structure.BaseModel
import org.parceler.Parcel
import java.io.Serializable

/**
 * Created by ifranseda on 11/14/14.
 */

@Parcel
@Table(database = AppDatabase::class)
class StockLocation : BaseModel(), IServiceModel, Serializable {

    object StoreType {
        const val NORMAL: String = "original"

        const val SPECIALTY: String = "special"
    }

    @SerializedName("id")
    @Column
    @Unique(unique = true)
    @PrimaryKey
    var remoteId: Long = 0

    @Column
    var name: String? = null

    @Column
    var address1: String? = null

    @Column
    var address2: String? = null

    @SerializedName("state_name")
    @Column
    var stateName: String? = null

    @SerializedName("country_id")
    @Column
    var countryId: Long? = null

    @SerializedName("store_type")
    @Column
    var storeType: String? = null

    @SerializedName("hybrid_store")
    @Column
    var isHybridStore: Boolean = false

    @Column
    var storePhoto: String? = null

    var location: Location? = null

    @SerializedName("photo")
    var photo: StockLocationPhoto? = null

    val isSpecialtyStore: Boolean
        get() {
            return StoreType.SPECIALTY == this.storeType
        }

    var country: Country? = null
        get() {
            if (field == null) {
                field = Country.findById(countryId)
            }

            return field
        }

    var cluster: Cluster? = null

    override fun save(): Boolean {
        val saved = super.save()

        country?.let {
            var country = Country.findById(this.countryId)
            if (country == null) {
                country = this.country
                country?.save()
            }
        }

        return saved
    }

    override fun equals(other: Any?): Boolean {
        return if (other is StockLocation) {
            other.remoteId == this.remoteId;
        } else {
            false
        }
    }

    override fun hashCode(): Int {
        return remoteId.toInt() * 23;
    }

    companion object {
        @JvmStatic
        fun findById(remoteId: Long?): StockLocation? {
            return SQLite.select().from(StockLocation::class.java).where(StockLocation_Table.remoteId.eq(remoteId)).querySingle()
        }
    }
}
