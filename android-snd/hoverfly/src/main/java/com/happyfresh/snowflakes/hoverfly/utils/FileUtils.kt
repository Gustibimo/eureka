package com.happyfresh.snowflakes.hoverfly.utils

import android.content.Context
import android.content.ContextWrapper
import android.net.Uri
import android.os.Environment
import android.os.FileObserver
import android.support.v4.content.FileProvider
import com.happyfresh.snowflakes.hoverfly.config.Constant
import java.io.*

/**
 * Created by ifranseda on 12/23/14.
 */

class FileUtils {

    companion object {

        private var queueFile: File? = null

        private var storageDir: File? = null

        private var failedQueueDir: File? = null

        @Throws(IOException::class)
        @JvmStatic
        fun copy(src: File, dst: File) {
            var `in`: InputStream? = null
            var out: OutputStream? = null

            try {
                `in` = FileInputStream(src)
                out = FileOutputStream(dst)

                // Transfer bytes from in to out
                val buf = ByteArray(1024)
                var len: Int = `in`.read(buf)
                while ((len) > 0) {
                    out.write(buf, 0, len)
                    len = `in`.read(buf)
                }
            }
            finally {
                if (`in` != null) {
                    `in`.close()
                }
                if (out != null) {
                    out.close()
                }
            }
        }

        @JvmStatic
        fun getFilesDir(context: Context, name: String): File {
            val directory = File(context.filesDir, name)

            if (!directory.exists()) {
                directory.mkdirs()
            }

            return directory
        }

        @JvmStatic
        fun getCacheDir(context: Context, name: String): File {
            val wrapper = ContextWrapper(context)
            var directory = File(wrapper.externalCacheDir, name)

            if (!directory.exists()) {
                directory.mkdirs()
            }

            if (!directory.canWrite()) {
                directory = getFilesDir(context, name)
            }

            return directory
        }

        @JvmStatic
        fun getQueueDir(context: Context): File {
            if (storageDir != null) {
                return storageDir!!
            }
            else {
                try {
                    val dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS)
                    storageDir = File(dir, Constant.QUEUE_DIRNAME)

                    if (!storageDir!!.exists()) {
                        storageDir!!.mkdirs()
                    }
                }
                catch (e: NoSuchFieldError) {
                    storageDir = context.filesDir
                }
                catch (e: Throwable) {
                    storageDir = context.filesDir
                }

                return storageDir!!
            }
        }

        @JvmStatic
        fun getFailedQueueDir(context: Context): File? {
            if (getQueueDir(context).canWrite()) {
                failedQueueDir = File(getQueueDir(context), Constant.FAILED_QUEUE_DIRNAME)

                if (!failedQueueDir!!.exists()) {
                    failedQueueDir!!.mkdirs()
                }

                return failedQueueDir
            }
            else {
                return null
            }
        }

        @JvmStatic
        fun getQueueFile(context: Context): File {
            if (queueFile != null && queueFile!!.exists()) {
                LogUtils.logEvent("Local Queue file", queueFile!!.absolutePath)
                return queueFile!!
            }
            else {
                val directory = getQueueDir(context)
                queueFile = File(directory, Constant.QUEUE_FILENAME)

                if (!queueFile!!.canWrite()) {
                    queueFile = File(context.filesDir, Constant.QUEUE_FILENAME)
                }

                LogUtils.logEvent("Local Queue file", queueFile!!.absolutePath)
                return queueFile!!
            }
        }

        @JvmStatic
        fun deleteQueueFile(context: Context): Boolean {
            var deleted = false
            if (queueFile != null) {
                deleted = getQueueFile(context).delete()
            }

            cleanDirectory(getQueueDir(context))

            queueFile = null
            return deleted
        }

        @JvmStatic private fun cleanDirectory(directory: File): Boolean {
            var deleted = false

            if (!directory.exists()) {
                return deleted
            }

            if (!directory.isDirectory) {
                return deleted
            }

            val files = directory.listFiles() ?: return deleted
            files.filter { it.exists() && it.delete() }.forEach { deleted = true }

            return deleted
        }

        @Throws(FileNotFoundException::class, FileEmptyException::class)
        @JvmStatic
        fun getFile(filePath: String?): File {
            if (filePath == null) {
                throw FileNotFoundException()
            }

            val file = File(filePath)
            if (!file.exists()) {
                throw FileNotFoundException()
            }

            val fileSize = file.length()
            if (fileSize == 0L) {
                throw FileEmptyException()
            }

            return file
        }
    }

    class FileEmptyException : IOException()
}
