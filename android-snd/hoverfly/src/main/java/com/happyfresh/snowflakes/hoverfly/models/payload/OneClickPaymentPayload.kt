package com.happyfresh.snowflakes.hoverfly.models.payload

import com.google.gson.annotations.SerializedName

/**
 * Created by kharda on 8/11/16.
 */

class OneClickPaymentPayload(
        @SerializedName("order_number")
        var orderNumber: String) : SerializablePayload()
