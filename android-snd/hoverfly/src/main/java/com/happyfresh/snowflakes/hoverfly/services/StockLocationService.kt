package com.happyfresh.snowflakes.hoverfly.services

import com.happyfresh.snowflakes.hoverfly.api.IStockLocationService
import com.happyfresh.snowflakes.hoverfly.models.payload.PlacementPayload
import com.happyfresh.snowflakes.hoverfly.models.response.PlacementResponse
import com.happyfresh.snowflakes.hoverfly.models.response.StockLocationResponse

import rx.Observable

/**
 * Created by galuh on 6/18/17.
 */

class StockLocationService : BaseService<IStockLocationService>() {

    override fun serviceClass(): Class<IStockLocationService> {
        return IStockLocationService::class.java
    }

    fun nearbyStores(lat: Double?, lon: Double?, distance: String): Observable<StockLocationResponse> {
        return observe(api().storeNearby(lat, lon, distance))
    }

    fun createPlacement(isoName: String, payload: PlacementPayload): Observable<PlacementResponse> {
        return observe(api().createPlacement(isoName, payload))
    }
}
