package com.happyfresh.snowflakes.hoverfly.models.payload

import com.google.gson.annotations.SerializedName

/**
 * Created by anton on 12/10/14.
 */

class ListItemReplacementPayload : SerializablePayload() {

    @SerializedName("items")
    var items: MutableList<ReplacementPayload> = mutableListOf()
}
