package com.happyfresh.snowflakes.hoverfly.models.payload

import com.google.gson.annotations.SerializedName
import org.parceler.Parcel

/**
 * Created by kharda on 24/8/18
 */
@Parcel
class SendBirdNotificationPayload {

    @SerializedName("channel")
    var channel: Channel? = null

    @SerializedName("sender")
    var sender: Sender? = null

    @SerializedName("recipient")
    var recipient: Recipient? = null

    fun getNotificationId(): Int {
        val allNonNumeric = Regex("\\D+")
        return channel?.name?.replace(allNonNumeric,"")!!.toInt()
    }

    @Parcel
    class Channel {

        var name: String? = null

        @SerializedName("channel_url")
        var channelUrl: String? = null
    }

    @Parcel
    class Sender {

        var name: String? = null
    }

    @Parcel
    class Recipient {

        var name: String? = null

        var id: String? = null
    }
}