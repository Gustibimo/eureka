package com.happyfresh.snowflakes.hoverfly.models.payload

import com.google.gson.annotations.SerializedName

/**
 * Created by ifranseda on 12/1/14.
 */

class ListFlagItemPayload : SerializablePayload() {
    @SerializedName("items")
    var items: ArrayList<FlagItemPayload> = arrayListOf()
}
