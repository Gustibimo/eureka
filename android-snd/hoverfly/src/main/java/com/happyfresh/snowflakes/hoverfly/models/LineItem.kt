package com.happyfresh.snowflakes.hoverfly.models

import android.content.Context
import android.support.v4.content.ContextCompat
import android.text.TextUtils
import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.R
import com.happyfresh.snowflakes.hoverfly.models.response.PriceChangeResponse
import com.happyfresh.snowflakes.hoverfly.shared.db.AppDatabase
import com.raizlabs.android.dbflow.annotation.*
import com.raizlabs.android.dbflow.sql.language.SQLite
import com.raizlabs.android.dbflow.structure.BaseModel
import org.parceler.Parcel

/**
 * Created by ifranseda on 11/14/14.
 */

@Parcel
@Table(database = AppDatabase::class)
class LineItem : BaseModel(), IServiceModel {
    @SerializedName("id")
    @Column
    var remoteId: Long = 0L

    @SerializedName("variant_id")
    @Column
    @PrimaryKey
    var variantId: Long = 0L

    @SerializedName("order_id")
    @Column
    @PrimaryKey
    var orderId: Long = 0L

    @SerializedName("stock_location_id")
    @Column
    @PrimaryKey
    var stockLocationId: Long = 0L

    @Column
    var total: Int = 0

    @Column
    var quantity: Int = 0

    @SerializedName("bundle_quantity")
    @Column
    var bundleQuantity: Int = 0

    @SerializedName("normal_quantity")
    @Column
    var normalQuantity: Int = 0

    @SerializedName("total_shopped")
    @Column
    var totalShopped: Int = 0

    @SerializedName("total_flagged")
    @Column
    var totalFlagged: Int = 0

    @SerializedName("total_replaced")
    @Column
    var totalReplaced: Int = 0

    @SerializedName("total_replaced_customer")
    @Column
    var totalReplacedCustomer: Int = 0

    @SerializedName("total_replaced_shopper")
    @Column
    var totalReplacedShopper: Int = 0

    @SerializedName("total_free_item")
    @Column
    var totalFreeItem: Int = 0

    @SerializedName("total_rejected_free_item")
    @Column
    var totalRejectedFreeItem: Int = 0

    @SerializedName("flag")
    @Column
    var flag: String? = null

    @SerializedName("rejected_items")
    var rejectionItems: List<RejectionItem> = ArrayList()

    @Column
    var price: Double? = null

    @SerializedName("cost_price")
    @Column
    var costPrice: Double? = null

    @SerializedName("supermarket_unit_cost_price")
    @Column
    var supermarketUnitCostPrice: Double? = 0.0

    @SerializedName("display_cost_price")
    @Column
    var displayCostPrice: String? = null

    @SerializedName("display_supermarket_unit_cost_price")
    @Column
    var displaySupermarketUnitCostPrice: String? = null

    @SerializedName("total_cost_price")
    @Column
    var totalCostPrice: Double? = null

    @SerializedName("display_total_cost_price")
    @Column
    var displayTotalCostPrice: String? = null

    @Column
    @ForeignKey(saveForeignKeyModel = false)
    var priceAmendment: PriceAmendment? = null
        get() {
            if (field == null) {
                field = PriceAmendment.findByLineItem(this)?.firstOrNull { it.replacementId == null }
            }

            return field
        }

    @SerializedName("single_display_amount")
    @Column
    var singleDisplayAmount: String? = null

    @SerializedName("display_amount")
    @Column
    var displayAmount: String? = null

    @Column
    var unit: String? = null

    @SerializedName("display_unit")
    @Column
    var displayUnit: String? = null

    @SerializedName("display_average_weight")
    @Column
    var displayAverageWeight: String? = null

    @SerializedName("natural_average_weight")
    @Column
    var naturalAverageWeight: Double? = null

    @SerializedName("supermarket_unit")
    @Column
    var supermarketUnit: String? = null

    @SerializedName("display_actual_weight")
    @Column
    var displayActualWeight: String? = null

    @SerializedName("actual_weight_adjustment_total")
    @Column
    var actualWeightAdjustmentTotal: Double? = null

    @SerializedName("actual_weight")
    @Column
    var actualWeight: Double? = null

    @Column
    var isFrozen: Boolean = false

    @SerializedName("taxon_name")
    @Column
    @Index
    var taxonName: String? = null

    @SerializedName("taxon_order")
    @Column
    @Index
    var taxonOrder: Int? = null

    @SerializedName("replacement_type")
    @Column
    var replacementType: String? = null

    @Column
    var displayBanner: String? = null

    @Column
    @ForeignKey(saveForeignKeyModel = false)
    var variant: Variant? = null

    @Column
    @ForeignKey(saveForeignKeyModel = false)
    var shipment: Shipment? = null

    @SerializedName("is_product_mismatch")
    @Column
    var isProductMismatch: Boolean = false

    @SerializedName("replacement")
    @ForeignKey(saveForeignKeyModel = false)
    var replacementShopper: Variant? = null

    @SerializedName("shopper_notes")
    @Column(name = "shopper_notes")
    var notes: String? = null

    @SerializedName("shopper_notes_fulfilled")
    @Column
    var isShopperNotesFulfilled: Boolean = false

    @SerializedName("replacements")
    var replacements: List<Replacement> = ArrayList()

    @SerializedName("price_amendments")
    var priceChanges: List<PriceChangeResponse> = ArrayList()

    @SerializedName("display_banner_text")
    var displayBannerText: List<String> = ArrayList()

    var isShoppingBag: Boolean = false

    @OneToMany(methods = arrayOf(OneToMany.Method.ALL), variableName = "rejectionItems", isVariablePrivate = true)
    fun rejectionItems(): List<RejectionItem> {
        rejectionItems = SQLite.select().from(RejectionItem::class.java)
                .where(RejectionItem_Table.item_variantId.eq(variantId))
                .and(RejectionItem_Table.item_orderId.eq(orderId))
                .and(RejectionItem_Table.item_stockLocationId.eq(stockLocationId))
                .queryList()

        return rejectionItems
    }

    @OneToMany(methods = arrayOf(OneToMany.Method.ALL), variableName = "replacements", isVariablePrivate = true)
    fun replacements(): List<Replacement> {
        if (replacements.isEmpty()) {
            replacements = SQLite.select().from(Replacement::class.java)
                    .where(Replacement_Table.lineItem_variantId.eq(variantId))
                    .and(Replacement_Table.lineItem_orderId.eq(orderId))
                    .and(Replacement_Table.lineItem_stockLocationId.eq(stockLocationId))
                    .queryList()
        }

        return replacements
    }

    val replacementItem: ShopperReplacement?
        get() {
            return shopperReplacements().lastOrNull()
        }

    @Deprecated(message = "HappyFresh customer app no longer provide customer replacement")
    val replacementCustomer: Variant? = null

    fun deleteReplacementShopper() {
        this.replacementShopper?.let { savedVariant ->
            val replacementShoppers = shopperReplacements()
            for (replacement in replacementShoppers) {
                SQLite.delete().from(SpreeImage::class.java).where(SpreeImage_Table.variant_remoteId.eq(replacement.variantId)).execute()
                SQLite.delete().from(StockItem::class.java).where(StockItem_Table.variant_remoteId.eq(replacement.variantId)).execute()

                SQLite.delete().from(ProductProperty::class.java).where(
                        ProductProperty_Table.variant_remoteId.eq(replacement.variantId)).execute()

                ShopperReplacement.findByReplacementId(replacement.replacementId)?.delete()

                replacement.variant?.delete()
                replacement.delete()

                savedVariant.delete()
            }
        }
    }

    val replacementShopperId: Long?
        get() {
            return replacementShopper?.remoteId
        }

    val priceAmendmentShopperReplacement: PriceAmendment?
        get() {
            val priceAmendments = SQLite.select().from(PriceAmendment::class.java)
                    .where(PriceAmendment_Table.variantId.eq(variantId))
                    .and(PriceAmendment_Table.orderId.eq(orderId))
                    .and(PriceAmendment_Table.stockLocationId.eq(stockLocationId))
                    .queryList()
            return priceAmendments.lastOrNull { it.replacementId != null && it.replacementId == replacementShopperId }
        }

    val flagName: String
        get() {
            var flagName = ""
            if (this.flag != null) {
                flagName = this.flag!!.replace("_", " ").toUpperCase()
            }
            return flagName
        }

    val purchasedAmount: String
        get() {
            return if (totalShopped > 0 || totalFlagged > 0) {
                (totalShopped - totalReplaced).toString() + " of " + total.toString()
            }
            else {
                total.toString()
            }
        }

    fun getTotalNaturalAverageWeight(count: Int): Double? {
        return naturalAverageWeight!! * count
    }

    val totalNaturalAverageWeight: Double?
        get() {
            if (!hasNaturalUnit()) {
                return 0.0
            }

            var count = total
            if (totalShopped > 0) {
                count = totalShopped
            }

            return getTotalNaturalAverageWeight(count)
        }

    fun totalOutOfStock(): Int {
        return total - totalShopped
    }

    fun totalReplacementRemaining(): Int {
        return total - totalShopped + totalReplacedShopper
    }

    val status: PurchaseStatus
        get() {
            flag?.let {
                if (it.equals("on_replace", ignoreCase = true)) {
                    return PurchaseStatus.ON_REPLACE
                }
            }

            if (totalShopped == total) {
                if (totalReplacedShopper == 0) {
                    return PurchaseStatus.COMPLETED
                }
                else {
                    if (totalShopped - totalReplaced > 0) {
                        return PurchaseStatus.FOUND_SOME
                    }
                    else {
                        return PurchaseStatus.NOT_FOUND
                    }
                }
            }

            if (totalFlagged == total) {
                return PurchaseStatus.NOT_FOUND
            }

            if (totalFlagged > 0 && totalShopped > 0 && totalShopped < total || total == totalShopped + totalFlagged) {
                return PurchaseStatus.FOUND_SOME
            }

            return PurchaseStatus.NOT_STARTED
        }

    fun hasNaturalUnit(): Boolean {
        return !TextUtils.isEmpty(displayAverageWeight)
    }

    enum class PurchaseStatus {
        COMPLETED,
        NOT_FOUND,
        FOUND_SOME,
        NOT_STARTED,
        ON_REPLACE;

        fun statusName(context: Context): String {
            if (this == COMPLETED) {
                return context.resources.getString(R.string.purchase_status_completed)
            }

            if (this == NOT_FOUND) {
                return context.resources.getString(R.string.purchase_status_not_found)
            }

            if (this == FOUND_SOME) {
                return context.resources.getString(R.string.purchase_status_found_some)
            }

            return ""
        }

        fun backgroundColor(context: Context): Int {
            if (this == COMPLETED) {
                return ContextCompat.getColor(context, R.color.purchase_status_completed)
            }

            if (this == NOT_FOUND) {
                return ContextCompat.getColor(context, R.color.purchase_status_not_found)
            }

            if (this == FOUND_SOME) {
                return ContextCompat.getColor(context, R.color.purchase_status_found_some)
            }

            return ContextCompat.getColor(context, android.R.color.white)
        }

        fun textColor(context: Context): Int {
            if (this == COMPLETED) {
                return ContextCompat.getColor(context, R.color.purchase_status_text_completed)
            }

            return ContextCompat.getColor(context, android.R.color.white)
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false

        other as LineItem
        if (this.variantId == other.variantId && this.orderId == other.orderId && this.stockLocationId == other.stockLocationId) return true

        return false
    }

    override fun save(): Boolean {
        this.priceChanges.forEach {
            PriceAmendment.createFromResponse(it).let {
                if (it.replacementId == null) {
                    this.priceAmendment = it
                }
            }
        }

        val saved = super.save()

        this.variant?.save()

        if (this.totalReplacedShopper > 0) {
            this.replacementShopper?.save()
        }
        else {
            this.replacementShopper?.delete()
        }

        this.replacements.forEach {
            val replacement = Replacement.findById(it.remoteId)
            replacement?.let { savedReplacement ->
                if (this.totalReplacedShopper == 0) {
                    savedReplacement.variant?.delete()
                    savedReplacement.delete()
                }
                else {
                    savedReplacement.lineItem = this
                    savedReplacement.save()

                    this.replacementShopper?.let { replacementShopper ->
                        if (replacementShopper.remoteId == it.variantId) {
                            replacementShopper.price = it.price
                            replacementShopper.displayPrice = it.displayPrice
                            replacementShopper.actualWeightAdjustmentTotal = it.variant?.actualWeightAdjustmentTotal
                            replacementShopper.displayActualWeight = it.variant?.displayActualWeight
                            replacementShopper.save()
                        }
                    }

                    val shopperReplacement = ShopperReplacement.findByReplacementId(savedReplacement.remoteId) ?:
                            ShopperReplacement(this.variantId, this.orderId, this.stockLocationId, savedReplacement.variant?.remoteId!!, savedReplacement.variantId)

                    savedReplacement.userId?.let { userId ->
                        shopperReplacement.userId = userId
                    }

                    shopperReplacement.save()
                }
            }
        }

        this.replacements = arrayListOf()

        return saved
    }

    private fun shopperReplacements(): List<ShopperReplacement> {
        return SQLite.select().from(ShopperReplacement::class.java)
                .where(ShopperReplacement_Table.variantId.eq(variantId))
                .and(ShopperReplacement_Table.lineItemOrderId.eq(orderId))
                .and(ShopperReplacement_Table.lineItemStockLocationId.eq(stockLocationId))
                .queryList()
    }

    companion object {
        @JvmStatic
        fun findByLineItem(lineItem: LineItem): LineItem? {
            return findByVariantIdOrderIdAndStockLocationId(lineItem.variantId, lineItem.orderId, lineItem.stockLocationId)
        }

        @JvmStatic
        fun findByVariantIdOrderIdAndStockLocationId(variantId: Long, orderId: Long, stockLocationId: Long): LineItem? {
            return SQLite.select().from(LineItem::class.java)
                    .where(LineItem_Table.variantId.eq(variantId))
                    .and(LineItem_Table.orderId.eq(orderId))
                    .and(LineItem_Table.stockLocationId.eq(stockLocationId))
                    .querySingle()
        }

        @JvmStatic
        fun findById(id: Long): LineItem? {
            return SQLite.select().from(LineItem::class.java).where(LineItem_Table.remoteId.eq(id)).querySingle()
        }
    }
}
