package com.happyfresh.snowflakes.hoverfly.shared.events

import com.happyfresh.snowflakes.hoverfly.models.response.VersionUpdate

class RequireUpdateEvent(data: VersionUpdate) : BaseEvent<VersionUpdate>(data)