package com.happyfresh.snowflakes.hoverfly.models

import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.shared.db.AppDatabase
import com.raizlabs.android.dbflow.annotation.*
import com.raizlabs.android.dbflow.sql.language.SQLite
import com.raizlabs.android.dbflow.structure.BaseModel
import org.parceler.Parcel
import java.io.Serializable
import java.util.*

/**
 * Created by ifranseda on 11/14/14.
 */

@Parcel
@Table(database = AppDatabase::class)
class Order : BaseModel(), Serializable {

    @SerializedName("id")
    @Column
    @Unique(unique = true)
    @Index
    @PrimaryKey
    var remoteId: Long? = null

    @Column
    var number: String? = null

    @SerializedName("item_total")
    @Column
    var itemTotal: Double? = null

    @Column
    var total: Double? = null

    @Column
    var state: String? = null

    @SerializedName("adjustment_total")
    @Column
    var adjustmentTotal: Double? = null

    @SerializedName("user_id")
    @Column
    var userId: Long? = null

    @SerializedName("user_first_name")
    @Column(name = "user_first_name")
    var userFirstName: String? = null

    @SerializedName("user_last_name")
    @Column(name = "user_last_name")
    var userLastName: String? = null

    @SerializedName("user_full_name")
    @Column(name = "user_full_name")
    var userFullName: String? = null

    @SerializedName("completed_at")
    @Column
    var completedAt: Date? = null

    @SerializedName("bill_address_id")
    @Column
    var billAddressId: Long? = null

    @SerializedName("ship_address_id")
    @Column
    var shipAddressId: Long? = null

    @SerializedName("payment_total")
    @Column
    var paymentTotal: Double? = null

    @SerializedName("shipping_method_id")
    @Column
    var shippingMethodId: Long? = null

    @SerializedName("shipment_state")
    @Column
    var shipmentState: String? = null

    @SerializedName("payment_state")
    @Column
    var paymentState: String? = null

    @Column
    var email: String? = null

    @SerializedName("special_instructions")
    @Column
    var specialInstructions: String? = null

    @SerializedName("created_at")
    @Column
    var createdAt: Date? = null

    @SerializedName("updated_at")
    @Column
    var updatedAt: Date? = null

    @Column
    var currency: String? = null

    @SerializedName("last_ip_address")
    @Column
    var lastIPAddress: String? = null

    @SerializedName("created_by_id")
    @Column(name = "created_by_id")
    var creatorUserId: Long? = null

    @SerializedName("shipment_total")
    @Column
    var shipmentTotal: Double? = null

    @SerializedName("additional_tax_total")
    @Column
    var additionalTaxTotal: Double? = null

    @SerializedName("promo_total")
    @Column
    var promoTotal: Double? = null

    @Column
    var channel: String? = null

    @SerializedName("included_tax_total")
    @Column
    var includedTaxTotal: Double? = null

    @SerializedName("item_count")
    @Column
    var itemCount: Int? = null

    @SerializedName("approver_id")
    @Column(name = "approver_id")
    var approverUserid: Long? = null

    @SerializedName("approved_at")
    @Column
    var approvedAt: Date? = null

    @SerializedName("confirmation_delivered")
    @Column
    var isConfirmationDelivered: Boolean = false

    @SerializedName("considered_risky")
    @Column
    var isConsideredRisky: Boolean = false

    @SerializedName("guest_token")
    @Column(name = "guest_token")
    var token: String? = null

    @Column
    var purchases: Int? = null

    @SerializedName("payment_method_type")
    @Column(name = "payment_method_type")
    var paymentMethod: String? = null

    @SerializedName("payment_completed")
    @Column
    var isPaymentCompleted: Boolean = false

    @SerializedName("recurring_payment")
    @Column
    var isRecurringPayment: Boolean = false

    @SerializedName("company_id")
    @Column(name = "company_id")
    var companyId: Long = 0L

    @SerializedName("company_name")
    @Column(name = "company_name")
    var companyName: String? = null

    @SerializedName("eligible_for_shopping_bag")
    @Column(name = "eligible_for_shopping_bag")
    var isEligibleForShoppingBag: Boolean = false
    
    @SerializedName("promotion_adjustments")
    var remotePromotionAdjustments: List<PromotionAdjustment>? = null

    @SerializedName("is_chat_enabled")
    @Column(name = "is_chat_enabled")
    var isChatEnabled: Boolean = false

    @SerializedName("chat")
    @Column
    @ForeignKey(saveForeignKeyModel = true)
    var chat: Chat? = null

    val isCCPromotionExistAndEligible: Boolean
        get() {
            val promotionAdjustments = promotionAdjustments()
            if (promotionAdjustments != null && promotionAdjustments.isNotEmpty()) {
                for (promotionAdjustment in promotionAdjustments) {
                    if (promotionAdjustment.isCreditCardPromotion!! && promotionAdjustment.isEligible == true) {
                        return true
                    }
                }
            }

            return false
        }

    val isCCPromotionExistAndNotEligible: Boolean
        get() {
            val promotionAdjustments = promotionAdjustments()
            if (promotionAdjustments != null && promotionAdjustments.isNotEmpty()) {
                for (promotionAdjustment in promotionAdjustments) {
                    if (promotionAdjustment.isCreditCardPromotion && !promotionAdjustment.isEligible) {
                        return true
                    }
                }
            }

            return false
        }

    val isOrderCompleteNotCanceled: Boolean
        get() {
            if (purchases == 1) {
                return true
            }

            return false
        }

    fun promotionAdjustments(): List<PromotionAdjustment>? {
        return SQLite.select().from(PromotionAdjustment::class.java).where(PromotionAdjustment_Table.orderId.eq(remoteId)).queryList()
    }

    companion object {
        fun findById(orderId: Long?): Order? {
            return SQLite.select().from(Order::class.java).where(Order_Table.remoteId.eq(orderId)).querySingle()
        }
    }
}