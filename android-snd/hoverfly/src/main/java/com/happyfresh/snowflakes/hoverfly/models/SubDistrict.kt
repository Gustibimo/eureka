package com.happyfresh.snowflakes.hoverfly.models


import com.google.gson.annotations.SerializedName
import org.parceler.Parcel

/**
 * Created by rsavianto on 2/7/15.
 */

@Parcel
class SubDistrict {

    @SerializedName("id")
    var remoteId: Long = 0

    var name: String? = null

    var zipcode: String? = null

    var district: District? = null

    override fun toString(): String {
        val builder = StringBuilder(name)
        builder.append(", ")

        if (district != null) {
            builder.append(district!!.name)
            builder.append(", ")
        }

        builder.append(zipcode)

        return builder.toString()
    }
}
