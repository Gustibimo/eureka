package com.happyfresh.snowflakes.hoverfly.models.payload

import com.google.gson.annotations.SerializedName

/**
 * Created by rsavianto on 12/24/14.
 */

class DriverFinishJobPayload(@SerializedName("receiver") var receiver: String)
