package com.happyfresh.snowflakes.hoverfly.models

import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.shared.db.AppDatabase
import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import com.raizlabs.android.dbflow.annotation.Unique
import com.raizlabs.android.dbflow.sql.language.SQLite
import com.raizlabs.android.dbflow.structure.BaseModel
import org.parceler.Parcel

/**
 * Created by ifranseda on 11/14/14.
 */

@Parcel
@Table(database = AppDatabase::class)
class Country : BaseModel() {

    @SerializedName("id")
    @Column
    @Unique(unique = true)
    @PrimaryKey
    var remoteId: Long = 0

    @SerializedName("iso_name")
    @Column
    var isoName: String? = null

    @Column
    var iso: String? = null

    var iso3: String? = null

    @Column
    var name: String? = null

    @Column
    var numcode: Int = 0

    companion object {
        fun findById(remoteId: Long?): Country? {
            return SQLite.select().from(Country::class.java).where(Country_Table.remoteId.eq(remoteId)).querySingle()
        }
    }
}
