package com.happyfresh.snowflakes.hoverfly.models.response

import com.google.gson.annotations.SerializedName

/**
 * Created by aldi on 7/27/17.
 */

class PauseResponse {

    @SerializedName("pause_at")
    val pause: String? = null
}