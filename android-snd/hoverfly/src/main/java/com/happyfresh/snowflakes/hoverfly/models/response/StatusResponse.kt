package com.happyfresh.snowflakes.hoverfly.models.response

import com.google.gson.annotations.SerializedName

class StatusResponse {

    object WorkingStatus {
        const val WORKING = "working"

        const val PAUSED = "paused"
    }

    @SerializedName("status")
    var workingStatus: String? = null

    @SerializedName("stock_location_id")
    var stockLocationId: Long = 0

    @SerializedName("stock_location_name")
    var stockLocationName: String? = null

    @SerializedName("ask_receipt_number")
    var requireReceiptNumber: Boolean = false

    @SerializedName("price_changed_will_update_price")
    var priceChangeAutoPublish: Boolean = false

    @SerializedName("price_changed_limit_percentage")
    var priceChangeLimit: Float = 0.toFloat()

    @SerializedName("last_modified")
    var lastModified: Long = 0

    @SerializedName("store_type")
    var storeType: String? = null

    @SerializedName("require_age_verification")
    var requireAgeVerification: Boolean = false

    @SerializedName("age_verification_disclaimer")
    var ageVerificationDisclaimer: String? = null

    @SerializedName("age_verification_disclaimer_en")
    var ageVerificationDisclaimerEnglish: String? = null

    @SerializedName("restricted_categories")
    var restrictedCategories: List<Long>? = null
}