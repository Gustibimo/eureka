package com.happyfresh.snowflakes.hoverfly.api

import com.happyfresh.snowflakes.hoverfly.models.payload.PlacementPayload
import com.happyfresh.snowflakes.hoverfly.models.response.PlacementResponse
import com.happyfresh.snowflakes.hoverfly.models.response.StockLocationResponse
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query
import rx.Observable

/**
 * Created by galuh on 6/18/17.
 */

interface IStockLocationService {

    @GET("stock_locations/nearby")
    fun storeNearby(
            @Query("lat")
            lat: Double?,
            @Query("lon")
            lon: Double?,
            @Query("distance")
            distance: String): Observable<StockLocationResponse>

    @POST("placements")
    fun createPlacement(
            @Query("locale")
            isoName: String,
            @Body
            payload: PlacementPayload): Observable<PlacementResponse>
}
