package com.happyfresh.snowflakes.hoverfly.models.payload

import android.support.annotation.Nullable
import com.google.gson.annotations.SerializedName

/**
 * Created by ifranseda on 11/27/14.
 */

class ItemShopPayload : SerializablePayload() {
    object SourceType {
        val FROM_SEARCH = 0
        val FROM_SUGGESTION = 1
    }

    @SerializedName("variant_id")
    var variantId: Long? = null

    var quantity: Int = 0

    @SerializedName("actual_weight")
    var actualWeight: Double? = null

    @SerializedName("shopper_notes_fulfilled")
    var shopperNotesFulfilled: Boolean = false

    @Nullable
    @SerializedName("source_type")
    var sourceType: Int? = null
}
