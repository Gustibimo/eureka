package com.happyfresh.snowflakes.hoverfly.stores

import android.content.Context
import android.content.SharedPreferences

/**
 * Created by ifranseda on 30/12/2016.
 */

abstract class BaseStore internal constructor(val context: Context) {

    internal var sharedPrefs: SharedPreferences

    private val preferenceName: String
        get() = this.context.packageName + ".preferences"

    init {
        this.sharedPrefs = sharedPrefs()
    }

    fun sharedPrefs(): SharedPreferences {
        return this.context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE)
    }

    protected fun sharedPrefEditor(): SharedPreferences.Editor {
        return sharedPrefs().edit()
    }

    protected fun editorCommit(listener: (SharedPreferences.Editor) -> Unit) {
        val editor = sharedPrefEditor()
        listener(editor)
        editor.commit()
    }
}
