package com.happyfresh.snowflakes.hoverfly.services

import com.happyfresh.snowflakes.hoverfly.Sprinkles
import com.happyfresh.snowflakes.hoverfly.api.IBatchService
import com.happyfresh.snowflakes.hoverfly.models.Batch
import com.happyfresh.snowflakes.hoverfly.models.LineItem
import com.happyfresh.snowflakes.hoverfly.models.response.BatchShoppingListResponse
import com.happyfresh.snowflakes.hoverfly.models.response.BatchesResponse
import com.happyfresh.snowflakes.hoverfly.models.response.PendingHandoverListResponse
import com.happyfresh.snowflakes.hoverfly.stores.BatchStore
import com.happyfresh.snowflakes.hoverfly.utils.ChatUtils
import retrofit.client.Response
import rx.Observable

/**
 * Created by galuh on 7/5/17.
 */

class BatchService : BaseService<IBatchService>() {

    private val store by lazy { Sprinkles.stores(BatchStore::class.java) }

    @Deprecated(message = "Active batch could be multiple batch")
    var activeBatch: Batch? = null
        get() {
            store?.currentBatchID()?.let {
                return Batch.findById(it)
            }
            return null
        }
        set(value) {
            value?.let {
                store?.saveCurrentBatch(it)
            }

            field = value
        }

    override fun serviceClass(): Class<IBatchService> {
        return IBatchService::class.java
    }

    fun activeList(showPromotions: Boolean): Observable<BatchesResponse> {
        return observe(api().activeList(showPromotions).map {
            it.batches.forEach {
                it.isActive = true
                it.save()
            }
            it
        })
    }

    fun available(): Observable<BatchesResponse> {
        return observe(api().available())
    }

    fun start(batchId: Long): Observable<Batch> {
        return observe(api().start(batchId)).map {
            it.isActive = true
            it.save()

            it.deleteAllItems()

            it
        }
    }

    fun cancel(batchId: Long): Observable<Batch> {
        return observe(api().cancel(batchId)).map {
            it.isActive = false
            it.save()
            it
        }
    }

    fun shoppingLineItems(batch: Batch): Observable<List<LineItem>> {
        val output = ArrayList<LineItem>()

        return shoppingList(batch).map { batchResponse ->
            try {
                batchResponse.data?.forEach { order ->
                    order.items?.let { output.addAll(it) }
                }
            }
            catch (e: Exception) {
            }

            output
        }
    }

    private fun shoppingList(batch: Batch): Observable<BatchShoppingListResponse> {
        batch.remoteId?.let { batchId ->
            return subscribe(api().shoppingList(batchId)).map { response ->
                response.batch = batch
                response.data?.map { shoppingList ->
                    shoppingList.shipment?.save()
                    shoppingList.items?.forEach { it.save() }
                }
                response
            }
        }

        return Observable.just(BatchShoppingListResponse())
    }

    fun finalize(batchId: Long, shipmentId: Long) : Observable<Batch> {
        return observe(api().finalize(batchId, shipmentId)).map {
            it.save()
            ChatUtils.freezeChatRoom(shipmentId)
            it
        }
    }

    fun pendingHandoverList() : Observable<PendingHandoverListResponse> {
        return observe(api().pendingHandoverList())
    }

    fun flagOpenPayment(batchId: Long) : Observable<Response> {
        return observe(api().flagOpenPayment(batchId))
    }
}
