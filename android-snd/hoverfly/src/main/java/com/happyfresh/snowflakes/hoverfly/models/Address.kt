package com.happyfresh.snowflakes.hoverfly.models

import android.text.TextUtils
import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.shared.db.AppDatabase
import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.ForeignKey
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import com.raizlabs.android.dbflow.sql.language.SQLite
import com.raizlabs.android.dbflow.structure.BaseModel
import org.parceler.Parcel
import java.io.Serializable
import java.util.*

/**
 * Created by ifranseda on 11/14/14.
 */

@Parcel
@Table(database = AppDatabase::class)
class Address : BaseModel(), Serializable {

    @SerializedName("id")
    @Column
    @PrimaryKey
    var remoteId: Long = 0

    @SerializedName("firstname")
    @Column
    var firstName: String? = null

    @SerializedName("lastname")
    @Column
    var lastName: String? = null

    @Column
    var address1: String? = null

    @Column
    var address2: String? = null

    @Column
    var city: String? = null

    @Column
    var zipcode: String? = null

    @SerializedName("state_name")
    @Column
    var stateName: String? = null

    @Column
    var phone: String? = null

    @SerializedName("alternative_phone")
    @Column
    var alternativePhone: String? = null

    @Column
    var company: String? = null

    @SerializedName("state_id")
    @Column
    var stateId: Long? = null

    @SerializedName("country_id")
    @Column
    var countryId: Long? = null

    @SerializedName("created_at")
    @Column
    var createdAt: Date? = null

    @SerializedName("update_at")
    @Column
    var updatedAt: Date? = null

    @SerializedName("address_type")
    @Column
    var addressType: String? = null

    @Column
    var isPrimary: Boolean = false

    @Column
    @ForeignKey(saveForeignKeyModel = false)
    var user: User? = null

    @SerializedName("delivery_instruction")
    @Column
    var deliveryInstruction: String? = null
        get() {
            if (TextUtils.isEmpty(field)) {
                return "-"
            }

            return field
        }

    @SerializedName("address_detail")
    @Column
    var addressDetail: String? = null

    @SerializedName("address_number")
    @Column
    var addressNumber: String? = null

    @Column(name = "latitude")
    var lat: Double? = null

    @Column(name = "longitude")
    var lon: Double? = null

    val fullName: String
        get() {
            val name = StringBuffer()
            if (!TextUtils.isEmpty(firstName)) {
                name.append(firstName)
            }

            if (!TextUtils.isEmpty(lastName)) {
                name.append(" ")
                name.append(lastName)
            }

            return name.toString()
        }

    val phoneNumber: String?
        get() {
            return if (!TextUtils.isEmpty(this.phone)) {
                this.phone
            }
            else {
                if (!TextUtils.isEmpty(this.alternativePhone)) {
                    this.alternativePhone
                }
                else {
                    null
                }
            }
        }

    fun buildCityInfo(): String {
        val builder = StringBuilder(city)

        zipcode?.let {
            builder.append(", ")
            builder.append(zipcode)
        }

        return builder.toString()
    }

    companion object {

        @JvmStatic
        fun findById(addressId: Long?): Address? {
            return SQLite.select().from(Address::class.java).where(Address_Table.remoteId.eq(addressId)).querySingle()
        }
    }
}
