package com.happyfresh.snowflakes.hoverfly.models.response

import com.google.gson.annotations.SerializedName

/**
 * Created by anton on 12/15/14.
 */

class ChangePriceResponse {

    var id: Long? = null

    @SerializedName("variant_id")
    var variantId: Long? = null

    @SerializedName("order_id")
    var orderId: Long? = null

    @SerializedName("stock_location_id")
    var stockLocationId: Long? = null

    @SerializedName("replacement_id")
    var replacementId: Long? = null

    @SerializedName("new_price")
    var newPrice: String? = null

    @SerializedName("user_id")
    var userId: Long? = null

    val user: String?
        get() {
            return userId?.toString()
        }
}
