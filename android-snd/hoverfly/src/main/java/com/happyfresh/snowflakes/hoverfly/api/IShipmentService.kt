package com.happyfresh.snowflakes.hoverfly.api

import com.happyfresh.snowflakes.hoverfly.models.LineItem
import com.happyfresh.snowflakes.hoverfly.models.payload.*
import com.happyfresh.snowflakes.hoverfly.models.response.ChangePriceResponse
import com.happyfresh.snowflakes.hoverfly.models.response.ChatResponse
import com.happyfresh.snowflakes.hoverfly.models.response.GrabExpressCancelReasonResponse
import com.happyfresh.snowflakes.hoverfly.models.response.LineItemResponse
import okhttp3.MultipartBody
import retrofit.client.Response
import retrofit2.http.*
import rx.Observable

/**
 * Created by galuh on 11/29/17.
 */
interface IShipmentService {

    @PUT("shipments/{id}/items/shop")
    fun shop(@Path("id") shipmentId: Long,
             @Body payload: ListItemShopPayload): Observable<LineItemResponse>

    @PUT("shipments/{id}/items/flag")
    fun flag(@Path("id") shipmentId: Long,
             @Body body: MultipartBody): Observable<LineItemResponse>

    @PUT("shipments/{id}/items/unflag")
    fun removeFlag(@Path("id") shipmentId: Long,
                   @Body payload: ListFlagItemPayload): Observable<LineItemResponse>

    @POST("shipments/{id}/items/flag_mismatch")
    fun mismatch(@Path("id") shipmentId: Long,
                 @Body body: MultipartBody): Observable<LineItem>

    @PUT("shipments/{id}/items/unflag_mismatch")
    fun removeMismatch(@Path("id") shipmentId: Long,
                       @Body payload: MismatchPayload): Observable<LineItem>

    @PUT("shipments/{id}/items/shop")
    fun replace(@Path("id") shipmentId: Long,
                @Body payload: ListItemReplacementPayload): Observable<LineItemResponse>

    @POST("orders/{order_id}/stock_locations/{stock_location_id}/variants/{variant_id}/price_amendments")
    fun changePrice(@Path("order_id") orderId: Long,
                    @Path("stock_location_id") stockLocationId: Long,
                    @Path("variant_id") variantId: Long,
                    @Body body: MultipartBody): Observable<ChangePriceResponse>

    @GET("grab/cancel_reasons")
    fun grabExpressCancelReason(): Observable<GrabExpressCancelReasonResponse>

    @POST("orders/{order_number}/grab/cancel")
    fun cancelGrabExpressOrder(@Path("order_number") orderNumber: String,
                               @Body payload: GrabExpressCancelReasonPayload): Observable<Response>

    @POST("orders/{order_number}/grab")
    fun orderGrabExpress(@Path("order_number") orderNumber: String): Observable<Response>

    @POST("orders/{order_number}/grab/to_hf")
    fun changeGrabExpressOrderToHF(@Path("order_number") orderNumber: String): Observable<Response>

    @POST("orders/{order_id}/chat/action/start")
    fun startChat(@Path("order_id") orderId: Long,
                  @Body payload: StartChatPayload): Observable<ChatResponse>

    @POST("orders/{order_id}/chat/action/finish")
    fun endChat(@Path("order_id") orderId: Long): Observable<Void>
}