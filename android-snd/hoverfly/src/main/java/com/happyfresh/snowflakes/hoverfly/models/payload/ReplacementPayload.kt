package com.happyfresh.snowflakes.hoverfly.models.payload

import com.google.gson.annotations.SerializedName

/**
 * Created by anton on 12/10/14.
 */

class ReplacementPayload : SerializablePayload() {

    @SerializedName("variant_id")
    var variantId: Long? = 0L

    var quantity: Int = 0

    var replacements: MutableList<ItemShopPayload> = mutableListOf()
}
