package com.happyfresh.snowflakes.hoverfly.utils

import android.content.Context
import android.graphics.Color
import android.util.TypedValue

/**
 * Created by ifranseda on 9/9/15.
 */
class ResourceUtils {

    companion object {

        @JvmStatic
        fun getResId(resName: String, c: Class<*>): Int {
            return try {
                val idField = c.getDeclaredField(resName)
                idField.getInt(idField)
            }
            catch (e: Exception) {
                -1
            }

        }

        @JvmStatic
        fun getColorResId(resName: String): Int {
            return getResId(resName, Color::class.java)
        }

        @JvmStatic
        fun getAttributedColor(context: Context, attribute: Int): Int {
            val typedValue = TypedValue()

            val a = context.obtainStyledAttributes(typedValue.data, intArrayOf(attribute))
            val color = a.getColor(0, 0)
            a.recycle()

            return color
        }

        @JvmStatic
        fun getAttributedString(context: Context, attribute: Int): String {
            val typedValue = TypedValue()

            val a = context.obtainStyledAttributes(typedValue.data, intArrayOf(attribute))
            val str = a.getString(0)
            a.recycle()

            return str
        }
    }
}