package com.happyfresh.snowflakes.hoverfly.models.response

import com.google.gson.annotations.SerializedName

/**
 * Created by ifranseda on 12/23/14.
 */

class ExceptionResponse : RuntimeException() {

    @SerializedName("type")
    var type: String? = null

    @SerializedName("exception")
    var exception: String? = null

    @SerializedName("message")
    private var thisMessage: String? = null

    @SerializedName("error")
    private var error: String? = null

    override val message: String?
        get() = type

    override fun getLocalizedMessage(): String {
        return if (type != null || thisMessage != null) {
            type + "\n" + thisMessage
        } else if (exception != null) {
            "" + exception
        } else {
            "" + error
        } 
    }
}