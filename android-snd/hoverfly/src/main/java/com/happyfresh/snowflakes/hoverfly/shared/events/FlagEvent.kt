package com.happyfresh.snowflakes.hoverfly.shared.events

import com.happyfresh.snowflakes.hoverfly.models.LineItem

/**
 * Created by ifranseda on 19/12/2017.
 */

class StockOutEventFailed(data: Throwable) : BaseEvent<Throwable>(data)

class StockOutEvent(data: List<LineItem>) : BaseEvent<List<LineItem>>(data)

class MismatchEventFailed(data: Throwable) : BaseEvent<Throwable>(data)

class MismatchEvent(data: List<LineItem>) : BaseEvent<List<LineItem>>(data)

class ShopEventFailed(data: Throwable) : BaseEvent<Throwable>(data)

class ShopEvent(data: List<LineItem>) : BaseEvent<List<LineItem>>(data)

class ReplaceEventFailed(data: Throwable) : BaseEvent<Throwable>(data)

class ReplaceEvent(data: List<LineItem>) : BaseEvent<List<LineItem>>(data)

class PriceChangedEventFailed(data: Throwable) : BaseEvent<Throwable>(data)

class PriceChangedEvent(data: List<LineItem>) : BaseEvent<List<LineItem>>(data)
