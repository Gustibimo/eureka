package com.happyfresh.snowflakes.hoverfly.models.payload

import com.google.gson.annotations.SerializedName

/**
 * Created by numannaufal on 9/20/16.
 */

class SampleCampaignPayload(
        @SerializedName("campaign_id")
        var campaignId: Long,

        @Transient
        var orderNumber: String,

        @SerializedName("items")
        var sampleItemPayloads: List<SampleItemPayload>,

        @SerializedName("rule_id")
        var ruleId: Long)