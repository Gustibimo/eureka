package com.happyfresh.snowflakes.hoverfly.models

import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.shared.db.AppDatabase
import com.raizlabs.android.dbflow.annotation.*
import com.raizlabs.android.dbflow.sql.language.SQLite
import com.raizlabs.android.dbflow.structure.BaseModel
import org.parceler.Parcel
import java.io.Serializable
import java.util.*

/**
 * Created by ifranseda on 11/14/14.
 */

@Parcel
@Table(database = AppDatabase::class)
class Slot : BaseModel(), Serializable {

    @SerializedName("id")
    @Column
    @Index
    @PrimaryKey
    var remoteId: Long? = null

    @SerializedName("stock_location_id")
    @Column
    var stockLocationId: Long = 0

    @SerializedName("start_time")
    @Column
    var startTime: Date? = null

    @SerializedName("end_time")
    @Column
    var endTime: Date? = null

    @SerializedName("stock_location")
    @Column
    @ForeignKey(saveForeignKeyModel = true)
    var stockLocation: StockLocation? = null

    override fun save(): Boolean {
        val saved = super.save()

        stockLocationId = stockLocation?.remoteId!!

        return saved
    }

    companion object {
        fun findById(remoteId: Long?): Slot? {
            return SQLite.select().from(Slot::class.java).where(Slot_Table.remoteId.eq(remoteId)).querySingle()
        }

        fun getStockLocation(slot: Slot?): StockLocation? {
            slot?.stockLocation?.let {
                return it
            }

            return StockLocation.findById(slot?.stockLocationId)
        }
    }
}
