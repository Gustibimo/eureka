package com.happyfresh.snowflakes.hoverfly.shared.subscribers

import com.happyfresh.snowflakes.hoverfly.RestClient
import com.happyfresh.snowflakes.hoverfly.exceptions.ServerException
import com.happyfresh.snowflakes.hoverfly.models.response.ServerExceptionResponse
import retrofit2.Response
import rx.Subscriber
import java.io.IOException
import java.net.UnknownHostException

/**
 * Created by ifranseda on 30/12/2016.
 */

abstract class ServiceSubscriber<T> : Subscriber<T>() {

    override fun onCompleted() {}

    override fun onNext(o: T) {
        if (o is Response<*>) {
            val response = o as Response<*>
            if (response.isSuccessful) {
                onSuccess(o)
                onCompleted()
            }
            else {
                try {
                    val serverError = RestClient.getInstance().convert(response.errorBody(), ServerExceptionResponse::class.java)

                    onFailure(ServerException(serverError.message!!))
                }
                catch (ue: UnknownHostException) {
                    onFailure(ue)
                    onDisconnected()
                }
                catch (e: IOException) {
                    onFailure(ServerException(response.code(), response.message()))
                }
                finally {
                    onCompleted()
                }
            }
        }
        else {
            onSuccess(o)
            onCompleted()
        }
    }

    override fun onError(e: Throwable) {
        e.printStackTrace()

        onFailure(e)

        if (e is UnknownHostException) {
            onDisconnected()
        }

        onCompleted()
    }

    abstract fun onFailure(e: Throwable)

    fun onDisconnected() {}

    abstract fun onSuccess(data: T)
}
