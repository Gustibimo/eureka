package com.happyfresh.snowflakes.hoverfly.models

import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.shared.db.AppDatabase
import com.raizlabs.android.dbflow.annotation.*
import com.raizlabs.android.dbflow.structure.BaseModel
import org.parceler.Parcel

/**
 * Created by ifranseda on 11/20/14.
 */

@Parcel
@Table(database = AppDatabase::class)
class StockItem : BaseModel() {

    @SerializedName("id")
    @Column
    @Unique(unique = true)
    @PrimaryKey
    var remoteId: Long? = null

    @Column
    @Index
    @ForeignKey(saveForeignKeyModel = false)
    var variant: Variant? = null

    @SerializedName("count_on_hand")
    @Column
    var countOnHand: Int? = null

    @SerializedName("backorderable")
    @Column
    var isBackorderable: Boolean = false

    @SerializedName("available")
    @Column
    var isAvailable: Boolean = false

    @SerializedName("stock_location_id")
    @Column
    var stockLocationId: Long? = null

    @SerializedName("stock_location_name")
    @Column
    var stockLocationName: String? = null
}
