package com.happyfresh.snowflakes.hoverfly.models.payload

/**
 * Created by kharda on 9/5/16.
 */
class ProductSamplePayload(var sku: String, var quantity: Int) : SerializablePayload()
