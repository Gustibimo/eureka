package com.happyfresh.snowflakes.hoverfly.models.payload

import com.google.gson.annotations.SerializedName

/**
 * Created by kharda on 23/8/18
 */
class StartChatPayload {

    @SerializedName("channel_url")
    var channelUrl: String? = null
}