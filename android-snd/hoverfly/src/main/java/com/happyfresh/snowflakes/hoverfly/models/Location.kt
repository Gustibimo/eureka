package com.happyfresh.snowflakes.hoverfly.models

import org.parceler.Parcel

/**
 * Created by rsavianto on 6/6/15.
 */

@Parcel
class Location {

    var lat: Float = 0.toFloat()

    var lon: Float = 0.toFloat()
}
