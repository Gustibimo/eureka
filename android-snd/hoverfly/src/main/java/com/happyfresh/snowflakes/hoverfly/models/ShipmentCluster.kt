package com.happyfresh.snowflakes.hoverfly.models

import org.parceler.Parcel
import java.util.*

/**
 * Created by rsavianto on 7/30/15.
 */

@Parcel
class ShipmentCluster {

    var stockLocationId: Long = 0

    var stockLocationName: String? = null

    var numberOfOrders = 0

    var lastDeliveryTime: Date? = null

    var lastDeliveryTimeString: String? = null
}
