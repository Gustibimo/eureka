package com.happyfresh.snowflakes.hoverfly.api

import com.happyfresh.snowflakes.hoverfly.models.Batch
import com.happyfresh.snowflakes.hoverfly.models.response.BatchShoppingListResponse
import com.happyfresh.snowflakes.hoverfly.models.response.BatchesResponse
import com.happyfresh.snowflakes.hoverfly.models.response.PendingHandoverListResponse
import retrofit.client.Response
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query
import rx.Observable

/**
 * Created by galuh on 7/5/17.
 */

interface IBatchService {

    @GET("batches/{id}")
    fun fetch(@Path("id") batchId: Long): Observable<Batch>

    @GET("v2/batches/active")
    fun activeList(@Query("show_promotions") showPromotions: Boolean): Observable<BatchesResponse>

    @GET("batches/available")
    fun available(): Observable<BatchesResponse>

    @POST("batches/{id}/start")
    fun start(@Path("id") batchId: Long): Observable<Batch>

    @POST("batches/{id}/cancel")
    fun cancel(@Path("id") batchId: Long): Observable<Batch>

    @GET("batches/{id}/items")
    fun shoppingList(@Path("id") batchId: Long): Observable<BatchShoppingListResponse>

    @POST("batches/{id}/shipments/{shipment_id}/finalize")
    fun finalize(@Path("id") batchId: Long?,
                 @Path("shipment_id") shipmentId: Long?): Observable<Batch>

    @GET("batches/pending_handovers")
    fun pendingHandoverList(): Observable<PendingHandoverListResponse>

    @POST("batches/{id}/grab")
    fun flagOpenPayment(@Path("id") batchId: Long?): Observable<Response>
}
