package com.happyfresh.snowflakes.hoverfly.stores

import android.content.Context
import com.happyfresh.snowflakes.hoverfly.models.Batch

/**
 * Created by ifranseda on 27/11/2017.
 */
class BatchStore internal constructor(context: Context) : BaseStore(context) {

    companion object {
        const val SINGLE_ACTIVE_BATCH_ID = "PREFS.SINGLE_ACTIVE_BATCH_ID"
    }

    @Deprecated(message = "Active batch contains multiple items", replaceWith = ReplaceWith(""))
    fun saveCurrentBatch(batch: Batch) {
        batch.remoteId?.let {
            sharedPrefEditor().putLong(SINGLE_ACTIVE_BATCH_ID, it).commit()
        }
    }

    @Deprecated(message = "Active batch contains multiple items", replaceWith = ReplaceWith(""))
    fun currentBatchID(): Long {
        return sharedPrefs().getLong(SINGLE_ACTIVE_BATCH_ID, 0L)
    }
}