package com.happyfresh.snowflakes.hoverfly.models.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by ifranseda on 29/12/2017.
 */
class PriceChangeResponse: Serializable {
    @SerializedName("id")
    var remoteId: Long? = null

    @SerializedName("variant_id")
    var variantId: Long? = null

    @SerializedName("order_id")
    var orderId: Long? = null

    @SerializedName("stock_location_id")
    var stockLocationId: Long? = null

    @SerializedName("replacement_id")
    var replacementId: Long? = null

    @SerializedName("user_id")
    var userId: String? = null

    @SerializedName("new_price")
    var newPrice: String? = null
}