package com.happyfresh.snowflakes.hoverfly.utils

import java.text.NumberFormat

/**
 * Created by ifranseda on 12/30/17.
 */
object FormatterUtils {
    @JvmStatic
    fun getNumberFormatter(): NumberFormat {
        return getNumberFormatter(null)
    }

    @JvmStatic
    fun getNumberFormatter(currencyCode: String?): NumberFormat {
        return CurrencyUtils.getInstance().numberFormatter(currencyCode)
    }
}
