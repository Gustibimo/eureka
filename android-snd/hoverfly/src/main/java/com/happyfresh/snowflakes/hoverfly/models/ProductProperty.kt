package com.happyfresh.snowflakes.hoverfly.models

import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.shared.db.AppDatabase
import com.raizlabs.android.dbflow.annotation.*
import com.raizlabs.android.dbflow.structure.BaseModel
import org.parceler.Parcel

/**
 * Created by ifranseda on 1/9/15.
 */

@Parcel
@Table(database = AppDatabase::class)
class ProductProperty : BaseModel() {

    @SerializedName("product_id")
    @Column
    @PrimaryKey
    var productId: Long? = null

    @SerializedName("property_id")
    @Column
    @PrimaryKey
    var propertyId: Long? = null

    @SerializedName("value")
    @Column
    var value: String? = null

    @SerializedName("property_name")
    @Column
    var propertyName: String? = null

    @ForeignKey(stubbedRelationship = true)
    var variant: Variant? = null

    enum class PropertyName {
        UNKNOWN,
        PACKAGE,
        SIZE,
        UNIT,
        AVG_WEIGHT,
        SUPERMARKET_UNIT,
        NATURAL_UNIT
    }

    val type: PropertyName?
        get() {
            if (propertyName == null) {
                return null
            }

            return when (propertyName) {
                "package"          -> PropertyName.PACKAGE
                "size"             -> PropertyName.SIZE
                "unit"             -> PropertyName.UNIT
                "avg_weight"       -> PropertyName.AVG_WEIGHT
                "supermarket_unit" -> PropertyName.SUPERMARKET_UNIT
                "natural_unit"     -> PropertyName.NATURAL_UNIT
                else               -> PropertyName.UNKNOWN
            }
        }

    val isPackage: Boolean
        get() = "package".equals(propertyName!!, ignoreCase = true)

    val isUnit: Boolean
        get() = "unit".equals(propertyName!!, ignoreCase = true)

    val isSize: Boolean
        get() = "size".equals(propertyName!!, ignoreCase = true)
}
