package com.happyfresh.snowflakes.hoverfly.models.response

import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.models.ProductSampleItem

/**
 * Created by kharda on 9/9/16.
 */

class ProductSampleListResponse {

    @SerializedName("order_number")
    var orderNumber: String? = null

    var items: List<ProductSampleItem>? = null
}
