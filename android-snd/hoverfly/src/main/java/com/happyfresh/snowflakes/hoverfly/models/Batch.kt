package com.happyfresh.snowflakes.hoverfly.models

import android.support.v4.content.ContextCompat
import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.R
import com.happyfresh.snowflakes.hoverfly.Sprinkles
import com.happyfresh.snowflakes.hoverfly.shared.db.AppDatabase
import com.happyfresh.snowflakes.hoverfly.stores.BatchStore
import com.raizlabs.android.dbflow.annotation.*
import com.raizlabs.android.dbflow.sql.language.OperatorGroup
import com.raizlabs.android.dbflow.sql.language.SQLite
import com.raizlabs.android.dbflow.structure.BaseModel
import org.parceler.Parcel
import java.io.Serializable
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashSet

/**
 * Created by ifranseda on 8/18/15.
 */

@Parcel
@Table(database = AppDatabase::class)
class Batch : BaseModel(), IServiceModel, Serializable {
    @Column
    @SerializedName("id")
    @PrimaryKey
    @Index
    var remoteId: Long? = null

    @Column
    @ForeignKey(saveForeignKeyModel = true)
    var slot: Slot? = null

    @Column
    @ForeignKey(saveForeignKeyModel = true)
    var state: BatchState? = null

    @Column
    var colors: String? = null

    @SerializedName("shipments")
    var remoteShipments: List<Shipment> = arrayListOf()

    @OneToMany(methods = arrayOf(OneToMany.Method.ALL), variableName = "remoteShipments")
    fun shipments(): List<Shipment> {
        if (remoteShipments.isEmpty()) {
            val operator = OperatorGroup.clause().and(Shipment_Table.batch_remoteId.eq(this.remoteId)).and(Shipment_Table.isCancelled.notEq(true))
            remoteShipments = SQLite.select().from(Shipment::class.java).where(operator).queryList()
        }

        return remoteShipments
    }

    @SerializedName("display_ship_distance")
    var displayShipDistance: String? = null

    @Column
    @SerializedName("start_time")
    var startTime: Date? = null

    @Column
    @SerializedName("end_time")
    var endTime: Date? = null

    @Column
    var isActive: Boolean = false

    val isCompleted: Boolean
        get() {
            if (BatchState.State.COMPLETED == this.state()) {
                return true
            }
            else {
                if (BatchState.State.DELIVERING == this.state() && state!!.progress === state!!.total) {
                    return true
                }
            }

            return false
        }

    fun shipments(stockLocationId: Long?): List<Shipment> {
        if (stockLocationId == null) {
            return shipments()
        }

        val shipments = ArrayList<Shipment>()
        shipments().map { shipment ->
            if (stockLocationId == shipment.slot?.stockLocation?.remoteId) {
                shipments.add(shipment)
            }
        }

        return shipments
    }

    fun unFinishShipments(): List<Shipment> {
        val shipments = ArrayList<Shipment>()
        for (shipment in shipments()) {
            shipment.shoppingJob?.let {
                if (!it.isFinished) {
                    shipments.add(shipment)
                }
            }
        }

        return shipments
    }

    fun stockLocation(): StockLocation {
        val stockLocations = HashSet<StockLocation>()

        for (shipment in shipments()) {
            shipment.slot?.stockLocation?.let {
                stockLocations.add(it)
            }
        }

        if (stockLocations.size > 1) {
            throw RuntimeException("Multiple stock location in batch!")
        }

        return stockLocations.first()
    }

    fun allStockLocations(): List<StockLocation> {
        val stockLocationIds = ArrayList<Long>()
        val stockLocations = ArrayList<StockLocation>()

        for (shipment in shipments()) {
            Slot.getStockLocation(shipment.slot)?.let { store ->
                stockLocations.add(store)
                stockLocationIds.add(store.remoteId)
            }
        }

        return stockLocations
    }

    fun allUniqueStockLocations(): List<StockLocation> {
        return allStockLocations().distinct()
    }

    private fun clearColor() {
        colors = null
    }

    private fun appendColor(color: Int) {
        colors = if (colors.isNullOrEmpty()) {
            color.toString()
        }
        else {
            val builder = StringBuilder()
            builder.append(colors)
            builder.append(",")
            builder.append(color.toString())
            builder.toString()
        }
    }

    private fun hasColor(color: Int): Boolean {
        return colors?.contains(color.toString()) == true
    }

    val nextColor: Int
        get() {
            val ctx = Sprinkles.sContext
            val shipmentColors = ctx.resources.getIntArray(R.array.shipment_colors)

            return shipmentColors.firstOrNull { !hasColor(it) } ?: R.color.shopping_order_unknown
        }

    fun state(): BatchState.State {
        return if (state != null) {
            state?.state!!
        }
        else {
            BatchState.State.STARTED
        }
    }

    fun shoppingFinished(): Boolean {
        var finished = true
        for (shipment in shipments()) {
            finished = finished and (shipment.shoppingJob?.isFinished == true)
        }

        return finished
    }

    private fun hasFinalizeUnavailableItems(): Boolean {
        var finished = true
        shipments().asSequence().filterNot { it.isCancelled }.forEach {
            finished = if (it.outOfStockItems.isNotEmpty()) {
                finished and (it.shoppingJob != null && (it.shoppingJob!!.isFinalizing || it.shoppingJob!!.isFinished))
            }
            else {
                finished and true
            }
        }

        return finished
    }

    fun hasFinalizeUnavailableItems(stockLocationId: Long?): Boolean {
        if (stockLocationId == null) {
            return hasFinalizeUnavailableItems()
        }

        var finished = true
        shipments().asSequence().filter { !it.isCancelled && stockLocationId == it.slot?.stockLocation?.remoteId }.forEach {
            it.load()
            finished = if (it.outOfStockItems.isNotEmpty()) {
                finished and (it.shoppingJob != null && (it.shoppingJob!!.isFinalizing || it.shoppingJob!!.isFinished))
            }
            else {
                finished and true
            }
        }

        return finished
    }

    @Synchronized
    fun hasFinalizeAny(): Boolean {
        var finished = false
        for (shipment in shipments()) {
            finished = finished or (shipment.shoppingJob != null && (shipment.shoppingJob!!.isFinalizing || shipment.shoppingJob!!.isFinished))
        }

        return finished
    }

    fun hasStartedShopping(): Boolean {
        return shipments().any { it.shoppingJob?.hasStartedShopping() == true }
    }

    fun hasCompleteShoppingList(): Boolean {
        return stockLocation().let {
            getPendingCounter(it.remoteId) <= 0
        }
    }

    fun hasCompleteShoppingList(stockLocation: Long): Boolean {
        return getPendingCounter(stockLocation) <= 0
    }

    fun pendingCounter(): Int {
        stockLocation().let {
            return getPendingCounter(it.remoteId)
        }
    }

    private fun getPendingCounter(stockLocationId: Long): Int {
        var pendingCounter = 0
        shipments().filter { it.slot?.stockLocation?.remoteId == stockLocationId }.forEach {
            it.load()
            when {
                it.shoppingJob == null                  -> pendingCounter += 100
                it.shoppingJob?.stillShopping() == true -> pendingCounter += it.pendingCount
                else                                    -> pendingCounter -= 1
            }
        }

        return pendingCounter
    }

    fun hasPayAllShopping(): Boolean {
        var finished = true
        for (shipment in shipments()) {
            finished = finished and (shipment.shoppingJob != null && shipment.shoppingJob!!.isFinished)
        }

        return finished
    }

    fun hasPayAllShoppingInStore(stockLocation: Long?): Boolean {
        if (stockLocation == null) {
            return false
        }

        var finished = true
        shipments().asSequence().filter { stockLocation == it.slot?.stockLocation?.remoteId }.forEach {
            finished = finished and (it.shoppingJob != null && it.shoppingJob!!.isFinished)
        }

        return finished
    }

    fun activeShipment(): Shipment? {
        for (shipment in shipments()) {
            val job = shipment.deliveryJob
            if (job != null && job.isActive) {
                return shipment
            }
        }

        return null
    }

    private fun allItems(): List<LineItem> {
        val shoppingItems = ArrayList<LineItem>()

        shipments().asSequence().filterNot { it.isCancelled }.map { it.allItems }.forEach {
            shoppingItems.addAll(it)
        }

        return shoppingItems
    }

    fun allItems(stockLocationId: Long?): List<LineItem> {
        if (stockLocationId == null) {
            return allItems()
        }

        val shoppingItems = ArrayList<LineItem>()
        shipments().asSequence().filter { !it.isCancelled && stockLocationId == it.slot?.stockLocation?.remoteId }.map { it.allItems }.forEach {
            shoppingItems.addAll(it)
        }

        return shoppingItems
    }

    fun deleteAllItems() {
        shipments().forEach {
            it.deleteAllItems()
        }
    }

    fun hasCorporateOrder(): Boolean {
        remoteShipments.forEach {
            if (it.order?.companyId != 0L) {
                return true
            }
        }

        return false
    }

    fun unreadChatMessageCount(): Int {
        var totalUnreadMessageCount = 0

        for (shipment in shipments()) {
            shipment.order?.chat?.unreadChatCount?.let { unreadChatMessage ->
                totalUnreadMessageCount += unreadChatMessage
            }
        }

        return totalUnreadMessageCount
    }

    fun isChatEnabled() : Boolean {
        var enabled = false
        for (shipment in shipments()) {
            shipment.order?.let {
                enabled = enabled or it.isChatEnabled
            }
        }

        return enabled
    }

    override fun save(): Boolean {
        clearColor()

        val saved = super.save()

        remoteShipments.forEach { shipment ->
            shipment.batch = this

            shipment.colorResource = nextColor
            shipment.order?.companyId?.let { companyId ->
                if (companyId > 0) {
                    shipment.colorResource = ContextCompat.getColor(Sprinkles.sContext, R.color.happycorporate_flag)
                }
            }

            shipment.save()

            appendColor(shipment.colorResource)
        }

        remoteShipments = arrayListOf()

        return saved
    }

    private val isSpecial: Boolean
        get() = this.slot?.stockLocation?.isSpecialtyStore == true

    val isRangerHasToShop: Boolean
        get() {
            val emails = shoppersEmail

            return isSpecial && !isHybridStore || isSpecial && isHybridStore && emails.isEmpty()
        }

    private val isHybridStore: Boolean
        get() = Slot.getStockLocation(slot!!)!!.isHybridStore

    private val shoppersEmail: Set<String>
        get() {
            val shoppersEmail = HashSet<String>()
            for (shipment in remoteShipments) {
                val shoppingJob = shipment.shoppingJob

                if (shoppingJob?.user != null) {
                    shoppersEmail.add(shoppingJob.user!!.email!!)
                }
            }

            return shoppersEmail
        }

    companion object {

        @JvmStatic
        fun sampleColor(): Int {
            val ctx = Sprinkles.sContext
            val shipmentColors = ctx.resources.getIntArray(R.array.shipment_colors)

            val max = shipmentColors.size - 1
            val min = 0
            val position = Random().nextInt(max - min + 1) + min

            return shipmentColors.filterIndexed { i, _ -> position == i }.firstOrNull() ?: shipmentColors[0]
        }

        @JvmStatic
        fun findById(remoteId: Long?): Batch? {
            return SQLite.select().from(Batch::class.java).where(Batch_Table.remoteId.eq(remoteId)).querySingle()
        }

        @JvmStatic
        @Deprecated(message = "Active batch contains multiple items")
        fun currentBatch(): Batch? {
            val store = Sprinkles.stores(BatchStore::class.java)
            store?.let {
                return findById(it.currentBatchID())
            }

            return null
        }

        @JvmStatic
        fun clear(): Boolean {
            return true
        }
    }
}
