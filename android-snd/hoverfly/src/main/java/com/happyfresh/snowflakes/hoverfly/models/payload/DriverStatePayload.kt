package com.happyfresh.snowflakes.hoverfly.models.payload

import com.google.gson.annotations.SerializedName

/**
 * Created by ifranseda on 4/10/15.
 */

class DriverStatePayload : SerializablePayload() {

    @SerializedName("state")
    var state: String? = null
}
