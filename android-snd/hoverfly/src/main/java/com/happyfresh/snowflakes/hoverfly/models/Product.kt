package com.happyfresh.snowflakes.hoverfly.models

import android.text.TextUtils
import com.google.gson.annotations.SerializedName
import com.raizlabs.android.dbflow.annotation.Column
import org.parceler.Parcel
import java.util.*

/**
 * Created by rsavianto on 11/14/14.
 */

@Parcel
class Product {

    @SerializedName("id")
    var remoteId: Long? = null

    var name: String? = null
    var description: String? = null
    var price: Double? = null

    @SerializedName("supermarket_unit_cost_price")
    var supermarketUnitCostPrice: Double = 0.toDouble()

    @SerializedName("display_price")
    var displayPrice: String? = null

    @SerializedName("display_supermarket_unit_cost_price")
    var displaySupermarketUnitCostPrice: String? = null

    @SerializedName("available_on")
    var availableOn: Date? = null

    var slug: String? = null

    @SerializedName("meta_description")
    var metaDescription: String? = null

    @SerializedName("meta_keywords")
    var metaKeywords: String? = null

    @SerializedName("shipping_category_id")
    var shippingCategoryId: Long? = null

    @SerializedName("taxon_ids")
    var taxonIds: List<Long>? = null

    @SerializedName("total_on_hand")
    var totalOnHand: Int? = null

    @SerializedName("has_variants")
    var hasVariants: Boolean = false

    var popularity: Double? = null
    var variants: List<Variant> = ArrayList()

    @SerializedName("option_types")
    var optionTypes: List<OptionType> = ArrayList()

    @SerializedName("product_properties")
    var productProperties: List<ProductProperty> = ArrayList()

    @SerializedName("display_unit")
    var displayUnit: String? = null

    @SerializedName("display_average_weight")
    var displayAverageWeight: String? = null

    @SerializedName("natural_average_weight")
    @Column
    var naturalAverageWeight: Double = 0.toDouble()

    @SerializedName("supermarket_unit")
    @Column
    var supermarketUnit: String? = null

    private var mDisplayProperty: String? = null

    var replaced: Boolean = false

    var category: String? = null

    fun displayProperty(): String {
        if (mDisplayProperty == null) {
            var pack: ProductProperty? = null
            var size: ProductProperty? = null
            var unit: ProductProperty? = null

            val properties = productProperties
            for (property in properties) {
                if (property.isPackage) {
                    pack = property
                }
                else if (property.isUnit) {
                    size = property
                }
                else if (property.isSize) {
                    unit = property
                }
            }

            val builder = StringBuilder()

            if (size != null && unit != null) {
                if (unit.value != "1") {
                    builder.append(unit.value)
                    builder.append(" ")
                }

                builder.append(size.value)
            }

            if (!TextUtils.isEmpty(builder) && pack != null && Integer.parseInt(pack.value) > 1) {
                builder.insert(0, " ")
                builder.insert(0, "x")
                builder.insert(0, " ")
                builder.insert(0, pack.value)
            }

            mDisplayProperty = builder.toString()
        }

        return mDisplayProperty!!
    }
}
