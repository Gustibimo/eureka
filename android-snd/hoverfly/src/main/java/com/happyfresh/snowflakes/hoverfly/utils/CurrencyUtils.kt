package com.happyfresh.snowflakes.hoverfly.utils

import java.text.NumberFormat
import java.util.*

/**
 * Created by ifranseda on 12/30/17.
 */
class CurrencyUtils {
    companion object {
        val currencyLocale: MutableMap<Currency, Locale> = mutableMapOf()
        fun getInstance(): CurrencyUtils = CurrencyUtils()
    }

    init {
        for (locale in Locale.getAvailableLocales()) {
            try {
                val currency = Currency.getInstance(locale)
                currencyLocale.put(currency, locale)
            }
            catch (e: Exception) {
            }
        }
    }

    fun symbolForCurrency(code: String): String {
        val currency = Currency.getInstance(code)
        val locale = currencyLocale[currency]
        return currency.getSymbol(locale)
    }

    fun numberFormatter(code: String?): NumberFormat {
        val currency = Currency.getInstance(code)
        val locale = currencyLocale[currency]
        return NumberFormat.getCurrencyInstance(locale).apply { this.currency = currency }
    }
}