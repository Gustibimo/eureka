package com.happyfresh.snowflakes.hoverfly.models

import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.shared.db.AppDatabase
import com.raizlabs.android.dbflow.annotation.*
import com.raizlabs.android.dbflow.sql.language.SQLite
import com.raizlabs.android.dbflow.structure.BaseModel
import org.parceler.Parcel
import java.io.Serializable
import java.util.*

/**
 * Created by rsavianto on 12/29/14.
 */

@Parcel
@Table(database = AppDatabase::class)
class JobState : BaseModel(), Serializable {

    @SerializedName("id")
    @Column
    @PrimaryKey
    var remoteId: String? = null

    @Column(name = "state_name")
    var state: String? = null

    @SerializedName("start_time")
    @Column
    var startTime: Date? = null

    @Column
    var jobId: Long? = null

    val isFoundAddress: Boolean
        get() = Job.Progress.FOUND_ADDRESS.equals(this.state, ignoreCase = true)

    val isAccepted: Boolean
        get() = Job.Progress.ACCEPTED.equals(this.state, ignoreCase = true)

    val job: Job?
        get() {
            return SQLite.select().from(Job::class.java).where(Job_Table.remoteId.eq(jobId)).querySingle()
        }

    companion object {
        fun clear(jobId: Long?) {
            SQLite.delete().from(JobState::class.java).where(JobState_Table.jobId.eq(jobId)).execute()
        }
    }
}
