package com.happyfresh.snowflakes.hoverfly.models


import com.google.gson.annotations.SerializedName
import org.parceler.Parcel

/**
 * Created by rsavianto on 2/7/15.
 */

@Parcel
class District {

    @SerializedName("id")
    var remoteId: Long = 0

    var name: String? = null

    var state: State? = null
}
