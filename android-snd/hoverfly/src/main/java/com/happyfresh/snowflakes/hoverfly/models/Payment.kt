package com.happyfresh.snowflakes.hoverfly.models

import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.shared.db.AppDatabase
import com.raizlabs.android.dbflow.annotation.*
import com.raizlabs.android.dbflow.structure.BaseModel
import org.parceler.Parcel
import java.util.*

/**
 * Created by ifranseda on 11/14/14.
 */

@Parcel
@Table(database = AppDatabase::class)
class Payment : BaseModel() {

    @SerializedName("id")
    @Column
    @Unique
    @PrimaryKey
    var remoteId: Long? = null

    @SerializedName("source_type")
    @Column
    var sourceType: String? = null

    @SerializedName("source_id")
    @Column
    var sourceId: Int = 0

    @Column
    var amount: Double = 0.toDouble()

    @SerializedName("display_amount")
    @Column
    var displayAmount: String? = null

    @SerializedName("payment_method_id")
    @Column
    var paymentMethodId: Int = 0

    @SerializedName("response_code")
    @Column
    var responseCode: String? = null

    @Column
    var state: String? = null

    @SerializedName("avs_response")
    @Column
    var avsResponse: String? = null

    @SerializedName("created_at")
    @Column
    var createdAt: Date? = null

    @SerializedName("update_at")
    @Column
    var updatedAt: Date? = null

    @SerializedName("payment_method")
    @Column
    @ForeignKey(saveForeignKeyModel = false)
    var paymentMethod: PaymentMethod? = null

    @Column
    @ForeignKey(saveForeignKeyModel = false)
    var source: Source? = null
}
