package com.happyfresh.snowflakes.hoverfly.shared.jobs

import com.birbit.android.jobqueue.Job
import com.birbit.android.jobqueue.Params
import com.birbit.android.jobqueue.RetryConstraint
import com.happyfresh.snowflakes.hoverfly.Sprinkles
import com.happyfresh.snowflakes.hoverfly.models.payload.ItemShopPayload
import com.happyfresh.snowflakes.hoverfly.models.payload.ListItemShopPayload
import com.happyfresh.snowflakes.hoverfly.models.response.LineItemResponse
import com.happyfresh.snowflakes.hoverfly.services.ShipmentService
import com.happyfresh.snowflakes.hoverfly.shared.events.EventBus
import com.happyfresh.snowflakes.hoverfly.shared.events.SyncEvent
import com.happyfresh.snowflakes.hoverfly.shared.events.SyncFailedEvent
import com.happyfresh.snowflakes.hoverfly.shared.subscribers.ServiceSubscriber

/**
 * Created by ifranseda on 12/12/17.
 */

class MultipleItemShopJob(val shipmentId: Long, val shopItems: List<ItemShopPayload>) : Job(Params(1).requireNetwork().persist()) {

    private val service: ShipmentService = Sprinkles.service(ShipmentService::class.java)

    override fun onRun() {
//        val payload = ListItemShopPayload().apply { items = shopItems }
//        val observable = service.shop(shipmentId, payload)
//        val subscriber = object : ServiceSubscriber<LineItemResponse>() {
//            override fun onFailure(e: Throwable) {
//                EventBus.handler.post(SyncFailedEvent(e))
//            }
//
//            override fun onSuccess(data: LineItemResponse) {
//                data.items?.forEach { it.save() }
//            }
//        }
//
//        observable.subscribe(subscriber)
    }

    override fun shouldReRunOnThrowable(throwable: Throwable, runCount: Int, maxRunCount: Int): RetryConstraint {
        return RetryConstraint.createExponentialBackoff(3, 2000)
    }

    override fun onAdded() {
        EventBus.handler.post(SyncEvent(shopItems))
    }

    override fun onCancel(cancelReason: Int, throwable: Throwable?) {

    }
}
