package com.happyfresh.snowflakes.hoverfly.models

import android.text.TextUtils

import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.shared.db.AppDatabase
import com.raizlabs.android.dbflow.annotation.*
import com.raizlabs.android.dbflow.sql.language.SQLite
import com.raizlabs.android.dbflow.structure.BaseModel
import org.parceler.Parcel

/**
 * Created by ifranseda on 11/14/14.
 */

@Parcel
@Table(database = AppDatabase::class)
class Replacement : BaseModel() {

    @SerializedName("id")
    @PrimaryKey
    var remoteId: Long = 0

    @Column(name = "line_item_id")
    var lineItemId: Long = 0

    @SerializedName("variant_id")
    @Column
    var variantId: Long = 0

    @Column(name = "priority")
    var priority: Double? = null

    @Column
    var price: Double? = null

    @SerializedName("display_price")
    @Column
    var displayPrice: String? = null

    @SerializedName("cost_price")
    @Column
    var costPrice: Double? = null

    @SerializedName("supermarket_unit_cost_price")
    @Column
    var supermarketUnitCostPrice: Double? = null

    @SerializedName("display_cost_price")
    @Column
    var displayCostPrice: String? = null

    @SerializedName("display_supermarket_unit_cost_price")
    @Column
    var displaySupermarketUnitCostPrice: String? = null

    @Column
    var currency: String? = null

    @Column
    var isActive: Boolean = false

    @SerializedName("user_id")
    @Column
    var userId: Long? = null

    @ForeignKey(stubbedRelationship = true)
    var lineItem: LineItem? = null
        set(value) {
            field = value
            lineItemId = field?.remoteId ?: 0
        }

    @ForeignKey(stubbedRelationship = true)
    var variant: Variant? = null
        set(value) {
            field = value
            lineItemId = field?.remoteId ?: 0
        }

    @SerializedName("product_properties")
    var productProperties: List<ProductProperty>? = null

    val properties: List<ProductProperty>
        get() = SQLite.select().from(ProductProperty::class.java).where(ProductProperty_Table.variant_remoteId.eq(remoteId)).queryList()

    val units: String
        get() {
            var pack: ProductProperty? = null
            var size: ProductProperty? = null
            var unit: ProductProperty? = null

            val properties = properties
            for (property in properties) {
                when (property.type) {
                    ProductProperty.PropertyName.PACKAGE -> pack = property

                    ProductProperty.PropertyName.SIZE -> size = property

                    ProductProperty.PropertyName.UNIT -> unit = property
                }
            }

            val builder = StringBuilder()

            if (size != null && unit != null) {
                if (size.value != "1") {
                    builder.append(size.value)
                    builder.append(" ")
                }

                builder.append(unit.value)
            }

            if (!TextUtils.isEmpty(builder) && pack != null && Integer.parseInt(pack.value) > 1) {
                builder.insert(0, " ")
                builder.insert(0, "x")
                builder.insert(0, " ")
                builder.insert(0, pack.value)
            }

            return builder.toString()
        }

    companion object {
        @JvmStatic
        fun getReplacementForItem(lineItemId: Long?): List<Replacement> {
            return SQLite.select().from(Replacement::class.java).where(Replacement_Table.variantId.eq(lineItemId)).queryList()
        }

        @JvmStatic
        fun findById(id: Long): Replacement? {
            return SQLite.select().from(Replacement::class.java).where(Replacement_Table.remoteId.eq(id)).querySingle()
        }

        @JvmStatic
        fun findAll(): List<Replacement> {
            return SQLite.select().from(Replacement::class.java).queryList()
        }
    }
}
