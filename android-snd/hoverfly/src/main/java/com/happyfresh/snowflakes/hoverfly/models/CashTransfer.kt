package com.happyfresh.snowflakes.hoverfly.models

import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.shared.db.AppDatabase
import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import com.raizlabs.android.dbflow.annotation.Unique
import com.raizlabs.android.dbflow.structure.BaseModel
import org.parceler.Parcel
import java.util.*

/**
 * Created by ifranseda on 5/6/15.
 */

@Parcel
@Table(database = AppDatabase::class)
class CashTransfer : BaseModel() {

    @SerializedName("id")
    @Column
    @Unique
    @PrimaryKey
    var remoteId: Long? = null

    @Column
    var currency: String? = null

    @Column
    var amount: Double? = null

    @Column
    var receiver: String? = null

    @SerializedName("user_id")
    @Column
    var userId: String? = null

    @SerializedName("created_at")
    @Column(name = "created_at")
    var created: Date? = null
}
