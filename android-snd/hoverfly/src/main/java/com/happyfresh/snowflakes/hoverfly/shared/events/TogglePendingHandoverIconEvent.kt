package com.happyfresh.snowflakes.hoverfly.shared.events

class TogglePendingHandoverIconEvent(isNotEmpty: Boolean) : BaseEvent<Boolean>(isNotEmpty)