package com.happyfresh.snowflakes.hoverfly.stores

import android.content.Context

/**
 * Created by adefruandta on 12/27/17.
 */
class ShoppingStore internal constructor(context: Context) : BaseStore(context) {

    companion object {
        const val SHOPPING_TIMER = "PREFS.SHOPPING_TIMER"
    }

    fun setShoppingTimer(batchId: Long, time: Long) {
        editorCommit {
            it.putLong(SHOPPING_TIMER + "." + batchId.toString(), time)
        }
    }

    fun getShoppingTimer(batchId: Long): Long {
        return sharedPrefs().getLong(SHOPPING_TIMER + "." + batchId.toString(), 0)
    }
}