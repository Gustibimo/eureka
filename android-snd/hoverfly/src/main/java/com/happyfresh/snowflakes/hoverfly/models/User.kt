package com.happyfresh.snowflakes.hoverfly.models

import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.config.Constant
import com.happyfresh.snowflakes.hoverfly.shared.db.AppDatabase
import com.raizlabs.android.dbflow.annotation.*
import com.raizlabs.android.dbflow.config.FlowManager
import com.raizlabs.android.dbflow.sql.language.SQLite
import com.raizlabs.android.dbflow.structure.BaseModel
import org.parceler.Parcel
import java.io.Serializable
import java.util.*

/**
 * Created by galuh on 6/21/17.
 */

@Parcel
@Table(database = AppDatabase::class)
class User : BaseModel(), Serializable {

    @SerializedName("id")
    @Column
    @PrimaryKey
    var remoteId: Long = 0

    @Column
    var email: String? = null

    @SerializedName("created_at")
    @Column
    var createdAt: Date? = null

    @SerializedName("updated_at")
    @Column
    var updatedAt: Date? = null

    @SerializedName("bill_address")
    @Column
    @ForeignKey(saveForeignKeyModel = false)
    var billAddress: Address? = null

    @SerializedName("ship_address")
    @Column
    @ForeignKey(saveForeignKeyModel = false)
    var shipAddress: Address? = null

    @SerializedName("token")
    var token: String? = null

    @SerializedName("is_shopper")
    @Column
    var isShopper: Boolean = false

    @SerializedName("is_driver")
    var isDriver: Boolean = false

    @SerializedName("first_name")
    var firstName: String? = null

    @SerializedName("last_name")
    var lastName: String? = null

    @SerializedName("strings")
    var strings: String? = null

    fun fullName(): String {
        if (firstName == null && lastName == null) {
            return email!!
        }
        else {
            return String.format("%s %s", firstName, lastName)
        }
    }

    fun firebaseTopic(): String {
        return String.format("%1\$s%2\$d", Constant.USER_TOPIC, remoteId)
    }

    fun persist(): Boolean? {
        var objId = false

        val db = FlowManager.getDatabase(AppDatabase.NAME).writableDatabase
        db.beginTransaction()

        try {
            this.save()

            if (!isShopper && !isDriver) {
                val billAddress = this.billAddress
                if (billAddress != null) {
                    billAddress.user = this
                    billAddress.save()
                    this.billAddress = billAddress
                }

                val shipAddress = this.shipAddress
                if (shipAddress != null) {
                    shipAddress.user = this
                    shipAddress.save()
                    this.shipAddress = shipAddress
                }
            }

            objId = this.save()
            db.setTransactionSuccessful()
        }
        finally {
            db.endTransaction()
        }

        return objId
    }

    companion object {
        @JvmStatic
        fun findById(userId: Long?): User? {
            return SQLite.select().from(User::class.java).where(User_Table.remoteId.eq(userId)).querySingle()
        }
    }
}
