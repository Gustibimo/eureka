package com.happyfresh.snowflakes.hoverfly.shared.events

import com.squareup.otto.Bus

/**
 * Created by ifranseda on 05/10/2017.
 */

object EventBus {

    val handler: Bus by lazy { Bus() }
}
