package com.happyfresh.snowflakes.hoverfly.models.response

/**
 * Created by kharda on 9/7/16.
 */

class BatchProductSampleListResponse {

    var data: List<ProductSampleListResponse>? = null
}
