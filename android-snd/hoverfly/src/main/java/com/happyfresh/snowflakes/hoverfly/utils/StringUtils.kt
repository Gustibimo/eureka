package com.happyfresh.snowflakes.hoverfly.utils

import com.happyfresh.snowflakes.hoverfly.models.Slot
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by rsavianto on 12/24/14.
 */
class StringUtils {

    companion object {

        @JvmStatic
        fun showDeliveryTime(startDate: Date, endDate: Date): String {
            val date = SimpleDateFormat("dd-MM-yy hha")
            val time = SimpleDateFormat("hha")

            return String.format("%s - %s", date.format(startDate), time.format(endDate))
        }

        @JvmStatic
        fun showDeliveryTime(start: String, end: String): String {
            val dateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss z")

            return try {
                val startDate = dateFormat.parse(start)
                val endDate = dateFormat.parse(end)
                showDeliveryTime(startDate, endDate)
            } catch (e: ParseException) {
                start + " - " + end
            }

        }

        @JvmStatic
        fun showDeliveryTime(start: String): String {
            val dateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss z")

            return try {
                val startDate = dateFormat.parse(start)
                startDate.toString()
            } catch (e: ParseException) {
                start
            }

        }

        @JvmStatic
        fun showShoppingTime(startDate: Date, endDate: Date, slot: Slot): String {
            val df = SimpleDateFormat("hh:mma")

            val deliverySlotWindowsInMinute = (((slot.startTime?.time!! - slot.endTime?.time!!) / (60 * 1000))).toInt()

            val cal = Calendar.getInstance()
            cal.time = startDate
            cal.add(Calendar.MINUTE, deliverySlotWindowsInMinute)
            val start = cal.time

            cal.time = endDate
            cal.add(Calendar.MINUTE, deliverySlotWindowsInMinute)
            val end = cal.time

            return String.format("%s - %s", df.format(start), df.format(end))
        }
    }
}
