package com.happyfresh.snowflakes.hoverfly.models

import com.happyfresh.snowflakes.hoverfly.shared.db.AppDatabase
import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import com.raizlabs.android.dbflow.annotation.Unique
import com.raizlabs.android.dbflow.sql.language.SQLite
import com.raizlabs.android.dbflow.structure.BaseModel
import org.parceler.Parcel

/**
 * Created by julianagalag on 5/13/15.
 */

@Parcel
@Table(database = AppDatabase::class)
class ShopperReplacement : BaseModel {

    @Column
    @PrimaryKey(autoincrement = true)
    var id: Long = 0

    @Column
    @Unique(unique = true, uniqueGroups = intArrayOf(1))
    var lineItemVariantId: Long = 0

    @Column
    @Unique(unique = true, uniqueGroups = intArrayOf(1))
    var lineItemOrderId: Long = 0

    @Column
    @Unique(unique = true, uniqueGroups = intArrayOf(1))
    var lineItemStockLocationId: Long = 0

    @Column
    @Unique(unique = true, uniqueGroups = intArrayOf(1))
    var variantId: Long = 0

    @Column
    var replacementId: Long = 0

    @Column
    var userId: Long = 0

    val variant: Variant?
        get() {
            return SQLite.select().from(Variant::class.java).where(Variant_Table.remoteId.eq(variantId)).querySingle()
        }

    val lineItem: LineItem?
        get() {
            return SQLite.select().from(LineItem::class.java)
                    .where(LineItem_Table.variantId.eq(lineItemVariantId))
                    .and(LineItem_Table.orderId.eq(lineItemOrderId))
                    .and(LineItem_Table.stockLocationId.eq(lineItemStockLocationId))
                    .querySingle()
        }

    constructor() : super()

    constructor(lineItemVariantId: Long, lineItemOrderId: Long, lineItemStockLocationId: Long, variantId: Long, replacementId: Long) : super() {
        this.lineItemVariantId = lineItemVariantId
        this.lineItemOrderId = lineItemOrderId
        this.lineItemStockLocationId = lineItemStockLocationId
        this.variantId = variantId
        this.replacementId = replacementId
    }

    companion object {

        @JvmStatic
        fun findByReplacementId(id: Long): ShopperReplacement? {
            return SQLite.select().from(ShopperReplacement::class.java).where(ShopperReplacement_Table.replacementId.eq(id)).querySingle()
        }
    }
}
