package com.happyfresh.snowflakes.hoverfly.models.payload

import com.google.gson.annotations.SerializedName

/**
 * Created by ifranseda on 11/12/15.
 */

class LocationPayload {

    @SerializedName("snd")
    private var coordinate: LocationUpdatePayload? = null

    constructor() {}

    constructor(latitude: Double, longitude: Double, accuracy: Double) {
        val coordinate = LocationUpdatePayload()
        coordinate.latitude = latitude
        coordinate.longitude = longitude
        coordinate.accuracy = accuracy

        this.coordinate = coordinate
    }
}
