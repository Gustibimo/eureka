package com.happyfresh.snowflakes.hoverfly.models.payload

import com.happyfresh.snowflakes.hoverfly.models.ReceiptItem
import java.util.*

/**
 * Created by ifranseda on 12/2/15.
 */

class ReceiptCollection(items: List<ReceiptItem>) {

    var receipts: List<ReceiptItem> = ArrayList(items)
}
