package com.happyfresh.snowflakes.hoverfly.models.payload

import com.google.gson.annotations.SerializedName

/**
 * Created by ifranseda on 12/1/14.
 */

class FlagItemPayload : SerializablePayload() {

    @SerializedName("variant_id")
    var variantId: Long? = null

    var state: String? = null

    @SerializedName("type_id")
    var typeId: Int? = null

    @SerializedName("total_weight_by_shopper")
    var totalWeightByShopper: Double? = null

    var detail: String? = null
}
