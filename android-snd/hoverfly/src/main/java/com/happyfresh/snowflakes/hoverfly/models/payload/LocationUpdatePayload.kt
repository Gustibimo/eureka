package com.happyfresh.snowflakes.hoverfly.models.payload

import com.google.gson.annotations.SerializedName

/**
 * Created by ifranseda on 11/12/15.
 */

class LocationUpdatePayload {

    @SerializedName("lat")
    var latitude: Double = 0.toDouble()

    @SerializedName("lon")
    var longitude: Double = 0.toDouble()

    @SerializedName("accuracy")
    var accuracy: Double = 0.toDouble()
}
