package com.happyfresh.snowflakes.hoverfly.stores

import android.content.Context
import android.text.TextUtils
import com.happyfresh.snowflakes.hoverfly.Sprinkles
import com.happyfresh.snowflakes.hoverfly.models.User
import com.happyfresh.snowflakes.hoverfly.models.response.StatusResponse

/**
 * Created by ifranseda on 19/07/2017.
 */
class UserStore internal constructor(context: Context) : BaseStore(context) {

    companion object {
        const val USER_TOKEN = "PREFS.USER_TOKEN"
        const val USER_ID = "PREFS.USER_ID"
        const val USER_NAME = "PREFS.USER_NAME"
        const val USER_EMAIL = "PREFS.USER_EMAIL"
        const val USER_TYPE = "PREFS.USER_TYPE"
        const val WORKING_STATUS = "PREFS.WORKING_STATUS"
        const val PAUSE_TIMER = "PREFS.PAUSE_TIMER"
        const val ON_PAUSED = "PREFS.ON_PAUSED"

        const val PRESENCE_STATUS = "SND_STATUS"
        const val STOCK_LOCATION_ID = "SND_STOCK_LOCATION_ID"
        const val STOCK_LOCATION_NAME = "SND_STOCK_LOCATION_NAME"
        const val ASK_RECEIPT_NUMBER = "SND_ASK_RECEIPT_NUMBER"
        const val PRICE_CHANGED_WILL_UPDATE_PRICE = "SND_PRICE_CHANGED_WILL_UPDATE_PRICE"
        const val REQUIRE_AGE_VERIFICATION = "SND_STATUS_REQUIRE_AGE_VERIFICATION"
        const val AGE_VERIFICATION_WORDING = "SND_STATUS_AGE_VERIFICATION_WORDING"
        const val AGE_VERIFICATION_WORDING_ENGLISH = "SND_STATUS_AGE_VERIFICATION_WORDING_ENGLISH"
        const val RESTRICTED_CATEGORIES = "SND_STATUS_RESTRICTED_CATEGORIES"
        const val LAST_MODIFIED = "SND_LAST_MODIFIED"
        const val STORE_TYPE = "SND_STORE_TYPE"

        const val SHOPPER = "NORMALLY_SHOPPER"
        const val DRIVER = "NORMALLY_DRIVER"
    }

    enum class UserType {
        Shopper,
        Driver,
        Ranger
    }

    var token: String?
        get() {
            return sharedPrefs().getString(USER_TOKEN, null)
        }
        set(value) {
            sharedPrefEditor().putString(USER_TOKEN, value).commit()
        }

    val hasToken: Boolean
        get () {
            return sharedPrefs().contains(USER_TOKEN) && !TextUtils.isEmpty(this.token)
        }

    val userType: String? = sharedPrefs().getString(USER_TYPE, null)

    val isShopper: Boolean? = this.userType?.equals(UserType.Shopper.name)

    val isDriver: Boolean? = this.userType?.equals(UserType.Driver.name)

    val userId: Long
        get() {
            return sharedPrefs.getLong(USER_ID, 0)
        }

    val userName: String? = sharedPrefs.getString(USER_NAME, "Shopper")

    fun saveUser(user: User) {
        token = user.token!!

        val editor = sharedPrefEditor()
                .putLong(USER_ID, user.remoteId)
                .putString(USER_NAME, user.fullName())
                .putString(USER_EMAIL, user.email)

        if (user.isShopper) {
            editor.putString(USER_TYPE, UserType.Shopper.name)
        }
        else if (user.isDriver) {
            editor.putString(USER_TYPE, UserType.Driver.name)
        }

        editor.commit()
    }

    fun reset() {
        sharedPrefEditor().clear().commit()
        Sprinkles.resetDatabase()
    }

    fun saveStatus(status: StatusResponse) {
        sharedPrefEditor().putLong(LAST_MODIFIED, status.lastModified)
                .putString(PRESENCE_STATUS, status.workingStatus)
                .putString(STOCK_LOCATION_NAME, status.stockLocationName)
                .putLong(STOCK_LOCATION_ID, status.stockLocationId)
                .putBoolean(ASK_RECEIPT_NUMBER, status.requireReceiptNumber)
                .putBoolean(PRICE_CHANGED_WILL_UPDATE_PRICE, status.priceChangeAutoPublish)
                .putBoolean(REQUIRE_AGE_VERIFICATION, status.requireAgeVerification)
                .putString(AGE_VERIFICATION_WORDING, status.ageVerificationDisclaimer)
                .putString(AGE_VERIFICATION_WORDING_ENGLISH, status.ageVerificationDisclaimerEnglish)
                .putString(RESTRICTED_CATEGORIES, TextUtils.join(",", status.restrictedCategories))
                .putString(STORE_TYPE, status.storeType)
                .commit()
    }

    fun currentStoreType(): String? {
        return sharedPrefs().getString(STORE_TYPE, null)
    }

    var workingStatus: Boolean
        get() {
            return sharedPrefs().getBoolean(WORKING_STATUS, false)
        }
        set(value) {
            sharedPrefEditor().putBoolean(WORKING_STATUS, value).commit()
        }

    var pauseTimer: Long
        get() = sharedPrefs.getLong(PAUSE_TIMER, 0L)
        set(value) {
            sharedPrefEditor().putLong(PAUSE_TIMER, value).commit()
        }

    var onPaused: Boolean
        get() = sharedPrefs().getBoolean(ON_PAUSED, false)
        set(value) {
            sharedPrefEditor().putBoolean(ON_PAUSED, value).commit()
        }
}