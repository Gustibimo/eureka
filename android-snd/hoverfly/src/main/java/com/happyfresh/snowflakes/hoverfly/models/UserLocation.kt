package com.happyfresh.snowflakes.hoverfly.models

import com.google.gson.annotations.SerializedName

/**
 * Created by aldi on 6/22/17.
 */

class UserLocation {

    @SerializedName("lat")
    var latitude: Double? = null

    @SerializedName("lon")
    var longitude: Double? = null

    @SerializedName("stock_location_id")
    var stockLocationId: Long? = null

    @SerializedName("stock_location_name")
    var stockLocationName: String? = null
}
