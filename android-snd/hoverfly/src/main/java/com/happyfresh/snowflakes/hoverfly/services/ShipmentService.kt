package com.happyfresh.snowflakes.hoverfly.services

import com.happyfresh.snowflakes.hoverfly.api.IShipmentService
import com.happyfresh.snowflakes.hoverfly.models.*
import com.happyfresh.snowflakes.hoverfly.models.payload.*
import com.happyfresh.snowflakes.hoverfly.models.response.ChangePriceResponse
import com.happyfresh.snowflakes.hoverfly.models.response.GrabExpressCancelReasonResponse
import com.happyfresh.snowflakes.hoverfly.models.response.LineItemResponse
import com.happyfresh.snowflakes.hoverfly.shared.events.*
import com.happyfresh.snowflakes.hoverfly.shared.subscribers.ServiceSubscriber
import com.happyfresh.snowflakes.hoverfly.utils.FileUtils
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit.client.Response
import rx.Observable

/**
 * Created by galuh on 11/29/17.
 */
class ShipmentService : BaseService<IShipmentService>() {

    object Flag {
        val OUT_OF_STOCK = "out_of_stock"
        val ON_REPLACE = "on_replace"
    }

    override fun serviceClass(): Class<IShipmentService> {
        return IShipmentService::class.java
    }

    fun shop(shipmentId: Long, variantId: Long, quantity: Int, actualWeight: Double?, shopperNotesFulfilled: Boolean) {
        val payload = ItemShopPayload()
        payload.variantId = variantId
        payload.quantity = quantity
        payload.actualWeight = actualWeight
        payload.shopperNotesFulfilled = shopperNotesFulfilled

        val listPayload = ListItemShopPayload()
        listPayload.items = arrayListOf(payload)

        val observable = observe(api().shop(shipmentId, listPayload))
        val subscriber = object : ServiceSubscriber<LineItemResponse>() {
            override fun onFailure(e: Throwable) {
                EventBus.handler.post(ShopEventFailed(e))
            }

            override fun onSuccess(data: LineItemResponse) {
                val lineItemList = ArrayList<LineItem>()

                for (lineItem in data.items.orEmpty()) {
                    LineItem.findByLineItem(lineItem)?.let { savedItem ->
                        savedItem.totalShopped = lineItem.totalShopped
                        savedItem.totalReplaced = lineItem.totalReplaced
                        savedItem.totalReplacedCustomer = lineItem.totalReplacedCustomer
                        savedItem.totalReplacedShopper = lineItem.totalReplacedShopper
                        savedItem.totalFlagged = lineItem.totalFlagged
                        savedItem.flag = lineItem.flag
                        savedItem.save()

                        lineItemList.add(savedItem)
                    }
                }

                EventBus.handler.post(ShopEvent(lineItemList))
            }
        }

        observable.subscribe(subscriber)
    }

    fun replaceItem(shipmentId: Long, lineItem: LineItem, replacementItem: Replacement, quantity: Int, actualWeight: Double) {
        val savedItem = LineItem.findByVariantIdOrderIdAndStockLocationId(lineItem.variantId, lineItem.orderId, lineItem.stockLocationId) ?: return

        val replacementPayload = ReplacementPayload()
        replacementPayload.variantId = savedItem.variant?.remoteId
        replacementPayload.quantity = (savedItem.totalShopped - savedItem.totalReplaced)

        val itemShopPayload = ItemShopPayload()
        itemShopPayload.variantId = replacementItem.remoteId
        itemShopPayload.quantity = quantity
        itemShopPayload.actualWeight = actualWeight
        replacementPayload.replacements.add(itemShopPayload)

        val payload = ListItemReplacementPayload().apply { items = mutableListOf(replacementPayload) }

        val observable = observe(api().replace(shipmentId, payload))
        val subscriber = object : ServiceSubscriber<LineItemResponse>() {
            override fun onFailure(e: Throwable) {
                EventBus.handler.post(ReplaceEventFailed(e))
            }

            override fun onSuccess(data: LineItemResponse) {
                val lineItemList = ArrayList<LineItem>()

                for (lineItem in data.items.orEmpty()) {
                    LineItem.findByLineItem(lineItem)?.let { savedItem ->
                        savedItem.totalShopped = lineItem.totalShopped
                        savedItem.totalReplaced = lineItem.totalReplaced
                        savedItem.totalReplacedCustomer = lineItem.totalReplacedCustomer
                        savedItem.totalReplacedShopper = lineItem.totalReplacedShopper
                        savedItem.totalFlagged = lineItem.totalFlagged
                        savedItem.flag = lineItem.flag

                        lineItemList.add(savedItem)

                        Shipment.findById(shipmentId)?.let { shipment ->
                            lineItem.replacements.forEach { replacement ->
                                replacement.variant?.let {
                                    it.price = replacement.price
                                    it.displayPrice = replacement.displayPrice
                                    it.costPrice = replacement.costPrice
                                    it.displayCostPrice = replacement.displayCostPrice
                                    it.supermarketUnitCostPrice = replacement.supermarketUnitCostPrice
                                    it.displaySupermarketUnitCostPrice = replacement.displaySupermarketUnitCostPrice
                                    it.save()
                                }

                                if (replacement.userId == null || (shipment.order != null && replacement.userId == shipment.order?.userId)) {
                                    Replacement.findById(replacement.remoteId)?.apply { this.lineItem = lineItem }?.save()
                                }
                                else {
                                    if (lineItem.replacementShopper == null && lineItem.totalReplacedShopper > 0) {
                                        val shopperReplacement = ShopperReplacement.findByReplacementId(replacement.remoteId) ?:
                                                ShopperReplacement(lineItem.variantId, lineItem.orderId, lineItem.stockLocationId, replacement.variant?.remoteId!!, replacement.variantId)
                                        shopperReplacement.apply { this.userId = replacement.userId!! }.save()
                                    }
                                }

                            }
                        }
                    }
                }

                EventBus.handler.post(ReplaceEvent(lineItemList))
            }
        }

        observable.subscribe(subscriber)
    }

    fun flagStockOut(shipmentId: Long, variantId: Long, flag: String, typeId: Int, detail: String?) {
        flagStockOut(shipmentId, variantId, flag, typeId, detail, null, null)
    }

    fun flagStockOut(shipmentId: Long, variantId: Long, flag: String, typeId: Int, detail: String?, newWeight: Double?, imagePath: String?) {
        val partBuilder = MultipartBody.Builder()
        partBuilder.addFormDataPart("items[][state]", flag)
        partBuilder.addFormDataPart("items[][variant_id]", variantId.toString())
        partBuilder.addFormDataPart("items[][type_id]", typeId.toString())

        detail?.let {
            partBuilder.addFormDataPart("items[][detail]", it)
        }

        newWeight?.let {
            partBuilder.addFormDataPart("items[][total_weight_by_shopper]", it.toString())
        }

        imagePath?.let {
            try {
                val imageFile = FileUtils.getFile(imagePath)
                val fileName = imageFile.name
                val attachment = RequestBody.create(MediaType.parse("image/jpg"), imageFile)
                partBuilder.addFormDataPart("items[][attachment]", fileName, attachment)
            }
            catch (e: Exception) {
            }
        }

        val observable = observe(api().flag(shipmentId, partBuilder.build()))
        val subscriber = object : ServiceSubscriber<LineItemResponse>() {
            override fun onFailure(e: Throwable) {
                EventBus.handler.post(StockOutEventFailed(e))
            }

            override fun onSuccess(data: LineItemResponse) {
                val lineItemList = ArrayList<LineItem>()

                for (lineItem in data.items.orEmpty()) {
                    LineItem.findByLineItem(lineItem)?.let { savedItem ->
                        savedItem.totalFlagged = lineItem.totalFlagged
                        savedItem.flag = lineItem.flag
                        savedItem.save()

                        lineItemList.add(savedItem)
                    }
                }

                EventBus.handler.post(StockOutEvent(lineItemList))
            }
        }

        observable.subscribe(subscriber)
    }

    fun removeFlagStockOut(shipmentId: Long, variant: Long) {
        val item = FlagItemPayload().apply { variantId = variant }

        val payload = ListFlagItemPayload()
        payload.items.add(item)

        val observable = observe(api().removeFlag(shipmentId, payload))
        val subscriber = object : ServiceSubscriber<LineItemResponse>() {
            override fun onFailure(e: Throwable) {
                EventBus.handler.post(StockOutEventFailed(e))
            }

            override fun onSuccess(data: LineItemResponse) {
                val lineItemList = ArrayList<LineItem>()

                for (lineItem in data.items.orEmpty()) {
                    LineItem.findByLineItem(lineItem)?.let { savedItem ->
                        savedItem.totalFlagged = lineItem.totalFlagged
                        savedItem.totalReplaced = lineItem.totalReplaced
                        savedItem.totalReplacedCustomer = lineItem.totalReplacedCustomer
                        savedItem.totalReplacedShopper = lineItem.totalReplacedShopper
                        savedItem.totalShopped = lineItem.totalShopped
                        savedItem.flag = lineItem.flag
                        savedItem.save()

                        lineItemList.add(savedItem)
                    }
                }

                EventBus.handler.post(StockOutEvent(lineItemList))
            }
        }

        observable.subscribe(subscriber)
    }

    fun flagMismatch(shipmentId: Long, lineItem: LineItem, imagePath: String) {
        val partBuilder = MultipartBody.Builder()
        partBuilder.addFormDataPart("variant_id", lineItem.variantId.toString())
        partBuilder.addFormDataPart("order_id", lineItem.orderId.toString())
        partBuilder.addFormDataPart("stock_location_id", lineItem.stockLocationId.toString())

        try {
            val imageFile = FileUtils.getFile(imagePath)
            val fileName = imageFile.name
            val attachment = RequestBody.create(MediaType.parse("image/jpg"), imageFile)
            partBuilder.addFormDataPart("attachment", fileName, attachment)
        }
        catch (e: Exception) {
            e.printStackTrace()
            return
        }

        val observable = observe(api(true).mismatch(shipmentId, partBuilder.build()))
        val subscriber = object : ServiceSubscriber<LineItem>() {
            override fun onFailure(e: Throwable) {
                EventBus.handler.post(MismatchEventFailed(e))
            }

            override fun onSuccess(data: LineItem) {
                LineItem.findByLineItem(data)?.let {
                    it.isProductMismatch = true
                    it.save()
                }

                EventBus.handler.post(MismatchEvent(arrayListOf(data)))
            }
        }

        observable.subscribe(subscriber)
    }

    fun removeFlagMismatch(shipmentId: Long, lineItem: LineItem) {
        val payload = MismatchPayload().apply {
            variantId = lineItem.variantId
            orderId = lineItem.orderId
            stockLocationId = lineItem.stockLocationId
        }

        val observable = observe(api().removeMismatch(shipmentId, payload))
        val subscriber = object : ServiceSubscriber<LineItem>() {
            override fun onFailure(e: Throwable) {
                EventBus.handler.post(MismatchEventFailed(e))
            }

            override fun onSuccess(data: LineItem) {
                LineItem.findByLineItem(data)?.let {
                    it.isProductMismatch = false
                    it.save()
                }

                EventBus.handler.post(MismatchEvent(arrayListOf(data)))
            }
        }

        observable.subscribe(subscriber)
    }

    fun changePrice(lineItem: LineItem, newPrice: Double, imagePath: String) {
        val partBuilder = MultipartBody.Builder()
        partBuilder.addFormDataPart("new_price", newPrice.toString())

        try {
            val imageFile = FileUtils.getFile(imagePath)
            val fileName = imageFile.name
            val attachment = RequestBody.create(MediaType.parse("image/jpg"), imageFile)
            partBuilder.addFormDataPart("attachment", fileName, attachment)
        }
        catch (e: Exception) {
            e.printStackTrace()
            return
        }

        val observable = observe(api(true).changePrice(lineItem.orderId, lineItem.stockLocationId, lineItem.variantId, partBuilder.build()))
        val subscriber = object : ServiceSubscriber<ChangePriceResponse>() {
            override fun onFailure(e: Throwable) {
                EventBus.handler.post(PriceChangedEventFailed(e))
            }

            override fun onSuccess(data: ChangePriceResponse) {
                if (data.variantId != null && data.orderId != null && data.stockLocationId != null) {
                    val savedItem = LineItem.findByVariantIdOrderIdAndStockLocationId(data.variantId!!, data.orderId!!, data.stockLocationId!!) ?: return

                    val priceAmendment = savedItem.priceAmendment ?: PriceAmendment()
                    priceAmendment.remoteId = data.id
                    priceAmendment.variantId = data.variantId
                    priceAmendment.orderId = data.orderId
                    priceAmendment.stockLocationId = data.stockLocationId
                    priceAmendment.replacementId = data.replacementId
                    priceAmendment.userId = data.user

                    if (data.replacementId != null) {
                        priceAmendment.replacement = data.newPrice
                    }
                    else {
                        priceAmendment.variant = data.newPrice
                    }

                    priceAmendment.save()

                    savedItem.priceAmendment = priceAmendment
                    savedItem.save()

                    EventBus.handler.post(PriceChangedEvent(arrayListOf(savedItem)))
                }
            }
        }

        observable.subscribe(subscriber)
    }

    fun fetchCancelBookingReason(): Observable<GrabExpressCancelReasonResponse> {
        return observe(api().grabExpressCancelReason())
    }

    fun cancelGrabExpressOrder(orderNumber: String, cancelReason: Int): Observable<Response> {
        val payload = GrabExpressCancelReasonPayload().apply {
            this.cancelReason = cancelReason
        }

        return observe(api().cancelGrabExpressOrder(orderNumber, payload))
    }

    fun orderGrabExpress(orderNumber: String): Observable<Response> {
        return observe(api().orderGrabExpress(orderNumber))
    }

    fun changeGrabExpressOrderToHF(orderNumber: String): Observable<Response> {
        return observe(api().changeGrabExpressOrderToHF(orderNumber))
    }

    fun startChat(orderId: Long, channelUrl: String): Observable<Chat> {
        val payload = StartChatPayload().apply {
            this.channelUrl = channelUrl
        }

        return subscribe(api().startChat(orderId, payload)).map {
            val order = Order.findById(orderId)
            val chat = it.chat
            order?.let {
                order.chat = chat
                order.save()
            }

            chat
        }
    }

    fun finishChat(orderId: Long) {
        subscribe(api().endChat(orderId)).subscribe()
    }
}