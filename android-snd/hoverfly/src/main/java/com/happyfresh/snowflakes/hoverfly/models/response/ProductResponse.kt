package com.happyfresh.snowflakes.hoverfly.models.response

import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.models.Product

/**
 * Created by ifranseda on 5/6/15.
 */

class ProductResponse {

    @SerializedName("search_id")
    var searchId: Long = 0

    var products: List<Product>? = null
}