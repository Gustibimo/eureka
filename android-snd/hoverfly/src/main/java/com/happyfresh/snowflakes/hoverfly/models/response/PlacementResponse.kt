package com.happyfresh.snowflakes.hoverfly.models.response

import com.happyfresh.snowflakes.hoverfly.models.Placement
import java.util.*

/**
 * Created by aldi on 6/22/17.
 */

class PlacementResponse(var placements: List<Placement> = ArrayList())