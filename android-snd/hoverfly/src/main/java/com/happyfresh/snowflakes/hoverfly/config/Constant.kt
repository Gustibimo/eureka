package com.happyfresh.snowflakes.hoverfly.config

/**
 * Created by rsavianto on 1/15/15.
 */

object Constant {

    const val USER_ACCESS_TOKEN_KEY = "USER_ACCESS_TOKEN_KEY"
    const val USER_ID_KEY = "USER_ID_KEY"

    const val USER_NAME_KEY = "USER_NAME_KEY"
    const val USER_EMAIL_KEY = "USER_EMAIL_KEY"

    const val USER_NORMAL_ROLE = "USER_NORMAL_ROLE"
    const val USER_TYPE_KEY = "USER_TYPE_KEY"

    const val STORE_NAME_KEY = "STORE_NAME_KEY"
    const val STORE_LAT_KEY = "STORE_LAT_KEY"
    const val STORE_LON_KEY = "STORE_LON_KEY"

    const val ONE_CLICK_PAY_RETRY_COUNT = "ONE_CLICK_PAY_RETRY_COUNT"

    const val SHOPPER_PREP_NOTIFICATION = "SHOPPER_PREP_NOTIFICATION"
    const val DRIVER_PREP_NOTIFICATION = "DRIVER_PREP_NOTIFICATION"

    const val IS_STOCK_LOCATION_SELECTED = "IS_STOCK_LOCATION_SELECTED"
    const val STOCK_LOCATION_COUNTRY_ISO = "STOCK_LOCATION_COUNTRY_ISO"

    const val BATCH_ID = "BATCH_ID"
    const val EXTRAS_BATCH_ID = "EXTRAS_BATCH_ID"
    const val SHIPMENT_ID = "SHIPMENT_ID"
    const val LINE_ITEM_ID = "LINE_ITEM_ID"
    const val STOCK_LOCATION_ID = "STOCK_LOCATION_ID"
    const val STOCK_LOCATION_NAME = "STOCK_LOCATION_NAME"
    const val SELECTED_CLUSTER = "SELECTED_CLUSTER"
    const val SELECTED_SHOPPING_STORE = "SELECTED_SHOPPING_STORE"
    const val TOTAL_INVOICED_AMOUNT = "TOTAL_INVOICED_AMOUNT"

    const val CASH_HANDOVER_AMOUNT = "CASH_HANDOVER_AMOUNT"
    const val CASH_HANDOVER_CURRENCY = "CASH_HANDOVER_CURRENCY"

    const val CashOnDeliveryMethod = "Spree::PaymentMethod::Check"
    const val InvalidStateError = "Spree::Api::InvalidStateError"
    const val AlreadyFinishedException = "Spree::Job::AlreadyFinishedException"
    const val AlreadyStartedException = "Spree::Job::AlreadyStartedException"
    const val AlreadyCancelledException = "Spree::Job::AlreadyCancelledException"
    const val OrderCanceledException = "Spree::Job::OrderCanceledException"
    const val PaymentAlreadyCompleted = "payment_already_completed"
    const val UserIsClockedOutException = "Spree::Snd::UserIsClockedOutException"

    const val QUEUE_DIRNAME = "queue"
    const val FAILED_QUEUE_DIRNAME = "failed"
    const val QUEUE_FILENAME = "network_task_queue"

    const val REMOTE_NOTIFICATION_KEY = "com.happyfresh.snowflakes.hoverfly.REMOTE_NOTIFICATION"

    const val MENU_PAUSE = 1
    const val MENU_CLOCK_OUT = 2

    const val STORE_TYPE_NORMAL = "original"
    const val STORE_TYPE_SPECIALTY = "special"

    const val SUGGEST_REPLACEMENT = "by_call"
    const val REPLACE_FREELY = "by_shopper"
    const val DONT_REPLACE = "dont_replace"

    const val PAYMENT_REVIEW_LIST = 0
    const val SHOPPING_LIST = 1

    const val PERMISSIONS_REQUEST_CODE = 1 // This should be 8 bit, due to android restriction
    const val OVERLAY_PERMISSION_REQUEST_CODE = 1001

    // actual weight
    const val ACTUAL_WEIGHT_WARN_FACTOR = 0.1
    const val ACTUAL_WEIGHT_MAX_FACTOR = 0.5

    const val PUSH_MESSAGE = "push_message"
    const val USER_TOPIC = "user_"
    const val UPDATE_JOBS = "update_jobs"

    // product sample
    const val IS_OUT_OF_STORE = "is_out_of_store"

    // one click payment
    const val ORDER_NUMBER = "order_number"
    const val ONE_CLICK_PAY_RESULT = "OCP_result"
    const val IS_CC_PROMOTION_EXIST_AND_ELIGIBLE = "cc_promotion_exist_and_eligible"

    const val ONE_CLICK_PAY = 0
    const val ALREADY_COMPLETED = 1
    const val SWITCH_TO_COD = 2
    const val PAYMENT_COMPLETED = 3

    // price tag image
    const val PRICE_TAG_PATH = "price_tag_path"

    const val PRICE_TAG_FRAGMENT = 4
    const val WEIGHT_TAG_FRAGMENT = 5

    const val PENDING_HANDOVER_LIST = "pending_handover_list"

    const val SENDBIRD = "sendbird"

    //FleetType
    const val HF_FLEET_TYPE = "hf"
    const val GE_FLEET_TYPE = "ge"
    const val TPL_FLEET_TYPE = "tpl"

    enum class CanceledDialogType {
        SHOP,
        PAY,
        ACCEPT,
        DELIVER,
        ARRIVE,
        REJECT,
        FINISH,
        FAILED_DELIVERY,
        REPLACEMENT
    }

    enum class RefreshFragmentType {
        PAY,
        OOS
    }

    object PRESENCE {
        const val WORKING = "working"
        const val PAUSED = "paused"
        const val FINISHED = "finish"
    }

    object CONFIG_STATUS {
        const val PRESENCE_STATUS = "SND_STATUS"
        const val STOCK_LOCATION_ID = "SND_STOCK_LOCATION_ID"
        const val STOCK_LOCATION_NAME = "SND_STOCK_LOCATION_NAME"
        const val ASK_RECEIPT_NUMBER = "SND_ASK_RECEIPT_NUMBER"
        const val PRICE_CHANGED_WILL_UPDATE_PRICE = "SND_PRICE_CHANGED_WILL_UPDATE_PRICE"

        const val REQUIRE_AGE_VERIFICATION = "SND_STATUS_REQUIRE_AGE_VERIFICATION"
        const val AGE_VERIFICATION_WORDING = "SND_STATUS_AGE_VERIFICATION_WORDING"
        const val AGE_VERIFICATION_WORDING_ENGLISH = "SND_STATUS_AGE_VERIFICATION_WORDING_ENGLISH"
        const val RESTRICTED_CATEGORIES = "SND_STATUS_RESTRICTED_CATEGORIES"

        const val LAST_MODIFIED = "SND_LAST_MODIFIED"
        const val STORE_TYPE = "SND_STORE_TYPE"
    }

    object USER_ROLE {
        const val SHOPPER = "NORMALLY_SHOPPER"
        const val DRIVER = "NORMALLY_DRIVER"
    }

    object USER_TYPE {
        const val SHOPPER = "SHOPPER"
        const val DRIVER = "DRIVER"
    }

    const val IS_WORKING = "IS_WORKING"

    var GoogleTranslateURL = "https://translate.google.com/m/translate#auto/%1\$s/%2\$s"

    val TYPE_ID = "type_id"
}
