package com.happyfresh.snowflakes.hoverfly.models

import com.google.gson.annotations.SerializedName
import org.parceler.Parcel

/**
 * Created by rsavianto on 7/29/15.
 */

@Parcel
class Cluster {

    @SerializedName("id")
    var remoteId: Long = 0

    @SerializedName("name")
    var name: String? = null
}
