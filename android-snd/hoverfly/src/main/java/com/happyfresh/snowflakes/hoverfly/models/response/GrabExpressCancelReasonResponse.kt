package com.happyfresh.snowflakes.hoverfly.models.response

import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.models.GrabExpressCancelReason

/**
 * Created by kharda on 06/07/18.
 */
class GrabExpressCancelReasonResponse {

    @SerializedName("grab_cancel_reasons")
    var cancelReasons: List<GrabExpressCancelReason> = ArrayList()
}