package com.happyfresh.snowflakes.hoverfly.models.payload

/**
 * Created by ifranseda on 5/6/15.
 */

class CashHandoverPayload {
    var amount: Double? = null

    var receiver: String? = null
}
