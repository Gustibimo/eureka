package com.happyfresh.snowflakes.hoverfly.shared.events

/**
 * Created by kharda on 21/8/18
 */
class UnreadChatFetchedEvent(data: Long) : BaseEvent<Long>(data)