package com.happyfresh.snowflakes.hoverfly.models.payload

import com.google.gson.annotations.SerializedName

/**
 * Created by kharda on 9/6/16.
 */

class ListProductSamplePayload(
        @SerializedName("user_id")
        var userId: Long,

        @SerializedName("order_number")
        var orderNumber: String,

        @SerializedName("stock_location_id")
        var stockLocationId: Long,

        @SerializedName("items")
        var productSamplePayloads: List<ProductSamplePayload>) : SerializablePayload()
