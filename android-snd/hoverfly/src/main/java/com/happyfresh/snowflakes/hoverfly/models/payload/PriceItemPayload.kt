package com.happyfresh.snowflakes.hoverfly.models.payload

import com.google.gson.annotations.SerializedName

/**
 * Created by anton on 12/15/14.
 */

class PriceItemPayload : SerializablePayload() {

    @SerializedName("new_price")
    var newPrice: Double = 0.toDouble()

    @SerializedName("replacement_id")
    var replacementId: Long = 0
}
