package com.happyfresh.snowflakes.hoverfly.models.payload

import java.io.Serializable
import java.util.*

/**
 * Created by ifranseda on 4/7/15.
 */
abstract class SerializablePayload : Serializable {

    private val reqId: String? = null

    val id: String
        get() {
            if (reqId == null) {
                val rds = RandomString(8)
                return rds.nextString()
            }

            return reqId
        }

    class RandomString(length: Int) {

        private val random = Random()

        private val buf: CharArray

        init {
            if (length < 1) throw IllegalArgumentException("length < 1: " + length)
            buf = CharArray(length)
        }

        fun nextString(): String {
            for (idx in buf.indices) buf[idx] = symbols[random.nextInt(symbols.size)]
            return String(buf)
        }

        companion object {
            private val symbols: CharArray

            init {
                val tmp = StringBuilder()
                run {
                    var ch = '0'
                    while (ch <= '9') {
                        tmp.append(ch)
                        ++ch
                    }
                }

                var ch = 'a'
                while (ch <= 'z') {
                    tmp.append(ch)
                    ++ch
                }

                symbols = tmp.toString().toCharArray()
            }
        }
    }
}
