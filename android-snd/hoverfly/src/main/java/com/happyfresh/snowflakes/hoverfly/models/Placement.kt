package com.happyfresh.snowflakes.hoverfly.models

import com.google.gson.annotations.SerializedName
import org.parceler.Parcel

/**
 * Created by rsavianto on 2/9/15.
 */

@Parcel
class Placement {

    @SerializedName("id")
    var remoteId: Long? = null

    @SerializedName("user_id")
    var userId: Long? = null

    @SerializedName("stock_location_id")
    var stockLocationId: Long? = null

    @SerializedName("store_type")
    var storeType: String? = null

    @SerializedName("ops_pooling")
    var opsPooling: Boolean = false
}
