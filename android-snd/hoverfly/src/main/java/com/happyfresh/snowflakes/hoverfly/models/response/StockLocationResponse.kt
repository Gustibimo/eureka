package com.happyfresh.snowflakes.hoverfly.models.response

import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.models.StockLocation
import java.util.*

/**
 * Created by galuh on 6/18/17.
 */

class StockLocationResponse(
        @SerializedName("stock_locations")
        var stockLocations: List<StockLocation> = ArrayList())
