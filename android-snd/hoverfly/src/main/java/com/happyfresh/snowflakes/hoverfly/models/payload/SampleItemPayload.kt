package com.happyfresh.snowflakes.hoverfly.models.payload

import com.google.gson.annotations.SerializedName

/**
 * Created by numannaufal on 9/20/16.
 */
class SampleItemPayload(
        @SerializedName("product_id")
        var productId: Long,

        @SerializedName("item_quantity")
        var quantity: Int,

        @SerializedName("item_price")
        var price: Double,

        @SerializedName("is_out_of_stock")
        var isOutOfStock: Boolean,

        @Transient
        var campaignId: Int,

        @Transient
        var orderNumber: String,

        @Transient
        var ruleId: Int)
