package com.happyfresh.snowflakes.hoverfly.models

import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.shared.db.AppDatabase
import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import com.raizlabs.android.dbflow.annotation.Unique
import com.raizlabs.android.dbflow.sql.language.SQLite
import com.raizlabs.android.dbflow.structure.BaseModel
import org.parceler.Parcel
import java.io.Serializable

/**
 * Created by kharda on 23/8/18
 */
@Parcel
@Table(database = AppDatabase::class)
class Chat : BaseModel(), Serializable {

    @SerializedName("channel_url")
    @Unique
    @PrimaryKey
    @Column
    var channelUrl: String? = null

    @Column
    var status: String? = null

    @SerializedName("shopper_name")
    @Column
    var shopperName: String? = null

    @Column
    var unreadChatCount: Int? = null

    fun isActive() : Boolean {
        return status.equals(ACTIVE, ignoreCase = true)
    }

    companion object {

        const val ACTIVE = "active"

        @JvmStatic
        fun findByChannelUrl(channelUrl: String?): Chat? {
            return SQLite.select().from(Chat::class.java).where(Chat_Table.channelUrl.eq(channelUrl)).querySingle()
        }
    }
}