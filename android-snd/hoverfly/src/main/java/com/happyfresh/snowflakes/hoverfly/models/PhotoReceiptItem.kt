package com.happyfresh.snowflakes.hoverfly.models

import android.os.Parcel
import android.os.Parcelable

import java.io.Serializable

class PhotoReceiptItem : Serializable, Cloneable, Parcelable {
    var position: Int = 0
    var imagePath: String = ""

    constructor() : super()

    constructor(position: Int, imagePath: String) {
        this.position = position
        this.imagePath = imagePath
    }

    public override fun clone(): PhotoReceiptItem {
        return try {
            super.clone() as PhotoReceiptItem
        } catch (e: CloneNotSupportedException) {
            PhotoReceiptItem(this.position, this.imagePath)
        }

    }

    private constructor(`in`: Parcel) {
        position = `in`.readInt()
        imagePath = `in`.readString()
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(parcel: Parcel, i: Int) {
        parcel.writeInt(position)
        parcel.writeString(imagePath)
    }

    companion object {

        @JvmField
        val CREATOR: Parcelable.Creator<PhotoReceiptItem?> = object : Parcelable.Creator<PhotoReceiptItem?> {
            override fun createFromParcel(`in`: Parcel): PhotoReceiptItem {
                return PhotoReceiptItem(`in`)
            }

            override fun newArray(size: Int): Array<PhotoReceiptItem?> {
                return arrayOfNulls(size)
            }
        }
    }
}