package com.happyfresh.snowflakes.hoverfly.models

import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.models.payload.RejectionPayload
import com.happyfresh.snowflakes.hoverfly.shared.db.AppDatabase
import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.ForeignKey
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import com.raizlabs.android.dbflow.sql.language.SQLite
import com.raizlabs.android.dbflow.structure.BaseModel
import org.parceler.Parcel

/**
 * Created by ifranseda on 4/23/15.
 */

@Parcel
@Table(database = AppDatabase::class)
class RejectionItem : BaseModel(), Cloneable {

    @SerializedName("variant_id")
    @Column
    @PrimaryKey
    var variantId: Long? = null

    @Column
    var quantity: Int = 0

    @Column
    var reason: String? = null

    @Column
    @ForeignKey(stubbedRelationship = true)
    var item: LineItem? = null

    fun toPayload(): RejectionPayload {
        val payload = RejectionPayload()
        payload.variantId = variantId
        payload.quantity = quantity
        payload.reason = reason

        return payload
    }

    @Throws(CloneNotSupportedException::class) public override fun clone(): RejectionItem {
        return super.clone() as RejectionItem
    }

    companion object {
        @JvmStatic
        fun findByLineItemAndVariantId(lineItem: LineItem, variantId: Long?): RejectionItem? {
            return SQLite.select().from(RejectionItem::class.java)
                    .where(RejectionItem_Table.item_variantId.eq(lineItem.variantId))
                    .and(RejectionItem_Table.item_orderId.eq(lineItem.orderId))
                    .and(RejectionItem_Table.item_stockLocationId.eq(lineItem.stockLocationId))
                    .and(RejectionItem_Table.variantId.eq(variantId))
                    .querySingle()
        }
    }
}