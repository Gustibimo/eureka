package com.happyfresh.snowflakes.hoverfly.models

import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.shared.db.AppDatabase
import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import com.raizlabs.android.dbflow.annotation.Unique
import com.raizlabs.android.dbflow.structure.BaseModel
import org.parceler.Parcel
import java.util.*

/**
 * Created by ifranseda on 5/15/15.
 */

@Parcel
@Table(database = AppDatabase::class)
class Cash : BaseModel() {

    @SerializedName("id")
    @Column
    @Unique
    @PrimaryKey
    var remoteId: Long? = null

    @Column
    var balance: Double? = null

    @SerializedName("source_amount")
    @Column
    var sourceAmount: Double? = null

    @SerializedName("source_id")
    @Column
    var sourceId: Long? = null

    @SerializedName("source_type")
    @Column
    var sourceType: String? = null

    @SerializedName("order_number")
    @Column
    var orderNumber: String? = null

    @Column
    var currency: String? = null

    @SerializedName("created_at")
    @Column(name = "created_at")
    var created: Date? = null
}
