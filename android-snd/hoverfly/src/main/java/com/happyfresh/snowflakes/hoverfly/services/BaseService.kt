package com.happyfresh.snowflakes.hoverfly.services

import com.happyfresh.snowflakes.hoverfly.RestClient
import rx.Observable
import rx.Scheduler
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.concurrent.TimeUnit

/**
 * Created by ifranseda on 30/12/2016.
 */

abstract class BaseService<T> {

    protected var service: T? = null

    protected fun api(multipart: Boolean): T {
        if (multipart) {
            return provideMultipartService()
        }

        return api()
    }

    protected fun api(): T {
        if (service == null) {
            service = provideService()
        }

        return service!!
    }

    private fun provideService(): T {
        return RestClient.Service.create(serviceClass())
    }

    private fun provideMultipartService(): T {
        return RestClient.Service.createMultipart(serviceClass())
    }

    protected abstract fun serviceClass(): Class<T>

    protected fun <O> observe(observable: Observable<O>): Observable<O> {
        return observe(observable, AndroidSchedulers.mainThread())
    }

    protected fun <O> observe(observable: Observable<O>, observerThread: Scheduler): Observable<O> {
        return observable.debounce(1, TimeUnit.SECONDS, observerThread).observeOn(observerThread).subscribeOn(Schedulers.io())
    }

    protected fun <O> subscribe(observable: Observable<O>): Observable<O> {
        return observable.debounce(1, TimeUnit.SECONDS, Schedulers.io()).subscribeOn(Schedulers.io())
    }
}
