package com.happyfresh.snowflakes.hoverfly.models

import com.google.gson.annotations.SerializedName
import com.happyfresh.snowflakes.hoverfly.shared.db.AppDatabase
import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.ForeignKey
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import com.raizlabs.android.dbflow.structure.BaseModel
import org.parceler.Parcel

/**
 * Created by ifranseda on 8/19/15.
 */

@Parcel
@Table(database = AppDatabase::class)
class BatchState : BaseModel() {

    enum class State {
        AVAILABLE,
        BOOKED,
        STARTED,
        FINALIZING,
        PAYMENT,
        PICKED_UP,
        ACCEPTING,
        ACCEPTED,
        DELIVERING,
        COMPLETED
    }

    @Column
    @PrimaryKey(autoincrement = true)
    var id: Long? = 0

    @SerializedName("state_name")
    @Column(name = "state_name")
    var name: String? = null

    @Column
    var progress: Int? = null

    @Column
    var total: Int? = null

    @Column
    @ForeignKey
    var batch: Batch? = null

    val state: State
        get() {
            when (name) {
                BatchState.booked     -> return State.BOOKED
                BatchState.started    -> return State.STARTED
                BatchState.finalizing -> return State.FINALIZING
                BatchState.payment    -> return State.PAYMENT
                BatchState.pickedUp   -> return State.PICKED_UP
                BatchState.accepted   -> return if (progress == total) {
                    State.DELIVERING
                }
                else {
                    State.ACCEPTING
                }
                BatchState.delivering -> return if (progress == total) {
                    State.COMPLETED
                }
                else {
                    State.DELIVERING
                }
                BatchState.completed  -> return State.COMPLETED
                else                  -> return State.AVAILABLE
            }
        }

    fun shoppingInProgress(): Boolean {
        return (state == State.FINALIZING || state == State.PAYMENT)
    }

    fun deliveryInProgress(): Boolean {
        return (state == State.PICKED_UP || state == State.DELIVERING || state == State.ACCEPTING)
    }

    companion object {
        val booked = "booked"

        val started = "started"

        val finalizing = "finalizing"

        val payment = "payment"

        val pickedUp = "picked_up"

        val accepted = "accepted"

        val delivering = "delivering"

        val completed = "completed"
    }
}
