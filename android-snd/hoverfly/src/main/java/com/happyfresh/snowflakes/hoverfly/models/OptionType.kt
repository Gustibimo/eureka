package com.happyfresh.snowflakes.hoverfly.models

import com.google.gson.annotations.SerializedName
import org.parceler.Parcel

/**
 * Created by rsavianto on 11/15/14.
 */

@Parcel
class OptionType {

    @SerializedName("id")
    var remoteId: Long = 0
}
