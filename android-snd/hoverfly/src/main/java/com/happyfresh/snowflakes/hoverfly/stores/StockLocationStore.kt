package com.happyfresh.snowflakes.hoverfly.stores

import android.content.Context
import com.happyfresh.snowflakes.hoverfly.models.StockLocation

/**
 * Created by ifranseda on 19/07/2017.
 */
class StockLocationStore internal constructor(context: Context) : BaseStore(context) {

    companion object {
        const val IS_STOCK_LOCATION_SELECTED = "PREFS.IS_STOCK_LOCATION_SELECTED"
        const val STORE_NAME_KEY = "PREFS.STORE_NAME_KEY"
        const val STORE_NAME_ID = "PREFS.STORE_NAME_ID"
        const val STORE_LAT_KEY = "PREFS.STORE_LAT_KEY"
        const val STORE_LON_KEY = "PREFS.STORE_LON_KEY"
    }

    fun savePreferences(stockLocation: StockLocation) {
        sharedPrefEditor().putBoolean(IS_STOCK_LOCATION_SELECTED, true).putString(STORE_NAME_KEY, stockLocation.name!!).putFloat(STORE_LAT_KEY,
                stockLocation.location?.lat!!).putFloat(STORE_LON_KEY, stockLocation.location?.lon!!).commit()
    }

    var currentStoreName: String?
        get() = sharedPrefs().getString(STORE_NAME_KEY, null)
        set(value) {
            sharedPrefEditor().putString(STORE_NAME_KEY, value).commit()
        }
    var currentStoreId: Long
        get() = sharedPrefs().getLong(STORE_NAME_ID, 0)
        set(value) {
            sharedPrefEditor().putLong(STORE_NAME_ID, value).commit()
        }

    var storeLatitude: String?
        get() = sharedPrefs().getString(STORE_LAT_KEY, null)
        set(value) {
            sharedPrefEditor().putString(STORE_LAT_KEY, value).commit()
        }
    var storeLongitude: String?
        get() = sharedPrefs().getString(STORE_LON_KEY, null)
        set(value) {
            sharedPrefEditor().putString(STORE_LON_KEY, value).commit()
        }
}