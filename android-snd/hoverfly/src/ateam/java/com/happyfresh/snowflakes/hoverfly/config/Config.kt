package com.happyfresh.snowflakes.hoverfly.config

/**
 * Created by ifranseda on 24/12/2016.
 */

object Config {
    val Origin = "http://api-a-team.happyfresh.com"

    @JvmStatic
    val BaseUrl = Origin + "/api/"

    @JvmStatic
    val ClientToken = "a-team-ulsLYBgj5kkPEAPDsEtOh6sD02BiBMT/ZMvLpBPx"

    @JvmStatic
    val SegmentWriteKey = "JHHPiyvqTPcxNFTkgZSF22UeY6n3LlSq"

    @JvmStatic
    val StockCheckerUrl = Origin + "/snd/item_checklist"

    @JvmStatic
    val brazeSenderId = "264410800530"

    @JvmStatic
    val SendbirdAppId = "9C99B0C8-D6C0-4675-97C3-815C27858F9E"
}