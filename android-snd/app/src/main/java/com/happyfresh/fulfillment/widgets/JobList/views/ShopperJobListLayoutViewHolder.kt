package com.happyfresh.fulfillment.widgets.JobList.views

import android.annotation.SuppressLint
import android.content.Context
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import butterknife.BindView
import butterknife.ButterKnife
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.fulfillment.widgets.BaseRecyclerViewHolder
import com.happyfresh.snowflakes.hoverfly.config.Constant
import com.happyfresh.snowflakes.hoverfly.models.Batch
import com.happyfresh.snowflakes.hoverfly.models.Order
import com.happyfresh.snowflakes.hoverfly.models.Shipment
import com.happyfresh.snowflakes.hoverfly.utils.StringUtils
import org.jetbrains.anko.imageResource

/**
 * Created by galuh on 7/4/17.
 */

class ShopperJobListLayoutViewHolder(context: Context, itemView: View) : BaseRecyclerViewHolder<BasePresenter<*, *, *>, Batch>(context, itemView) {

    @JvmField
    @BindView(R.id.order_delivery_time)
    internal var orderDeliveryTime: TextView? = null

    @JvmField
    @BindView(R.id.order_size)
    internal var orderSize: TextView? = null

    @JvmField
    @BindView(R.id.view_all_orders_layout)
    internal var viewOrderListLayout: RelativeLayout? = null

    @JvmField
    @BindView(R.id.parent_order_item_view)
    internal var parentOrderItemView: ViewGroup? = null

    @JvmField
    @BindView(R.id.batch_id)
    internal var batchId: TextView? = null

    @JvmField
    @BindView(R.id.shopping_time)
    internal var shoppingTime: TextView? = null

    @JvmField
    @BindView(R.id.arrow_indicator)
    internal var arrowIndicator: ImageView? = null

    @JvmField
    @BindView(R.id.textview_toggle_all_order)
    internal var toggleAllOrderTextView: TextView? = null

    @JvmField
    @BindView(R.id.status_layout)
    internal var statusLayout: LinearLayout? = null

    @JvmField
    @BindView(R.id.view_shopping_list_button)
    internal var viewShoppingList: Button? = null

    @JvmField
    @BindView(R.id.shopping_in_progress)
    internal var shoppingInProgress: View? = null

    @JvmField
    @BindView(R.id.happycorporate_layout)
    internal var corporateFlag: View? = null

    private var orderItemView: View? = null

    private var orderNumber: TextView? = null

    private var orderName: TextView? = null

    private var happyCorporateFlag: TextView? = null

    private var shopperName: TextView? = null

    private var shopperNameLabel: TextView? = null

    private var shoppingBagFlagTextView: TextView? = null

    private var newCustomerFlagTextView: TextView? = null

    private var fleetTypeFlagTextView: TextView? = null

    private var paymentTypeFlagTextView: TextView? = null

    private var isLoading: Boolean = false
        set(value) {
            field = value
            setButtonStatus()
        }

    override fun bind(data: Batch) {
        setActionButtonListener(data)

        val showDeliveryTimeInOrderDetail = data.shipments().any { it.slot?.startTime != data.startTime }
        setAllData(data, showDeliveryTimeInOrderDetail)

        setButtonStatus()
    }

    private fun setActionButtonListener(data: Batch) {
        viewShoppingList?.setOnClickListener {
            onItemSelection?.onSelected(data)
        }

        viewOrderListLayout?.setOnClickListener {
            showOrderList()
        }
    }

    private fun showOrderList() {
        if (parentOrderItemView?.visibility == View.GONE) {
            arrowIndicator?.imageResource = R.drawable.icon_dropup
            toggleAllOrderTextView?.text = context.getString(R.string.label_close_all_orders)
            parentOrderItemView?.visibility = View.VISIBLE
        } else {
            arrowIndicator?.imageResource = R.drawable.icon_dropdown
            toggleAllOrderTextView?.text = context.getString(R.string.label_view_all_orders)
            parentOrderItemView?.visibility = View.GONE
        }
    }

    private fun setAllData(data: Batch, showDeliveryTimeInOrderDetail: Boolean) {
        setBatchData(data, showDeliveryTimeInOrderDetail)
        setOrderListData(data, showDeliveryTimeInOrderDetail)
    }

    @SuppressLint("SetTextI18n")
    private fun setBatchData(batch: Batch, showDeliveryTimeInOrderDetail: Boolean) {
        val slot = batch.slot
        var deliveryTimeText = StringUtils.showDeliveryTime(slot?.startTime!!, slot.endTime!!)
        if (showDeliveryTimeInOrderDetail) {
            deliveryTimeText = context.getString(R.string.see_detail)
        }
        orderDeliveryTime?.text = deliveryTimeText

        orderSize?.text = "${batch.remoteShipments.size}"

        batchId?.text = batch.remoteId.toString()

        shoppingTime?.text = StringUtils.showShoppingTime(batch.startTime!!, batch.endTime!!, batch.slot!!)

        shoppingInProgress?.visibility = if (batch.hasStartedShopping()) View.VISIBLE else View.GONE

        statusLayout?.visibility = if (batch.isActive) View.VISIBLE else View.GONE

        corporateFlag?.visibility = if (batch.hasCorporateOrder()) View.VISIBLE else View.GONE
    }

    private fun setOrderListData(batch: Batch, showDeliveryTimeInOrderDetail: Boolean) {
        orderItemView?.parent?.let { (orderItemView?.parent as ViewGroup).removeAllViews() }

        for ((x, shipment) in batch.remoteShipments.withIndex()) {
            orderItemView = LayoutInflater.from(context).inflate(R.layout.widget_job_list_order_item, parentOrderItemView, false)
            orderNumber = ButterKnife.findById(orderItemView!!, R.id.item_order_number)
            happyCorporateFlag = ButterKnife.findById(orderItemView!!, R.id.corporate_flag)
            shoppingBagFlagTextView = ButterKnife.findById(orderItemView!!, R.id.textview_shopping_bag_flag)
            newCustomerFlagTextView = ButterKnife.findById(orderItemView!!, R.id.textview_new_customer_flag)
            fleetTypeFlagTextView = ButterKnife.findById(orderItemView!!, R.id.textview_fleet_type_flag)
            paymentTypeFlagTextView = ButterKnife.findById(orderItemView!!, R.id.textview_payment_type_flag)

            shopperNameLabel = ButterKnife.findById(orderItemView!!, R.id.label_shopper_name)
            shopperName = ButterKnife.findById(orderItemView!!, R.id.shopper_name)

            if (showDeliveryTimeInOrderDetail) {
                shopperNameLabel?.text = context.getString(R.string.delivery_time)
                shopperNameLabel?.visibility = View.VISIBLE

                shopperName?.visibility = View.VISIBLE
                shopperName?.text = StringUtils.showDeliveryTime(shipment.slot?.startTime!!, shipment.slot?.endTime!!)
            } else {
                shopperNameLabel?.visibility = View.GONE

                shopperName?.text = null
                shopperName?.visibility = View.GONE
            }

            orderNumber?.text = shipment.order?.number

            happyCorporateFlag?.visibility = if (shipment.order?.companyId!! > 0) View.VISIBLE else View.GONE

            shoppingBagFlagTextView?.visibility = if (shipment.order?.isEligibleForShoppingBag!!) View.VISIBLE else View.GONE

            newCustomerFlagTextView?.visibility = if (shipment.order?.isOrderCompleteNotCanceled!!) View.VISIBLE else View.GONE

            fleetTypeFlagTextView?.text = getFleetType(shipment)

            if (shipment.order!!.paymentMethod.equals(Constant.CashOnDeliveryMethod)) {
                paymentTypeFlagTextView?.setCompoundDrawablesWithIntrinsicBounds(R.drawable.img_cod_large, 0, 0, 0)
                paymentTypeFlagTextView?.text = context.getString(R.string.cod)
            } else {
                paymentTypeFlagTextView?.setCompoundDrawablesWithIntrinsicBounds(R.drawable.img_debit_credit_large, 0, 0, 0)
                paymentTypeFlagTextView?.text = context.getString(R.string.credit_card)
            }

            if (x % 2 == 0) {
                orderItemView?.setBackgroundColor(context.resources.getColor(R.color.shady_background))
            }

            parentOrderItemView?.addView(orderItemView)
        }
    }

    private fun getFleetType(shipment: Shipment): String {
        return when (shipment.deliveryFleetType) {
            Constant.GE_FLEET_TYPE  -> context.getString(R.string.grab_express)
            Constant.TPL_FLEET_TYPE -> context.getString(R.string.tpl)
            else                    -> context.getString(R.string.hf_driver)
        }
    }

    private fun setButtonStatus() {
        viewShoppingList?.setTextColor(ContextCompat.getColor(context, R.color.shopper_button_background))
    }
}
