package com.happyfresh.fulfillment.modules.Tag

import android.content.Context
import android.os.Bundle
import com.happyfresh.fulfillment.abstracts.BasePresenter
import java.io.File

/**
 * Created by galuh on 11/24/17.
 */
abstract class TagPresenter<out T : TagInteractor<*>, out R : TagRouter, out V : TagViewBehavior>(context: Context) :
        BasePresenter<T, R, V>(context), TagInteractorOutput {

    init {
        interactor().context = context
    }

    fun openCamera() {
        interactor().openCamera()
    }

    fun processCameraOutput() {
        interactor().processCameraOutput()
    }

    fun prepareSubmitPhoto(bundle: Bundle?) {
        interactor().submitPhotoOutput(bundle)
    }

    override fun openCamera(tagFilePath: String?) {
        router().openCamera(tagFilePath)
    }

    override fun showErrorDialog(errorMessage: String) {
        view()?.showErrorDialog(errorMessage)
    }

    override fun showTagImage(imageFile: File) {
        view()?.showTagImage(imageFile)
    }

    override fun hideCameraIndicator() {
        view()?.hideCameraIndicator()
    }

    override fun enableSubmitButton(isEnabled: Boolean) {
        view()?.enableSubmitButton(isEnabled)
    }

    override fun submitPhotoTag(tagFilePath: String?, bundle: Bundle?) {
        router().trySubmitPhoto(tagFilePath, bundle)
    }
}