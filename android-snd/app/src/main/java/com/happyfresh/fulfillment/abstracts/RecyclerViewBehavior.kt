package com.happyfresh.fulfillment.abstracts

import com.happyfresh.fulfillment.widgets.BaseAdapter

/**
 * Created by ifranseda on 05/10/2017.
 */
interface RecyclerViewBehavior<in A : BaseAdapter<*, *>> : ViewBehavior {
    fun setAdapter(adapter: A)

    fun updateRefreshState(o: Boolean)
}
