package com.happyfresh.fulfillment.abstracts

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import com.happyfresh.fulfillment.modules.Launch.LaunchRouter

/**
 * Created by ifranseda on 10/02/2017.
 */
abstract class BaseRouter(protected var context: Context) {

    abstract fun detach()

    open fun viewClass(): Class<out BaseActivity<out BaseFragment<*>, *>>? {
        return null
    }

    @JvmOverloads
    fun start(bundle: Bundle? = null) {
        val intent = createIntent(bundle)
        this.context.startActivity(intent)
    }

    @JvmOverloads
    fun startClearTop(bundle: Bundle? = null) {
        val intent = createIntent(bundle)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK

        this.context.startActivity(intent)
    }

    @JvmOverloads
    fun startForResult(bundle: Bundle? = null, requestCode: Int) {
        val intent = createIntent(bundle)
        (this.context as Activity).startActivityForResult(intent, requestCode)
    }

    @JvmOverloads
    fun startFileProvider(action: String, extraName: String, extraUri: Uri, requestCode: Int) {
        val intent = createIntentFileProvider(action, extraName, extraUri)
        intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION

        (this.context as Activity).startActivityForResult(intent, requestCode)
    }

    fun backToLogin() {
        detach()

        val launchRouter = LaunchRouter(context)
        launchRouter.startRoot()
    }

    //region Private methods

    private fun createIntent(bundle: Bundle? = null): Intent {
        val intent = Intent(this.context, viewClass())
        if (bundle != null) {
            intent.putExtras(bundle)
        }

        return intent
    }

    private fun createIntentFileProvider(action: String, extraName: String, extraUri: Uri): Intent {
        val intent = Intent(action)
        intent.putExtra(extraName, extraUri)

        return intent
    }

    //endregion
}
