package com.happyfresh.fulfillment.widgets.Dialogs.ValueChangeDialog

/**
 * Created by galuh on 11/21/17.
 */
object Value {
    const val PRICE_CHANGE = "PRICE_CHANGE"
    const val WEIGHT_DIFFERENCE = "WEIGHT_DIFFERENCE"
}