package com.happyfresh.fulfillment.widgets.FinalizeList

import com.happyfresh.fulfillment.abstracts.BaseInteractor
import com.happyfresh.snowflakes.hoverfly.Sprinkles
import com.happyfresh.snowflakes.hoverfly.models.Batch
import com.happyfresh.snowflakes.hoverfly.models.LineItem
import com.happyfresh.snowflakes.hoverfly.models.ProductSampleItem
import com.happyfresh.snowflakes.hoverfly.models.Shipment
import com.happyfresh.snowflakes.hoverfly.services.BatchService
import com.happyfresh.snowflakes.hoverfly.shared.subscribers.ServiceSubscriber
import com.shopper.listeners.DataListener
import com.shopper.utils.SampleUtils
import java.util.*

/**
 * Created by ifranseda on 03/01/2018.
 */
class FinalizeListLayoutInteractor : BaseInteractor<FinalizeListLayoutInteractorOutput>() {
    private var batch: Batch? = null
        set(value) {
            field = value
            loadData()
        }

    fun setBatch(batchId: Long?) {
        this.batch = Batch.findById(batchId)
    }

    fun loadData() {
        if (shouldFetchShoppingList()) {
            fetchShoppingList()
        }
        else {
            drawFinalizeList()
        }
    }

    fun finalize(shipment: Shipment, position: Int) {
        val batchService = Sprinkles.service(BatchService::class.java)
        val observable = batchService.finalize(this.batch?.remoteId!!, shipment?.remoteId!!)
        val subscriber = object : ServiceSubscriber<Batch>() {
            override fun onFailure(e: Throwable) {

            }

            override fun onSuccess(data: Batch) {
                finalizeShopping()
            }
        }

        observable.subscribe(subscriber)
    }

    private fun shouldFetchShoppingList(): Boolean {
        this.batch?.stockLocation()?.remoteId?.let {
            return this.batch?.allItems(it)?.isEmpty() == true
        }

        return false
    }

    private fun fetchShoppingList() {
        val batchService = Sprinkles.service(BatchService::class.java)
        this.batch?.let {
            val observable = batchService.shoppingLineItems(it)
            val subscriber = object : ServiceSubscriber<List<LineItem>>() {
                override fun onFailure(e: Throwable) {

                }

                override fun onSuccess(data: List<LineItem>) {
                    drawFinalizeList()
                }
            }

            observe(observable).subscribe(subscriber)
        }
    }

    private fun drawFinalizeList() {
        val items = ArrayList<LineItem>()
        this.batch?.shipments()?.filterNot { it.isCancelled }?.forEach {
            items.addAll(it.outOfStockItems)
        }

        output().drawStockOutItems(items)
    }

    private fun finalizeShopping() {
        if (this.batch?.hasFinalizeUnavailableItems(this.batch?.stockLocation()?.remoteId) == true) {
            fetchProductSample()
        } else {
            output().reloadFinalizeList()
        }
    }

    private fun fetchProductSample() {
        var stockLocationId = this.batch?.stockLocation()?.remoteId!!
        SampleUtils().withBatch(this.batch).withStockLocation(this.batch?.stockLocation())
                .fetchProductSample(object : DataListener<Boolean>() {
                    override fun onCompleted(success: Boolean, data: Boolean?) {
                        if (success) {
                            if (ProductSampleItem.isThereInStoreSample(batch?.remoteId!!)) {
                                output().openProductSampleScreen(batch?.remoteId!!)
                            }
                            else {
                                output().openPaymentScreen(batch?.remoteId!!, stockLocationId)
                            }
                        }
                        else {
                            output().showAlertUnableCheckProductSample()
                        }

                        output().showProgress(false)
                    }

                    override fun onFailed(exception: Throwable?) {
                        output().openPaymentScreen(batch?.remoteId!!, stockLocationId)
                    }
                })
    }
}