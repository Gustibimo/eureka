package com.happyfresh.fulfillment.widgets.PendingHandoverList

import android.content.Context
import android.view.View
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.widgets.BaseAdapter
import com.happyfresh.fulfillment.widgets.PendingHandoverList.views.PendingHandoverListLayoutViewHolder
import com.happyfresh.snowflakes.hoverfly.models.PendingHandover

/**
 * Created by kharda on 28/06/18.
 */

class PendingHandoverListAdapter(context: Context, data: List<PendingHandover>) : BaseAdapter<PendingHandover, PendingHandoverListLayoutViewHolder>(context, data) {

    var listener: GrabExpressBookingListener? = null

    override fun itemLayoutResID(type: Int): Int {
        return R.layout.widget_pending_handover_list_item
    }

    override fun provideViewHolder(view: View, type: Int): PendingHandoverListLayoutViewHolder {
        return PendingHandoverListLayoutViewHolder(this.context, view, this.listener!!)
    }

    fun setBookingActionListener(listener: GrabExpressBookingListener) {
        this.listener = listener
    }

}