package com.happyfresh.fulfillment.widgets.FinalizeList

import com.happyfresh.fulfillment.abstracts.InteractorOutput
import com.happyfresh.snowflakes.hoverfly.models.LineItem

/**
 * Created by ifranseda on 03/01/2018.
 */
interface FinalizeListLayoutInteractorOutput : InteractorOutput {
    fun drawStockOutItems(items: List<LineItem>)

    fun showProgress(show: Boolean)

    fun showAlertUnableCheckProductSample()

    fun openPaymentScreen(batchId: Long, stockLocationId: Long)

    fun openProductSampleScreen(batchId: Long)

    fun reloadFinalizeList()
}