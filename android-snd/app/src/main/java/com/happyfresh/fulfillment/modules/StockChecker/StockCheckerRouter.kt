package com.happyfresh.fulfillment.modules.StockChecker

import android.app.Activity
import android.content.Context
import com.happyfresh.fulfillment.abstracts.BaseActivity
import com.happyfresh.fulfillment.abstracts.BaseFragment
import com.happyfresh.fulfillment.abstracts.BaseRouter

/**
 * Created by Novanto on 12/27/17.
 */

class StockCheckerRouter(context: Context) : BaseRouter(context) {
    override fun viewClass(): Class<out BaseActivity<BaseFragment<*>, *>>? {
        return StockCheckerActivity::class.java
    }

    override fun detach() {
        (context as Activity).finish()
    }
}