package com.happyfresh.fulfillment.widgets.JobList

import com.happyfresh.fulfillment.abstracts.RecyclerViewBehavior

/**
 * Created by galuh on 7/4/17.
 */

interface JobListLayoutViewBehavior : RecyclerViewBehavior<JobListAdapter> {

    fun unableLoadData()

    fun showEmpty()
}
