package com.happyfresh.fulfillment.widgets.StockChecker

import com.happyfresh.fulfillment.abstracts.ViewBehavior

/**
 * Created by Novanto on 12/27/17.
 */
interface StockCheckerViewBehavior : ViewBehavior
