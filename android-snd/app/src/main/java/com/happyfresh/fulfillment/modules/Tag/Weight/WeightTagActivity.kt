package com.happyfresh.fulfillment.modules.Tag.Weight

import android.os.Bundle
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.fulfillment.modules.Tag.TagActivity

/**
 * Created by galuh on 11/24/17.
 */
class WeightTagActivity : TagActivity<WeightTagFragment, BasePresenter<*, *, *>>() {
    override fun title(): String? {
        return getString(R.string.action_weight_difference)
    }

    override fun provideFragment(bundle: Bundle?): WeightTagFragment? {
        return WeightTagFragment().apply { arguments = bundle }
    }
}