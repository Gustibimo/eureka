package com.happyfresh.fulfillment.modules.LineItem.behaviors

import com.happyfresh.fulfillment.abstracts.ViewBehavior
import com.happyfresh.snowflakes.hoverfly.models.LineItem

interface LineItemDetailViewBehavior : ViewBehavior {

    fun showLineItem(lineItem: LineItem)
}