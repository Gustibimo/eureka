package com.happyfresh.fulfillment.widgets.StockChecker

import android.app.Activity
import android.content.Context
import android.os.Bundle
import com.happyfresh.fulfillment.abstracts.BaseRouter
import com.happyfresh.fulfillment.modules.StockChecker.StockCheckerRouter

/**
 * Created by Novanto on 12/27/17.
 */
class StockCheckerRouter (context: Context) : BaseRouter(context) {
    private val manualStockCheckerRouter by lazy { StockCheckerRouter(context) }

    override fun detach() {
        (context as Activity).finish()
    }

    fun openStockChecker() {
        manualStockCheckerRouter.start(Bundle())
    }
}