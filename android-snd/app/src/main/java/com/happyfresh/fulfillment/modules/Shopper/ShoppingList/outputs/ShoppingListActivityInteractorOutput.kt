package com.happyfresh.fulfillment.modules.Shopper.ShoppingList.outputs

/**
 * Created by ifranseda on 09/10/2017.
 */

interface ShoppingListActivityInteractorOutput : ShoppingListInteractorOutput {

    fun showName(name: String)

    fun showImage(image: String?)

    fun showAddress(address: String)

    fun toggleStartButton(show: Boolean)

    fun toggleChatButton(show: Boolean)

    fun hideCancelMenu()

    fun returnToBatchList()

    fun failedToStart()

    fun pleaseTryAgain()

    fun setTimer(time: String)

    fun openShopperChatList(batchId: Long)

    fun showChatBadge(count: Int)
}