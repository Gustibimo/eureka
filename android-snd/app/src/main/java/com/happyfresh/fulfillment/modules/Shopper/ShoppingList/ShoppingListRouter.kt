package com.happyfresh.fulfillment.modules.Shopper.ShoppingList

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.happyfresh.fulfillment.abstracts.BaseActivity
import com.happyfresh.fulfillment.abstracts.BaseFragment
import com.happyfresh.fulfillment.abstracts.BaseRouter
import com.happyfresh.fulfillment.modules.Shopper.ChatList.ShopperChatListRouter
import com.happyfresh.fulfillment.modules.Shopper.JobList.ShopperJobListRouter
import com.happyfresh.fulfillment.modules.StockOut.StockOutRouter
import com.happyfresh.snowflakes.hoverfly.models.Batch
import com.shopper.activities.BatchPaymentActivity
import com.shopper.activities.ProductSampleListActivity
import com.shopper.common.Constant.BATCH_ID
import com.shopper.common.Constant.STOCK_LOCATION_ID
import org.parceler.Parcels

/**
 * Created by galuh on 9/27/17.
 */
class ShoppingListRouter(context: Context) : BaseRouter(context) {

    private val jobListRouter by lazy { ShopperJobListRouter(context) }

    private val stockOutRouter by lazy { StockOutRouter(context) }

    private val shopperChatListRouter by lazy { ShopperChatListRouter(context) }

    override fun detach() {
        (context as Activity).finish()
    }

    override fun viewClass(): Class<out BaseActivity<BaseFragment<*>, *>>? {
        return ShoppingListActivity::class.java
    }

    fun switchToBatchList() {
        jobListRouter.startClearTop()
    }

    fun startWithBatch(batch: Batch) {
        val bundle = getBundleBatch(batch)
        start(bundle)
    }

    fun startWithBatchClearTop(batch: Batch) {
        val bundle = getBundleBatch(batch)
        startClearTop(bundle)
    }

    private fun getBundleBatch(batch: Batch) : Bundle {
        val bundle = Bundle()
        bundle.putParcelable(SELECTED_BATCH, Parcels.wrap(batch))
        bundle.putLong(STOCK_LOCATION_ID, batch.stockLocation().remoteId)

        return bundle
    }

    fun finalizeBatch(batch: Batch) {
        batch.remoteId?.let {
            stockOutRouter.startWithBatchID(it)
        }
    }

    companion object {
        val SELECTED_BATCH: String = "ShoppingListRouter.SELECTED_BATCH"
    }

    fun openPaymentScreen(batchId: Long, stockLocationId: Long) {
        val intent = Intent(context, BatchPaymentActivity::class.java)
        intent.putExtra(BATCH_ID, batchId)
        intent.putExtra(STOCK_LOCATION_ID, stockLocationId)
        intent.putExtra(ProductSampleListActivity.SELECTED_BATCH, batchId)

        context.startActivity(intent)
    }

    fun openProductSampleScreen(batchId: Long) {
        val intent = Intent(context, ProductSampleListActivity::class.java)
        intent.putExtra(ProductSampleListActivity.SELECTED_BATCH, batchId)

        context.startActivity(intent)
    }

    fun openShopperChatList(batchId: Long) {
        shopperChatListRouter.startWithBatchId(batchId)
    }
}