package com.happyfresh.fulfillment.modules.ChooseStore

import com.happyfresh.fulfillment.abstracts.InteractorOutput
import com.happyfresh.snowflakes.hoverfly.models.StockLocation

/**
 * Created by galuh on 6/12/17.
 */

interface ChooseStoreInteractorOutput : InteractorOutput {

    fun drawSignalErrorGettingLocation()

    fun drawNearbyStores(stores: List<StockLocation>)

    fun updateNearbyStores(stores: List<StockLocation>)

    fun drawEmptyStores()

    fun drawNoStoreSelected()

    fun drawClockInDialog(stockLocation: StockLocation)

    fun drawSelectStoreButtonEnabled()

    fun drawSelectStoreButtonDisabled()

    fun drawStatusIsWorking()

    fun openShoppingJobList(stockLocationId: Long?)

    fun openDriverJobList()
}
