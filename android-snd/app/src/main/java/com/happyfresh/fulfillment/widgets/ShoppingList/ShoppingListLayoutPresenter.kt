package com.happyfresh.fulfillment.widgets.ShoppingList

import android.content.Context
import android.widget.Toast
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.fulfillment.widgets.ViewHolder.OnItemSelection
import com.happyfresh.snowflakes.hoverfly.models.Batch
import com.happyfresh.snowflakes.hoverfly.models.LineItem

/**
 * Created by galuh on 10/2/17.
 */

class ShoppingListLayoutPresenter(context: Context) :
        BasePresenter<ShoppingListLayoutInteractor, ShoppingListLayoutRouter, ShoppingListLayoutViewBehavior>(context),
        ShoppingListLayoutInteractorOutput {
    private var adapter: ShoppingListLayoutAdapter? = null

    private var itemList: MutableList<LineItem>? = null

    private val onItemSelection = object : OnItemSelection<LineItem> {
        override fun onSelected(data: LineItem) {
            val job = data.shipment?.shoppingJob

            if (job != null && !job?.stillShopping()) {
                Toast.makeText(context, context.getString(R.string.shopping_job_is_finalized), Toast.LENGTH_LONG)
                        .show()
            } else if (Batch.Companion.currentBatch()?.hasStartedShopping()!!) {
                openItemDetail(data)
            }
        }
    }

    override fun provideInteractor(): ShoppingListLayoutInteractor? {
        return ShoppingListLayoutInteractor()
    }

    override fun provideRouter(context: Context): ShoppingListLayoutRouter? {
        return ShoppingListLayoutRouter(context)
    }

    override fun prepareShoppingList(items: List<LineItem>?) {
        this.itemList = items as MutableList<LineItem>

        if (this.adapter != null) {
            this.adapter?.addAll(this.itemList!!)
        } else {
            this.adapter = ShoppingListLayoutAdapter(context, this.itemList!!)

            this.adapter?.setOnItemSelection(onItemSelection)

            view()?.setAdapter(this.adapter!!)
        }

        view()?.showEmptyContainer(items.isEmpty())
        view()?.updateRefreshState(false)
    }

    override fun clearShoppingList() {
        this.itemList?.clear()
        this.adapter?.notifyDataSetChanged()
    }

    override fun shoppingNotInProgress(o: Boolean) {
        if (o) {
            view()?.showOverlay()
        }
        else {
            view()?.hideOverlay()
        }
    }

    override fun showToast(message: String) {
        view()?.showToast(message)
    }

    //region Public methods

    fun setData(batch: Batch) {
        interactor().batch = batch
    }

    fun updateShoppingList() {
        interactor().fetchShoppingList()
    }

    fun openItemDetail(item: LineItem) {
        router().openItemDetail(item)
    }

    //endregion
}