package com.happyfresh.fulfillment.modules.Shopper.ChatList

import android.app.Activity
import android.content.Context
import android.os.Bundle
import com.happyfresh.fulfillment.abstracts.BaseActivity
import com.happyfresh.fulfillment.abstracts.BaseFragment
import com.happyfresh.fulfillment.abstracts.BaseRouter
import com.happyfresh.happychat.ChatRouter
import com.happyfresh.snowflakes.hoverfly.config.Constant
import com.happyfresh.snowflakes.hoverfly.models.Shipment
import com.happyfresh.snowflakes.hoverfly.utils.ChatUtils

/**
 * Created by kharda on 15/8/18
 */
class ShopperChatListRouter(context: Context) : BaseRouter(context) {

    private val chatRouter by lazy { ChatRouter(context) }

    override fun detach() {
        (context as Activity).finish()
    }

    override fun viewClass(): Class<out BaseActivity<out BaseFragment<*>, *>>? {
        return ShopperChatListActivity::class.java
    }

    fun startWithBatchId(batchId: Long) {
        val bundle = Bundle()
        bundle.putLong(Constant.BATCH_ID, batchId)
        start(bundle)
    }

    fun openChatScreen(shopperId: String, shipment: Shipment) {
        ChatUtils.openChatScreen(context, shopperId, shipment)
    }
}