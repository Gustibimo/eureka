package com.happyfresh.fulfillment.widgets

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.RelativeLayout
import butterknife.ButterKnife
import com.happyfresh.snowflakes.hoverfly.shared.events.EventBus
import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.fulfillment.abstracts.ViewBehavior

/**
 * Created by ifranseda on 01/11/2016.
 */

abstract class BaseLayout<out P : BasePresenter<*, *, *>>
@JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : RelativeLayout(context, attrs, defStyleAttr),
        ViewBehavior {

    private var presenter: P? = null

    init {
        if (attrs != null) {
            initializeAttr(attrs, defStyleAttr)
        }

        initializeLayout()
        initialize()
    }

    protected abstract fun layoutResID(): Int

    open protected fun initializeLayout() {
        val resourceLayout = layoutResID()

        if (resourceLayout > 0) {
            val layoutInflater = LayoutInflater.from(this.context)
            layoutInflater.inflate(resourceLayout, this)

            ButterKnife.bind(this)
        }
    }

    open protected fun initializeAttr(attrs: AttributeSet) = this.initializeAttr(attrs, 0)

    open protected fun initializeAttr(attrs: AttributeSet, defStyleAttr: Int) {}

    open protected fun initialize() { }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()

        this.subscribe()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()

        this.unsubscribe()
    }

    protected fun presenter(): P {
        if (presenter == null) {
            presenter = providePresenter()
            presenter?.setView(this)
        }

        return presenter as P
    }

    abstract protected fun providePresenter(): P?

    open protected fun isSubscriber(): Boolean {
        return false
    }

    private fun subscribe() {
        if (isSubscriber()) {
            EventBus.handler.register(this)
        }
    }

    private fun unsubscribe() {
        if (isSubscriber()) {
            try {
                EventBus.handler.unregister(this)
            }
            catch (e: IllegalArgumentException) {
                e.printStackTrace()
            }
        }
    }
}
