package com.happyfresh.fulfillment.widgets.PendingHandoverList.views

import android.content.Context
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.OnClick
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.fulfillment.widgets.BaseRecyclerViewHolder
import com.happyfresh.fulfillment.widgets.Dialogs.PhoneCallDialog.PhoneCallDialog
import com.happyfresh.fulfillment.widgets.PendingHandoverList.GrabExpressBookingListener
import com.happyfresh.snowflakes.hoverfly.models.PendingHandover
import com.shopper.views.CircleImageView
import com.squareup.picasso.Picasso
import org.jetbrains.anko.backgroundColor

/**
 * Created by kharda on 28/06/18.
 */

class PendingHandoverListLayoutViewHolder(context: Context, itemView: View, var listener: GrabExpressBookingListener) : BaseRecyclerViewHolder<BasePresenter<*, *, *>, PendingHandover>(context, itemView) {


    @JvmField
    @BindView(R.id.order_indicator_icon)
    internal var orderIndicatorIcon: CircleImageView? = null

    @JvmField
    @BindView(R.id.order_indicator_number)
    internal var orderNumber: TextView? = null

    @JvmField
    @BindView(R.id.pending_handover_status)
    internal var status: TextView? = null

    @JvmField
    @BindView(R.id.pending_handover_driver)
    internal var driver: TextView? = null

    @JvmField
    @BindView(R.id.pending_handover_phone_number)
    internal var phoneNumber: TextView? = null

    @JvmField
    @BindView(R.id.pending_handover_pin)
    internal var pin: TextView? = null

    @JvmField
    @BindView(R.id.pending_handover_driver_photo)
    internal var driverPhoto: ImageView? = null

    @JvmField
    @BindView(R.id.pending_handover_delivery_slot)
    internal var deliverySlot: TextView? = null

    @JvmField
    @BindView(R.id.pending_handover_booking_time)
    internal var bookingTime: TextView? = null

    @JvmField
    @BindView(R.id.pending_handover_cancel_button)
    internal var cancelButton: Button? = null

    @JvmField
    @BindView(R.id.pending_handover_booking_button)
    internal var bookingButton: Button? = null

    @JvmField
    @BindView(R.id.pending_handover_change_to_hf_button)
    internal var changeToHFButton: Button? = null

    override fun bind(data: PendingHandover) {
        if (data.isActive()) {
            cancelButton?.isEnabled = true
            bookingButton?.isEnabled = false
            changeToHFButton?.isEnabled = false
        } else {
            cancelButton?.isEnabled = false
            bookingButton?.isEnabled = true
            changeToHFButton?.isEnabled = true
        }

        orderIndicatorIcon?.backgroundColor = data.indicatorColor!!
        orderNumber?.text = data.orderNumber ?: "-"
        status?.text = data.status ?: "-"
        driver?.text = data.driverName ?: "-"
        phoneNumber?.text = data.driverPhone ?: "-"
        pin?.text = data.pin ?: "-"
        Picasso.with(context).load(data.driverPhotoUrl).placeholder(R.drawable.icon_default_person).into(driverPhoto)
        deliverySlot?.text = data.getSlotTimeDisplay() ?: "-"
        bookingTime?.text = data.getBookingTIme() ?: "-"

        if (data.driverName != null && data.driverPhone != null) {
            phoneNumber?.setOnClickListener { callDriver(data.driverName!!, data.driverPhone!!) }
        }
    }

    private fun callDriver(driverName: String, driverPhone: String) {
        val phoneCallDialog = PhoneCallDialog(context, R.string.action_call_driver, R.string.alert_message_call_customer, driverName, driverPhone)
        phoneCallDialog.show()
    }

    @OnClick(R.id.pending_handover_cancel_button)
    fun cancel() {
        listener.onCancelBooking(orderNumber?.text.toString())
    }

    @OnClick(R.id.pending_handover_booking_button)
    fun book() {
        listener.onCreateBooking(orderNumber?.text.toString())
    }

    @OnClick(R.id.pending_handover_change_to_hf_button)
    fun changeToHF() {
        listener.onChangeBookingToHF(orderNumber?.text.toString())
    }
}