package com.happyfresh.fulfillment.modules.Shopper.ShoppingList.interactors

import android.os.Handler
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.happyfresh.fulfillment.modules.Shopper.ShoppingList.interactors.ShoppingListActivityInteractor.ShoppingProcessEvent.Step
import com.happyfresh.fulfillment.modules.Shopper.ShoppingList.interactors.ShoppingListActivityInteractor.ShoppingProcessEvent.Step.*
import com.happyfresh.fulfillment.modules.Shopper.ShoppingList.outputs.ShoppingListActivityInteractorOutput
import com.happyfresh.snowflakes.hoverfly.Sprinkles
import com.happyfresh.snowflakes.hoverfly.models.Batch
import com.happyfresh.snowflakes.hoverfly.models.response.ExceptionResponse
import com.happyfresh.snowflakes.hoverfly.shared.events.*
import com.happyfresh.snowflakes.hoverfly.shared.subscribers.ServiceSubscriber
import com.happyfresh.snowflakes.hoverfly.stores.ShoppingStore
import com.happyfresh.snowflakes.hoverfly.stores.UserStore
import com.happyfresh.snowflakes.hoverfly.utils.ChatUtils
import com.sendbird.android.SendBird
import com.shopper.events.StartingBatchEvent
import com.squareup.otto.Subscribe
import retrofit2.adapter.rxjava.HttpException

/**
 * Created by ifranseda on 09/10/2017.
 */

class ShoppingListActivityInteractor : ShoppingListInteractor<ShoppingListActivityInteractorOutput>() {

    private val CONNECTION_HANDLER_ID = "CONNECTION_HANDLER_SHOPPING_LIST_ACTIVITY_INTERACTOR"

    private val CHANNEL_HANDLER_ID = "CHANNEL_HANDLER_SHOPPING_LIST_ACTIVITY_INTERACTOR"

    private val userStore = Sprinkles.stores(UserStore::class.java)

    var startTime: Long = 0

    val timerHandler = Handler()

    val timerRunnable: Runnable by lazy {
        Runnable {
            val millis = System.currentTimeMillis() - startTime
            var seconds = (millis / 1000)
            val minutes = seconds / 60
            seconds %= 60

            output().setTimer(String.format("%d:%02d", minutes, seconds))

            timerHandler.postDelayed(timerRunnable, 500)
        }
    }

    val shoppingStore: ShoppingStore by lazy {
        Sprinkles.stores(ShoppingStore::class.java) as ShoppingStore
    }

    override fun shouldListenEventBus(): Boolean {
        return true
    }

    fun prepareContents() {
        batch?.stockLocation()?.let {
            output().showName(it.name.orEmpty())
            output().showImage(it.storePhoto)
            output().showAddress(it.address1 + " " + it.address2)
        }
    }

    fun startBatch() {
        showLoading()

        batch?.let {
            val subscriber = object : ServiceSubscriber<Batch>() {
                override fun onFailure(e: Throwable) {
                    try {
                        val jsonString = (e as HttpException).response().errorBody().string()
                        val exceptionResponse = Gson().fromJson(jsonString, ExceptionResponse::class.java)

                        if ("There are orders still processing".equals(exceptionResponse.exception!!, ignoreCase = true)) {
                            output().pleaseTryAgain()
                        } else {
                            output().failedToStart()
                        }
                    } catch (e: Exception) {
                        output().failedToStart()
                    }

                    loadingCompleted()
                }

                override fun onSuccess(data: Batch) {
                    batch = data

                    shouldShowShopAndChatButton()
                    shouldHideCancelMenu()

                    val startingBatchEvent = StartingBatchEvent(data)
                    EventBus.handler.post(startingBatchEvent)

                    loadingPartiallyCompleted()
                }
            }

            batchService.start(it.remoteId!!).subscribe(subscriber)?.let {
                subscriptions().add(it)
            }
        }
    }

    fun submitCancelBatch() {
        showLoading()

        batch?.let {
            val subscriber = object : ServiceSubscriber<Batch>() {
                override fun onFailure(e: Throwable) {
                    output().returnToBatchList()
                }

                override fun onSuccess(data: Batch) {
                    batch = data

                    output().returnToBatchList()
                    loadingCompleted()
                }
            }

            batchService.cancel(it.remoteId!!).subscribe(subscriber)?.let {
                subscriptions().add(it)
            }
        }
    }

    fun shouldShowShopAndChatButton() {
        output().toggleStartButton(!(batch?.hasStartedShopping() ?: false))
        output().toggleChatButton((batch?.hasStartedShopping() ?: false) && batch?.isChatEnabled()!!)
    }

    fun shouldHideCancelMenu() {
        batch?.shipments()?.let {shipments ->
            for (shipment in shipments) {
                if (shipment.order?.chat?.isActive() == true) {
                    output().hideCancelMenu()
                    return
                }
            }
        }
    }

    fun startTimer() {
        startTime = shoppingStore.getShoppingTimer(batchId)
        if (startTime == 0L) {
            startTime = System.currentTimeMillis()
            shoppingStore.setShoppingTimer(batchId, startTime)
        }

        timerHandler.postDelayed(timerRunnable, 0)
    }

    fun stopTimer() {
        shoppingStore.setShoppingTimer(batchId, 0L)
        timerHandler.removeCallbacks(timerRunnable)
    }

    fun prepareTimer(): Boolean {
        if (batch?.hasStartedShopping() == false) {
            return false
        }

        startTimer()
        return true
    }

    fun prepareOpenShopperChatList() {
        output().openShopperChatList(batchId)
    }

    fun addChatHandler() {
        val shopperId = userStore?.userId.toString()

        ChatUtils.addChatHandler(batchId, CONNECTION_HANDLER_ID, CHANNEL_HANDLER_ID, shopperId)
    }

    fun removeChatHandler() {
        ChatUtils.removeChatHandler(CONNECTION_HANDLER_ID, CHANNEL_HANDLER_ID)
    }

    fun disconnectChat() {
        SendBird.disconnect { }
    }

    private fun loadingCompleted() {
        EventBus.handler.post(ShoppingProcessEvent(COMPLETED))
    }

    private fun loadingPartiallyCompleted() {
        EventBus.handler.post(ShoppingProcessEvent(PARTIALLY_COMPLETED))
    }

    private fun showLoading() {
        EventBus.handler.post(ShoppingProcessEvent(LOADING))
    }

    class ShoppingProcessEvent(e: Step) : BaseEvent<Step>(e) {

        enum class Step {
            LOADING,
            COMPLETED,
            PARTIALLY_COMPLETED
        }
    }

    @Subscribe
    fun onSendbirdConnected(event: SendbirdConnectedEvent) {
        FirebaseInstanceId.getInstance().token?.let {
            SendBird.registerPushTokenForCurrentUser(it) { _, _ -> }
        }
    }

    @Subscribe
    fun onUnreadChatFetched(event: UnreadChatFetchedEvent) {
        if (batchId == event.data) {
            val batch = Batch.findById(event.data)
            batch?.let {
                output().showChatBadge(batch.unreadChatMessageCount())
            }
        }
    }

    @Subscribe
    fun onChatStarted(event: ChatStartedEvent) {
        output().hideCancelMenu()
    }
}