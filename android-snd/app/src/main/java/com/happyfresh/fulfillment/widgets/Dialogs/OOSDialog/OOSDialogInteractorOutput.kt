package com.happyfresh.fulfillment.widgets.Dialogs.OOSDialog

import com.happyfresh.fulfillment.abstracts.InteractorOutput
import com.happyfresh.snowflakes.hoverfly.models.FeedbackType

/**
 * Created by galuh on 11/10/17.
 */
interface OOSDialogInteractorOutput : InteractorOutput {

    fun oosFeedbackListSuccess(feedbackTypes: List<FeedbackType>)

    fun oosFeedbackListFailed()
}