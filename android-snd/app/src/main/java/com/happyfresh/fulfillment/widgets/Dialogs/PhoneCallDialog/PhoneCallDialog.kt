package com.happyfresh.fulfillment.widgets.Dialogs.PhoneCallDialog

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.text.Html
import com.afollestad.materialdialogs.MaterialDialog
import com.happyfresh.fulfillment.R
import com.happyfresh.snowflakes.hoverfly.utils.PermissionUtils

/**
 * Created by kharda on 18/7/18
 */
class PhoneCallDialog(context: Context, title: Int, message: Int, name: String?, phoneNumber: String?) {

    var dialogBuilder: MaterialDialog.Builder? = null

    init {
        dialogBuilder = MaterialDialog.Builder(context)
        dialogBuilder!!.title(context.getString(title))

        val content: String? = context.getString(message, name, phoneNumber)
        dialogBuilder!!
                .content(Html.fromHtml(content))
                .negativeText(R.string.alert_button_cancel)
                .positiveText(R.string.alert_button_call)
                .onPositive { _, _ -> callPhone(context, phoneNumber) }
    }

    fun show() {
        dialogBuilder?.show()
    }

    @SuppressLint("MissingPermission")
    private fun callPhone(context: Context, number: String?) {
        number?.let {
            val uri = "tel:" + it

            val intent = Intent(Intent.ACTION_CALL)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            intent.data = Uri.parse(uri)

            if (PermissionUtils.callPhone(context)) {
                context.startActivity(intent)
                return
            }
        }
    }
}