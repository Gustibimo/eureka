package com.happyfresh.fulfillment.widgets.Dialogs.ItemFoundDialog.view

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.LayerDrawable
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.util.AttributeSet
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.CheckBox
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import com.afollestad.materialdialogs.MaterialDialog
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.fulfillment.widgets.BaseLayout
import com.happyfresh.fulfillment.widgets.Dialogs.ItemFoundDialog.TextChangedListener
import com.happyfresh.snowflakes.hoverfly.models.LineItem
import com.shopper.common.Constant
import org.apache.commons.lang3.StringUtils
import org.jetbrains.anko.imageResource
import org.jetbrains.anko.textColor

/**
 * Created by galuh on 10/26/17.
 */
class ItemFoundDialogLayout @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
        BaseLayout<BasePresenter<*, *, *>>(context, attrs, defStyleAttr) {

    @JvmField
    @BindView(R.id.item_actual)
    var itemActualContainer: View? = null

    @JvmField
    @BindView(R.id.item_found_count)
    var itemCounter: EditText? = null

    @JvmField
    @BindView(R.id.item_actual_weight)
    var itemActualWeight: EditText? = null

    @JvmField
    @BindView(R.id.item_found_max_amount)
    var amountText: TextView? = null

    @JvmField
    @BindView(R.id.item_expected_weight)
    var expectedText: TextView? = null

    @JvmField
    @BindView(R.id.item_error_message)
    var errorLayout: View? = null

    @JvmField
    @BindView(R.id.item_error_message_text)
    var errorMessage: TextView? = null

    @JvmField
    @BindView(R.id.item_error_message_icon)
    var errorIcon: ImageView? = null

    @JvmField
    @BindView(R.id.item_shopper_note)
    var itemShopperNoteContainer: View? = null

    @JvmField
    @BindView(R.id.item_shopper_note_checkbox)
    var shopperNoteCheckbox: CheckBox? = null

    private var shopperNoteFulfilled: Boolean = false

    override fun layoutResID(): Int {
        return R.layout.dialog_item_found
    }

    override fun providePresenter(): BasePresenter<*, *, *>? {
        return null
    }

    var itemActualWeightBackground = (itemActualWeight?.background as LayerDrawable).findDrawableByLayerId(R.id.line) as GradientDrawable

    lateinit var lineItem: LineItem

    fun show(callback: FoundItemCallback) {
        val builder = MaterialDialog.Builder(context)

        builder.title(R.string.alert_title_item_found)
        builder.customView(this, false)

        builder.negativeText(R.string.alert_button_cancel)
        builder.positiveText(R.string.alert_button_ok)

        builder.onNegative { dialog, _ ->
            itemActualWeightBackground.setStroke(1, Color.BLACK)
            dialog.dismiss()
        }

        builder.onPositive { dialog, _ ->
            var foundCounter = -1

            var actualWeight: Double? = null
            var actualWeightIsValid = false

            val counterText = itemCounter?.text.toString()
            if (counterText.isNotBlank()) {
                foundCounter = Integer.valueOf(counterText)
            }

            val counterIsValid = foundCounter >= 0 && (foundCounter <= this.lineItem.total)

            val actualWeightText = itemActualWeight?.text.toString()
            if (actualWeightText.isNotBlank()) {
                actualWeight = actualWeightText.toDouble()
            }

            if (this.lineItem.hasNaturalUnit()) {
                try {
                    actualWeightIsValid = actualWeightValidator.validate()
                }
                catch (ignored: Exception) {
                }

                if (actualWeightIsValid && counterIsValid) {
                    callback.found(foundCounter, actualWeight, this.shopperNoteFulfilled)

                    itemActualWeightBackground.setStroke(1, Color.BLACK)
                    dialog.dismiss()
                }
            }
            else if (counterIsValid) {
                callback.found(foundCounter, actualWeight, this.shopperNoteFulfilled)

                itemActualWeightBackground.setStroke(1, Color.BLACK)
                dialog.dismiss()
            }
            else {
                itemActualWeightBackground.setStroke(1, Color.BLACK)
                dialog.dismiss()
            }
        }

        showItemCounter()
        showShopperNote()

        builder.build().show()

        itemCounter?.requestFocus()

        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(itemCounter, InputMethodManager.SHOW_IMPLICIT)
        imm.showSoftInput(itemActualWeight, InputMethodManager.SHOW_IMPLICIT)
    }

    private var actualWeightValidator = object : ActualWeightValidator {
        internal var messageColor: Int = 0

        fun warn() {
            messageColor = ContextCompat.getColor(context, R.color.notification_warn)
            itemActualWeight?.textColor = messageColor
            itemActualWeightBackground.setStroke(1, messageColor)

            errorMessage?.text = context.getString(R.string.item_actual_weight_warn_message)
            errorIcon?.imageResource = R.drawable.icon_circle_question_mark
            errorLayout?.visibility = View.VISIBLE
        }

        fun error() {
            messageColor = ContextCompat.getColor(context, R.color.notification_error)
            itemActualWeight?.textColor = messageColor
            itemActualWeightBackground.setStroke(1, messageColor)

            errorMessage?.text = context.getString(R.string.item_actual_weight_max_message)
            errorIcon?.imageResource = R.drawable.icon_circle_error
            errorLayout?.visibility = View.VISIBLE
        }

        fun required() {
            messageColor = ContextCompat.getColor(context, R.color.notification_error)
            itemActualWeightBackground.setStroke(1, messageColor)

            itemActualWeight?.textColor = messageColor
            errorLayout?.visibility = View.GONE
        }

        override fun validate(): Boolean {
            val text = itemActualWeight?.text.toString()

            if (text.isEmpty()) {
                required()

                return false
            }

            val actualWeight = text.toDouble()
            val foundCount = itemCounter?.text.toString()

            errorIcon?.visibility = View.VISIBLE

            val expectedWeight: Double?
            expectedWeight = if (foundCount.isEmpty()) {
                lineItem.totalNaturalAverageWeight
            }
            else {
                val count = Integer.valueOf(foundCount)
                if (count == 0 && actualWeight == 0.0) {
                    return true
                }

                lineItem.getTotalNaturalAverageWeight(count)
            } ?: 0.0

            // above 50%;
            if (actualWeight >= expectedWeight * (1 + Constant.ACTUAL_WEIGHT_MAX_FACTOR)) {
                error()

                return false
            }

            // above 10%
            if (actualWeight >= expectedWeight * (1 + Constant.ACTUAL_WEIGHT_WARN_FACTOR)) {
                warn()

                return true
            }

            // bellow 50%
            if (actualWeight <= expectedWeight * (1 - Constant.ACTUAL_WEIGHT_MAX_FACTOR)) {
                error()

                return false
            }

            // bellow 10%
            if (actualWeight <= expectedWeight * (1 - Constant.ACTUAL_WEIGHT_WARN_FACTOR)) {
                warn()

                return true
            }

            messageColor = ContextCompat.getColor(context, R.color.black)
            itemActualWeight?.textColor = messageColor
            itemActualWeightBackground.setStroke(1, messageColor)

            errorLayout?.visibility = TextView.GONE

            return true
        }
    }

    private fun showItemCounter() {
        if (this.lineItem.hasNaturalUnit()) {
            itemActualContainer?.visibility = View.VISIBLE

            itemActualWeight?.addTextChangedListener(object : TextChangedListener() {
                override fun afterTextChanged(s: Editable?) {
                    try {
                        actualWeightValidator.validate()
                    }
                    catch (ignored: Exception) {
                    }
                }
            }
                                                    )

            itemActualWeight?.setOnFocusChangeListener { _, hasFocus ->
                if (hasFocus) {
                    itemActualWeight?.hint = null
                }
                else {
                    itemActualWeight?.hint = context.getString(R.string.item_actual_weight)

                    try {
                        actualWeightValidator.validate()
                    }
                    catch (e: Exception) {
                    }
                }
            }

            itemCounter?.setOnFocusChangeListener { _, hasFocus ->
                val value = itemCounter?.text.toString()
                if (!hasFocus && !value.isEmpty()) {
                    val weight = lineItem.getTotalNaturalAverageWeight(Integer.parseInt(value))
                    expectedText?.text = context.getString(R.string.alert_item_expected_weight, weight, lineItem.supermarketUnit)
                }
            }
        }
        else {
            itemActualContainer?.visibility = View.GONE
        }

        if (this.lineItem.totalShopped > 0) {
            itemCounter?.setText(this.lineItem.totalShopped.toString())
        }

        amountText?.text = context.getString(R.string.alert_item_found_amount, lineItem.total)
        expectedText?.text = context.getString(R.string.alert_item_expected_weight, lineItem.totalNaturalAverageWeight, lineItem.supermarketUnit)
    }

    private fun showShopperNote() {
        if (StringUtils.isEmpty(this.lineItem.notes)) {
            itemShopperNoteContainer?.visibility = View.GONE
            shopperNoteCheckbox?.setOnCheckedChangeListener(null)
        }
        else {
            itemShopperNoteContainer?.visibility = View.VISIBLE
            shopperNoteCheckbox?.setOnCheckedChangeListener({ _, checked -> shopperNoteFulfilled = checked })
        }
    }

    interface FoundItemCallback {
        fun found(quantity: Int, actualWeight: Double?, shopperNotesFulfilled: Boolean)
    }

    private interface ActualWeightValidator {
        fun validate(): Boolean
    }
}