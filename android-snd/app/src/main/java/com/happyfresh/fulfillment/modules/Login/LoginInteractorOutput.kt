package com.happyfresh.fulfillment.modules.Login

import com.happyfresh.fulfillment.abstracts.InteractorOutput

/**
 * Created by galuh on 6/9/17.
 */

interface LoginInteractorOutput : InteractorOutput {

    fun openChooseStore()

    fun tellLoginNotMatch()

    fun tellLoginFailed()

    fun tellLoginSuccessful()
}
