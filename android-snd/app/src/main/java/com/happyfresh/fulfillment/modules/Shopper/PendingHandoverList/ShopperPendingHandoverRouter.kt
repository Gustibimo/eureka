package com.happyfresh.fulfillment.modules.Shopper.PendingHandoverList

import android.app.Activity
import android.content.Context
import android.os.Bundle
import com.happyfresh.fulfillment.abstracts.BaseActivity
import com.happyfresh.fulfillment.abstracts.BaseFragment
import com.happyfresh.fulfillment.abstracts.BaseRouter
import com.happyfresh.snowflakes.hoverfly.config.Constant
import com.happyfresh.snowflakes.hoverfly.models.PendingHandover
import org.parceler.Parcels

/**
 * Created by kharda on 28/06/18.
 */

class ShopperPendingHandoverRouter(context: Context) : BaseRouter(context) {

    override fun detach() {
        (context as Activity).finish()
    }

    override fun viewClass(): Class<out BaseActivity<BaseFragment<*>, *>>? {
        return ShopperPendingHandoverListActivity::class.java
    }

    fun startWithCurrentData(pendingHandoverList: List<PendingHandover>) {
        val bundle = Bundle()
        bundle.putParcelable(Constant.PENDING_HANDOVER_LIST, Parcels.wrap(pendingHandoverList))
        start(bundle)
    }
}
