package com.happyfresh.fulfillment.widgets.LineItem

import android.app.Activity
import android.content.Context
import android.content.Intent
import com.happyfresh.fulfillment.abstracts.BaseRouter
import com.happyfresh.snowflakes.hoverfly.models.Shipment
import com.happyfresh.snowflakes.hoverfly.models.SpreeImage
import com.happyfresh.snowflakes.hoverfly.utils.ChatUtils
import com.shopper.activities.FullImageActivity
import org.parceler.Parcels

/**
 * Created by ifranseda on 10/01/2018.
 */
class LineItemDetailLayoutRouter(context: Context) : BaseRouter(context) {

    override fun detach() {

    }

    fun openDetailImages(images: List<SpreeImage>?) {
        val intent = Intent(context, FullImageActivity::class.java)
        val imagesParcel = Parcels.wrap(images)
        intent.putExtra(FullImageActivity.PRODUCT_IMAGES, imagesParcel)

        (context as Activity).startActivity(intent)
    }

    fun openChatScreen(shopperId: String, shipment: Shipment) {
        ChatUtils.openChatScreen(context, shopperId, shipment)
    }
}