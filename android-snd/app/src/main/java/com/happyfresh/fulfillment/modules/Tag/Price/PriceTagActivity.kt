package com.happyfresh.fulfillment.modules.Tag.Price

import android.os.Bundle
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.fulfillment.modules.Tag.TagActivity

/**
 * Created by galuh on 11/13/17.
 */
class PriceTagActivity : TagActivity<PriceTagFragment, BasePresenter<*, *, *>>() {
    override fun title(): String? {
        return getString(R.string.action_price_change)
    }

    override fun provideFragment(bundle: Bundle?): PriceTagFragment? {
        return PriceTagFragment().apply { arguments = bundle }
    }
}