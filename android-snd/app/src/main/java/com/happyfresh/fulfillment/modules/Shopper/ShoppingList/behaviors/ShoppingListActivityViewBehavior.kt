package com.happyfresh.fulfillment.modules.Shopper.ShoppingList.behaviors

import com.happyfresh.fulfillment.abstracts.ViewBehavior

/**
 * Created by ifranseda on 09/10/2017.
 */

interface ShoppingListActivityViewBehavior : ViewBehavior {

    fun showStoreName(name: String)

    fun showStoreImage(image: String)

    fun showStoreAddress(address: String)

    fun showShoppingButton()

    fun hideShoppingButton()

    fun showChatButton()

    fun hideChatButton()

    fun hideCancelMenu()

    fun showFailedToStart()

    fun pleaseTryAgain()

    fun setTimer(time: String)

    fun showTimer()

    fun hideTimer()

    fun showChatBadge(count: Int)
}