package com.happyfresh.fulfillment.modules.LineItem.interactors

import com.happyfresh.fulfillment.abstracts.BaseInteractor
import com.happyfresh.fulfillment.modules.LineItem.outputs.LineItemDetailInteractorOutput
import com.happyfresh.snowflakes.hoverfly.models.LineItem
import com.happyfresh.snowflakes.hoverfly.models.response.FeedbackTypeResponse
import com.happyfresh.snowflakes.hoverfly.services.AppService
import com.happyfresh.snowflakes.hoverfly.shared.subscribers.ServiceSubscriber
import com.shopper.utils.LogUtils

/**
 * Created by ifranseda on 10/10/2017.
 */

abstract class LineItemDetailInteractor<out T : LineItemDetailInteractorOutput> : BaseInteractor<T>() {

    private val appService: AppService by lazy { AppService() }

    protected var lineItem: LineItem? = null

    fun currentItem(): LineItem? {
        return this.lineItem
    }

    fun initialize(item: LineItem?) {
        item?.let {
            LineItem.findByLineItem(it)?.let { savedItem ->
                this.lineItem = savedItem

                output().onLineItem(savedItem)

                this.fetchStockOutFeedbackList()
            }
        }
    }

    fun reloadLineItem() {
        this.lineItem?.load()

        this.lineItem?.let { savedItem ->
            output().onLineItem(savedItem)
        }
    }

    private fun fetchStockOutFeedbackList() {
        lineItem?.variant?.remoteId?.let {
            val observable = appService.feedbackTypes(it)
            val subscriber = object : ServiceSubscriber<FeedbackTypeResponse>() {
                override fun onFailure(e: Throwable) {
                    LogUtils.LOG(e.localizedMessage)
                }

                override fun onSuccess(data: FeedbackTypeResponse) {
                    output().onStockOutFeedbackList(data.feedbackTypes)
                }
            }

            val subscription = observable.subscribe(subscriber)
            subscriptions().add(subscription)
        }
    }

}