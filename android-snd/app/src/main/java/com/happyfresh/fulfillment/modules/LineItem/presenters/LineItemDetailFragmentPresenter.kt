package com.happyfresh.fulfillment.modules.LineItem.presenters

import android.content.Context
import android.content.Intent
import com.happyfresh.fulfillment.modules.LineItem.behaviors.LineItemDetailFragmentViewBehavior
import com.happyfresh.fulfillment.modules.LineItem.interactors.LineItemDetailFragmentInteractor
import com.happyfresh.fulfillment.modules.LineItem.outputs.LineItemDetailFragmentInteractorOutput
import com.happyfresh.fulfillment.modules.Tag.TagRouter
import com.happyfresh.fulfillment.modules.LineItem.LineItemDetailRouter
import com.happyfresh.snowflakes.hoverfly.models.FeedbackType
import com.happyfresh.snowflakes.hoverfly.models.LineItem

class LineItemDetailFragmentPresenter(context: Context) :
        LineItemDetailPresenter<LineItemDetailFragmentInteractor, LineItemDetailFragmentViewBehavior>(context),
        LineItemDetailFragmentInteractorOutput {

    override fun provideInteractor(): LineItemDetailFragmentInteractor? {
        return LineItemDetailFragmentInteractor()
    }

    override fun provideRouter(context: Context): LineItemDetailRouter? {
        return LineItemDetailRouter(context)
    }

    override fun onLineItem(lineItem: LineItem) {
        view()?.showLineItem(lineItem)
    }

    override fun onStockOutFeedbackList(feedbackType: List<FeedbackType>) {
        view()?.setStockOutFeedbackList(feedbackType)
    }

    override fun photoWeightDiffForOrder(orderNumber: String?, typeId: Int, newValue: String) {
        router().openCameraForWeightDifference(orderNumber, typeId, newValue)
    }

    override fun photoPriceChangeForOrder(orderNumber: String?, newValue: String) {
        router().openCameraForPriceChange(orderNumber, newValue)
    }

    override fun photoProductMismatch(orderNumber: String?) {
        router().openCameraForProductMismatch(orderNumber)
    }

    override fun invalidateOptionMenu() {
        view()?.invalidateOptionMenu()
    }

    override fun closeFragment() {
        view()?.closeFragment()
    }

    fun handleResult(requestCode: Int, data: Intent?): Boolean {
        val outputFilePath = data?.getStringExtra(TagRouter.OUTPUT_PATH)

        when (requestCode) {
            TagRouter.PriceChangeRequestCode     -> {
                val newValue = data?.getStringExtra(TagRouter.OUTPUT_NEW_VALUE)

                outputFilePath?.let {
                    interactor().changePrice(it, newValue?.toDouble())
                    return true
                }
            }
            TagRouter.WeightDiffRequestCode      -> {
                val newValue = data?.getStringExtra(TagRouter.OUTPUT_NEW_VALUE)
                val typeId = data?.getIntExtra(TagRouter.OUTPUT_TYPE_ID, -1) ?: -1

                outputFilePath?.let {
                    interactor().weightDiff(it, typeId, newValue?.toDouble())
                    return true
                }
            }
            TagRouter.ProductMismatchRequestCode -> outputFilePath?.let {
                interactor().flagMismatch(it)
                return true
            }
        }

        return false
    }

    fun lineItem(): LineItem? {
        return interactor().currentItem()
    }

    fun initiatePhotoForWeightDiff(typeId: Int, newValue: String) {
        interactor().prepareWeightDiffForOrder(typeId, newValue)
    }

    fun initiatePhotoForPriceChange(newValue: String) {
        interactor().preparePriceChangeForOrder(newValue)
    }

    fun initiatePhotoForProductMismatch() {
        interactor().prepareProductMismatch()
    }

    fun removeProductMismatch() {
        interactor().removeProductMismatch()
    }

    fun removeFlagItem() {
        interactor().removeFlagItem()
    }

    fun flagItem(typeId: Int?, detail: String?) {
        interactor().flagItem(typeId, detail)
    }

    fun itemFound(quantity: Int, actualWeight: Double?, shopperNotesFulfilled: Boolean) {
        interactor().itemFound(quantity, actualWeight, shopperNotesFulfilled)
    }
}