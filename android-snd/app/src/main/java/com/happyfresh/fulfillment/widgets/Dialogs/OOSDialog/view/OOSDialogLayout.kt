package com.happyfresh.fulfillment.widgets.Dialogs.OOSDialog.view

import android.content.Context
import android.util.AttributeSet
import com.afollestad.materialdialogs.DialogAction
import com.afollestad.materialdialogs.MaterialDialog
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.fulfillment.widgets.BaseLayout
import com.happyfresh.snowflakes.hoverfly.models.FeedbackType

/**
 * Created by galuh on 11/10/17.
 */
class OOSDialogLayout @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
        BaseLayout<BasePresenter<*, *, *>>(context, attrs, defStyleAttr) {

    val feedbackTypeList: MutableList<FeedbackType> = mutableListOf()

    override fun layoutResID(): Int {
        return 0
    }

    override fun providePresenter(): BasePresenter<*, *, *>? {
        return null
    }

    var newWeight: Double? = null

    fun setFeedbackList(feedbackList: List<FeedbackType>) {
        this.feedbackTypeList.clear()
        this.feedbackTypeList.addAll(feedbackList)
    }

    fun show(selectedIndex: Int, callback: MaterialDialog.ListCallbackSingleChoice) {
        MaterialDialog.Builder(context).title(R.string.alert_title_out_of_stock_reason)
                .items(this.feedbackTypeList)
                .positiveText(R.string.alert_button_ok)
                .negativeText(R.string.alert_button_cancel)
                .itemsCallbackSingleChoice(selectedIndex, callback)
                .build()
                .show()
    }
}