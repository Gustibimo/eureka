package com.happyfresh.fulfillment.modules.Tag.Weight

import android.os.Bundle
import android.view.View
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.modules.Tag.TagFragment

/**
 * Created by galuh on 11/24/17.
 */
class WeightTagFragment : TagFragment<WeightTagPresenter>() {
    override var warningContentRes = R.string.discard_new_weight

    override fun providePresenter(): WeightTagPresenter {
        return WeightTagPresenter(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        presenter().initialize(this.bundle)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        captureText?.text = getString(R.string.tap_capture_weight_tag)
    }
}