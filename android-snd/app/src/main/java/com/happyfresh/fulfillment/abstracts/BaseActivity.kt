package com.happyfresh.fulfillment.abstracts

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.view.*
import android.widget.RelativeLayout
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.happyfresh.fulfillment.R
import com.shopper.utils.AppUtils

/**
 * Created by ifranseda on 10/02/2017.
 */
abstract class BaseActivity<out V : BaseFragment<*>, out P : BasePresenter<*, *, *>> : AppCompatActivity(), ViewBehavior {

    @JvmField
    @BindView(R.id.layout_container)
    protected var layoutContainer: View? = null

    @JvmField
    @BindView(R.id.app_bar_layout)
    var appBarLayout: AppBarLayout? = null

    @JvmField
    @BindView(R.id.toolbar_padding)
    protected var toolbarPadding: View? = null

    @JvmField
    @BindView(R.id.toolbar)
    var toolbar: Toolbar? = null

    private var fragment: V? = null

    private var presenter: P? = null

    private val mContainerResId = R.id.layout_container

    internal var bundle: Bundle? = null

    internal var unbinder: Unbinder? = null

    //region Abstractions

    open protected fun layoutResID(): Int {
        return R.layout.activity_fragment_new
    }

    open protected fun needTranslucentToolbar(): Boolean {
        return false
    }

    open protected fun menuResID(): Int {
        return -1
    }

    open protected fun title(): String? {
        return null
    }

    open protected fun showBackButton(): Boolean {
        return false
    }

    open protected fun resourceForUpIndicator(): Int {
        return -1
    }

    open protected fun provideFragment(bundle: Bundle?): V? {
        return null
    }

    fun fragment(): V? {
        return this.fragment
    }

    open protected fun providePresenter(): P? {
        return null
    }

    fun presenter(): P? {
        if (this.presenter == null) {
            this.presenter = providePresenter()
            this.presenter?.setView(this)
        }

        return this.presenter as P
    }

    private val containerFragment: Fragment?
        get() = supportFragmentManager.findFragmentById(mContainerResId)

    //endregion

    //region Lifecycle

    override fun onCreate(savedInstanceState: Bundle?) {
        if (AppUtils.currentUserType() == AppUtils.UserType.RANGER) {
            setTheme(R.style.HappyFreshTheme_Rangers)
        }
        else {
            setTheme(R.style.HappyFreshTheme_Shoppers)
        }

        super.onCreate(savedInstanceState)

        var toolbarLayoutID: Int = R.layout.layout_toolbar

        if (needTranslucentToolbar()) {
            toolbarLayoutID = R.layout.layout_toolbar_translucent
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        }

        val layoutResID = this.layoutResID()
        if (layoutResID > 0) {
            setContentView(layoutResID)

            val viewStub = findViewById(R.id.toolbar_container) as ViewStub?
            viewStub?.let {
                it.layoutResource = toolbarLayoutID
                it.inflate()
            }

            this.unbinder = ButterKnife.bind(this)
        }

        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        this.bundle = savedInstanceState
        if (this.bundle == null && intent != null) {
            this.bundle = intent.extras
        }

        this.title()?.let {
            title = it
        }

        this.toolbar?.let {
            setSupportActionBar(it)
        }

        supportActionBar?.let { actionBar ->
            if (showBackButton()) {
                actionBar.setDisplayShowHomeEnabled(true)
                actionBar.setDisplayHomeAsUpEnabled(true)

                val upResourceId = resourceForUpIndicator()
                if (upResourceId > -1) {
                    actionBar.setHomeAsUpIndicator(upResourceId)
                }
            }

            if (needTranslucentToolbar()) {
                toolbarPadding?.let {
                    val params = it.layoutParams as AppBarLayout.LayoutParams
                    params.height = getStatusBarHeight()
                    it.layoutParams = params
                }

                layoutContainer?.let {
                    val params = it.layoutParams as RelativeLayout.LayoutParams
                    params.setMargins(0, 0, 0, 0)
                    it.layoutParams = params
                }
            }
        }

        this.fragment = provideFragment(this.bundle)
        this.fragment?.let {
            val containerFragment = containerFragment
            if (containerFragment == null) {
                addFragment(it)
            }
            else {
                restoreFragment(containerFragment)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        super.onCreateOptionsMenu(menu)

        val menuResID = this.menuResID()
        val hasOptionMenu = menuResID > 0

        if (hasOptionMenu) {
            menuInflater.inflate(menuResID, menu)
        }

        return hasOptionMenu || showBackButton()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        this.unbinder?.unbind()

        super.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        if (this.bundle != null) {
            outState.putAll(this.bundle!!)
        }

        super.onSaveInstanceState(outState)
    }

    //endregion

    //region Private methods

    private fun restoreFragment(fragment: Fragment) {
        this.fragment = fragment as V?
    }

    private fun addFragment(viewLayer: V?) {
        if (viewLayer != null) {
            supportFragmentManager.beginTransaction().add(mContainerResId, viewLayer).commit()
        }
    }

    private fun getStatusBarHeight(): Int {
        var result = 0
        val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resourceId > 0) {
            result = resources.getDimensionPixelSize(resourceId)
        }
        return result
    }

    //endregion
}
