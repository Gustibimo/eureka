package com.happyfresh.fulfillment.modules.Shopper.ChatList

import android.os.Bundle
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.abstracts.BaseActivity
import com.happyfresh.fulfillment.abstracts.BasePresenter

/**
 * Created by kharda on 15/8/18
 */
class ShopperChatListActivity : BaseActivity<ShopperChatListFragment, BasePresenter<*, *, *>>() {

    override fun title(): String? {
        return getString(R.string.title_chat_list)
    }

    override fun showBackButton(): Boolean {
        return true
    }

    override fun provideFragment(bundle: Bundle?): ShopperChatListFragment? {
        val fragment = ShopperChatListFragment()
        fragment.arguments = bundle
        return fragment
    }
}