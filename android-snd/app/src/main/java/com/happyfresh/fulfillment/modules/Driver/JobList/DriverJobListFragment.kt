package com.happyfresh.fulfillment.modules.Driver.JobList

import android.os.Bundle
import android.view.View

import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.abstracts.BaseFragment

/**
 * Created by galuh on 6/9/17.
 */

class DriverJobListFragment : BaseFragment<DriverJobListPresenter>(), DriverJobListViewBehavior {

    override fun layoutResID(): Int {
        return R.layout.fragment_driver_job_list
    }

    override fun providePresenter(): DriverJobListPresenter {
        return DriverJobListPresenter(context)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }
}
