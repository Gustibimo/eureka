package com.happyfresh.fulfillment.modules.ChooseStore

import android.content.pm.PackageManager
import android.os.Bundle

import com.happyfresh.fulfillment.abstracts.BaseActivity
import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.snowflakes.hoverfly.config.Constant

/**
 * Created by galuh on 6/12/17.
 */

class ChooseStoreActivity : BaseActivity<ChooseStoreFragment, BasePresenter<*, *, *>>() {

    override fun provideFragment(bundle: Bundle?): ChooseStoreFragment? {
        return ChooseStoreFragment()
    }

    public override fun onStart() {
        super.onStart()

        fragment()?.checkClockInStatus()
        fragment()?.connectGoogleApiClient()
    }

    public override fun onStop() {
        fragment()?.disconnectGoogleApiClient()
        super.onStop()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            Constant.PERMISSIONS_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    fragment()?.checkAndRequestLocation()
                }
            }
        }
    }
}
