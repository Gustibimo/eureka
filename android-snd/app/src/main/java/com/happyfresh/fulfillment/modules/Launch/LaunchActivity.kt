package com.happyfresh.fulfillment.modules.Launch

import android.app.Activity
import android.os.Bundle
import android.os.Parcelable
import com.happyfresh.snowflakes.hoverfly.config.Constant
import com.happyfresh.snowflakes.hoverfly.models.payload.SendBirdNotificationPayload
import org.parceler.Parcels

/**
 * Created by galuh on 6/10/17.
 */

class LaunchActivity : Activity() {

    private val root = LaunchRouter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(null)

        val parcelableNotificationData: Parcelable? = this.intent?.extras?.getParcelable(Constant.SENDBIRD)
        if (parcelableNotificationData != null) {
            val notificationData: SendBirdNotificationPayload = Parcels.unwrap(parcelableNotificationData)
            root.openChatScreen(notificationData)
        } else {
            root.startRoot()
        }

        root.detach()
    }
}
