package com.happyfresh.fulfillment.widgets.FinalizeList

import android.app.Activity
import android.content.Context
import android.content.Intent
import com.happyfresh.fulfillment.abstracts.BaseRouter
import com.happyfresh.snowflakes.hoverfly.models.LineItem
import com.shopper.activities.BatchPaymentActivity
import com.shopper.activities.ProductSampleListActivity
import com.shopper.activities.ReplacementShopperActivity
import com.shopper.activities.SearchProductActivity
import com.shopper.common.Constant
import com.shopper.fragments.SearchProductFragment
import org.parceler.Parcels

/**
 * Created by ifranseda on 03/01/2018.
 */
class FinalizeListLayoutRouter(context: Context) : BaseRouter(context) {
    override fun detach() {
        (context as Activity).finish()
    }

    fun searchForReplacement(lineItem: LineItem): Boolean {
        if (lineItem.totalReplacedShopper > 0) {
            lineItem.replacementShopper?.let {
                openReplacement(lineItem)
                return false
            }
        }

        (context as Activity).startActivityForResult(Intent(context, SearchProductActivity::class.java).apply {
            putExtra(Constant.SHIPMENT_ID, lineItem.shipment?.remoteId)
            putExtra(SearchProductFragment.LINE_ITEM, Parcels.wrap(lineItem))
        }, REQUEST_CODE_SEARCH_REPLACEMENT)

        return true
    }

    private fun openReplacement(lineItem: LineItem) {
        val intent = Intent(context, ReplacementShopperActivity::class.java).apply {
            putExtra(SearchProductFragment.SHIPMENT_ID, lineItem.shipment?.remoteId)
            putExtra(SearchProductFragment.LINE_ITEM, Parcels.wrap(lineItem))
            putExtra(SearchProductFragment.VARIANT, Parcels.wrap(lineItem.replacementShopper))
        }

        (context as Activity).startActivity(intent)
    }

    fun openPaymentScreen(batchId: Long, stockLocationId: Long) {
        val intent = Intent(context, BatchPaymentActivity::class.java)
        intent.putExtra(Constant.BATCH_ID, batchId)
        intent.putExtra(Constant.STOCK_LOCATION_ID, stockLocationId)
        intent.putExtra(ProductSampleListActivity.SELECTED_BATCH, batchId)

        context.startActivity(intent)
    }

    fun openProductSampleScreen(batchId: Long) {
        val intent = Intent(context, ProductSampleListActivity::class.java)
        intent.putExtra(ProductSampleListActivity.SELECTED_BATCH, batchId)

        context.startActivity(intent)
    }

    companion object {
        val REQUEST_CODE_SEARCH_REPLACEMENT = 1001
    }
}