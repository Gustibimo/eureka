package com.happyfresh.fulfillment.modules.ChooseStore

import android.location.Location

/**
 * Created by rsavianto on 2/9/15.
 */
interface LocationRetrievedListener {

    fun onLocationRetrieved(location: Location)

    fun onLocationNotRetrieved()
}
