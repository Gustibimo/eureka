package com.happyfresh.fulfillment.modules.Pause

import android.os.Bundle
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.abstracts.BaseActivity
import com.happyfresh.fulfillment.abstracts.BasePresenter

/**
 * Created by aldi on 7/26/17.
 */

class PauseActivity : BaseActivity<PauseFragment, BasePresenter<*, *, *>>() {

    public override fun title(): String? {
        return getString(R.string.title_pause)
    }

    override fun provideFragment(bundle: Bundle?): PauseFragment? {
        return PauseFragment()
    }
}