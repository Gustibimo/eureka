package com.happyfresh.fulfillment.widgets.JobList

import com.happyfresh.fulfillment.abstracts.InteractorOutput
import com.happyfresh.snowflakes.hoverfly.models.Batch

/**
 * Created by ifranseda on 10/10/2017.
 */

interface JobListLayoutInteractorOutput : InteractorOutput {

    fun showLoading(o: Boolean)

    fun unableLoadData()

    fun showData(batchList: List<Batch>)

    fun showEmpty()

    fun openBatch(batch: Batch)

    fun openPayment(batch: Batch)
}