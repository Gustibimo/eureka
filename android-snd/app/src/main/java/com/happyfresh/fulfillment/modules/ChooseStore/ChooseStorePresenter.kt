package com.happyfresh.fulfillment.modules.ChooseStore

import android.content.Context

import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.snowflakes.hoverfly.models.StockLocation

/**
 * Created by galuh on 6/12/17.
 */

class ChooseStorePresenter(context: Context) : BasePresenter<ChooseStoreInteractor, ChooseStoreRouter, ChooseStoreViewBehavior>(context),
        ChooseStoreInteractorOutput {

    init {
        interactor().setActivityContext(context)
    }

    override fun provideInteractor(): ChooseStoreInteractor? {
        return ChooseStoreInteractor()
    }

    override fun provideRouter(context: Context): ChooseStoreRouter? {
        return ChooseStoreRouter(context)
    }

    fun prepareGoogleApiClient() {
        interactor().prepareGoogleApiClient()
    }

    fun connectGoogleApiClient() {
        interactor().connectGoogleApiClient()
    }

    fun disconnectGoogleApiClient() {
        interactor().disconnectGoogleApiClient()
    }

    fun checkAndRequestLocation() {
        interactor().checkAndRequestLocation()
    }

    fun checkClockInStatus() {
        interactor().checkClockInStatus()
    }

    fun refreshStores() {
        interactor().refreshNearbyStores()
    }

    fun selectStore(stockLocation: StockLocation?) {
        interactor().storeSelected(stockLocation)
    }

    fun createPlacement(stockLocation: StockLocation) {
        interactor().createPlacement(stockLocation)
    }

    override fun drawSignalErrorGettingLocation() {
        view()?.showErrorGettingLocation()
    }

    override fun drawNearbyStores(stores: List<StockLocation>) {
        view()?.showStores(stores)
        view()?.hideProgress()
    }

    override fun updateNearbyStores(stores: List<StockLocation>) {
        view()?.updateStores(stores)
        view()?.hideProgress()
    }

    override fun drawEmptyStores() {
        view()?.showEmptyStores()
        view()?.hideProgress()
    }

    override fun openShoppingJobList(stockLocationId: Long?) {
        router().openShoppingJobList(stockLocationId)
    }

    override fun openDriverJobList() {
        router().openDriverJobList()
    }

    override fun drawNoStoreSelected() {
        view()?.showNoStoreSelected()
    }

    override fun drawClockInDialog(stockLocation: StockLocation) {
        view()?.showClockInDialog(stockLocation)
    }

    override fun drawSelectStoreButtonEnabled() {
        view()?.showSelectStoreButtonEnabled()
    }

    override fun drawSelectStoreButtonDisabled() {
        view()?.showSelectStoreButtonDisabled()
    }

    override fun drawStatusIsWorking() {
        view()?.showStatusIsWorking()
    }
}
