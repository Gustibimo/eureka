package com.happyfresh.fulfillment.widgets.LineItem

import com.happyfresh.fulfillment.abstracts.InteractorOutput

/**
 * Created by galuh on 11/5/17.
 */
interface LineItemStockOutLayoutInteractorOutput : InteractorOutput {
    fun showFinalizeItemDialog(orderNumber: String)
}