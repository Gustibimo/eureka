package com.happyfresh.fulfillment.modules.Shopper.ShoppingList.presenters

import android.content.Context
import com.happyfresh.fulfillment.modules.Shopper.ShoppingList.ShoppingListRouter
import com.happyfresh.fulfillment.modules.Shopper.ShoppingList.outputs.ShoppingListFragmentInteractorOutput
import com.happyfresh.fulfillment.modules.Shopper.ShoppingList.behaviors.ShoppingListFragmentViewBehavior
import com.happyfresh.fulfillment.modules.Shopper.ShoppingList.interactors.ShoppingListFragmentInteractor
import com.happyfresh.snowflakes.hoverfly.models.Batch

/**
 * Created by ifranseda on 09/10/2017.
 */

class ShoppingListFragmentPresenter(context: Context) :
        ShoppingListPresenter<ShoppingListFragmentInteractor, ShoppingListFragmentViewBehavior>(context),
        ShoppingListFragmentInteractorOutput {

    override fun provideInteractor(): ShoppingListFragmentInteractor? {
        return ShoppingListFragmentInteractor()
    }

    override fun provideRouter(context: Context): ShoppingListRouter? {
        return ShoppingListRouter(context)
    }

    override fun onBatch(batch: Batch) {
        view()?.initializeShoppingList(batch)
    }

    fun finalizeShoppingList() {
        interactor().finalizeShopping()
    }

    fun prepareToggleStartShoppingButton() {
        interactor().toggleStartShoppingButton()
    }

    override fun showAlertIncompleteLineItem(pendingCounter: Int) {
        view()?.showAlertIncompleteLineItem(pendingCounter)
    }

    override fun showProgress(show: Boolean) {
        if (show) {
            view()?.showProgress()
        }
        else {
            view()?.hideProgress()
        }
    }

    override fun showAlertUnableCheckProductSample() {
        view()?.showAlertUnableCheckProductSample()
    }

    override fun openPaymentScreen(batchId: Long, stockLocationId: Long) {
        router().openPaymentScreen(batchId, stockLocationId)
    }

    override fun openProductSampleScreen(batchId: Long) {
        router().openProductSampleScreen(batchId)
    }

    override fun openFinalizeItems(batch: Batch) {
        router().finalizeBatch(batch)
    }

    override fun unableToFinalize() {
        view()?.shoppingNotComplete()
    }

    override fun toggleStartShoppingButton(isEnable: Boolean) {
        view()?.toggleStartShoppingButton(isEnable)
    }
}