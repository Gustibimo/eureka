package com.happyfresh.fulfillment.widgets.LineItem

import com.happyfresh.fulfillment.abstracts.ViewBehavior

/**
 * Created by kharda on 30/8/18
 */
interface LineItemDetailLayoutViewBehavior : ViewBehavior {

    fun toggleChatButton(show: Boolean)
}