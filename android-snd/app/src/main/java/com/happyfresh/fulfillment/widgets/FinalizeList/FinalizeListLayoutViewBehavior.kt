package com.happyfresh.fulfillment.widgets.FinalizeList

import com.happyfresh.fulfillment.abstracts.RecyclerViewBehavior
import com.happyfresh.snowflakes.hoverfly.models.Shipment

/**
 * Created by ifranseda on 03/01/2018.
 */
interface FinalizeListLayoutViewBehavior : RecyclerViewBehavior<FinalizeListLayoutAdapter> {
    fun hideProgress()

    fun showProgress()

    fun showFinalizeDialog(shipment: Shipment, position: Int)

    fun reload()
}