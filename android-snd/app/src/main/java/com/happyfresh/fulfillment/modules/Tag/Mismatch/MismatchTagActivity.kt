package com.happyfresh.fulfillment.modules.Tag.Mismatch

import android.os.Bundle
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.fulfillment.modules.Tag.TagActivity

/**
 * Created by ifranseda on 29/12/2017.
 */
class MismatchTagActivity : TagActivity<MismatchTagFragment, BasePresenter<*, *, *>>() {
    override fun title(): String? {
        return getString(R.string.action_flag_mismatch)
    }

    override fun provideFragment(bundle: Bundle?): MismatchTagFragment? {
        return MismatchTagFragment().apply { arguments = bundle }
    }
}