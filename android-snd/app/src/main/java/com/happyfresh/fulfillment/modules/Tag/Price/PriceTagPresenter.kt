package com.happyfresh.fulfillment.modules.Tag.Price

import android.content.Context
import android.os.Bundle
import com.happyfresh.fulfillment.modules.Tag.TagPresenter
import com.happyfresh.fulfillment.modules.Tag.TagViewBehavior

/**
 * Created by galuh on 11/13/17.
 */
class PriceTagPresenter(context: Context) : TagPresenter<PriceTagInteractor, PriceTagRouter, TagViewBehavior>(context) {

    init {
        interactor().context = context
    }

    override fun provideInteractor(): PriceTagInteractor? {
        return PriceTagInteractor()
    }

    override fun provideRouter(context: Context): PriceTagRouter? {
        return PriceTagRouter(context)
    }

    fun initialize(extras: Bundle?) {
        interactor().initialize(extras)
    }
}