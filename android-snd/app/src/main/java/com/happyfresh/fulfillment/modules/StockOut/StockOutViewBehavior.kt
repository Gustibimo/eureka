package com.happyfresh.fulfillment.modules.StockOut

import com.happyfresh.fulfillment.abstracts.ViewBehavior

/**
 * Created by ifranseda on 02/01/2018.
 */
interface StockOutViewBehavior : ViewBehavior {

    fun drawFinalizeForBatch(batchId: Long)

    fun showChatBadge(count: Int)

    fun hideChatButton()
}