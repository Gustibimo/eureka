package com.happyfresh.fulfillment.modules.Shopper.ShoppingList.behaviors

import com.happyfresh.fulfillment.abstracts.ViewBehavior
import com.happyfresh.snowflakes.hoverfly.models.Batch
import com.happyfresh.snowflakes.hoverfly.models.StockLocation

/**
 * Created by galuh on 9/27/17.
 */
interface ShoppingListFragmentViewBehavior : ViewBehavior {

    fun initializeShoppingList(batch: Batch)

    fun shoppingNotComplete()

    fun showAlertIncompleteLineItem(pendingCounter: Int)

    fun showProgress()

    fun hideProgress()

    fun showAlertUnableCheckProductSample()

    fun toggleStartShoppingButton(isEnable: Boolean)
}