package com.happyfresh.fulfillment.modules.StockChecker

import com.happyfresh.fulfillment.abstracts.InteractorOutput

/**
 * Created by Novanto on 12/28/17.
 */

interface StockCheckerInteractorOutput : InteractorOutput {
    fun prepareLoadUrl(url: String, accessToken: String)
}