package com.happyfresh.fulfillment.widgets.StockChecker

import android.content.Context
import com.happyfresh.fulfillment.abstracts.BaseInteractor
import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.fulfillment.abstracts.BaseRouter

/**
 * Created by Novanto on 12/27/17.
 */

class StockCheckerPresenter(context: Context) : BasePresenter<BaseInteractor<*>, StockCheckerRouter, StockCheckerViewBehavior>(context) {
    override fun provideRouter(context: Context): StockCheckerRouter? {
        return StockCheckerRouter(context)
    }

    override fun provideInteractor(): BaseInteractor<*>? {
        return null
    }

    fun prepareOpenStockChecker() {
        router().openStockChecker()
    }
}