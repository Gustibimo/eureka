package com.happyfresh.fulfillment.widgets.LineItem.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import butterknife.BindView
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.fulfillment.widgets.BaseLayout
import com.happyfresh.snowflakes.hoverfly.models.LineItem
import com.happyfresh.snowflakes.hoverfly.utils.FormatterUtils
import com.shopper.common.Constant
import com.shopper.views.CircleImageView
import com.squareup.picasso.Picasso

/**
 * Created by ifranseda on 03/10/2017.
 */

open class LineItemLayout<out P : BasePresenter<*, *, *>>
@JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
        BaseLayout<P>(context, attrs, defStyleAttr) {
    @JvmField
    @BindView(R.id.container)
    var container: LinearLayout? = null

    @JvmField
    @BindView(R.id.category_container)
    var categoryContainer: LinearLayout? = null

    @JvmField
    @BindView(R.id.category_name)
    var categoryName: TextView? = null

    @JvmField
    @BindView(R.id.order_container)
    var orderContainer: View? = null

    @JvmField
    @BindView(R.id.order_indicator)
    var orderIndicator: CircleImageView? = null

    @JvmField
    @BindView(R.id.order_number)
    var orderNumber: TextView? = null

    @JvmField
    @BindView(R.id.customer_name)
    var customerName: TextView? = null

    @JvmField
    @BindView(R.id.item_image)
    var itemImage: ImageView? = null

    @JvmField
    @BindView(R.id.item_status)
    var itemStatus: TextView? = null

    @JvmField
    @BindView(R.id.item_display_banner_container)
    var itemPromotionBannerContainer: View? = null

    @JvmField
    @BindView(R.id.item_display_banner)
    var itemPromotionBanner: TextView? = null

    @JvmField
    @BindView(R.id.item_detail_container)
    var itemDetailContainer: RelativeLayout? = null

    @JvmField
    @BindView(R.id.item_replacement_status)
    var itemReplacementStatus: TextView? = null

    @JvmField
    @BindView(R.id.item_original_container)
    var itemOriginalContainer: RelativeLayout? = null

    @JvmField
    @BindView(R.id.item_counter_total)
    var itemCounterTotal: TextView? = null

    @JvmField
    @BindView(R.id.item_counter)
    var itemCounter: TextView? = null

    @JvmField
    @BindView(R.id.item_variant_name)
    var itemVariantName: TextView? = null

    @JvmField
    @BindView(R.id.item_price)
    var itemPrice: TextView? = null

    @JvmField
    @BindView(R.id.shopper_note_container)
    var shopperNoteContainer: View? = null

    @JvmField
    @BindView(R.id.shopper_note)
    var shopperNote: TextView? = null

    @JvmField
    @BindView(R.id.shopper_replacement)
    var shopperReplacementLayout: RelativeLayout? = null

    @JvmField
    @BindView(R.id.sr_item_image)
    var shopperReplacementItemImage: ImageView? = null

    @JvmField
    @BindView(R.id.sr_item_container)
    var shopperReplacementItemContainer: RelativeLayout? = null

    @JvmField
    @BindView(R.id.sr_item_counter)
    var shopperReplacementItemCounter: TextView? = null

    @JvmField
    @BindView(R.id.sr_item_variant_name)
    var shopperReplacementItemVariantName: TextView? = null

    @JvmField
    @BindView(R.id.sr_item_price)
    var shopperReplacementItemPrice: TextView? = null

    override fun layoutResID(): Int {
        return R.layout.widget_line_item
    }

    override fun providePresenter(): P? {
        return null
    }

    //region Public methods

    fun setCategory(name: String?) {
        if (name != null) {
            categoryContainer?.visibility = View.VISIBLE
            categoryName?.text = name
        }
        else {
            categoryContainer?.visibility = View.GONE
        }
    }

    open var lineItem: LineItem? = null
        set(value) {
            field = value

            showPromotionBanner()
            showOrderIndicator()
            showPurchaseStatus()

            showItem()
            showShopperNotes()

            showReplacement()
            showReplacementOptions()
        }

    //endregion

    //region Protected & Private methods

    protected fun showPromotionBanner() {
        if (this.lineItem?.displayBanner != null) {
            itemPromotionBannerContainer?.visibility = View.VISIBLE
            itemPromotionBanner?.text = this.lineItem?.displayBanner
        }
        else {
            itemPromotionBannerContainer?.visibility = View.GONE
        }
    }

    open protected fun showOrderIndicator() {
        this.lineItem?.let {
            it.shipment?.colorResource?.let { color ->
                orderIndicator?.setBackgroundColor(color)
                orderNumber?.setTextColor(color)
                customerName?.setTextColor(color)
            }

            orderNumber?.text = it.shipment?.order?.number

            customerName?.text = it.shipment?.customerName
        }
    }

    open fun showPurchaseStatus() {
        val purchaseStatus = this.lineItem?.status
        if (purchaseStatus === LineItem.PurchaseStatus.NOT_STARTED || this.lineItem?.isShoppingBag!!) {
            itemStatus?.visibility = View.GONE
        }
        else {
            itemStatus?.visibility = View.VISIBLE
            itemStatus?.text = purchaseStatus?.statusName(context)

            itemStatus?.setTextColor(purchaseStatus?.textColor(context)!!)
            itemStatus?.setBackgroundColor(purchaseStatus?.backgroundColor(context)!!)
        }
    }

    open fun showReplacementOptions() {
        itemReplacementStatus?.visibility = View.VISIBLE

        when {
            this.lineItem?.replacementType?.isNotEmpty() == true -> when {
                this.lineItem?.replacementType.equals(Constant.SUGGEST_REPLACEMENT_BY_CALL, ignoreCase = true) -> {
                    itemReplacementStatus?.setText(R.string.call_me)
                }
                this.lineItem?.replacementType.equals(Constant.SUGGEST_REPLACEMENT_BY_CHAT, ignoreCase = true) -> {
                    itemReplacementStatus?.setText(R.string.chat_me)
                }
                this.lineItem?.replacementType.equals(Constant.REPLACE_FREELY, ignoreCase = true)      -> {
                    itemReplacementStatus?.setText(R.string.replace_freely)
                }
                else                                                                                   -> {
                    itemReplacementStatus?.visibility = View.GONE
                    itemReplacementStatus?.setText(R.string.empty_string)
                }
            }
            this.lineItem?.isShoppingBag!! -> itemReplacementStatus?.visibility = View.INVISIBLE
            else -> itemReplacementStatus?.setText(R.string.suggest_replacement)
        }
    }

    open protected fun showItem() {
        itemVariantName?.text = this.lineItem?.variant?.name?.trim()

        if (this.lineItem?.isShoppingBag!!){
            itemPrice?.visibility = View.GONE
            itemCounterTotal?.visibility = View.GONE
            itemCounter?.visibility = View.GONE

            Picasso.with(context).cancelRequest(itemImage)
            Picasso.with(context).load(R.drawable.icon_bag_with_logo).into(itemImage)
        } else {
            val productUrl = this.lineItem?.variant?.images()?.firstOrNull()?.productUrl
            Picasso.with(context).cancelRequest(itemImage)
            Picasso.with(context).load(productUrl).placeholder(R.drawable.default_product).into(itemImage)

            val shoppedCounter = getShoppedItem()
            val totalItem = getTotalItem()
            val shoppedCount = context.getString(R.string.item_shopped_counter, shoppedCounter, totalItem)

            itemCounter?.text = shoppedCounter.toString()
            itemCounterTotal?.text = shoppedCount

            itemPrice?.text = itemPrice()
        }
    }

    open protected fun getShoppedItem(): Int {
        return this.lineItem?.totalShopped!! - this.lineItem?.totalReplacedCustomer!! - this.lineItem?.totalReplacedShopper!!
    }

    open protected fun getTotalItem(): Int {
        return this.lineItem?.total!!
    }

    protected fun showShopperNotes() {
        if (this.lineItem?.notes.isNullOrEmpty()) {
            shopperNoteContainer?.visibility = View.GONE
        }
        else {
            shopperNoteContainer?.visibility = View.VISIBLE
            shopperNote?.text = this.lineItem?.notes
        }
    }

    private fun showReplacement(): Boolean {
        if ((this.lineItem?.totalReplacedShopper ?: 0) > 0) {
            this.lineItem?.replacementShopper?.let {
                shopperReplacementLayout?.visibility = View.VISIBLE

                val imageUrl = it.images()?.firstOrNull()?.productUrl

                Picasso.with(context).cancelRequest(shopperReplacementItemImage)
                Picasso.with(context).load(imageUrl).placeholder(R.drawable.default_product).into(shopperReplacementItemImage)

                shopperReplacementItemCounter?.text = this.lineItem?.totalReplacedShopper.toString()
                shopperReplacementItemVariantName?.text = it.name!!.trim()
                shopperReplacementItemPrice?.text = replacementPrice()

                return true
            }
        }

        shopperReplacementLayout?.visibility = View.GONE

        return false
    }

    open protected fun itemPrice(): String? {
        val numberFormatter = this.lineItem?.shipment?.order?.let {
            FormatterUtils.getNumberFormatter(it.currency)
        }

        var price = this.lineItem?.displayCostPrice
        this.lineItem?.priceAmendment?.variant?.let {
            price = numberFormatter?.format(it.toDouble())
        }

        return if (this.lineItem?.hasNaturalUnit()!!) {
            String.format("%s/%s", price, this.lineItem?.displayAverageWeight)
        }
        else {
            price
        }
    }

    private fun replacementPrice(): String? {
        val replacement = this.lineItem?.replacementShopper ?: return String()

        val numberFormatter = this.lineItem?.shipment?.order?.let {
            FormatterUtils.getNumberFormatter(it.currency)
        }

        var price = replacement.displayPrice
        this.lineItem?.priceAmendmentShopperReplacement?.replacement?.let {
            price = numberFormatter?.format(it.toDouble())
        }

        return if (replacement.hasNaturalUnit()) {
            String.format("%s/%s", price, replacement.displayAverageWeight)
        }
        else {
            price
        }
    }

    //endregion
}