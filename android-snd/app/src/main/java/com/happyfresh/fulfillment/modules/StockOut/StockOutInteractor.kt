package com.happyfresh.fulfillment.modules.StockOut

import com.happyfresh.fulfillment.abstracts.BaseInteractor
import com.happyfresh.snowflakes.hoverfly.Sprinkles
import com.happyfresh.snowflakes.hoverfly.models.Batch
import com.happyfresh.snowflakes.hoverfly.shared.events.UnreadChatFetchedEvent
import com.happyfresh.snowflakes.hoverfly.stores.UserStore
import com.happyfresh.snowflakes.hoverfly.utils.ChatUtils
import com.squareup.otto.Subscribe

/**
 * Created by kharda on 21/8/18
 */
class StockOutInteractor : BaseInteractor<StockOutInteractorOutput>() {

    private val CONNECTION_HANDLER_ID = "CONNECTION_HANDLER_STOCK_OUT_INTERACTOR"

    private val CHANNEL_HANDLER_ID = "CHANNEL_HANDLER_STOCK_OUT_INTERACTOR"

    private val userStore = Sprinkles.stores(UserStore::class.java)

    var batchId: Long? = null

    override fun shouldListenEventBus(): Boolean {
        return true
    }

    fun initialize(batchId: Long) {
        this.batchId = batchId
        prepareShowChatBadge()
    }

    fun addChatHandler() {
        val shopperId = userStore?.userId.toString()

        ChatUtils.addChatHandler(batchId, CONNECTION_HANDLER_ID, CHANNEL_HANDLER_ID, shopperId)
    }

    fun removeChatHandler() {
        ChatUtils.removeChatHandler(CONNECTION_HANDLER_ID, CHANNEL_HANDLER_ID)
    }

    private fun prepareShowChatBadge() {
        val batch = Batch.findById(batchId)
        batch?.let {
            if (batch.isChatEnabled()) {
                output().showChatBadge(batch.unreadChatMessageCount())
            } else {
                output().hideChatButton()
            }
        }
    }

    @Subscribe
    fun onUnreadChatFetched(event: UnreadChatFetchedEvent) {
        if (batchId == event.data) {
            prepareShowChatBadge()
        }
    }
}