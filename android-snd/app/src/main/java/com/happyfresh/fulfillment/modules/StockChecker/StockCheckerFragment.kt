package com.happyfresh.fulfillment.modules.StockChecker

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ProgressBar
import butterknife.BindView
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.abstracts.BaseFragment

/**
 * Created by Novanto on 12/27/17.
 */

class StockCheckerFragment : BaseFragment<StockCheckerPresenter>(), StockCheckerViewBehavior {

    @JvmField
    @BindView(R.id.webview)
    var webView: WebView? = null

    @JvmField
    @BindView(R.id.pb_loading)
    var progressBar: ProgressBar? = null

    override fun providePresenter(): StockCheckerPresenter {
        return StockCheckerPresenter(context)
    }

    override fun layoutResID(): Int {
        return R.layout.fragment_manual_stock_checker
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        webView?.setWebViewClient(WebViewClient())
        webView?.setWebChromeClient(object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, progress: Int) {
                if (progress < 100 && progressBar?.visibility == ProgressBar.GONE) {
                    progressBar?.visibility = ProgressBar.VISIBLE
                }

                progressBar?.progress = progress
                if (progress == 100) {
                    progressBar?.visibility = ProgressBar.GONE
                }
            }
        })

        webView?.settings?.javaScriptEnabled = true
        presenter().prepareLoadUrlData()
    }

    override fun loadUrl(url: String, header: HashMap<String, String>) {
        webView?.loadUrl(url, header)
    }
}