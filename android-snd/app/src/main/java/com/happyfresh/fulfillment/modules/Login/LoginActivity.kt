package com.happyfresh.fulfillment.modules.Login

import android.os.Bundle

import com.happyfresh.fulfillment.abstracts.BaseActivity
import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.snowflakes.hoverfly.utils.PermissionUtils

/**
 * Created by galuh on 6/10/17.
 */

class LoginActivity : BaseActivity<LoginFragment, BasePresenter<*, *, *>>() {

    override fun title(): String? {
        return "Login"
    }

    override fun provideFragment(bundle: Bundle?): LoginFragment {
        return LoginFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        PermissionUtils.requestPermissionIfNeeded(this)
    }
}
