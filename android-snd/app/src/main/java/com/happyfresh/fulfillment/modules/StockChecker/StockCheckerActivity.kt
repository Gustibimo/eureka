package com.happyfresh.fulfillment.modules.StockChecker

import android.os.Bundle
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.abstracts.BaseActivity
import com.happyfresh.fulfillment.abstracts.BasePresenter


/**
 * Created by Novanto on 12/27/17.
 */

class StockCheckerActivity : BaseActivity<StockCheckerFragment, BasePresenter<*, *, *>>() {

    override fun title(): String? {
        return getString(R.string.manual_stock_checker)
    }

    override fun provideFragment(bundle: Bundle?): StockCheckerFragment {
        return StockCheckerFragment()
    }

    override fun showBackButton(): Boolean {
        return true
    }
}