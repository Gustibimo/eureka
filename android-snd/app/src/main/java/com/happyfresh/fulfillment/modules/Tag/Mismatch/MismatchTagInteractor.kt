package com.happyfresh.fulfillment.modules.Tag.Mismatch

import android.os.Bundle
import com.happyfresh.fulfillment.modules.Tag.TagInteractor
import com.happyfresh.fulfillment.modules.Tag.TagInteractorOutput
import com.happyfresh.fulfillment.modules.Tag.TagRouter

/**
 * Created by ifranseda on 29/12/2017.
 */
class MismatchTagInteractor : TagInteractor<TagInteractorOutput>() {
    override var photoFileFormat = "MISMATCH_%s_%d.jpg"

    override var photoDirectory = "mismatch"

    override fun initialize(extras: Bundle?) {
        orderNumber = extras?.getString(TagRouter.ORDER_NUMBER)
    }
}