package com.happyfresh.fulfillment.modules.Shopper.JobList

import com.happyfresh.fulfillment.abstracts.InteractorOutput
import com.happyfresh.snowflakes.hoverfly.models.PendingHandover
import com.happyfresh.snowflakes.hoverfly.models.response.VersionUpdate

/**
 * Created by galuh on 6/9/17.
 */

interface ShopperJobListInteractorOutput : InteractorOutput {

    fun openPause()

    fun askToClockOut(storeLocation: String?)

    fun showRequireUpdateDialog(data: VersionUpdate)

    fun openChooseStore()

    fun openPendingHandover(pendingHandoverList: List<PendingHandover>?)

    fun togglePendingHandoverIcon(isNotEmpty: Boolean)
}
