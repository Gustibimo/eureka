package com.happyfresh.fulfillment.widgets.JobList

import android.content.Context
import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.fulfillment.widgets.ViewHolder
import com.happyfresh.snowflakes.hoverfly.models.Batch

/**
 * Created by galuh on 7/4/17.
 */

class JobListLayoutPresenter(context: Context) : BasePresenter<JobListLayoutInteractor, JobListLayoutRouter, JobListLayoutViewBehavior>(context),
        JobListLayoutInteractorOutput {
    private var adapter: JobListAdapter? = null

    private var batchList: MutableList<Batch> = mutableListOf()

    init {
        this.adapter = JobListAdapter(context, this.batchList)
    }

    private val onItemSelection = object : ViewHolder.OnItemSelection<Batch> {
        override fun onSelected(data: Batch) {
            openShoppingList(data)
        }
    }

    override fun provideInteractor(): JobListLayoutInteractor? {
        return JobListLayoutInteractor()
    }

    override fun provideRouter(context: Context): JobListLayoutRouter? {
        return JobListLayoutRouter(context)
    }

    override fun showLoading(o: Boolean) {
        view()?.updateRefreshState(o)
    }

    override fun showData(batchList: List<Batch>) {
        this.batchList.clear()
        this.adapter?.notifyDataSetChanged()

        this.batchList.addAll(batchList)
        this.adapter?.notifyDataSetChanged()

        this.adapter?.setOnItemSelection(onItemSelection)

        view()?.setAdapter(this.adapter!!)
    }

    override fun unableLoadData() {
        view()?.unableLoadData()
    }

    override fun showEmpty() {
        view()?.showEmpty()
    }

    override fun openBatch(batch: Batch) {
        router().openActiveBatch(batch)
    }

    override fun openPayment(batch: Batch) {
        router().openBatchPayment(batch)
    }

    fun initialize() {
        interactor().fetchBatches()
    }

    fun updateStartedBatch(batch: Batch) {
        interactor().updateStartedBatch(batch)
    }

    private fun openShoppingList(data: Batch) {
        router().showShoppingList(data)
    }
}
