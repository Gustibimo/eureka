package com.happyfresh.fulfillment.modules.Tag

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import com.happyfresh.fulfillment.abstracts.BaseInteractor
import com.happyfresh.snowflakes.hoverfly.utils.FileUtils
import com.happyfresh.snowflakes.hoverfly.utils.LogUtils
import retrofit.android.MainThreadExecutor
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.util.*
import java.util.concurrent.ArrayBlockingQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit

/**
 * Created by galuh on 11/24/17.
 */
abstract class TagInteractor<out T : TagInteractorOutput> : BaseInteractor<T>() {

    abstract var photoFileFormat: String

    abstract var photoDirectory: String

    private var photoTagFilePath: String? = null
        get() {
            if (field == null) {
                field = createPhotoTagFilePath()
            }

            return field
        }

    private var isImageSaved: Boolean = false

    protected open var orderNumber: String? = null

    abstract fun initialize(extras: Bundle?)

    fun openCamera() {
        output().openCamera(photoTagFilePath)
    }

    fun processCameraOutput() {
        LogUtils.LOG("Camera Output: " + photoTagFilePath)

        val executor = ThreadPoolExecutor(1, 1, 300, TimeUnit.SECONDS, ArrayBlockingQueue<Runnable>(1))
        executor.execute {
            var out: FileOutputStream? = null
            try {
                val output = BitmapFactory.decodeFile(photoTagFilePath)

                var width = output.width
                var height = output.height
                val max = 1600

                if (width > height && width >= max) {
                    val aspectRatio = height.toDouble() / width.toDouble()
                    width = max
                    height = (width * aspectRatio).toInt()
                }
                else if (height >= max) {
                    val aspectRatio = height.toDouble() / width.toDouble()
                    height = max
                    width = (height / aspectRatio).toInt()
                }

                LogUtils.LOG("Size >> $width - $height")

                val resized = Bitmap.createScaledBitmap(output, width, height, true)

                val aFile = File(photoTagFilePath)
                out = FileOutputStream(aFile)
                resized.compress(Bitmap.CompressFormat.JPEG, 90, out)

                isImageSaved = true

                resized.recycle()
                output.recycle()

            }
            catch (oom: OutOfMemoryError) {
                oom.printStackTrace()

                MainThreadExecutor().execute {
                    isImageSaved = false
                    showErrorDialog(oom.localizedMessage)
                }
            }
            catch (e: Exception) {
                e.printStackTrace()
                isImageSaved = false
            }
            finally {
                try {
                    out?.close()
                }
                catch (e: IOException) {
                    e.printStackTrace()
                }

                MainThreadExecutor().execute {
                    if (isImageSaved) {
                        showTagImage(photoTagFilePath)
                    }

                    enableSubmitButton(isImageSaved)
                }
            }
        }
    }

    fun submitPhotoOutput(bundle: Bundle?) {
        output().submitPhotoTag(photoTagFilePath, bundle)
    }

    private fun createPhotoTagFilePath(): String? {
        var filePath: String? = null

        val nextInt = Math.abs(Random().nextInt())
        val fileName = String.format(photoFileFormat, orderNumber, nextInt)

        context?.let { ctx ->
            var imageDir = FileUtils.getCacheDir(ctx, photoDirectory).apply {
                if (!this.exists()) this.mkdirs()
            }

            var imageFile: File? = null
            try {
                imageFile = File(imageDir, fileName).apply { createNewFile() }
            }
            catch (e: IOException) {
                try {
                    imageDir = FileUtils.getFilesDir(ctx, photoDirectory).apply {
                        if (!this.exists()) this.mkdirs()
                    }

                    imageFile = File(imageDir, fileName).apply { createNewFile() }
                }
                catch (e1: IOException) {
                }
            }
            finally {
                imageFile?.let {
                    filePath = it.absolutePath
                }
            }
        }

        return filePath
    }

    private fun showErrorDialog(errorMessage: String) {
        output().showErrorDialog(errorMessage)
    }

    private fun showTagImage(imagePath: String?) {
        try {
            val imageFile = FileUtils.getFile(imagePath)
            output().showTagImage(imageFile)
        }
        catch (e: FileNotFoundException) {
            output().hideCameraIndicator()
            e.printStackTrace()
        }
        catch (e: FileUtils.FileEmptyException) {

        }
    }

    private fun enableSubmitButton(isEnabled: Boolean) {
        output().enableSubmitButton(isEnabled)
    }
}