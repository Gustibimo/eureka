package com.happyfresh.fulfillment.modules.LineItem.interactors

import com.happyfresh.fulfillment.modules.LineItem.outputs.LineItemDetailFragmentInteractorOutput
import com.happyfresh.snowflakes.hoverfly.services.ShipmentService
import com.happyfresh.snowflakes.hoverfly.shared.events.*
import com.shopper.utils.LogUtils
import com.squareup.otto.Subscribe

/**
 * Created by ifranseda on 10/10/2017.
 */

class LineItemDetailFragmentInteractor : LineItemDetailInteractor<LineItemDetailFragmentInteractorOutput>() {

    private val shipmentService: ShipmentService by lazy { ShipmentService() }

    override fun shouldListenEventBus(): Boolean {
        return true
    }

    fun prepareWeightDiffForOrder(typeId: Int, newValue: String) {
        lineItem?.shipment?.order?.number?.let {
            output().photoWeightDiffForOrder(it, typeId, newValue)
        }
    }

    fun preparePriceChangeForOrder(newValue: String) {
        lineItem?.shipment?.order?.number?.let {
            output().photoPriceChangeForOrder(it, newValue)
        }
    }

    fun prepareProductMismatch() {
        lineItem?.shipment?.order?.number?.let {
            output().photoProductMismatch(it)
        }
    }

    fun removeProductMismatch() {
        lineItem?.let { savedItem ->
            savedItem.shipment?.remoteId?.let { shipmentId ->
                shipmentService.removeFlagMismatch(shipmentId, savedItem)
            }
        }
    }

    fun removeFlagItem() {
        val shipmentId = lineItem?.shipment?.remoteId
        val variantId = lineItem?.variant?.remoteId

        if (shipmentId != null && variantId != null) {
            shipmentService.removeFlagStockOut(shipmentId, variantId)
        }
    }

    fun flagItem(typeId: Int?, detail: String?) {
        val shipmentId = lineItem?.shipment?.remoteId
        val variantId = lineItem?.variant?.remoteId

        if (shipmentId != null && variantId != null && typeId != null) {
            shipmentService.flagStockOut(shipmentId, variantId, ShipmentService.Flag.OUT_OF_STOCK, typeId, detail)
        }
    }

    fun itemFound(quantity: Int, actualWeight: Double?, shopperNotesFulfilled: Boolean) {
        val shipmentId = lineItem?.shipment?.remoteId
        val variantId = lineItem?.variant?.remoteId

        if (shipmentId != null && variantId != null) {
            shipmentService.shop(shipmentId, variantId, quantity, actualWeight, shopperNotesFulfilled)
        }
    }

    fun weightDiff(imagePath: String, typeId: Int, newWeight: Double?) {
        val shipmentId = lineItem?.shipment?.remoteId
        val variantId = lineItem?.variant?.remoteId

        if (shipmentId != null && variantId != null) {
            newWeight?.let { weight ->
                shipmentService.flagStockOut(shipmentId, variantId, ShipmentService.Flag.OUT_OF_STOCK, typeId, null, weight, imagePath)
            }
        }
    }

    fun flagMismatch(imagePath: String) {
        lineItem?.let { savedItem ->
            savedItem.shipment?.remoteId?.let { shipmentId ->
                shipmentService.flagMismatch(shipmentId, savedItem, imagePath)
            }
        }
    }

    fun changePrice(imagePath: String, newPrice: Double?) {
        this.lineItem?.let { savedItem ->
            newPrice?.let { price ->
                this.shipmentService.changePrice(savedItem, price, imagePath)
            }
        }
    }

    fun closeFragment() {
        output().closeFragment()
    }

    @Subscribe
    fun shopFailed(event: ShopEventFailed) {
        this.reloadLineItem()
    }

    @Subscribe
    fun shopCompleted(event: ShopEvent) {
        lineItem?.load()
        if (lineItem?.shipment?.pendingCount == 0 || lineItem?.totalShopped == lineItem?.total) {
            closeFragment()
        } else {
            this.reloadLineItem()
        }
    }

    @Subscribe
    fun stockOutFailed(event: StockOutEventFailed) {
        this.reloadLineItem()
    }

    @Subscribe
    fun stockOutCompleted(event: StockOutEvent) {
        lineItem?.load()
        lineItem?.totalFlagged?.let {
            if (it > 0) {
                closeFragment()
                return
            }
        }

        this.reloadLineItem()
    }

    @Subscribe
    fun mismatchCompleted(event: MismatchEvent) {
        this.reloadLineItem()
    }

    @Subscribe
    fun mismatchFailed(event: MismatchEventFailed) {
        this.reloadLineItem()
    }

    @Subscribe
    fun priceChangedCompleted(event: PriceChangedEvent) {
        this.reloadLineItem()
    }
}