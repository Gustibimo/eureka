package com.happyfresh.fulfillment.modules.Tag.Weight

import android.content.Context
import android.os.Bundle
import com.happyfresh.fulfillment.abstracts.BaseActivity
import com.happyfresh.fulfillment.abstracts.BaseFragment
import com.happyfresh.fulfillment.modules.Tag.TagRouter

/**
 * Created by galuh on 11/24/17.
 */
class WeightTagRouter(context: Context) : TagRouter(context) {
    override fun viewClass(): Class<out BaseActivity<BaseFragment<*>, *>>? {
        return WeightTagActivity::class.java
    }

    fun startWithOrderNumber(orderNumber: String?, typeId: Int, newValue: String?) {
        val bundle = Bundle().apply {
            putString(ORDER_NUMBER, orderNumber)
            putInt(OUTPUT_TYPE_ID, typeId)
            putString(OUTPUT_NEW_VALUE, newValue)
        }

        startForResult(bundle, WeightDiffRequestCode)
    }
}