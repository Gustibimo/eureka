package com.happyfresh.fulfillment.modules.LineItem.outputs

interface LineItemDetailFragmentInteractorOutput : LineItemDetailInteractorOutput {

    fun photoWeightDiffForOrder(orderNumber: String?, typeId: Int, newValue: String)

    fun photoPriceChangeForOrder(orderNumber: String?, newValue: String)

    fun photoProductMismatch(orderNumber: String?)

    fun invalidateOptionMenu()

    fun closeFragment()
}