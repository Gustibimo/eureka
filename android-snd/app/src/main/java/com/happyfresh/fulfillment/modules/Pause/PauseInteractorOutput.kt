package com.happyfresh.fulfillment.modules.Pause

import com.happyfresh.fulfillment.abstracts.InteractorOutput

/**
 * Created by aldi on 7/26/17.
 */

interface PauseInteractorOutput : InteractorOutput {

    fun showDate(date: String)

    fun showTime(timer: String)

    fun backToJobList()
}