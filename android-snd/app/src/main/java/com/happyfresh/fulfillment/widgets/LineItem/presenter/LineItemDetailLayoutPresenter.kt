package com.happyfresh.fulfillment.widgets.LineItem.presenter

import android.content.Context
import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.fulfillment.widgets.LineItem.LineItemDetailLayoutInteractor
import com.happyfresh.fulfillment.widgets.LineItem.LineItemDetailLayoutInteractorOutput
import com.happyfresh.fulfillment.widgets.LineItem.LineItemDetailLayoutRouter
import com.happyfresh.fulfillment.widgets.LineItem.LineItemDetailLayoutViewBehavior
import com.happyfresh.snowflakes.hoverfly.models.LineItem
import com.happyfresh.snowflakes.hoverfly.models.Shipment

/**
 * Created by ifranseda on 10/01/2018.
 */
class LineItemDetailLayoutPresenter(context: Context) :
        BasePresenter<LineItemDetailLayoutInteractor, LineItemDetailLayoutRouter, LineItemDetailLayoutViewBehavior>(context),
        LineItemDetailLayoutInteractorOutput {

    override fun provideInteractor(): LineItemDetailLayoutInteractor {
        return LineItemDetailLayoutInteractor()
    }

    override fun provideRouter(context: Context): LineItemDetailLayoutRouter? {
        return LineItemDetailLayoutRouter(context)
    }

    fun showImageDetail(lineItem: LineItem) {
        lineItem.variant?.let {
            router().openDetailImages(it.images())
        }
    }

    fun prepareToggleChatButton(orderId: Long?) {
        interactor().prepareToggleChatButton(orderId)
    }

    fun prepareOpenChatScreen(shipment: Shipment?) {
        interactor().prepareOpenChatScreen(shipment)
    }

    override fun toggleChatButton(show: Boolean) {
        view()?.toggleChatButton(show)
    }

    override fun openChatScreen(shopperId: String, shipment: Shipment) {
        router().openChatScreen(shopperId, shipment)
    }
}