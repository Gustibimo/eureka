package com.happyfresh.fulfillment.modules.Shopper.ChatList

import android.content.Context
import android.os.Bundle
import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.fulfillment.widgets.ViewHolder
import com.happyfresh.snowflakes.hoverfly.config.Constant
import com.happyfresh.snowflakes.hoverfly.models.Shipment

/**
 * Created by kharda on 15/8/18
 */
class ShopperChatListPresenter(context: Context) : BasePresenter<ShopperChatListInteractor, ShopperChatListRouter, ShopperChatListViewBehavior>(context),
        ShopperChatListInteractorOutput {

    private var adapter: ShopperChatListAdapter? = null

    private var shipmentList: MutableList<Shipment> = mutableListOf()

    private val onItemSelection = object : ViewHolder.OnItemSelection<Shipment> {
        override fun onSelected(data: Shipment) {
            data.order?.let {
                interactor().prepareOpenChatScreen(data)
            }
        }
    }

    override fun provideInteractor(): ShopperChatListInteractor? {
        return ShopperChatListInteractor()
    }

    override fun provideRouter(context: Context): ShopperChatListRouter? {
        return ShopperChatListRouter(context)
    }

    fun initialize(extras: Bundle?) {
        val batchId: Long? = extras?.getLong(Constant.BATCH_ID)
        interactor().initialize(batchId)
    }

    fun addChatHandler() {
        interactor().addChatHandler()
    }

    fun removeChatHandler() {
        interactor().removeChatHandler()
    }

    override fun showChatList(shipmentList: List<Shipment>) {
        this.shipmentList.clear()
        this.shipmentList.addAll(shipmentList)
        if (this.adapter != null) {
            this.adapter?.notifyDataSetChanged()
        } else {
            this.adapter = ShopperChatListAdapter(context, this.shipmentList)
            this.adapter?.setOnItemSelection(onItemSelection)
            view()?.setAdapter(this.adapter!!)
        }
    }

    override fun openChatScreen(shopperId: String, shipment: Shipment) {
        router().openChatScreen(shopperId, shipment)
    }
}