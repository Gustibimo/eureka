package com.happyfresh.fulfillment.modules.LineItem.behaviors

import com.happyfresh.snowflakes.hoverfly.models.FeedbackType

interface LineItemDetailFragmentViewBehavior : LineItemDetailViewBehavior {

    fun setStockOutFeedbackList(feedbackList: List<FeedbackType>)

    fun invalidateOptionMenu()

    fun closeFragment()
}