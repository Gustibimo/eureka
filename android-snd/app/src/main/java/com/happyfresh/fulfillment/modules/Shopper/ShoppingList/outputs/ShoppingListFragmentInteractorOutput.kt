package com.happyfresh.fulfillment.modules.Shopper.ShoppingList.outputs

import com.happyfresh.snowflakes.hoverfly.models.Batch

/**
 * Created by ifranseda on 09/10/2017.
 */

interface ShoppingListFragmentInteractorOutput : ShoppingListInteractorOutput {

    fun openFinalizeItems(batch: Batch)

    fun unableToFinalize()

    fun showAlertIncompleteLineItem(pendingCounter: Int)

    fun showProgress(show: Boolean)

    fun showAlertUnableCheckProductSample()

    fun openPaymentScreen(batchId: Long, stockLocationId: Long)

    fun openProductSampleScreen(batchId: Long)

    fun toggleStartShoppingButton(isEnable: Boolean)
}