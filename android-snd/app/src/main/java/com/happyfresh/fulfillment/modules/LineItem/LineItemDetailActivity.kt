package com.happyfresh.fulfillment.modules.LineItem

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.happyfresh.fulfillment.abstracts.BaseActivity
import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.fulfillment.modules.LineItem.behaviors.LineItemDetailViewBehavior
import com.happyfresh.snowflakes.hoverfly.models.LineItem

/**
 * Created by ifranseda on 10/10/2017.
 */

class LineItemDetailActivity : BaseActivity<LineItemDetailFragment, BasePresenter<*, *, *>>(), LineItemDetailViewBehavior {

    public override fun provideFragment(bundle: Bundle?): LineItemDetailFragment? {
        return LineItemDetailFragment().apply { arguments = bundle }
    }

    override fun showBackButton(): Boolean {
        return true
    }

    override fun needTranslucentToolbar(): Boolean {
        return true
    }

    override fun showLineItem(lineItem: LineItem) {
        title = null
    }

    override fun title(): String? {
        return String()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            fragment()?.onActivityResult(requestCode, resultCode, data)
        }
        else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }
}
