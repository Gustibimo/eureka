package com.happyfresh.fulfillment.widgets.ShoppingList

import android.content.Context
import android.view.View
import butterknife.BindView
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.fulfillment.widgets.BaseRecyclerViewHolder
import com.happyfresh.fulfillment.widgets.LineItem.view.LineItemLayout
import com.happyfresh.snowflakes.hoverfly.models.LineItem
import com.happyfresh.snowflakes.hoverfly.models.Shipment
import com.happyfresh.snowflakes.hoverfly.models.Variant

/**
 * Created by galuh on 10/2/17.
 */

class ShoppingListLayoutViewHolder(context: Context, itemView: View) : BaseRecyclerViewHolder<BasePresenter<*, *, *>, LineItem>(context, itemView) {
    @JvmField
    @BindView(R.id.line_item)
    var lineItemLayout: LineItemLayout<BasePresenter<*, *, *>>? = null

    override fun bind(data: LineItem) {
        super.bind(data)
        lineItemLayout?.lineItem = data
    }

    fun bindCategory(data: LineItem, previousData: LineItem?) {
        val name = if (data.taxonName.equals(previousData?.taxonName)) {
            null
        }
        else {
            data.taxonName
        }

        lineItemLayout?.setCategory(name)
    }

    fun bindShoppingBag(shipment: Shipment?) {
        val lineItem = LineItem()
        val variant = Variant()

        val shoppingBagDummyId = -999L
        val shoppingBagText = this.context.getString(R.string.shopping_bag);

        variant.name = shoppingBagText
        variant.remoteId = shoppingBagDummyId

        lineItem.remoteId = shoppingBagDummyId
        lineItem.variant = variant
        lineItem.isShoppingBag = true
        lineItem.shipment = shipment
        lineItem.replacementType = ""

        lineItem.taxonOrder = Int.MAX_VALUE

        bind(lineItem)

        lineItemLayout?.setCategory(shoppingBagText)
    }
}