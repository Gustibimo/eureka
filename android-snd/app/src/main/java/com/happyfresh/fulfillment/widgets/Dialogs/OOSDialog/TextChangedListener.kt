package com.happyfresh.fulfillment.widgets.Dialogs.OOSDialog

import android.text.TextWatcher

/**
 * Created by galuh on 7/25/17.
 */

abstract class TextChangedListener : TextWatcher {

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
}