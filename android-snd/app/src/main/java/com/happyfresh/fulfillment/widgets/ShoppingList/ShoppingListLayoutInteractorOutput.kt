package com.happyfresh.fulfillment.widgets.ShoppingList

import com.happyfresh.fulfillment.abstracts.InteractorOutput
import com.happyfresh.snowflakes.hoverfly.models.LineItem

/**
 * Created by ifranseda on 10/6/17.
 */

interface ShoppingListLayoutInteractorOutput : InteractorOutput {

    fun prepareShoppingList(items: List<LineItem>?)

    fun clearShoppingList()

    fun shoppingNotInProgress(o: Boolean)

    fun showToast(message: String)
}