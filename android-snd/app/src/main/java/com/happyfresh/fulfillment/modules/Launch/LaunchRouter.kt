package com.happyfresh.fulfillment.modules.Launch

import android.app.Activity
import android.content.Context
import com.happyfresh.fulfillment.abstracts.BaseRouter
import com.happyfresh.fulfillment.modules.ChooseStore.ChooseStoreRouter
import com.happyfresh.fulfillment.modules.Login.LoginRouter
import com.happyfresh.happychat.ChatRouter
import com.happyfresh.snowflakes.hoverfly.Sprinkles
import com.happyfresh.snowflakes.hoverfly.models.payload.SendBirdNotificationPayload
import com.happyfresh.snowflakes.hoverfly.stores.UserStore

/**
 * Created by galuh on 6/10/17.
 */

class LaunchRouter(context: Context) : BaseRouter(context) {

    private val userStore = Sprinkles.stores(UserStore::class.java)

    private var loginRouter: LoginRouter? = null

    private var chooseStoreRouter: ChooseStoreRouter? = null

    fun startRoot() {
        if (userStore?.hasToken == true) {
            chooseStoreRouter = ChooseStoreRouter(context)
            chooseStoreRouter?.startChooseStore()
        }
        else {
            loginRouter = LoginRouter(context)
            loginRouter?.startLogin()
        }
    }

    fun openChatScreen(notificationData: SendBirdNotificationPayload) {
        val chatRouter = ChatRouter(context)
        chatRouter.putUseCustomHeader(true)
        chatRouter.open(notificationData.recipient?.id!!, notificationData.channel?.channelUrl!!)
    }

    override fun detach() {
        (context as Activity).finish()
    }
}
