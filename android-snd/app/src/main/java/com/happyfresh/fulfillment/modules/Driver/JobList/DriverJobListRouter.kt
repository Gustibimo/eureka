package com.happyfresh.fulfillment.modules.Driver.JobList

import android.app.Activity
import android.content.Context

import com.happyfresh.fulfillment.abstracts.BaseActivity
import com.happyfresh.fulfillment.abstracts.BaseFragment
import com.happyfresh.fulfillment.abstracts.BaseRouter

/**
 * Created by galuh on 6/9/17.
 */

class DriverJobListRouter(context: Context) : BaseRouter(context) {

    override fun detach() {
        (context as Activity).finish()
    }

    override fun viewClass(): Class<out BaseActivity<out BaseFragment<*>, *>>? {
        return DriverJobListActivity::class.java
    }

    fun startDriverJobList() {
        start()
    }
}
