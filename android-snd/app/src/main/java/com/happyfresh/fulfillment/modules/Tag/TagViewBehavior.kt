package com.happyfresh.fulfillment.modules.Tag

import com.happyfresh.fulfillment.abstracts.ViewBehavior
import java.io.File

/**
 * Created by galuh on 11/24/17.
 */
interface TagViewBehavior : ViewBehavior {

    fun showErrorDialog(errorMessage: String)

    fun showTagImage(imageFile: File)

    fun hideCameraIndicator()

    fun enableSubmitButton(isEnabled: Boolean)
}