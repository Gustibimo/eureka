package com.happyfresh.fulfillment.modules.Tag.Weight

import android.content.Context
import android.os.Bundle
import com.happyfresh.fulfillment.modules.Tag.TagPresenter
import com.happyfresh.fulfillment.modules.Tag.TagViewBehavior
import com.happyfresh.snowflakes.hoverfly.config.Constant

/**
 * Created by galuh on 11/24/17.
 */
class WeightTagPresenter(context: Context) : TagPresenter<WeightTagInteractor, WeightTagRouter, TagViewBehavior>(context) {

    init {
        interactor().context = context
    }

    override fun provideInteractor(): WeightTagInteractor? {
        return WeightTagInteractor()
    }

    override fun provideRouter(context: Context): WeightTagRouter? {
        return WeightTagRouter(context)
    }

    fun initialize(extras: Bundle?) {
        interactor().initialize(extras)
    }
}