package com.happyfresh.fulfillment.widgets.FinalizeList

import android.content.Context
import android.view.View
import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.fulfillment.widgets.ViewHolder
import com.happyfresh.snowflakes.hoverfly.models.LineItem
import com.happyfresh.snowflakes.hoverfly.models.Shipment
import com.shopper.interfaces.OutOfStockItemListener

/**
 * Created by ifranseda on 03/01/2018.
 */
class FinalizeListLayoutPresenter(context: Context) :
        BasePresenter<FinalizeListLayoutInteractor, FinalizeListLayoutRouter, FinalizeListLayoutViewBehavior>(context),
        FinalizeListLayoutInteractorOutput, OutOfStockItemListener {
    private var adapter: FinalizeListLayoutAdapter? = null

    private val onItemSelection = object : ViewHolder.OnItemSelection<LineItem> {
        override fun onSelected(data: LineItem) {
            router().searchForReplacement(data)
        }
    }

    override fun provideInteractor(): FinalizeListLayoutInteractor? {
        return FinalizeListLayoutInteractor()
    }

    override fun provideRouter(context: Context): FinalizeListLayoutRouter? {
        return FinalizeListLayoutRouter(context)
    }

    override fun drawStockOutItems(items: List<LineItem>) {
        view()?.hideProgress()

        this.adapter = FinalizeListLayoutAdapter(context, items)

        this.adapter?.let {
            it.setOnItemSelection(onItemSelection)
            view()?.setAdapter(it)
        }
    }

    override fun reloadFinalizeList() {
        view()?.reload()
    }

    // region OutOfStockItemListener

    override fun searchProduct(view: View?, lineItem: LineItem?) {

    }

    override fun showShopperReplacement(lineItem: LineItem?) {

    }

    override fun finalize(shipment: Shipment?, position: Int) {
        view()?.showFinalizeDialog(shipment!!, position)
    }

    override fun callCustomer(shipment: Shipment?) {

    }

    // endregion

    // region FinalizeListLayoutInteractorOutput

    override fun showProgress(show: Boolean) {

    }

    override fun showAlertUnableCheckProductSample() {
        //view()?.showAlertUnableCheckProductSample()
    }

    override fun openPaymentScreen(batchId: Long, stockLocationId: Long) {
        router().openPaymentScreen(batchId, stockLocationId)
    }

    override fun openProductSampleScreen(batchId: Long) {
        router().openProductSampleScreen(batchId)
    }

    // endregion

    fun setBatch(batchId: Long) {
        interactor().setBatch(batchId)
    }

    fun loadFinalizationList() {
        view()?.showProgress()

        interactor().loadData()
    }

    fun prepareSetOOSItemListener() {
        this.adapter?.setOOSItemListener(this)
    }

    fun prepareFinalize(shipment: Shipment, position: Int) {
        interactor().finalize(shipment, position)
    }

    fun hideButtonProgress() {
        this.adapter?.hideButtonProgress()
    }
}