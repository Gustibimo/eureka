package com.happyfresh.fulfillment.modules.Shopper.JobList

import android.content.Context
import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.snowflakes.hoverfly.models.PendingHandover
import com.happyfresh.snowflakes.hoverfly.models.response.VersionUpdate

/**
 * Created by galuh on 6/9/17.
 */

class ShopperJobListPresenter(context: Context) : BasePresenter<ShopperJobListInteractor, ShopperJobListRouter, ShopperJobListViewBehavior>(context),
        ShopperJobListInteractorOutput {

    init {
        interactor().setActivityContext(context)
    }

    override fun provideInteractor(): ShopperJobListInteractor? {
        return ShopperJobListInteractor()
    }

    override fun provideRouter(context: Context): ShopperJobListRouter? {
        return ShopperJobListRouter(context)
    }

    fun pause() {
        interactor().pause()
    }

    fun askToClockOut() {
        interactor().askToClockOut()
    }

    fun clockOut() {
        interactor().clockOut()
    }

    fun checkPauseStatus() {
        interactor().checkPauseStatus()
    }

    fun getPendingHandover() {
        interactor().getPendingHandover()
    }

    fun prepareOpenPendingHandover() {
        interactor().openPendingHandover()
    }

    //    override fun drawBatchesNotFound() {
    //        view()?.showBatchesNotFound()
    //    }
    //
    //    override fun hideBatchesNotFound() {
    //        view()?.hideBatchesNotFound()
    //    }
    //
    //    override fun drawBatches(batches: List<Batch>) {
    //        view()?.showBatches(batches)
    //    }
    //
    //    override fun drawUnableLoadData() {
    //        view()?.showUnableLoadData()
    //    }
    //
    //    override fun resetListData() {
    //        view()?.resetListData()
    //    }
    //
    //    override fun setLoading(loading: Boolean) {
    //        view()?.setLoading(loading)
    //    }
    //
    //    override fun swipeRefreshing(isRefreshing: Boolean) {
    //        view()?.swipeRefreshing(isRefreshing)
    //    }
    //
    //    override fun drawJobHasStarted(batch: Batch) {
    //        view()?.showJobHasStarted(batch)
    //    }
    //
    //    override fun drawShopAlreadyStarted(message: String) {
    //        view()?.showShopAlreadyStarted(message)
    //    }
    //
    //    override fun drawRefreshJobDialog() {
    //        view()?.showRefreshJobDialog()
    //    }

    override fun openPause() {
        router().openPause()
    }

    override fun askToClockOut(storeLocation: String?) {
        view()?.showClockOutDialog(storeLocation)
    }

    override fun showRequireUpdateDialog(data: VersionUpdate) {
        view()?.showRequireUpdateDialog(data)
    }

    override fun openChooseStore() {
        router().openChooseStore()
    }

    override fun openPendingHandover(pendingHandoverList: List<PendingHandover>?) {
        if (pendingHandoverList != null) {
            router().openPendingHandover(pendingHandoverList)
        } else {
            router().openPendingHandover()
        }
    }

    override fun togglePendingHandoverIcon(isNotEmpty: Boolean) {
        view()?.togglePendingHandoverIcon(isNotEmpty)
    }
}
