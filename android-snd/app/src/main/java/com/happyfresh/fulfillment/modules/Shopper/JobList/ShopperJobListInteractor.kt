package com.happyfresh.fulfillment.modules.Shopper.JobList

import android.content.Context
import com.happyfresh.fulfillment.abstracts.BaseInteractor
import com.happyfresh.snowflakes.hoverfly.Sprinkles
import com.happyfresh.snowflakes.hoverfly.models.PendingHandover
import com.happyfresh.snowflakes.hoverfly.models.response.PauseResponse
import com.happyfresh.snowflakes.hoverfly.models.response.PendingHandoverListResponse
import com.happyfresh.snowflakes.hoverfly.models.response.StatusResponse
import com.happyfresh.snowflakes.hoverfly.services.AppService
import com.happyfresh.snowflakes.hoverfly.services.BatchService
import com.happyfresh.snowflakes.hoverfly.shared.events.RequireUpdateEvent
import com.happyfresh.snowflakes.hoverfly.shared.events.TogglePendingHandoverIconEvent
import com.happyfresh.snowflakes.hoverfly.shared.subscribers.ServiceSubscriber
import com.happyfresh.snowflakes.hoverfly.stores.StockLocationStore
import com.happyfresh.snowflakes.hoverfly.stores.UserStore
import com.happyfresh.snowflakes.hoverfly.utils.LogUtils
import com.squareup.otto.Subscribe

/**
 * Created by galuh on 6/9/17.
 */

class ShopperJobListInteractor : BaseInteractor<ShopperJobListInteractorOutput>() {

    private val userStore = Sprinkles.stores(UserStore::class.java)

    private val stockLocationStore = Sprinkles.stores(StockLocationStore::class.java)

    private val appService = Sprinkles.service(AppService::class.java)

    private val batchService by lazy { BatchService() }

    private var pendingHandoverList: List<PendingHandover> = ArrayList()

    private var fetchDataTime: Long = 0L

    private var reloadDataLimit: Int = 30

    override fun shouldListenEventBus(): Boolean {
        return true
    }

    fun setActivityContext(context: Context) {
        this.context = context
    }

    fun checkPauseStatus() {
        val statusObservable = appService.status()
        val subscriber = object : ServiceSubscriber<StatusResponse>() {
            override fun onFailure(e: Throwable) {
                LogUtils.LOG(e.localizedMessage)
            }

            override fun onSuccess(data: StatusResponse) {
                if (data.workingStatus == StatusResponse.WorkingStatus.PAUSED) {
                    userStore?.onPaused = true

                    dispatch()
                }
            }
        }

        subscriptions().add(statusObservable.subscribe(subscriber)!!)
    }

    fun dispatch() {
        redirectToPausePage()
    }

    fun pause() {
        val observable = appService.pause()
        val subscriber = object : ServiceSubscriber<PauseResponse>() {
            override fun onFailure(e: Throwable) {
                e.printStackTrace()
            }

            override fun onSuccess(data: PauseResponse) {
                userStore?.onPaused = true

                redirectToPausePage()
            }
        }

        subscriptions().add(observable.subscribe(subscriber)!!)
    }

    fun askToClockOut() {
        output().askToClockOut(stockLocationStore?.currentStoreName)
    }

    fun clockOut() {
        val observable = appService.clockOut()
        val subscriber = object : ServiceSubscriber<StatusResponse>() {
            override fun onFailure(e: Throwable) {
                e.printStackTrace()
            }

            override fun onSuccess(data: StatusResponse) {
                userStore?.workingStatus = false
                stockLocationStore?.currentStoreName = null

                redirectToChooseStorePage()
            }
        }

        subscriptions().add(observable.subscribe(subscriber)!!)
    }

    fun getPendingHandover() {
        val observable = batchService.pendingHandoverList()
        val subscriber = object : ServiceSubscriber<PendingHandoverListResponse>() {
            override fun onFailure(e: Throwable) {
                e.printStackTrace()
            }

            override fun onSuccess(data: PendingHandoverListResponse) {
                fetchDataTime = System.currentTimeMillis()
                pendingHandoverList = data.pendingHandovers
                output().togglePendingHandoverIcon(pendingHandoverList.isNotEmpty())
            }
        }

        subscriptions().add(observable.subscribe(subscriber)!!)
    }

    fun openPendingHandover() {
        if (((System.currentTimeMillis() - fetchDataTime) / 1000) > reloadDataLimit) {
            output().openPendingHandover(null)
        } else {
            output().openPendingHandover(pendingHandoverList)
        }
    }

    fun redirectToPausePage() {
        output().openPause()
    }

    fun redirectToChooseStorePage() {
        output().openChooseStore()
    }

    @Subscribe
    fun requireUpdate(event: RequireUpdateEvent) {
        output().showRequireUpdateDialog(event.data)
    }

    @Subscribe
    fun togglePendingHandoverIcon(event: TogglePendingHandoverIconEvent) {
        output().togglePendingHandoverIcon(event.data)
    }
//
//    fun loadBatch() {
//        fetchActive()
//    }
//
//    fun startJob(batch: Batch) {
//        isStarting = true
//
//        val observable = batchService.start(batch.remoteId!!).doOnNext { it.save() }
//        val subscriber = object : ServiceSubscriber<Batch>() {
//            override fun onFailure(e: Throwable) {
//                isStarting = false
//
//                startJobFailed(e, batch)
//                fetchActive()
//            }
//
//            override fun onSuccess(data: Batch) {
//                openJobDetail(data)
//
//                isStarting = false
//            }
//        }
//
//        subscriptions().add(observable?.subscribe(subscriber)!!)
//    }
//
//    private fun fetchActive() {
//        if (isStarting || isRefreshing) {
//            return
//        }
//
//        output().swipeRefreshing(true)
//        output().hideBatchesNotFound()
//
//        output().resetListData()
//        output().setLoading(false)
//
//        isRefreshing = true
//
//        val showPromotions = false
//
//        val observable = batchService.active(showPromotions)
//        val subscriber = object : ServiceSubscriber<BatchShoppingListResponse>() {
//            override fun onFailure(e: Throwable) {
//                isRefreshing = false
//
//                fetchAvailableBatch()
//            }
//
//            override fun onSuccess(data: BatchShoppingListResponse) {
//                isRefreshing = false
//
//                output().swipeRefreshing(false)
//                openJobDetail(data.batch!!)
//            }
//        }
//
//        subscriptions().add(observable.subscribe(subscriber)!!)
//    }
//
//    private fun fetchAvailableBatch() {
//        val observable = batchService.available()
//        val subscriber = object : ServiceSubscriber<BatchesResponse>() {
//            override fun onFailure(e: Throwable) {
//                output().swipeRefreshing(false)
//                isRefreshing = false
//
//                loadData(null)
//
//                output().drawUnableLoadData()
//            }
//
//            override fun onSuccess(data: BatchesResponse) {
//                output().swipeRefreshing(false)
//                isRefreshing = false
//
//                loadData(data)
//            }
//        }
//
//        subscriptions().add(observable.subscribe(subscriber)!!)
//    }
//
//    private fun loadData(data: BatchesResponse?) {
//        val batches = data?.batches
//
//        if (batches == null || batches.isEmpty()) {
//            output().drawBatchesNotFound()
//            return
//        }
//
//        if (!isStarting) {
//            Collections.sort(batches, { batch1, batch2 -> batch1.slot!!.startTime!!.compareTo(batch2.slot!!.startTime) })
//
//            output().resetListData()
//            output().drawBatches(batches)
//            output().setLoading(false)
//        }
//    }
//
//    private fun openJobDetail(batch: Batch) {
////        output().drawJobHasStarted(batch)
//        output().openShoppingList(batch)
//    }
//
//    private fun startJobFailed(e: Throwable, batch: Batch) {
//        if (e is HttpException && e.code() == 422) {
//            val jsonStr = String(e.response().errorBody().bytes())
//            val response = Gson().fromJson<Any>(jsonStr, ServerExceptionResponse::class.java) as ServerExceptionResponse
//
//            val type = response.type
//            if (type.equals(Constant.AlreadyStartedException, ignoreCase = true)) {
//                openJobDetail(batch)
//            }
//            else if (type.equals(Constant.InvalidStateError, ignoreCase = true)) {
//                var message = context!!.getString(R.string.shopping_already_taken)
//                if (response.message.equals("There are orders still processing")) {
//                    message = response.message
//                }
//
//                output().drawShopAlreadyStarted(message)
//            }
//            else if (type.equals(Constant.OrderCanceledException, ignoreCase = true)) {
//                output().drawRefreshJobDialog()
//            }
//        }
//    }
}
