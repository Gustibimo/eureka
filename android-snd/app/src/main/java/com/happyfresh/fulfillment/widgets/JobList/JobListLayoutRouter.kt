package com.happyfresh.fulfillment.widgets.JobList

import android.app.Activity
import android.content.Context
import android.content.Intent
import com.happyfresh.fulfillment.abstracts.BaseRouter
import com.happyfresh.fulfillment.modules.Shopper.ShoppingList.ShoppingListRouter
import com.happyfresh.snowflakes.hoverfly.models.Batch
import com.shopper.activities.BatchPaymentActivity
import com.shopper.activities.ProductSampleListActivity
import com.shopper.common.Constant

/**
 * Created by galuh on 10/10/17.
 */

class JobListLayoutRouter(context: Context) : BaseRouter(context) {

    private val shoppingListRouter by lazy { ShoppingListRouter(context) }

    override fun detach() {
        (context as Activity).finish()
    }

    fun showShoppingList(batch: Batch) {
        shoppingListRouter.startWithBatch(batch)
    }

    fun openActiveBatch(batch: Batch) {
        shoppingListRouter.startWithBatch(batch)
    }

    fun openBatchPayment(batch: Batch) {
        val intent = Intent(context, BatchPaymentActivity::class.java)

        intent.putExtra(Constant.BATCH_ID, batch.remoteId)
        intent.putExtra(Constant.STOCK_LOCATION_ID, batch.stockLocation().remoteId)
        intent.putExtra(ProductSampleListActivity.SELECTED_BATCH, batch.remoteId)

        (context as Activity).startActivity(intent)
    }
}