package com.happyfresh.fulfillment.modules.Login

import android.app.Activity
import android.content.Context

import com.happyfresh.fulfillment.abstracts.BaseActivity
import com.happyfresh.fulfillment.abstracts.BaseFragment
import com.happyfresh.fulfillment.abstracts.BaseRouter
import com.happyfresh.fulfillment.modules.ChooseStore.ChooseStoreRouter

/**
 * Created by galuh on 6/9/17.
 */

class LoginRouter(context: Context) : BaseRouter(context) {

    private var chooseStoreRouter: ChooseStoreRouter? = null

    override fun detach() {
        (context as Activity).finish()
    }

    override fun viewClass(): Class<out BaseActivity<BaseFragment<*>, *>>? {
        return LoginActivity::class.java
    }

    fun startLogin() {
        start()
    }

    private fun chooseStoreRouter(): ChooseStoreRouter {
        this.chooseStoreRouter = ChooseStoreRouter(context)
        return this.chooseStoreRouter!!
    }

    fun openChooseStore() {
        chooseStoreRouter().startChooseStore()
        detach()
    }
}
