package com.happyfresh.fulfillment.modules.Tag.Weight

import android.os.Bundle
import com.happyfresh.fulfillment.modules.Tag.TagInteractor
import com.happyfresh.fulfillment.modules.Tag.TagInteractorOutput
import com.happyfresh.fulfillment.modules.Tag.TagRouter

/**
 * Created by galuh on 11/24/17.
 */
class WeightTagInteractor : TagInteractor<TagInteractorOutput>() {
    override var photoFileFormat = "WEIGHTTAG_%s_%d.jpg"

    override var photoDirectory = "weighttags"

    var typeId: Int? = null

    override fun initialize(extras: Bundle?) {
        orderNumber = extras?.getString(TagRouter.ORDER_NUMBER)
        typeId = extras?.getInt(TagRouter.OUTPUT_TYPE_ID)
    }
}