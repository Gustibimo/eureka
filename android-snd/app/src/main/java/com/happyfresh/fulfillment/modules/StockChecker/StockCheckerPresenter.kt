package com.happyfresh.fulfillment.modules.StockChecker

import android.content.Context

import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.snowflakes.hoverfly.config.Spree

/**
 * Created by Novanto on 12/27/17.
 */

class StockCheckerPresenter(context: Context) : BasePresenter<StockCheckerInteractor, StockCheckerRouter, StockCheckerViewBehavior>(context),
        StockCheckerInteractorOutput {

    override fun provideRouter(context: Context): StockCheckerRouter? {
        return StockCheckerRouter(context)
    }

    override fun provideInteractor(): StockCheckerInteractor {
        return StockCheckerInteractor()
    }

    override fun prepareLoadUrl(url: String, accessToken: String) {
        val headers = HashMap<String, String>().apply { put(Spree.UserToken, accessToken) }
        view()?.loadUrl(url, headers)
    }

    fun prepareLoadUrlData() {
        interactor().loadUrlData()
    }
}