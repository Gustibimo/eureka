package com.happyfresh.fulfillment.widgets.FinalizeList

import android.content.Context
import android.view.View
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.widgets.BaseAdapter
import com.happyfresh.fulfillment.widgets.FinalizeList.viewholder.StockOutListItemViewHolder
import com.happyfresh.snowflakes.hoverfly.models.Job
import com.happyfresh.snowflakes.hoverfly.models.LineItem
import com.shopper.interfaces.OutOfStockItemListener

/**
 * Created by ifranseda on 02/01/2018.
 */
class FinalizeListLayoutAdapter(context: Context, data: List<LineItem>) : BaseAdapter<LineItem, StockOutListItemViewHolder>(context, data) {
    val viewTypes: MutableList<Int> = mutableListOf()

    var listener: OutOfStockItemListener? = null

    override fun itemLayoutResID(type: Int): Int {
        return when (type) {
            TOP, TOP_REPLACED               -> R.layout.layout_out_of_stock_item
            BOTTOM, BOTTOM_REPLACED         -> R.layout.layout_out_of_stock_item
            TOP_BOTTOM, TOP_BOTTOM_REPLACED -> R.layout.layout_out_of_stock_item
            TOP_FINALIZED                   -> R.layout.layout_out_of_stock_item_finalized
            FINALIZED                       -> R.layout.layout_out_of_stock_item_hidden
            else                            -> R.layout.layout_out_of_stock_item
        }
    }

    override fun provideViewHolder(view: View, type: Int): StockOutListItemViewHolder {
        return when (type) {
            TOP, TOP_REPLACED               -> StockOutListItemViewHolder(this.context, view, listener!!) //R.layout.layout_out_of_stock_item_top
            BOTTOM, BOTTOM_REPLACED         -> StockOutListItemViewHolder(this.context, view, listener!!) //R.layout.layout_out_of_stock_item_bottom
            TOP_BOTTOM, TOP_BOTTOM_REPLACED -> StockOutListItemViewHolder(this.context, view, listener!!) //R.layout.layout_out_of_stock_item_top_bottom
            TOP_FINALIZED                   -> StockOutListItemViewHolder(this.context, view, listener!!) //R.layout.layout_out_of_stock_item_finalized
            FINALIZED                       -> StockOutListItemViewHolder(this.context, view, listener!!) //R.layout.layout_out_of_stock_item_hidden
            else                            -> StockOutListItemViewHolder(this.context, view, listener!!)
        }
    }

    override fun getItemViewType(position: Int): Int {
        val item = data[position]
        val replaced = (item.totalReplacedShopper > 0 && item.replacementShopper != null)

        var prevItem: LineItem? = null
        var nextItem: LineItem? = null

        var shipmentFirstItem = false
        var shipmentLastItem = false

        try {
            prevItem = data[position - 1]
        }
        catch (ie: IndexOutOfBoundsException) {
        }

        try {
            nextItem = data[position + 1]
        }
        catch (ie: IndexOutOfBoundsException) {
        }

        if (prevItem == null && nextItem == null) {
            shipmentFirstItem = true
            shipmentLastItem = true
        }
        else if (prevItem == null && nextItem != null) {
            shipmentFirstItem = true
            shipmentLastItem = (item.shipment?.remoteId != nextItem.shipment?.remoteId)
        }
        else if (prevItem != null && nextItem != null) {
            shipmentFirstItem = (item.shipment?.remoteId != prevItem.shipment?.remoteId)
            shipmentLastItem = (item.shipment?.remoteId != nextItem.shipment?.remoteId)
        }
        else if (prevItem != null && nextItem == null) {
            shipmentFirstItem = (item.shipment?.remoteId != prevItem.shipment?.remoteId)
            shipmentLastItem = true
        }

        item.shipment?.shoppingJob?.let {
            if (Job.Progress.FINALIZE.equals(it.state!!, ignoreCase = true) || Job.Progress.FINISHED.equals(it.state!!, ignoreCase = true)) {
                val type = if (shipmentFirstItem) TOP_FINALIZED else FINALIZED
                viewTypes.add(position, type)

                return type
            }
        }

        val type = if (shipmentFirstItem && shipmentLastItem) {
            if (replaced) TOP_BOTTOM_REPLACED else TOP_BOTTOM
        }
        else if (!shipmentFirstItem && !shipmentLastItem) {
            if (replaced) MIDDLE_REPLACED else MIDDLE
        }
        else if (shipmentFirstItem && !shipmentLastItem) {
            if (replaced) TOP_REPLACED else TOP
        }
        else {
            if (replaced) BOTTOM_REPLACED else BOTTOM
        }

        viewTypes.add(position, type)

        return type
    }

    override fun onBindViewHolder(holder: StockOutListItemViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)

        val viewType = getItemViewType(position)

        holder.otherView(this.data[position], viewType)
    }

    fun setOOSItemListener(listener: OutOfStockItemListener) {
        this.listener = listener
    }

    fun hideButtonProgress() {
        notifyDataSetChanged()
    }

    companion object ViewType {
        const val TOP = 0
        const val MIDDLE = 1
        const val BOTTOM = 2
        const val TOP_BOTTOM = 3
        const val TOP_REPLACED = 4
        const val MIDDLE_REPLACED = 5
        const val BOTTOM_REPLACED = 6
        const val TOP_BOTTOM_REPLACED = 7
        const val TOP_FINALIZED = 8
        const val FINALIZED = 9
    }
}