package com.happyfresh.fulfillment.widgets.Dialogs.OOSDialog.view

import android.content.Context
import android.text.Editable
import android.text.TextUtils
import android.util.AttributeSet
import android.widget.EditText
import butterknife.BindView
import com.afollestad.materialdialogs.DialogAction
import com.afollestad.materialdialogs.MaterialDialog
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.fulfillment.abstracts.ViewBehavior
import com.happyfresh.fulfillment.widgets.BaseLayout
import com.happyfresh.fulfillment.widgets.Dialogs.OOSDialog.TextChangedListener

/**
 * Created by galuh on 11/10/17.
 */
class InputStockOutReasonDialogLayout @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
        BaseLayout<BasePresenter<*, *, *>>(context, attrs, defStyleAttr), ViewBehavior {

    @JvmField
    @BindView(R.id.oos_reason)
    var reasonInput: EditText? = null

    var dialog: MaterialDialog? = null

    private var feedbackTypeId: Int? = null

    override fun layoutResID(): Int {
        return R.layout.dialog_out_of_stock_reason
    }

    override fun providePresenter(): BasePresenter<*, *, *>? {
        return null
    }

    private var reasonInputTextChangedListener = object : TextChangedListener() {
        override fun afterTextChanged(editable: Editable?) {
            val editableNotEmpty = !TextUtils.isEmpty(editable.toString())

            dialog?.getActionButton(DialogAction.POSITIVE)?.isEnabled = editableNotEmpty
        }
    }

    fun showWithType(typeId: Int, callback: ReasonCallback) {
        this.feedbackTypeId = typeId

        reasonInput?.addTextChangedListener(reasonInputTextChangedListener)

        val builder = MaterialDialog.Builder(context)
        builder.title(R.string.alert_title_flag_enter_reason)
                .positiveText(R.string.alert_button_ok)
                .negativeText(R.string.alert_button_cancel)
                .customView(this, false)
                .onPositive { _, _ ->
                    if (reasonInput?.text?.isNotBlank() == true) {
                        callback.onReasonDefined(reasonInput?.text.toString())
                    }
                }

        builder.build().show()
    }

    interface ReasonCallback {
        fun onReasonDefined(reason: String)
    }
}