package com.happyfresh.fulfillment.modules.Shopper.ChatList.views

import android.content.Context
import android.view.View
import android.widget.TextView
import butterknife.BindView
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.fulfillment.widgets.BaseRecyclerViewHolder
import com.happyfresh.snowflakes.hoverfly.models.Shipment

/**
 * Created by kharda on 15/8/18
 */
class ShopperChatListViewHolder(context: Context, itemView: View) : BaseRecyclerViewHolder<BasePresenter<*, *, *>, Shipment>(context, itemView) {

    @JvmField
    @BindView(R.id.customer_name)
    internal var customerName: TextView? = null

    @JvmField
    @BindView(R.id.order_number)
    internal var orderNumber: TextView? = null

    @JvmField
    @BindView(R.id.unread_count)
    internal var unreadCount: TextView? = null

    override fun bind(data: Shipment) {
        super.bind(data)

        customerName?.text = data.customerName
        orderNumber?.text = data.order?.number
        orderNumber?.setTextColor(data.colorResource)

        val unreadChatCount = data.order?.chat?.unreadChatCount ?: 0
        if (unreadChatCount > 0) {
            unreadCount?.text = unreadChatCount.toString()
            unreadCount?.visibility = View.VISIBLE
        } else {
            unreadCount?.visibility = View.GONE
        }
    }
}