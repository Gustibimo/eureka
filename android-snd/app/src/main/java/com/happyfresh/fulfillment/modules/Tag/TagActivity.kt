package com.happyfresh.fulfillment.modules.Tag

import android.app.Activity
import android.content.Intent
import com.happyfresh.fulfillment.abstracts.BaseActivity
import com.happyfresh.fulfillment.abstracts.BasePresenter

/**
 * Created by galuh on 11/24/17.
 */
abstract class TagActivity<out V : TagFragment<*>, out P : BasePresenter<*, *, *>> : BaseActivity<V, P>() {
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            TagRouter.CAMERA_REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    fragment()?.processCameraOutput()
                }
                else {
                    fragment()?.hideProgressBar()
                    fragment()?.tryEnableSubmitButton()
                }
            }

            else                          -> {
                super.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

    override fun onBackPressed() {
        fragment()?.showDiscardNewValueDialog()
    }

    fun discardNewValueDialog() {
        super.onBackPressed()
    }
}