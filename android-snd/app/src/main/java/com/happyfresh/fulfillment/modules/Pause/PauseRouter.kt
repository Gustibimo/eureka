package com.happyfresh.fulfillment.modules.Pause

import android.app.Activity
import android.content.Context
import com.happyfresh.fulfillment.abstracts.BaseActivity
import com.happyfresh.fulfillment.abstracts.BaseFragment
import com.happyfresh.fulfillment.abstracts.BaseRouter
import com.happyfresh.fulfillment.modules.Shopper.JobList.ShopperJobListRouter

/**
 * Created by aldi on 7/26/17.
 */

class PauseRouter(context: Context) : BaseRouter(context) {

    private val shopperJobListRouter by lazy { ShopperJobListRouter(context) }

    override fun detach() {
        (context as Activity).finish()
    }

    override fun viewClass(): Class<out BaseActivity<BaseFragment<*>, *>>? {
        return PauseActivity::class.java
    }

    fun startPause() {
        start()
    }

    fun openShoppingJobList() {
        shopperJobListRouter.start()
        detach()
    }
}