package com.happyfresh.fulfillment.modules.Shopper.ChatList

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import butterknife.BindView
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.abstracts.BaseFragment

/**
 * Created by kharda on 15/8/18
 */
class ShopperChatListFragment : BaseFragment<ShopperChatListPresenter>(), ShopperChatListViewBehavior {

    @JvmField
    @BindView(R.id.recycler_list)
    internal var recyclerView: RecyclerView? = null

    override fun layoutResID(): Int {
        return R.layout.fragment_shopper_chat_list
    }

    override fun providePresenter(): ShopperChatListPresenter {
        return ShopperChatListPresenter(context)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter().initialize(this.bundle)
    }

    override fun onResume() {
        presenter().addChatHandler()
        super.onResume()
    }

    override fun onPause() {
        presenter().removeChatHandler()
        super.onPause()
    }

    override fun setAdapter(adapter: ShopperChatListAdapter) {
        val linearLayoutManager = LinearLayoutManager(context)
        recyclerView?.layoutManager = linearLayoutManager
        recyclerView?.adapter = adapter
    }
}