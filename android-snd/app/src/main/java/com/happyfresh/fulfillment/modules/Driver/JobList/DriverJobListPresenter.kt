package com.happyfresh.fulfillment.modules.Driver.JobList

import android.content.Context

import com.happyfresh.fulfillment.abstracts.BasePresenter

/**
 * Created by galuh on 6/9/17.
 */

class DriverJobListPresenter(context: Context) : BasePresenter<DriverJobListInteractor, DriverJobListRouter, DriverJobListViewBehavior>(context),
        DriverJobListInteractorOutput {

    override fun provideInteractor(): DriverJobListInteractor? {
        return DriverJobListInteractor()
    }

    override fun provideRouter(context: Context): DriverJobListRouter? {
        return DriverJobListRouter(context)
    }
}
