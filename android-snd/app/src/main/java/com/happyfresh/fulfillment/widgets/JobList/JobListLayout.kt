package com.happyfresh.fulfillment.widgets.JobList

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.TextView
import android.widget.Toast
import butterknife.BindView
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.widgets.BaseRecyclerViewLayout
import com.happyfresh.snowflakes.hoverfly.models.Batch

/**
 * Created by galuh on 7/4/17.
 */

class JobListLayout @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
        BaseRecyclerViewLayout<JobListAdapter, JobListLayoutPresenter>(context, attrs, defStyleAttr),
        JobListLayoutViewBehavior {

    @JvmField
    @BindView(R.id.not_found_container)
    var notFoundContainer: View? = null

    @JvmField
    @BindView(R.id.not_found)
    var notFound: TextView? = null

    override fun layoutResID(): Int {
        return R.layout.widget_job_list
    }

    override fun providePresenter(): JobListLayoutPresenter {
        return JobListLayoutPresenter(context)
    }

    override fun isSwipeRefreshLayoutEnabled(): Boolean {
        return true
    }

    override fun updateRefreshState(o: Boolean) {
        super.updateRefreshState(o)

        notFoundContainer?.visibility = View.GONE
    }

    override fun onSwipeRefresh() {
        fetchData()
    }

    override fun initialize() {
        super.initialize()

        fetchData()
    }

    override fun unableLoadData() {
        Toast.makeText(context, context.getString(R.string.toast_unable_load_data_from_server), Toast.LENGTH_LONG).show()
    }

    override fun showEmpty() {
        notFoundContainer?.visibility = View.VISIBLE
        notFound?.text = context.getString(R.string.job_not_found)
    }

    fun fetchData() {
        presenter().initialize()
    }

    fun updateStartedBatch(batch: Batch) {
        presenter().updateStartedBatch(batch)
    }
}