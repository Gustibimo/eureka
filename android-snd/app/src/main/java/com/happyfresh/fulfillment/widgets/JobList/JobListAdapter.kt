package com.happyfresh.fulfillment.widgets.JobList

import android.content.Context
import android.view.View
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.widgets.BaseAdapter
import com.happyfresh.fulfillment.widgets.JobList.views.ShopperJobListLayoutViewHolder
import com.happyfresh.snowflakes.hoverfly.models.Batch

/**
 * Created by galuh on 7/4/17.
 */

class JobListAdapter(context: Context, data: List<Batch>) : BaseAdapter<Batch, ShopperJobListLayoutViewHolder>(context, data) {
    override fun itemLayoutResID(type: Int): Int {
        return R.layout.widget_job_list_item
    }

    override fun provideViewHolder(view: View, type: Int): ShopperJobListLayoutViewHolder {
        return ShopperJobListLayoutViewHolder(this.context,
                view)
    }
}
