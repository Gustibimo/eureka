package com.happyfresh.fulfillment.widgets.LineItem.view

import android.content.Context
import android.graphics.Paint
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import butterknife.BindView
import butterknife.OnClick
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.widgets.Dialogs.PhoneCallDialog.PhoneCallDialog
import com.happyfresh.fulfillment.widgets.LineItem.LineItemDetailLayoutViewBehavior
import com.happyfresh.fulfillment.widgets.LineItem.presenter.LineItemDetailLayoutPresenter
import com.happyfresh.snowflakes.hoverfly.models.LineItem

/**
 * Created by ifranseda on 03/10/2017.
 */

class LineItemDetailLayout
@JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
        LineItemLayout<LineItemDetailLayoutPresenter>(context, attrs, defStyleAttr), LineItemDetailLayoutViewBehavior {
    @JvmField
    @BindView(R.id.item_flag_info)
    var headerFlag: TextView? = null

    @JvmField
    @BindView(R.id.item_sku_number)
    var itemSkuNumber: TextView? = null

    @JvmField
    @BindView(R.id.item_average_weight)
    var itemAverageWeight: TextView? = null

    @JvmField
    @BindView(R.id.item_customer_price)
    var itemCustomerPrice: TextView? = null

    @JvmField
    @BindView(R.id.item_product_mismatch_label)
    var itemProductMismatchLabel: TextView? = null

    @JvmField
    @BindView(R.id.item_store_price_new)
    var itemStorePriceNew: TextView? = null

    @JvmField
    @BindView(R.id.customer_chat)
    var chatButton: LinearLayout? = null

    override fun layoutResID(): Int {
        return R.layout.widget_line_item_detail
    }

    override fun providePresenter(): LineItemDetailLayoutPresenter? {
        return LineItemDetailLayoutPresenter(context)
    }

    override var lineItem: LineItem? = null
        set(value) {
            field = value

            showPromotionBanner()

            showOrderIndicator()

            showItem()
            showShopperNotes()

            showReplacementOptions()

            setEditPriceStatus()
            showPurchaseStatus()

            showProductMismatchLabel()

            showProductPriceChanged()

            presenter().prepareToggleChatButton(field?.orderId)
        }

    override fun showItem() {
        super.showItem()

        setCategory(this.lineItem?.taxonName)

        val shoppedCounter = this.lineItem?.totalShopped!! - this.lineItem?.totalReplacedCustomer!! - this.lineItem?.totalReplacedShopper!!
        val shoppedCount = context.getString(R.string.item_shopped_counter, shoppedCounter, this.lineItem?.total)

        itemCounter?.text = shoppedCount

        itemSkuNumber?.text = this.lineItem?.variant?.sku
        itemAverageWeight?.text = this.lineItem?.displayUnit
        itemCustomerPrice?.text = customerPrice()
    }

    private fun customerPrice(): String? {
        val price = this.lineItem?.displayAmount

        return if (this.lineItem?.hasNaturalUnit() == true) {
            String.format("%s/%s", this.lineItem?.singleDisplayAmount, this.lineItem?.displayAverageWeight)
        }
        else {
            price
        }
    }

    var canEditPriceStatus = false

    fun showCannotEditPrice() {
        AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.item_detail_cant_edit_dialog_title))
                .setMessage(context.getString(R.string.item_detail_cant_edit_dialog_message))
                .setPositiveButton(context.getString(R.string.item_detail_cant_edit_dialog_positive), { dialog, _ -> dialog.dismiss() })
    }

    override fun toggleChatButton(show: Boolean) {
        chatButton?.isEnabled = show
    }

    @OnClick(R.id.customer_call)
    fun callCustomer() {
        val phoneNumber: String? = lineItem!!.shipment!!.address!!.phoneNumber
        val customerName: String? = lineItem!!.shipment!!.customerName

        val phoneCallDialog = PhoneCallDialog(context, R.string.alert_title_call_customer, R.string.alert_message_call_customer, customerName, phoneNumber)
        phoneCallDialog.show()
    }

    @OnClick(R.id.customer_chat)
    fun chatCustomer() {
        presenter().prepareOpenChatScreen(this.lineItem?.shipment)
    }

    @OnClick(R.id.item_image)
    fun onImageClicked() {
        this.lineItem?.let {
            presenter().showImageDetail(it)
        }
    }

    private fun setEditPriceStatus() {
        lineItem?.displayBanner?.let {
            canEditPriceStatus = false

            showCannotEditPrice()

            return
        }

        canEditPriceStatus = true
    }

    private fun showProductMismatchLabel() {
        if (this.lineItem?.isProductMismatch == true) {
            itemProductMismatchLabel?.visibility = View.VISIBLE
        }
        else {
            itemProductMismatchLabel?.visibility = View.GONE
        }
    }

    private fun showProductPriceChanged() {
        if (this.lineItem?.priceAmendment != null) {
            itemStorePriceNew?.visibility = View.VISIBLE
            itemStorePriceNew?.text = itemPrice()

            itemPrice?.text = this.lineItem?.displayCostPrice

            itemPrice?.paintFlags?.or(Paint.STRIKE_THRU_TEXT_FLAG)?.let {
                itemPrice?.paintFlags = it
                itemPrice?.setTextColor(ContextCompat.getColor(context, R.color.snd_color_attention))
            }
        }
        else {
            itemStorePriceNew?.visibility = View.GONE
        }
    }
}