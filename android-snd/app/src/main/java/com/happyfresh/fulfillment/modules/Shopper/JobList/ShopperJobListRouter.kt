package com.happyfresh.fulfillment.modules.Shopper.JobList

import android.app.Activity
import android.content.Context
import com.happyfresh.fulfillment.abstracts.BaseActivity
import com.happyfresh.fulfillment.abstracts.BaseFragment
import com.happyfresh.fulfillment.abstracts.BaseRouter
import com.happyfresh.fulfillment.modules.ChooseStore.ChooseStoreRouter
import com.happyfresh.fulfillment.modules.Pause.PauseRouter
import com.happyfresh.fulfillment.modules.Shopper.PendingHandoverList.ShopperPendingHandoverRouter
import com.happyfresh.snowflakes.hoverfly.models.PendingHandover

/**
 * Created by galuh on 6/9/17.
 */

class ShopperJobListRouter(context: Context) : BaseRouter(context) {

    private val pauseRouter by lazy { PauseRouter(context) }

    private val chooseStoreRouter by lazy { ChooseStoreRouter(context) }

    private val pendingHandoverListRouter by lazy { ShopperPendingHandoverRouter(context) }

    override fun detach() {
        (context as Activity).finish()
    }

    override fun viewClass(): Class<out BaseActivity<BaseFragment<*>, *>>? {
        return ShopperJobListActivity::class.java
    }

    fun startShopperJobList() {
        start()
    }

    fun openPause() {
        pauseRouter.startPause()
        detach()
    }

    fun openChooseStore() {
        chooseStoreRouter.startChooseStore()
        detach()
    }

    fun openPendingHandover() {
        pendingHandoverListRouter.start()
    }

    fun openPendingHandover(pendingHandoverList: List<PendingHandover>) {
        pendingHandoverListRouter.startWithCurrentData(pendingHandoverList)
    }
}
