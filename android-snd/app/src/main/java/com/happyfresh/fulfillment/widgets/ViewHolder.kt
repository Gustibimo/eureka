package com.happyfresh.fulfillment.widgets

import com.happyfresh.snowflakes.hoverfly.models.IServiceModel

/**
 * Created by ifranseda on 03/02/2017.
 */

interface ViewHolder<O : IServiceModel> {

    fun bind(data: O)

    fun setOnItemClickListener(listener: OnItemSelection<O>?)

    interface OnItemSelection<in T : IServiceModel> {
        fun onSelected(data: T)
    }
}