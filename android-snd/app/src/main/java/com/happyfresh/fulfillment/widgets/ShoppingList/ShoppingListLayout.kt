package com.happyfresh.fulfillment.widgets.ShoppingList

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.Toast
import butterknife.BindView
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.modules.Shopper.ShoppingList.interactors.ShoppingListActivityInteractor.ShoppingProcessEvent
import com.happyfresh.fulfillment.modules.Shopper.ShoppingList.interactors.ShoppingListActivityInteractor.ShoppingProcessEvent.Step
import com.happyfresh.fulfillment.widgets.BaseRecyclerViewLayout
import com.happyfresh.snowflakes.hoverfly.models.Batch
import com.happyfresh.snowflakes.hoverfly.shared.events.*
import com.squareup.otto.Subscribe

/**
 * Created by galuh on 10/2/17.
 */

class ShoppingListLayout @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
        BaseRecyclerViewLayout<ShoppingListLayoutAdapter, ShoppingListLayoutPresenter>(context, attrs, defStyleAttr),
        ShoppingListLayoutViewBehavior {

    @JvmField
    @BindView(R.id.empty_container)
    internal var notFoundContainer: View? = null

    //region BaseLayout

    override fun layoutResID(): Int {
        return R.layout.widget_shopping_list
    }

    override fun providePresenter(): ShoppingListLayoutPresenter {
        return ShoppingListLayoutPresenter(context)
    }

    override fun isSwipeRefreshLayoutEnabled(): Boolean {
        return true
    }

    override fun onSwipeRefresh() {
        presenter().updateShoppingList()
    }

    override fun initialize() {
        super.initialize()

        isRefreshing = true
    }

    override fun isSubscriber(): Boolean {
        return true
    }

    //endregion

    //region ShoppingListLayoutViewBehavior

    override fun showEmptyContainer(o: Boolean) {
        notFoundContainer?.visibility = if (o) View.VISIBLE else View.GONE
    }

    override fun hideOverlay() {
        recyclerView?.alpha = 1f
    }

    override fun showOverlay() {
        recyclerView?.alpha = 0.5f
    }

    override fun showToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    //endregion

    //region Public methods

    @Subscribe
    fun shopCompleted(shop: ShopEvent) {
        onSwipeRefresh()
    }

    @Subscribe
    fun stockOutCompleted(shop: StockOutEvent) {
        onSwipeRefresh()
    }

    @Subscribe
    fun shoppingHasStarted(e: ShoppingProcessEvent) {
        when (e.data) {
            Step.LOADING -> {
                isRefreshing = true
                showOverlay()

            }

            Step.COMPLETED -> {
                isRefreshing = false
                hideOverlay()
            }

            Step.PARTIALLY_COMPLETED -> {
                hideOverlay()
            }
        }
    }

    @Subscribe
    fun mismatchCompleted(event: MismatchEvent) {
        onSwipeRefresh()
    }

    @Subscribe
    fun mismatchFailed(event: MismatchEventFailed) {
        onSwipeRefresh()
    }

    @Subscribe
    fun priceChangedCompleted(event: PriceChangedEvent) {
        onSwipeRefresh()
    }

    fun setData(batch: Batch) {
        presenter().setData(batch)
    }

    //endregion
}