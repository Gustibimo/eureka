package com.happyfresh.fulfillment.modules.Shopper.ShoppingList.interactors

import android.os.Bundle
import android.os.Parcelable
import com.happyfresh.fulfillment.abstracts.BaseInteractor
import com.happyfresh.fulfillment.modules.Shopper.ShoppingList.ShoppingListRouter
import com.happyfresh.fulfillment.modules.Shopper.ShoppingList.outputs.ShoppingListInteractorOutput
import com.happyfresh.snowflakes.hoverfly.models.Batch
import com.happyfresh.snowflakes.hoverfly.models.StockLocation
import com.happyfresh.snowflakes.hoverfly.services.BatchService
import com.shopper.common.Constant
import org.parceler.Parcels

/**
 * Created by galuh on 9/27/17.
 */

abstract class ShoppingListInteractor<out T: ShoppingListInteractorOutput> : BaseInteractor<T>() {

    val batchService by lazy { BatchService() }

    var batch: Batch? = null
        set(value) {
            field = value

            if (value?.exists() == true && value.isActive) {
                field?.load()
            }
            else {
                field?.save()
            }

            batchService.activeBatch = field
            output().onBatch(field!!)
        }

    val batchId: Long
        get() = this.batch?.remoteId ?: 0

    var stockLocationId: Long? = null

    val stockLocation: StockLocation?
        get() {
            return StockLocation.findById(stockLocationId)
        }

    fun initialize(extras: Bundle?) {
        val parcelable: Parcelable? = extras?.getParcelable(ShoppingListRouter.SELECTED_BATCH)
        batch = Parcels.unwrap<Batch>(parcelable)
        stockLocationId = extras?.getLong(Constant.STOCK_LOCATION_ID)
    }
}