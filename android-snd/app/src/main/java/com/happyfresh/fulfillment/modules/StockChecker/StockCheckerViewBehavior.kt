package com.happyfresh.fulfillment.modules.StockChecker

import com.happyfresh.fulfillment.abstracts.ViewBehavior

/**
 * Created by Novanto on 12/28/17.
 */

interface StockCheckerViewBehavior : ViewBehavior {
    fun loadUrl(url: String, header: HashMap<String, String>)
}