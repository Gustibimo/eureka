package com.happyfresh.fulfillment.modules.ChooseStore

import com.happyfresh.fulfillment.abstracts.ViewBehavior
import com.happyfresh.snowflakes.hoverfly.models.StockLocation

/**
 * Created by galuh on 6/12/17.
 */

interface ChooseStoreViewBehavior : ViewBehavior {

    fun showProgress()

    fun hideProgress()

    fun showErrorGettingLocation()

    fun showStores(stores: List<StockLocation>)

    fun updateStores(stores: List<StockLocation>)

    fun showEmptyStores()

    fun showNoStoreSelected()

    fun showClockInDialog(stockLocation: StockLocation)

    fun showSelectStoreButtonEnabled()

    fun showSelectStoreButtonDisabled()

    fun showStatusIsWorking()
}
