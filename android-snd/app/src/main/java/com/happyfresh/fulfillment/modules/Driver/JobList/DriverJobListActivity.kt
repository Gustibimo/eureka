package com.happyfresh.fulfillment.modules.Driver.JobList

import android.os.Bundle

import com.happyfresh.fulfillment.abstracts.BaseActivity
import com.happyfresh.fulfillment.abstracts.BasePresenter

/**
 * Created by galuh on 6/10/17.
 */

class DriverJobListActivity : BaseActivity<DriverJobListFragment, BasePresenter<*, *, *>>() {

    override fun title(): String? {
        return "Jobs"
    }

    override fun provideFragment(bundle: Bundle?): DriverJobListFragment? {
        return DriverJobListFragment()
    }
}
