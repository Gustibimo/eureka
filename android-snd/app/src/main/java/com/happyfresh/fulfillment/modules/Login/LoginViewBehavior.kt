package com.happyfresh.fulfillment.modules.Login

import com.happyfresh.fulfillment.abstracts.ViewBehavior

/**
 * Created by galuh on 6/9/17.
 */

interface LoginViewBehavior : ViewBehavior {

    fun showProgress()

    fun hideProgress()

    fun showLoginNotMatch()

    fun showLoginFailed()

    fun showLoginSuccessful()
}
