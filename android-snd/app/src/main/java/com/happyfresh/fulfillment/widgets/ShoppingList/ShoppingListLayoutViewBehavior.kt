package com.happyfresh.fulfillment.widgets.ShoppingList

import com.happyfresh.fulfillment.abstracts.RecyclerViewBehavior

/**
 * Created by galuh on 10/2/17.
 */

interface ShoppingListLayoutViewBehavior : RecyclerViewBehavior<ShoppingListLayoutAdapter> {
    fun showEmptyContainer(o: Boolean)

    fun hideOverlay()

    fun showOverlay()

    fun showToast(message: String)
}