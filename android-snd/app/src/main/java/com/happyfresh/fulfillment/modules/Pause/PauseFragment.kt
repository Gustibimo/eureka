package com.happyfresh.fulfillment.modules.Pause

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import butterknife.BindView
import com.afollestad.materialdialogs.MaterialDialog
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.abstracts.BaseFragment

/**
 * Created by aldi on 7/26/17.
 */

class PauseFragment : BaseFragment<PausePresenter>(), PauseViewBehavior {

    @JvmField
    @BindView(R.id.date_pause)
    internal var date: TextView? = null

    @JvmField
    @BindView(R.id.timer_pause)
    internal var timer: TextView? = null

    @JvmField
    @BindView(R.id.resume_button)
    internal var resumeButton: Button? = null

    override fun layoutResID(): Int {
        return R.layout.fragment_pause
    }

    override fun providePresenter(): PausePresenter {
        return PausePresenter(context)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter().pauseInitialize()

        resumeButton!!.setOnClickListener { _ -> askToResume() }
    }

    fun askToResume() {
        val resumeDialog = MaterialDialog.Builder(activity).title(R.string.alert_title_warning).content(
                getString(R.string.ask_to_resume)).negativeText(R.string.alert_button_no).positiveText(
                R.string.alert_button_yes).onPositive { _, _ -> presenter().resume() }

        resumeDialog.show()
    }

    override fun showDate(date: String) {
        this.date?.text = date
    }

    override fun showTime(timer: String) {
        this.timer?.text = timer
    }
}