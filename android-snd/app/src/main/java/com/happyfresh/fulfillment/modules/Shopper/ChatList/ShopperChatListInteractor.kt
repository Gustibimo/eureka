package com.happyfresh.fulfillment.modules.Shopper.ChatList

import com.happyfresh.fulfillment.abstracts.BaseInteractor
import com.happyfresh.snowflakes.hoverfly.Sprinkles
import com.happyfresh.snowflakes.hoverfly.models.Batch
import com.happyfresh.snowflakes.hoverfly.models.Shipment
import com.happyfresh.snowflakes.hoverfly.shared.events.UnreadChatFetchedEvent
import com.happyfresh.snowflakes.hoverfly.stores.UserStore
import com.happyfresh.snowflakes.hoverfly.utils.ChatUtils
import com.squareup.otto.Subscribe

/**
 * Created by kharda on 15/8/18
 */
class ShopperChatListInteractor : BaseInteractor<ShopperChatListInteractorOutput>() {

    private val CONNECTION_HANDLER_ID = "CONNECTION_HANDLER_SHOPPER_CHAT_LIST_INTERACTOR"

    private val CHANNEL_HANDLER_ID = "CHANNEL_HANDLER_SHOPPER_CHAT_LIST_INTERACTOR"

    private val userStore = Sprinkles.stores(UserStore::class.java)

    var batch: Batch? = null

    fun initialize(batchId: Long?) {
        this.batch = Batch.findById(batchId)
        prepareShowChatList()
    }

    override fun shouldListenEventBus(): Boolean {
        return true
    }

    fun addChatHandler() {
        batch?.remoteId.let {
            val shopperId = userStore?.userId.toString()

            ChatUtils.addChatHandler(it, CONNECTION_HANDLER_ID, CHANNEL_HANDLER_ID, shopperId)
        }
    }

    fun removeChatHandler() {
        ChatUtils.removeChatHandler(CONNECTION_HANDLER_ID, CHANNEL_HANDLER_ID)
    }

    fun prepareOpenChatScreen(shipment: Shipment) {
        val shopperId = userStore?.userId.toString()
        output().openChatScreen(shopperId, shipment)
    }

    private fun prepareShowChatList() {
        batch?.let {
            output().showChatList(it.shipments())
        }
    }

    @Subscribe
    fun onUnreadChatFetched(event: UnreadChatFetchedEvent) {
        if (batch?.remoteId == event.data) {
            val batch = Batch.findById(event.data)
            batch?.let {
                this.batch = batch
                prepareShowChatList()
            }
        }
    }
}