package com.happyfresh.fulfillment.widgets.Store

import android.content.Context
import android.view.View
import android.widget.CheckBox
import android.widget.TextView
import butterknife.BindView
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.fulfillment.widgets.BaseRecyclerViewHolder
import com.happyfresh.snowflakes.hoverfly.models.StockLocation

/**
 * Created by aldi on 6/16/17.
 */

class StoreViewHolder(context: Context, itemView: View) :
        BaseRecyclerViewHolder<BasePresenter<*, *, *>, StockLocation>(context, itemView) {

    internal var adapter: StoreAdapter? = null

    @JvmField
    @BindView(R.id.checkbox)
    internal var checkBox: CheckBox? = null

    @JvmField
    @BindView(R.id.store_container)
    internal var storeContainer: View? = null

    @JvmField
    @BindView(R.id.location_container)
    internal var locationContainer: View? = null

    @JvmField
    @BindView(R.id.location)
    internal var location: TextView? = null

    @JvmField
    @BindView(R.id.cluster)
    internal var cluster: TextView? = null

    override fun bind(data: StockLocation) {
        location!!.text = data.name

        if (adapter?.selectedStockLocation != null) {
            checkBox!!.isChecked = adapter?.selectedStockLocation?.remoteId == data.remoteId
        }
        else {
            checkBox?.isChecked = false
        }

        if (data.cluster != null) {
            cluster?.visibility = View.VISIBLE
            cluster?.text = data.cluster!!.name
        }
        else {
            cluster?.visibility = View.GONE
            cluster?.text = null
        }

        storeContainer!!.setOnClickListener {
            if (checkBox!!.isChecked) {
                adapter?.selectedStockLocation = null
                checkBox!!.isChecked = false
            }
            else {
                adapter?.lastCheckBox?.isChecked = false
                checkBox!!.isChecked = true
                adapter?.selectedStockLocation = data
            }

            adapter?.lastCheckBox = checkBox!!
        }
    }
}
