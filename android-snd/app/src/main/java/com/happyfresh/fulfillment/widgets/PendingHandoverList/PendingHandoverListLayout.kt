package com.happyfresh.fulfillment.widgets.PendingHandoverList

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.TextView
import android.widget.Toast
import butterknife.BindView
import com.afollestad.materialdialogs.DialogAction
import com.afollestad.materialdialogs.MaterialDialog
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.widgets.BaseRecyclerViewLayout
import com.happyfresh.snowflakes.hoverfly.models.GrabExpressCancelReason
import com.happyfresh.snowflakes.hoverfly.models.PendingHandover

/**
 * Created by kharda on 28/06/18.
 */

class PendingHandoverListLayout @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
        BaseRecyclerViewLayout<PendingHandoverListAdapter, PendingHandoverListLayoutPresenter>(context, attrs, defStyleAttr),
        PendingHandoverListLayoutViewBehavior {

    @JvmField
    @BindView(R.id.not_found_container)
    var notFoundContainer: View? = null

    @JvmField
    @BindView(R.id.not_found)
    var notFound: TextView? = null

    override fun layoutResID(): Int {
        return R.layout.widget_pending_handover_list
    }

    override fun providePresenter(): PendingHandoverListLayoutPresenter {
        return PendingHandoverListLayoutPresenter(context)
    }

    override fun setAdapter(adapter: PendingHandoverListAdapter) {
        super.setAdapter(adapter)
        presenter().prepareSetBookingActionListener()
    }

    override fun isSwipeRefreshLayoutEnabled(): Boolean {
        return true
    }

    override fun updateRefreshState(o: Boolean) {
        super.updateRefreshState(o)

        notFoundContainer?.visibility = View.GONE
    }

    override fun onSwipeRefresh() {
        fetchData()
    }

    override fun unableLoadData() {
        Toast.makeText(context, context.getString(R.string.toast_unable_load_data_from_server), Toast.LENGTH_LONG).show()
    }

    override fun showEmpty() {
        notFoundContainer?.visibility = View.VISIBLE
        notFound?.text = context.getString(R.string.booking_not_found)
    }

    override fun showCancelBookingConfirmationDialog(orderNumber: String, grabExpressCancelReasons: List<GrabExpressCancelReason>) {
        var selectedPosition = 0
        val dialog = MaterialDialog.Builder(context)
                .title(R.string.cancellation_reason).items(grabExpressCancelReasons)
                .positiveText(R.string.alert_button_ok)
                .negativeText(R.string.alert_button_cancel)
                .alwaysCallSingleChoiceCallback()
                .itemsCallbackSingleChoice(-1) { dialog, _, position, _ ->
                    if (position > -1) {
                        dialog.getActionButton(DialogAction.POSITIVE).isEnabled = true
                        selectedPosition = position
                        dialog.selectedIndex = -1
                    }
                    true
                }
                .onPositive { _, _ ->
                    presenter().cancelBooking(orderNumber, grabExpressCancelReasons[selectedPosition].key)
                }
                .build()

        dialog.getActionButton(DialogAction.POSITIVE).isEnabled = false
        dialog.show()
    }

    override fun showCreateBookingConfirmationDialog(orderNumber: String) {
        MaterialDialog.Builder(context)
                .title(R.string.alert_title_warning)
                .content(context.getString(R.string.ask_to_book))
                .cancelable(false)
                .negativeText(R.string.alert_button_no)
                .positiveText(R.string.alert_button_yes)
                .onPositive { _, _ -> presenter().createBooking(orderNumber) }
                .show()
    }

    override fun showChangeBookingToHFConfirmationDialog(orderNumber: String) {
        MaterialDialog.Builder(context)
                .title(R.string.alert_title_warning)
                .content(context.getString(R.string.ask_to_change_booking_to_hf))
                .cancelable(false)
                .negativeText(R.string.alert_button_no)
                .positiveText(R.string.alert_button_yes)
                .onPositive { _, _ -> presenter().changeBookingToHF(orderNumber) }
                .show()
    }

    fun fetchData() {
        presenter().fetchData()
    }

    fun showData(pendingHandoverList: List<PendingHandover>) {
        presenter().prepareShowPendingHandover(pendingHandoverList)
    }
}