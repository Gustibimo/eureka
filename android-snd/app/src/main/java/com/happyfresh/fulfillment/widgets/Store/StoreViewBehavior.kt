package com.happyfresh.fulfillment.widgets.Store

import com.happyfresh.fulfillment.abstracts.ViewBehavior

/**
 * Created by aldi on 6/16/17.
 */

interface StoreViewBehavior : ViewBehavior {

    fun initiateView(adapter: StoreAdapter)
}
