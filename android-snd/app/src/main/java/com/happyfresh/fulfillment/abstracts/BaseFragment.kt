package com.happyfresh.fulfillment.abstracts

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import butterknife.ButterKnife
import butterknife.Unbinder
import com.afollestad.materialdialogs.MaterialDialog
import com.happyfresh.fulfillment.R
import com.happyfresh.snowflakes.hoverfly.shared.events.EventBus

/**
 * Created by ifranseda on 10/02/2017.
 */
abstract class BaseFragment<out T : BasePresenter<*, *, *>> : Fragment(), ViewBehavior {

    private var readExtrasSuper: Boolean = false

    private var presenter: T? = null

    internal var bundle: Bundle? = null

    internal var unbinder: Unbinder? = null

    //region Abstractions

    abstract fun layoutResID(): Int

    open protected fun menuResID(): Int {
        return -1
    }

    protected abstract fun providePresenter(): T

    protected fun presenter(): T {
        if (this.presenter == null) {
            this.presenter = providePresenter()
            this.presenter?.setView(this)
        }

        return this.presenter as T
    }

    open protected fun isSubscriber(): Boolean {
        return false
    }

    //endregion

    //region Lifecycle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        this.bundle = savedInstanceState
        if (this.bundle == null) {
            this.bundle = arguments
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(layoutResID(), container, false)
        this.unbinder = ButterKnife.bind(this, view)

        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(menuResID() > -1)
    }

    override fun onResume() {
        super.onResume()

        subscribe()
    }

    override fun onDestroyView() {
        unsubscribe()
        presenter().detach()

        this.unbinder?.unbind()

        super.onDestroyView()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        this.bundle?.let {
            outState?.putAll(it)
        }

        super.onSaveInstanceState(outState)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item!!.itemId) {
            R.id.action_logout -> {
                logout()
                true
            }
            else               -> super.onOptionsItemSelected(item)
        }
    }

    //endregion

    //region methods

    private fun subscribe() {
        if (isSubscriber()) {
            try {
                EventBus.handler.register(this)
            }
            catch (e: Exception) {
            }
        }
    }

    private fun unsubscribe() {
        if (isSubscriber()) {
            EventBus.handler.unregister(this)
        }
    }

    protected fun logout() {
        val logoutDialog = MaterialDialog.Builder(activity).title(R.string.alert_title_warning).content(R.string.alert_message_logout).negativeText(
                R.string.alert_button_no).positiveText(R.string.alert_button_yes).onPositive { _, _ -> presenter().logout() }

        logoutDialog.show()
    }

    //endregion
}
