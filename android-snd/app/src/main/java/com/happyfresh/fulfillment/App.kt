package com.happyfresh.fulfillment

import android.app.Application
import android.content.Context
import android.support.multidex.MultiDex
import com.crashlytics.android.Crashlytics
import com.happyfresh.snowflakes.hoverfly.Sprinkles
import com.shopper.common.ShopperApplication
import io.fabric.sdk.android.Fabric

/**
 * Created by ifranseda on 30/01/2017.
 */

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        App.context = this

        Sprinkles.initialize(this)

        ShopperApplication.onCreate(this)

        Fabric.with(this, Crashlytics())
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    companion object {
        var context: Context? = null

        fun appContext(): Context {
            return context!!
        }
    }
}
