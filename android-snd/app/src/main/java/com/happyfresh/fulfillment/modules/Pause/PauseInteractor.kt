package com.happyfresh.fulfillment.modules.Pause

import android.os.Handler
import com.happyfresh.fulfillment.abstracts.BaseInteractor
import com.happyfresh.snowflakes.hoverfly.Sprinkles
import com.happyfresh.snowflakes.hoverfly.services.AppService
import com.happyfresh.snowflakes.hoverfly.shared.subscribers.ServiceSubscriber
import com.happyfresh.snowflakes.hoverfly.stores.UserStore
import retrofit.client.Response
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by aldi on 7/26/17.
 */

class PauseInteractor : BaseInteractor<PauseInteractorOutput>() {

    private val timerHandler = Handler()

    private var startTime: Long = 0L

    private val userStore = Sprinkles.stores(UserStore::class.java)

    private val appService = Sprinkles.service(AppService::class.java)

    private var timerRunnable: Runnable = object : Runnable {
        override fun run() {
            val millis = System.currentTimeMillis() - startTime
            var seconds = (millis / 1000).toInt()
            var minutes = seconds / 60
            val hours = minutes / 60

            seconds %= 60
            minutes %= 60

            showTime(String.format("%02d : %02d : %02d", hours, minutes, seconds))
            timerHandler.postDelayed(this, 500)
        }
    }

    fun pauseInitialize() {
        getCurrentDate()
        startTimer()
    }

    fun resume() {
        val observable = appService.resume()
        val subscriber = object : ServiceSubscriber<Response>() {
            override fun onFailure(e: Throwable) {
                e.printStackTrace()
            }

            override fun onSuccess(data: Response) {
                userStore!!.onPaused = false

                stopTimer()
            }
        }

        subscriptions().add(observable.subscribe(subscriber)!!)
    }

    private fun getCurrentDate() {
        val c = Calendar.getInstance()
        val df = SimpleDateFormat("dd MMMM yyyy")
        val date = df.format(c.time)
        showDate(date)
    }

    private fun startTimer() {
        startTime = userStore!!.pauseTimer
        if (startTime == 0L) {
            startTime = System.currentTimeMillis()
            userStore.pauseTimer = startTime
        }
        timerHandler.postDelayed(timerRunnable, 0L)
    }

    private fun stopTimer() {
        userStore!!.pauseTimer = 0L
        pauseTimer()
    }

    private fun pauseTimer() {
        timerHandler.let { timerHandler.removeCallbacks(timerRunnable) }
        redirectPage()
    }

    private fun redirectPage() {
        output().backToJobList()
    }

    fun showDate(date: String) {
        output().showDate(date)
    }

    fun showTime(timer: String) {
        output().showTime(timer)
    }
}