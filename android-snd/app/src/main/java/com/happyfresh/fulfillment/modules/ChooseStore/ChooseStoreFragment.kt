package com.happyfresh.fulfillment.modules.ChooseStore

import android.os.Bundle
import android.view.*
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import butterknife.BindView
import com.afollestad.materialdialogs.MaterialDialog
import com.happyfresh.fulfillment.App
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.abstracts.BaseFragment
import com.happyfresh.fulfillment.widgets.Store.StoreListLayout
import com.happyfresh.snowflakes.hoverfly.models.StockLocation
import com.happyfresh.snowflakes.hoverfly.utils.ResourceUtils
import org.jetbrains.anko.find
import java.util.*

/**
 * Created by galuh on 6/12/17.
 */

class ChooseStoreFragment : BaseFragment<ChooseStorePresenter>(), ChooseStoreViewBehavior {

    private var storeListLayout: StoreListLayout? = null

    @JvmField
    @BindView(R.id.store_not_found)
    internal var storeNotFound: TextView? = null

    @JvmField
    @BindView(R.id.choose_store_progress)
    internal var progressBar: ProgressBar? = null

    @JvmField
    @BindView(R.id.select_store_button)
    internal var selectStoreButton: Button? = null

    override fun layoutResID(): Int {
        return R.layout.fragment_choose_store_new
    }

    override fun providePresenter(): ChooseStorePresenter {
        return ChooseStorePresenter(context)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        presenter().prepareGoogleApiClient()
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = super.onCreateView(inflater, container, savedInstanceState)

        storeListLayout = view?.find(R.id.StoreListLayout)

        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        showProgress()
        selectStoreButton?.setOnClickListener { _ -> selectStoreButtonTapped() }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)

        menu?.let {
            inflater?.inflate(R.menu.store_new, it)

            it.findItem(R.id.action_refresh_job).isVisible = true
            it.findItem(R.id.action_settings).isVisible = true
            it.findItem(R.id.action_logout).isVisible = true
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.action_refresh_job -> {
                refreshStores()
                true
            }

            R.id.action_settings    -> true
            else                    -> super.onOptionsItemSelected(item)
        }
    }

    private fun selectStoreButtonTapped() {
        val stockLocation = storeListLayout?.selectStore()
        presenter().selectStore(stockLocation)
    }

    fun connectGoogleApiClient() {
        presenter().connectGoogleApiClient()
    }

    fun disconnectGoogleApiClient() {
        presenter().disconnectGoogleApiClient()
    }

    fun checkAndRequestLocation() {
        presenter().checkAndRequestLocation()
    }

    fun checkClockInStatus() {
        presenter().checkClockInStatus()
    }

    private fun refreshStores() {
        storeListLayout?.resetData()
        showProgress()
        presenter().refreshStores()
    }

    override fun showProgress() {
        storeNotFound?.visibility = View.GONE
        progressBar?.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progressBar?.visibility = View.GONE
    }

    override fun showErrorGettingLocation() {
        progressBar?.visibility = View.INVISIBLE
        selectStoreButton?.visibility = View.VISIBLE

        Toast.makeText(App.context, R.string.cannot_retrieve_location, Toast.LENGTH_LONG).show()
    }

    override fun showStores(stores: List<StockLocation>) {
        storeNotFound?.visibility = View.GONE
        storeListLayout?.setData(stores as MutableList<StockLocation>)
    }

    override fun updateStores(stores: List<StockLocation>) {
        storeNotFound?.visibility = View.GONE
        storeListLayout?.addAllData(stores)
    }

    override fun showEmptyStores() {
        storeNotFound?.visibility = View.VISIBLE
        storeListLayout?.setData(ArrayList())
    }

    override fun showNoStoreSelected() {
        Toast.makeText(App.appContext(), R.string.toast_choose_store_location, Toast.LENGTH_LONG).show()
    }

    override fun showClockInDialog(stockLocation: StockLocation) {
        if (activity != null) {
            val clockInDialog = MaterialDialog.Builder(activity).title(R.string.clock_in).titleColor(
                    ResourceUtils.getAttributedColor(activity, R.attr.colorPrimary)).content(
                    getString(R.string.ask_to_clock_in) + " " + stockLocation.name + "?").negativeText(R.string.alert_button_no).positiveText(
                    R.string.alert_button_yes).onPositive { _, _ ->
                presenter().createPlacement(stockLocation)
            }

            clockInDialog.show()
        }
    }

    override fun showSelectStoreButtonEnabled() {
        selectStoreButton?.isEnabled = true
        selectStoreButton?.setText(R.string.select)
    }

    override fun showSelectStoreButtonDisabled() {
        selectStoreButton?.isEnabled = false
        selectStoreButton?.setText(R.string.please_wait)
    }

    override fun showStatusIsWorking() {
        Toast.makeText(App.appContext(), "Your status is working", Toast.LENGTH_LONG).show()
    }
}
