package com.happyfresh.fulfillment.widgets.Store

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import butterknife.BindView
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.widgets.BaseLayout
import com.happyfresh.fulfillment.widgets.ViewHolder
import com.happyfresh.snowflakes.hoverfly.models.StockLocation

/**
 * Created by aldi on 6/16/17.
 */

class StoreListLayout @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
        BaseLayout<StorePresenter>(context, attrs, defStyleAttr), StoreViewBehavior {

    @JvmField
    @BindView(R.id.recyclerview_store)
    internal var recyclerView: RecyclerView? = null

    override fun layoutResID(): Int {
        return R.layout.widget_store
    }

    override fun providePresenter(): StorePresenter {
        return StorePresenter(context)
    }

    fun setData(stockLocation: MutableList<StockLocation>) {
        presenter().setData(stockLocation)
    }

    fun resetData() {
        presenter().resetData()
    }

    fun selectStore(): StockLocation? {
        return presenter().selectedStore
    }

    fun addAllData(stores: List<StockLocation>) {
        presenter().addAllData(stores)
    }

    override fun initiateView(adapter: StoreAdapter) {
        val linearLayoutManager = LinearLayoutManager(context)
        recyclerView?.layoutManager = linearLayoutManager
        recyclerView?.adapter = adapter
    }

    val stockLocations: List<StockLocation>?
        get() = presenter().stockLocations
}
