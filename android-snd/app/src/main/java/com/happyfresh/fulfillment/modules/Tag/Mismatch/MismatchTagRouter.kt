package com.happyfresh.fulfillment.modules.Tag.Mismatch

import android.content.Context
import android.os.Bundle
import com.happyfresh.fulfillment.abstracts.BaseActivity
import com.happyfresh.fulfillment.abstracts.BaseFragment
import com.happyfresh.fulfillment.modules.Tag.TagRouter

/**
 * Created by ifranseda on 29/12/2017.
 */
class MismatchTagRouter(context: Context) : TagRouter(context) {
    override fun viewClass(): Class<out BaseActivity<BaseFragment<*>, *>>? {
        return MismatchTagActivity::class.java
    }

    fun startWithOrderNumber(orderNumber: String?) {
        val bundle = Bundle().apply {
            putString(ORDER_NUMBER, orderNumber)
        }

        startForResult(bundle, ProductMismatchRequestCode)
    }
}