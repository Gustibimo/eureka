package com.happyfresh.fulfillment.modules.Shopper.ChatList

import android.content.Context
import android.view.View
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.modules.Shopper.ChatList.views.ShopperChatListViewHolder
import com.happyfresh.fulfillment.widgets.BaseAdapter
import com.happyfresh.snowflakes.hoverfly.models.Shipment

/**
 * Created by kharda on 15/8/18
 */
class ShopperChatListAdapter(context: Context, data: List<Shipment>) : BaseAdapter<Shipment, ShopperChatListViewHolder>(context, data) {

    override fun itemLayoutResID(type: Int): Int {
        return R.layout.shopping_chat_list_item
    }

    override fun provideViewHolder(view: View, type: Int): ShopperChatListViewHolder {
        return ShopperChatListViewHolder(context, view)
    }
}