package com.happyfresh.fulfillment.widgets.LineItem

import com.happyfresh.fulfillment.abstracts.InteractorOutput
import com.happyfresh.snowflakes.hoverfly.models.Shipment

/**
 * Created by kharda on 20/8/18
 */
interface LineItemDetailLayoutInteractorOutput : InteractorOutput {

    fun toggleChatButton(show: Boolean)

    fun openChatScreen(shopperId: String, shipment: Shipment)
}