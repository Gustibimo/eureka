package com.happyfresh.fulfillment.widgets.ShoppingList

import android.content.Context
import com.happyfresh.fulfillment.abstracts.BaseRouter
import com.happyfresh.fulfillment.modules.LineItem.LineItemDetailRouter
import com.happyfresh.snowflakes.hoverfly.models.LineItem

/**
 * Created by ifranseda on 10/10/2017.
 */

class ShoppingListLayoutRouter(context: Context) : BaseRouter(context) {

    private val lineItemRouter by lazy { LineItemDetailRouter(context) }

    override fun detach() {
    }

    fun openItemDetail(item: LineItem) {
        if(!item?.isShoppingBag!!) {
            lineItemRouter.startWithLineItem(item)
        }
    }
}