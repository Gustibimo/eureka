package com.happyfresh.fulfillment.modules.Shopper.ShoppingList

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import butterknife.BindView
import butterknife.OnClick
import com.afollestad.materialdialogs.MaterialDialog
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.abstracts.BaseFragment
import com.happyfresh.fulfillment.modules.Shopper.ShoppingList.behaviors.ShoppingListFragmentViewBehavior
import com.happyfresh.fulfillment.modules.Shopper.ShoppingList.presenters.ShoppingListFragmentPresenter
import com.happyfresh.fulfillment.widgets.ShoppingList.ShoppingListLayout
import com.happyfresh.snowflakes.hoverfly.models.Batch
import com.happyfresh.snowflakes.hoverfly.shared.events.ShopEvent
import com.happyfresh.snowflakes.hoverfly.shared.events.StockOutEvent
import com.squareup.otto.Subscribe

/**
 * Created by galuh on 9/27/17.
 */

class ShoppingListFragment : BaseFragment<ShoppingListFragmentPresenter>(),
        ShoppingListFragmentViewBehavior {

    @JvmField
    @BindView(R.id.shopping_list)
    var shoppingList: ShoppingListLayout? = null

    @JvmField
    @BindView(R.id.next_button)
    var nextButton: Button? = null

    @JvmField
    @BindView(R.id.progress_container)
    var progressContainer: View? = null

    override fun layoutResID(): Int {
        return R.layout.fragment_shopping_list
    }

    override fun providePresenter(): ShoppingListFragmentPresenter {
        return ShoppingListFragmentPresenter(context)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter().initialize(this.bundle)
    }

    override fun isSubscriber(): Boolean {
        return true
    }

    override fun initializeShoppingList(batch: Batch) {
        shoppingList?.setData(batch)
        presenter().prepareToggleStartShoppingButton()
    }

    override fun shoppingNotComplete() {
        Toast.makeText(context, R.string.shopping_is_not_finished, Toast.LENGTH_SHORT).show()
    }

    override fun showAlertIncompleteLineItem(pendingCounter: Int) {
        val inputDialog = MaterialDialog.Builder(context)
        inputDialog.title(R.string.alert_title_pending_item)
        inputDialog.content(getString(R.string.alert_message_pending_item, pendingCounter))
        inputDialog.positiveText(R.string.alert_button_ok)

        inputDialog.show()
    }

    override fun showProgress() {
        progressContainer?.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progressContainer?.visibility = View.GONE
    }

    override fun showAlertUnableCheckProductSample() {
        Toast.makeText(context,
                getString(R.string.toast_unable_check_product_sample_list), Toast.LENGTH_LONG)
                .show()
    }

    override fun toggleStartShoppingButton(isEnable: Boolean) {
        nextButton?.isEnabled = isEnable
    }

    @OnClick(R.id.next_button)
    fun nextButtonClicked() {
        presenter().finalizeShoppingList()
    }

    @Subscribe
    fun shopCompleted(shop: ShopEvent) {
        presenter().prepareToggleStartShoppingButton()
    }

    @Subscribe
    fun stockOutCompleted(shop: StockOutEvent) {
        presenter().prepareToggleStartShoppingButton()
    }
}