package com.happyfresh.fulfillment.modules.Shopper.PendingHandoverList

import android.os.Bundle
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.abstracts.BaseActivity
import com.happyfresh.fulfillment.abstracts.BasePresenter

/**
 * Created by kharda on 28/06/18.
 */

class ShopperPendingHandoverListActivity : BaseActivity<ShopperPendingHandoverListFragment, BasePresenter<*, *, *>>() {

    public override fun title(): String? {
        return getString(R.string.title_grab_express_booking)
    }

    public override fun provideFragment(bundle: Bundle?): ShopperPendingHandoverListFragment? {
        val fragment = ShopperPendingHandoverListFragment()
        fragment.arguments = bundle
        return fragment
    }
}
