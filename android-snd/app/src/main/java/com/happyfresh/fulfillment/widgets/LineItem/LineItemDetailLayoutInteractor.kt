package com.happyfresh.fulfillment.widgets.LineItem

import com.happyfresh.fulfillment.abstracts.BaseInteractor
import com.happyfresh.snowflakes.hoverfly.Sprinkles
import com.happyfresh.snowflakes.hoverfly.models.Order
import com.happyfresh.snowflakes.hoverfly.models.Shipment
import com.happyfresh.snowflakes.hoverfly.stores.UserStore

/**
 * Created by kharda on 20/8/18
 */
class LineItemDetailLayoutInteractor : BaseInteractor<LineItemDetailLayoutInteractorOutput>() {

    private val userStore = Sprinkles.stores(UserStore::class.java)

    fun prepareToggleChatButton(orderId: Long?) {
        var chatEnable = false
        val order = Order.findById(orderId)
        order?.let {
            chatEnable = it.isChatEnabled
        }

        output().toggleChatButton(chatEnable)
    }

    fun prepareOpenChatScreen(shipment: Shipment?) {
        val shopperId = userStore?.userId.toString()
        shipment?.let {
            output().openChatScreen(shopperId, shipment)
        }
    }
}