package com.happyfresh.fulfillment.modules.LineItem.outputs

import com.happyfresh.fulfillment.abstracts.InteractorOutput
import com.happyfresh.snowflakes.hoverfly.models.FeedbackType
import com.happyfresh.snowflakes.hoverfly.models.LineItem

interface LineItemDetailInteractorOutput : InteractorOutput {

    fun onLineItem(lineItem: LineItem)

    fun onStockOutFeedbackList(feedbackType: List<FeedbackType>)
}