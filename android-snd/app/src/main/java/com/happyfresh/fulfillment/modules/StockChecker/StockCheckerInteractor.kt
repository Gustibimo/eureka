package com.happyfresh.fulfillment.modules.StockChecker

import com.happyfresh.fulfillment.abstracts.BaseInteractor
import com.happyfresh.snowflakes.hoverfly.Sprinkles
import com.happyfresh.snowflakes.hoverfly.config.Config
import com.happyfresh.snowflakes.hoverfly.stores.UserStore

/**
 * Created by Novanto on 12/28/17.
 */

class StockCheckerInteractor : BaseInteractor<StockCheckerInteractorOutput>() {

    val url = Config.StockCheckerUrl

    fun loadUrlData() {
        val userToken = Sprinkles.stores(UserStore::class.java)?.token
        userToken?.let { token ->
            output().prepareLoadUrl(url, token)
        }
    }
}