package com.happyfresh.fulfillment.modules.Pause

import android.content.Context
import com.happyfresh.fulfillment.abstracts.BasePresenter

/**
 * Created by aldi on 7/26/17.
 */

class PausePresenter(context: Context) : BasePresenter<PauseInteractor, PauseRouter, PauseViewBehavior>(context), PauseInteractorOutput {

    override fun provideInteractor(): PauseInteractor? {
        return PauseInteractor()
    }

    override fun provideRouter(context: Context): PauseRouter? {
        return PauseRouter(context)
    }

    fun pauseInitialize() {
        interactor().pauseInitialize()
    }

    fun resume() {
        interactor().resume()
    }

    override fun showDate(date: String) {
        view()?.showDate(date)
    }

    override fun showTime(timer: String) {
        view()?.showTime(timer)
    }

    override fun backToJobList() {
        router().openShoppingJobList()
    }
}