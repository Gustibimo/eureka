package com.happyfresh.fulfillment.modules.StockOut

import android.content.Context
import android.os.Bundle
import com.happyfresh.fulfillment.abstracts.BasePresenter

/**
 * Created by ifranseda on 02/01/2018.
 */
class StockOutPresenter(context: Context) : BasePresenter<StockOutInteractor, StockOutRouter, StockOutViewBehavior>(context), StockOutInteractorOutput {

    override fun provideInteractor(): StockOutInteractor {
        return StockOutInteractor()
    }

    override fun provideRouter(context: Context): StockOutRouter? {
        return StockOutRouter(context)
    }

    override fun showChatBadge(count: Int) {
        view()?.showChatBadge(count)
    }

    override fun hideChatButton() {
        view()?.hideChatButton()
    }

    fun initialize(bundle: Bundle?) {
        bundle?.getLong(StockOutRouter.SELECTED_BATCH_ID)?.let { batchId ->
            view()?.drawFinalizeForBatch(batchId)
            interactor().initialize(batchId)
        }
    }

    fun addChatHandler() {
        interactor().addChatHandler()
    }

    fun removeChatHandler() {
        interactor().removeChatHandler()
    }

    fun openShopperChatList() {
        router().openShopperChatList(interactor().batchId)
    }
}