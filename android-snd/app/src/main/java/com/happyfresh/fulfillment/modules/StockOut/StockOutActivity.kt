package com.happyfresh.fulfillment.modules.StockOut

import android.os.Bundle
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.abstracts.BaseActivity
import com.happyfresh.fulfillment.abstracts.BasePresenter

/**
 * Created by ifranseda on 02/01/2018.
 */
class StockOutActivity : BaseActivity<StockOutFragment, BasePresenter<*, *, *>>() {
    override fun provideFragment(bundle: Bundle?): StockOutFragment? {
        return StockOutFragment().apply { arguments = bundle }
    }

    override fun title(): String? {
        return getString(R.string.out_of_stock)
    }

    override fun showBackButton(): Boolean {
        return true
    }
}