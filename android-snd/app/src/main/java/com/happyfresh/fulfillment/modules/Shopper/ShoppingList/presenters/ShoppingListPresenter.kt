package com.happyfresh.fulfillment.modules.Shopper.ShoppingList.presenters

import android.content.Context
import android.os.Bundle
import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.fulfillment.abstracts.InteractorOutput
import com.happyfresh.fulfillment.abstracts.ViewBehavior
import com.happyfresh.fulfillment.modules.Shopper.ShoppingList.ShoppingListRouter
import com.happyfresh.fulfillment.modules.Shopper.ShoppingList.interactors.ShoppingListInteractor

/**
 * Created by galuh on 9/27/17.
 */

abstract class ShoppingListPresenter<out T : ShoppingListInteractor<*>, out V : ViewBehavior>(context: Context) :
        BasePresenter<T, ShoppingListRouter, V>(context), InteractorOutput {

    init {
        interactor().context = context
    }

    override fun provideRouter(context: Context): ShoppingListRouter? {
        return ShoppingListRouter(context)
    }

    open fun initialize(extras: Bundle?) {
        interactor().initialize(extras)
    }
}