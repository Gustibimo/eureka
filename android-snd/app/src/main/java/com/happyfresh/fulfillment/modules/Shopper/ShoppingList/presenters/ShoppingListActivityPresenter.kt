package com.happyfresh.fulfillment.modules.Shopper.ShoppingList.presenters

import android.content.Context
import com.happyfresh.fulfillment.modules.Shopper.ShoppingList.ShoppingListRouter
import com.happyfresh.fulfillment.modules.Shopper.ShoppingList.behaviors.ShoppingListActivityViewBehavior
import com.happyfresh.fulfillment.modules.Shopper.ShoppingList.interactors.ShoppingListActivityInteractor
import com.happyfresh.fulfillment.modules.Shopper.ShoppingList.outputs.ShoppingListActivityInteractorOutput
import com.happyfresh.snowflakes.hoverfly.models.Batch

/**
 * Created by ifranseda on 09/10/2017.
 */

class ShoppingListActivityPresenter(context: Context) :
        ShoppingListPresenter<ShoppingListActivityInteractor, ShoppingListActivityViewBehavior>(context),
        ShoppingListActivityInteractorOutput {

    override fun provideInteractor(): ShoppingListActivityInteractor {
        return ShoppingListActivityInteractor()
    }

    override fun provideRouter(context: Context): ShoppingListRouter? {
        return ShoppingListRouter(context)
    }

    override fun onBatch(batch: Batch) {
        interactor().prepareContents()
        interactor().shouldShowShopAndChatButton()
        interactor().shouldHideCancelMenu()
    }

    override fun showName(name: String) {
        view()?.showStoreName(name)
    }

    override fun showImage(image: String?) {
        image?.let {
            view()?.showStoreImage(it)
        }
    }

    override fun showAddress(address: String) {
        view()?.showStoreAddress(address)
    }

    override fun toggleStartButton(show: Boolean) {
        if (show) {
            view()?.showShoppingButton()
        }
        else {
            view()?.hideShoppingButton()
        }
    }

    override fun toggleChatButton(show: Boolean) {
        if (show) {
            view()?.showChatButton()
        }
        else {
            view()?.hideChatButton()
        }
    }

    override fun hideCancelMenu() {
        view()?.hideCancelMenu()
    }

    override fun returnToBatchList() {
        router().switchToBatchList()
    }

    override fun failedToStart() {
        view()?.showFailedToStart()
    }

    override fun pleaseTryAgain() {
        view()?.pleaseTryAgain()
    }

    override fun setTimer(time: String) {
        view()?.setTimer(time)
    }

    override fun openShopperChatList(batchId: Long) {
        router().openShopperChatList(batchId)
    }

    override fun showChatBadge(count: Int) {
        view()?.showChatBadge(count)
    }

    //region Public methods

    fun initiateStartShopping() {
        interactor().startBatch()
    }

    fun initiateCancel() {
        interactor().submitCancelBatch()
        interactor().stopTimer()
        view()?.hideTimer()
    }

    fun switchBatch() {
        router().switchToBatchList()
    }

    fun prepareTimer() {
        if (interactor().prepareTimer()) {
            view()?.showTimer()
        }
    }

    fun addChatHandler() {
        interactor().addChatHandler()
    }

    fun removeChatHandler() {
        interactor().removeChatHandler()
    }

    fun prepareOpenShopperChatList() {
        interactor().prepareOpenShopperChatList()
    }

    fun disconnectChat() {
        interactor().disconnectChat()
    }

    //endregion
}