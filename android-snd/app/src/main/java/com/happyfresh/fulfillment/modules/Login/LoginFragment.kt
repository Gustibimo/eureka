package com.happyfresh.fulfillment.modules.Login

import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.LayerDrawable
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.text.method.PasswordTransformationMethod
import android.view.MotionEvent
import android.view.View
import android.view.animation.CycleInterpolator
import android.view.animation.TranslateAnimation
import android.widget.*
import butterknife.BindView
import com.afollestad.materialdialogs.MaterialDialog
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.abstracts.BaseFragment
import com.shopper.common.ShopperApplication

/**
 * Created by galuh on 6/9/17.
 */

class LoginFragment : BaseFragment<LoginPresenter>(), LoginViewBehavior {

    @JvmField
    @BindView(R.id.username_input)
    internal var usernameField: EditText? = null

    @JvmField
    @BindView(R.id.password_input)
    internal var passwordField: EditText? = null

    @JvmField
    @BindView(R.id.login_button)
    internal var loginButton: Button? = null

    @JvmField
    @BindView(R.id.login_progress)
    internal var progressBar: ProgressBar? = null

    @JvmField
    @BindView(R.id.login_error_state)
    internal var loginErrorState: RelativeLayout? = null

    @JvmField
    @BindView(R.id.login_error_text)
    internal var loginErrorText: TextView? = null

    @JvmField
    @BindView(R.id.version_name)
    internal var versionNameText: TextView? = null

    private var iconViewPassword: Drawable? = null

    private var usernameGradient: GradientDrawable? = null

    private var passwordGradient: GradientDrawable? = null

    private var isUsernameFilled: Boolean = false

    private var isPasswordFilled: Boolean = false

    private var usernameTextChangedListener = object : TextChangedListener() {
        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            isUsernameFilled = s.isNotEmpty()
            switchLoginButton()
        }
    }

    private var passwordTextChangedListener = object : TextChangedListener() {
        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            isPasswordFilled = s.isNotEmpty()
            switchLoginButton()
        }
    }

    private var passwordFieldTouchListener = fun(_: View?, event: MotionEvent): Boolean {
        val DRAWABLE_RIGHT = 2
        if (event.action == MotionEvent.ACTION_UP) {
            if (event.rawX >= passwordField!!.right - passwordField!!.compoundDrawables[DRAWABLE_RIGHT].bounds.width()) {
                revealPassword()
                return true
            }
        }
        return false
    }

    override fun layoutResID(): Int {
        return R.layout.fragment_login
    }

    override fun providePresenter(): LoginPresenter {
        return LoginPresenter(context)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initiateDrawable()
        setListener()
        versionNameText?.text = ShopperApplication.getInstance().version
    }

    override fun onDestroyView() {
        removeListener()

        super.onDestroyView()
    }

    override fun showProgress() {
        progressBar!!.visibility = View.VISIBLE
        loginButton!!.isEnabled = false
    }

    override fun hideProgress() {
        progressBar!!.visibility = View.GONE
        loginButton!!.isEnabled = true
    }

    override fun showLoginNotMatch() {
        showNotMatchSignal()
    }

    override fun showLoginFailed() {
        showFailedRetryPopup()
    }

    override fun showLoginSuccessful() {
        hideNotMatchSignal()
        Toast.makeText(context, R.string.login_successful, Toast.LENGTH_SHORT).show()
    }

    //region Private Methods

    private fun initiateDrawable() {
        val usernameLayer = usernameField!!.background as LayerDrawable
        usernameGradient = usernameLayer.findDrawableByLayerId(R.id.item2) as GradientDrawable

        val passwordLayer = passwordField!!.background as LayerDrawable
        passwordGradient = passwordLayer.findDrawableByLayerId(R.id.item2) as GradientDrawable

        iconViewPassword = ContextCompat.getDrawable(context, R.drawable.icon_view_password)
    }

    private fun setListener() {
        setEmptyFormListener()

        passwordField?.setOnTouchListener(passwordFieldTouchListener)
        loginButton?.setOnClickListener { _ -> loginButtonTapped() }
    }

    private fun removeListener() {
        removeEmptyFormListener()
    }

    private fun setEmptyFormListener() {
        usernameField?.addTextChangedListener(usernameTextChangedListener)
        passwordField?.addTextChangedListener(passwordTextChangedListener)
    }

    private fun removeEmptyFormListener() {
        usernameField?.removeTextChangedListener(usernameTextChangedListener)
        passwordField?.removeTextChangedListener(passwordTextChangedListener)
    }

    private fun switchLoginButton() {
        loginButton!!.isEnabled = isUsernameFilled && isPasswordFilled
    }

    private fun loginButtonTapped() {
        val username = usernameField!!.text.toString()
        val password = passwordField!!.text.toString()

        presenter().login(username, password)
    }

    private fun revealPassword() {
        val tm = passwordField?.transformationMethod
        if (tm != null) {
            passwordField?.transformationMethod = null
        }
        else {
            passwordField?.transformationMethod = PasswordTransformationMethod()
        }
    }

    private fun showNotMatchSignal() {
        loginErrorText?.text = getString(R.string.login_not_match)

        passwordField!!.text.clear()

        usernameGradient!!.setStroke(2, ContextCompat.getColor(context, R.color.snd_color_code_1))
        passwordGradient!!.setStroke(2, ContextCompat.getColor(context, R.color.snd_color_code_1))

        val shake = TranslateAnimation(0f, 10f, 0f, 0f)
        shake.duration = 200
        shake.interpolator = CycleInterpolator(7f)

        usernameField!!.startAnimation(shake)
        passwordField!!.startAnimation(shake)
    }

    private fun hideNotMatchSignal() {
        loginErrorText!!.text = getString(R.string.empty_string)

        usernameGradient!!.setStroke(2, ContextCompat.getColor(context, R.color.snd_color_text))
        passwordGradient!!.setStroke(2, ContextCompat.getColor(context, R.color.snd_color_text))
    }

    private fun showFailedRetryPopup() {
        val positiveColor = ContextCompat.getColor(context, R.color.snd_color_warning)

        val failedRetryDialog = MaterialDialog.Builder(activity).title(R.string.alert_title_failed_login)
                .content(R.string.alert_message_try_again)
                .negativeText(R.string.alert_button_ok).positiveText(R.string.alert_button_retry)
                .positiveColor(positiveColor)
                .onNegative { _, _ -> clearAllForm() }
                .onPositive { _, _ -> loginButtonTapped() }

        failedRetryDialog.show()
    }

    private fun clearAllForm() {
        usernameField!!.text.clear()
        passwordField!!.text.clear()
    }

    //endregion
}
