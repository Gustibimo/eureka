package com.happyfresh.fulfillment.widgets.Dialogs.ValueChangeDialog

/**
 * Created by galuh on 11/8/17.
 */
interface ValueChangeListener {

    fun onValueChange(newValue: String)
}