package com.happyfresh.fulfillment.modules.Tag

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import butterknife.BindView
import butterknife.OnClick
import com.afollestad.materialdialogs.MaterialDialog
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.abstracts.BaseFragment
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import java.io.File

/**
 * Created by galuh on 11/24/17.
 */
abstract class TagFragment<out T : TagPresenter<*, *, *>> : BaseFragment<T>(), TagViewBehavior {

    @JvmField
    @BindView(R.id.hint_capture)
    internal var hintOpenCamera: View? = null

    @JvmField
    @BindView(R.id.hint_capture_text)
    var captureText: TextView? = null

    @JvmField
    @BindView(R.id.open_camera_button)
    internal var openCameraButton: View? = null

    @JvmField
    @BindView(R.id.camera_result_image)
    internal var cameraResultImage: ImageView? = null

    @JvmField
    @BindView(R.id.camera_indicator)
    internal var cameraIndicator: ImageView? = null

    @JvmField
    @BindView(R.id.image_progress_bar)
    internal var progressBar: ProgressBar? = null

    @JvmField
    @BindView(R.id.submit_image_button)
    internal var submitButton: Button? = null

    private var isSubmitButtonEnabled: Boolean = false

    abstract var warningContentRes: Int

    override fun layoutResID(): Int {
        return R.layout.fragment_tag
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        openCamera()
    }

    fun showDiscardNewValueDialog() {
        val dialogBuilder = MaterialDialog.Builder(context)

        dialogBuilder.title(R.string.alert_title_warning)
        dialogBuilder.content(warningContentRes)
        dialogBuilder.negativeText(R.string.alert_button_cancel)
        dialogBuilder.positiveText(R.string.alert_button_discard)
        dialogBuilder.onPositive { _, _ -> (activity as TagActivity<*, *>).discardNewValueDialog() }
        dialogBuilder.build().show()
    }

    fun processCameraOutput() {
        presenter().processCameraOutput()
    }

    @OnClick(R.id.open_camera_button)
    fun openCamera() {
        presenter().openCamera()
    }

    override fun showErrorDialog(errorMessage: String) {
        val dialogBuilder = MaterialDialog.Builder(activity)
        dialogBuilder.title(R.string.alert_title_error)
        dialogBuilder.content(errorMessage)
        dialogBuilder.positiveText(R.string.alert_button_ok)

        dialogBuilder.build().show()
    }

    override fun showTagImage(imageFile: File) {
        Picasso.with(activity).load(imageFile).skipMemoryCache().into(cameraResultImage, object : Callback {
            override fun onSuccess() {
                hideProgressBar()
            }

            override fun onError() {
                hideProgressBar()
            }
        })

        hintOpenCamera?.visibility = View.GONE

        cameraResultImage?.visibility = View.VISIBLE
        cameraIndicator?.visibility = View.VISIBLE
    }

    override fun enableSubmitButton(isEnabled: Boolean) {
        isSubmitButtonEnabled = isEnabled
        tryEnableSubmitButton()
    }

    override fun hideCameraIndicator() {
        cameraResultImage?.visibility = View.GONE
        cameraIndicator?.visibility = View.GONE
    }

    fun tryEnableSubmitButton() {
        submitButton?.isEnabled = isSubmitButtonEnabled
    }

    fun hideProgressBar() {
        progressBar?.visibility = View.GONE
    }

    @OnClick(R.id.submit_image_button)
    fun submitPhoto() {
        presenter().prepareSubmitPhoto(bundle)
    }
}