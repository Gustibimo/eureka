package com.happyfresh.fulfillment.modules.Pause

import com.happyfresh.fulfillment.abstracts.ViewBehavior

/**
 * Created by aldi on 7/26/17.
 */

interface PauseViewBehavior : ViewBehavior {

    fun showDate(date: String)

    fun showTime(timer: String)
}