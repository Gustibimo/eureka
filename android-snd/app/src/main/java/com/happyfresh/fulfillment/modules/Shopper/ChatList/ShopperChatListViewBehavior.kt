package com.happyfresh.fulfillment.modules.Shopper.ChatList

import com.happyfresh.fulfillment.abstracts.ViewBehavior

/**
 * Created by kharda on 15/8/18
 */
interface ShopperChatListViewBehavior : ViewBehavior {

    fun setAdapter(adapter: ShopperChatListAdapter)
}