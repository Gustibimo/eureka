package com.happyfresh.fulfillment.widgets.Store

import android.content.Context
import com.happyfresh.fulfillment.abstracts.BaseInteractor
import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.fulfillment.abstracts.BaseRouter
import com.happyfresh.fulfillment.widgets.ViewHolder
import com.happyfresh.snowflakes.hoverfly.models.IServiceModel
import com.happyfresh.snowflakes.hoverfly.models.StockLocation

/**
 * Created by aldi on 6/16/17.
 */

class StorePresenter(context: Context) : BasePresenter<BaseInteractor<*>, BaseRouter, StoreViewBehavior>(context) {

    internal var adapter: StoreAdapter? = null

    internal var stockLocations: MutableList<StockLocation>? = null

    override fun provideInteractor(): BaseInteractor<*>? {
        return null
    }

    override fun provideRouter(context: Context): BaseRouter? {
        return null
    }

    fun setData(stockLocations: MutableList<StockLocation>) {
        this.stockLocations = stockLocations
        this.adapter = StoreAdapter(context, this.stockLocations!!)

        view()?.initiateView(this.adapter!!)
    }

    fun resetData() {
        if (stockLocations != null) {
            stockLocations!!.clear()
            adapter!!.notifyDataSetChanged()
        }
    }

    fun addAllData(stores: List<StockLocation>) {
        resetData()

        stockLocations!!.addAll(stores)
        adapter!!.notifyDataSetChanged()
    }

    val selectedStore: StockLocation?
        get() = adapter?.selectedStockLocation
}
