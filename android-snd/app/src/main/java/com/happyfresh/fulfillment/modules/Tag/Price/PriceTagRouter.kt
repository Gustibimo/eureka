package com.happyfresh.fulfillment.modules.Tag.Price

import android.content.Context
import android.os.Bundle
import com.happyfresh.fulfillment.abstracts.BaseActivity
import com.happyfresh.fulfillment.abstracts.BaseFragment
import com.happyfresh.fulfillment.modules.Tag.TagRouter

/**
 * Created by galuh on 11/13/17.
 */
class PriceTagRouter(context: Context) : TagRouter(context) {
    override fun viewClass(): Class<out BaseActivity<BaseFragment<*>, *>>? {
        return PriceTagActivity::class.java
    }

    fun startWithOrderNumber(orderNumber: String?, newValue: String?) {
        val bundle = Bundle().apply {
            putString(ORDER_NUMBER, orderNumber)
            putString(OUTPUT_NEW_VALUE, newValue)
        }

        startForResult(bundle, PriceChangeRequestCode)
    }
}