package com.happyfresh.fulfillment.widgets.PendingHandoverList

import com.happyfresh.fulfillment.abstracts.InteractorOutput
import com.happyfresh.snowflakes.hoverfly.models.GrabExpressCancelReason
import com.happyfresh.snowflakes.hoverfly.models.PendingHandover

/**
 * Created by kharda on 28/06/18.
 */

interface PendingHandoverListLayoutInteractorOutput : InteractorOutput {

    fun showLoading(o: Boolean)

    fun unableLoadData()

    fun prepareShowPendingHandover(pendingHandoverList: List<PendingHandover>)

    fun showData(pendingHandoverList: List<PendingHandover>)

    fun showEmpty()

    fun showCancelBookingDialog(orderNumber: String, grabExpressCancelReasons: List<GrabExpressCancelReason>)
}