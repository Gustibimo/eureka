package com.happyfresh.fulfillment.modules.Tag.Price

import android.os.Bundle
import com.happyfresh.fulfillment.modules.Tag.TagInteractor
import com.happyfresh.fulfillment.modules.Tag.TagInteractorOutput
import com.happyfresh.fulfillment.modules.Tag.TagRouter

/**
 * Created by galuh on 11/13/17.
 */
class PriceTagInteractor : TagInteractor<TagInteractorOutput>() {
    override var photoFileFormat = "PRICETAG_%s_%d.jpg"

    override var photoDirectory = "pricetags"

    override fun initialize(extras: Bundle?) {
        orderNumber = extras?.getString(TagRouter.ORDER_NUMBER)
    }
}