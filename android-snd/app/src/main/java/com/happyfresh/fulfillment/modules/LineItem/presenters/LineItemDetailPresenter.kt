package com.happyfresh.fulfillment.modules.LineItem.presenters

import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.fulfillment.abstracts.InteractorOutput
import com.happyfresh.fulfillment.abstracts.ViewBehavior
import com.happyfresh.fulfillment.modules.LineItem.interactors.LineItemDetailInteractor
import com.happyfresh.fulfillment.modules.LineItem.LineItemDetailRouter
import com.happyfresh.snowflakes.hoverfly.models.LineItem
import org.parceler.Parcels

abstract class LineItemDetailPresenter<out T : LineItemDetailInteractor<*>, out V : ViewBehavior>(context: Context) :
        BasePresenter<T, LineItemDetailRouter, V>(context), InteractorOutput {

    init {
        interactor().context = context
    }

    override fun provideRouter(context: Context): LineItemDetailRouter? {
        return LineItemDetailRouter(context)
    }

    fun initialize(extras: Bundle?) {
        val parcelable: Parcelable? = extras?.getParcelable(LineItemDetailRouter.SELECTED_LINE_ITEM)
        interactor().initialize(Parcels.unwrap<LineItem>(parcelable))
    }
}