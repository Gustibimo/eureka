package com.happyfresh.fulfillment.widgets.ShoppingList

import com.happyfresh.fulfillment.abstracts.BaseInteractor
import com.happyfresh.snowflakes.hoverfly.models.Batch
import com.happyfresh.snowflakes.hoverfly.models.LineItem
import com.happyfresh.snowflakes.hoverfly.services.BatchService

/**
 * Created by ifranseda on 10/6/17.
 */

class ShoppingListLayoutInteractor : BaseInteractor<ShoppingListLayoutInteractorOutput>() {

    private val batchService: BatchService by lazy { BatchService() }

    var batch: Batch? = null
        set(value) {
            field = value

            output().shoppingNotInProgress(!(field?.hasStartedShopping() ?: false))

            field?.stockLocation()?.remoteId?.let { stockLocationId ->
                val items = field?.allItems(stockLocationId)
                if (this.batch?.isActive!! && items?.isNotEmpty() == true) {
                    output().prepareShoppingList(sortItems(items))
                }
                else {
                    fetchShoppingList()
                }
            }
        }

    fun fetchShoppingList() {
        val observable = this.batch?.let {
            observe(batchService.shoppingLineItems(it)).subscribe(
                    { items -> output().prepareShoppingList(sortItems(items)) },
                    { error ->
                        error.localizedMessage?.let {
                            output().showToast(it)
                        }
                        output().prepareShoppingList(ArrayList())
                    })
        }

        observable?.let {
            subscriptions().add(it)
        }
    }

    private fun sortItems(items: List<LineItem>?): MutableList<LineItem> {
        items?.let {
            return items.sortedWith(compareByDescending<LineItem>{ it.taxonOrder }.thenBy { it.taxonName }.thenBy { it.variant?.name }) as MutableList<LineItem>
        }

        return ArrayList()
    }
}
