package com.happyfresh.fulfillment.widgets.PendingHandoverList

/**
 * Created by kharda on 7/3/18.
 */
interface GrabExpressBookingListener {

    fun onCancelBooking(orderNumber: String)

    fun onCreateBooking(orderNumber: String)

    fun onChangeBookingToHF(orderNumber: String)
}