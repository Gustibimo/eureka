package com.happyfresh.fulfillment.widgets.PendingHandoverList

import android.content.Context
import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.fulfillment.abstracts.BaseRouter
import com.happyfresh.snowflakes.hoverfly.models.GrabExpressCancelReason
import com.happyfresh.snowflakes.hoverfly.models.PendingHandover

class PendingHandoverListLayoutPresenter(context: Context) : BasePresenter<PendingHandoverListLayoutInteractor, BaseRouter, PendingHandoverListLayoutViewBehavior>(context), PendingHandoverListLayoutInteractorOutput, GrabExpressBookingListener {

    private var adapter: PendingHandoverListAdapter? = null

    private var pendingHandoverList: MutableList<PendingHandover> = mutableListOf()

    init {
        interactor().context = context
    }

    override fun provideInteractor(): PendingHandoverListLayoutInteractor? {
        return PendingHandoverListLayoutInteractor()
    }

    override fun provideRouter(context: Context): BaseRouter? {
        return null
    }

    override fun showLoading(o: Boolean) {
        view()?.updateRefreshState(o)
    }

    override fun unableLoadData() {
        view()?.unableLoadData()
    }

    override fun prepareShowPendingHandover(pendingHandoverList: List<PendingHandover>) {
        if (pendingHandoverList.isEmpty()) {
            showEmpty()
        } else {
            interactor().prepareShowPendingHandover(pendingHandoverList)
        }
    }

    override fun showData(pendingHandoverList: List<PendingHandover>) {
        this.pendingHandoverList.clear()
        this.pendingHandoverList.addAll(pendingHandoverList)
        if (this.adapter != null) {
            this.adapter?.notifyDataSetChanged()
        } else {
            this.adapter = PendingHandoverListAdapter(context, this.pendingHandoverList)
            view()?.setAdapter(this.adapter!!)
        }
    }

    override fun showEmpty() {
        view()?.showEmpty()
        showData(mutableListOf())
    }

    override fun showCancelBookingDialog(orderNumber: String, grabExpressCancelReasons: List<GrabExpressCancelReason>) {
        view()?.showCancelBookingConfirmationDialog(orderNumber, grabExpressCancelReasons)
    }

    override fun onCancelBooking(orderNumber: String) {
        interactor().fetchCancelBookingReason(orderNumber)
    }

    override fun onCreateBooking(orderNumber: String) {
        view()?.showCreateBookingConfirmationDialog(orderNumber)
    }

    override fun onChangeBookingToHF(orderNumber: String) {
        view()?.showChangeBookingToHFConfirmationDialog(orderNumber)
    }

    fun fetchData() {
        interactor().fetchPendingHandover()
    }

    fun prepareSetBookingActionListener() {
        this.adapter?.setBookingActionListener(this)
    }

    fun cancelBooking(orderNumber: String, cancelReason: Int) {
        interactor().cancelBooking(orderNumber, cancelReason)
    }

    fun createBooking(orderNumber: String) {
        interactor().createBooking(orderNumber)
    }

    fun changeBookingToHF(orderNumber: String) {
        interactor().changeBookingToHF(orderNumber)
    }
}