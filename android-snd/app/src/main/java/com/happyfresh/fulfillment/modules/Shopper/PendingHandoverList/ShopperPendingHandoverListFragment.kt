package com.happyfresh.fulfillment.modules.Shopper.PendingHandoverList

import android.os.Bundle
import android.os.Parcelable
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import butterknife.BindView
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.abstracts.BaseFragment
import com.happyfresh.fulfillment.abstracts.ViewBehavior
import com.happyfresh.fulfillment.widgets.PendingHandoverList.PendingHandoverListLayout
import com.happyfresh.snowflakes.hoverfly.config.Constant
import com.happyfresh.snowflakes.hoverfly.models.PendingHandover
import org.parceler.Parcels

/**
 * Created by kharda on 28/6/18.
 */

class ShopperPendingHandoverListFragment : BaseFragment<ShopperPendingHandoverPresenter>(), ViewBehavior {


    @JvmField
    @BindView(R.id.pending_handover_list_layout)
    var pendingHandoverListLayout: PendingHandoverListLayout? = null

    override fun layoutResID(): Int {
        return R.layout.fragment_pending_handover_list
    }

    override fun providePresenter(): ShopperPendingHandoverPresenter {
        return ShopperPendingHandoverPresenter(context)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)

        val parcelable: Parcelable? = this.bundle?.getParcelable(Constant.PENDING_HANDOVER_LIST)
        val pendingHandoverList: List<PendingHandover>? = Parcels.unwrap(parcelable)
        if (pendingHandoverList != null) {
            pendingHandoverListLayout?.showData(pendingHandoverList)
        } else {
            pendingHandoverListLayout?.fetchData()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)

        inflater!!.inflate(R.menu.pending_handover, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item!!.itemId) {
            R.id.action_refresh_job -> {
                reloadPendingHandover()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun reloadPendingHandover() {
        pendingHandoverListLayout?.fetchData()
    }
}
