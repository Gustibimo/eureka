package com.happyfresh.fulfillment.modules.Shopper.JobList

import com.happyfresh.fulfillment.abstracts.ViewBehavior
import com.happyfresh.snowflakes.hoverfly.models.response.VersionUpdate

/**
 * Created by galuh on 6/9/17.
 */

interface ShopperJobListViewBehavior : ViewBehavior {

    fun showClockOutDialog(storeLocation: String?)

    fun showRequireUpdateDialog(data: VersionUpdate)

    fun togglePendingHandoverIcon(isNotEmpty: Boolean)
}
