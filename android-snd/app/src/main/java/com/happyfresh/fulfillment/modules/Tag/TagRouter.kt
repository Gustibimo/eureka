package com.happyfresh.fulfillment.modules.Tag

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.content.FileProvider
import com.happyfresh.fulfillment.abstracts.BaseRouter
import java.io.File

/**
 * Created by galuh on 11/24/17.
 */
abstract class TagRouter(context: Context) : BaseRouter(context) {
    override fun detach() { }

    fun openCamera(tagFilePath: String?) {
        val cameraOutput = FileProvider.getUriForFile(context, context.packageName + ".fileprovider", File(tagFilePath))
        startFileProvider(MediaStore.ACTION_IMAGE_CAPTURE, MediaStore.EXTRA_OUTPUT, cameraOutput, CAMERA_REQUEST_CODE)
    }

    fun trySubmitPhoto(tagFilePath: String?, bundle: Bundle?) {
        val activity = (context as Activity)
        bundle?.let {
            activity.setResult(Activity.RESULT_OK, Intent().apply {
                putExtras(bundle)
                putExtra(TagRouter.OUTPUT_PATH, tagFilePath)
            })

            activity.finish()
        }
    }

    companion object {
        val CAMERA_REQUEST_CODE: Int = 1001

        val OUTPUT_PATH: String = "TAG.OUTPUT_PATH"
        val OUTPUT_NEW_VALUE: String = "TAG.OUTPUT_NEW_VALUE"
        val OUTPUT_TYPE_ID: String = "TAG.OUTPUT_TYPE_ID"

        val ORDER_NUMBER = "TAG.ORDER_NUMBER"

        val PriceChangeRequestCode = 12
        val WeightDiffRequestCode = 13
        val ProductMismatchRequestCode = 14
    }
}