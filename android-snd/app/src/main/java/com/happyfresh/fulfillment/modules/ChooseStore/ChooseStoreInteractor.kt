package com.happyfresh.fulfillment.modules.ChooseStore

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.happyfresh.fulfillment.abstracts.BaseInteractor
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.happyfresh.snowflakes.hoverfly.Sprinkles
import com.happyfresh.snowflakes.hoverfly.models.StockLocation
import com.happyfresh.snowflakes.hoverfly.models.UserLocation
import com.happyfresh.snowflakes.hoverfly.models.payload.ClockingPayload
import com.happyfresh.snowflakes.hoverfly.models.payload.PlacementPayload
import com.happyfresh.snowflakes.hoverfly.models.response.PlacementResponse
import com.happyfresh.snowflakes.hoverfly.models.response.StockLocationResponse
import com.happyfresh.snowflakes.hoverfly.services.AppService
import com.happyfresh.snowflakes.hoverfly.services.StockLocationService
import com.happyfresh.snowflakes.hoverfly.shared.subscribers.ServiceSubscriber
import com.happyfresh.snowflakes.hoverfly.stores.StockLocationStore
import com.happyfresh.snowflakes.hoverfly.stores.UserStore
import com.happyfresh.snowflakes.hoverfly.utils.LogUtils
import com.happyfresh.snowflakes.hoverfly.utils.PermissionUtils
import com.happyfresh.snowflakes.hoverfly.models.response.StatusResponse

/**
 * Created by galuh on 6/12/17.
 */

class ChooseStoreInteractor : BaseInteractor<ChooseStoreInteractorOutput>(), GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private var googleApiClient: GoogleApiClient? = null

    private val googleApi = GoogleApiAvailability.getInstance()

    private var activity: Activity? = null

    private var runnable: Runnable? = null

    private val handler = Handler(Looper.getMainLooper())

    private var redirected: Boolean = false

    private val userStore = Sprinkles.stores(UserStore::class.java)

    private val stockLocationStore = Sprinkles.stores(StockLocationStore::class.java)

    private val stockLocationService = Sprinkles.service(StockLocationService::class.java)

    private val appService by lazy { Sprinkles.service(AppService::class.java) }

    private var locationRetrievedListener: LocationRetrievedListener? = object : LocationRetrievedListener {
        override fun onLocationRetrieved(location: Location) {
            if (location.hasAccuracy() && location.accuracy < 2000f) {
                fetchNearbyStoresByLocation(location.latitude, location.longitude)
            }
        }

        override fun onLocationNotRetrieved() {
            showErrorGettingLocation()
        }
    }

    fun setActivityContext(context: Context) {
        this.activity = context as Activity
        this.context = context
    }

    fun checkClockInStatus() {
        val statusObservable = appService.status()
        val subscriber = object : ServiceSubscriber<StatusResponse>() {
            override fun onFailure(e: Throwable) {
                LogUtils.LOG(e.localizedMessage)
            }

            override fun onSuccess(data: StatusResponse) {
                userStore?.saveStatus(data)

                val status: Boolean = data.workingStatus == StatusResponse.WorkingStatus.WORKING || data.workingStatus == StatusResponse.WorkingStatus.PAUSED

                if (status) {
                    userStore?.workingStatus = true

                    stockLocationStore?.currentStoreName = data.stockLocationName
                    stockLocationStore?.currentStoreId = data.stockLocationId

                    dispatch(stockLocationStore?.currentStoreId!!)
                }
            }
        }

        statusObservable.subscribe(subscriber)
    }

    // Start of Google Play Service and Location
    fun prepareGoogleApiClient() {
        servicesConnected()

        this.context?.let {
            googleApiClient = GoogleApiClient.Builder(it)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build()
        }
    }

    private fun servicesConnected(): Boolean {
        this.context?.let {
            val resultCode = googleApi.isGooglePlayServicesAvailable(it)
            return if (ConnectionResult.SUCCESS == resultCode) {
                true
            }
            else {
                googleApi.getErrorDialog(this.activity, resultCode, REQUEST_CODE)?.let {
                    it.setTitle("Location Updates")
                    it.show()
                }

                false
            }
        }

        return false
    }

    private fun removeLocationUpdates() {
        if (googleApiClient!!.isConnected) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this)
        }
    }

    fun connectGoogleApiClient() {
        googleApiClient!!.connect()
    }

    fun disconnectGoogleApiClient() {
        removeLocationUpdates()

        googleApiClient!!.disconnect()
    }

    override fun onConnected(bundle: Bundle?) {
        LogUtils.LOG("Location service connected")
        checkAndRequestLocation()
    }

    override fun onConnectionSuspended(i: Int) {
        LogUtils.LOG("Location service disconnected")
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        LogUtils.LOG("Location service connection failed")
    }

    override fun onLocationChanged(location: Location) {
        handler.removeCallbacks(runnable)
        locationRetrievedListener?.onLocationRetrieved(location)
        removeLocationUpdates()
    }

    fun checkAndRequestLocation() {
        if (!accessLocationGranted()) {
            PermissionUtils.requestPermissionIfNeeded(activity)
        }
        else if (providerEnabled()) {
            startRequestLocation()
        }
    }

    private fun accessLocationGranted(): Boolean {
        return PermissionUtils.accessLocation(context)
    }

    private fun providerEnabled(): Boolean {
        val locationManager = activity!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !locationManager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER)) {
            showErrorGettingLocation()
            return false
        }
        return true
    }

    @SuppressLint("MissingPermission")
    private fun startRequestLocation() {
        runnable = Runnable {
            locationRetrievedListener?.onLocationNotRetrieved()
            removeLocationUpdates()
        }

        handler.postDelayed(runnable, TIMEOUT)

        val locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 3600
        locationRequest.fastestInterval = 60

        if (googleApiClient!!.isConnected) {
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this)
        }
    }

    private fun showErrorGettingLocation() {
        output().drawSignalErrorGettingLocation()
    }

    // End of Google Play Service and Location

    fun fetchNearbyStoresByLocation(lat: Double?, lon: Double?) {
        val distance = "1km"
        val observable = stockLocationService.nearbyStores(lat, lon, distance)

        val subscriber = object : ServiceSubscriber<StockLocationResponse>() {
            override fun onFailure(e: Throwable) {
                e.printStackTrace()
            }

            override fun onSuccess(data: StockLocationResponse) {
                fetchNearbyStoresSuccessful(data)
            }
        }

        observable.subscribe(subscriber)
    }

    fun fetchNearbyStoresSuccessful(data: StockLocationResponse) {
        val stores = data.stockLocations

        if (stores.isEmpty()) {
            output().drawEmptyStores()
        }
        else {
            if (!redirected) {
                redirected = true
                output().drawNearbyStores(stores)
            }
            else {
                output().updateNearbyStores(stores)
            }
        }
    }

    fun refreshNearbyStores() {
        checkAndRequestLocation()
    }

    private fun dispatch(stockLocationId: Long) {
        redirectPage(stockLocationId)
    }

    private fun redirectPage(stockLocationId: Long?) {
        if (userStore?.isShopper == true) {
            output().openShoppingJobList(stockLocationId)
        }
        else {
            output().openDriverJobList()
        }
    }

    fun storeSelected(stockLocation: StockLocation?) {
        if (stockLocation == null) {
            output().drawNoStoreSelected()
        }
        else {
            output().drawClockInDialog(stockLocation)
        }
    }

    fun createPlacement(stockLocation: StockLocation) {
        output().drawSelectStoreButtonDisabled()

        val stockLocationId = mutableListOf(stockLocation.remoteId)

        val isoName = stockLocation.country?.isoName
        val payload = PlacementPayload(stockLocationId)

        val observable = stockLocationService.createPlacement(isoName!!, payload)
        val subscriber = object : ServiceSubscriber<PlacementResponse>() {
            override fun onFailure(e: Throwable) {
                e.printStackTrace()
                output().drawSelectStoreButtonEnabled()
            }

            override fun onSuccess(data: PlacementResponse) {
                stockLocationStore?.savePreferences(stockLocation)

                val latitude = stockLocation.location?.lat?.toDouble()
                val longitude = stockLocation.location?.lat?.toDouble()
                clockIn(latitude, longitude, stockLocation.remoteId, isoName)
            }
        }

        observable.subscribe(subscriber)
    }

    private fun clockIn(latitude: Double?, longitude: Double?, stockLocationId: Long?, isoName: String) {
        val sndResponse = UserLocation()
        sndResponse.latitude = latitude
        sndResponse.longitude = longitude
        sndResponse.stockLocationId = stockLocationId

        val clockingPayload = ClockingPayload(sndResponse)

        val observable = appService.clockIn(clockingPayload)
        val subscriber = object : ServiceSubscriber<UserLocation>() {
            override fun onFailure(e: Throwable) {
                e.printStackTrace()

                output().drawSelectStoreButtonEnabled()
                output().drawStatusIsWorking()
            }

            override fun onSuccess(data: UserLocation) {
                userStore?.workingStatus = true

                redirectPage(stockLocationId)

                output().drawSelectStoreButtonEnabled()
            }
        }

        observable.subscribe(subscriber)
    }

    companion object {
        private val TIMEOUT = 15000L
        private val REQUEST_CODE = 1
    }
}
