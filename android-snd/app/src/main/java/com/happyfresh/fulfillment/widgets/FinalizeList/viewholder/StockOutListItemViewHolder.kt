package com.happyfresh.fulfillment.widgets.FinalizeList.viewholder

import android.content.Context
import android.graphics.PorterDuff
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.Button
import butterknife.BindView
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.fulfillment.widgets.BaseRecyclerViewHolder
import com.happyfresh.fulfillment.widgets.FinalizeList.FinalizeListLayoutAdapter
import com.happyfresh.fulfillment.widgets.LineItem.view.LineItemStockOutLayout
import com.happyfresh.snowflakes.hoverfly.models.LineItem
import com.shopper.components.CircularProgressDrawable
import com.shopper.interfaces.OutOfStockItemListener
import com.shopper.utils.ViewUtils

/**
 * Created by ifranseda on 02/01/2018.
 */
class StockOutListItemViewHolder(context: Context, itemView: View, listener: OutOfStockItemListener) : BaseRecyclerViewHolder<BasePresenter<*, *, *>, LineItem>(context, itemView) {
    @JvmField
    @BindView(R.id.line_item)
    var stockOutItemLayout: LineItemStockOutLayout? = null

    @JvmField
    @BindView(R.id.item_order_finalize)
    var finalizeButton: Button? = null

    var listener : OutOfStockItemListener = listener

    private var progressing : Boolean = false

    override fun bind(data: LineItem) {
        super.bind(data)
        stockOutItemLayout?.lineItem = data
    }

    fun otherView(data: LineItem, viewType: Int) {
        if (viewType == FinalizeListLayoutAdapter.TOP || viewType == FinalizeListLayoutAdapter.TOP_BOTTOM || viewType == FinalizeListLayoutAdapter.TOP_REPLACED || viewType == FinalizeListLayoutAdapter.TOP_BOTTOM_REPLACED || viewType == FinalizeListLayoutAdapter.TOP_FINALIZED) {
            stockOutItemLayout?.showHeader()
        }
        else {
            stockOutItemLayout?.hideHeader()
        }

        if (viewType == FinalizeListLayoutAdapter.BOTTOM || viewType == FinalizeListLayoutAdapter.TOP_BOTTOM || viewType == FinalizeListLayoutAdapter.BOTTOM_REPLACED || viewType == FinalizeListLayoutAdapter.TOP_BOTTOM_REPLACED) {
            stockOutItemLayout?.showFooter()
            showProgress(false)

            finalizeButton?.setOnClickListener {
                showProgress(true)
                finalizeButton?.isEnabled = false
                displayButtonProgress()

                listener.finalize(data.shipment, adapterPosition)
            }

            if (!progressing) {
                finalizeButton?.isEnabled = true
                hideButtonProgress()
            }
        }
        else {
            stockOutItemLayout?.hideFooter()
        }
    }

    fun showProgress(progress: Boolean) {
        this.progressing = progress
    }

    private fun displayButtonProgress() {
        val drawable = CircularProgressDrawable(android.R.color.white, 4f)
        finalizeButton?.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null)

        val left = ViewUtils.dpToPx(-40.0)
        val top = ViewUtils.dpToPx(-15.0)
        val right = ViewUtils.dpToPx(-10.0)
        val bottom = ViewUtils.dpToPx(15.0)
        drawable.setBounds(left, top, right, bottom)

        drawable.setColorFilter(ContextCompat.getColor(context, android.R.color.white), PorterDuff.Mode.LIGHTEN)
        drawable.start()
    }

    private fun hideButtonProgress() {
        finalizeButton?.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null)
    }
}