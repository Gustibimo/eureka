package com.happyfresh.fulfillment.widgets.FinalizeList

import android.content.Context
import android.text.Html
import android.util.AttributeSet
import android.view.View
import butterknife.BindView
import com.afollestad.materialdialogs.MaterialDialog
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.widgets.BaseRecyclerViewLayout
import com.happyfresh.snowflakes.hoverfly.models.Shipment
import com.shopper.components.CircularProgressBar

/**
 * Created by ifranseda on 03/01/2018.
 */
class FinalizeListLayout @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
        BaseRecyclerViewLayout<FinalizeListLayoutAdapter, FinalizeListLayoutPresenter>(context, attrs, defStyleAttr),
        FinalizeListLayoutViewBehavior {

    @JvmField
    @BindView(R.id.progress_container)
    var progressContainer: View? = null

    @JvmField
    @BindView(R.id.progress_bar)
    var progressBar: CircularProgressBar? = null

    override fun layoutResID(): Int {
        return R.layout.widget_finalize_list
    }

    override fun providePresenter(): FinalizeListLayoutPresenter? {
        return FinalizeListLayoutPresenter(context)
    }

    override fun isSwipeRefreshLayoutEnabled(): Boolean {
        return true
    }

    override fun onSwipeRefresh() {
        presenter().loadFinalizationList()
    }

    override fun initialize() {
        super.initialize()
        updateRefreshState(true)
    }

    override fun setAdapter(adapter: FinalizeListLayoutAdapter) {
        super.setAdapter(adapter)
        presenter().prepareSetOOSItemListener()
    }

    // region FinalizeListLayoutViewBehavior

    override fun hideProgress() {
        updateRefreshState(false)

        progressContainer?.visibility = View.GONE
        progressBar?.visibility = View.GONE
    }

    override fun showProgress() {
        updateRefreshState(true)

        progressContainer?.visibility = View.VISIBLE
        progressBar?.visibility = View.VISIBLE
    }

    override fun showFinalizeDialog(shipment: Shipment, position: Int) {
        val inputDialog = MaterialDialog.Builder(context)
        inputDialog.title(R.string.alert_title_warning)
        inputDialog.cancelable(false)
        inputDialog.content(Html.fromHtml(context.getString(R.string.alert_finalize_shopping_job, shipment?.order?.number!!)))
        inputDialog.negativeText(R.string.alert_button_no)
        inputDialog.positiveText(R.string.alert_button_yes)

        inputDialog.onPositive { _, _ -> presenter().prepareFinalize(shipment, position) }

        inputDialog.onNegative { _, _ -> presenter().hideButtonProgress() }

        inputDialog.show()
    }

    override fun reload() {
        onSwipeRefresh()
    }

    // endregion

    fun setBatch(batchId: Long) {
        presenter().setBatch(batchId)
    }
}