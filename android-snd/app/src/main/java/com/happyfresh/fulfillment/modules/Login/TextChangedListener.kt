package com.happyfresh.fulfillment.modules.Login

import android.text.Editable
import android.text.TextWatcher

/**
 * Created by galuh on 7/25/17.
 */

abstract class TextChangedListener : TextWatcher {

    override fun afterTextChanged(s: Editable) {}

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
}