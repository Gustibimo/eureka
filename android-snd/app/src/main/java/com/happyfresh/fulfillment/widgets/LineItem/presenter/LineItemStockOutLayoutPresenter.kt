package com.happyfresh.fulfillment.widgets.LineItem.presenter

import android.content.Context
import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.fulfillment.abstracts.BaseRouter
import com.happyfresh.fulfillment.widgets.LineItem.LineItemStockOutLayoutInteractor
import com.happyfresh.fulfillment.widgets.LineItem.LineItemStockOutLayoutInteractorOutput
import com.happyfresh.fulfillment.widgets.LineItem.LineItemStockOutLayoutViewBehavior

/**
 * Created by ifranseda on 03/10/2017.
 */

class LineItemStockOutLayoutPresenter(context: Context) :
        BasePresenter<LineItemStockOutLayoutInteractor, BaseRouter, LineItemStockOutLayoutViewBehavior>(context),
        LineItemStockOutLayoutInteractorOutput {
    override fun provideInteractor(): LineItemStockOutLayoutInteractor? {
        return LineItemStockOutLayoutInteractor()
    }

    override fun provideRouter(context: Context): BaseRouter? {
        return null
    }

    override fun showFinalizeItemDialog(orderNumber: String) {

    }

    fun prepareFinalizeItem() {
        interactor().prepareFinalizeItem()
    }
}