package com.happyfresh.fulfillment.modules.Tag.Price

import android.os.Bundle
import android.view.View
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.modules.Tag.TagFragment

/**
 * Created by galuh on 11/13/17.
 */
class PriceTagFragment : TagFragment<PriceTagPresenter>() {
    override var warningContentRes = R.string.discard_new_price

    override fun providePresenter(): PriceTagPresenter {
        return PriceTagPresenter(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        presenter().initialize(this.bundle)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        captureText?.text = getString(R.string.tap_capture_price_tag)
    }
}