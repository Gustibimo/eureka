package com.happyfresh.fulfillment.widgets.Dialogs.ItemFoundDialog

import android.content.Context
import com.happyfresh.fulfillment.abstracts.BaseInteractor
import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.fulfillment.abstracts.BaseRouter

/**
 * Created by galuh on 10/26/17.
 */
class ValueChangeDialogPresenter(context: Context) : BasePresenter<BaseInteractor<*>, BaseRouter, ValueChangeDialogViewBehavior>(context) {

    override fun provideInteractor(): BaseInteractor<*>? {
        return null
    }

    override fun provideRouter(context: Context): BaseRouter? {
        return null
    }
}