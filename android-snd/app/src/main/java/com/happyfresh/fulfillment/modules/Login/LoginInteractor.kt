package com.happyfresh.fulfillment.modules.Login

import android.util.Base64
import com.appboy.Appboy
import com.happyfresh.fulfillment.abstracts.BaseInteractor
import com.happyfresh.snowflakes.hoverfly.Sprinkles
import com.happyfresh.snowflakes.hoverfly.config.Config
import com.happyfresh.snowflakes.hoverfly.models.User
import com.happyfresh.snowflakes.hoverfly.services.LoginService
import com.happyfresh.snowflakes.hoverfly.shared.subscribers.ServiceSubscriber
import com.happyfresh.snowflakes.hoverfly.stores.UserStore
import com.happyfresh.snowflakes.hoverfly.utils.LogUtils
import retrofit2.adapter.rxjava.HttpException
import java.io.UnsupportedEncodingException

/**
 * Created by galuh on 6/9/17.
 */

class LoginInteractor : BaseInteractor<LoginInteractorOutput>() {

    private val loginService = Sprinkles.service(LoginService::class.java)

    private val userStore = Sprinkles.stores(UserStore::class.java)

    fun login(username: String, password: String) {
        val builder = StringBuilder()
        builder.append(username).append(":").append(password)

        val basicAuth = encodeBasicAuth(builder.toString())

        val loginObservable = loginService.login(basicAuth, Config.ClientToken)
        val subscriber = object : ServiceSubscriber<User>() {
            override fun onFailure(e: Throwable) {
                LogUtils.LOG(e.localizedMessage)
                var errorCode = 0
                if (e is HttpException) {
                    errorCode = e.code()
                }

                loginFailed(errorCode)
            }

            override fun onSuccess(data: User) {
                LogUtils.LOG("onSuccess >>> " + data)
                userStore?.saveUser(data)
                Appboy.getInstance(context).changeUser(data.remoteId.toString())

                loginSuccessful()
            }
        }

        loginObservable.subscribe(subscriber)
    }

    private fun loginFailed(errorCode: Int) {
        if (errorCode == 401) {
            output().tellLoginNotMatch()
        }
        else {
            output().tellLoginFailed()
        }
    }

    private fun loginSuccessful() {
        output().tellLoginSuccessful()
        output().openChooseStore()
    }

    private fun encodeBasicAuth(string: String): String {
        var base64 = ""

        try {
            val data = string.toByteArray(charset("UTF-8"))
            base64 = Base64.encodeToString(data, Base64.NO_WRAP)
        }
        catch (ex: UnsupportedEncodingException) {
            ex.printStackTrace()
        }

        return "Basic " + base64
    }
}
