package com.happyfresh.fulfillment.widgets.LineItem.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.OnClick
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.widgets.Dialogs.PhoneCallDialog.PhoneCallDialog
import com.happyfresh.fulfillment.widgets.LineItem.LineItemStockOutLayoutViewBehavior
import com.happyfresh.fulfillment.widgets.LineItem.presenter.LineItemStockOutLayoutPresenter
import com.shopper.common.Constant

/**
 * Created by ifranseda on 03/01/2018.
 */
class LineItemStockOutLayout
@JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
        LineItemLayout<LineItemStockOutLayoutPresenter>(context, attrs, defStyleAttr), LineItemStockOutLayoutViewBehavior {
    @JvmField
    @BindView(R.id.ouf_of_stock_header)
    var headerContainer: View? = null

    @JvmField
    @BindView(R.id.item_order_number)
    var itemOrderNumber: TextView? = null

    @JvmField
    @BindView(R.id.item_customer_name)
    var itemCustomerName: TextView? = null

    @JvmField
    @BindView(R.id.corporate_flag)
    var corporateFlag: TextView? = null

    @JvmField
    @BindView(R.id.item_order_checked)
    var itemOrderChecked: ImageView? = null

    @JvmField
    @BindView(R.id.replacement_options)
    var replacementOptions: View? = null

    @JvmField
    @BindView(R.id.call_customer_button)
    var callCustomerButton: Button? = null

    @JvmField
    @BindView(R.id.out_of_stock_footer)
    var footerContainer: View? = null

    @JvmField
    @BindView(R.id.item_order_finalize)
    var finalizeButton: Button? = null

    // region BaseLayout

    override fun initialize() {
        categoryContainer?.visibility = View.GONE
        orderContainer?.visibility = View.GONE
    }

    // endregion

    // region LineItemLayout

    override fun layoutResID(): Int {
        return R.layout.widget_line_item_stockout
    }

    override fun providePresenter(): LineItemStockOutLayoutPresenter? {
        return LineItemStockOutLayoutPresenter(context)
    }

    override fun showReplacementOptions() {
        super.showReplacementOptions()

        if (this.lineItem?.replacementType.equals(Constant.REPLACE_FREELY, ignoreCase = true)) {
            replacementOptions?.visibility = View.GONE
        }
        else {
            replacementOptions?.visibility = View.VISIBLE
            callCustomerButton?.visibility = View.VISIBLE
        }
    }

    override fun getShoppedItem(): Int {
        return maxOf(super.getShoppedItem() - this.lineItem?.bundleQuantity!!, 0)
    }

    override fun getTotalItem(): Int {
        return this.lineItem?.total!! - this.lineItem?.bundleQuantity!!
    }

    // endregion

    // region Public methods

    fun showHeader() {
        headerContainer?.visibility = View.VISIBLE

        this.lineItem?.shipment?.let { savedShipment ->
            itemOrderNumber?.text = savedShipment.order?.number
            itemCustomerName?.text = savedShipment.customerName

            if (savedShipment.order?.companyId != 0L) {
                corporateFlag?.visibility = View.VISIBLE
            }
            else {
                corporateFlag?.visibility = View.GONE
            }
        }
    }

    fun hideHeader() {
        headerContainer?.visibility = View.GONE
    }

    fun showFooter() {
        footerContainer?.visibility = View.VISIBLE
    }

    fun hideFooter() {
        footerContainer?.visibility = View.GONE
    }

    @OnClick(R.id.call_customer_button)
    fun callCustomer() {
        val phoneNumber: String? = lineItem!!.shipment!!.address!!.phoneNumber
        val customerName: String? = lineItem!!.shipment!!.customerName

        val phoneCallDialog = PhoneCallDialog(context, R.string.alert_title_call_customer, R.string.alert_message_call_customer, customerName, phoneNumber)
        phoneCallDialog.show()
    }

    // endregion
}