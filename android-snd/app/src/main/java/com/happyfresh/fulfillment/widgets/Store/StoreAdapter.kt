package com.happyfresh.fulfillment.widgets.Store

import android.content.Context
import android.view.View
import android.widget.CheckBox
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.widgets.BaseAdapter
import com.happyfresh.snowflakes.hoverfly.models.StockLocation

/**
 * Created by aldi on 6/16/17.
 */

class StoreAdapter(context: Context, data: List<StockLocation>) : BaseAdapter<StockLocation, StoreViewHolder>(context, data) {
    internal var lastCheckBox: CheckBox? = null

    internal var selectedStockLocation: StockLocation? = null

    override fun itemLayoutResID(type: Int): Int {
        return R.layout.widget_store_item
    }

    override fun provideViewHolder(view: View, type: Int): StoreViewHolder {
        return StoreViewHolder(this.context, view)
    }

    override fun onBindViewHolder(holder: StoreViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.adapter = this
    }
}

