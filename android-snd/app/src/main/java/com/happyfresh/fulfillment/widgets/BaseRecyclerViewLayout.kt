package com.happyfresh.fulfillment.widgets

import android.content.Context
import android.support.annotation.Nullable
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import butterknife.BindView
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.fulfillment.abstracts.RecyclerViewBehavior

/**
 * Created by ifranseda on 05/10/2017.
 */

abstract class BaseRecyclerViewLayout<in B : BaseAdapter<*, *>, out P : BasePresenter<*, *, *>> @JvmOverloads
    constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
        BaseLayout<P>(context, attrs, defStyleAttr), RecyclerViewBehavior<B> {

    @JvmField
    @BindView(R.id.recycler_list)
    internal var recyclerView: RecyclerView? = null

    @JvmField
    @BindView(R.id.swipe_container)
    internal var swipeRefreshLayout: SwipeRefreshLayout? = null

    protected var isRefreshing: Boolean = false
        set(value) {
            field = value

            swipeRefreshLayout?.isRefreshing = value
        }

    override fun initialize() {
        updateSwipeRefresh()
    }

    override fun setAdapter(adapter: B) {
        val linearLayoutManager = LinearLayoutManager(context)
        recyclerView?.layoutManager = linearLayoutManager
        recyclerView?.adapter = adapter
    }

    override fun updateRefreshState(o: Boolean) {
        isRefreshing = o
    }

    open protected fun isSwipeRefreshLayoutEnabled(): Boolean {
        return false
    }

    open protected fun onSwipeRefresh() {
        // Do nothing
    }

    //region Private methods

    private fun updateSwipeRefresh() {
        isRefreshing = false

        if (this.isSwipeRefreshLayoutEnabled()) {
            swipeRefreshLayout?.isEnabled = true
            swipeRefreshLayout?.setColorSchemeResources(R.color.happyfresh_accent,
                                                        R.color.shopping_order_2,
                                                        R.color.shopping_order_1,
                                                        R.color.shopping_order_5)
            swipeRefreshLayout?.setProgressViewOffset(false, 0, resources.getDimensionPixelSize(R.dimen.refresh_indicator_end))

            swipeRefreshLayout?.setOnRefreshListener { onSwipeRefresh() }
        }
        else {
            swipeRefreshLayout?.isEnabled = false
        }
    }

    //endregion
}