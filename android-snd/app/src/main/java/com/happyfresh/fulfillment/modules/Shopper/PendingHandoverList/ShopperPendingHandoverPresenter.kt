package com.happyfresh.fulfillment.modules.Shopper.PendingHandoverList

import android.content.Context
import com.happyfresh.fulfillment.abstracts.BaseInteractor
import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.fulfillment.abstracts.BaseRouter
import com.happyfresh.fulfillment.abstracts.ViewBehavior

/**
 * Created by kharda on 28/06/18.
 */
class ShopperPendingHandoverPresenter(context: Context) : BasePresenter<BaseInteractor<*>, BaseRouter, ViewBehavior>(context) {

    override fun provideInteractor(): BaseInteractor<*>? {
        return null
    }

    override fun provideRouter(context: Context): BaseRouter? {
        return null
    }
}