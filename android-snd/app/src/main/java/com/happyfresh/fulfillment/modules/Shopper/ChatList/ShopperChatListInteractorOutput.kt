package com.happyfresh.fulfillment.modules.Shopper.ChatList

import com.happyfresh.fulfillment.abstracts.InteractorOutput
import com.happyfresh.snowflakes.hoverfly.models.Shipment

/**
 * Created by kharda on 15/8/18
 */
interface ShopperChatListInteractorOutput : InteractorOutput {

    fun showChatList(shipmentList: List<Shipment>)

    fun openChatScreen(shopperId: String, shipment: Shipment)
}