package com.happyfresh.fulfillment.abstracts

/**
 * Created by ifranseda on 10/02/2017.
 */
interface InteractorOutput {

    fun backToLogin()
}
