package com.happyfresh.fulfillment.widgets.JobList

import com.happyfresh.fulfillment.abstracts.BaseInteractor
import com.happyfresh.snowflakes.hoverfly.models.Batch
import com.happyfresh.snowflakes.hoverfly.models.response.BatchesResponse
import com.happyfresh.snowflakes.hoverfly.models.response.VersionUpdate
import com.happyfresh.snowflakes.hoverfly.services.AppService
import com.happyfresh.snowflakes.hoverfly.services.BatchService
import com.happyfresh.snowflakes.hoverfly.shared.events.EventBus
import com.happyfresh.snowflakes.hoverfly.shared.events.RequireUpdateEvent
import com.happyfresh.snowflakes.hoverfly.shared.subscribers.ServiceSubscriber
import com.shopper.common.ShopperApplication

/**
 * Created by ifranseda on 10/10/2017.
 */

class JobListLayoutInteractor : BaseInteractor<JobListLayoutInteractorOutput>() {

    private val batchService by lazy { BatchService() }

    private val appService by lazy { AppService() }

    var batches: MutableList<Batch> = ArrayList()

    fun fetchBatches() {
        batches.clear()

        fetchActive()
    }

    fun updateStartedBatch(batch: Batch) {
        val index = batches.indexOfFirst { it.remoteId == batch.remoteId }
        batches.removeAt(index)
        batches.add(0, batch)

        output().showData(batches)
    }

    private fun fetchActive() {
        output().showLoading(true)

        val showPromotions = false
        val observable = batchService.activeList(showPromotions)
        val subscriber = object : ServiceSubscriber<BatchesResponse>() {
            override fun onFailure(e: Throwable) {
                fetchAvailable()

                checkUpdate()
            }

            override fun onSuccess(data: BatchesResponse) {
                batches.addAll(data.batches)

                fetchAvailable()

                if (data.batches.isEmpty()) {
                    checkUpdate()
                }
            }
        }

        val subscription = observable.subscribe(subscriber)
        subscriptions().add(subscription)
    }

    private fun fetchAvailable() {
        val observable = batchService.available()
        val subscriber = object : ServiceSubscriber<BatchesResponse>() {
            override fun onFailure(e: Throwable) {
                output().showLoading(false)

                unableLoadData()
            }

            override fun onSuccess(data: BatchesResponse) {
                output().showLoading(false)

                batches.addAll(data.batches)

                prepareData()
            }
        }

        val subscription = observable.subscribe(subscriber)
        subscriptions().add(subscription)
    }

    private fun unableLoadData() {
        if (batches.isEmpty()) {
            output().unableLoadData()
        }
        else {
            output().showData(batches)
        }
    }

    private fun prepareData() {
        if (batches.isEmpty()) {
            output().showEmpty()
        }
        else {
            shouldDispatchShoppingProgress()

            output().showData(batches)
        }
    }

    private fun shouldDispatchShoppingProgress() {
        if (initiatePayment().isNotEmpty()) {
            fetchShoppingList(initiatePayment().first())
        }
        else if (pickingLastItem().isNotEmpty()) {
            batchService.activeBatch = pickingLastItem().first()
            output().openBatch(batchService.activeBatch!!)
        }
    }

    private fun pickingLastItem(): List<Batch> {
        return batches.filter { it.shipments().isNotEmpty() && it.shipments()[0].shoppingJob?.isPickingLastItem == true }
    }

    private fun initiatePayment(): List<Batch> {
        return batches.filter { it.shipments().isNotEmpty() && it.shipments()[0].shoppingJob?.isFinalizing == true }
    }

    private fun fetchShoppingList(batch: Batch) {
        val observable = observe(batchService.shoppingLineItems(batch)).subscribe(
                {
                    batchService.activeBatch = initiatePayment().firstOrNull()
                    output().openPayment(batchService.activeBatch!!)
                }, {})

        observable?.let {
            subscriptions().add(it)
        }
    }

    private fun checkUpdate() {
        val observable = appService.checkUpdate(ShopperApplication.getInstance().buildNumber)
        val subscriber = object : ServiceSubscriber<VersionUpdate>() {
            override fun onFailure(e: Throwable) {
                //no op
            }

            override fun onSuccess(data: VersionUpdate) {
                if (data.required) {
                    EventBus.handler.post(RequireUpdateEvent(data))
                }
            }
        }

        val subscription = observable.subscribe(subscriber)
        subscriptions().add(subscription)
    }
}