package com.happyfresh.fulfillment.widgets.Dialogs.ValueChangeDialog.view

import android.content.Context
import android.util.AttributeSet
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.widgets.BaseLayout
import com.happyfresh.fulfillment.widgets.Dialogs.ItemFoundDialog.ValueChangeDialogPresenter
import com.happyfresh.fulfillment.widgets.Dialogs.ItemFoundDialog.ValueChangeDialogViewBehavior

/**
 * Created by galuh on 11/8/17.
 */

class ValueChangeInputFieldLayout @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : BaseLayout<ValueChangeDialogPresenter>(context,
        attrs, defStyleAttr), ValueChangeDialogViewBehavior {

    override fun layoutResID(): Int {
        return R.layout.dialog_value_change_input_field
    }

    override fun providePresenter(): ValueChangeDialogPresenter {
        return ValueChangeDialogPresenter(this.context)
    }
}
