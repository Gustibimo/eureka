package com.happyfresh.fulfillment.modules.StockOut

import com.happyfresh.fulfillment.abstracts.InteractorOutput

/**
 * Created by kharda on 24/8/18
 */
interface StockOutInteractorOutput : InteractorOutput {

    fun showChatBadge(count: Int)

    fun hideChatButton()
}