package com.happyfresh.fulfillment.modules.LineItem

import android.content.Context
import android.os.Bundle
import com.happyfresh.fulfillment.abstracts.BaseActivity
import com.happyfresh.fulfillment.abstracts.BaseFragment
import com.happyfresh.fulfillment.abstracts.BaseRouter
import com.happyfresh.fulfillment.modules.Tag.Mismatch.MismatchTagRouter
import com.happyfresh.fulfillment.modules.Tag.Price.PriceTagRouter
import com.happyfresh.fulfillment.modules.Tag.Weight.WeightTagRouter
import com.happyfresh.snowflakes.hoverfly.models.LineItem
import org.parceler.Parcels

/**
 * Created by ifranseda on 10/10/2017.
 */

class LineItemDetailRouter(context: Context) : BaseRouter(context) {

    private val weightTagRouter by lazy { WeightTagRouter(context) }

    private val priceTagRouter by lazy { PriceTagRouter(context) }

    private val mismatchRouter by lazy { MismatchTagRouter(context) }

    override fun detach() {

    }

    override fun viewClass(): Class<out BaseActivity<BaseFragment<*>, *>>? {
        return LineItemDetailActivity::class.java
    }

    fun startWithLineItem(item: LineItem) {
        val bundle = Bundle()
        bundle.putParcelable(SELECTED_LINE_ITEM, Parcels.wrap(item))
        start(bundle)
    }

    fun openCameraForWeightDifference(orderNumber: String?, typeId: Int, newValue: String?) {
        weightTagRouter.startWithOrderNumber(orderNumber, typeId, newValue)
    }

    fun openCameraForPriceChange(orderNumber: String?, newValue: String?) {
        priceTagRouter.startWithOrderNumber(orderNumber, newValue)
    }

    fun openCameraForProductMismatch(orderNumber: String?) {
        mismatchRouter.startWithOrderNumber(orderNumber)
    }

    companion object {
        val SELECTED_LINE_ITEM: String = "LineItemDetailRouter.SELECTED_LINE_ITEM"
    }
}