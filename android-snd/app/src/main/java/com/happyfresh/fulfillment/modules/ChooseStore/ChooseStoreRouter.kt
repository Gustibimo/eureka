package com.happyfresh.fulfillment.modules.ChooseStore

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.happyfresh.fulfillment.abstracts.BaseActivity
import com.happyfresh.fulfillment.abstracts.BaseFragment
import com.happyfresh.fulfillment.abstracts.BaseRouter
import com.happyfresh.fulfillment.modules.Driver.JobList.DriverJobListRouter
import com.happyfresh.fulfillment.modules.Shopper.JobList.ShopperJobListRouter
import com.shopper.activities.DriverListActivity
import com.shopper.common.Constant.STOCK_LOCATION_ID

/**
 * Created by galuh on 6/12/17.
 */

class ChooseStoreRouter(context: Context) : BaseRouter(context) {

    private val driverJobListRouter by lazy { DriverJobListRouter(context) }

    private val shopperJobListRouter by lazy { ShopperJobListRouter(context) }

    override fun detach() {
        (context as Activity).finish()
    }

    override fun viewClass(): Class<out BaseActivity<out BaseFragment<*>, *>>? {
        return ChooseStoreActivity::class.java
    }

    fun startChooseStore() {
        start()
    }

    fun openShoppingJobList(stockLocationId: Long?) {
        val bundle = Bundle()
        bundle.putLong(STOCK_LOCATION_ID, stockLocationId ?: 0)
        shopperJobListRouter.start(bundle)
        detach()
    }

    fun openDriverJobList() {
        val intent = Intent(context, DriverListActivity::class.java)
        (context as Activity).startActivity(intent)

//        driverJobListRouter.startDriverJobList()
        detach()
    }
}
