package com.happyfresh.fulfillment.widgets.PendingHandoverList

import com.happyfresh.fulfillment.abstracts.RecyclerViewBehavior
import com.happyfresh.snowflakes.hoverfly.models.GrabExpressCancelReason

/**
 * Created by kharda on 28/06/18.
 */

interface PendingHandoverListLayoutViewBehavior : RecyclerViewBehavior<PendingHandoverListAdapter> {

    fun unableLoadData()

    fun showEmpty()

    fun showCancelBookingConfirmationDialog(orderNumber: String, grabExpressCancelReasons: List<GrabExpressCancelReason>)

    fun showCreateBookingConfirmationDialog(orderNumber: String)

    fun showChangeBookingToHFConfirmationDialog(orderNumber: String)
}