package com.happyfresh.fulfillment.modules.StockOut

import android.content.Context
import android.os.Bundle
import com.happyfresh.fulfillment.abstracts.BaseActivity
import com.happyfresh.fulfillment.abstracts.BaseFragment
import com.happyfresh.fulfillment.abstracts.BaseRouter
import com.happyfresh.fulfillment.modules.Shopper.ChatList.ShopperChatListRouter

/**
 * Created by ifranseda on 02/01/2018.
 */
class StockOutRouter(context: Context) : BaseRouter(context) {

    private val shopperChatListRouter by lazy { ShopperChatListRouter(context) }

    override fun detach() { }

    override fun viewClass(): Class<out BaseActivity<BaseFragment<*>, *>>? {
        return StockOutActivity::class.java
    }

    fun startWithBatchID(batchID: Long) {
        start(Bundle().apply {
            putLong(SELECTED_BATCH_ID, batchID)
        })
    }

    fun openShopperChatList(batchId: Long?) {
        batchId?.let {
            shopperChatListRouter.startWithBatchId(batchId)
        }
    }

    companion object {
        val SELECTED_BATCH_ID: String = "StockOutRouter.SELECTED_BATCH_ID"
    }
}