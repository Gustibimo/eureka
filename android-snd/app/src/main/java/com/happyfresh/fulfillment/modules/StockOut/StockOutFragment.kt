package com.happyfresh.fulfillment.modules.StockOut

import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.view.View
import android.widget.TextView
import butterknife.BindView
import butterknife.OnClick
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.abstracts.BaseFragment
import com.happyfresh.fulfillment.widgets.FinalizeList.FinalizeListLayout
import com.shopper.events.ShoppedEvent
import com.squareup.otto.Subscribe

/**
 * Created by ifranseda on 02/01/2018.
 */
class StockOutFragment : BaseFragment<StockOutPresenter>(), StockOutViewBehavior {

    @JvmField
    @BindView(R.id.finalize_list)
    var finalizeList: FinalizeListLayout? = null

    @JvmField
    @BindView(R.id.chat_button)
    var chatButton: FloatingActionButton? = null

    @JvmField
    @BindView(R.id.chat_badge)
    var chatBadge: TextView? = null

    override fun layoutResID(): Int {
        return R.layout.fragment_stock_out_list
    }

    override fun providePresenter(): StockOutPresenter {
        return StockOutPresenter(context)
    }

    override fun isSubscriber(): Boolean {
        return true
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter().initialize(bundle)
    }

    override fun drawFinalizeForBatch(batchId: Long) {
        finalizeList?.setBatch(batchId)
    }

    override fun showChatBadge(count: Int) {
        if (count > 0) {
            chatBadge?.text = count.toString()
            chatBadge?.visibility = View.VISIBLE
        } else {
            chatBadge?.visibility = View.GONE
        }
    }

    override fun hideChatButton() {
        chatButton?.visibility = View.GONE
        chatBadge?.visibility = View.GONE
    }

    override fun onResume() {
        presenter().addChatHandler()

        super.onResume()

        finalizeList?.reload()
    }

    override fun onPause() {
        presenter().removeChatHandler()

        super.onPause()
    }

    @OnClick(R.id.chat_button)
    fun onChatButtonClicked() {
        presenter().openShopperChatList()
    }

    @Subscribe
    fun onReplaced(event: ShoppedEvent) {
        finalizeList?.reload()
    }
}