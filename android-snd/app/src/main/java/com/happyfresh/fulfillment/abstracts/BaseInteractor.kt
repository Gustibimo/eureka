package com.happyfresh.fulfillment.abstracts

import android.content.Context
import com.google.firebase.iid.FirebaseInstanceId
import com.happyfresh.snowflakes.hoverfly.Sprinkles
import com.happyfresh.snowflakes.hoverfly.config.Config
import com.happyfresh.snowflakes.hoverfly.shared.events.EventBus
import com.happyfresh.snowflakes.hoverfly.stores.UserStore
import com.shopper.utils.LogUtils
import org.jetbrains.anko.doAsync
import rx.Observable
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import java.io.IOException
import java.util.*

/**
 * Created by ifranseda on 10/02/2017.
 */
abstract class BaseInteractor<out O : InteractorOutput> {

    private var subscriptions: MutableList<Subscription>? = null

    private var output: O? = null

    private var _context: Context? = null
    var context: Context?
        get() = _context
        set(value) {
            _context = value
        }

    init {
        startListenEventBus()
    }

    protected fun output(): O {
        if (this.output == null) {
            throw RuntimeException("No interactor output")
        }

        return this.output as O
    }

    fun setOutput(output: InteractorOutput) {
        this.output = output as? O
    }

    protected fun subscriptions(): MutableList<Subscription> {
        if (this.subscriptions == null) {
            this.subscriptions = ArrayList()
        }

        return this.subscriptions!!
    }

    open fun unsubscribe() {
        for (sub in subscriptions()) {
            sub.unsubscribe()
        }

        if (shouldListenEventBus()) {
            EventBus.handler.unregister(this)
        }
    }

    open fun shouldListenEventBus() : Boolean {
        return false
    }

    private fun startListenEventBus() {
        if (this.shouldListenEventBus()) {
            EventBus.handler.register(this)
        }
    }

    fun logout() {
        val userStore = Sprinkles.stores(UserStore::class.java)
        userStore?.reset()

        try {
            doAsync {
                FirebaseInstanceId.getInstance().deleteToken(Config.brazeSenderId, "FCM")
                FirebaseInstanceId.getInstance().deleteInstanceId()
            }
        } catch (e: IOException) {
            LogUtils.LOG("Exception while delete Firebase token")
        }

        redirectPage()
    }

    private fun redirectPage() {
        output().backToLogin()
    }

    protected fun <O> observe(observable: Observable<O>): Observable<O> {
        return observable.observeOn(AndroidSchedulers.mainThread())
    }
}
