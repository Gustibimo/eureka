package com.happyfresh.fulfillment.modules.Tag

import android.os.Bundle
import com.happyfresh.fulfillment.abstracts.InteractorOutput
import java.io.File

/**
 * Created by galuh on 11/24/17.
 */
interface TagInteractorOutput : InteractorOutput {

    fun openCamera(tagFilePath: String?)

    fun submitPhotoTag(tagFilePath: String?, bundle: Bundle?)

    fun showErrorDialog(errorMessage: String)

    fun showTagImage(imageFile: File)

    fun hideCameraIndicator()

    fun enableSubmitButton(isEnabled: Boolean)
}