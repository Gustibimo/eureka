package com.happyfresh.fulfillment.widgets

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.happyfresh.fulfillment.widgets.ViewHolder.OnItemSelection
import com.happyfresh.snowflakes.hoverfly.models.IServiceModel
import java.util.*

/**
 * Created by ifranseda on 03/02/2017.
 */

abstract class BaseAdapter<O : IServiceModel, V : BaseRecyclerViewHolder<*, O>>(internal val context: Context, data: List<O>) :
        RecyclerView.Adapter<V>() {

    internal var data: List<O> = ArrayList()

    private var onItemSelection: OnItemSelection<O>? = null

    init {
        this.data = data
    }

    abstract fun itemLayoutResID(type: Int): Int

    abstract fun provideViewHolder(view: View, type: Int): V

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): V {
        val view = LayoutInflater.from(this.context).inflate(itemLayoutResID(viewType), parent, false)
        return provideViewHolder(view, viewType)
    }

    override fun onBindViewHolder(holder: V, position: Int) {
        holder.bind(this.data[position])
        holder.setOnItemClickListener(this.onItemSelection)
    }

    override fun getItemCount(): Int {
        return this.data.size
    }

    fun setOnItemSelection(listener: OnItemSelection<O>) {
        this.onItemSelection = listener
        notifyDataSetChanged()
    }
}
