package com.happyfresh.fulfillment.modules.Shopper.JobList

import android.os.Bundle
import com.happyfresh.fulfillment.R

import com.happyfresh.fulfillment.abstracts.BaseActivity
import com.happyfresh.fulfillment.abstracts.BasePresenter

/**
 * Created by galuh on 6/10/17.
 */

class ShopperJobListActivity : BaseActivity<ShopperJobListFragment, BasePresenter<*, *, *>>() {

    public override fun title(): String? {
        return getString(R.string.title_job_list)
    }

    public override fun provideFragment(bundle: Bundle?): ShopperJobListFragment? {
        val fragment = ShopperJobListFragment()
        fragment.arguments = bundle
        return fragment
    }

    override fun onStart() {
        super.onStart()
        fragment()?.checkPauseStatus()
    }
}
