package com.happyfresh.fulfillment.modules.Shopper.ShoppingList.outputs

import com.happyfresh.fulfillment.abstracts.InteractorOutput
import com.happyfresh.snowflakes.hoverfly.models.Batch

/**
 * Created by galuh on 9/27/17.
 */

interface ShoppingListInteractorOutput : InteractorOutput {

    fun onBatch(batch: Batch)
}