package com.happyfresh.fulfillment.widgets

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import butterknife.ButterKnife
import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.snowflakes.hoverfly.models.IServiceModel

/**
 * Created by ifranseda on 03/02/2017.
 */

abstract class BaseRecyclerViewHolder<out T : BasePresenter<*, *, *>, O : IServiceModel>(protected var context: Context, itemView: View) :
        RecyclerView.ViewHolder(itemView), ViewHolder<O> {

    protected var onItemSelection: ViewHolder.OnItemSelection<O>? = null

    private var presenter: T? = null

    init {
        ButterKnife.bind(this, itemView)
    }

    fun presenter(): T {
        if (this.presenter == null) {
            this.presenter = providePresenter()

            if (this.presenter == null) {
                throw RuntimeException("Presenter must be initialized first")
            }
        }

        return this.presenter as T
    }

    protected fun providePresenter(): T? {
        return null
    }

    override fun bind(data: O) {
        this.itemView.setOnClickListener {
            this.onItemSelection?.onSelected(data)
        }
    }

    override fun setOnItemClickListener(listener: ViewHolder.OnItemSelection<O>?) {
        this.onItemSelection = listener
    }
}
