package com.happyfresh.fulfillment.modules.Login

import android.content.Context

import com.happyfresh.fulfillment.abstracts.BasePresenter

/**
 * Created by galuh on 6/9/17.
 */

class LoginPresenter(context: Context) : BasePresenter<LoginInteractor, LoginRouter, LoginViewBehavior>(context), LoginInteractorOutput {

    init {
        interactor().context = context
    }

    override fun provideInteractor(): LoginInteractor? {
        return LoginInteractor()
    }

    override fun provideRouter(context: Context): LoginRouter? {
        return LoginRouter(context)
    }

    fun login(username: String, password: String) {
        view()?.showProgress()
        interactor().login(username, password)
    }

    override fun openChooseStore() {
        router().openChooseStore()
        view()?.hideProgress()
    }

    override fun tellLoginNotMatch() {
        view()?.hideProgress()
        view()?.showLoginNotMatch()
    }

    override fun tellLoginFailed() {
        view()?.hideProgress()
        view()?.showLoginFailed()
    }

    override fun tellLoginSuccessful() {
        view()?.hideProgress()
        view()?.showLoginSuccessful()
    }
}
