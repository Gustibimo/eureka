package com.happyfresh.fulfillment.abstracts

import android.content.Context

/**
 * Created by ifranseda on 10/02/2017.
 */
abstract class BasePresenter<out I : BaseInteractor<*>, out R : BaseRouter, out V : ViewBehavior>(val context: Context) : InteractorOutput {

    private var view: V? = null

    private var interactor: I? = null

    private var router: R? = null

    fun setView(view: ViewBehavior) {
        this.view = view as V
    }

    protected abstract fun provideInteractor(): I?

    protected abstract fun provideRouter(context: Context): R?

    protected fun interactor(): I {
        if (this.interactor == null) {
            this.interactor = provideInteractor()
            this.interactor?.setOutput(this)
        }

        return this.interactor as I
    }

    protected fun router(): R {
        if (this.router == null) {
            this.router = provideRouter(this.context)!!
        }

        return this.router as R
    }

    fun view(): V? {
        return this.view
    }

    fun detach() {
        this.interactor?.let {
            it.unsubscribe()
        }
    }

    fun logout() {
        interactor().context = this.context
        interactor().logout()
    }

    override fun backToLogin() {
        (router() as BaseRouter).backToLogin()
    }
}
