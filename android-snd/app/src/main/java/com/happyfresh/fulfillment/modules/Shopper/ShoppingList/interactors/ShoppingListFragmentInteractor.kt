package com.happyfresh.fulfillment.modules.Shopper.ShoppingList.interactors

import com.happyfresh.fulfillment.modules.Shopper.ShoppingList.outputs.ShoppingListFragmentInteractorOutput
import com.happyfresh.snowflakes.hoverfly.models.ProductSampleItem
import com.shopper.events.StartingBatchEvent
import com.shopper.listeners.DataListener
import com.shopper.utils.SampleUtils
import com.squareup.otto.Subscribe

/**
 * Created by ifranseda on 09/10/2017.
 */

class ShoppingListFragmentInteractor : ShoppingListInteractor<ShoppingListFragmentInteractorOutput>() {

    override fun shouldListenEventBus(): Boolean {
        return true
    }

    fun finalizeShopping() {
        if (isShoppingComplete()) {
            if (this.batch?.hasFinalizeUnavailableItems(this.stockLocationId) == false) {
                output().openFinalizeItems(this.batch!!)
            }
            else {
                fetchProductSample()
            }
        }
        else {
            this.batch?.load()
            this.batch?.pendingCounter()?.let { counter ->
                output().showAlertIncompleteLineItem(counter)
            }
        }
    }

    fun toggleStartShoppingButton() {
        output().toggleStartShoppingButton(isShoppingComplete())
    }

    private fun isShoppingComplete(): Boolean {
        this.batch?.load()
        return this.batch?.hasCompleteShoppingList() ?: false
    }

    private fun fetchProductSample() {
        output().showProgress(true)

        SampleUtils().withBatch(this.batch).withStockLocation(stockLocation)
                .fetchProductSample(object : DataListener<Boolean>() {
                    override fun onCompleted(success: Boolean, data: Boolean?) {
                        if (success) {
                            if (ProductSampleItem.isThereInStoreSample(batchId)) {
                                output().openProductSampleScreen(batchId)
                            }
                            else {
                                output().openPaymentScreen(batchId, stockLocationId ?: 0)
                            }
                        }
                        else {
                            output().showAlertUnableCheckProductSample()
                        }

                        output().showProgress(false)
                    }

                    override fun onFailed(exception: Throwable?) {
                        output().openPaymentScreen(batchId, stockLocationId ?: 0)
                        output().showProgress(false)
                    }
                })
    }

    @Subscribe
    fun onStartingBatch(event: StartingBatchEvent) {
        if (this.batch?.remoteId == event.batch.remoteId) {
            batch = event.batch
        }
    }
}