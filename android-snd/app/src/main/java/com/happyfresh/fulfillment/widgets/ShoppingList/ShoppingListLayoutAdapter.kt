package com.happyfresh.fulfillment.widgets.ShoppingList

import android.content.Context
import android.view.View
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.widgets.BaseAdapter
import com.happyfresh.snowflakes.hoverfly.models.LineItem
import com.happyfresh.snowflakes.hoverfly.models.Shipment
import java.util.ArrayList

/**
 * Created by galuh on 10/2/17.
 */

class ShoppingListLayoutAdapter(context: Context, data: List<LineItem>) : BaseAdapter<LineItem, ShoppingListLayoutViewHolder>(context, data) {

    private val shipmentWithShoppingBag = ArrayList<Long>()

    init {
        createShipmentWithShoppingBagList()
    }

    fun addAll(data: List<LineItem>) {
        this.data = data
        createShipmentWithShoppingBagList()
        notifyDataSetChanged()
    }

    private fun createShipmentWithShoppingBagList() {
        if (data.isNotEmpty()) {
            data.forEach { lineItem ->
                lineItem.shipment?.order?.isEligibleForShoppingBag?.let {
                    if (it && !shipmentWithShoppingBag.contains(lineItem.shipment?.remoteId!!)) {
                        shipmentWithShoppingBag.add(lineItem.shipment?.remoteId!!)
                    }
                }
            }
        }
    }

    override fun itemLayoutResID(type: Int): Int {
        return R.layout.widget_shopping_list_item
    }

    override fun provideViewHolder(view: View, type: Int): ShoppingListLayoutViewHolder {
        return ShoppingListLayoutViewHolder(this.context, view)
    }

    override fun onBindViewHolder(holder: ShoppingListLayoutViewHolder, position: Int) {
        if (position >= itemCount - shipmentWithShoppingBag.size) {
            holder.bindShoppingBag(Shipment.findById(shipmentWithShoppingBag[position - super.getItemCount()]))
        } else {
            super.onBindViewHolder(holder, position)

            var previousData: LineItem? = null
            try {
                previousData = this.data[position - 1]
            }
            catch (ie: IndexOutOfBoundsException) {
            }
            finally {
                holder.bindCategory(this.data[position], previousData)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position >= itemCount - shipmentWithShoppingBag.size) {
            ViewType.SHOPPING_BAG
        } else {
            ViewType.LINE_ITEM
        }
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + shipmentWithShoppingBag.size
    }

    class ViewType {
        companion object {
            @JvmField val LINE_ITEM = 0
            @JvmField val SHOPPING_BAG = 1
        }
    }

}