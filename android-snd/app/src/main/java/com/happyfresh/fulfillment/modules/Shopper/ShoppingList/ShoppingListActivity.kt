package com.happyfresh.fulfillment.modules.Shopper.ShoppingList

import android.os.Bundle
import android.support.design.widget.CollapsingToolbarLayout
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import butterknife.BindView
import butterknife.OnClick
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.abstracts.BaseActivity
import com.happyfresh.fulfillment.modules.Shopper.ShoppingList.behaviors.ShoppingListActivityViewBehavior
import com.happyfresh.fulfillment.modules.Shopper.ShoppingList.presenters.ShoppingListActivityPresenter
import com.shopper.utils.ViewUtils
import com.shopper.views.CollapsibleImageView
import com.squareup.picasso.Picasso

/**
 * Created by galuh on 9/27/17.
 */
class ShoppingListActivity : BaseActivity<ShoppingListFragment, ShoppingListActivityPresenter>(),
        ShoppingListActivityViewBehavior {

    @JvmField
    @BindView(R.id.collapsing_toolbar)
    var collapsingToolbarLayout: CollapsingToolbarLayout? = null

    @JvmField
    @BindView(R.id.toolbar_image)
    var storeImage: CollapsibleImageView? = null

    @JvmField
    @BindView(R.id.store_name)
    var storeName: TextView? = null

    @JvmField
    @BindView(R.id.store_address)
    var storeAddress: TextView? = null

    @JvmField
    @BindView(R.id.shop_now_button)
    var shopNowButton: Button? = null

    @JvmField
    @BindView(R.id.chat_button_container)
    var chatButtonContainer: RelativeLayout? = null

    @JvmField
    @BindView(R.id.chat_badge)
    var chatBadge: TextView? = null

    var timerTextView: TextView? = null

    var progressBar: View? = null

    var cancelMenu: MenuItem? = null

    private var hasCancelButton: Boolean = false

    override fun title(): String? {
        return getString(R.string.shopping_list)
    }

    override fun provideFragment(bundle: Bundle?): ShoppingListFragment? {
        return ShoppingListFragment().apply { arguments = bundle }
    }

    override fun providePresenter(): ShoppingListActivityPresenter {
        return ShoppingListActivityPresenter(this)
    }

    override fun layoutResID(): Int {
        return R.layout.activity_shopping_list_new
    }

    override fun menuResID(): Int {
        return R.menu.shipment
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        collapsingToolbarLayout?.setExpandedTitleColor(ContextCompat.getColor(this, android.R.color.transparent))
        collapsingToolbarLayout?.setCollapsedTitleTextColor(ContextCompat.getColor(this, android.R.color.white))

        presenter()?.initialize(this.bundle)
    }

    override fun onResume() {
        presenter()?.addChatHandler()
        super.onResume()
    }

    override fun onPause() {
        presenter()?.removeChatHandler()
        super.onPause()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val container = LayoutInflater.from(this).inflate(R.layout.layout_shipment_timer, null)
        timerTextView = container?.findViewById(R.id.timer) as TextView
        progressBar = container.findViewById(R.id.progress_timer)

        hideTimer()

        menu.add(Menu.NONE, Menu.NONE, Menu.FIRST, R.string.tab_items).setActionView(container)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS)

        val created = super.onCreateOptionsMenu(menu)

        cancelMenu = menu.findItem(R.id.action_cancel)
        cancelMenu?.isVisible = hasCancelButton

        presenter()?.prepareTimer()

        return created
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.action_switch -> {
                switch()
                return true
            }

            R.id.action_cancel -> {
                cancel()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        presenter()?.disconnectChat()

        super.onDestroy()
    }

    override fun showStoreName(name: String) {
        storeName?.text = name
    }

    override fun showStoreImage(image: String) {
        Picasso.with(applicationContext).cancelRequest(storeImage)
        Picasso.with(applicationContext).load(image).placeholder(R.drawable.default_product).into(storeImage)
    }

    override fun showStoreAddress(address: String) {
        storeAddress?.text = address
    }

    override fun showShoppingButton() {
        shopNowButton?.visibility = View.VISIBLE

        hasCancelButton = false
        invalidateOptionsMenu()
    }

    override fun hideShoppingButton() {
        shopNowButton?.visibility = View.GONE

        hasCancelButton = true
        invalidateOptionsMenu()
    }

    override fun showChatButton() {
        chatButtonContainer?.visibility = View.VISIBLE
    }

    override fun hideChatButton() {
        chatButtonContainer?.visibility = View.GONE
    }

    override fun hideCancelMenu() {
        hasCancelButton = false
        cancelMenu?.isVisible = false
    }

    override fun showFailedToStart() {
        Toast.makeText(applicationContext, R.string.failed_to_start_shopping, Toast.LENGTH_SHORT).show()
    }

    override fun pleaseTryAgain() {
        Toast.makeText(applicationContext, R.string.waiting_customer_please_try_again, Toast.LENGTH_SHORT).show()
    }

    override fun setTimer(time: String) {
        timerTextView?.text = time
    }

    override fun showTimer() {
        ViewUtils.setVisibilityViews(View.VISIBLE, timerTextView, progressBar)
    }

    override fun hideTimer() {
        ViewUtils.setVisibilityViews(View.GONE, timerTextView, progressBar)
    }

    override fun showChatBadge(count: Int) {
        if (count > 0) {
            chatBadge?.text = count.toString()
            chatBadge?.visibility = View.VISIBLE
        } else {
            chatBadge?.visibility = View.GONE
        }
    }

    @OnClick(R.id.shop_now_button)
    fun shopNowButtonClicked() {
        presenter()?.initiateStartShopping()
    }

    @OnClick(R.id.chat_button)
    fun chatButtonClicked() {
        presenter()?.prepareOpenShopperChatList()
    }

    fun cancel() {
        presenter()?.initiateCancel()
    }

    fun switch() {
        presenter()?.switchBatch()
    }
}