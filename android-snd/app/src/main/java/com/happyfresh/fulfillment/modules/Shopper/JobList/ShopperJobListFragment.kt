package com.happyfresh.fulfillment.modules.Shopper.JobList

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import butterknife.BindView
import com.afollestad.materialdialogs.MaterialDialog
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.abstracts.BaseFragment
import com.happyfresh.fulfillment.widgets.JobList.JobListLayout
import com.happyfresh.fulfillment.widgets.StockChecker.StockCheckerLayout
import com.happyfresh.snowflakes.hoverfly.models.response.VersionUpdate
import com.shopper.events.StartingBatchEvent
import com.shopper.services.DownloadService
import com.shopper.utils.AppUtils
import com.squareup.otto.Subscribe

/**
 * Created by galuh on 6/9/17.
 */

class ShopperJobListFragment : BaseFragment<ShopperJobListPresenter>(), ShopperJobListViewBehavior {

    @JvmField
    @BindView(R.id.job_list_list_layout)
    var jobListListLayout: JobListLayout? = null

    @JvmField
    @BindView(R.id.stock_checker_layout)
    var stockCheckerLayout: StockCheckerLayout? = null

    @JvmField
    @BindView(R.id.update_layout)
    var updateLayout: LinearLayout? = null

    var updateDialog: MaterialDialog? = null

    private var menu: Menu? = null

    private var pendingHandoverIsNotEmpty = false

    override fun layoutResID(): Int {
        return R.layout.fragment_shopper_job_list
    }

    override fun providePresenter(): ShopperJobListPresenter {
        return ShopperJobListPresenter(context)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter().getPendingHandover()

        setHasOptionsMenu(true)
    }

    override fun isSubscriber(): Boolean {
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)

        this.menu = menu

        inflater!!.inflate(R.menu.shopper_new, menu)

        menu!!.findItem(R.id.action_pending_handover).isVisible = true
        menu.findItem(R.id.action_refresh_job).isVisible = true
        menu.findItem(R.id.action_settings).isVisible = true
        menu.findItem(R.id.action_logout).isVisible = true
        menu.findItem(R.id.action_pause).isVisible = true
        menu.findItem(R.id.action_clockout).isVisible = true

        if (pendingHandoverIsNotEmpty) {
            togglePendingHandoverIcon(pendingHandoverIsNotEmpty)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.action_pending_handover -> {
                presenter().prepareOpenPendingHandover()
                return true
            }
            R.id.action_refresh_job -> {
                reloadBatch()
                return true
            }
            R.id.action_pause       -> {
                askToPause()
                return true
            }
            R.id.action_clockout    -> {
                askToClockOut()
                return true
            }
            R.id.action_settings    -> return true
            else                    -> return super.onOptionsItemSelected(item)
        }
    }

    private fun reloadBatch() {
        jobListListLayout?.fetchData()
    }

    fun checkPauseStatus() {
        presenter().checkPauseStatus()
    }

    private fun askToPause() {
        val pauseDialog = MaterialDialog.Builder(activity).title(R.string.alert_title_warning).content(getString(R.string.ask_to_pause)).negativeText(
                R.string.alert_button_no).positiveText(R.string.alert_button_yes).onPositive { _, _ -> presenter().pause() }

        pauseDialog.show()
    }

    private fun askToClockOut() {
        presenter().askToClockOut()
    }

    override fun showClockOutDialog(storeLocation: String?) {
        val clockoutDialog = MaterialDialog.Builder(activity)
                .title(R.string.alert_title_warning)
                .content(getString(R.string.ask_to_clock_out) + " " + storeLocation)
                .negativeText(R.string.alert_button_no)
                .positiveText(R.string.alert_button_yes).onPositive { _, _ -> presenter().clockOut() }

        clockoutDialog.show()
    }

    override fun showRequireUpdateDialog(data: VersionUpdate) {
        if (!DownloadService.isDownloading) {
            showUpdateLayout()

            val dialog = MaterialDialog.Builder(context)
            dialog.title(R.string.alert_title_update_info)
            dialog.content(R.string.alert_message_update_required)
            dialog.cancelable(false)
            dialog.positiveText(R.string.alert_button_ok)

            dialog.callback(object : MaterialDialog.ButtonCallback() {
                override fun onPositive(dialog: MaterialDialog?) {
                    AppUtils.downloadUpdate(data)
                }
            })

            updateDialog?.dismiss()

            updateDialog = dialog.build()
            updateDialog?.show()
        }
    }

    override fun togglePendingHandoverIcon(isNotEmpty: Boolean) {
        pendingHandoverIsNotEmpty = isNotEmpty

        if (isNotEmpty) {
            menu?.getItem(0)?.icon = ContextCompat.getDrawable(context, R.drawable.hf_icon_driver_with_dot)
        } else {
            menu?.getItem(0)?.icon = ContextCompat.getDrawable(context, R.drawable.hf_icon_driver)
        }
    }

    private fun showUpdateLayout() {
        jobListListLayout?.visibility = View.GONE
        jobListListLayout = null

        stockCheckerLayout?.visibility = View.GONE
        updateLayout?.visibility = View.VISIBLE
    }

    @Subscribe
    fun onStartingBatch(event: StartingBatchEvent) {
        jobListListLayout?.updateStartedBatch(event.batch)
    }
}
