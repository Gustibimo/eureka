package com.happyfresh.fulfillment.modules.LineItem

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.Button
import butterknife.BindView
import butterknife.OnClick
import com.afollestad.materialdialogs.MaterialDialog
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.abstracts.BaseFragment
import com.happyfresh.fulfillment.abstracts.BasePresenter
import com.happyfresh.fulfillment.modules.LineItem.behaviors.LineItemDetailFragmentViewBehavior
import com.happyfresh.fulfillment.modules.LineItem.presenters.LineItemDetailFragmentPresenter
import com.happyfresh.fulfillment.widgets.Dialogs.ItemFoundDialog.view.ItemFoundDialogLayout
import com.happyfresh.fulfillment.widgets.Dialogs.OOSDialog.view.InputStockOutReasonDialogLayout
import com.happyfresh.fulfillment.widgets.Dialogs.OOSDialog.view.OOSDialogLayout
import com.happyfresh.fulfillment.widgets.Dialogs.ValueChangeDialog.ValueChangeListener
import com.happyfresh.fulfillment.widgets.Dialogs.ValueChangeDialog.view.ValueChangeDialogLayout
import com.happyfresh.fulfillment.widgets.LineItem.view.LineItemDetailLayout
import com.happyfresh.fulfillment.widgets.LineItem.view.LineItemLayout
import com.happyfresh.snowflakes.hoverfly.models.FeedbackType
import com.happyfresh.snowflakes.hoverfly.models.LineItem

class LineItemDetailFragment : BaseFragment<LineItemDetailFragmentPresenter>(), LineItemDetailFragmentViewBehavior {

    @JvmField
    @BindView(R.id.line_item)
    var lineItemLayout: LineItemLayout<BasePresenter<*, *, *>>? = null

    @JvmField
    @BindView(R.id.found_button)
    var foundButton: Button? = null

    private var itemFoundDialogLayout: ItemFoundDialogLayout? = null

    private var stockOutDialogLayout: OOSDialogLayout? = null

    private var valueChangeDialogLayout: ValueChangeDialogLayout? = null

    override fun layoutResID(): Int {
        return R.layout.fragment_item_detail_new
    }

    override fun providePresenter(): LineItemDetailFragmentPresenter {
        return LineItemDetailFragmentPresenter(context)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter().initialize(this.bundle)

        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)

        inflater?.inflate(R.menu.item_detail_new, menu)

        presenter().lineItem()?.let { item ->
            // Mismatch
            if (item.isProductMismatch) {
                menu?.findItem(R.id.action_flag_mismatch)?.isVisible = false
                menu?.findItem(R.id.action_unflag_mismatch)?.isVisible = true
            }
            else {
                menu?.findItem(R.id.action_flag_mismatch)?.isVisible = true
                menu?.findItem(R.id.action_unflag_mismatch)?.isVisible = false
            }

            // Flagged
            if (item.totalFlagged > 0) {
                menu?.findItem(R.id.action_unflag_item)?.isVisible = true
                menu?.findItem(R.id.action_flag_item)?.isVisible = false
                menu?.findItem(R.id.action_call_operator)?.isVisible = false
            }
            else if (item.totalReplacedShopper > 0) {
                menu?.findItem(R.id.action_unflag_item)?.isVisible = false
                menu?.findItem(R.id.action_flag_item)?.isVisible = false
                menu?.findItem(R.id.action_price_change)?.isVisible = false
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_unflag_item   -> {
                removeItemFlag()
                return true
            }

            R.id.action_flag_item     -> {
                showOOSFeedbackList()
                return true
            }

            R.id.action_flag_mismatch -> {
                showProductMismatch()
                return true
            }

            R.id.action_unflag_mismatch-> {
                removeProductMismatch()
                return true
            }

            R.id.action_price_change  -> {
                showPriceChangeDialog()
                return true
            }

            else                      -> {
                return super.onOptionsItemSelected(item)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val successful = presenter().handleResult(requestCode, data)
        if (!successful) {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun showLineItem(lineItem: LineItem) {
        lineItemLayout?.lineItem = lineItem

        foundButton?.isEnabled = lineItem.totalFlagged == 0

        setDialogs(lineItem)

        invalidateOptionMenu()
    }

    override fun setStockOutFeedbackList(feedbackList: List<FeedbackType>) {
        stockOutDialogLayout = OOSDialogLayout(context)
        stockOutDialogLayout?.setFeedbackList(feedbackList)
    }

    override fun invalidateOptionMenu() {
        activity.invalidateOptionsMenu()
    }

    override fun closeFragment() {
        activity.finish()
    }

    private fun setDialogs(lineItem: LineItem) {
        setItemFoundDialog(lineItem)

        setValueChangeDialog(lineItem)
    }

    private fun setItemFoundDialog(lineItem: LineItem) {
        itemFoundDialogLayout = ItemFoundDialogLayout(context).apply { this.lineItem = lineItem }
    }

    private fun setValueChangeDialog(lineItem: LineItem) {
        valueChangeDialogLayout = ValueChangeDialogLayout(context).apply { this.lineItem = lineItem }
    }

    // Found Item

    @OnClick(R.id.found_button)
    internal fun foundButtonTapped() {
        itemFoundDialogLayout?.show(object : ItemFoundDialogLayout.FoundItemCallback {
            override fun found(quantity: Int, actualWeight: Double?, shopperNotesFulfilled: Boolean) {
                // Should disable found button, but should put somewhere to re-enable it
                //                enableNavigation(false)

                presenter().itemFound(quantity, actualWeight, shopperNotesFulfilled)
            }
        })
    }

    // Stock Out

    private fun removeItemFlag() {
        val dialogBuilder = MaterialDialog.Builder(activity)
        dialogBuilder.title(R.string.alert_title_unflag_item)
        dialogBuilder.content(R.string.alert_message_unflag_item)

        dialogBuilder.negativeText(R.string.alert_button_cancel)
        dialogBuilder.positiveText(R.string.alert_button_yes)

        dialogBuilder.onPositive { _, _ -> presenter().removeFlagItem() }

        dialogBuilder.show()
    }

    private fun showOOSFeedbackList() {
        stockOutDialogLayout?.show(-1, MaterialDialog.ListCallbackSingleChoice { _, _, position, _ ->
            if (position > -1) {
                stockOutDialogLayout?.feedbackTypeList?.get(position)?.let {
                    if (FeedbackType.Type.WEIGHT_DISCREPANCY == it.feedbackType) {
                        showWeightDifferenceDialog(it.typeId)
                    }
                    else {
                        if (it.needDetail) {
                            it.typeId?.let { typeId ->
                                InputStockOutReasonDialogLayout(context).showWithType(typeId,
                                                                                      object : InputStockOutReasonDialogLayout.ReasonCallback {
                                                                                          override fun onReasonDefined(reason: String) {
                                                                                              flagItem(typeId, reason)
                                                                                          }
                                                                                      })
                            }
                        }
                        else {
                            flagItem(it.typeId, null)
                        }
                    }
                }
            }

            true
        })
    }

    // Weight Diff

    private fun showWeightDifferenceDialog(typeId: Int?) {
        typeId?.let {
            valueChangeDialogLayout?.showWeightDifference(object : ValueChangeListener {
                override fun onValueChange(newValue: String) {
                    presenter().initiatePhotoForWeightDiff(typeId, newValue)
                }
            })
        }
    }

    // Product Mismatch

    private fun removeProductMismatch() {
        val dialogBuilder = MaterialDialog.Builder(activity)
        dialogBuilder.title(R.string.action_unflag_mismatch)
        dialogBuilder.content(R.string.alert_message_unflag_item)

        dialogBuilder.negativeText(R.string.alert_button_cancel)
        dialogBuilder.positiveText(R.string.alert_button_yes)

        dialogBuilder.onPositive { _, _ -> presenter().removeProductMismatch() }

        dialogBuilder.show()
    }

    private fun showProductMismatch() {
        presenter().initiatePhotoForProductMismatch()
    }

    // Price Change

    private fun showPriceChangeDialog() {
        val lineItemDetailLayout = lineItemLayout as LineItemDetailLayout

        if (lineItemDetailLayout.canEditPriceStatus) {
            valueChangeDialogLayout?.showPriceChange(object : ValueChangeListener {
                override fun onValueChange(newValue: String) {
                    presenter().initiatePhotoForPriceChange(newValue)
                }
            })
        }
        else {
            lineItemDetailLayout.showCannotEditPrice()
        }
    }

    private fun flagItem(typeId: Int?, detail: String?) {
        presenter().flagItem(typeId, detail)
    }

    // Others

    private fun enableNavigation(enabled: Boolean) {
        foundButton?.isEnabled = enabled
    }
}