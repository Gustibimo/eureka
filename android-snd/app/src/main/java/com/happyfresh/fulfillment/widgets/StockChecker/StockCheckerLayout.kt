package com.happyfresh.fulfillment.widgets.StockChecker

import android.content.Context
import android.util.AttributeSet
import butterknife.OnClick
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.widgets.BaseLayout

/**
 * Created by Novanto on 12/27/17.
 */

class StockCheckerLayout @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
        BaseLayout<StockCheckerPresenter>(context, attrs, defStyleAttr), StockCheckerViewBehavior {
    override fun providePresenter(): StockCheckerPresenter? {
        return StockCheckerPresenter(context)
    }

    override fun layoutResID(): Int {
        return R.layout.widget_stock_checker
    }

    @OnClick(R.id.button_start)
    fun onStartButtonClick() {
        presenter().prepareOpenStockChecker()
    }
}