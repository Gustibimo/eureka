package com.happyfresh.fulfillment.modules.Tag.Mismatch

import android.content.Context
import android.os.Bundle
import com.happyfresh.fulfillment.modules.Tag.TagPresenter
import com.happyfresh.fulfillment.modules.Tag.TagViewBehavior
import com.happyfresh.snowflakes.hoverfly.config.Constant

/**
 * Created by ifranseda on 29/12/2017.
 */
class MismatchTagPresenter(context: Context) : TagPresenter<MismatchTagInteractor, MismatchTagRouter, TagViewBehavior>(context) {

    init {
        interactor().context = context
    }

    override fun provideInteractor(): MismatchTagInteractor? {
        return MismatchTagInteractor()
    }

    override fun provideRouter(context: Context): MismatchTagRouter? {
        return MismatchTagRouter(context)
    }

    fun initialize(extras: Bundle?) {
        interactor().initialize(extras)
    }
}