package com.happyfresh.fulfillment.modules.Tag.Mismatch

import android.os.Bundle
import android.view.View
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.modules.Tag.Price.PriceTagPresenter
import com.happyfresh.fulfillment.modules.Tag.TagFragment

/**
 * Created by ifranseda on 29/12/2017.
 */
class MismatchTagFragment : TagFragment<PriceTagPresenter>() {
    override var warningContentRes = R.string.discard

    override fun providePresenter(): PriceTagPresenter {
        return PriceTagPresenter(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        presenter().initialize(this.bundle)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        captureText?.text = getString(R.string.take_product_image)
    }
}