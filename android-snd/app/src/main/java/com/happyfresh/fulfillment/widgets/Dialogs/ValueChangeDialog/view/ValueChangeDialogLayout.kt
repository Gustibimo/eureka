package com.happyfresh.fulfillment.widgets.Dialogs.ValueChangeDialog.view

import android.app.Activity
import android.content.Context
import android.graphics.Paint
import android.text.TextUtils
import android.util.AttributeSet
import android.view.View
import android.widget.EditText
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.afollestad.materialdialogs.MaterialDialog
import com.happyfresh.fulfillment.R
import com.happyfresh.fulfillment.widgets.BaseLayout
import com.happyfresh.fulfillment.widgets.Dialogs.ItemFoundDialog.ValueChangeDialogPresenter
import com.happyfresh.fulfillment.widgets.Dialogs.ItemFoundDialog.ValueChangeDialogViewBehavior
import com.happyfresh.fulfillment.widgets.Dialogs.ValueChangeDialog.Value
import com.happyfresh.fulfillment.widgets.Dialogs.ValueChangeDialog.ValueChangeListener
import com.happyfresh.snowflakes.hoverfly.models.LineItem
import com.shopper.utils.ViewUtils

/**
 * Created by galuh on 11/8/17.
 */
class ValueChangeDialogLayout @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
        BaseLayout<ValueChangeDialogPresenter>(context, attrs, defStyleAttr), ValueChangeDialogViewBehavior {

    @JvmField
    @BindView(R.id.item_new_value_text)
    var valueChangeText: TextView? = null

    @JvmField
    @BindView(R.id.item_new_value_input)
    var valueChangeField: ValueChangeInputFieldLayout? = null

    private var valueInputField = ButterKnife.findById<EditText>(valueChangeField!!, R.id.new_value_input_field)

    @JvmField
    @BindView(R.id.item_reenter_new_value_text)
    var valueChangeReenterText: TextView? = null

    @JvmField
    @BindView(R.id.item_reenter_new_value_input)
    var valueChangeReenterField: ValueChangeInputFieldLayout? = null

    private var valueInputReenterField = ButterKnife.findById<EditText>(valueChangeReenterField!!, R.id.new_value_input_field)

    @JvmField
    @BindView(R.id.value_change_not_match)
    var valueNotMatchTextView: TextView? = null

    private var currency: String? = null
    private var unit: String? = null

    var lineItem: LineItem? = null
        set(value) {
            field = value

            currency = field?.shipment?.order?.currency
            unit = when (field?.naturalAverageWeight != null) {
                true  -> field?.supermarketUnit
                false -> field?.displayUnit
            }
        }

    override fun layoutResID(): Int {
        return R.layout.dialog_value_change
    }

    override fun providePresenter(): ValueChangeDialogPresenter {
        return ValueChangeDialogPresenter(this.context)
    }

    fun showPriceChange(callback: ValueChangeListener) {
        setGeneralAttribute(context.getString(R.string.dialog_subtitle_enter_new_price),
                            context.getString(R.string.dialog_subtitle_reenter_new_price),
                            context.getString(R.string.price_change_not_match))

        setPriceChangeDialogAttribute(valueChangeField, valueInputField)
        setPriceChangeDialogAttribute(valueChangeReenterField, valueInputReenterField)

        builder(callback).show()
    }

    fun showWeightDifference(callback: ValueChangeListener) {
        setGeneralAttribute(context.getString(R.string.dialog_subtitle_enter_actual_weight),
                            context.getString(R.string.dialog_subtitle_reenter_actual_weight),
                            context.getString(R.string.weight_not_match))

        setWeightDifferenceDialogAttribute(valueChangeField, valueInputField)
        setWeightDifferenceDialogAttribute(valueChangeReenterField, valueInputReenterField)

        builder(callback).show()
    }

    private fun builder(callback: ValueChangeListener?) : MaterialDialog.Builder {
        val builder = MaterialDialog.Builder(context as Activity)

        builder.customView(this, false)
        builder.cancelable(false)
        builder.negativeText(R.string.alert_button_cancel)
        builder.positiveText(R.string.alert_button_ok)
        builder.autoDismiss(false)

        builder.onPositive { dialog, _ ->
            val newValue = valueInputField?.text.toString()
            val newRevalue = valueInputReenterField?.text.toString()

            if (newValue.isNotEmpty() && newRevalue.isNotEmpty() && newValue == newRevalue) {
                callback?.onValueChange(newValue)

                dialog.dismiss()
            }
            else {
                valueNotMatchTextView?.visibility = View.VISIBLE
            }
        }

        builder.onNegative { materialDialog, _ ->
            materialDialog.dismiss()
        }

        builder.showListener {
            valueInputField?.text = null
            valueInputReenterField?.text = null
            valueNotMatchTextView?.visibility = View.GONE
        }

        return builder
    }

    private fun setGeneralAttribute(valueText: String, valueReenterText: String, valueNotMatch: String) {
        valueChangeText?.text = valueText
        valueChangeReenterText?.text = valueReenterText
        valueNotMatchTextView?.text = valueNotMatch
    }

    private fun setPriceChangeDialogAttribute(priceChangeField: View?, priceInput: EditText?) {
        val attributeSidePadding = 15
        var unitWidth = 0

        val priceCurrencyTextView = ButterKnife.findById<TextView>(priceChangeField!!, R.id.value_currency)
        priceCurrencyTextView.text = currency
        priceCurrencyTextView.visibility = View.VISIBLE

        if (!TextUtils.isEmpty(unit)) {
            val priceUnitTextView = ButterKnife.findById<TextView>(priceChangeField, R.id.value_unit)
            priceUnitTextView.text = "/" + unit
            unitWidth = ViewUtils.dpToPx((Paint().measureText(unit) + attributeSidePadding).toDouble())
        }

        val currencyWidth = ViewUtils.dpToPx((Paint().measureText(currency) + attributeSidePadding).toDouble())

        priceInput?.setPadding(currencyWidth, priceInput.paddingTop, unitWidth, priceInput.paddingBottom)
    }

    private fun setWeightDifferenceDialogAttribute(weightDifferenceField: View?, weightInput: EditText?) {
        var unitWidth = 0

        val valueCurrencyTextView = ButterKnife.findById<TextView>(weightDifferenceField!!, R.id.value_currency)
        valueCurrencyTextView.visibility = View.GONE

        if (!TextUtils.isEmpty(unit)) {
            val priceUnitTextView = weightDifferenceField.findViewById(R.id.value_unit) as TextView
            priceUnitTextView.text = unit
            unitWidth = ViewUtils.dpToPx(Paint().measureText(unit).toDouble())
        }

        weightInput?.setPadding(0, weightInput.paddingTop, unitWidth, weightInput.paddingBottom)
    }
}