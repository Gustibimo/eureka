package com.happyfresh.fulfillment.widgets.PendingHandoverList

import android.widget.Toast
import com.happyfresh.fulfillment.abstracts.BaseInteractor
import com.happyfresh.snowflakes.hoverfly.models.PendingHandover
import com.happyfresh.snowflakes.hoverfly.models.response.GrabExpressCancelReasonResponse
import com.happyfresh.snowflakes.hoverfly.models.response.PendingHandoverListResponse
import com.happyfresh.snowflakes.hoverfly.services.BatchService
import com.happyfresh.snowflakes.hoverfly.services.ShipmentService
import com.happyfresh.snowflakes.hoverfly.shared.events.EventBus
import com.happyfresh.snowflakes.hoverfly.shared.events.TogglePendingHandoverIconEvent
import com.happyfresh.snowflakes.hoverfly.shared.subscribers.ServiceSubscriber
import retrofit.client.Response

/**
 * Created by kharda on 28/06/18.
 */

class PendingHandoverListLayoutInteractor : BaseInteractor<PendingHandoverListLayoutInteractorOutput>() {

    private val batchService by lazy { BatchService() }

    private val shipmentService by lazy { ShipmentService() }

    var colors: String? = null

    fun fetchPendingHandover() {
        output().showLoading(true)

        val observable = batchService.pendingHandoverList()
        val subscriber = object : ServiceSubscriber<PendingHandoverListResponse>() {
            override fun onFailure(e: Throwable) {
                e.printStackTrace()

                output().showLoading(false)
                output().unableLoadData()
            }

            override fun onSuccess(data: PendingHandoverListResponse) {
                output().showLoading(false)
                prepareShowPendingHandover(data.pendingHandovers)
                EventBus.handler.post(TogglePendingHandoverIconEvent(data.pendingHandovers.isNotEmpty()))
            }
        }

        subscriptions().add(observable.subscribe(subscriber)!!)
    }

    fun prepareShowPendingHandover(pendingHandoverList: List<PendingHandover>) {
        colors = null

        for (pendingHandover in pendingHandoverList) {
            setOrderIndicatorColor(pendingHandover)
        }

        if (pendingHandoverList.isEmpty()) {
            output().showEmpty()
        } else {
            output().showData(pendingHandoverList)
        }
    }

    fun fetchCancelBookingReason(orderNumber: String) {
        val observable = shipmentService.fetchCancelBookingReason()
        val subscriber = object : ServiceSubscriber<GrabExpressCancelReasonResponse>() {
            override fun onFailure(e: Throwable) {
                e.printStackTrace()
                Toast.makeText(context, "error: ${e.message}", Toast.LENGTH_SHORT).show()
            }

            override fun onSuccess(data: GrabExpressCancelReasonResponse) {
                output().showCancelBookingDialog(orderNumber, data.cancelReasons)
            }

        }

        subscriptions().add(observable.subscribe(subscriber)!!)
    }

    fun cancelBooking(orderNumber: String, cancelReason: Int) {
        val observable = shipmentService.cancelGrabExpressOrder(orderNumber, cancelReason)
        val subscriber = object : ServiceSubscriber<Response?>() {
            override fun onFailure(e: Throwable) {
                e.printStackTrace()
                Toast.makeText(context, "error: ${e.message}", Toast.LENGTH_SHORT).show()
            }

            override fun onSuccess(data: Response?) {
                fetchPendingHandover()
            }

        }

        subscriptions().add(observable.subscribe(subscriber)!!)
    }

    fun createBooking(orderNumber: String) {
        val observable = shipmentService.orderGrabExpress(orderNumber)
        val subscriber = object : ServiceSubscriber<Response>() {
            override fun onFailure(e: Throwable) {
                e.printStackTrace()
                Toast.makeText(context, "error: ${e.message}", Toast.LENGTH_SHORT).show()
            }

            override fun onSuccess(data: Response) {
                fetchPendingHandover()
            }

        }

        subscriptions().add(observable.subscribe(subscriber)!!)
    }

    fun changeBookingToHF(orderNumber: String) {
        val observable = shipmentService.changeGrabExpressOrderToHF(orderNumber)
        val subscriber = object : ServiceSubscriber<Response?>() {
            override fun onFailure(e: Throwable) {
                e.printStackTrace()
                Toast.makeText(context, "error: ${e.message}", Toast.LENGTH_SHORT).show()
            }

            override fun onSuccess(data: Response?) {
                fetchPendingHandover()
            }
        }

        subscriptions().add(observable.subscribe(subscriber)!!)
    }

    private fun setOrderIndicatorColor(data: PendingHandover) {
        if (data.indicatorColor == null) {
            data.indicatorColor = nextColor
        }

        appendColor(data.indicatorColor!!)
    }

    private val nextColor: Int
        get() {
            val shipmentColors = context?.resources?.getIntArray(com.happyfresh.snowflakes.hoverfly.R.array.shipment_colors)

            return shipmentColors?.firstOrNull { !hasColor(it) }
                    ?: com.happyfresh.snowflakes.hoverfly.R.color.shopping_order_unknown
        }

    private fun hasColor(color: Int): Boolean {
        return colors?.contains(color.toString()) == true
    }

    private fun appendColor(color: Int) {
        colors = if (colors.isNullOrEmpty()) {
            color.toString()
        } else {
            val builder = StringBuilder()
            builder.append(colors)
            builder.append(",")
            builder.append(color.toString())
            builder.toString()
        }
    }
}