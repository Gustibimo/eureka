ALTER TABLE `Order` add COLUMN `is_chat_enabled` BOOLEAN;
ALTER TABLE `Order` add COLUMN `chat_channelUrl` TEXT;
ALTER TABLE `Order` add COLUMN `user_first_name` TEXT;
ALTER TABLE `Order` add COLUMN `user_last_name` TEXT;
ALTER TABLE `Order` add COLUMN `user_full_name` TEXT;