ALTER TABLE `line_items` add COLUMN `display_actual_weight` TEXT;
ALTER TABLE `line_items` add COLUMN `actual_weight_adjustment_total` REAL;
ALTER TABLE `variants` add COLUMN `display_actual_weight` TEXT;
ALTER TABLE `variants` add COLUMN `actual_weight_adjustment_total` REAL;