ALTER TABLE `line_items` add COLUMN `display_supermarket_unit_cost_price` TEXT;
ALTER TABLE `line_items` add COLUMN `supermarket_unit_cost_price` DOUBLE;
ALTER TABLE `variants` add COLUMN `display_supermarket_unit_cost_price` TEXT;
ALTER TABLE `variants` add COLUMN `supermarket_unit_cost_price` DOUBLE;
