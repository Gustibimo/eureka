ALTER TABLE `line_items` add COLUMN `unit` TEXT;
ALTER TABLE `line_items` add COLUMN `display_unit` TEXT;
ALTER TABLE `line_items` add COLUMN `display_average_weight` TEXT;