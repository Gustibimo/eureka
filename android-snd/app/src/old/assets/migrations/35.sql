ALTER TABLE `line_items` add COLUMN `supermarket_unit` TEXT;
ALTER TABLE `line_items` add COLUMN `natural_average_weight` REAL;
ALTER TABLE `line_items` add COLUMN `actual_weight` REAL;

ALTER TABLE `shipments` add COLUMN `cancelled` INTEGER;
