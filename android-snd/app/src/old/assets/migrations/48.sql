ALTER TABLE `addresses` add COLUMN `address_detail` TEXT;
ALTER TABLE `addresses` add COLUMN `address_number` TEXT;
ALTER TABLE `addresses` add COLUMN `lat` REAL;
ALTER TABLE `addresses` add COLUMN `lon` REAL;
