ALTER TABLE `variants` add COLUMN `display_unit` TEXT;
ALTER TABLE `variants` add COLUMN `display_average_weight` TEXT;
ALTER TABLE `variants` add COLUMN `display_cost_price` TEXT;