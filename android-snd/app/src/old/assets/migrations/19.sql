ALTER TABLE `line_items` add COLUMN `total_replaced_shopper` INTEGER;
ALTER TABLE `line_items` add COLUMN `total_replaced_customer` INTEGER;
ALTER TABLE `replacement` add COLUMN `user_id` TEXT;
ALTER TABLE `replacement` add COLUMN `display_price` TEXT;
ALTER TABLE `replacement` add COLUMN `display_cost_price` TEXT;
ALTER TABLE `replacement` add COLUMN `line_item_id` REAL;
ALTER TABLE `price_amendment` add COLUMN `remote_id` REAL;
ALTER TABLE `price_amendment` add COLUMN `line_item_id` REAL;
ALTER TABLE `price_amendment` add COLUMN `replacement_id` REAL;
ALTER TABLE `price_amendment` add COLUMN `new_price` REAL;
ALTER TABLE `price_amendment` add COLUMN `user_id` REAL;
ALTER TABLE `price_amendment` add COLUMN `line_item` INTEGER;