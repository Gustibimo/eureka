package com.shopper.components;

/**
 * Created by rsavianto on 11/28/14.
 */

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;

import com.happyfresh.fulfillment.R;

/**
 * Simplest custom view possible, using CircularProgressDrawable
 */
public class CircularProgressBar extends View {

    private CircularProgressDrawable mDrawable;

    public CircularProgressBar(Context context) {
        this(context, null);
    }

    public CircularProgressBar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CircularProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        int circleColor = getResources().getColor(android.R.color.white);
        int borderWidth = 4;

        if (attrs != null) {
            TypedArray typedArray = getContext().getTheme().obtainStyledAttributes(attrs, R.styleable.com_shopper_customs_CircularProgressBar, 0, 0);
            try {
                circleColor = typedArray.getColor(R.styleable.com_shopper_customs_CircularProgressBar_circle_color, circleColor);
//                borderWidth = typedArray.getDimensionPixelSize(R.styleable.com_shopper_customs_CircularProgressBar_border_width, borderWidth);
            } finally {
                typedArray.recycle();
            }

            assert (borderWidth > 0);
        }

        mDrawable = new CircularProgressDrawable(circleColor, borderWidth);
        mDrawable.setCallback(this);
    }

    @Override
    protected void onVisibilityChanged(View changedView, int visibility) {
        super.onVisibilityChanged(changedView, visibility);

        if (mDrawable == null) {
            return;
        }

        if (visibility == VISIBLE) {
            mDrawable.start();
        } else {
            mDrawable.stop();
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mDrawable.setBounds(0, 0, w, h);
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        mDrawable.draw(canvas);
    }

    @Override
    protected boolean verifyDrawable(Drawable who) {
        return who == mDrawable || super.verifyDrawable(who);
    }
}