package com.shopper.adapters;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.Batch;
import com.happyfresh.snowflakes.hoverfly.models.Job;
import com.happyfresh.snowflakes.hoverfly.models.Shipment;
import com.happyfresh.snowflakes.hoverfly.models.Slot;
import com.happyfresh.snowflakes.hoverfly.models.User;
import com.happyfresh.snowflakes.hoverfly.utils.ResourceUtils;
import com.shopper.common.ShopperApplication;
import com.shopper.utils.StringUtils;
import com.shopper.utils.ViewUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by ifranseda on 7/8/15.
 */

public class DriverListAdapter extends RecyclerView.Adapter<DriverListAdapter.ViewHolder> {
    private static final SimpleDateFormat sDateFormat = new SimpleDateFormat("h:mm a");
    private Context mContext;
    private OnStartListener mOnStartListener;
    private List<Batch> mBatchList = new ArrayList<>();
    private boolean mIsLoading = false;

    public DriverListAdapter(Context ctx, List<Batch> batchList) {
        mContext = ctx;
        mBatchList = batchList;

        sDateFormat.setTimeZone(TimeZone.getTimeZone("gmt"));
    }

    public void setOnStartClickListener(OnStartListener startListener) {
        mOnStartListener = startListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_driver_item, parent, false);
        return new ViewHolder(mContext, view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        final Batch batch = mBatchList.get(position);
        if (batch == null)
            return;

        viewHolder.bookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnStartListener != null) {
                    mOnStartListener.onClick(position);
                }
            }
        });

        viewHolder.viewAllOrderLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewHolder.showListOrder();
            }
        });

        viewHolder.setData(batch);
        viewHolder.setListOrder(batch);
        viewHolder.showLoading(mIsLoading);

        if (viewHolder.parentItemOrderListLayout.getVisibility() == View.VISIBLE) {
            viewHolder.arrowIndicator.setImageResource(R.drawable.icon_dropup);
            viewHolder.parentItemOrderListLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mBatchList.size();
    }

    public void setLoading(boolean loading) {
        mIsLoading = loading;
    }

    public interface OnStartListener {
        void onClick(int position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public Button bookButton;

        public View corporateFlag;

        public ImageView arrowIndicator;
        public TextView viewCloseAllOrder;

        public TextView storeLocation;
        public TextView orderSize;
        public TextView orderDeliveryTime;
        public TextView batchId;
        public TextView orderNumber;
        public TextView happyCorporateFlag;
        public TextView shopperName;
        public TextView shopperNameLabel;
        public TextView shoppingStatus;
        public TextView shoppingStatusLabel;
        public TextView shoppingEstimationTimeLabel;
        public TextView shoppingEstimationTime;
        public TextView shoppingBagFlag;

        GradientDrawable shoppingStatusBackground;

        public String[] shoppingStatusText;
        public int[] shoppingStatusTextColors;
        public int[] shoppingStatusBorderColors;
        public int[] shoppingStatusBackgroundColors;

        public RelativeLayout viewAllOrderLayout;
        public View itemView;
        public ViewGroup parentItemOrderListLayout;

        public LinearLayout shipDistanceLayout;
        public TextView orderShipDistance;

        private Context mContext;
        private User mCurrentUser;

        public ViewHolder(Context ctx, View view) {
            super(view);

            bookButton = (Button) view.findViewById(R.id.book_button);

            corporateFlag = view.findViewById(R.id.happycorporate_layout);

            arrowIndicator = (ImageView) view.findViewById(R.id.arrow_indicator);
            viewCloseAllOrder = (TextView) view.findViewById(R.id.textview_toggle_all_order);
            parentItemOrderListLayout = (ViewGroup) view.findViewById(R.id.parent_list_order_number_layout_item);
            viewAllOrderLayout = (RelativeLayout) view.findViewById(R.id.view_all_orders_layout);
            itemView = LayoutInflater.from(ctx).inflate(R.layout.layout_order_item, parentItemOrderListLayout, false);

            storeLocation = (TextView) view.findViewById(R.id.store_location_name);
            orderSize = (TextView) view.findViewById(R.id.order_size);
            orderDeliveryTime = (TextView) view.findViewById(R.id.order_delivery_time);
            batchId = (TextView) view.findViewById(R.id.batch_id);

            shipDistanceLayout = (LinearLayout) view.findViewById(R.id.ship_distance_layout);
            orderShipDistance = (TextView) view.findViewById(R.id.order_ship_distance);

            shoppingStatusText = ctx.getResources().getStringArray(R.array.shopping_status);
            shoppingStatusTextColors = ctx.getResources().getIntArray(R.array.shopping_status_text);
            shoppingStatusBorderColors = ctx.getResources().getIntArray(R.array.shopping_status_border);
            shoppingStatusBackgroundColors = ctx.getResources().getIntArray(R.array.shopping_status_background);


            mContext = ctx;
            mCurrentUser = ShopperApplication.getInstance().getCurrentUser();
        }

        public void setData(Batch batch) {
            Slot slot = batch.getSlot();

            storeLocation.setText(slot.getStockLocation().getName());
            orderSize.setText(String.valueOf(batch.shipments().size()));
            orderDeliveryTime.setText(StringUtils.showDeliveryTime(slot.getStartTime(), slot.getEndTime()));
            batchId.setText(String.valueOf(batch.getRemoteId()));

            if (batch.hasCorporateOrder()) {
                corporateFlag.setVisibility(View.VISIBLE);
            }
            else {
                corporateFlag.setVisibility(View.GONE);
            }
        }

        private void setListOrder(Batch batch) {
            if (itemView.getParent() != null) {
                ((ViewGroup) itemView.getParent()).removeAllViews();
                itemView = null;
            }

            int x = 1;
            for (Shipment shipment : batch.shipments()) {
                itemView = LayoutInflater.from(mContext).inflate(R.layout.layout_order_item, parentItemOrderListLayout, false);
                orderNumber = (TextView) itemView.findViewById(R.id.item_order_number);
                happyCorporateFlag = (TextView) itemView.findViewById(R.id.corporate_flag);

                shopperNameLabel = (TextView) itemView.findViewById(R.id.label_shopper_name);
                shopperName = (TextView) itemView.findViewById(R.id.shopper_name);

                shoppingStatus = (TextView) itemView.findViewById(R.id.shopping_status);
                shoppingStatusBackground = (GradientDrawable) shoppingStatus.getBackground();
                shoppingStatusLabel = (TextView) itemView.findViewById(R.id.label_shopping_status);

                shoppingEstimationTimeLabel = (TextView) itemView.findViewById(R.id.estimation_time_label);
                shoppingEstimationTime = (TextView) itemView.findViewById(R.id.estimation_time);
                shoppingBagFlag = (TextView) itemView.findViewById(R.id.textview_shopping_bag_flag);

                orderNumber.setText(shipment.getOrder().getNumber());

                // only shown in driver app
                shopperNameLabel.setVisibility(View.VISIBLE);
                shopperName.setVisibility(View.VISIBLE);
                shoppingStatusLabel.setVisibility(View.VISIBLE);
                shoppingStatus.setVisibility(View.VISIBLE);
                shoppingEstimationTimeLabel.setVisibility(View.VISIBLE);
                shoppingEstimationTime.setVisibility(View.VISIBLE);

                if (batch.isRangerHasToShop()) {
                    shopperNameLabel.setVisibility(View.GONE);
                    shopperName.setVisibility(View.GONE);
                    shoppingStatusLabel.setVisibility(View.GONE);
                    shoppingStatus.setVisibility(View.GONE);
                    shoppingEstimationTimeLabel.setVisibility(View.GONE);
                    shoppingEstimationTime.setVisibility(View.GONE);
                }

                if (shipment.getOrder().getCompanyId() != 0) {
                    happyCorporateFlag.setVisibility(View.VISIBLE);
                }
                else {
                    happyCorporateFlag.setVisibility(View.GONE);
                }

                if (shipment.getOrder().isEligibleForShoppingBag()){
                    shoppingBagFlag.setVisibility(View.VISIBLE);
                }
                else{
                    shoppingBagFlag.setVisibility(View.GONE);
                }


                final Job shoppingJob = shipment.getShoppingJob();
                if (shoppingJob != null && shoppingJob.getUser() != null) {
                    shopperName.setText(shoppingJob.getUser().getEmail());

                    shoppingEstimationTime.setText(shipment.getEstimatedTime() + " " + mContext.getResources().getString(R.string.minute));

                    changeShoppingStatus(shipment.getShoppingStatus());
                } else {
                    shopperName.setText("-");

                    shoppingEstimationTimeLabel.setText("-");
                    shoppingEstimationTime.setText("");

                    changeShoppingStatus("");
                }

                if (x % 2 == 0) {
                    itemView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.shady_background));
                }
                x++;

                parentItemOrderListLayout.addView(itemView);
            }

            String shipDistance = batch.getDisplayShipDistance();
            int visibility = org.apache.commons.lang3.StringUtils.isEmpty(shipDistance) ? View.GONE : View.VISIBLE;

            shipDistanceLayout.setVisibility(visibility);
            orderShipDistance.setText(shipDistance);
        }

        public void showLoading(boolean loading) {
            if (loading) {
                bookButton.setText(ResourceUtils.getAttributedString(mContext, R.attr.startingOrBooking));
                bookButton.setEnabled(false);
            } else {
                bookButton.setText(ResourceUtils.getAttributedString(mContext, R.attr.startOrBook));
                bookButton.setEnabled(true);
            }

            bookButton.setTextColor(ContextCompat.getColor(mContext, R.color.shopper_button_background));
        }

        public void showListOrder() {
            if (parentItemOrderListLayout.getVisibility() == View.GONE) {
                arrowIndicator.setImageResource(R.drawable.icon_dropup);
                viewCloseAllOrder.setText(mContext.getString(R.string.label_close_all_orders));
                parentItemOrderListLayout.setVisibility(View.VISIBLE);
            } else {
                arrowIndicator.setImageResource(R.drawable.icon_dropdown);
                viewCloseAllOrder.setText(mContext.getString(R.string.label_view_all_orders));
                parentItemOrderListLayout.setVisibility(View.GONE);
            }
        }

        public void changeShoppingStatus(String status) {
            int index;

            if (TextUtils.isEmpty(status)) {
                index = 0;
            } else {
                switch (status) {
                    case "in_progress":
                        index = 1;
                        break;
                    case "last_item_picked":
                        index = 2;
                        break;
                    case "finished":
                        index = 3;
                        break;
                    default:
                        index = 0;
                        break;
                }
            }

            shoppingStatus.setText(shoppingStatusText[index]);
            shoppingStatus.setTextColor(shoppingStatusTextColors[index]);
            shoppingStatusBackground.setColor(shoppingStatusBackgroundColors[index]);
            shoppingStatusBackground.setStroke(ViewUtils.dpToPx(1), shoppingStatusBorderColors[index]);
        }
    }
}

