package com.shopper.adapters;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;

import com.happyfresh.snowflakes.hoverfly.models.StockLocation;
import com.shopper.fragments.ProductSampleListFragment;

import java.util.List;

/**
 * Created by kharda on 9/7/16.
 */
public class ProductSamplePagerAdapter extends FragmentPagerAdapter {
    private List<StockLocation> mStockLocations;

    public ProductSamplePagerAdapter(FragmentManager fm, List<StockLocation> stores) {
        super(fm);
        mStockLocations = stores;
    }

    @Override
    public Fragment getItem(int position) {
        StockLocation stockLocation = mStockLocations.get(position);
        return ProductSampleListFragment.newInstance(stockLocation);
    }

    @Override
    public int getCount() {
        return mStockLocations.size();
    }
}
