package com.shopper.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.CashTransfer;
import com.happyfresh.snowflakes.hoverfly.utils.FormatterUtils;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;

import butterknife.ButterKnife;
import butterknife.BindView;

/**
 * Created by ifranseda on 5/18/15.
 */
public class HandoverHistoryAdapter extends RecyclerView.Adapter<HandoverHistoryAdapter.HistoryViewHolder> {
    private final Context mContext;
    private List<CashTransfer> mCashTransfers;
    private DateFormat mDateFormatter;
    private DateFormat mTimeFormatter;
    private NumberFormat mNumberFormatter;

    public HandoverHistoryAdapter(Context context, List<CashTransfer> cashTransfers) {
        mContext = context;
        mCashTransfers = cashTransfers;

        mDateFormatter = new SimpleDateFormat("dd MMM, yyyy");
        mDateFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));

        mTimeFormatter = new SimpleDateFormat("hh:mm a");
    }

    private NumberFormat getNumberFormatter(String currencyCode) {
        if (mNumberFormatter == null) {
            mNumberFormatter = FormatterUtils.getNumberFormatter(currencyCode);
        }

        return mNumberFormatter;
    }

    @Override
    public HandoverHistoryAdapter.HistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(mContext).inflate(R.layout.layout_handover_item, parent, false);
        return new HistoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HandoverHistoryAdapter.HistoryViewHolder holder, int position) {
        CashTransfer cashTransfer = mCashTransfers.get(position);

        HistoryViewHolder viewHolder = holder;
        viewHolder.orderDate.setText(mDateFormatter.format(cashTransfer.getCreated()));
        viewHolder.orderTime.setText(mTimeFormatter.format(cashTransfer.getCreated()));
        viewHolder.amount.setText(getNumberFormatter(cashTransfer.getCurrency()).format(cashTransfer.getAmount()));
    }

    @Override
    public int getItemCount() {
        return mCashTransfers.size();
    }

    public static class HistoryViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.order_number)
        TextView orderDate;

        @BindView(R.id.order_date)
        TextView orderTime;

        @BindView(R.id.source_amount)
        TextView amount;

        public HistoryViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
