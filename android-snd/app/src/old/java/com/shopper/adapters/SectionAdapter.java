package com.shopper.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.happyfresh.fulfillment.R;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ifranseda on 11/19/14.
 */
public class SectionAdapter extends BaseAdapter {
    private ArrayList<Map.Entry<String, String>> mData = new ArrayList<>();
    private Map<String, View> mCustomView = new HashMap<>();
    private ArrayList<Integer> mViewType = new ArrayList<>();
    private boolean isPriceChanged = false;
    private String mNewPrice;
    private Context mContext;
    private LayoutInflater mInflater;

    public SectionAdapter(Context context) {
        mContext = context;
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void addItem(final Map.Entry<String, String> item) {
        addItem(item, ViewType.TYPE_ITEM);
    }

    public void addItem(final Map.Entry<String, String> item, int type) {
        mViewType.add(type);
        mData.add(item);
        notifyDataSetChanged();
    }

    public void addCustomView(String key, View customView) {
        mCustomView.put(key, customView);
        notifyDataSetChanged();
    }

    public void clearItem() {
        mViewType.clear();
        mData.clear();
        mCustomView.clear();
    }

    public void addSectionHeaderItem(final String item) {
        mData.add(new AbstractMap.SimpleEntry<String, String>(item, item));
        mViewType.add(ViewType.TYPE_SECTION);
        notifyDataSetChanged();
    }

    public void setPriceChanged(boolean isPriceChanged) {
        this.isPriceChanged = isPriceChanged;
    }

    public void setNewPrice(String newPrice) {
        this.mNewPrice = newPrice;
    }

    @Override
    public int getItemViewType(int position) {
        if (mCustomView.containsKey(mData.get(position).getKey())) {
            return ViewType.TYPE_CUSTOM_VIEW;
        } else {
            return mViewType.get(position);
        }
    }

    @Override
    public int getViewTypeCount() {
        return ViewType.VIEW_TYPE_COUNT;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        int rowType = getItemViewType(position);

        if (convertView == null) {
            viewHolder = new ViewHolder();

            switch (rowType) {
                case ViewType.TYPE_SECTION:
                    convertView = mInflater.inflate(R.layout.layout_item_section, null);
                    viewHolder.labelTextView = (TextView) convertView.findViewById(R.id.header);
                    viewHolder.itemType = ViewType.TYPE_SECTION;
                    break;

                case ViewType.TYPE_ITEM:
                    convertView = mInflater.inflate(R.layout.layout_item_content, null);
                    viewHolder.labelTextView = (TextView) convertView.findViewById(R.id.label);
                    viewHolder.detailTextView = (TextView) convertView.findViewById(R.id.detail);
                    viewHolder.newPriceTextView = (TextView) convertView.findViewById(R.id.new_price);
                    viewHolder.itemType = ViewType.TYPE_ITEM;
                    break;

                case ViewType.TYPE_ITEM_VERTICAL:
                    convertView = mInflater.inflate(R.layout.layout_item_content_vertical, null);
                    viewHolder.labelTextView = (TextView) convertView.findViewById(R.id.label);
                    viewHolder.detailTextView = (TextView) convertView.findViewById(R.id.detail);
                    viewHolder.itemType = ViewType.TYPE_ITEM;
                    break;

                case ViewType.TYPE_CUSTOM_VIEW:
                    convertView = mInflater.inflate(R.layout.layout_item_content_customview, null);
                    viewHolder.containerView = (ViewGroup) convertView.findViewById(R.id.container_view);
                    viewHolder.itemType = ViewType.TYPE_ITEM;
                    break;

                case ViewType.TYPE_ITEM_PROMOTIONS:
                    convertView = mInflater.inflate(R.layout.layout_item_promotions, null);
                    viewHolder.labelTextView = (TextView) convertView.findViewById(R.id.label);
                    viewHolder.detailTextView = (TextView) convertView.findViewById(R.id.detail);
                    viewHolder.newPriceTextView = (TextView) convertView.findViewById(R.id.new_price);
                    viewHolder.itemType = ViewType.TYPE_ITEM;
                    break;
            }

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Map.Entry<String, String> data = mData.get(position);
        if (mCustomView.containsKey(data.getKey())) {
            viewHolder.setView(mCustomView.get(data.getKey()));
        } else {
            viewHolder.setData(data);

            boolean isPriceUnit = mContext.getString(R.string.item_detail_label_price_per_unit).equals(viewHolder.labelTextView.getText());
            if (isPriceUnit && isPriceChanged) {
                viewHolder.flagPriceChanged(mNewPrice);
            }
        }

        return convertView;
    }

    public static final class ViewType {
        public static final int TYPE_SECTION = 0;
        public static final int TYPE_ITEM = 1;
        public static final int TYPE_ITEM_VERTICAL = 2;
        public static final int TYPE_CUSTOM_VIEW = 3;
        public static final int TYPE_ITEM_PROMOTIONS = 4;
        public static final int VIEW_TYPE_COUNT = 5;
    }

    private static class ViewHolder {
        public int itemType;
        public TextView labelTextView;
        public TextView detailTextView;
        public TextView newPriceTextView;
        public ViewGroup containerView;

        public void setData(Map.Entry<String, String> data) {
            if (itemType == ViewType.TYPE_SECTION) {
                labelTextView.setText(data.getKey());
            } else {
                labelTextView.setText(data.getKey());
                detailTextView.setText(data.getValue());
            }
        }

        public void setView(View customView) {
            if (customView.getParent() != null) {
                ((ViewGroup) customView.getParent()).removeViewAt(0);
            }
            containerView.addView(customView, 0);
        }

        public void flagPriceChanged(String newPrice) {
            newPriceTextView.setVisibility(View.VISIBLE);
            newPriceTextView.setText(newPrice);

            detailTextView.setPaintFlags(detailTextView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
    }
}
