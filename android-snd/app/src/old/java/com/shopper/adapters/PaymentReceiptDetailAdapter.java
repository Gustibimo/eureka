package com.shopper.adapters;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;

import com.shopper.fragments.PaymentReceiptDetailFragment;
import com.happyfresh.snowflakes.hoverfly.models.ReceiptItem;

import java.util.ArrayList;
import java.util.List;

public class PaymentReceiptDetailAdapter extends FragmentPagerAdapter {
    private List<ReceiptItem> mReceiptItems = new ArrayList<>();
    private Long mShipmentId;

    public PaymentReceiptDetailAdapter(FragmentManager fm, Long shipmentId, List<ReceiptItem> receiptItems) {
        super(fm);
        mReceiptItems = receiptItems;
        mShipmentId = shipmentId;
    }

    @Override
    public Fragment getItem(int position) {
        ReceiptItem receiptItem = mReceiptItems.get(position);
        receiptItem.setPosition(position);

        return PaymentReceiptDetailFragment.newInstance(mShipmentId, receiptItem);
    }

    @Override
    public int getCount() {
        return mReceiptItems.size();
    }
}