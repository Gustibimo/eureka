package com.shopper.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.Replacement;
import com.happyfresh.snowflakes.hoverfly.models.SpreeImage;
import com.happyfresh.snowflakes.hoverfly.models.Variant;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by ifranseda on 11/20/14.
 */
public class ReplacementItemAdapter extends BaseAdapter {
    private final Context mContext;
    private List<Replacement> mItems;

    public ReplacementItemAdapter(Context context, List<Replacement> lineItems) {
        mContext = context;
        mItems = lineItems;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Replacement getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Replacement replacement = getItem(position);
        Variant variant = replacement.getVariant();

        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.layout_replacement_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        List<SpreeImage> images = variant.images();
        String imageUrl = null;
        if (images.size() > 0) {
            imageUrl = images.get(0).getProductUrl();
        }
        Picasso.with(mContext).load(imageUrl).placeholder(R.drawable.default_product).into(viewHolder.itemImage);

        viewHolder.itemName.setText(variant.getName());
        viewHolder.itemPrice.setText(String.valueOf(variant.getDisplayPrice()));

        return convertView;
    }

    public static class ViewHolder {
        public ImageView itemImage;
        public TextView itemName;
        public TextView itemPrice;

        public ViewHolder(View view) {
            itemImage = (ImageView) view.findViewById(R.id.item_image);
            itemName = (TextView) view.findViewById(R.id.item_variant_name);
            itemPrice = (TextView) view.findViewById(R.id.item_price);

        }
    }
}
