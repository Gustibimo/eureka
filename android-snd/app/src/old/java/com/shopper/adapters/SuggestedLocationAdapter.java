package com.shopper.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.SubDistrict;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by rsavianto on 2/7/15.
 */
public class SuggestedLocationAdapter extends RecyclerView.Adapter<SuggestedLocationAdapter.ViewHolder> {

    private OnItemClickListener mOnItemClickListener;

    private Context mContext;

    private List<SubDistrict> mSubDistricts = new ArrayList<SubDistrict>();

    public SuggestedLocationAdapter(Context context) {
        this.mContext = context;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    public void addAll(List<SubDistrict> subDistricts) {
        mSubDistricts.clear();
        mSubDistricts.addAll(subDistricts);
    }

    public void clearAll() {
        mSubDistricts.clear();
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.list_location_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        SubDistrict subDistrict = mSubDistricts.get(position);

        StringBuilder builder = new StringBuilder(subDistrict.getName());
        builder.append(", ");
        builder.append(subDistrict.getDistrict().getName());
        builder.append(", ");
        builder.append(subDistrict.getZipcode());

        viewHolder.itemLocation.setText(builder.toString());
    }

    @Override
    public int getItemCount() {
        return mSubDistricts.size();
    }

    public interface OnItemClickListener {

        void onItemClick(View v, Object object);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.location)
        TextView itemLocation;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mOnItemClickListener != null) {
                SubDistrict subDistrict = mSubDistricts.get(getPosition());
                mOnItemClickListener.onItemClick(v, subDistrict);
            }
        }
    }
}



