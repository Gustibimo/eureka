package com.shopper.adapters;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v13.app.FragmentPagerAdapter;

import com.happyfresh.snowflakes.hoverfly.models.Shipment;
import com.shopper.fragments.DriverShoppingListFragment;

import java.util.List;

/**
 * Created by ifranseda on 8/6/15.
 */
public class DriverShoppingListBatchAdapter extends FragmentPagerAdapter {
    private Context mContext;

    private List<Shipment> mShipmentList;

    public DriverShoppingListBatchAdapter(FragmentManager fm, Context context, List<Shipment> shipments) {
        super(fm);
        mContext = context;
        mShipmentList = shipments;
    }

    @Override
    public Fragment getItem(int position) {
        Shipment shipment = mShipmentList.get(position);
        return DriverShoppingListFragment.newInstance(shipment);
    }

    @Override
    public int getCount() {
        return mShipmentList.size();
    }
}