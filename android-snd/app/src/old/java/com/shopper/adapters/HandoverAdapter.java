package com.shopper.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.Cash;
import com.happyfresh.snowflakes.hoverfly.utils.FormatterUtils;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;

import butterknife.ButterKnife;
import butterknife.BindView;

/**
 * Created by ifranseda on 5/18/15.
 */
public class HandoverAdapter extends RecyclerView.Adapter<HandoverAdapter.BalanceViewHolder> {
    private final Context mContext;
    private List<Cash> mCashes;
    private DateFormat mDateFormatter;
    private NumberFormat mNumberFormatter;

    public HandoverAdapter(Context context, List<Cash> cashes) {
        mContext = context;
        mCashes = cashes;

        mDateFormatter = new SimpleDateFormat("dd MMM");
        mDateFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    private NumberFormat getNumberFormatter(String currencyCode) {
        if (mNumberFormatter == null) {
            mNumberFormatter = FormatterUtils.getNumberFormatter(currencyCode);
        }

        return mNumberFormatter;
    }

    @Override
    public HandoverAdapter.BalanceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(mContext).inflate(R.layout.layout_handover_item, parent, false);
        return new BalanceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HandoverAdapter.BalanceViewHolder holder, int position) {
        Cash cash = mCashes.get(position);

        BalanceViewHolder viewHolder = holder;
        viewHolder.orderNumber.setText(cash.getOrderNumber());
        viewHolder.orderDate.setText(mDateFormatter.format(cash.getCreated()));
        viewHolder.sourceAmount.setText(getNumberFormatter(cash.getCurrency()).format(cash.getSourceAmount()));
    }

    @Override
    public int getItemCount() {
        return mCashes.size();
    }

    public static class BalanceViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.order_number)
        TextView orderNumber;

        @BindView(R.id.order_date)
        TextView orderDate;

        @BindView(R.id.source_amount)
        TextView sourceAmount;

        public BalanceViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
