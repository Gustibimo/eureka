package com.shopper.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.Job;
import com.happyfresh.snowflakes.hoverfly.models.Shipment;
import com.happyfresh.snowflakes.hoverfly.utils.FormatterUtils;
import com.shopper.common.Constant;
import com.shopper.components.CircularProgressBar;
import com.shopper.interfaces.OnCallCustomerListener;
import com.shopper.utils.StringUtils;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.BindView;

/**
 * Created by ifranseda on 8/10/15.
 */
public class BatchDeliveryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final class ViewType {
        public static final int CREDITS = 0;
        public static final int COD = 1;
        public static final int DELIVERED = 2;
        public static final int FAILED = 3;
        public static final int CORPORATE = 4;
    }

    public interface OnStartDeliverNow {
        void onStart(Shipment shipment);
    }

    private Context mContext;
    private List<Shipment> mShipmentList = new ArrayList<>();
    private OnCallCustomerListener mCallCustomerListener;
    private OnStartDeliverNow mOnStartDeliverNow;

    public BatchDeliveryAdapter(Context ctx, List<Shipment> shipments) {
        mContext = ctx;
        mShipmentList = shipments;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == ViewType.DELIVERED || viewType == ViewType.FAILED) {
            View view = inflater.inflate(R.layout.layout_driver_item_delivered, parent, false);
            return new DeliveredViewHolder(view);
        } else {
            View view = inflater.inflate(R.layout.layout_driver_item_deliver_now, parent, false);
            return new ViewHolder(mContext, view, viewType);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final Shipment shipment = mShipmentList.get(position);
        int viewType = getItemViewType(position);

        if (viewType == ViewType.DELIVERED || viewType == ViewType.FAILED) {
            DeliveredViewHolder viewHolder = (DeliveredViewHolder) holder;
            viewHolder.setData(shipment);
        } else {
            ViewHolder viewHolder = (ViewHolder) holder;
            viewHolder.setData(shipment, mCallCustomerListener, mOnStartDeliverNow);
        }
    }

    @Override
    public int getItemCount() {
        return mShipmentList.size();
    }

    @Override
    public int getItemViewType(int position) {
        Shipment shipment = mShipmentList.get(position);
        if (Job.Progress.getFINISHED().equalsIgnoreCase(shipment.getDeliveryJob().getState())) {
            return ViewType.DELIVERED;
        } else if (Job.Progress.getFAILED().equalsIgnoreCase(shipment.getDeliveryJob().getState())) {
            return ViewType.FAILED;
        } else {
            if (shipment != null && shipment.getOrder() != null && shipment.getOrder().getPaymentMethod() != null) {
                if (shipment.getOrder().getCompanyId() != 0) {
                    return ViewType.CORPORATE;
                }

                if (Constant.CashOnDeliveryMethod.equalsIgnoreCase(shipment.getOrder().getPaymentMethod())) {
                    return ViewType.COD;
                }
            }

            return ViewType.CREDITS;
        }
    }

    public void setCallCustomerListener(OnCallCustomerListener listener) {
        mCallCustomerListener = listener;
    }

    public void setStartDeliverListener(OnStartDeliverNow listener) {
        mOnStartDeliverNow = listener;
    }

    public static class DeliveredViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.order_number)
        TextView orderNumber;

        @BindView(R.id.happycorporate_layout)
        View happyCorporateLayout;

        public DeliveredViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setData(Shipment shipment) {
            orderNumber.setText(shipment.getOrder().getNumber());

            if (shipment.getOrder().getCompanyId() != 0) {
                happyCorporateLayout.setVisibility(View.VISIBLE);
            }
            else {
                happyCorporateLayout.setVisibility(View.GONE);
            }
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.happycorporate_layout)
        View happyCorporateLayout;

        @BindView(R.id.delivery_payment_layout)
        LinearLayout deliveryPaymentLayout;

        @BindView(R.id.payment_method_icon)
        ImageView paymentMethodIcon;

        @BindView(R.id.payment_method)
        TextView paymentMethod;

        @BindView(R.id.total_order)
        TextView totalOrder;

        @BindView(R.id.order_number)
        TextView orderNumber;

        @BindView(R.id.call_customer_button)
        Button callCustomerButton;

        @BindView(R.id.delivery_location_name)
        TextView deliveryLocationName;

        @BindView(R.id.delivery_location_address1)
        TextView deliveryLocationAddress1;

        @BindView(R.id.delivery_location_address2)
        TextView deliveryLocationAddress2;

        @BindView(R.id.delivery_instruction_text)
        TextView deliveryInstruction;

        @BindView(R.id.button_deliver_now)
        Button buttonDeliverNow;

        @BindView(R.id.progress_indicator)
        CircularProgressBar progresIndicator;

        @BindView(R.id.delivery_location_address_details)
        TextView deliveryLocationAddressDetails;

        @BindView(R.id.delivery_location_address_number)
        TextView deliveryLocationAddressNumber;

        @BindView(R.id.delivery_location_details_text)
        TextView deliveryLocationDetailsText;

        Context context;
        NumberFormat numberFormatter;
        int viewType;

        public ViewHolder(Context ctx, View itemView, int type) {
            super(itemView);
            context = ctx;
            viewType = type;

            ButterKnife.bind(this, itemView);

            progresIndicator.setVisibility(View.GONE);
        }

        private NumberFormat getNumberFormatter(String currencyCode) {
            if (numberFormatter == null) {
                numberFormatter = FormatterUtils.getNumberFormatter(currencyCode);
            }

            return numberFormatter;
        }

        public void setData(final Shipment shipment, final OnCallCustomerListener listener, final OnStartDeliverNow deliverListener) {
            if (shipment.getOrder().getCompanyId() != 0) {
                happyCorporateLayout.setVisibility(View.VISIBLE);
                deliveryPaymentLayout.setVisibility(View.GONE);
            }
            else {
                if (viewType == ViewType.COD) {
                    happyCorporateLayout.setVisibility(View.GONE);
                    deliveryPaymentLayout.setVisibility(View.VISIBLE);
                    deliveryPaymentLayout
                            .setBackgroundColor(context.getResources().getColor(R.color.payment_cod_background));
                    paymentMethodIcon.setImageResource(R.drawable.icon_cod);
                    paymentMethod.setText(R.string.payment_total_cod);
                    paymentMethod.setTextColor(context.getResources().getColor(android.R.color.black));
                    totalOrder.setTextColor(context.getResources().getColor(android.R.color.black));
                }
                else {
                    happyCorporateLayout.setVisibility(View.GONE);
                    deliveryPaymentLayout.setVisibility(View.VISIBLE);
                    deliveryPaymentLayout
                            .setBackgroundColor(context.getResources().getColor(R.color.payment_cc_background));
                    paymentMethodIcon.setImageResource(R.drawable.icon_cc);
                    paymentMethod.setText(R.string.payment_total_credit_card);
                    paymentMethod.setTextColor(context.getResources().getColor(android.R.color.white));
                    totalOrder.setTextColor(context.getResources().getColor(android.R.color.white));
                }
            }

            String total = getNumberFormatter(shipment.getOrder().getCurrency()).format(shipment.getOrder().getTotal());
            totalOrder.setText(total);
            orderNumber.setText(shipment.getOrder().getNumber());

            if (shipment.getAddress() != null) {
                deliveryLocationName.setText(shipment.getAddress().getAddress1());
                deliveryLocationAddress1.setText(shipment.getAddress().getAddress2());
                deliveryLocationAddress2.setText(StringUtils.buildAddressCityInfo(shipment.getAddress()));

                if (shipment.getAddress().getAddressDetail() != null && !shipment.getAddress().getAddressDetail().trim()
                        .isEmpty()) {
                    deliveryLocationAddressDetails.setText(shipment.getAddress().getAddressDetail());

                    deliveryLocationAddressDetails.setVisibility(View.VISIBLE);
                    deliveryLocationDetailsText.setVisibility(View.VISIBLE);
                } else {
                    deliveryLocationAddressDetails.setVisibility(View.GONE);
                    deliveryLocationDetailsText.setVisibility(View.GONE);
                }

                if (shipment.getAddress().getAddressNumber() != null && !shipment.getAddress().getAddressNumber().trim()
                        .isEmpty()) {
                    deliveryLocationAddressNumber.setText(shipment.getAddress().getAddressNumber());
                    deliveryLocationAddressNumber.setVisibility(View.VISIBLE);
                }
                else {
                    deliveryLocationAddressNumber.setVisibility(View.GONE);
                }
            } else {
                deliveryLocationName.setText("Unknown");
                deliveryLocationAddress1.setText("Unknown");
                deliveryLocationAddress2.setText("Unknown");
            }

            deliveryInstruction.setText(shipment.getAddress().getDeliveryInstruction());

            callCustomerButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onClick(shipment);
                    }
                }
            });

            buttonDeliverNow.setEnabled(true);
            progresIndicator.setVisibility(View.GONE);

            buttonDeliverNow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (deliverListener != null) {
                        buttonDeliverNow.setEnabled(false);
                        progresIndicator.setVisibility(View.VISIBLE);

                        deliverListener.onStart(shipment);
                    }
                }
            });
        }
    }
}
