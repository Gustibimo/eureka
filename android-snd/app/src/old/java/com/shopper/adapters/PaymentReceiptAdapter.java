package com.shopper.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.ReceiptItem;
import com.happyfresh.snowflakes.hoverfly.models.Shipment;
import com.shopper.listeners.PaymentReceiptListener;
import com.shopper.views.PaymentAddReceiptButton;
import com.shopper.views.PaymentHeaderViewHolder;
import com.shopper.views.PaymentReceiptViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ifranseda on 11/25/15.
 */
public class PaymentReceiptAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private Shipment mShipment;
    private List<ReceiptItem> mReceiptList = new ArrayList<>();
    private boolean requireNumber;
    private PaymentReceiptListener listener;

    public PaymentReceiptAdapter(Context context, Shipment shipment,
            List<ReceiptItem> items, boolean requireNumber,
            PaymentReceiptListener listener) {
        this.mContext = context;
        this.mShipment = shipment;
        this.mReceiptList = items;
        this.requireNumber = requireNumber;
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == ViewType.HEADER) {
            View view = inflater.inflate(R.layout.payment_fragment_header_payment, parent, false);
            return new PaymentHeaderViewHolder(view);
        } else if (viewType == ViewType.ADD_BUTTON) {
            View view = inflater.inflate(R.layout.payment_fragment_add_receipt, parent, false);
            return new PaymentAddReceiptButton(view);
        } else {
            View view = inflater.inflate(R.layout.layout_payment_receipt, parent, false);
            return new PaymentReceiptViewHolder(view, listener);
        }
    }

    @Override
    public int getItemViewType(int position) {
//        ReceiptItem receiptItem = mReceiptList.get(position);
        if (position == 0) {
            return ViewType.HEADER;
        } else if (mReceiptList.size() > 0 && getItemCount() - 1 == position) {
            ReceiptItem lastReceipt = mReceiptList.get(mReceiptList.size() - 1);
            if (lastReceipt.completed()) {
                return ViewType.ADD_BUTTON;
            } else {
                return ViewType.RECEIPT;
            }
        } else {
            return ViewType.RECEIPT;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);

        if (viewType == ViewType.HEADER) {
            PaymentHeaderViewHolder viewHolder = (PaymentHeaderViewHolder) holder;
            viewHolder.showData(mShipment);
        } else if (viewType == ViewType.ADD_BUTTON) {
            PaymentAddReceiptButton viewHolder = (PaymentAddReceiptButton) holder;
        } else if (viewType == ViewType.RECEIPT) {
            int receiptPosition = position - 1;
            ReceiptItem receiptItem = mReceiptList.get(receiptPosition);
            receiptItem.setPosition(receiptPosition);

            PaymentReceiptViewHolder viewHolder = (PaymentReceiptViewHolder) holder;
            viewHolder.showData(receiptItem, requireNumber, mContext, receiptPosition);
        }
    }

    @Override
    public int getItemCount() {
        int receiptListSize = mReceiptList.size();
        if (receiptListSize > 0) {
            ReceiptItem lastReceipt = mReceiptList.get(receiptListSize - 1);
            if (lastReceipt.completed()) {
                return receiptListSize + 2;
            } else {
                return receiptListSize + 1;
            }
        } else {
            return 2;
        }
    }

    private static final class ViewType {
        public static final int HEADER = 0;
        public static final int RECEIPT = 1;
        public static final int ADD_BUTTON = 2;
    }
}
