package com.shopper.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.LineItem;
import com.happyfresh.snowflakes.hoverfly.models.Product;
import com.happyfresh.snowflakes.hoverfly.models.Shipment;
import com.happyfresh.snowflakes.hoverfly.models.Variant;
import com.happyfresh.snowflakes.hoverfly.utils.FormatterUtils;
import com.shopper.common.ShopperApplication;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.BindView;

/**
 * Created by julianagalag on 5/6/15.
 */
public class SearchProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = SearchProductAdapter.class.getSimpleName();

    private List<Product> mResults = new ArrayList<Product>();
    private Context mContext;
    private ShopperApplication mApplication;
    private LineItem mLineItem;
    private NumberFormat mNumberFormat;
    private Shipment mShipment;

    OnReplacementListener mOnReplacementListener;

    public SearchProductAdapter(Context context, Shipment shipment, LineItem lineItem) {
        mContext = context;
        mShipment = shipment;
        mLineItem = lineItem;

        mApplication = ShopperApplication.getInstance();
        mNumberFormat = FormatterUtils.getNumberFormatter(mShipment.getOrder().getCurrency());
    }

    public void setOnReplaceListener(OnReplacementListener listener) {
        mOnReplacementListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.list_search_product, null);
        return new ReplacementProductViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        Product product = mResults.get(position);
        Variant variant = product.getVariants().get(0);

        final ReplacementProductViewHolder itemViewHolder = (ReplacementProductViewHolder) viewHolder;
        itemViewHolder.replaceButtonProgress.setVisibility(View.INVISIBLE);

        itemViewHolder.productName.setText(variant.getName());

        String replacedItem = null;
        if (variant.getImages().size() > 0) {
            replacedItem = variant.getImages().get(0).getProductUrl();
        }

        Picasso.with(mContext).cancelRequest(itemViewHolder.productImage);
        Picasso.with(mContext).load(replacedItem)
                .error(R.drawable.default_product)
                .placeholder(R.drawable.default_product)
                .into(itemViewHolder.productImage);

        Long productVariantId = product.getVariants().get(0).getRemoteId();

        String diffString = mNumberFormat.format(product.getPrice());
        itemViewHolder.productPrice.setText("" + diffString);

        if (mLineItem.getReplacementCustomer() == null
                && mLineItem.getVariant().getRemoteId() == productVariantId) {
            product.setReplaced(true);
        }

        if (mLineItem.getReplacementCustomer() != null &&
                (mLineItem.getReplacementCustomer().getRemoteId() == productVariantId)) {
            product.setReplaced(true);
        }

        if (product.getReplaced()) {
            itemViewHolder.showAsReplaced();
            itemViewHolder.replaceButton.setOnClickListener(null);
        } else {
            itemViewHolder.showAsNormal();
            itemViewHolder.replaceButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnReplacementListener != null) {
                        mOnReplacementListener.onReplacementClick(position);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mResults.size();
    }

    @Override
    public long getItemId(int position) {
        return mResults.get(position).getRemoteId();
    }

    public Product getItem(int position) {
        return mResults.get(position);
    }

    public void addAll(List<Product> items) {
        mResults.clear();
        mResults = items;
    }

    public void clear() {
        mResults.clear();
    }

    class ReplacementProductViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.replacement_product_image)
        ImageView productImage;

        @BindView(R.id.replacement_product_name)
        TextView productName;

        @BindView(R.id.replacement_product_price)
        TextView productPrice;

        @BindView(R.id.replace_button_container)
        View replaceButtonContainer;

        @BindView(R.id.replace_button)
        ImageView replaceButton;

        @BindView(R.id.replace_button_progress)
        View replaceButtonProgress;

        public ReplacementProductViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void showProgress(boolean show) {
            if (show) {
                replaceButton.setVisibility(View.GONE);
                replaceButtonProgress.setVisibility(View.VISIBLE);
            } else {
                replaceButton.setVisibility(View.VISIBLE);
                replaceButtonProgress.setVisibility(View.INVISIBLE);
            }
        }

        public void showAsReplaced() {
            replaceButton.setImageResource(R.drawable.icon_cancel_replacement);
        }

        public void showAsNormal() {
            replaceButton.setImageResource(R.drawable.icon_add_replacement);
        }
    }

    public interface OnReplacementListener {
        void onReplacementClick(int position);
    }
}
