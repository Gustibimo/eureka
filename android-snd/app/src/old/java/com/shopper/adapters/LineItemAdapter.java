package com.shopper.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.LineItem;
import com.happyfresh.snowflakes.hoverfly.models.PriceAmendment;
import com.happyfresh.snowflakes.hoverfly.models.SpreeImage;
import com.happyfresh.snowflakes.hoverfly.models.Variant;
import com.happyfresh.snowflakes.hoverfly.utils.FormatterUtils;
import com.shopper.common.Constant;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.List;

/**
 * Created by ifranseda on 11/20/14.
 */
public class LineItemAdapter extends RecyclerView.Adapter<LineItemAdapter.LineItemViewHolder> {
    private Context mContext;
    private List<LineItem> mItems;
    private int mType;

    public LineItemAdapter(Context context, List<LineItem> lineItems, int type) {
        mContext = context;
        mItems = lineItems;
        mType = type;
    }

    @Override
    public LineItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(mContext).inflate(R.layout.layout_line_item, parent, false);
        return new LineItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LineItemViewHolder viewHolder, int position) {
        LineItem item = mItems.get(position);

        LineItem prevItem = null;
        if (position > 0) {
            prevItem = mItems.get(position - 1);
        }

        if (prevItem != null && item != null) {
            if (prevItem.getTaxonName() != null && item.getTaxonName() != null) {
                if (prevItem.getTaxonName().equals(item.getTaxonName())) {
                    viewHolder.setTaxon(null);
                } else {
                    viewHolder.setTaxon(item.getTaxonName());
                }
            } else {
                viewHolder.setTaxon(null);
            }
        } else {
            if (item != null) {
                viewHolder.setTaxon(item.getTaxonName());
            } else {
                viewHolder.setTaxon(null);
            }
        }

        // Add replacement status
        if (mType == Constant.SHOPPING_LIST) {
            String replacementType = item.getReplacementType();
            if (replacementType != null) {
                if (replacementType.length() > 0) {
                    if (replacementType.equalsIgnoreCase(Constant.SUGGEST_REPLACEMENT_BY_CALL)) {
                        viewHolder.itemReplacementLayout.setVisibility(View.VISIBLE);
                        viewHolder.itemReplacementStatus.setText(R.string.call_me);
                    } else if (replacementType.equalsIgnoreCase(Constant.SUGGEST_REPLACEMENT_BY_CHAT)) {
                        viewHolder.itemReplacementLayout.setVisibility(View.VISIBLE);
                        viewHolder.itemReplacementStatus.setText(R.string.chat_me);
                    } else if (replacementType.equalsIgnoreCase(Constant.REPLACE_FREELY)) {
                        viewHolder.itemReplacementLayout.setVisibility(View.VISIBLE);
                        viewHolder.itemReplacementStatus.setText(R.string.replace_freely);
                    } else {
                        viewHolder.itemReplacementLayout.setVisibility(View.GONE);
                        viewHolder.itemReplacementStatus.setText("");
                    }
                }
            } else {
                viewHolder.itemReplacementLayout.setVisibility(View.VISIBLE);
                viewHolder.itemReplacementStatus.setText(R.string.suggest_replacement);
            }
        }

        viewHolder.orderIndicator.setBackgroundColor(item.getShipment().getColorResource());
        viewHolder.orderNumber.setText(item.getShipment().getOrder().getNumber());
        viewHolder.customerName.setText(item.getShipment().getCustomerName());

        List<SpreeImage> images = item.getVariant().images();
        String productUrl = null;
        if (images.size() > 0) {
            productUrl = images.get(0).getProductUrl();
        }
        Picasso.with(mContext).cancelRequest(viewHolder.itemImage);
        Picasso.with(mContext).load(productUrl).placeholder(R.drawable.default_product).into(viewHolder.itemImage);

        int shoppedCounter = item.getTotalShopped() - item.getTotalReplacedCustomer() - item.getTotalReplacedShopper();
        String shoppedCount = mContext.getString(R.string.item_shopped_counter, shoppedCounter, item.getTotal());
        viewHolder.itemCounter.setText(String.valueOf(shoppedCounter));
        viewHolder.itemCounterTotal.setText(shoppedCount);
        viewHolder.itemName.setText(item.getVariant().getName().trim());

        if (item.hasNaturalUnit()) {
            viewHolder.itemPrice.setText(String.format("%s/%s", itemPrice(item), item.getDisplayAverageWeight()));
        } else {
            viewHolder.itemPrice.setText(itemPrice(item));
        }

        LineItem.PurchaseStatus purchaseStatus = item.getStatus();
        if (purchaseStatus == LineItem.PurchaseStatus.NOT_STARTED) {
            viewHolder.itemStatus.setVisibility(View.GONE);
        } else {
            viewHolder.itemStatus.setVisibility(View.VISIBLE);
            viewHolder.itemStatus.setText(purchaseStatus.statusName(mContext));
            viewHolder.itemStatus.setTextColor(purchaseStatus.textColor(mContext));
            viewHolder.itemStatus.setBackgroundColor(purchaseStatus.backgroundColor(mContext));
        }

        Variant custReplacement = item.getReplacementCustomer();
        if (item.getTotalReplacedCustomer() > 0 && custReplacement != null) {
            String replacementProductUrl = null;
            List<SpreeImage> replacementImages = custReplacement.images();
            if (replacementImages.size() > 0) {
                replacementProductUrl = replacementImages.get(0).getProductUrl();
            }

            Picasso.with(mContext).load(replacementProductUrl).placeholder(R.drawable.default_product).into(viewHolder.replacementItemImage);

            viewHolder.replacementView.setVisibility(View.VISIBLE);
            viewHolder.replacementItemCounter.setText(String.valueOf(item.getTotalReplacedCustomer()));
            viewHolder.replacementItemName.setText(custReplacement.getName().trim());
            viewHolder.replacementItemName.setVisibility(View.VISIBLE);

            if (custReplacement.getDisplayAverageWeight() != null) {
                viewHolder.replacementItemPrice.setText(String.format("%s/%s", customerReplacementPrice(item), item.getDisplayAverageWeight()));
            } else {
                viewHolder.replacementItemPrice.setText(customerReplacementPrice(item));
            }
        } else {
            viewHolder.replacementView.setVisibility(View.GONE);
        }

        Variant replacement = item.getReplacementShopper();
        if (item.getTotalReplacedShopper() > 0 && replacement != null) {
            String imageUrl = null;
            List<SpreeImage> replacementImages = replacement.images();
            if (replacementImages.size() > 0) {
                imageUrl = replacementImages.get(0).getProductUrl();
            }

            Picasso.with(mContext).cancelRequest(viewHolder.shopReplacementItemImage);
            Picasso.with(mContext).load(imageUrl)
                    .placeholder(R.drawable.default_product)
                    .into(viewHolder.shopReplacementItemImage);

            viewHolder.shopReplacementView.setVisibility(View.VISIBLE);
            viewHolder.shopReplacementItemCounter.setText(String.valueOf(item.getTotalReplacedShopper()));
            viewHolder.shopReplacementItemName.setText(replacement.getName());
            viewHolder.shopReplacementItemName.setVisibility(View.VISIBLE);

            if (replacement.getDisplayAverageWeight() != null) {
                viewHolder.shopReplacementItemPrice.setText(String.format("%s/%s", shopperReplacementPrice(item), item.getDisplayAverageWeight()));
            } else {
                viewHolder.shopReplacementItemPrice.setText(shopperReplacementPrice(item));
            }
        } else {
            viewHolder.shopReplacementView.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    private String itemPrice(LineItem item) {
        if (item.getPriceAmendment() != null) {
            if (item.getPriceAmendment().getVariant() != null) {
                NumberFormat formatter = FormatterUtils.getNumberFormatter(item.getShipment().getOrder().getCurrency());
                Double newPrice = Double.parseDouble(item.getPriceAmendment().getVariant());
                return formatter.format(newPrice);
            }
        }

        return item.getDisplayCostPrice();
    }

    private String customerReplacementPrice(LineItem item) {
        if (item.getPriceAmendment() != null) {
            if (item.getPriceAmendment().getReplacement() != null) {
                NumberFormat formatter = FormatterUtils.getNumberFormatter(item.getShipment().getOrder().getCurrency());
                Double newPrice = Double.parseDouble(item.getPriceAmendment().getReplacement());
                return formatter.format(newPrice);
            }
        }

        Variant replacement = item.getReplacementCustomer();
        if (replacement.hasNaturalUnit()) {
            return String.format("%s/%s", replacement.getDisplayCostPrice(), replacement.getDisplayAverageWeight());
        } else {
            return replacement.getDisplayCostPrice();
        }
    }

    private String shopperReplacementPrice(LineItem item) {
        PriceAmendment priceAmendment = item.getPriceAmendmentShopperReplacement();
        if (priceAmendment != null) {
            if (priceAmendment.getReplacement() != null) {
                NumberFormat formatter = FormatterUtils.getNumberFormatter(item.getShipment().getOrder().getCurrency());
                Double newPrice = Double.parseDouble(priceAmendment.getReplacement());
                return formatter.format(newPrice);
            }
        }

        Variant replacement = item.getReplacementShopper();
        if (replacement.hasNaturalUnit()) {
            return String.format("%s/%s", replacement.getDisplayPrice(), replacement.getDisplayAverageWeight());
        } else { // Ordinary Item
            return replacement.getDisplayPrice();
        }
    }

    public static class LineItemViewHolder extends RecyclerView.ViewHolder {
        public View taxonContainer;
        public TextView taxonName;

        public View orderIndicator;

        public TextView orderNumber;
        public TextView customerName;

        public ImageView itemImage;
        public TextView itemCounter;
        public TextView itemCounterTotal;
        public TextView itemName;
        public TextView itemPrice;
        public TextView itemStatus;
        public TextView itemReplacementStatus;
        public RelativeLayout itemReplacementLayout;

        public View replacementView;
        public TextView replacementItemName;
        public TextView replacementItemPrice;
        public TextView replacementItemCounter;
        public ImageView replacementItemImage;

        public View shopReplacementView;
        public TextView shopReplacementItemName;
        public TextView shopReplacementItemPrice;
        public TextView shopReplacementItemCounter;
        public ImageView shopReplacementItemImage;

        public LineItemViewHolder(View view) {
            super(view);

            taxonContainer = view.findViewById(R.id.taxon_name_container);
            taxonName = (TextView) view.findViewById(R.id.taxon_name);

            orderIndicator = view.findViewById(R.id.order_color_indicator);

            orderNumber = (TextView) view.findViewById(R.id.order_number);
            customerName = (TextView) view.findViewById(R.id.customer_name);

            itemImage = (ImageView) view.findViewById(R.id.item_image);
            itemCounter = (TextView) view.findViewById(R.id.item_counter);
            itemCounterTotal = (TextView) view.findViewById(R.id.item_counter_total);
            itemName = (TextView) view.findViewById(R.id.item_variant_name);
            itemPrice = (TextView) view.findViewById(R.id.item_price);
            itemStatus = (TextView) view.findViewById(R.id.item_status);
            itemReplacementStatus = (TextView) view.findViewById(R.id.item_replacement_status);
            itemReplacementLayout = (RelativeLayout) view.findViewById(R.id.item_replacement_status_layout);

            replacementView = view.findViewById(R.id.replacement_layout);
            replacementItemName = (TextView) view.findViewById(R.id.replace_item_variant_name);
            replacementItemPrice = (TextView) view.findViewById(R.id.replace_item_price);
            replacementItemCounter = (TextView) view.findViewById(R.id.replace_item_counter);
            replacementItemImage = (ImageView) view.findViewById(R.id.replace_item_image);

            shopReplacementView = view.findViewById(R.id.sr_replacement_layout);
            shopReplacementItemName = (TextView) view.findViewById(R.id.sr_replace_item_variant_name);
            shopReplacementItemPrice = (TextView) view.findViewById(R.id.sr_replace_item_price);
            shopReplacementItemCounter = (TextView) view.findViewById(R.id.sr_replace_item_counter);
            shopReplacementItemImage = (ImageView) view.findViewById(R.id.sr_replace_item_image);
        }

        public void setTaxon(String taxon) {
            if (taxon == null) {
                taxonContainer.setVisibility(View.GONE);
            } else {
                taxonContainer.setVisibility(View.VISIBLE);
                taxonName.setText(taxon);
            }
        }
    }
}
