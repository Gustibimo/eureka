package com.shopper.adapters;

/**
 * Created by ifranseda on 4/24/15.
 */
public enum RejectionType {
    ORIGINAL,
    REPLACEMENT_CUSTOMER,
    REPLACEMENT_SHOPPER
}