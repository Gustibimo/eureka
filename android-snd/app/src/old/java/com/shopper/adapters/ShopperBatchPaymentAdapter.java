package com.shopper.adapters;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v13.app.FragmentPagerAdapter;

import com.happyfresh.snowflakes.hoverfly.models.Shipment;
import com.shopper.fragments.PaymentReviewListFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ifranseda on 9/15/15.
 */
public class ShopperBatchPaymentAdapter extends FragmentPagerAdapter {
    private Context mContext;
    private List<Shipment> mShipmentList = new ArrayList<>();

    public ShopperBatchPaymentAdapter(FragmentManager fm, Context context, List<Shipment> shipments) {
        super(fm);
        mContext = context;
        mShipmentList = shipments;
    }

    @Override
    public Fragment getItem(int position) {
        Shipment shipment = mShipmentList.get(position);
        return PaymentReviewListFragment.newInstance(shipment.getRemoteId());
    }

    @Override
    public int getCount() {
        return mShipmentList.size();
    }
}