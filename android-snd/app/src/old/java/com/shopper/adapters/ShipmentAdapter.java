package com.shopper.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.Job;
import com.happyfresh.snowflakes.hoverfly.models.Shipment;
import com.happyfresh.snowflakes.hoverfly.utils.FormatterUtils;
import com.shopper.common.Constant;
import com.shopper.interfaces.OnCallCustomerListener;
import com.shopper.utils.StringUtils;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.BindView;

/**
 * Created by ifranseda on 8/7/15.
 */
public class ShipmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private List<Shipment> mShipmentList = new ArrayList<Shipment>();
    private OnCallCustomerListener mCallCustomerListener;

    public ShipmentAdapter(Context ctx, List<Shipment> shipments) {
        mContext = ctx;
        mShipmentList = shipments;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_driver_pickup_item, parent, false);
        return new ViewHolder(mContext, view, viewType);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final Shipment shipment = mShipmentList.get(position);
        ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.setData(shipment, mCallCustomerListener);
    }

    @Override
    public int getItemViewType(int position) {
        Shipment shipment = mShipmentList.get(position);
        if (shipment != null && shipment.getOrder() != null && shipment.getOrder().getPaymentMethod() != null) {
            if (shipment.getOrder().getCompanyId() != 0) {
                return ViewType.CORPORATE;
            }
            if (Constant.CashOnDeliveryMethod.equalsIgnoreCase(shipment.getOrder().getPaymentMethod())) {
                return ViewType.COD;
            }
        }

        return ViewType.CREDITS;
    }

    @Override
    public int getItemCount() {
        return mShipmentList.size();
    }

    public void setCallCustomerListener(OnCallCustomerListener listener) {
        mCallCustomerListener = listener;
    }

    private static final class ViewType {
        public static final int CREDITS = 0;
        public static final int COD = 1;
        public static final int CORPORATE = 2;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.call_customer_button)
        public Button callCustomerButton;

        @BindView(R.id.happycorporate_layout)
        View happyCorporateLayout;

        @BindView(R.id.delivery_payment_layout)
        LinearLayout deliveryPaymentLayout;

        @BindView(R.id.payment_method_icon)
        ImageView paymentMethodIcon;

        @BindView(R.id.payment_method)
        TextView paymentMethod;

        @BindView(R.id.total_order)
        TextView totalOrder;

        @BindView(R.id.shopper_name)
        TextView shopperName;

        @BindView(R.id.order_number)
        TextView orderNumber;

        @BindView(R.id.delivery_location_name)
        TextView deliveryLocationName;

        @BindView(R.id.delivery_location_address1)
        TextView deliveryLocationAddress1;

        @BindView(R.id.delivery_location_address2)
        TextView deliveryLocationAddress2;

        @BindView(R.id.delivery_location_address_details)
        TextView deliveryLocationAddressDetails;

        @BindView(R.id.delivery_location_address_number)
        TextView deliveryLocationAddressNumber;

        @BindView(R.id.delivery_location_details_text)
        TextView deliveryLocationDetailsText;

        Context context;
        NumberFormat numberFormatter;
        int viewType;

        public ViewHolder(Context ctx, View itemView, int type) {
            super(itemView);
            context = ctx;
            viewType = type;

            ButterKnife.bind(this, itemView);
        }

        private NumberFormat getNumberFormatter(String currencyCode) {
            if (numberFormatter == null) {
                numberFormatter = FormatterUtils.getNumberFormatter(currencyCode);
            }

            return numberFormatter;
        }

        public void setData(final Shipment shipment, final OnCallCustomerListener listener) {
            if (viewType == ViewType.COD) {
                happyCorporateLayout.setVisibility(View.GONE);
                deliveryPaymentLayout.setVisibility(View.VISIBLE);
                deliveryPaymentLayout.setBackgroundColor(context.getResources().getColor(R.color.payment_cod_background));
                paymentMethodIcon.setImageResource(R.drawable.icon_cod);
                paymentMethod.setText(R.string.payment_total_cod);

                paymentMethod.setTextColor(context.getResources().getColor(android.R.color.black));
                totalOrder.setTextColor(context.getResources().getColor(android.R.color.black));
            }
            else if (viewType == ViewType.CORPORATE) {
                happyCorporateLayout.setVisibility(View.VISIBLE);
                deliveryPaymentLayout.setVisibility(View.GONE);
            }
            else {
                happyCorporateLayout.setVisibility(View.GONE);
                deliveryPaymentLayout.setVisibility(View.VISIBLE);
                deliveryPaymentLayout.setBackgroundColor(context.getResources().getColor(R.color.payment_cc_background));
                paymentMethodIcon.setImageResource(R.drawable.icon_cc);
                paymentMethod.setText(R.string.payment_total_credit_card);

                paymentMethod.setTextColor(context.getResources().getColor(android.R.color.white));
                totalOrder.setTextColor(context.getResources().getColor(android.R.color.white));
            }

            String total = getNumberFormatter(shipment.getOrder().getCurrency()).format(shipment.getOrder().getTotal());
            totalOrder.setText(total);

            Job shoppingJob = shipment.getShoppingJob();
            if (shoppingJob != null) {
                if (!shoppingJob.isFinished()) {
                    shopperName.setText(R.string.shopper_not_finished);
                    shopperName.setTextColor(context.getResources().getColor(android.R.color.holo_red_light));
                } else {
                    shopperName.setText(shoppingJob.getUser().fullName());
                    shopperName.setTextColor(context.getResources().getColor(R.color.text_body));
                }
            } else {
                shopperName.setText(R.string.shopper_not_started);
                shopperName.setTextColor(context.getResources().getColor(android.R.color.holo_red_light));
            }

            orderNumber.setText(shipment.getOrder().getNumber());

            if (shipment.getAddress() != null) {
                deliveryLocationName.setText(shipment.getAddress().getAddress1());
                deliveryLocationAddress1.setText(shipment.getAddress().getAddress2());
                deliveryLocationAddress2.setText(StringUtils.buildAddressCityInfo(shipment.getAddress()));

                if (shipment.getAddress().getAddressDetail() != null && !shipment.getAddress().getAddressDetail().trim()
                        .isEmpty()) {
                    deliveryLocationAddressDetails.setText(shipment.getAddress().getAddressDetail());

                    deliveryLocationAddressDetails.setVisibility(View.VISIBLE);
                    deliveryLocationDetailsText.setVisibility(View.VISIBLE);
                } else {
                    deliveryLocationAddressDetails.setVisibility(View.GONE);
                    deliveryLocationDetailsText.setVisibility(View.GONE);
                }

                if (shipment.getAddress().getAddressNumber() != null && !shipment.getAddress().getAddressNumber().trim()
                        .isEmpty()) {
                    deliveryLocationAddressNumber.setText(shipment.getAddress().getAddressNumber());
                    deliveryLocationAddressNumber.setVisibility(View.VISIBLE);
                }
                else {
                    deliveryLocationAddressNumber.setVisibility(View.GONE);
                }
            } else {
                deliveryLocationName.setText("Unknown");
                deliveryLocationAddress1.setText("Unknown");
                deliveryLocationAddress2.setText("Unknown");
            }

            callCustomerButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onClick(shipment);
                    }
                }
            });
        }
    }
}
