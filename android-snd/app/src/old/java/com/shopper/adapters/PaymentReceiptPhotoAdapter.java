package com.shopper.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.PhotoReceiptItem;
import com.happyfresh.snowflakes.hoverfly.models.ReceiptItem;
import com.happyfresh.snowflakes.hoverfly.models.Shipment;
import com.shopper.views.PaymentPhotoReceiptViewHolder;
import com.shopper.views.PaymentReceiptHeaderViewHolder;

public class PaymentReceiptPhotoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private Shipment mShipment;
    private ReceiptItem mReceipt;

    public PaymentReceiptPhotoAdapter(Context context,
            Shipment shipment,
            ReceiptItem receiptItem) {
        this.mContext = context;
        this.mShipment = shipment;
        this.mReceipt = receiptItem;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == ViewType.HEADER) {
            View view = inflater.inflate(R.layout.payment_receipt_detail_header, parent, false);
            return new PaymentReceiptHeaderViewHolder(view);
        } else {
            View view = inflater.inflate(R.layout.payment_receipt_detail_camera, parent, false);
            return new PaymentPhotoReceiptViewHolder(mContext, view);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return ViewType.HEADER;
        } else {
            return ViewType.CONTENT;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        if (viewType == ViewType.HEADER) {
            PaymentReceiptHeaderViewHolder viewHolder = (PaymentReceiptHeaderViewHolder) holder;
            viewHolder.showData(mShipment, mReceipt);
        } else {
            int receiptPhotoPosition = position - 1;
            PhotoReceiptItem photoReceiptItem = mReceipt.getPhotoReceiptItems().get(receiptPhotoPosition);
            photoReceiptItem.setPosition(receiptPhotoPosition);

            PaymentPhotoReceiptViewHolder viewHolder = (PaymentPhotoReceiptViewHolder) holder;
            viewHolder.setCameraOnClick(photoReceiptItem);
            viewHolder.showData(photoReceiptItem.getImagePath());

//            viewHolder.showData(photoReceiptItem);
        }
    }

    @Override
    public int getItemCount() {
        return mReceipt.getPhotoReceiptItems().size() + 1;
    }

    private static final class ViewType {
        public static final int HEADER = 0;
        public static final int CONTENT = 1;
    }
}