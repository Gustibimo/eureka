package com.shopper.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.Batch;
import com.happyfresh.snowflakes.hoverfly.models.Shipment;
import com.happyfresh.snowflakes.hoverfly.models.Slot;
import com.shopper.utils.StringUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by ifranseda on 11/17/14.
 */
public class ShopperListAdapter extends RecyclerView.Adapter<ShopperListAdapter.OrderViewHolder> {
    private static final SimpleDateFormat sDateFormat = new SimpleDateFormat("h:mm a");
    private Context mContext;
    private List<Batch> mItems = new ArrayList<>();
    private OnStartListener mOnStartListener;
    private boolean mIsLoading = false;

    public ShopperListAdapter(Context ctx, List<Batch> mShipmentList) {
        mContext = ctx;
        mItems = mShipmentList;

        sDateFormat.setTimeZone(TimeZone.getTimeZone("gmt"));
    }

    public void setOnStartClickListener(OnStartListener startListener) {
        mOnStartListener = startListener;
    }

    @Override
    public OrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_job_item, parent, false);
        return new OrderViewHolder(mContext, view);
    }

    @Override
    public void onBindViewHolder(final OrderViewHolder viewHolder, final int position) {
        final Batch batch = mItems.get(position);
        if (batch == null)
            return;

        viewHolder.startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnStartListener != null) {
                    mOnStartListener.onClick(position);
                }
            }
        });

        viewHolder.viewAllOrderLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewHolder.showListOrder();
            }
        });

        boolean showDeliveryTimeInOrderDetail = false;
        for(Shipment shipment : batch.shipments()) {
            if (!shipment.getSlot().getStartTime().equals(batch.getStartTime())) {
                showDeliveryTimeInOrderDetail = true;
                break;
            }
        }

        viewHolder.setData(batch, showDeliveryTimeInOrderDetail);
        viewHolder.setListOrder(batch, showDeliveryTimeInOrderDetail);
        viewHolder.showLoading(mIsLoading);

        if (viewHolder.parentItemOrderListLayout.getVisibility() == View.VISIBLE) {
            viewHolder.arrowIndicator.setImageResource(R.drawable.icon_dropup);
            viewHolder.parentItemOrderListLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public interface OnStartListener {
        void onClick(int position);
    }

    public void setLoading(boolean loading) {
        mIsLoading = loading;
    }

    public static class OrderViewHolder extends RecyclerView.ViewHolder {
        public Button startButton;

        public ImageView arrowIndicator;

        public TextView shoppingTime;
        public TextView orderSize;
        public TextView orderDeliveryTime;
        public TextView batchId;
        public TextView orderNumber;
        public TextView orderName;

        public RelativeLayout viewAllOrderLayout;
        public View itemView;
        public ViewGroup parentItemOrderListLayout;

        public LinearLayout shipDistanceLayout;
        public TextView shipDistanceValue;

        private Context mContext;

        public OrderViewHolder(Context ctx, View view) {
            super(view);

            startButton = (Button) view.findViewById(R.id.start_button);

            arrowIndicator = (ImageView) view.findViewById(R.id.arrow_indicator);
            parentItemOrderListLayout = (ViewGroup) view.findViewById(R.id.parent_list_order_number_layout_item);
            viewAllOrderLayout = (RelativeLayout) view.findViewById(R.id.view_all_orders_layout);
            itemView = LayoutInflater.from(ctx).inflate(R.layout.layout_order_item, parentItemOrderListLayout, false);

            shoppingTime = (TextView) view.findViewById(R.id.shopping_time);
            orderSize = (TextView) view.findViewById(R.id.order_size);
            orderDeliveryTime = (TextView) view.findViewById(R.id.order_delivery_time);
            batchId = (TextView) view.findViewById(R.id.batch_id);

            shipDistanceLayout = (LinearLayout) view.findViewById(R.id.ship_distance_layout);
            shipDistanceValue = (TextView) view.findViewById(R.id.ship_distance_value);

            mContext = ctx;
        }

        public void setData(Batch batch, boolean showDeliveryTimeInOrderDetail) {
            Slot slot = batch.getSlot();

            shoppingTime.setText(StringUtils.show1HourEarlierOfShoppingTime(batch.getStartTime(), batch.getEndTime()));
            orderSize.setText(String.valueOf(batch.shipments().size()));

            String deliveryTimeText = StringUtils.showDeliveryTime(slot.getStartTime(), slot.getEndTime());
            if (showDeliveryTimeInOrderDetail) {
                deliveryTimeText = mContext.getString(R.string.see_detail);
            }

            orderDeliveryTime.setText(deliveryTimeText);

            batchId.setText(String.valueOf(batch.getRemoteId()));
        }

        private void setListOrder(Batch batch, boolean showDeliveryTimeInOrderDetail) {
            if (itemView.getParent() != null) {
                ((ViewGroup) itemView.getParent()).removeAllViews();
                itemView = null;
            }

            int x = 1;
            for (Shipment shipment : batch.shipments()) {
                itemView = LayoutInflater.from(mContext).inflate(R.layout.layout_order_item, parentItemOrderListLayout, false);
                orderNumber = (TextView) itemView.findViewById(R.id.item_order_number);

                Slot slot = shipment.getSlot();
                if (showDeliveryTimeInOrderDetail) {
                    orderName.setText(StringUtils.showDeliveryTime(batch.getStartTime(), slot.getEndTime()));
                    orderName.setVisibility(View.VISIBLE);
                } else {
                    orderName.setVisibility(View.INVISIBLE);
                }
                orderNumber.setText(shipment.getOrder().getNumber());

                if (x % 2 == 0) {
                    itemView.setBackgroundColor(mContext.getResources().getColor(R.color.shady_background));
                }
                x++;

                parentItemOrderListLayout.addView(itemView);
            }

            String shipDistance = batch.getDisplayShipDistance();
            int visibility = org.apache.commons.lang3.StringUtils.isEmpty(shipDistance) ? View.GONE : View.VISIBLE;

            shipDistanceLayout.setVisibility(visibility);
            shipDistanceValue.setText(shipDistance);
        }

        public void showLoading(boolean loading) {
            if (loading) {
                startButton.setText(R.string.starting_job);
                startButton.setEnabled(false);
            } else {
                startButton.setText(R.string.start_job);
                startButton.setEnabled(true);
            }

            startButton.setTextColor(mContext.getResources().getColor(R.color.shopper_button_background));
        }

        public void showListOrder() {
            if (parentItemOrderListLayout.getVisibility() == View.GONE) {
                arrowIndicator.setImageResource(R.drawable.icon_dropdown);
                parentItemOrderListLayout.setVisibility(View.VISIBLE);
            } else {
                arrowIndicator.setImageResource(R.drawable.icon_dropup);
                parentItemOrderListLayout.setVisibility(View.GONE);
            }
        }
    }
}