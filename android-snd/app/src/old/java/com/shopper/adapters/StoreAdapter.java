package com.shopper.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.StockLocation;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.BindView;

/**
 * Created by rsavianto on 2/7/15.
 */
public class StoreAdapter extends RecyclerView.Adapter<StoreAdapter.ViewHolder> {
    private Context mContext;
    private List<StockLocation> mStockLocations = new ArrayList<StockLocation>();
    private List<StockLocation> mSelectedStockLocation = new ArrayList<StockLocation>();
    private boolean isShopper = false;
    private static CheckBox mLastCheckedBox = null;

    public List<StockLocation> getSelectedStockLocation() {
        return mSelectedStockLocation;
    }

    public void resetSelectedStockLocation() {
        mSelectedStockLocation.clear();
        if (mLastCheckedBox != null) {
            mLastCheckedBox.setChecked(false);
            mLastCheckedBox = null;
        }
    }

    public StoreAdapter(Context context, boolean isShopper) {
        this.mContext = context;
        this.isShopper = isShopper;
    }

    public void addAll(List<StockLocation> stockLocations) {
        mStockLocations.clear();
        notifyDataSetChanged();

        mStockLocations.addAll(stockLocations);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.list_store_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        final StockLocation stockLocation = mStockLocations.get(position);

        viewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mSelectedStockLocation.add(stockLocation);
                } else {
                    mSelectedStockLocation.remove(stockLocation);
                }
            }
        });

        viewHolder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckBox checkBox = (CheckBox) v;

                if (checkBox.isChecked()) {
                    if (mLastCheckedBox != null) {
                        mLastCheckedBox.setChecked(false);
                    }

                    mLastCheckedBox = checkBox;
                }
            }
        });

        viewHolder.itemLocation.setText(stockLocation.getName());

        if (isShopper) {
            viewHolder.itemCluster.setVisibility(View.GONE);
        } else {
            if (stockLocation.getCluster() != null && !TextUtils.isEmpty(stockLocation.getCluster().getName())) {
                viewHolder.itemCluster.setText(stockLocation.getCluster().getName());
                viewHolder.itemCluster.setVisibility(View.VISIBLE);
            } else {
                viewHolder.itemCluster.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mStockLocations.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.location)
        TextView itemLocation;

        @BindView(R.id.cluster)
        TextView itemCluster;

        @BindView(R.id.checkbox)
        CheckBox checkBox;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (checkBox.isChecked()) {
                checkBox.setChecked(false);
            } else {
                if (mLastCheckedBox != null) {
                    mLastCheckedBox.setChecked(false);
                }

                checkBox.setChecked(true);
            }

            mLastCheckedBox = checkBox;
        }
    }

}
