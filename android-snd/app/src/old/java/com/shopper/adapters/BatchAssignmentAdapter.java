package com.shopper.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.Shipment;
import com.shopper.utils.StringUtils;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.BindView;

/**
 * Created by ifranseda on 11/18/15.
 */
public class BatchAssignmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context mContext;
    private List<Shipment> mShipments;

    public BatchAssignmentAdapter(Context context, List<Shipment> shipments) {
        mContext = context;
        mShipments = shipments;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(mContext).inflate(R.layout.layout_shipment_item, parent, false);
        return new BatchAssignmentShipmentViewHolder(mContext, view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Shipment shipment = mShipments.get(position);

        BatchAssignmentShipmentViewHolder viewHolder = (BatchAssignmentShipmentViewHolder) holder;
        viewHolder.setData(shipment);
    }

    @Override
    public int getItemCount() {
        return mShipments.size();
    }

    public static class BatchAssignmentShipmentViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.order_color_indicator)
        View orderIndicator;

        @BindView(R.id.order_number)
        TextView orderNumber;

        @BindView(R.id.customer_name)
        TextView customerName;

        @BindView(R.id.delivery_location_name)
        TextView deliveryLocationName;

        @BindView(R.id.delivery_location_address1)
        TextView deliveryLocationAddress1;

        @BindView(R.id.delivery_location_address2)
        TextView deliveryLocationAddress2;

        Context context;

        public BatchAssignmentShipmentViewHolder(Context ctx, View itemView) {
            super(itemView);
            context = ctx;
            ButterKnife.bind(this, itemView);
        }

        public void setData(Shipment shipment) {
            orderIndicator.setBackgroundColor(shipment.getColorResource());
            orderNumber.setText(shipment.getOrder().getNumber());
            customerName.setText(shipment.getRecipientName());

            if (shipment.getAddress() != null) {
                deliveryLocationName.setText(shipment.getAddress().getAddress1());
                deliveryLocationAddress1.setText(shipment.getAddress().getAddress2());
                deliveryLocationAddress2.setText(StringUtils.buildAddressCityInfo(shipment.getAddress()));
            } else {
                deliveryLocationName.setText("Unknown");
                deliveryLocationAddress1.setText("Unknown");
                deliveryLocationAddress2.setText("Unknown");
            }
        }
    }
}
