package com.shopper.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.LineItem;
import com.happyfresh.snowflakes.hoverfly.models.RejectionItem;
import com.happyfresh.snowflakes.hoverfly.models.SpreeImage;
import com.happyfresh.snowflakes.hoverfly.models.Variant;
import com.shopper.interfaces.OnRejectedListener;
import com.shopper.interfaces.RejectionListAction;
import com.shopper.views.AllReplacementViewHolder;
import com.shopper.views.CustomerReplacementViewHolder;
import com.shopper.views.DeliveryItemViewHolder;
import com.shopper.views.ShopperReplacementViewHolder;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ifranseda on 4/23/15.
 */
public class DeliveryItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements RejectionListAction {
    public static final class ViewType {
        public static final int NO_REPLACEMENT = 0;

        public static final int WITH_CUSTOMER_REPLACEMENT = 1;

        public static final int WITH_ALL_REPLACEMENT = 2;

        public static final int WITH_SHOPPER_REPLACEMENT = 3;

        public static final int SHOPPING_BAG = 4;
    }

    private final Context mContext;

    private List<LineItem> mItems;

    private OnRejectedListener mOnRejectedListener;

    private ArrayList<Long> shipmentWithShoppingBag = new ArrayList<>();
    private int totalShoppingBag = 0;

    public DeliveryItemAdapter(Context context, List<LineItem> lineItems) {
        mContext = context;
        mItems = lineItems;
    }

    public void setOnRejectedListener(OnRejectedListener listener) {
        mOnRejectedListener = listener;
    }

    public void checkShoppingBag(){
        for (LineItem item : mItems){
            if(item.getShipment().getOrder().isEligibleForShoppingBag() && !shipmentWithShoppingBag.contains(item.getShipment().getRemoteId())){
                shipmentWithShoppingBag.add(item.getShipment().getRemoteId());
                totalShoppingBag++;
            }
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        switch (viewType) {
            case ViewType.WITH_SHOPPER_REPLACEMENT: {
                view = LayoutInflater.from(mContext)
                        .inflate(R.layout.layout_delivery_item_with_shopper_replacement, parent, false);
                ShopperReplacementViewHolder viewHolder = new ShopperReplacementViewHolder(view, mContext, this);
                return viewHolder;
            }

            case ViewType.WITH_CUSTOMER_REPLACEMENT: {
                view = LayoutInflater.from(mContext)
                        .inflate(R.layout.layout_delivery_item_with_customer_replacement, parent, false);
                CustomerReplacementViewHolder viewHolder = new CustomerReplacementViewHolder(view, mContext, this);
                return viewHolder;
            }

            case ViewType.WITH_ALL_REPLACEMENT: {
                view = LayoutInflater.from(mContext)
                        .inflate(R.layout.layout_delivery_item_with_all_replacement, parent, false);
                AllReplacementViewHolder viewHolder = new AllReplacementViewHolder(view, mContext, this);
                return viewHolder;
            }

            case ViewType.SHOPPING_BAG: {
                view = LayoutInflater.from(mContext).inflate(R.layout.layout_delivery_item, parent, false);
                DeliveryItemViewHolder viewHolder = new DeliveryItemViewHolder(view, mContext, this);
                return viewHolder;
            }

            default:
            case ViewType.NO_REPLACEMENT: {
                view = LayoutInflater.from(mContext).inflate(R.layout.layout_delivery_item, parent, false);
                DeliveryItemViewHolder viewHolder = new DeliveryItemViewHolder(view, mContext, this);
                return viewHolder;
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position >= getItemCount() - totalShoppingBag) {
            return ViewType.SHOPPING_BAG;
        } else{
            LineItem item = mItems.get(position);
            if (item.getTotalReplacedShopper() > 0 && item.getTotalReplacedCustomer() == 0) {
                return ViewType.WITH_SHOPPER_REPLACEMENT;
            } else if (item.getTotalReplacedShopper() == 0 && item.getTotalReplacedCustomer() > 0) {
                return ViewType.WITH_CUSTOMER_REPLACEMENT;
            } else if (item.getTotalReplacedShopper() > 0 && item.getTotalReplacedCustomer() > 0) {
                return ViewType.WITH_ALL_REPLACEMENT;
            }
            else {
                return ViewType.NO_REPLACEMENT;
            }
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size() + totalShoppingBag;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int position) {
        LineItem item = new LineItem();
        if (position < getItemCount() - totalShoppingBag) {
            item = mItems.get(position);
        }else{
            item = mItems.get(0);
        }

        switch (getItemViewType(position)) {
            case ViewType.WITH_SHOPPER_REPLACEMENT:
                ((ShopperReplacementViewHolder) viewHolder).showData(item, position);
                break;

            case ViewType.WITH_CUSTOMER_REPLACEMENT:
                ((CustomerReplacementViewHolder) viewHolder).showData(item, position);
                break;

            case ViewType.WITH_ALL_REPLACEMENT:
                ((AllReplacementViewHolder) viewHolder).showData(item, position);
                break;
            case ViewType.SHOPPING_BAG:
                ((DeliveryItemViewHolder) viewHolder).showShoppingBag(item.getShipment(), position);
                break;

            default:
            case ViewType.NO_REPLACEMENT:
                ((DeliveryItemViewHolder) viewHolder).showData(item, position);
                break;
        }
    }

    @Override
    public void onReject(int position, RejectionType type, int quantity, int reasonIndex, String reason) {
        final LineItem item = mItems.get(position);

        RejectionItem rejection = new RejectionItem();
        Long variantId = item.getVariant().getRemoteId();
        if (type == RejectionType.REPLACEMENT_CUSTOMER) {
            variantId = item.getReplacementCustomer().getRemoteId();
        } else if (type == RejectionType.REPLACEMENT_SHOPPER) {
            variantId = item.getReplacementShopper().getRemoteId();
        }

        RejectionItem localRejection = RejectionItem.findByLineItemAndVariantId(item, variantId);
        if (localRejection != null) {
            localRejection.delete();
            rejection = new RejectionItem();
        }

        TypedArray reasonArray = mContext.getResources().obtainTypedArray(R.array.rejection_reason);
        String reasonKey = reason;
        try {
            int resId = reasonArray.getResourceId(reasonIndex, -1);
            reasonKey = mContext.getResources().getResourceEntryName(resId);
        } catch (Exception e) {
        }

        rejection.setItem(item);
        rejection.setVariantId(variantId);
        rejection.setReason(reasonKey);
        rejection.setQuantity(quantity);
        rejection.save();
//        rejection.persist();

        notifyDataSetChanged();

        if (mOnRejectedListener != null) {
            mOnRejectedListener.onRejected(position, type, quantity, reason);
        }
    }

    @Override
    public void onDiscard(int position, RejectionType type) {
        final LineItem item = mItems.get(position);

        Long variantId = item.getVariant().getRemoteId();
        if (type == RejectionType.REPLACEMENT_CUSTOMER) {
            variantId = item.getReplacementCustomer().getRemoteId();
        } else if (type == RejectionType.REPLACEMENT_SHOPPER) {
            variantId = item.getReplacementShopper().getRemoteId();
        }

        int previousQuantity = 0;
        RejectionItem localRejection = RejectionItem.findByLineItemAndVariantId(item, variantId);
        if (localRejection != null) {
            previousQuantity = localRejection.getQuantity();
            localRejection.delete();

            localRejection = new RejectionItem();
            localRejection.setItem(item);
            localRejection.setQuantity(0);
            localRejection.setVariantId(variantId);
            localRejection.save();
//            localRejection.persist();
        }

        notifyDataSetChanged();

        if (mOnRejectedListener != null) {
            mOnRejectedListener.onDiscarded(position, type, previousQuantity);
        }
    }

    public void showRejectionDialog(final int position, final RejectionType type, final int amount,
            final Variant variant, String price) {
        View contentDialog = LayoutInflater.from(mContext).inflate(R.layout.layout_rejection_dialog_view, null);

        Drawable bgDrawable = ContextCompat.getDrawable(mContext, R.drawable.abc_spinner_mtrl_am_alpha);
        bgDrawable.setColorFilter(ContextCompat.getColor(mContext, R.color.item_background), PorterDuff.Mode.SRC_ATOP);

        // Create dialog content views
        TextView itemTotal = (TextView) contentDialog.findViewById(R.id.item_total);
        ImageView itemImage = (ImageView) contentDialog.findViewById(R.id.item_image);
        TextView itemName = (TextView) contentDialog.findViewById(R.id.item_name);
        TextView itemPrice = (TextView) contentDialog.findViewById(R.id.item_price);

        View shopperNoteContainer = contentDialog.findViewById(R.id.shopper_notes_container);
        ImageView shopperNotesFulfilled = (ImageView) contentDialog.findViewById(R.id.shopper_note_icon);

        LineItem item = mItems.get(position);
        if (type == RejectionType.ORIGINAL) {
            if (StringUtils.isEmpty(item.getNotes())) {
                shopperNoteContainer.setVisibility(View.GONE);
            } else {
                shopperNoteContainer.setVisibility(View.VISIBLE);
            }
        } else {
            shopperNoteContainer.setVisibility(View.GONE);
        }

        if (item.isShopperNotesFulfilled()) {
            shopperNotesFulfilled.setImageResource(R.drawable.icon_shopper_note_fulfilled);
        } else {
            shopperNotesFulfilled.setImageResource(R.drawable.icon_shopper_note_not_fulfilled);
        }

        final EditText rejectAmount = (EditText) contentDialog.findViewById(R.id.reject_amount);
        final Spinner rejectReason = (Spinner) contentDialog.findViewById(R.id.reject_reason);
        rejectReason.setBackground(bgDrawable);

        List<String> reasons = Arrays.asList(mContext.getResources().getStringArray(R.array.rejection_reason));
        if (StringUtils.isEmpty(item.getNotes()) || type != RejectionType.ORIGINAL) {
            int index = reasons.size() - 1;
            reasons = reasons.subList(0, index);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter(mContext, android.R.layout.simple_spinner_dropdown_item,
                reasons);
        rejectReason.setAdapter(adapter);

        // Set values
        itemTotal.setText(String.valueOf(amount));
        itemName.setText(variant.getName());
        itemPrice.setText(price);

        List<SpreeImage> images = variant.images();
        String productUrl = null;
        if (images.size() > 0) {
            productUrl = images.get(0).getProductUrl();
        }

        Picasso.with(mContext).load(productUrl).error(R.drawable.default_product)
                .placeholder(R.drawable.default_product).into(itemImage);

        final MaterialDialog.Builder dialogBuilder = new MaterialDialog.Builder(mContext)
                .title(R.string.rejection_dialog_title).customView(contentDialog, false)
                .positiveText(R.string.alert_button_ok).negativeText(R.string.alert_button_cancel)
                .backgroundColor(Color.WHITE).cancelable(false).autoDismiss(false)
                .showListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialogInterface) {
                        int quantity = -1;
                        String reason = null;

                        LineItem item = mItems.get(position);
                        if (item.rejectionItems().size() > 0) {
                            for (RejectionItem rejected : item.rejectionItems()) {
                                if (rejected.getVariantId().equals(variant.getRemoteId())) {
                                    quantity = rejected.getQuantity();
                                    reason = rejected.getReason();
                                    break;
                                }
                            }
                        }

                        if (reason != null) {
                            int resId = mContext.getResources()
                                    .getIdentifier(reason, "string", mContext.getPackageName());
                            String reasonStr = mContext.getString(resId);

                            String[] reasons = mContext.getResources().getStringArray(R.array.rejection_reason);
                            int index = Arrays.asList(reasons).indexOf(reasonStr);
                            rejectReason.setSelection(index, true);
                        }

                        if (quantity <= 0) {
                            rejectAmount.setText("");
                            rejectReason.setSelection(0);
                        } else {
                            String value = String.valueOf(quantity);
                            rejectAmount.setText(value);
                            rejectAmount.setSelection(value.length());
                        }
                    }
                }).callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        int quantity = 0;
                        if (!TextUtils.isEmpty(rejectAmount.getText())) {
                            quantity = Integer.valueOf(rejectAmount.getText().toString());
                        }

                        int index = rejectReason.getSelectedItemPosition();
                        String reason = rejectReason.getSelectedItem().toString();

                        if (quantity > 0 && quantity <= amount) {
                            onReject(position, type, quantity, index, reason);

                            dialog.dismiss();
                        } else {
                            int error = R.string.rejection_number_missed;
                            if (quantity > amount) {
                                error = R.string.rejection_number_exceeded;
                            }

                            rejectAmount.setError(mContext.getString(error));
                        }
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        dialog.dismiss();
                    }
                });

        dialogBuilder.show();
    }
}
