package com.shopper.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.ProductSampleItem;
import com.shopper.views.ProductSampleItemViewHolder;

import java.util.List;

/**
 * Created by kharda on 9/7/16.
 */
public class ProductSampleListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<ProductSampleItem> mSampleItems;

    public ProductSampleListAdapter(Context context, List<ProductSampleItem> items) {
        mContext = context;
        mSampleItems = items;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_shopping_item, parent, false);
        return new ProductSampleItemViewHolder(mContext, view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ProductSampleItem item = mSampleItems.get(position);
        ProductSampleItemViewHolder viewHolder = (ProductSampleItemViewHolder) holder;

        ProductSampleItem prevItem = null;
        if (position > 0) {
            prevItem = mSampleItems.get(position - 1);
        }

        if (prevItem != null && item != null) {
            if (prevItem.getTaxonName() != null && item.getTaxonName() != null) {
                if (prevItem.getTaxonName().equals(item.getTaxonName())) {
                    viewHolder.setCategory(null);
                } else {
                    viewHolder.setCategory(item.getTaxonName());
                }
            } else {
                viewHolder.setCategory(null);
            }
        } else {
            if (item != null) {
                viewHolder.setCategory(item.getTaxonName());
            } else {
                viewHolder.setCategory(null);
            }
        }
        viewHolder.showItemDetail(item);
    }

    @Override
    public int getItemCount() {
        return mSampleItems.size();
    }
}
