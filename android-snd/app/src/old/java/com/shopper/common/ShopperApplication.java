package com.shopper.common;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.Geocoder;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.PowerManager;
import android.os.StrictMode;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.appboy.AppboyLifecycleCallbackListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.GsonBuilder;
import com.happyfresh.fulfillment.App;
import com.happyfresh.fulfillment.R;
import com.happyfresh.happychat.ChatUtils;
import com.happyfresh.happychat.ChooseMediaDialog;
import com.happyfresh.happydialog.HappyDialogButton;
import com.happyfresh.snowflakes.hoverfly.Sprinkles;
import com.happyfresh.snowflakes.hoverfly.config.Config;
import com.happyfresh.snowflakes.hoverfly.models.Batch;
import com.happyfresh.snowflakes.hoverfly.models.Shipment;
import com.happyfresh.snowflakes.hoverfly.models.User;
import com.happyfresh.snowflakes.hoverfly.services.BatchService;
import com.happyfresh.snowflakes.hoverfly.stores.UserStore;
import com.happyfresh.snowflakes.hoverfly.utils.FileUtils;
import com.happyfresh.snowflakes.hoverfly.utils.PermissionUtils;
import com.segment.analytics.Analytics;
import com.segment.analytics.android.integrations.amplitude.AmplitudeIntegration;
import com.segment.analytics.android.integrations.appboy.AppboyIntegration;
import com.sendbird.android.SendBird;
import com.shopper.listeners.NetworkQueueChangedListener;
import com.shopper.listeners.NetworkStateListener;
import com.shopper.listeners.UnauthorizedCallbackListener;
import com.shopper.managers.AppManager;
import com.shopper.managers.BatchManager;
import com.shopper.managers.CashHandoverManager;
import com.shopper.managers.JobManager;
import com.shopper.managers.ProductManager;
import com.shopper.managers.ProductSampleManager;
import com.shopper.managers.ShipmentManager;
import com.shopper.managers.tracking.TrackingUtils;
import com.shopper.tasks.NetworkTaskQueue;
import com.shopper.utils.LogUtils;
import com.shopper.utils.SharedPrefUtils;
import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by ifranseda on 11/13/14.
 */
public class ShopperApplication extends Application {

    private static Context mContext;

    private static ShopperApplication sInstance;

    @Deprecated
    private Batch currentBatch;

    private String currentStatus = "";

    private ShipmentManager mShipmentManager;

    private JobManager mJobManager;

    private ProductManager mProductManager;

    private CashHandoverManager mHandoverManager;

    private BatchManager mBatchManager;

    private AppManager mAppManager;

    private ProductSampleManager mProductSampleManager;

    private List<NetworkStateListener> networkStateListeners = new ArrayList<>();

    private List<NetworkQueueChangedListener> networkQueueListener = new ArrayList<>();

    private PowerManager.WakeLock wakeLock;

    private Bus bus;

    private NetworkTaskQueue queue;

    private String mContentLanguage;

    private UnauthorizedCallbackListener mUnauthorizedCallback = new UnauthorizedCallbackListener() {
        @Override
        public void onUnauthorized() {
            //set CurrentStatus to ""
            currentStatus = "";

            SharedPrefUtils.clearAll(mContext);
            LogUtils.logEvent("User Logged out");

            clearQueue();
            clearContentLanguage();

            UserStore userStore = Sprinkles.stores(UserStore.class);
            userStore.reset();

            new Thread(() -> {
                try {
                    FirebaseInstanceId.getInstance().deleteToken(Config.getBrazeSenderId(), "FCM");
                    FirebaseInstanceId.getInstance().deleteInstanceId();
                } catch (IOException e) {
                    LogUtils.LOG("Exception while delete Firebase token");
                }
            }).start();
        }
    };

    public static Context getContext() {
        return App.Companion.getContext();
    }

    public static ShopperApplication getInstance() {
        return sInstance;
    }

    public static void onCreate(Context context) {
        mContext = context;

        if (sInstance == null) {
            sInstance = new ShopperApplication();
        }

        sInstance.bus = new Bus(ThreadEnforcer.MAIN);
        sInstance.queue = NetworkTaskQueue.create(mContext, new GsonBuilder().create());

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        sInstance.initAnalytics();

        sInstance.initSendBird();

        sInstance.initHappyChat();
    }

    public UnauthorizedCallbackListener getUnauthorizedCallback() {
        return mUnauthorizedCallback;
    }

    public Bus getBus() {
        if (this.bus == null) {
            bus = new Bus(ThreadEnforcer.MAIN);
        }

        return this.bus;
    }

    public NetworkTaskQueue getTaskQueue() {
        if (queue == null) {
            queue = NetworkTaskQueue.create(getContext(), new GsonBuilder().create());
        }

        return this.queue;
    }

    public void clearQueue() {
        try {
            if (this.queue.size() > 0) {
                this.queue.remove();
            }
        } catch (Exception e) {
        }

        if (FileUtils.deleteQueueFile(this)) {
            LogUtils.logEvent("Queue file deleted");
        }
        else {
            LogUtils.logEvent("Queue file cannot be deleted");
        }
    }

    public synchronized Shipment getCurrentShipment() {
        if (getBatch() != null) {
            return getBatch().activeShipment();
        }
        else {
            return null;
        }
    }

    public synchronized Batch getBatch() {
        currentBatch = Batch.Companion.currentBatch();
        return currentBatch;
    }

    public void setBatch(Batch batch) {
        BatchService batchService = Sprinkles.service(BatchService.class);
        if (batchService != null) {
            batchService.setActiveBatch(batch);
        }
    }

    public ShipmentManager getShipmentManager() {
        if (mShipmentManager == null) {
            mShipmentManager = new ShipmentManager(this);
        }
        return mShipmentManager;
    }

    public String getVersion() {
        try {
            PackageInfo packageInfo = getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0);
            String versionName = packageInfo.versionName;
            int versionCode = packageInfo.versionCode;

            String versionStr = String.format("%s (%s)", versionName, versionCode);
            return getContext().getString(R.string.version, versionStr);
        } catch (PackageManager.NameNotFoundException e) {
            return "Unknown";
        }
    }

    public int getBuildNumber() {
        try {
            PackageInfo packageInfo = getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0);
            int versionCode = packageInfo.versionCode;
            return versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            return -1;
        }
    }

    public JobManager getJobManager() {
        if (mJobManager == null) {
            mJobManager = new JobManager(this);
        }
        return mJobManager;
    }

    public ProductManager getProductManager() {
        if (mProductManager == null) {
            mProductManager = new ProductManager(this);
        }
        return mProductManager;
    }

    public CashHandoverManager getHandoverManager() {
        mHandoverManager = new CashHandoverManager(this);
        return mHandoverManager;
    }

    public BatchManager getBatchManager() {
        if (mBatchManager == null) {
            mBatchManager = new BatchManager(this);
        }
        return mBatchManager;
    }

    public AppManager getAppManager() {
        mAppManager = new AppManager(this);
        return mAppManager;
    }

    public ProductSampleManager getProductSampleManager() {
        if (mProductSampleManager == null) {
            mProductSampleManager = new ProductSampleManager(this);
        }
        return mProductSampleManager;
    }

    public void registerNetworkStateListener(NetworkStateListener listener) {
        if (!networkStateListeners.contains(listener)) {
            networkStateListeners.add(listener);
        }
    }

    public void unregisterNetworkStateListener(NetworkStateListener listener) {
        if (networkStateListeners.contains(listener)) {
            networkStateListeners.remove(listener);
        }
    }

    public void raiseNetworkState(NetworkInfo.State state) {
        if (state == NetworkInfo.State.CONNECTED) {
            getTaskQueue().startService();
        }

        for (NetworkStateListener listener : networkStateListeners) {
            listener.onStateChanged(state);
        }
    }

    public void registerQueueChangedListener(NetworkQueueChangedListener listener) {
        if (!networkQueueListener.contains(listener)) {
            networkQueueListener.add(listener);
        }
    }

    public void unregisterQueueChangedListener(NetworkQueueChangedListener listener) {
        if (!networkQueueListener.contains(listener)) {
            networkQueueListener.remove(listener);
        }
    }

    public void raiseQueueChanged(int size) {
        for (NetworkQueueChangedListener listener : networkQueueListener) {
            listener.onUpdated(size);
        }
    }

    @SuppressLint("MissingPermission")
    public String getCountryCode() {
        Geocoder geocoder = new Geocoder(getContext());
        android.location.LocationManager locationManager = (android.location.LocationManager) getContext()
                .getSystemService(LOCATION_SERVICE);

        android.location.Location location = null;
        List<String> providers = locationManager.getAllProviders();
        for (String provider : providers) {
            if (PermissionUtils.accessLocation(getContext())) {
                location = locationManager.getLastKnownLocation(provider);
            }

            if (location != null) {
                break;
            }
        }

        if (location != null) {
            try {
                List<android.location.Address> addresses = geocoder
                        .getFromLocation(location.getLatitude(), location.getLongitude(), 100);
                for (android.location.Address address : addresses) {
                    if (address != null && address.getCountryCode() != null) {
                        return address.getCountryCode().toUpperCase();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        TelephonyManager tm = (TelephonyManager) getContext().getSystemService(this.TELEPHONY_SERVICE);
        if (!TextUtils.isEmpty(tm.getNetworkCountryIso())) {
            return tm.getNetworkCountryIso().toUpperCase();
        }

        return mContext.getResources().getConfiguration().locale.getCountry();
    }

    public String getContentLanguage() {
        Locale locale = App.Companion.getContext().getResources().getConfiguration().locale;

        String lang = locale.getLanguage();
        if (locale.getCountry().equals("ID")) {
            lang = "id";
        }

        mContentLanguage = lang;
        return mContentLanguage;
    }

    public void clearContentLanguage() {
        mContentLanguage = null;
    }

    @SuppressLint("MissingPermission")
    public void callPhone(String number) {
        if (number == null) {
            return;
        }

        String uri = "tel:" + number;

        Intent intent = new Intent(Intent.ACTION_CALL);
        // Calling startActivity() from outside of an Activity  context requires the FLAG_ACTIVITY_NEW_TASK flag
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setData(Uri.parse(uri));

        if (PermissionUtils.callPhone(getContext())) {
            getContext().startActivity(intent);
        }
    }

    public boolean hasPendingQueue() {
        if (getTaskQueue().size() > 0) {
            getTaskQueue().startService();
            return true;
        }
        else {
            return false;
        }
    }

    public Locale getLocaleByCountryCode(String countryCode) {
        // Indonesia new Locale("id", "id")
        // Malaysia new Locale("ms", "my")
        // Thailand new Locale("th", "th")

        Locale locale = null;
        if (countryCode != null) {
            if (countryCode.equalsIgnoreCase("my")) {
                locale = new Locale("ms", "my");
            }
            else {
                locale = new Locale(countryCode, countryCode);
            }
        }

        String prefCountry = SharedPrefUtils
                .getString(ShopperApplication.getContext(), Constant.STOCK_LOCATION_COUNTRY_ISO);
        if (prefCountry != null) {
            locale = new Locale(prefCountry, prefCountry);
        }

        if (locale == null) {
            locale = Locale.getDefault();
        }

        return locale;
    }

    public User getCurrentUser() {
        try {
            Long userId = Sprinkles.stores(UserStore.class).getUserId();
            return User.findById(userId);
        } catch (Exception e) {
            return null;
        }
    }

    public String getStatus() {
        synchronized(currentStatus) {
            return currentStatus;
        }
    }

    public void setStatus(String status) {
        if (status == null) {
            status = "";
        }

        synchronized(currentStatus) {
            currentStatus = status;
        }
    }

    private void initAnalytics() {
        Analytics analytics = new Analytics.Builder(mContext, Config.getSegmentWriteKey())
                .use(AmplitudeIntegration.FACTORY).use(AppboyIntegration.FACTORY).flushQueueSize(1).build();

        analytics.onIntegrationReady(AmplitudeIntegration.FACTORY.key(),
                instance -> TrackingUtils.identify(getContext(), getCurrentUser()));

        analytics.onIntegrationReady(AppboyIntegration.FACTORY.key(),
                instance -> registerActivityLifecycleCallbacks(new AppboyLifecycleCallbackListener()));

        Analytics.setSingletonInstance(analytics);
    }

    private void initSendBird() {
        SendBird.init(Config.getSendbirdAppId(), mContext);
    }

    private void initHappyChat() {
        ChatUtils.setChooseMediaDialog(new ChooseMediaDialog() {
            @Override
            public String getTitle() {
                return getContext().getString(R.string.select_image);
            }

            @Override
            public String getMessage() {
                return getContext().getString(R.string.where_do_you_prefer_select_image_from);
            }

            @Override
            public String getPositiveButtonText() {
                return getContext().getString(R.string.camera);
            }

            @Override
            public HappyDialogButton.Type getPositiveButtonType() {
                return new HappyDialogButton.Type() {
                    @Override
                    public int getId() {
                        return 0;
                    }

                    @NotNull
                    @Override
                    public Drawable getBackground() {
                        return ContextCompat.getDrawable(getContext(), R.drawable.button_positive_selector);
                    }

                    @Override
                    public int getTextColor() {
                        return ContextCompat.getColor(getContext(), android.R.color.white);
                    }
                };
            }

            @Override
            public String getDefaultButtonText() {
                return getContext().getString(R.string.galery);
            }

            @Override
            public HappyDialogButton.Type getDefaultButtonType() {
                return new HappyDialogButton.Type() {
                    @Override
                    public int getId() {
                        return 1;
                    }

                    @NotNull
                    @Override
                    public Drawable getBackground() {
                        return ContextCompat.getDrawable(getContext(), R.drawable.button_default_selector);
                    }

                    @Override
                    public int getTextColor() {
                        return ContextCompat.getColor(getContext(), R.color.hf_text_primary);
                    }
                };
            }
        });
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public PowerManager.WakeLock getWakeLock() {
        if (wakeLock == null) {
            PowerManager pm = (PowerManager) getContext().getSystemService(Context.POWER_SERVICE);
            wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "wake_lock");
            wakeLock.acquire();
        }
        return wakeLock;
    }

    public void stopWakeLock() {
        if (wakeLock != null) {
            if (wakeLock.isHeld()) {
                wakeLock.release();
            }
        }
    }
}
