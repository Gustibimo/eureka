package com.shopper.common;

/**
 * Created by rsavianto on 1/15/15.
 */
public class Constant {
    public static final String USER_ACCESS_TOKEN_KEY = "USER_ACCESS_TOKEN_KEY";
    public static final String USER_ID_KEY = "PREFS.USER_ID";

    public static final String USER_NAME_KEY = "PREFS.USER_NAME";
    public static final String USER_EMAIL_KEY = "PREFS.USER_EMAIL";

    @Deprecated
    public static final String IS_SHOPPER_KEY = "IS_SHOPPER_KEY";
    @Deprecated
    public static final String IS_DRIVER_KEY = "IS_DRIVER_KEY";

    public static final String USER_NORMAL_ROLE = "USER_NORMAL_ROLE";
    public static final String USER_TYPE_KEY = "PREFS.USER_TYPE";

    public static final String STORE_NAME_KEY = "STORE_NAME_KEY";
    public static final String STORE_LAT_KEY = "STORE_LAT_KEY";
    public static final String STORE_LON_KEY = "STORE_LON_KEY";

    public static final String ONE_CLICK_PAY_RETRY_COUNT = "ONE_CLICK_PAY_RETRY_COUNT";

//    public static final String SND_STATUS_PRICE_CHANGE_LIMIT = "SND_STATUS_PRICE_CHANGE_LIMIT";
//    public static final String SND_STATUS_LAST_MODIFIED = "SND_STATUS_LAST_MODIFIED";
//
//    public static final String SND_STATUS_REQUIRE_AGE_VERIFICATION = "SND_STATUS_REQUIRE_AGE_VERIFICATION";
//    public static final String SND_STATUS_AGE_VERIFICATION_WORDING = "SND_STATUS_AGE_VERIFICATION_WORDING";
//    public static final String SND_STATUS_AGE_VERIFICATION_WORDING_ENGLISH = "SND_STATUS_AGE_VERIFICATION_WORDING_ENGLISH";
//    public static final String SND_STATUS_RESTRICTED_CATEGORIES = "SND_STATUS_RESTRICTED_CATEGORIES";
//
//    public static final String PRESENCE_STATUS_KEY = "PRESENCE_STATUS_KEY";
//    public static final String REQUIRE_RECEIPT_NUMBER_KEY = "REQUIRE_RECEIPT_NUMBER_KEY";

//    public static final String PRICE_CHANGE_LIMIT = "PRICE_CHANGE_LIMIT";
//    public static final String SND_STATUS_LAST_MODIFIED = "SND_STATUS_LAST_MODIFIED";
//    public static final String PRESENCE_STATUS_KEY = "PRESENCE_STATUS_KEY";
//    public static final String REQUIRE_RECEIPT_NUMBER_KEY = "REQUIRE_RECEIPT_NUMBER_KEY";

    public static final String SHOPPER_PREP_NOTIFICATION = "SHOPPER_PREP_NOTIFICATION";
    public static final String DRIVER_PREP_NOTIFICATION = "DRIVER_PREP_NOTIFICATION";

    public static final String IS_STOCK_LOCATION_SELECTED = "IS_STOCK_LOCATION_SELECTED";
    public static final String STOCK_LOCATION_COUNTRY_ISO = "STOCK_LOCATION_COUNTRY_ISO";

    public static final String BATCH_ID = "BATCH_ID";
    public static final String EXTRAS_BATCH_ID = "EXTRAS_BATCH_ID";
    public static final String SHIPMENT_ID = "SHIPMENT_ID";
    public static final String LINE_ITEM_ID = "LINE_ITEM_ID";
    public static final String STOCK_LOCATION_ID = "STOCK_LOCATION_ID";
    public static final String STOCK_LOCATION_NAME = "STOCK_LOCATION_NAME";
    public static final String SELECTED_CLUSTER = "SELECTED_CLUSTER";
    public static final String SELECTED_SHOPPING_STORE = "SELECTED_SHOPPING_STORE";
    public static final String TOTAL_INVOICED_AMOUNT = "TOTAL_INVOICED_AMOUNT";

    public static final String CASH_HANDOVER_AMOUNT = "CASH_HANDOVER_AMOUNT";
    public static final String CASH_HANDOVER_CURRENCY = "CASH_HANDOVER_CURRENCY";

    public static final String CashOnDeliveryMethod = "Spree::PaymentMethod::Check";
    public static final String InvalidStateError = "Spree::Api::InvalidStateError";
    public static final String AlreadyFinishedException = "Spree::Job::AlreadyFinishedException";
    public static final String AlreadyStartedException = "Spree::Job::AlreadyStartedException";
    public static final String AlreadyCancelledException = "Spree::Job::AlreadyCancelledException";
    public static final String OrderCanceledException = "Spree::Job::OrderCanceledException";
    public static final String PaymentAlreadyCompleted = "payment_already_completed";
    public static final String UserIsClockedOutException = "Spree::Snd::UserIsClockedOutException";

    public static final String QUEUE_DIRNAME = "queue";
    public static final String FAILED_QUEUE_DIRNAME = "failed";
    public static final String QUEUE_FILENAME = "network_task_queue";

    public static final String REMOTE_NOTIFICATION_KEY = "com.shopper.REMOTE_NOTIFICATION";

    public static final int MENU_PAUSE = 1;
    public static final int MENU_CLOCK_OUT = 2;

    public static final String STORE_TYPE_NORMAL = "original";
    public static final String STORE_TYPE_SPECIALTY = "special";

    public static final String SUGGEST_REPLACEMENT_BY_CALL = "by_call";
    public static final String SUGGEST_REPLACEMENT_BY_CHAT = "by_chat";
    public static final String REPLACE_FREELY = "by_shopper";
    public static final String DONT_REPLACE = "dont_replace";

    public static final int PAYMENT_REVIEW_LIST = 0;
    public static final int SHOPPING_LIST = 1;

    public static final int PERMISSIONS_REQUEST_CODE = 1; // This should be 8 bit, due to android restriction
    public static final int OVERLAY_PERMISSION_REQUEST_CODE = 1001;

    // actual weight
    public static final Double ACTUAL_WEIGHT_WARN_FACTOR = 0.1;
    public static final Double ACTUAL_WEIGHT_MAX_FACTOR = 0.5;

    public static final String PUSH_MESSAGE = "push_message";
    public static final String USER_TOPIC = "user_";
    public static final String UPDATE_JOBS = "update_jobs";

    // product sample
    public static final String IS_OUT_OF_STORE = "is_out_of_store";

    // one click payment
    public static final String ORDER_NUMBER = "order_number";
    public static final String ONE_CLICK_PAY_RESULT = "OCP_result";
    public static final String IS_CC_PROMOTION_EXIST_AND_ELIGIBLE = "cc_promotion_exist_and_eligible";

    public static final int ONE_CLICK_PAY = 0;
    public static final int ALREADY_COMPLETED = 1;
    public static final int SWITCH_TO_COD = 2;
    public static final int PAYMENT_COMPLETED = 3;

    // price tag image
    public static final String PRICE_TAG_PATH = "price_tag_path";

    public static final int PRICE_TAG_FRAGMENT = 4;

    public enum CanceledDialogType {
        SHOP, PAY, ACCEPT, DELIVER, ARRIVE, REJECT, FINISH, FAILED_DELIVERY, REPLACEMENT
    }

    public enum RefreshFragmentType {
        PAY, OOS
    }

    public static class PRESENCE {
        public static final String WORKING = "working";
        public static final String PAUSED = "paused";
        public static final String FINISHED = "finish";
    }

    public static class CONFIG_STATUS {
        public static final String PRESENCE_STATUS = "SND_STATUS";
        public static final String STOCK_LOCATION_ID = "SND_STOCK_LOCATION_ID";
        public static final String STOCK_LOCATION_NAME = "SND_STOCK_LOCATION_NAME";
        public static final String ASK_RECEIPT_NUMBER = "SND_ASK_RECEIPT_NUMBER";
        public static final String PRICE_CHANGED_WILL_UPDATE_PRICE = "SND_PRICE_CHANGED_WILL_UPDATE_PRICE";

        public static final String REQUIRE_AGE_VERIFICATION = "SND_STATUS_REQUIRE_AGE_VERIFICATION";
        public static final String AGE_VERIFICATION_WORDING = "SND_STATUS_AGE_VERIFICATION_WORDING";
        public static final String AGE_VERIFICATION_WORDING_ENGLISH = "SND_STATUS_AGE_VERIFICATION_WORDING_ENGLISH";
        public static final String RESTRICTED_CATEGORIES = "SND_STATUS_RESTRICTED_CATEGORIES";

        public static final String LAST_MODIFIED = "SND_LAST_MODIFIED";
        public static final String STORE_TYPE = "SND_STORE_TYPE";
    }

    public static class USER_ROLE {
        public static final String SHOPPER = "NORMALLY_SHOPPER";
        public static final String DRIVER = "NORMALLY_DRIVER";
    }

    public static String GoogleTranslateURL = "https://translate.google.com/m/translate#auto/%1$s/%2$s";
}