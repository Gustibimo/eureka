package com.shopper.common;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.happyfresh.snowflakes.hoverfly.Sprinkles;
import com.happyfresh.snowflakes.hoverfly.config.Config;
import com.happyfresh.snowflakes.hoverfly.stores.UserStore;
import com.shopper.services.AppService;
import com.shopper.services.CashHandoverService;
import com.shopper.services.JobService;
import com.shopper.services.ProductSampleService;
import com.shopper.services.ProductService;
import com.shopper.services.ShipmentService;

import java.lang.reflect.Modifier;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.Date;

import okhttp3.OkHttpClient;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created by ifranseda on 11/13/14.
 */
public class RestClient {
    private final static RestClient INSTANCE = new RestClient();

    private RestAdapter restAdapter;

    private RestAdapter.Builder restBuilder;

    private JobService jobService;

    private ShipmentService shipmentService;

    private ProductService productService;

    private CashHandoverService cashService;

    private AppService appService;

    private ProductSampleService productSampleService;

    public RestClient() {
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.1.100.23", 8888));
        OkHttpClient.Builder builder = new OkHttpClient.Builder().proxy(proxy);
        OkHttpClient httpClient = builder.build();

        Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new DateDeserializer())
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC).create();

        restBuilder = new RestAdapter.Builder()
//                .setClient(new Ok3Client(httpClient))
                .setEndpoint(Config.getBaseUrl()).setConverter(new GsonConverter(gson));
    }

    public static RestClient getInstance() {
        return INSTANCE;
    }

    public static <T> T createService(Class<T> cls, boolean multipart) {
        if (multipart) {
            return getInstance().getSignedUploadAdapter().create(cls);
        } else {
            return getInstance().getSignedRestAdapter().create(cls);
        }
    }

    private RequestInterceptor getSignedInterceptor(final boolean uploader) {
        return new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader("X-Spree-Token", getUserToken());
                if (!uploader) {
                    request.addHeader("Content-Type", "application/json");
                }

                String lang = ShopperApplication.getInstance().getContentLanguage();
                if (lang != null) {
                    request.addHeader("Locale", lang);
                    request.addQueryParam("Locale", lang);
                }
            }
        };
    }

    private String getUserToken() {
        return Sprinkles.stores(UserStore.class).getToken();
    }

    private RestAdapter getSignedRestAdapter() {
        if (restAdapter == null) {
            restAdapter = restBuilder.setRequestInterceptor(getSignedInterceptor(false)).build();
        }
        return restAdapter;
    }

    private RestAdapter getSignedUploadAdapter() {
        return restBuilder.setRequestInterceptor(getSignedInterceptor(true)).build();
    }

    public JobService getJobService() {
        if (jobService == null) {
            jobService = getSignedRestAdapter().create(JobService.class);
        }
        return jobService;
    }

    public JobService getJobServiceWithUploader() {
        return getSignedUploadAdapter().create(JobService.class);
    }

    public ShipmentService getShipmentService() {
        if (shipmentService == null) {
            shipmentService = getSignedRestAdapter().create(ShipmentService.class);
        }
        return shipmentService;
    }

    public CashHandoverService getHandoverService() {
        if (cashService == null) {
            cashService = getSignedRestAdapter().create(CashHandoverService.class);
        }
        return cashService;
    }

    public CashHandoverService getHandoverServiceWithUploader() {
        return getSignedUploadAdapter().create(CashHandoverService.class);
    }

    public ProductService getProductService() {
        if (productService == null) {
            productService = getSignedRestAdapter().create(ProductService.class);
        }
        return productService;
    }

    public AppService getAppService() {
        if (appService == null) {
            appService = getSignedRestAdapter().create(AppService.class);
        }
        return appService;
    }

    public ProductSampleService getProductSampleService() {
        if (productSampleService == null) {
            productSampleService = getSignedRestAdapter().create(ProductSampleService.class);
        }
        return productSampleService;
    }
}
