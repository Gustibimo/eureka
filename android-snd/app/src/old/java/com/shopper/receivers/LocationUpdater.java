package com.shopper.receivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

import com.shopper.common.Constant;
import com.shopper.common.ShopperApplication;
import com.shopper.utils.LogUtils;
import com.shopper.utils.SharedPrefUtils;

/**
 * Created by ifranseda on 11/12/15.
 */
public class LocationUpdater extends BroadcastReceiver {
    static AlarmManager alarmManager;
    static PendingIntent alarmIntent;

    static void setSchedule(Context context) {
        LogUtils.LOG("LOCATION UPDATE SCHEDULE");

        Boolean isDriver = SharedPrefUtils.getBoolean(ShopperApplication.getContext(), Constant.IS_DRIVER_KEY);
        if (!isDriver) {
            LogUtils.LOG("CURRENT USER IS NOT A DRIVER");
            return;
        }

        Intent intent = new Intent(context, LocationUpdaterReceiver.class);
        alarmIntent = PendingIntent.getBroadcast(context, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        final long INTERVAL_FIVE_MINUTES = 300000L;

        alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(alarmIntent);

        alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() + INTERVAL_FIVE_MINUTES, INTERVAL_FIVE_MINUTES, alarmIntent);

        LogUtils.LOG("SET LOCATION UPDATE SCHEDULE >> " + SystemClock.elapsedRealtime() + INTERVAL_FIVE_MINUTES);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        setSchedule(context);
    }


    public static void startSchedule(Context context) {
        setSchedule(context);
    }

    public static void cancel() {
        if (alarmIntent != null) {
            alarmIntent.cancel();
        }

        if (alarmManager != null) {
            alarmManager.cancel(alarmIntent);
        }
    }
}
