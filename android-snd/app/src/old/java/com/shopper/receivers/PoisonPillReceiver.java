package com.shopper.receivers;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

/**
 * Created by ifranseda on 11/25/14.
 */
public abstract class PoisonPillReceiver extends BroadcastReceiver {
    public static final String FINISH_ACTIVITIES_ACTION = "com.shopper.activities.activity.FINISH_ACTIVITIES_ACTION";
    private static final IntentFilter INTENT_FILTER = createIntentFilter();

    protected Activity mActivity;

    PoisonPillReceiver(Activity activity) {
        mActivity = activity;
    }

    protected static IntentFilter createIntentFilter() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(FINISH_ACTIVITIES_ACTION);
        return filter;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(FINISH_ACTIVITIES_ACTION)) {
            mActivity.finish();
        }
    }

    public static void register(Activity activity, PoisonPillReceiver receiver) {
        activity.registerReceiver(receiver, PoisonPillReceiver.INTENT_FILTER);
    }

    public static void unregister(Activity activity, PoisonPillReceiver receiver) {
        activity.unregisterReceiver(receiver);
    }


    public static void finishAll(Activity activityKiller) {
        activityKiller.sendBroadcast(new Intent(FINISH_ACTIVITIES_ACTION));
    }
}
