package com.shopper.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.shopper.common.ShopperApplication;
import com.shopper.utils.LogUtils;

/**
 * Created by ifranseda on 2/13/15.
 */
public class NetworkStateReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (intent.getExtras() != null) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            int networkType = intent.getExtras().getInt(ConnectivityManager.EXTRA_NETWORK_TYPE);
            NetworkInfo networkInfo = connectivityManager.getNetworkInfo(networkType);

            NetworkInfo.State state = NetworkInfo.State.UNKNOWN;
            if (networkInfo != null) {
                if (networkInfo.getState() == NetworkInfo.State.CONNECTED) {
                    state = NetworkInfo.State.CONNECTED;
                } else if (networkInfo.getState() == NetworkInfo.State.CONNECTING ||
                        networkInfo.getState() == NetworkInfo.State.DISCONNECTING ||
                        networkInfo.getState() == NetworkInfo.State.DISCONNECTED ||
                        networkInfo.getState() == NetworkInfo.State.SUSPENDED) {
                    state = NetworkInfo.State.DISCONNECTED;
                }
            } else if (intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, Boolean.FALSE)) {
                state = NetworkInfo.State.DISCONNECTED;
            }

            LogUtils.LOG("Network state >> " + state);

            ShopperApplication app = ShopperApplication.getInstance();
            app.raiseNetworkState(state);
        }
    }
}
