package com.shopper.receivers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v7.app.NotificationCompat;

import com.happyfresh.fulfillment.R;
import com.shopper.common.ShopperApplication;
import com.shopper.services.DownloadService;

import java.io.File;

/**
 * Created by ifranseda on 10/20/15.
 */
public class DownloadReceiver extends ResultReceiver {
    static final int notificationId = 42424242;
    static boolean isDownloading;

    NotificationManager mNotifyManager;
    NotificationCompat.Builder mBuilder;

    public DownloadReceiver(Handler handler) {
        super(handler);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        super.onReceiveResult(resultCode, resultData);

        if (resultCode == DownloadService.UPDATE_PROGRESS) {
            int progress = resultData.getInt(DownloadService.DOWNLOAD_PROGRESS);
            showNotificationProgress(progress);

        } else if (resultCode == DownloadService.DOWNLOAD_STATUS) {
            int state = resultData.getInt(DownloadService.DOWNLOAD_STATE);
            String fileName = resultData.getString(DownloadService.APP_FILENAME);

            switch (state) {
                case DownloadService.DownloadState.INITIATE:
                    setUpNotification();
                    break;

                case DownloadService.DownloadState.STARTED:
                    isDownloading = true;
                    break;

                case DownloadService.DownloadState.COMPLETED:
                    isDownloading = false;
                    break;

                case DownloadService.DownloadState.FAILED:
                    isDownloading = false;
                    break;
            }

            showDownloadStatus(state, fileName);
        }
    }

    void setUpNotification() {
        Context context = ShopperApplication.getContext();

        mNotifyManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilder = new NotificationCompat.Builder(context);
        mBuilder.setContentTitle(context.getString(R.string.download_update))
                .setAutoCancel(false)
                .setSmallIcon(R.mipmap.ic_launcher);
    }

    void showNotificationProgress(int progress) {
        Context context = ShopperApplication.getContext();

        mBuilder.setProgress(0, 0, true);
        mBuilder.setAutoCancel(false);
        mBuilder.setContentText(context.getString(R.string.downloading_update));

        Notification notification = mBuilder.build();
        notification.flags |= Notification.FLAG_NO_CLEAR;

        mNotifyManager.notify(notificationId, notification);
    }

    void showDownloadStatus(int state, String fileName) {
        Context context = ShopperApplication.getContext();

        mNotifyManager.cancel(notificationId);

        int flags = Notification.FLAG_FOREGROUND_SERVICE;

        String status = "";
        switch (state) {
            case DownloadService.DownloadState.INITIATE:
                status = context.getString(R.string.download_starting);
                flags = Notification.FLAG_NO_CLEAR;
                break;

            case DownloadService.DownloadState.COMPLETED:
                status = context.getString(R.string.download_completed);
                flags = Notification.FLAG_AUTO_CANCEL;
                break;

            case DownloadService.DownloadState.FAILED:
                status = context.getString(R.string.download_failed);
                flags = Notification.FLAG_AUTO_CANCEL;
                break;
        }

        if (state == DownloadService.DownloadState.COMPLETED) {
            setUpNotification();

            File downloadDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            File appFile = new File(downloadDir, fileName);

            Intent newIntent = new Intent(Intent.ACTION_VIEW);
            newIntent.setDataAndType(Uri.fromFile(appFile), "application/vnd.android.package-archive");
            newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, newIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(pendingIntent);
        }

        mBuilder.setContentText(status);

        Notification notification = mBuilder.build();
        notification.flags = flags;

        mNotifyManager.notify(notificationId, notification);
    }
}