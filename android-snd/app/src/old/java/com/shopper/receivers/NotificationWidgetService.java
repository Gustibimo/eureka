package com.shopper.receivers;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.happyfresh.fulfillment.R;
import com.happyfresh.fulfillment.modules.Shopper.JobList.ShopperJobListActivity;
import com.happyfresh.snowflakes.hoverfly.utils.PermissionUtils;
import com.shopper.activities.DriverBatchAssignmentActivity;
import com.shopper.activities.DriverListActivity;
import com.shopper.common.Constant;
import com.shopper.common.ShopperApplication;
import com.shopper.listeners.DataListener;
import com.shopper.utils.AppUtils;
import com.shopper.utils.LogUtils;
import com.shopper.utils.StringUtils;

/**
 * Created by ifranseda on 11/13/15.
 */
public class NotificationWidgetService extends Service {
    private WindowManager mWindowManager;
    private View mView;

    private Long mBatchId;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LogUtils.LOG("onStartCommand");

        if (intent != null && intent.getExtras() != null) {
            Bundle bundle = intent.getExtras().getBundle(Constant.PUSH_MESSAGE);
            if (bundle != null) {
                show(bundle);
            }
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        LogUtils.LOG("onBind");
        return null;
    }

    @Override
    public void onCreate() {
        LogUtils.LOG("onCreate");
        super.onCreate();

        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        LayoutInflater mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mView = mInflater.inflate(R.layout.notification_widget, null);
        Button closeButton = (Button) mView.findViewById(R.id.close);
        Button viewButton = (Button) mView.findViewById(R.id.view);

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                skipAssignment();
                stopSelf();
            }
        });

        viewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), DriverBatchAssignmentActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra(Constant.EXTRAS_BATCH_ID, mBatchId);
                getApplicationContext().startActivity(intent);

                clearNotification();
                stopSelf();
            }
        });


        Button closeBroadcast = (Button) mView.findViewById(R.id.close_broadcast);
        Button viewBroadcast = (Button) mView.findViewById(R.id.view_broadcast);

        closeBroadcast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearNotification();
                stopSelf();
            }
        });

        viewBroadcast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                if (AppUtils.currentUserType() == AppUtils.UserType.SHOPPER) {
                    intent = new Intent(getApplicationContext(), ShopperJobListActivity.class);
                } else {
                    intent = new Intent(getApplicationContext(), DriverListActivity.class);
                }

                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                getApplicationContext().startActivity(intent);
                clearNotification();
                stopSelf();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mBatchId = null;
        removeNow();
    }

    private void show(Bundle bundle) {
        View assignmentView = mView.findViewById(R.id.assignment);
        TextView batchId = (TextView) mView.findViewById(R.id.batch_number);
        TextView deliveryTime = (TextView) mView.findViewById(R.id.delivery_time);
        TextView storeLocation = (TextView) mView.findViewById(R.id.store_location_name);

        View broadcastView = mView.findViewById(R.id.broadcast);
        TextView messageTextView = (TextView) mView.findViewById(R.id.broadcast_message);

        int assigned = -1;
        try {
            Object a = bundle.get("a");
            assigned = Integer.valueOf(String.valueOf(a));
        } catch(NumberFormatException ex) {
        }

        if (assigned == 1) {
            assignmentView.setVisibility(View.VISIBLE);
            broadcastView.setVisibility(View.GONE);

            try {
                Object b = bundle.get("b");
                mBatchId = Long.valueOf(String.valueOf(b));
                batchId.setText(String.valueOf(mBatchId));
            } catch(NumberFormatException ex) {

            }

            storeLocation.setText(String.valueOf(bundle.get("s")));

            String slotString = String.valueOf(bundle.get("d"));
            String[] slots = slotString.split(";");

            if (slots.length > 1) {
                String timeSlot = StringUtils.showDeliveryTime(slots[0], slots[1]);
                deliveryTime.setText(timeSlot);
            } else {
                String timeSlot = StringUtils.showDeliveryTime(slots[0]);
                deliveryTime.setText(timeSlot);
            }
        } else {
            assignmentView.setVisibility(View.GONE);
            broadcastView.setVisibility(View.VISIBLE);

            messageTextView.setText(String.valueOf(bundle.get("alert")));
        }

        WindowManager.LayoutParams mLayoutParams = new WindowManager.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 0, 0,
                WindowManager.LayoutParams.TYPE_SYSTEM_ERROR,
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                        | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                        | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                        | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                        | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN,
                PixelFormat.RGBA_8888);

        removeNow();

        boolean systemAlertWindow = PermissionUtils.alertWindow(this);
        if (systemAlertWindow && !mView.isShown()) {
            mWindowManager.addView(mView, mLayoutParams);
        } else {
            LogUtils.WTF("NO PERMISSION TO SYSTEM_ALERT_WINDOW. Permission is " + systemAlertWindow);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.canDrawOverlays(this) && !mView.isShown()) {
                mWindowManager.addView(mView, mLayoutParams);
            }
        }
    }

    public void removeNow() {
        if (mView != null && ViewCompat.isAttachedToWindow(mView)) {
            mWindowManager.removeViewImmediate(mView);
        }
    }

    private void clearNotification() {
        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nm.cancelAll();
    }

    private void skipAssignment() {
        LogUtils.logEvent("DRIVER ASSIGNMENT", "SKIP ASSIGNMENT " + mBatchId);

        clearNotification();

        if (mBatchId == null) {
            return;
        }

        ShopperApplication.getInstance().getBatchManager().skip(mBatchId, new DataListener() {
            @Override
            public void onCompleted(boolean success, Object data) {
                LogUtils.logEvent("DRIVER ASSIGNMENT", "SKIP ASSIGNMENT COMPLETED");
            }

            @Override
            public void onFailed(Throwable exception) {
                LogUtils.logEvent("DRIVER ASSIGNMENT", "SKIP ASSIGNMENT FAILED");
            }
        });
    }
}
