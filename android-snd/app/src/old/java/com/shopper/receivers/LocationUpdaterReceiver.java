package com.shopper.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.SystemClock;

import com.google.android.gms.location.LocationListener;
import com.shopper.common.ShopperApplication;
import com.shopper.utils.LocationUtils;
import com.shopper.utils.LogUtils;

/**
 * Created by ifranseda on 11/12/15.
 */
public class LocationUpdaterReceiver extends BroadcastReceiver {
    static LocationListener listener;

    @Override
    public void onReceive(Context context, Intent intent) {
        final long INTERVAL_FIVE_MINUTES = 300000L;
        LogUtils.LOG("NEXT LOCATION UPDATE >> " + SystemClock.elapsedRealtime() + INTERVAL_FIVE_MINUTES);

        if (listener == null) {
            listener = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    LogUtils.logEvent("Location Update", "Accuracy >> " + location.getAccuracy());
                    if (location.hasAccuracy() && location.getAccuracy() < 1000) {
                        ShopperApplication.getInstance().getAppManager().updateLocation(
                                location.getLatitude(),
                                location.getLongitude(),
                                location.getAccuracy()
                        );
                    }
                }
            };
        }

        LocationUtils.connect(14400, listener);
    }
}
