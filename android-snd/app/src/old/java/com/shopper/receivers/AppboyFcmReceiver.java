package com.shopper.receivers;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import com.appboy.push.AppboyNotificationUtils;
import com.shopper.common.Constant;

public class AppboyFcmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String packageName = context.getPackageName();
        String pushReceivedAction = packageName + AppboyNotificationUtils.APPBOY_NOTIFICATION_RECEIVED_SUFFIX;

        String action = intent.getAction();
        if (pushReceivedAction.equals(action)) {
            Bundle notificationBundle = new Bundle();

            Bundle extras = intent.getExtras();
            if (extras != null) {
                extras = extras.getBundle("extra");
                int assigned = -1;
                if (!extras.isEmpty()) {
                    try {
                        Object a = extras.get("a");
                        assigned = Integer.valueOf(String.valueOf(a));
                    } catch (NumberFormatException ex) {
                    }

                    if (assigned != 2) {
                        notificationBundle.putBundle(Constant.PUSH_MESSAGE, extras);
                        showNotificationWidget(context.getApplicationContext(), notificationBundle);
                    } else {
                        clearNotification(context.getApplicationContext());
                    }
                    updateJobList(context);
                } else {
                    clearNotification(context.getApplicationContext());
                }
            } else {
                clearNotification(context.getApplicationContext());
            }
        }


    }

    private void showNotificationWidget(Context context, Bundle bundle) {
        Intent intent = new Intent(context, NotificationWidgetService.class);
        intent.putExtras(bundle);
        context.startService(intent);
    }

    private void updateJobList(Context context) {
        LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(Constant.UPDATE_JOBS));
    }

    private void clearNotification(Context context) {
        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.cancelAll();
    }
}
