package com.shopper.listeners;

import com.google.gson.Gson;
import com.shopper.common.ShopperApplication;
import com.happyfresh.snowflakes.hoverfly.models.response.ExceptionResponse;
import com.shopper.utils.LogUtils;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.android.MainThreadExecutor;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by ifranseda on 8/19/15.
 */
public abstract class BackgroundCallback<T> implements Callback<T> {

    private ShopperApplication mApplication;
    private DataListener mListener;
    private Executor callbackExecutor;
    private Executor uiExecutor;

    public BackgroundCallback(ShopperApplication application, DataListener listener) {
        this.mApplication = application;
        this.mListener = listener;

        this.callbackExecutor = new ThreadPoolExecutor(1, 1, 10, TimeUnit.SECONDS,
                new ArrayBlockingQueue<Runnable>(1));
        this.uiExecutor = new MainThreadExecutor();
    }

    public T beforeCompletion(T o, Response response) {
        return o;
    }

    public abstract void afterCompletion(T o);

    @Override
    public final void success(final T o, final Response response) {
        this.callbackExecutor.execute(new Runnable() {
            @Override
            public void run() {
                final T output = beforeCompletion(o, response);
                uiExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        afterCompletion(output);
                    }
                });
            }
        });
    }

    @Override
    public void failure(final RetrofitError error) {
        if (error.getResponse() != null && error.getResponse().getStatus() == 401) {
            LogUtils.logEvent("REST Exception", "UNAUTHORIZED!!");
            mApplication.getUnauthorizedCallback().onUnauthorized();
        } else {
            if (error.getResponse() != null && error.getResponse().getBody() != null) {
                try {
                    LogUtils.logEvent("HTTP STATUS CODE", "RESPONSE >> " + error.getResponse().getStatus());

                    String jsonStr = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                    ExceptionResponse exceptionResponse = new Gson().fromJson(jsonStr, ExceptionResponse.class);

                    LogUtils.logEvent("REST Exception", jsonStr);
                    exception(exceptionResponse);
                    return;
                } catch (Exception e) {
                    e.getMessage();
                }

                uiExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        mListener.onFailed(error);
                    }
                });
            } else {
                uiExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        mListener.onCompleted(false, null);
                    }
                });
            }
        }
    }

    public void exception(final ExceptionResponse exception) {
        uiExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mListener.onFailed(exception);
            }
        });
    }

}