package com.shopper.listeners;

/**
 * Created by ifranseda on 12/23/14.
 */
public interface UnauthorizedCallbackListener {
    void onUnauthorized();
}
