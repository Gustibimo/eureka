package com.shopper.listeners;

/**
 * Created by ifranseda on 11/17/14.
 */
public abstract class DataListener<T> {
    public abstract void onCompleted(boolean success, T data);

    public void onFailed(Throwable exception) {
    }

    public static abstract class AwaitListener {
        public abstract void promise();
    }
}
