package com.shopper.listeners;

import com.google.gson.Gson;
import com.shopper.common.ShopperApplication;
import com.happyfresh.snowflakes.hoverfly.models.response.ExceptionResponse;
import com.shopper.utils.LogUtils;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by ifranseda on 12/22/14.
 */
public abstract class SimpleCallback<T> implements Callback<T> {

    private ShopperApplication mApplication;
    private DataListener mListener;

    public SimpleCallback(ShopperApplication application, DataListener listener) {
        mApplication = application;
        mListener = listener;
    }

    @Override
    public abstract void success(T o, Response response);

    @Override
    public void failure(RetrofitError error) {
        if (error.getResponse() != null && error.getResponse().getStatus() == 401) {
            LogUtils.logEvent("REST Exception", "UNAUTHORIZED!!");
            mApplication.getUnauthorizedCallback().onUnauthorized();
        } else {
            if (error != null && error.getResponse() != null && error.getResponse().getBody() != null) {
                try {
                    String jsonStr = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                    ExceptionResponse exceptionResponse = new Gson().fromJson(jsonStr, ExceptionResponse.class);

                    LogUtils.logEvent("REST Exception", jsonStr);
                    exception(exceptionResponse);
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                }

                mListener.onFailed(error);
            } else {
                mListener.onCompleted(false, null);
            }
        }
    }

    public void exception(ExceptionResponse exception) {
        mListener.onCompleted(false, exception);
    }
}