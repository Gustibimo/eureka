package com.shopper.listeners;

/**
 * Created by ifranseda on 7/24/15.
 */
public interface NetworkQueueChangedListener {
    void onUpdated(int size);
}
