package com.shopper.listeners;

/**
 * Created by rsavianto on 12/11/17.
 */

public interface PaymentReceiptListener {
    void remove(int position);
}