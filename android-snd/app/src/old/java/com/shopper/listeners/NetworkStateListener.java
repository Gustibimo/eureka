package com.shopper.listeners;

import android.net.NetworkInfo;

/**
 * Created by ifranseda on 2/13/15.
 */
public interface NetworkStateListener {
    void onStateChanged(NetworkInfo.State state);
}
