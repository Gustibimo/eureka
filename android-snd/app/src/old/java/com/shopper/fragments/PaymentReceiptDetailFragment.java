package com.shopper.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.happyfresh.fulfillment.R;
import com.shopper.adapters.PaymentReceiptPhotoAdapter;
import com.shopper.common.ShopperApplication;
import com.shopper.events.OpenCameraEvent;
import com.shopper.events.TakePictureEvent;
import com.happyfresh.snowflakes.hoverfly.models.PhotoReceiptItem;
import com.happyfresh.snowflakes.hoverfly.models.ReceiptItem;
import com.happyfresh.snowflakes.hoverfly.models.Shipment;
import com.shopper.utils.BitmapUtils;
import com.shopper.utils.LogUtils;
import com.squareup.otto.Subscribe;

import org.parceler.Parcels;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import butterknife.ButterKnife;
import retrofit.android.MainThreadExecutor;

public class PaymentReceiptDetailFragment extends BaseRecyclerFragment {
    public static final String RECEIPT_ITEM = "com.shopper.PaymentReceiptDetailFragment.RECEIPT_ITEM";

    public static final String SHIPMENT_ID = "com.shopper.PaymentReceiptDetailFragment.SHIPMENT_ID";

    public static final String CURRENT_RECEIPT = "com.shopper.PaymentFragment.CURRENT_RECEIPT";

    public static final String CURRENT_PHOTO_RECEIPT = "com.shopper.PaymentFragment.CURRENT_PHOTO_RECEIPT";

    public static final String RECEIPT_FILE_PATH = "com.shopper.PaymentFragment.RECEIPT_FILE_PATH";

    private static final int CAMERA_REQUEST_CODE = 1001;

    private String mReceiptFilePath;

    private ReceiptItem mCurrentReceipt;
    private PhotoReceiptItem mCurrentPhotoReceipt;

    View mProgressContainer;

    Shipment mShipment;

    PaymentReceiptPhotoAdapter mAdapter;

    private boolean isRegistered = false;

    @Override
    public boolean isSwipeRefreshLayoutEnabled() {
        return false;
    }

    public static PaymentReceiptDetailFragment newInstance(Long shipmentId, ReceiptItem receiptItem) {
        PaymentReceiptDetailFragment fragment = new PaymentReceiptDetailFragment();

        Bundle bundle = new Bundle();
        bundle.putParcelable(PaymentReceiptDetailFragment.RECEIPT_ITEM, Parcels.wrap(receiptItem));
        bundle.putLong(PaymentReceiptDetailFragment.SHIPMENT_ID, shipmentId);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            long shipmentId = getArguments().getLong(SHIPMENT_ID);
            mShipment = Shipment.findById(shipmentId);
            mCurrentReceipt = Parcels.unwrap(getArguments().getParcelable(PaymentReceiptDetailFragment.RECEIPT_ITEM));
        } else {
            Long shipmentId = savedInstanceState.getLong(SHIPMENT_ID, 0);
            mShipment = Shipment.findById(shipmentId);
            mCurrentReceipt = Parcels.unwrap(savedInstanceState.getParcelable(CURRENT_RECEIPT));
            mReceiptFilePath = savedInstanceState.getString(RECEIPT_FILE_PATH);
            mCurrentPhotoReceipt = savedInstanceState.getParcelable(CURRENT_PHOTO_RECEIPT);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (mShipment != null) {
            outState.putLong(SHIPMENT_ID, mShipment.getRemoteId());
            outState.putParcelable(CURRENT_RECEIPT, Parcels.wrap(mCurrentReceipt));
            outState.putString(RECEIPT_FILE_PATH, mReceiptFilePath);
            outState.putParcelable(CURRENT_PHOTO_RECEIPT, mCurrentPhotoReceipt);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_payment_receipt_detail, null);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (savedInstanceState != null) {
            long shipmentId = savedInstanceState.getLong(SHIPMENT_ID, 0);
            if (shipmentId > 0) {
                mShipment = Shipment.findById(shipmentId);
            }
        }

        if (savedInstanceState == null) {
            mCurrentReceipt.getPhotoReceiptItems().add(new PhotoReceiptItem());
        }

        mAdapter = new PaymentReceiptPhotoAdapter(getActivity(), mShipment, mCurrentReceipt);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (!isRegistered) {
                ShopperApplication.getInstance().getBus().register(this);
                isRegistered = !isRegistered;
            }

            if (isResumed()){
                onResume();
            }
        } else {
            if (isRegistered) {
                ShopperApplication.getInstance().getBus().unregister(this);
                isRegistered = !isRegistered;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!getUserVisibleHint()) {
            return;
        }
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//
//        ShopperApplication.getInstance().getBus().register(this);
//    }

//    @Override
//    public void onPause() {
//        super.onPause();
//
//        ShopperApplication.getInstance().getBus().unregister(this);
//    }


//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//
//        ShopperApplication.getInstance().getBus().register(this);
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//
//        ShopperApplication.getInstance().getBus().unregister(this);
//    }


//    @Override
//    public void onResumeFragment() {
//        ShopperApplication.getInstance().getBus().register(this);
//    }
//
//    @Override
//    public void onPauseFragment() {
//        ShopperApplication.getInstance().getBus().unregister(this);
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case CAMERA_REQUEST_CODE: {
                if (resultCode == Activity.RESULT_OK) {
                    processCameraOutput();
                } else {
                    mAdapter.notifyDataSetChanged();
                }

                break;
            }

            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    @Subscribe
    public void openCamera(OpenCameraEvent e) {
        LogUtils.LOG("OpenCameraEvent >> " + e);

        if (e.getData() != null) {
            mCurrentPhotoReceipt = e.getData();
        }

        if (getActivity() == null) {
            return;
        }

        Uri cameraOutput = Uri.fromFile(new File(getReceiptPath()));
        LogUtils.LOG("Camera Output >> " + cameraOutput);

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, cameraOutput);
        startActivityForResult(intent, CAMERA_REQUEST_CODE);
    }

    private String getReceiptPath() {
        if (mReceiptFilePath == null) {
            mReceiptFilePath = BitmapUtils.getReceiptImagePath(getActivity(), mShipment.getOrder().getNumber());
        }

        return mReceiptFilePath;
    }

    private void processCameraOutput() {
        final String receiptPath = getReceiptPath();
        LogUtils.LOG("Camera Output: " + receiptPath);

        ThreadPoolExecutor executor = new ThreadPoolExecutor(1, 1, 300, TimeUnit.SECONDS,
                new ArrayBlockingQueue<Runnable>(1));
        executor.execute(new Runnable() {
            @Override
            public void run() {
                FileOutputStream out = null;
                try {
                    Bitmap output = BitmapFactory.decodeFile(receiptPath);

                    if (output == null) {
                        output = getBitmapWithDelay(1);

                        if (output == null) {
                            output = getBitmapWithDelay(3);

                            if (output == null) {
                                output = getBitmapWithDelay(5);
                            }
                        }
                    }

                    int width = output.getWidth();
                    int height = output.getHeight();
                    int max = 1600;

                    if (width > height && width >= max) {
                        double aspectRatio = (double) height / (double) width;
                        width = max;
                        height = (int) (width * aspectRatio);
                    } else if (height >= max) {
                        double aspectRatio = (double) height / (double) width;
                        height = max;
                        width = (int) (height / aspectRatio);
                    }

                    LogUtils.LOG("Size >> " + width + " - " + height);

                    Bitmap resized = Bitmap.createScaledBitmap(output, width, height, true);

                    File aFile = new File(receiptPath);
                    out = new FileOutputStream(aFile);
                    resized.compress(Bitmap.CompressFormat.JPEG, 90, out);

                    resized.recycle();
                    output.recycle();

                } catch (final OutOfMemoryError oom) {
                    oom.printStackTrace();

                    new MainThreadExecutor().execute(new Runnable() {
                        @Override
                        public void run() {
                            MaterialDialog.Builder dialogBuilder = new MaterialDialog.Builder(getActivity());
                            dialogBuilder.title(R.string.alert_title_error);
                            dialogBuilder.content(oom.getLocalizedMessage());
                            dialogBuilder.positiveText(R.string.alert_button_ok);
                            dialogBuilder.onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    if (mCurrentReceipt.getPhotoReceiptItems().size() > 0) {
                                        mAdapter.notifyDataSetChanged();
                                    }
                                }
                            });
                            dialogBuilder.build().show();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    mCurrentReceipt = null;
                } finally {
                    try {
                        if (out != null) {
                            out.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    new MainThreadExecutor().execute(new Runnable() {
                        @Override
                        public void run() {
                            if (mCurrentPhotoReceipt != null) {
                                mCurrentPhotoReceipt.setImagePath(receiptPath);
                                PhotoReceiptItem newPhotoReceiptItem = mCurrentPhotoReceipt.clone();

                                mCurrentReceipt.getPhotoReceiptItems().remove(newPhotoReceiptItem.getPosition());
                                mCurrentReceipt.getPhotoReceiptItems().add(newPhotoReceiptItem.getPosition(), newPhotoReceiptItem);
                            }

                            boolean isAllCompleted = true;
                            for (PhotoReceiptItem photoReceiptItem : mCurrentReceipt.getPhotoReceiptItems()) {
                                if (TextUtils.isEmpty(photoReceiptItem.getImagePath())) {
                                    isAllCompleted = false;
                                    break;
                                }
                            }

                            if (isAllCompleted) {
                                mCurrentReceipt.getPhotoReceiptItems().add(new PhotoReceiptItem());
                            }

                            mAdapter.notifyDataSetChanged();

                            mReceiptFilePath = null;
                            mCurrentPhotoReceipt = null;

                            ShopperApplication.getInstance().getBus().post(new TakePictureEvent(mCurrentReceipt));
                        }
                    });
                }
            }

            private Bitmap getBitmapWithDelay(int delay) throws InterruptedException {
                Thread.sleep(delay * 1000);
                return BitmapFactory.decodeFile(receiptPath);
            }
        });
    }
}