package com.shopper.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.utils.FileUtils;
import com.shopper.activities.DriverSignatureActivity;
import com.shopper.activities.HandoverHistoryActivity;
import com.shopper.common.Constant;
import com.shopper.common.ShopperApplication;
import com.shopper.components.CircularProgressBar;
import com.shopper.listeners.DataListener;
import com.shopper.utils.BitmapUtils;
import com.shopper.utils.LogUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;

import butterknife.ButterKnife;
import butterknife.BindView;
import butterknife.OnClick;
import retrofit.mime.TypedFile;

/**
 * Created by ifranseda on 5/18/15.
 */
public class HandoverSubmissionFragment extends Fragment {
    public static final int SIGNATURE = 998;
    public static final int RECIPIENT_PHOTO = 999;
    public static final String CLIENT_TIMESTAMP = "com.shopper.HandoverSubmissionFragment.CLIENT_TIMESTAMP";

    private static final String CAMERA_BUFFER_DIR = "CAM";
    private static final String CAMERA_BUFFER_NAME = "HANDOVER_%d.jpg";

    private static final String HANDOVER_DIRECTORY = "handover";
    private static final String HANDOVER_FILE = "HANDOVER_%s.jpg";

    private long clientTimestamp;

    @BindView(R.id.handover_method_spinner)
    Spinner handoverMethodSpinner;

    @BindView(R.id.receiver_layout)
    View receiverLayout;

    @BindView(R.id.handover_receiver_name)
    TextView handoverReceiverName;

    @BindView(R.id.signature_container)
    RelativeLayout signatureContainer;

    @BindView(R.id.signature_image)
    ImageView signatureImage;

    @BindView(R.id.signature_hint)
    View signatureHint;

    @BindView(R.id.recipient_layout)
    LinearLayout recipientLayout;

    @BindView(R.id.recipient_container)
    FrameLayout recipientContainer;

    @BindView(R.id.recipient_image)
    ImageView recipientImage;

    @BindView(R.id.submit_handover_button)
    Button submitHandoverButton;

    @BindView(R.id.progress_container)
    RelativeLayout progressContainer;

    @BindView(R.id.fragment_progressbar)
    CircularProgressBar progressbar;

    private String mCameraOutputPath;
    private String mBitmapOutputPath;
    private double cashHandoverAmount = 0;
    private String cashCurrency;
    private String signatureImageFilePath;

    public static HandoverSubmissionFragment newInstance(double handoverAmount, String currency) {
        HandoverSubmissionFragment fragment = new HandoverSubmissionFragment();

        Bundle bundle = new Bundle();
        bundle.putDouble(Constant.CASH_HANDOVER_AMOUNT, handoverAmount);
        bundle.putString(Constant.CASH_HANDOVER_CURRENCY, currency);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_handover_submission, null);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Drawable bgDrawable = getResources().getDrawable(R.drawable.abc_spinner_mtrl_am_alpha);
        bgDrawable.setColorFilter(getResources().getColor(R.color.handover_spinner_icon), PorterDuff.Mode.SRC_ATOP);
        handoverMethodSpinner.setBackground(bgDrawable);

        cashHandoverAmount = getArguments().getDouble(Constant.CASH_HANDOVER_AMOUNT);
        cashCurrency = getArguments().getString(Constant.CASH_HANDOVER_CURRENCY);

        progressbar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SIGNATURE && resultCode == Activity.RESULT_OK) {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    File file = BitmapUtils.getSignatureImage(ShopperApplication.getContext());
                    signatureImageFilePath = file.getAbsolutePath();
                    Bitmap bitmap = BitmapFactory.decodeFile(signatureImageFilePath);
                    signatureImage.setImageBitmap(bitmap);
                }
            }, 500);

            signatureHint.setVisibility(View.GONE);

        } else if (requestCode == RECIPIENT_PHOTO && resultCode == Activity.RESULT_OK) {
            processCameraOutput();

        } else {
            super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == SIGNATURE) {
                signatureHint.setVisibility(View.VISIBLE);
            } else {
                deleteCameraOutput();
            }
        }
    }

    @OnClick(R.id.receiver_layout)
    void showReceiverDialog() {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View customerView = inflater.inflate(R.layout.dialog_customer_name, null);

        MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity());

        final EditText editText = (EditText) customerView.findViewById(R.id.dialog_customer_name);

        builder.title(R.string.handover_receive_money);
        builder.customView(customerView, false);

        builder.negativeText(R.string.alert_button_cancel);
        builder.positiveText(R.string.alert_button_ok);
        builder.callback(new MaterialDialog.ButtonCallback() {
            @Override
            public void onPositive(MaterialDialog dialog) {
                String text = editText.getText().toString();
                if (!TextUtils.isEmpty(text)) {
                    handoverReceiverName.setText(editText.getText().toString());
                }
            }
        });

        MaterialDialog inputDialog = builder.build();
        inputDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        inputDialog.show();
    }

    @OnClick(R.id.signature_container)
    void inputSignature() {
        Intent intent = new Intent(getActivity(), DriverSignatureActivity.class);
        startActivityForResult(intent, SIGNATURE);
    }

    @OnClick(R.id.recipient_container)
    void openCamera() {
        // DO NOT reset mBitmapOutputPath to reset form validation, unless there is another validation work around
        Uri cameraFileUri = createCameraOutputUrl();
        mCameraOutputPath = cameraFileUri.getPath();

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, cameraFileUri);

        startActivityForResult(intent, RECIPIENT_PHOTO);
    }

    @OnClick(R.id.submit_handover_button)
    void submit() {
        progressContainer.setVisibility(View.VISIBLE);
        progressbar.setVisibility(View.VISIBLE);

        submitHandoverButton.setText(R.string.please_wait);
        submitHandoverButton.setEnabled(false);

        if (TextUtils.isEmpty(handoverReceiverName.getText())) {
            formErrorDialog(R.string.alert_message_empty_receiver);
            return;
        }

        if (signatureImageFilePath == null) {
            formErrorDialog(R.string.alert_message_empty_signature);
            return;
        }

        File signatureFile = new File(signatureImageFilePath);
        if (!signatureFile.exists()) {
            formErrorDialog(R.string.alert_message_empty_signature);
            return;
        }


        if (mBitmapOutputPath == null) {
            formErrorDialog(R.string.alert_message_empty_photo);
            return;
        }

        File photoFile = new File(mBitmapOutputPath);
        if (!photoFile.exists()) {
            formErrorDialog(R.string.alert_message_empty_photo);
            return;
        }


        TypedFile signature = new TypedFile("image/jpg", signatureFile);
        TypedFile photo = new TypedFile("image/jpg", photoFile);

        ShopperApplication.getInstance().getHandoverManager().submit(cashHandoverAmount,
                cashCurrency, handoverReceiverName.getText().toString(),
                signature, photo, clientTimestamp,
                new DataListener() {
                    @Override
                    public void onCompleted(boolean success, Object data) {
                        if (success) {
                            openHandoverHistory();
                        } else {
                            if (getActivity() != null) {
                                String text = getActivity().getString(R.string.unable_to_submit_cash_transfer);
                                Toast.makeText(getActivity(), text, Toast.LENGTH_LONG).show();
                            }
                        }

                        progressContainer.setVisibility(View.GONE);

                        submitHandoverButton.setText(R.string.handover_submit);
                        submitHandoverButton.setEnabled(true);
                    }
                });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            clientTimestamp = System.currentTimeMillis();
        } else
            clientTimestamp = savedInstanceState.getLong(CLIENT_TIMESTAMP, System.currentTimeMillis());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (clientTimestamp > 0L) {
            outState.putLong(CLIENT_TIMESTAMP, clientTimestamp);
        }
    }

    public void formErrorDialog(int res) {
        MaterialDialog.Builder inputDialog = new MaterialDialog.Builder(getActivity());
        inputDialog.title(R.string.alert_title_warning);

        inputDialog.content(res);
        inputDialog.positiveText(R.string.alert_button_ok);
        inputDialog.dismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                progressContainer.setVisibility(View.GONE);

                submitHandoverButton.setText(R.string.handover_submit);
                submitHandoverButton.setEnabled(true);
            }
        });

        inputDialog.show();
    }

    private void openHandoverHistory() {
        getActivity().finish();
        Intent intent = new Intent(getActivity(), HandoverHistoryActivity.class);
        getActivity().startActivity(intent);
    }

    private Uri createCameraOutputUrl() {
        File directory = FileUtils.getCacheDir(getActivity(), CAMERA_BUFFER_DIR);

        String timestamp = String.format(CAMERA_BUFFER_NAME, (new Date().getTime() / 1000));
        File cameraBuffer = new File(directory, timestamp);

        try {
            cameraBuffer.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Uri.fromFile(cameraBuffer);
    }

    private void deleteCameraOutput() {
        try {
            File file = new File(mCameraOutputPath);
            file.delete();
        } catch (Exception e) {
            LogUtils.logEvent("Handover delete camera", e.getLocalizedMessage());
        } finally {
            mCameraOutputPath = null;
        }
    }

    void processCameraOutput() {
        if (mCameraOutputPath != null && (new File(mCameraOutputPath)).exists()) {
            String filename = String.format(HANDOVER_FILE,
                    String.valueOf(System.currentTimeMillis()));
            File receiptDir = FileUtils.getCacheDir(getActivity(), HANDOVER_DIRECTORY);
            if (!receiptDir.exists()) {
                receiptDir.mkdirs();
            }

            mBitmapOutputPath = new File(receiptDir, filename).getAbsolutePath();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    createBitmapOutput(1);
                }
            }).run();

        } else {
            MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity());
            builder.title(R.string.alert_title_warning);
            builder.content(R.string.alert_camera_error);
            builder.positiveText(R.string.alert_button_ok);

            builder.show();
        }
    }

    private void createBitmapOutput(int inDensity) {
        if (inDensity > 4) {
            return;
        }

        Bitmap src = null;
        File outFile = null;

        try {
            src = BitmapFactory.decodeFile(mCameraOutputPath);
            outFile = reduceReceiptFile(mBitmapOutputPath, 1600, src);

        } catch (OutOfMemoryError e) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferQualityOverSpeed = false;
            options.inScaled = true;
            options.inDensity = inDensity;
            options.inTargetDensity = 1;

            try {
                src = BitmapFactory.decodeFile(mCameraOutputPath, options);
                outFile = reduceReceiptFile(mBitmapOutputPath, 1000, src);
            } catch (OutOfMemoryError e1) {
                createBitmapOutput(inDensity + 1);
            }

        } finally {
            if (outFile != null && outFile.exists()) {
                if (src != null) {
                    final Bitmap finalSrc = src;
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            recipientImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
                            recipientImage.setImageBitmap(finalSrc);
                        }
                    });
                }
            } else {
                createBitmapOutput(inDensity + 1);
            }
        }
    }

    private File reduceReceiptFile(String path, int maxSize, Bitmap bitmap) {
        // Copy resized receipt file to public document directory
        // this file should be uploaded
        if (maxSize < 1000) {
            return new File(path);
        }

        boolean compressed = false;
        OutputStream os = null;

        try {
            bitmap = BitmapUtils.getResizedBitmap(bitmap, maxSize);

            os = new FileOutputStream(path);
            compressed = bitmap.compress(Bitmap.CompressFormat.JPEG, 60, os);
            os.flush();

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            try {
                if (os != null) {
                    os.close();
                }
            } catch (IOException e1) {
            }

            if (!compressed) {
                return reduceReceiptFile(path, maxSize - 100, bitmap);
            } else {
                return new File(path);
            }
        }
    }
}
