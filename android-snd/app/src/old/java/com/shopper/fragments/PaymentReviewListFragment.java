package com.shopper.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.LineItem;
import com.happyfresh.snowflakes.hoverfly.models.Shipment;
import com.shopper.adapters.LineItemAdapter;
import com.shopper.common.Constant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by ifranseda on 9/15/15.
 */
public class PaymentReviewListFragment extends BaseRecyclerFragment {
    public static final String SHIPMENT_ID = "com.shopper.PaymentReviewListFragment.SHIPMENT_ID";

    View mStartButtonContainer;
    Button mNextButton;
    View mProgressContainer;

    Shipment mShipment;
    LineItemAdapter mAdapter;
    List<LineItem> mLineItems = new ArrayList<LineItem>();

    public static PaymentReviewListFragment newInstance(Long shipmentId) {
        PaymentReviewListFragment fragment = new PaymentReviewListFragment();

        Bundle bundle = new Bundle();
        bundle.putLong(PaymentReviewListFragment.SHIPMENT_ID, shipmentId);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public boolean isSwipeRefreshLayoutEnabled() {
        return false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.job_detail_recycler_list, null);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mStartButtonContainer = view.findViewById(R.id.bottom);
        mNextButton = (Button) view.findViewById(R.id.start_job_button);
        mProgressContainer = view.findViewById(R.id.progress_container);

        mProgressContainer.setVisibility(View.GONE);
        mStartButtonContainer.setVisibility(View.GONE);
        mNextButton.setVisibility(View.GONE);

        mAdapter = new LineItemAdapter(getActivity(), mLineItems, Constant.PAYMENT_REVIEW_LIST);
        mRecyclerView.setAdapter(mAdapter);

        loadData();
    }

    private void loadData() {
        long shipmentId = getArguments().getLong(PaymentReviewListFragment.SHIPMENT_ID);
        mShipment = Shipment.findById(shipmentId);

        mLineItems.clear();
        mAdapter.notifyDataSetChanged();

        if (mShipment != null) {
            List<LineItem> finalItems = new ArrayList<>(mShipment.getAllItems());
            Collections.sort(finalItems, new Comparator<LineItem>() {
                @Override
                public int compare(LineItem a, LineItem b) {
                    String a2 = (a.getTaxonName() != null) ? a.getTaxonName() : "";
                    String b2 = (b.getTaxonName() != null) ? b.getTaxonName() : "";
                    int c = a2.compareToIgnoreCase(b2);

                    if (c == 0) {
                        c = a.getVariant().getName().compareToIgnoreCase(b.getVariant().getName());
                    }

                    return c;
                }
            });

            Collections.sort(finalItems, new Comparator<LineItem>() {
                @Override
                public int compare(LineItem a, LineItem b) {
                    Integer a1 = a.getTaxonOrder();
                    Integer b1 = b.getTaxonOrder();
                    int c = b1.compareTo(a1);

                    if (c != 0) {
                        if (a1 > b1) return -1;
                        else return 1;
                    }

                    return c;
                }
            });

            mLineItems.addAll(finalItems);
            mAdapter.notifyDataSetChanged();
        }
    }
}
