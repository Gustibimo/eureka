package com.shopper.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.Batch;
import com.happyfresh.snowflakes.hoverfly.models.FreeItem;
import com.happyfresh.snowflakes.hoverfly.models.LineItem;
import com.happyfresh.snowflakes.hoverfly.models.RejectionItem;
import com.happyfresh.snowflakes.hoverfly.models.Shipment;
import com.happyfresh.snowflakes.hoverfly.models.payload.ListItemRejectionPayload;
import com.happyfresh.snowflakes.hoverfly.models.payload.RejectionPayload;
import com.happyfresh.snowflakes.hoverfly.utils.FormatterUtils;
import com.shopper.activities.DriverDeliveryListActivity;
import com.shopper.activities.DriverOneClickPaymentActivity;
import com.shopper.adapters.DeliveryItemAdapter;
import com.shopper.adapters.RejectionType;
import com.shopper.common.Constant;
import com.shopper.common.ShopperApplication;
import com.shopper.components.CircularProgressBar;
import com.shopper.interfaces.OnRejectedListener;
import com.shopper.listeners.DataListener;
import com.shopper.utils.LogUtils;
import com.shopper.utils.SharedPrefUtils;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by ifranseda on 4/27/15.
 */
public class DriverDeliveryListFragment extends BaseRecyclerFragment implements OnRejectedListener {

    @BindView(R.id.finalize_button)
    Button mFinalizeButton;

    @BindView(R.id.progress_container)
    View mProgressContainer;

    @BindView(R.id.progress_indicator)
    CircularProgressBar mProgressBar;

    @BindView(R.id.progress_label)
    TextView mProgressLabel;

    @BindView(R.id.happycorporate_layout)
    View happyCorporateLayout;

    @BindView(R.id.delivery_payment_layout)
    View deliveryPaymentLayout;

    @BindView(R.id.payment_method)
    TextView paymentMethodText;

    @BindView(R.id.total_order)
    TextView totalOrder;

    Shipment shipment;

    boolean finalized;

    DeliveryItemAdapter mAdapter;

    List<LineItem> lineItems = new ArrayList<>();

    List<RejectionItem> rejectedItems = new ArrayList<>();

    private Unbinder unbinder;

    public static DriverDeliveryListFragment newInstance() {
        DriverDeliveryListFragment fragment = new DriverDeliveryListFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            Long shipmentId = savedInstanceState.getLong("SHIPMENT_ID");
            shipment = Shipment.findById(shipmentId);

            finalized = savedInstanceState.getBoolean("FINALIZED");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putLong("SHIPMENT_ID", shipment.getRemoteId());
        outState.putBoolean("FINALIZED", finalized);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_delivery, null);
        unbinder = ButterKnife.bind(this, view);

        happyCorporateLayout.setVisibility(View.GONE);
        deliveryPaymentLayout.setVisibility(View.GONE);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public boolean isSwipeRefreshLayoutEnabled() {
        return false;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        reload();
    }

    private NumberFormat getNumberFormatter(String currencyCode) {
        return FormatterUtils.getNumberFormatter(currencyCode);
    }

    public void reload() {
        finalized = false;

        shipment = ShopperApplication.getInstance().getCurrentShipment();

        if (shipment == null) {
            return;
        }

        mAdapter = new DeliveryItemAdapter(getActivity(), lineItems);
        mAdapter.setOnRejectedListener(this);

        mRecyclerView.setAdapter(mAdapter);

        if (shipment.getOrder().getCompanyId() != 0) {
            happyCorporateLayout.setVisibility(View.VISIBLE);
            deliveryPaymentLayout.setVisibility(View.GONE);
        }
        else {
            if (shipment.getOrder().getPaymentMethod() != null && shipment.getOrder().getPaymentMethod()
                    .equals(Constant.CashOnDeliveryMethod)) {

                happyCorporateLayout.setVisibility(View.GONE);
                deliveryPaymentLayout.setVisibility(View.VISIBLE);

                deliveryPaymentLayout.setBackgroundColor(getResources().getColor(R.color.payment_cod_background));
                paymentMethodText.setText(R.string.payment_total_cod);

                paymentMethodText.setTextColor(getResources().getColor(android.R.color.black));
                totalOrder.setTextColor(ContextCompat.getColor(getActivity(), android.R.color.black));
            }
            else {
                happyCorporateLayout.setVisibility(View.GONE);
                deliveryPaymentLayout.setVisibility(View.VISIBLE);

                deliveryPaymentLayout.setBackgroundColor(getResources().getColor(R.color.payment_cc_background));
                paymentMethodText.setText(R.string.payment_total_credit_card);

                paymentMethodText.setTextColor(getResources().getColor(android.R.color.white));
                totalOrder.setTextColor(ContextCompat.getColor(getActivity(), android.R.color.white));
            }
        }

        String total = getNumberFormatter(shipment.getOrder().getCurrency()).format(shipment.getOrder().getTotal());
        totalOrder.setText(total);

        loadData();
    }

    @Override
    public void onRejected(final int position, RejectionType types, int quantity, String reason) {
        LineItem rejectedItem = lineItems.get(position);

        if ((rejectedItem.getTotalFreeItem() != 0 || rejectedItem
                .getTotalRejectedFreeItem() != 0) && types == RejectionType.ORIGINAL) {
            ShopperApplication application = ShopperApplication.getInstance();
            application.getBatchManager()
                    .rejectionsFreeItem(rejectedItem.getShipment().getRemoteId(),
                            rejectedItem.getVariant().getRemoteId(), quantity,
                            new DataListener<FreeItem>() {
                                @Override
                                public void onCompleted(boolean success, FreeItem data) {
                                    if (success) {
                                        LineItem rejectedItem = lineItems.get(position);

                                        rejectedItem.setTotalFreeItem(data.getTotalFreeItem());
                                        rejectedItem.setTotalRejectedFreeItem(data.getTotalRejectedFreeItem());
                                        mAdapter.notifyItemChanged(position);
                                    }
                                    else {
                                        Toast.makeText(getActivity(), getString(R.string.toast_unable_to_sync_data),
                                                Toast.LENGTH_LONG).show();
                                    }
                                }

                                @Override
                                public void onFailed(Throwable exception) {
                                    Toast.makeText(getActivity(), getString(R.string.toast_unable_to_sync_data),
                                            Toast.LENGTH_LONG).show();
                                    super.onFailed(exception);
                                }
                            });
        }
    }

    @Override
    public void onDiscarded(int position, RejectionType type, int previousQuantity) {

    }

    private void loadData() {
        /* Request for delivery list from Backend */
        mProgressContainer.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.INVISIBLE);
        mSwipeRefreshLayout.setRefreshing(true);

        ShopperApplication.getInstance().getShipmentManager().getAllItems(shipment.getRemoteId(), new DataListener() {
            @Override
            public void onCompleted(boolean success, Object data) {
                if (getActivity() != null) {
                    mProgressContainer.setVisibility(View.GONE);
                    mProgressBar.setVisibility(View.INVISIBLE);
                    mSwipeRefreshLayout.setRefreshing(false);

                    if (success) {
                        lineItems.clear();
                        mAdapter.notifyDataSetChanged();

                        if (data != null) {
                            List<LineItem> items = (List) data;
                            sortItems(items);
                            lineItems.addAll(new ArrayList(items));

                            shipment.setDeliveryItems(lineItems);
                            mAdapter.notifyDataSetChanged();
                            mAdapter.checkShoppingBag();
                        }
                    }
                }
            }
        });
    }

    private void sortItems(List<LineItem> items) {
        Collections.sort(items, new Comparator<LineItem>() {
            @Override
            public int compare(LineItem a, LineItem b) {
                String a2 = (a.getTaxonName() != null) ? a.getTaxonName() : "";
                String b2 = (b.getTaxonName() != null) ? b.getTaxonName() : "";
                int c = a2.compareToIgnoreCase(b2);

                if (c == 0) {
                    c = a.getVariant().getName().compareToIgnoreCase(b.getVariant().getName());
                }

                return c;
            }
        });

        Collections.sort(items, new Comparator<LineItem>() {
            @Override
            public int compare(LineItem a, LineItem b) {
                Integer a1 = a.getTaxonOrder();
                Integer b1 = b.getTaxonOrder();
                int c = b1.compareTo(a1);

                if (c != 0) {
                    if (a1 > b1) {
                        return -1;
                    }
                    else {
                        return 1;
                    }
                }

                return c;
            }
        });
    }

    @OnClick(R.id.finalize_button)
    void finalizeButtonOnClick() {
        finalizeItems();
    }

    void showFailedRejectDialog(Object data) {
        String str = getString(R.string.alert_message_failed_reject);
        if (data == null) {
            str = getString(R.string.connect_to_finalize);
        }

        MaterialDialog.Builder dialog = new MaterialDialog.Builder(getActivity()).title(R.string.alert_title_warning)
                .content(str).positiveText(R.string.alert_button_ok).negativeText(R.string.cancel_rejection)
                .cancelable(false).callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        cancelRejection();
                    }
                });

        dialog.build().show();
    }

    private void finalizeItems() {
        mFinalizeButton.setText(getString(R.string.driver_waiting));
        mFinalizeButton.setEnabled(false);

        ShopperApplication application = ShopperApplication.getInstance();

        List<RejectionPayload> payload = new ArrayList<>();
        StringBuilder rejectionLog = new StringBuilder();

        List<LineItem> deliveryItem = shipment.getDeliveryItems();
        for (LineItem item : deliveryItem) {
            for (RejectionItem i : item.rejectionItems()) {
                RejectionPayload p = i.toPayload();
                if (item.getReplacements() != null && item.getReplacements().size() > 0 && i.getVariantId()
                        .equals(item.getReplacements().get(0).getVariantId())) {
                    p.setReplacementId(item.getReplacements().get(0).getRemoteId());
                }
                payload.add(p);

                String info = p.getVariantId() + " - " + p.getQuantity() + " - " + p.getReason() + ";";
                rejectionLog.append(info);
            }
        }

        ListItemRejectionPayload rejectionPayload = new ListItemRejectionPayload();
        rejectionPayload.setItems(payload);

        if (rejectionPayload.getItems().size() > 0) {
            mProgressContainer.setVisibility(View.VISIBLE);
            mProgressBar.setVisibility(View.VISIBLE);

            LogUtils.logEvent("Finalize items", rejectionLog.toString());

            Batch batch = application.getBatch();

            application.getBatchManager()
                    .finalizeRejections(batch.getRemoteId(), shipment.getRemoteId(), rejectionPayload,
                            new DataListener<Shipment>() {
                                @Override
                                public void onCompleted(boolean success, Shipment data) {
                                    restoreButton();

                                    if (success) {
                                        if (data != null) {
                                            shipment.getOrder().setTotal(data.getOrder().getTotal());
                                            shipment.getOrder().save();
                                        }

                                        finalized = true;

                                        finishRejection();
                                    }
                                    else {
                                        showFailedRejectDialog(data);
                                    }
                                }

                                @Override
                                public void onFailed(Throwable exception) {
                                    final DriverDeliveryListActivity activity = (DriverDeliveryListActivity) getActivity();

                                    if (Constant.OrderCanceledException.equalsIgnoreCase(exception.getMessage())) {
                                        activity.showOrderCanceledDialog(Constant.CanceledDialogType.REJECT);
                                    }
                                    else {
                                        restoreButton();
                                        showFailedRejectDialog(exception);
                                    }
                                }
                            });
        }
        else {
            finishRejection();
        }
    }

    void restoreButton() {
        mFinalizeButton.setText(getString(R.string.driver_finalize));
        mFinalizeButton.setEnabled(true);

        mProgressContainer.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.INVISIBLE);
    }

    public void cancelRejection() {
        for (LineItem item : lineItems) {
            for (RejectionItem rejected : item.rejectionItems()) {
                boolean exists = false;
                for (RejectionItem saved : rejectedItems) {
                    if (saved.getItem().equals(rejected.getItem()) && saved.getVariantId()
                            .equals(rejected.getVariantId())) {
                        if (rejected.getQuantity() != saved.getQuantity()) {
                            rejected.delete();

                            rejected = new RejectionItem();
                            rejected.setItem(saved.getItem());
                            rejected.setReason(saved.getReason());
                            rejected.setQuantity(saved.getQuantity());
                            rejected.setVariantId(saved.getVariantId());
//                            rejected.persist();
                        }

                        exists = true;
                        break;
                    }
                }

                if (!exists) {
                    rejected.delete();
                }
            }
        }

        finishRejection();
    }

    private void finishRejection() {
        if (shipment.getOrder().getPaymentMethod().equals(Constant.CashOnDeliveryMethod) || shipment.getOrder()
                .isPaymentCompleted()) {
            finishActivity();
        }
        else {
            doOneClickPayment();
        }
    }

    private void finishActivity() {
        Intent result = new Intent();
        getActivity().setResult(Activity.RESULT_OK, result);
        getActivity().finish();
    }

    private void doOneClickPayment() {
        Intent intent = new Intent(getActivity(), DriverOneClickPaymentActivity.class);
        intent.putExtra(Constant.ORDER_NUMBER, shipment.getOrder().getNumber());
        intent.putExtra(Constant.IS_CC_PROMOTION_EXIST_AND_ELIGIBLE,
                shipment.getOrder().isCCPromotionExistAndEligible());
        startActivityForResult(intent, Constant.ONE_CLICK_PAY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constant.ONE_CLICK_PAY && resultCode == Activity.RESULT_OK) {
            SharedPrefUtils.writeInt(getActivity(), Constant.ONE_CLICK_PAY_RETRY_COUNT, 0);
            int requestData = data.getIntExtra(Constant.ONE_CLICK_PAY_RESULT, 1);
            if (requestData == Constant.PAYMENT_COMPLETED) {
                shipment.getOrder().setPaymentCompleted(true);
                shipment.getOrder().save();
            }
            else if (requestData == Constant.SWITCH_TO_COD) {
                shipment.getOrder().setPaymentMethod(Constant.CashOnDeliveryMethod);
                shipment.getOrder().save();
            }
            finishActivity();
        }
        else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
