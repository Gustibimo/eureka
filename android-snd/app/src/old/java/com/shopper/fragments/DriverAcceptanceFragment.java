package com.shopper.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.Shipment;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.BindView;

/**
 * Created by ifranseda on 8/6/15.
 */
public class DriverAcceptanceFragment extends Fragment implements ViewPager.OnPageChangeListener {

    @BindView(R.id.order_number)
    TextView orderNumber;

    @BindView(R.id.current_order_accepted)
    ImageView currentOrderAccepted;

    @BindView(R.id.accepted_orders)
    TextView acceptedOrders;

    private List<Shipment> mShipments = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_delivery_batch_indicator, null);
        ButterKnife.bind(this, view);

        return view;
    }

    public void setShipmentList(List<Shipment> shipments) {
        mShipments = new ArrayList<>(shipments);

        if (mShipments.size() > 0) {
            orderNumber.setText(mShipments.get(0).getOrder().getNumber());
            update(0);
        }
    }

    public void update(int position) {
        Shipment shipment = mShipments.get(position);
        orderNumber.setText(shipment.getOrder().getNumber());

        if (shipment.getDeliveryJob().getState().equals("accepted")) {
            currentOrderAccepted.setImageResource(R.drawable.icon_check_list);
        } else {
            currentOrderAccepted.setImageBitmap(null);
        }

        int acceptedCounter = 0;
        for (Shipment s : mShipments) {
            if (s.getDeliveryJob().getState().equals("accepted")) {
                acceptedCounter++;
            }
        }

        acceptedOrders.setText(String.format("%1$d/%2$d", acceptedCounter, mShipments.size()));
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        update(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }
}
