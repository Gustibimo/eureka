package com.shopper.fragments;


import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.LineItem;
import com.happyfresh.snowflakes.hoverfly.models.Product;
import com.happyfresh.snowflakes.hoverfly.models.ProductProperty;
import com.happyfresh.snowflakes.hoverfly.models.Shipment;
import com.happyfresh.snowflakes.hoverfly.models.StockLocation;
import com.happyfresh.snowflakes.hoverfly.models.Variant;
import com.shopper.activities.ReplacementShopperActivity;
import com.shopper.adapters.SearchProductAdapter;
import com.shopper.common.Constant;
import com.shopper.common.ShopperApplication;
import com.shopper.listeners.DataListener;
import com.happyfresh.snowflakes.hoverfly.models.response.ProductResponse;

import org.parceler.Parcels;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.BindView;
import butterknife.Unbinder;

/**
 * Created by julianagalag on 5/6/15.
 */
public class SearchProductFragment extends Fragment implements SearchProductAdapter.OnReplacementListener {
    public static final String LINE_ITEM = ".shopper.fragments.SearchProductFragment.LINE_ITEM";
    public static final String LINE_ITEM_VARIANT_ID = ".shopper.fragments.SearchProductFragment.LINE_ITEM_VARIANT_ID";
    public static final String LINE_ITEM_ORDER_ID = ".shopper.fragments.SearchProductFragment.LINE_ITEM_ORDER_ID";
    public static final String LINE_ITEM_STOCK_LOCATION_ID = ".shopper.fragments.SearchProductFragment.LINE_ITEM_STOCK_LOCATION_ID";
    public static final String SHIPMENT_ID = ".shopper.fragments.SearchProductFragment.SHIPMENT_ID";
    public static final String VARIANT = ".shopper.fragments.SearchProductFragment.VARIANT";
    private static int REQUEST_CODE = 100;
    @BindView(R.id.list)
    RecyclerView mList;
    @BindView(R.id.list_search_suggestion)
    RecyclerView mSearchSuggestionList;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.progress_container)
    View mProgressContainer;
    @BindView(R.id.replacement_not_found_text)
    TextView mReplacementNotFoundText;
    @BindView(R.id.replacement_suggestion_text)
    TextView mReplacementSuggestionText;
    private LinearLayoutManager mProductLayoutManager;
    private LinearLayoutManager mProductSuggestionLayoutManager;
    private Context mContext;
    private SearchView mSearchView;

    private SearchProductAdapter mAdapter;
    private SearchProductAdapter mSearchSuggestionAdapter;
    private Shipment mShipment;
    private LineItem mLineItem;

    private Unbinder unbinder;

    public static SearchProductFragment newInstance(Long shipmentId, LineItem lineItem) {
        SearchProductFragment fragment = new SearchProductFragment();

        Bundle bundle = new Bundle();
        bundle.putLong(Constant.SHIPMENT_ID, shipmentId);
        bundle.putLong(LINE_ITEM_VARIANT_ID, lineItem.getVariantId());
        bundle.putLong(LINE_ITEM_ORDER_ID, lineItem.getOrderId());
        bundle.putLong(LINE_ITEM_STOCK_LOCATION_ID, lineItem.getStockLocationId());
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = ShopperApplication.getContext();

        Long shipmentId = getArguments().getLong(Constant.SHIPMENT_ID);
        mShipment = Shipment.findById(shipmentId);

        Long lineItemVariantId = getArguments().getLong(LINE_ITEM_VARIANT_ID);
        Long lineItemOrderId = getArguments().getLong(LINE_ITEM_ORDER_ID);
        Long lineItemStockLocationId = getArguments().getLong(LINE_ITEM_STOCK_LOCATION_ID);

        mLineItem = LineItem.findByVariantIdOrderIdAndStockLocationId(lineItemVariantId, lineItemOrderId, lineItemStockLocationId);

        mAdapter = new SearchProductAdapter(mContext, mShipment, mLineItem);
        mAdapter.setOnReplaceListener(this);

        mSearchSuggestionAdapter = new SearchProductAdapter(mContext, mShipment, mLineItem);
        mSearchSuggestionAdapter.setOnReplaceListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_product, null);
        unbinder = ButterKnife.bind(this, view);
        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mProductLayoutManager = new LinearLayoutManager(getActivity());
        mList.setLayoutManager(mProductLayoutManager);
        mList.setAdapter(mAdapter);

        mProductSuggestionLayoutManager = new LinearLayoutManager(getActivity());
        mSearchSuggestionList.setLayoutManager(mProductSuggestionLayoutManager);
        mSearchSuggestionList.setAdapter(mSearchSuggestionAdapter);

        mReplacementNotFoundText.setVisibility(View.GONE);
        mReplacementSuggestionText.setVisibility(View.GONE);

        fetchSuggestions();
    }

    void clearSearchResult() {
        mAdapter.clear();
        mAdapter.notifyDataSetChanged();
        mReplacementNotFoundText.setVisibility(View.GONE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem searchMenuItem = menu.findItem(R.id.action_search);
        MenuItemCompat.expandActionView(searchMenuItem);

        mSearchView = (SearchView) MenuItemCompat.getActionView(searchMenuItem);
        mSearchView.setQueryHint(getString(R.string.eg_apple));

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                hideSoftKeyboard();
            }
        }, 100);

        SearchView.SearchAutoComplete searchAutoComplete = (SearchView.SearchAutoComplete) mSearchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchAutoComplete.setHintTextColor(ContextCompat.getColor(getActivity(), android.R.color.white));
        searchAutoComplete.setTextColor(ContextCompat.getColor(getActivity(), android.R.color.white));

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                search(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (s.length() == 0) {
                    clearSearchResult();
                    hideSearch();
                    showSuggestion();
                }

                return false;
            }
        });

//        MenuItemCompat.setOnActionExpandListener(searchMenuItem, new MenuItemCompat.OnActionExpandListener() {
//            @Override
//            public boolean onMenuItemActionExpand(MenuItem item) {
//                return false;
//            }
//
//            @Override
//            public boolean onMenuItemActionCollapse(MenuItem item) {
//                getActivity().finish();
//                return false;
//            }
//        });

        hideSoftKeyboard();
    }

    private void search(final String query) {
        if (query.length() == 0) {
            return;
        }

        hideSoftKeyboard();
        mProgressBar.setVisibility(View.VISIBLE);

        StockLocation stockLocation = mShipment.getSlot().getStockLocation();
        if (stockLocation == null) {
            Toast.makeText(getActivity(), R.string.toast_stock_location_empty, Toast.LENGTH_LONG).show();
            return;
        }

        ShopperApplication.getInstance().getProductManager().searchProduct(stockLocation.getRemoteId(), query, new DataListener<ProductResponse>() {
            @Override
            public void onCompleted(boolean success, ProductResponse productResponse) {
                if (getActivity() == null) return;

                if (success) {
                    mProgressBar.setVisibility(View.GONE);

                    List<Product> products = productResponse.getProducts();

                    if (products.size() > 0) {
                        mAdapter.addAll(products);
                        mAdapter.notifyDataSetChanged();
                        mReplacementSuggestionText.setVisibility(View.GONE);

                        showSearch();
                        hideSuggestion();
                    } else {
                        mReplacementNotFoundText.setText(getString(R.string.replacement_not_found, query));
                        mReplacementNotFoundText.setVisibility(View.VISIBLE);

                        hideSearch();
                        showSuggestion();
                    }
                } else {
                    mProgressBar.setVisibility(View.INVISIBLE);
                }
            }
        });

    }

    private void fetchSuggestions() {
        mProgressBar.setVisibility(View.VISIBLE);

        Long productId = null;
        List<ProductProperty> productProperties = mLineItem.getVariant().getProperties();
        if (productProperties.size() > 0) {
            productId = productProperties.get(0).getProductId();
        }

        if (productId == null) {
            mProgressBar.setVisibility(View.GONE);
            return;
        }

        StockLocation stockLocation = mShipment.getSlot().getStockLocation();
        if (stockLocation == null) {
            Toast.makeText(getActivity(), R.string.toast_stock_location_empty, Toast.LENGTH_LONG).show();
            return;
        }

        ShopperApplication.getInstance().getProductManager().getProductSuggestion(stockLocation.getRemoteId(), productId, new DataListener<ProductResponse>() {
            @Override
            public void onCompleted(boolean success, ProductResponse productResponse) {
                if (getActivity() == null) return;

                if (success) {
                    mProgressBar.setVisibility(View.GONE);

                    List<Product> productSuggestions = productResponse.getProducts();
                    if (productSuggestions.size() > 0) {
                        mSearchSuggestionAdapter.addAll(productResponse.getProducts());
                        hideSearch();
                        showSuggestion();
                    }
                } else {
                    mProgressBar.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onFailed(Throwable exception) {
                super.onFailed(exception);

                mProgressBar.setVisibility(View.INVISIBLE);
            }
        });

    }

    void showSuggestion() {
        if (mSearchSuggestionAdapter.getItemCount() > 0) {
            mReplacementSuggestionText.setVisibility(View.VISIBLE);
            mSearchSuggestionList.setVisibility(View.VISIBLE);
        }
    }

    void hideSuggestion() {
        mReplacementSuggestionText.setVisibility(View.GONE);
        mSearchSuggestionList.setVisibility(View.GONE);
    }

    void showSearch() {
        mList.setVisibility(View.VISIBLE);
    }

    void hideSearch() {
        mList.setVisibility(View.GONE);
    }

    void hideSoftKeyboard() {
        ((InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(mSearchView.getWindowToken(), 0);
    }

    @Override
    public void onReplacementClick(int position) {
        mProgressContainer.setVisibility(View.VISIBLE);

        Product product;
        if (mList.getVisibility() == View.VISIBLE) {
            product = mAdapter.getItem(position);
        } else {
            product = mSearchSuggestionAdapter.getItem(position);
        }

        Variant variant = product.getVariants().get(0);
        variant.setPrice(product.getPrice());
        variant.setDisplayPrice(product.getDisplayPrice());
        variant.setCostPrice(product.getPrice());
        variant.setDisplayCostPrice(product.getDisplayPrice());
        variant.setSupermarketUnitCostPrice(product.getSupermarketUnitCostPrice());
        variant.setDisplaySupermarketUnitCostPrice(product.getDisplaySupermarketUnitCostPrice());
        variant.setDisplayAverageWeight(product.getDisplayAverageWeight());
        variant.setNaturalAverageWeight(product.getNaturalAverageWeight());
        variant.setSupermarketUnit(product.getSupermarketUnit());

        Intent intent = new Intent(getActivity(), ReplacementShopperActivity.class);
        intent.putExtra(SHIPMENT_ID, mShipment.getRemoteId());
        intent.putExtra(SearchProductFragment.LINE_ITEM, Parcels.wrap(mLineItem));
        intent.putExtra(VARIANT, Parcels.wrap(variant));

        startActivityForResult(intent, REQUEST_CODE);

        getActivity().finish();
        getActivity().overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.stay_animation);
    }

}
