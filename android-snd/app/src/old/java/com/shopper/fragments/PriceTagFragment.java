package com.shopper.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.utils.FileUtils;
import com.shopper.common.Constant;
import com.shopper.utils.BitmapUtils;
import com.shopper.utils.LogUtils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import butterknife.ButterKnife;
import butterknife.BindView;
import butterknife.OnClick;
import retrofit.android.MainThreadExecutor;

/**
 * Created by kharda on 03/01/16.
 */

public class PriceTagFragment extends Fragment {

    private static final int CAMERA_REQUEST_CODE = 1001;

    private String mOrderNumber;

    private String mPriceTagFilePath;

    private boolean mImageSaved;

    @BindView(R.id.submit_image_button)
    Button submitButton;

    @BindView(R.id.open_camera_button)
    View openCameraButton;

    @BindView(R.id.camera_result_image)
    ImageView cameraResultImage;

    @BindView(R.id.camera_indicator)
    ImageView cameraIndicator;

    @BindView(R.id.hint_capture)
    View hintOpenCamera;

    @BindView(R.id.image_progress_bar)
    ProgressBar progressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getActivity().getIntent() != null) {
            mOrderNumber = getActivity().getIntent().getStringExtra(Constant.ORDER_NUMBER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tag, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        openCamera();
    }

    public static PriceTagFragment newInstance() {
        return new PriceTagFragment();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case CAMERA_REQUEST_CODE: {
                if (resultCode == Activity.RESULT_OK) {
                    processCameraOutput();
                } else {
                    progressBar.setVisibility(View.GONE);
                    tryEnableButton();
                }

                break;
            }

            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    private void processCameraOutput() {
        final String priceTagPath = getPriceTagPath();
        LogUtils.LOG("Camera Output: " + priceTagPath);

        ThreadPoolExecutor executor = new ThreadPoolExecutor(1, 1, 300, TimeUnit.SECONDS,
                new ArrayBlockingQueue<Runnable>(1));
        executor.execute(new Runnable() {
            @Override
            public void run() {
                FileOutputStream out = null;
                try {
                    Bitmap output = BitmapFactory.decodeFile(priceTagPath);

                    int width = output.getWidth();
                    int height = output.getHeight();
                    int max = 1600;

                    if (width > height && width >= max) {
                        double aspectRatio = (double) height / (double) width;
                        width = max;
                        height = (int) (width * aspectRatio);
                    } else if (height >= max) {
                        double aspectRatio = (double) height / (double) width;
                        height = max;
                        width = (int) (height / aspectRatio);
                    }

                    LogUtils.LOG("Size >> " + width + " - " + height);

                    Bitmap resized = Bitmap.createScaledBitmap(output, width, height, true);

                    File aFile = new File(priceTagPath);
                    out = new FileOutputStream(aFile);
                    resized.compress(Bitmap.CompressFormat.JPEG, 90, out);

                    mImageSaved = true;

                    resized.recycle();
                    output.recycle();

                } catch (final OutOfMemoryError oom) {
                    oom.printStackTrace();

                    new MainThreadExecutor().execute(new Runnable() {
                        @Override
                        public void run() {
                            showErrorDialog(oom.getLocalizedMessage());
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    mImageSaved = false;
                } finally {
                    try {
                        if (out != null) {
                            out.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    new MainThreadExecutor().execute(new Runnable() {
                        @Override
                        public void run() {
                            if (mImageSaved) {
                                showPriceTagImage(priceTagPath);
                            }

                            tryEnableButton();
                        }
                    });
                }
            }
        });
    }

    private String getPriceTagPath() {
        if (mPriceTagFilePath == null) {
            mPriceTagFilePath = BitmapUtils.getPriceTagImagePath(getActivity(), mOrderNumber);
        }

        return mPriceTagFilePath;
    }

    private void showErrorDialog(String errorMessage) {
        MaterialDialog.Builder dialogBuilder = new MaterialDialog.Builder(getActivity());
        dialogBuilder.title(R.string.alert_title_error);
        dialogBuilder.content(errorMessage);
        dialogBuilder.positiveText(R.string.alert_button_ok);
        dialogBuilder.onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                mImageSaved = false;
            }
        });
        dialogBuilder.build().show();
    }

    private void showPriceTagImage(String imagePath) {
        try {
            File file = FileUtils.getFile(imagePath);
            Picasso.with(getActivity()).load(file).into(cameraResultImage, new Callback() {
                @Override
                public void onSuccess() {
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onError() {
                    progressBar.setVisibility(View.GONE);
                }
            });

            hintOpenCamera.setVisibility(View.GONE);

            cameraResultImage.setVisibility(View.VISIBLE);
            cameraIndicator.setVisibility(View.VISIBLE);
        } catch (FileNotFoundException | FileUtils.FileEmptyException e) {
            cameraResultImage.setVisibility(View.GONE);
            cameraIndicator.setVisibility(View.GONE);
            e.printStackTrace();
        }
    }

    private void tryEnableButton() {
        if (submitButton == null) {
            return;
        }

        if (mImageSaved) {
            submitButton.setEnabled(true);
        } else {
            submitButton.setEnabled(false);
        }
    }

    @OnClick(R.id.open_camera_button)
    void openCamera() {
        Uri cameraOutput = Uri.fromFile(new File(getPriceTagPath()));
        LogUtils.LOG("Camera Output >> " + cameraOutput);

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, cameraOutput);
        startActivityForResult(intent, CAMERA_REQUEST_CODE);
    }

    @OnClick(R.id.submit_image_button)
    void submitPriceChange() {
        finishActivity(mPriceTagFilePath);
    }

    private void finishActivity(String filePath) {
        Intent resultData = new Intent();
        resultData.putExtra(Constant.PRICE_TAG_PATH, filePath);
        getActivity().setResult(Activity.RESULT_OK, resultData);
        getActivity().finish();
    }
}
