package com.shopper.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.Batch;
import com.happyfresh.snowflakes.hoverfly.models.Shipment;
import com.happyfresh.snowflakes.hoverfly.models.Slot;
import com.shopper.activities.DriverArriveActivity;
import com.shopper.activities.DriverBatchDeliveryActivity;
import com.shopper.activities.FragmentActivity;
import com.shopper.adapters.BatchDeliveryAdapter;
import com.shopper.common.Constant;
import com.shopper.common.ShopperApplication;
import com.shopper.interfaces.OnCallCustomerListener;
import com.shopper.listeners.DataListener;
import com.shopper.utils.ActivityUtils;
import com.shopper.utils.LogUtils;
import com.shopper.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.BindView;
import butterknife.Unbinder;

import static com.shopper.common.Constant.CanceledDialogType.DELIVER;

/**
 * Created by ifranseda on 8/10/15.
 */
public class DriverBatchDeliveryFragment extends BaseRecyclerFragment {

    @BindView(R.id.delivery_time)
    TextView deliveryTime;

    @BindView(R.id.time_left)
    TextView timeLeft;

    @BindView(R.id.delivery_progress)
    TextView deliveryProgress;

    @BindView(R.id.progress_container)
    View progressContainer;

    private BatchDeliveryAdapter mAdapter;
    private List<Shipment> mShipmentList = new ArrayList<>();

    private Unbinder unbinder;

    public static DriverBatchDeliveryFragment newInstance() {
        return new DriverBatchDeliveryFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.driver_deliver_recycler_list, null);
        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public boolean isSwipeRefreshLayoutEnabled() {
        return false;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mAdapter = new BatchDeliveryAdapter(getActivity(), mShipmentList);
        mAdapter.setCallCustomerListener(new OnCallCustomerListener() {
            @Override
            public void onClick(Shipment shipment) {
                ((FragmentActivity) getActivity()).callCustomer(shipment);
            }
        });

        mAdapter.setStartDeliverListener(new BatchDeliveryAdapter.OnStartDeliverNow() {
            @Override
            public void onStart(Shipment shipment) {
                LogUtils.LOG("SHIPMENT >> " + shipment);
                deliverNow(shipment);
            }
        });

        mRecyclerView.setAdapter(mAdapter);

        updateView();
    }

    @Override
    public void onResume() {
        super.onResume();
        progressContainer.setVisibility(View.GONE);
    }

    private void updateView() {
        Batch batch = ShopperApplication.getInstance().getBatch();
        if (batch == null) {
            return;
        }

        Slot slot = batch.getSlot();
        if (slot != null) {
            deliveryTime.setText(StringUtils.showDeliveryTimeNoDate(slot.getStartTime(), slot.getEndTime()));
            timeLeft.setText(StringUtils.buildTimeDiffWithNow(getActivity(), slot.getEndTime()));
            deliveryProgress.setText(String.format("%1$d/%2$d", batch.getState().getProgress(), batch.getState().getTotal()));
        } else {
            LogUtils.logEvent("Update View", "Slot is null");
            deliveryTime.setText("Not available");
            timeLeft.setText("-");
            deliveryProgress.setText("-");
        }

        mShipmentList.clear();
        mAdapter.notifyDataSetChanged();

        mShipmentList.addAll(batch.shipments());
        mAdapter.notifyDataSetChanged();
    }

    private void deliverNow(Shipment shipment) {
        progressContainer.setVisibility(View.VISIBLE);

        Batch batch = ShopperApplication.getInstance().getBatch();
        ShopperApplication.getInstance().getBatchManager().deliver(batch.getRemoteId(), shipment.getRemoteId(), new DataListener<Batch>() {
            @Override
            public void onCompleted(boolean success, Batch data) {
                if (success) {
                    arriveScreen();

                    if (getActivity() != null) {
                        progressContainer.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailed(Throwable exception) {
                final DriverBatchDeliveryActivity activity = (DriverBatchDeliveryActivity) getActivity();
                if (activity != null) {
                    if (Constant.OrderCanceledException.equalsIgnoreCase(exception.getMessage())) {
                        activity.showOrderCanceledDialog(DELIVER);
                    } else {
                        Toast.makeText(activity, R.string.driver_deliver_error, Toast.LENGTH_LONG).show();
                        progressContainer.setVisibility(View.GONE);
                        updateView();
                    }
                }
            }
        });
    }

    private void arriveScreen() {
        ActivityUtils.startClearTop(getActivity(), DriverArriveActivity.class);
    }
}
