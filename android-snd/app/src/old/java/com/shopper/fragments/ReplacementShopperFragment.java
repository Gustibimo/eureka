package com.shopper.fragments;

import android.app.Activity;
import android.app.ListFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.Sprinkles;
import com.happyfresh.snowflakes.hoverfly.models.LineItem;
import com.happyfresh.snowflakes.hoverfly.models.Order;
import com.happyfresh.snowflakes.hoverfly.models.PriceAmendment;
import com.happyfresh.snowflakes.hoverfly.models.Replacement;
import com.happyfresh.snowflakes.hoverfly.models.Shipment;
import com.happyfresh.snowflakes.hoverfly.models.ShopperReplacement;
import com.happyfresh.snowflakes.hoverfly.models.SpreeImage;
import com.happyfresh.snowflakes.hoverfly.models.Variant;
import com.happyfresh.snowflakes.hoverfly.stores.UserStore;
import com.happyfresh.snowflakes.hoverfly.utils.ChatUtils;
import com.happyfresh.snowflakes.hoverfly.utils.FormatterUtils;
import com.shopper.activities.PriceTagActivity;
import com.shopper.adapters.SectionAdapter;
import com.shopper.common.Constant;
import com.shopper.common.ShopperApplication;
import com.shopper.listeners.DataListener;
import com.shopper.managers.ShipmentManager;
import com.shopper.utils.DialogUtils;
import com.shopper.utils.LogUtils;
import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import java.text.NumberFormat;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by julianagalag on 5/12/15.
 */
public class ReplacementShopperFragment extends ListFragment {

    public static final String NEW_PRICE = ".shopper.fragments.ReplacementShopperFragment.NEW_PRICE";

    @BindView(R.id.found_button)
    Button mFoundButton;

    AbsListView.OnScrollListener mOnScrollListener = new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView absListView, int i) {
        }

        @Override
        public void onScroll(AbsListView absListView, int i, int i2, int i3) {
            int scrollY = 0;
            View c = absListView.getChildAt(0);
            if (c != null) {
                scrollY = -c.getTop();
            }

            if (scrollY >= 50) {
                int alpha = scrollY - 50;
                if (scrollY > 255) alpha = 255;

                ColorDrawable colorDrawable = new ColorDrawable(getResources().getColor(R.color.happyfresh_green));
                colorDrawable.setAlpha(alpha);
                ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(colorDrawable);
            } else {
                ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
            }
        }
    };

    private LinkedHashMap<String, String> mContent = new LinkedHashMap<String, String>();

    private View mHeaderView;
    private View mFooterView;
    private ImageView mHeaderItemImage;
    private TextView mHeaderItemCounter;
    private TextView mHeaderItemAverageWeight;
    private TextView mHeaderVariantName;
    private TextView mHeaderFlag;
    private View mHeaderMismatch;

    private Long mShipmentId;
    private Long mLineItemVariantId;
    private Long mLineItemOrderId;
    private Long mLineItemStockLocationId;
    private Shipment mShipment;
    private LineItem mItem;

    private SectionAdapter mAdapter;
    private Variant mVariant;

    private MaterialDialog mPriceUpdateDialog;

    private Double mNewPrice;

    private Unbinder unbinder;

    public static ReplacementShopperFragment newInstance(Long shipmentId, LineItem lineItem, Variant variant) {
        ReplacementShopperFragment fragment = new ReplacementShopperFragment();

        Bundle bundle = new Bundle();
        bundle.putLong(SearchProductFragment.SHIPMENT_ID, shipmentId);
        bundle.putLong(SearchProductFragment.LINE_ITEM_VARIANT_ID, lineItem.getVariantId());
        bundle.putLong(SearchProductFragment.LINE_ITEM_ORDER_ID, lineItem.getOrderId());
        bundle.putLong(SearchProductFragment.LINE_ITEM_STOCK_LOCATION_ID, lineItem.getStockLocationId());
        bundle.putParcelable(SearchProductFragment.VARIANT, Parcels.wrap(variant));
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        if (savedInstanceState == null) {
            if (getArguments() != null) {
                getVariablesFromInstances(getArguments());
            }
        } else {
            mNewPrice = savedInstanceState.getDouble(NEW_PRICE, 0);
            getVariablesFromInstances(savedInstanceState);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putLong(SearchProductFragment.SHIPMENT_ID, mShipmentId);
        outState.putLong(SearchProductFragment.LINE_ITEM_VARIANT_ID, mLineItemVariantId);
        outState.putLong(SearchProductFragment.LINE_ITEM_ORDER_ID, mLineItemOrderId);
        outState.putLong(SearchProductFragment.LINE_ITEM_STOCK_LOCATION_ID, mLineItemStockLocationId);
        outState.putParcelable(SearchProductFragment.VARIANT, Parcels.wrap(mVariant));
        if (mNewPrice != null) {
            outState.putDouble(NEW_PRICE, mNewPrice);
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_settings).setVisible(false);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (mShipment.getShoppingJob() != null) {
            inflater.inflate(R.menu.item_detail_replacement_shopper, menu);

            if (mItem != null && mItem.getReplacementShopper() != null &&
                    mItem.getReplacementShopper().getRemoteId() == mVariant.getRemoteId()) {

                menu.findItem(R.id.action_cancel_add_replacement).setVisible(true);
                menu.findItem(R.id.action_price_change).setVisible(true);
            }

            toggleChatButton(menu.findItem(R.id.action_chat_customer));
        }

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_cancel_add_replacement:
                cancelShopperReplacement();
                return true;

            case R.id.action_price_change:
                showPriceChangeDialog();
                return true;

            case R.id.action_call_customer:
                callCustomer();
                return true;

            case R.id.action_call_operator:
                callOperator();
                return true;

            case R.id.action_chat_customer:
                chatCustomer();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_detail, null);
        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupViews(view);

        mHeaderMismatch.setVisibility(View.GONE);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null) {
            getVariablesFromInstances(savedInstanceState);
        }

        mFoundButton.setText(getResources().getString(R.string.item_replacement_found));

        if (Shipment.findById(mShipmentId).getShoppingJob() == null) {
            mFoundButton.setVisibility(View.GONE);

            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.MATCH_PARENT);
            layoutParams.setMargins(0, 0, 0, 0);
            getListView().setLayoutParams(layoutParams);
        }

        mAdapter = new SectionAdapter(getActivity());

        getListView().setOnScrollListener(mOnScrollListener);

        drawVariantInfo();
        drawFooter();

        generateData();
        drawDetail();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constant.PRICE_TAG_FRAGMENT && resultCode == Activity.RESULT_OK) {
            String priceTagPath = data.getStringExtra(Constant.PRICE_TAG_PATH);
            if (priceTagPath != null) {
                sendPriceChange(mNewPrice, priceTagPath);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void getVariablesFromInstances(Bundle savedInstanceState) {
        mShipmentId = savedInstanceState.getLong(SearchProductFragment.SHIPMENT_ID, 0);
        mLineItemVariantId = savedInstanceState.getLong(SearchProductFragment.LINE_ITEM_VARIANT_ID, 0);
        mLineItemOrderId = savedInstanceState.getLong(SearchProductFragment.LINE_ITEM_ORDER_ID, 0);
        mLineItemStockLocationId = savedInstanceState.getLong(SearchProductFragment.LINE_ITEM_STOCK_LOCATION_ID, 0);

        mVariant = Parcels.unwrap(savedInstanceState.getParcelable(SearchProductFragment.VARIANT));

        mShipment = Shipment.findById(mShipmentId);
        mItem = LineItem.findByVariantIdOrderIdAndStockLocationId(mLineItemVariantId, mLineItemOrderId,
                mLineItemStockLocationId);
    }

    void setupViews(View view) {
        // Setup all views, except replacement view
        mFoundButton = (Button) view.findViewById(R.id.found_button);

        mHeaderView = getActivity().getLayoutInflater().inflate(R.layout.layout_item_detail_header, null);

        mHeaderItemImage = (ImageView) mHeaderView.findViewById(R.id.item_image);
        mHeaderItemCounter = (TextView) mHeaderView.findViewById(R.id.item_counter);
        mHeaderItemAverageWeight = (TextView) mHeaderView.findViewById(R.id.average_weight);
        mHeaderVariantName = (TextView) mHeaderView.findViewById(R.id.item_variant_name);
        mHeaderFlag = (TextView) mHeaderView.findViewById(R.id.item_flag_info);
        mHeaderMismatch = mHeaderView.findViewById(R.id.item_flag_mismatch);

        mFooterView = getActivity().getLayoutInflater().inflate(R.layout.layout_item_footer, null);
    }

    @OnClick(R.id.found_button)
    void foundButtonClick() {
        replacementFoundDialog();
    }

    private void cancelShopperReplacement() {
        LogUtils.logEvent("Cancel Shopper Replacement", "Show cancel shopper replacement dialog");

        MaterialDialog.Builder inputDialog = new MaterialDialog.Builder(getActivity());
        inputDialog.title(R.string.alert_title_warning);

        inputDialog.content(R.string.alert_message_cancel_shopping);
        inputDialog.negativeText(R.string.alert_button_no);
        inputDialog.positiveText(R.string.alert_button_yes);
        inputDialog.callback(new MaterialDialog.ButtonCallback() {
            @Override
            public void onPositive(MaterialDialog dialog) {
                LogUtils.logEvent("Cancel Shopper Replacement", "Cancelling");
                replacementFound(0, null);
            }
        });

        inputDialog.show();
    }

    private void showPriceChangeDialog() {
        DialogUtils.OnPriceChangeListener priceChangeListener = new DialogUtils.OnPriceChangeListener() {
            @Override
            public void onPriceChange(String newPrice) {
                mNewPrice = Double.parseDouble(newPrice);
                openPriceTagActivity();
            }
        };

        if (mPriceUpdateDialog == null) {
            mPriceUpdateDialog = DialogUtils.getPriceChangeDialog(getActivity(),
                    mShipment.getOrder().getCurrency(), getPriceUnit(), priceChangeListener);
        }

        mPriceUpdateDialog.show();
    }

    private String getPriceUnit() {
        if (mItem.getNaturalAverageWeight() != null) {
            return mItem.getSupermarketUnit();
        } else {
            return mItem.getDisplayUnit();
        }
    }

    private void openPriceTagActivity() {
        Intent intent = new Intent(getActivity(), PriceTagActivity.class);
        intent.putExtra(Constant.ORDER_NUMBER, mShipment.getOrder().getNumber());
        startActivityForResult(intent, Constant.PRICE_TAG_FRAGMENT);
    }

    private void sendPriceChange(final double newPrice, String imagePath) {
        ShopperApplication.getInstance()
                .getShipmentManager()
                .changePrice(mItem, mVariant.getRemoteId(), newPrice, imagePath, new DataListener.AwaitListener() {
                    @Override
                    public void promise() {
                        PriceAmendment priceAmendment = new PriceAmendment();
                        priceAmendment.setVariantId(mItem.getVariantId());
                        priceAmendment.setOrderId(mItem.getOrderId());
                        priceAmendment.setStockLocationId(mItem.getStockLocationId());
                        priceAmendment.setReplacementId(mItem.getReplacementShopperId());
                        priceAmendment.setReplacement(String.valueOf(newPrice));
                        priceAmendment.setUserId(Long.toString(ShopperApplication.getInstance().getCurrentUser().getRemoteId()));
                        priceAmendment.save();
//                        priceAmendment.persist();

                        NumberFormat formatter = FormatterUtils.getNumberFormatter(mShipment.getOrder().getCurrency());
                        String newPriceStr = formatter.format(newPrice);

                        mAdapter.setPriceChanged(true);
                        mAdapter.setNewPrice(newPriceStr);
                        mAdapter.notifyDataSetChanged();
                    }
                });
    }

    private void callCustomer() {
        MaterialDialog.Builder inputDialog = new MaterialDialog.Builder(getActivity());
        inputDialog.title(R.string.alert_title_call_customer);

        final String phoneNumber = mShipment.getAddress().getPhoneNumber();
        String message = getString(R.string.alert_message_call_customer, mShipment.getCustomerName(), phoneNumber);
        inputDialog.content(Html.fromHtml(message));

        inputDialog.negativeText(R.string.alert_button_cancel);
        inputDialog.positiveText(R.string.alert_button_call);
        inputDialog.callback(new MaterialDialog.ButtonCallback() {
            @Override
            public void onPositive(MaterialDialog dialog) {
                callPhone(phoneNumber);
            }
        });

        inputDialog.show();
    }

    private void callOperator() {
        MaterialDialog.Builder inputDialog = new MaterialDialog.Builder(getActivity());
        inputDialog.title(R.string.alert_title_call_operator);
        inputDialog.content(R.string.alert_message_call_operator);

        inputDialog.negativeText(R.string.alert_button_cancel);
        inputDialog.positiveText(R.string.alert_button_call);
        inputDialog.callback(new MaterialDialog.ButtonCallback() {
            @Override
            public void onPositive(MaterialDialog dialog) {
//                callPhone("888");
            }
        });

        inputDialog.show();
    }

    private void callPhone(String number) {
        ShopperApplication.getInstance().callPhone(number);
    }

    private void chatCustomer() {
        UserStore userStore = Sprinkles.stores(UserStore.class);
        String shopperId = String.valueOf(userStore.getUserId());
        ChatUtils.openChatScreen(getActivity(), shopperId, mShipment);
    }

    private void replacementFoundDialog() {
        mFoundButton.setEnabled(false);

        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View alertView = inflater.inflate(R.layout.dialog_item_found, null);

        final EditText itemCounter = (EditText) alertView.findViewById(R.id.item_found_count);
        final TextView amountText = (TextView) alertView.findViewById(R.id.item_found_max_amount);
        final EditText itemActualWeight = (EditText) alertView.findViewById(R.id.item_actual_weight);
        final TextView expectedText = (TextView) alertView.findViewById(R.id.item_expected_weight);
        final TextView errorMessage = (TextView) alertView.findViewById(R.id.item_error_message_text);
        final ImageView errorIcon = (ImageView) alertView.findViewById(R.id.item_error_message_icon);
        final LinearLayout errorLayout = (LinearLayout) alertView.findViewById(R.id.item_error_message);
        final RelativeLayout itemActual = (RelativeLayout) alertView.findViewById(R.id.item_actual);
        final GradientDrawable itemActualWeightBackground = (GradientDrawable) (
                (LayerDrawable) itemActualWeight.getBackground()
        ).getDrawable(1);

        final Callable<Boolean> itemActualWeightCallable = new Callable<Boolean>() {
            int mMessageColor;
            Context mContext;

            public void warn() {
                mMessageColor = ContextCompat.getColor(mContext, R.color.notification_warn);
                itemActualWeight.setTextColor(mMessageColor);
                itemActualWeightBackground.setStroke(1, mMessageColor);

                errorMessage.setText(R.string.item_actual_weight_warn_message);
                errorIcon.setImageResource(R.drawable.icon_circle_question_mark);
                errorLayout.setVisibility(View.VISIBLE);
            }

            public void error() {
                mMessageColor = ContextCompat.getColor(mContext, R.color.notification_error);
                itemActualWeight.setTextColor(mMessageColor);
                itemActualWeightBackground.setStroke(1, mMessageColor);

                errorMessage.setText(R.string.item_actual_weight_max_message);
                errorIcon.setImageResource(R.drawable.icon_circle_error);
                errorLayout.setVisibility(View.VISIBLE);
            }

            public void required() {
                mMessageColor = ContextCompat.getColor(mContext, R.color.notification_error);
                itemActualWeightBackground.setStroke(1, mMessageColor);

                itemActualWeight.setTextColor(mMessageColor);
                errorLayout.setVisibility(View.GONE);
            }

            @Override
            public Boolean call() throws Exception {
                String text = itemActualWeight.getText().toString();
                mContext = ShopperApplication.getContext();

                if (text.isEmpty()) {
                    required();

                    return false;
                }

                Double actualWeight = Double.valueOf(text);
                Double expectedWeight;
                errorIcon.setVisibility(View.VISIBLE);
                String foundCount = itemCounter.getText().toString();
                if (foundCount.isEmpty()) {
                    expectedWeight = mVariant.getTotalNaturalAverageWeight(1);
                } else {
                    int count = Integer.valueOf(foundCount);
                    if (count == 0 && actualWeight == 0d) {
                        return true;
                    }
                    expectedWeight = mVariant.getTotalNaturalAverageWeight(count);
                }

                // above 50%;
                if (actualWeight >= expectedWeight * (1 + Constant.ACTUAL_WEIGHT_MAX_FACTOR)) {
                    error();

                    return false;
                }

                // above 10%
                if (actualWeight >= expectedWeight * (1 + Constant.ACTUAL_WEIGHT_WARN_FACTOR)) {
                    warn();

                    return true;
                }

                // bellow 50%
                if (actualWeight <= expectedWeight * (1 - Constant.ACTUAL_WEIGHT_MAX_FACTOR)) {
                    error();

                    return false;
                }

                // bellow 10%
                if (actualWeight <= expectedWeight * (1 - Constant.ACTUAL_WEIGHT_WARN_FACTOR)) {
                    warn();

                    return true;
                }


                mMessageColor = ContextCompat.getColor(mContext, R.color.black);
                itemActualWeight.setTextColor(mMessageColor);
                itemActualWeightBackground.setStroke(1, mMessageColor);

                errorLayout.setVisibility(TextView.GONE);

                return true;
            }
        };

        if (mVariant.hasNaturalUnit()) {
            itemActualWeight.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    try {
                        itemActualWeightCallable.call();
                    } catch (Exception ignored) {
                    }
                }
            });

            itemActualWeight.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        itemActualWeight.setHint(R.string.item_actual_weight);
                        try {
                            itemActualWeightCallable.call();
                        } catch (Exception e) {
                            LogUtils.logEvent("ActualWeight: ", e.getMessage());
                        }
                    } else {
                        itemActualWeight.setHint("");
                    }
                }
            });


            itemCounter.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    String value = itemCounter.getText().toString();
                    if (!hasFocus && !value.isEmpty()) {
                        int count = Integer.parseInt(value);
                        expectedText.setText(
                                getString(
                                        R.string.alert_item_expected_weight,
                                        mVariant.getTotalNaturalAverageWeight(count),
                                        mVariant.getSupermarketUnit()
                                )
                        );
                    }
                }
            });
        } else {
            itemActual.setVisibility(View.GONE);
        }

        if (mItem.getTotalReplacedShopper() > 0) {
            itemCounter.setText(String.valueOf(mItem.getTotalReplacedShopper()));
        }

        amountText.setText(getString(R.string.alert_item_found_amount, mItem.totalReplacementRemaining()));
        amountText.setVisibility(View.GONE);
        expectedText.setText(
                getString(
                        R.string.alert_item_expected_weight,
                        mVariant.getTotalNaturalAverageWeight(1),
                        mVariant.getSupermarketUnit()
                )
        );

        MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity());
        builder.title(R.string.alert_title_item_found);
        builder.customView(alertView, false);
        builder.autoDismiss(false);
        builder.onNegative(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                itemActualWeightBackground.setStroke(1, Color.BLACK);
                materialDialog.dismiss();
            }
        });

        builder.negativeText(R.string.alert_button_cancel);
        builder.positiveText(R.string.alert_button_ok);
        builder.onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                int foundCounter = -1;
                Double actualWeight = null;
                Boolean actualWeightValid = !mVariant.hasNaturalUnit();

                String counterText = itemCounter.getText().toString();
                if (!counterText.isEmpty()) {
                    foundCounter = Integer.valueOf(counterText);
                }

                String actualWeightText = itemActualWeight.getText().toString();
                if (!actualWeightText.isEmpty()) {
                    actualWeight = Double.valueOf(actualWeightText);
                }

                if (mVariant.hasNaturalUnit()) {
                    try {
                        actualWeightValid = itemActualWeightCallable.call();
                    } catch (Exception ignored) {
                    }
                    if (actualWeightValid) {
                        replacementFound(foundCounter, actualWeight);
                        itemActualWeightBackground.setStroke(1, Color.BLACK);
                        materialDialog.dismiss();
                    }
                } else if (foundCounter >= 0) {
                    replacementFound(foundCounter, null);
                    itemActualWeightBackground.setStroke(1, Color.BLACK);
                    materialDialog.dismiss();
                }
            }
        });

        MaterialDialog inputDialog = builder.build();
        inputDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        inputDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if (mFoundButton != null) {
                    mFoundButton.setEnabled(true);
                }
            }
        });
        inputDialog.show();

        itemCounter.requestFocus();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(inputDialog.getContext().INPUT_METHOD_SERVICE);
        imm.showSoftInput(itemCounter, InputMethodManager.SHOW_IMPLICIT);
    }

    private void replacementFound(final int amount, final Double actualWeight) {
        mFoundButton.setEnabled(false);

        Replacement replacement = new Replacement();
        replacement.setRemoteId(mVariant.getRemoteId());

        Variant currentShopperReplacement = mItem.getReplacementShopper();
        if (currentShopperReplacement != null) {
            if (currentShopperReplacement.getRemoteId() != mVariant.getRemoteId()) {
                mItem.deleteReplacementShopper();
            }
        }

        ShopperApplication.getInstance().getShipmentManager()
                .shopperReplaceItem(mShipmentId, mItem, replacement, amount, actualWeight,
                        new DataListener.AwaitListener() {
                            @Override
                            public void promise() {
                                mItem.setTotalShopped((mItem.getTotalShopped() - mItem.getTotalReplacedShopper()) + amount);
                                mItem.setTotalFlagged((mItem.getTotalFlagged() + mItem.getTotalReplacedShopper()) - amount);
                                mItem.setTotalReplaced((mItem.getTotalReplaced() - mItem.getTotalReplacedShopper()) + amount);
                                mItem.setTotalReplacedShopper(amount);

                                if (amount == 0) {
                                    mItem.setFlag(ShipmentManager.OUT_OF_STOCK);
                                }

                                mItem.save();

                                LineItem lineItem = LineItem.findByLineItem(mItem);
                                if (lineItem != null) {
                                    if (amount == 0) {
                                        lineItem.deleteReplacementShopper();
                                    } else {
                                        if (lineItem.getReplacementShopper() == null && lineItem.getTotalReplacedShopper() > 0) {
                                            lineItem.setReplacementShopper(mVariant);
                                            lineItem.save();

                                            ShopperReplacement replacement = new ShopperReplacement(lineItem.getVariantId(), lineItem.getOrderId(), lineItem.getStockLocationId(), mVariant.getRemoteId(), mVariant.getRemoteId());
                                            replacement.save();
                                        }
                                    }
                                }

                                closeOnCompleted();
                            }
                        });
    }

    private void closeOnCompleted() {
        if (getActivity() != null) {
            getActivity().finish();
        }
    }

    private void showAllItemCompletedDialog() {
        MaterialDialog.Builder completedDialog = new MaterialDialog.Builder(getActivity());
        completedDialog.title(R.string.alert_title_good_job);
        completedDialog.content(R.string.alert_message_good_job);

        completedDialog.positiveText(R.string.alert_button_ok);
        completedDialog.callback(new MaterialDialog.ButtonCallback() {
            @Override
            public void onPositive(MaterialDialog dialog) {
                closeOnCompleted();
            }
        });

        completedDialog.cancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                closeOnCompleted();
            }
        });

        completedDialog.show();
    }

    public void generateData() {
        LinkedHashMap<String, String> itemDetail = new LinkedHashMap<String, String>();

        itemDetail.put(getString(R.string.item_detail_label_sku_number), mVariant.getSku());
        if (mVariant.hasNaturalUnit()) { // Item with natural unit, e.g.: meat, fruit
            itemDetail.put(getString(R.string.item_detail_label_price_per_unit), String.format("%s/%s", getNumberFormatter().format(mVariant.getSupermarketUnitCostPrice()), "-"));
            itemDetail.put(getString(R.string.item_detail_label_customer_price), String.format("%s/%s", mVariant.getDisplayPrice(), mVariant.getDisplayAverageWeight()));
        } else { // Ordinary Item
            itemDetail.put(getString(R.string.item_detail_label_price_per_unit), getNumberFormatter().format(mVariant.getCostPrice()));
            itemDetail.put(getString(R.string.item_detail_label_customer_price), mVariant.getDisplayPrice());
        }

        itemDetail.put(getString(R.string.item_detail_label_unit), "-");
        itemDetail.put(getString(R.string.item_detail_label_notes), mVariant.getDescription());

        mContent = itemDetail;
    }

    public void drawDetail() {
        for (Map.Entry<String, String> content : mContent.entrySet()) {
            if (content.getKey().equals(getString(R.string.item_detail_label_notes))) {
                mAdapter.addItem(content, SectionAdapter.ViewType.TYPE_ITEM_VERTICAL);
            } else {
                mAdapter.addItem(content);
            }
        }
        setListAdapter(mAdapter);

        if (mItem == null) {
            return;
        }

        PriceAmendment priceAmendment = mItem.getPriceAmendmentShopperReplacement();
        if (priceAmendment != null && priceAmendment.getReplacement() != null) {
            NumberFormat formatter = FormatterUtils.getNumberFormatter(mShipment.getOrder().getCurrency());
            Double newPrice = Double.parseDouble(priceAmendment.getReplacement());

            String newPriceStr = formatter.format(newPrice);
            if (mItem.getReplacementShopper().hasNaturalUnit()) {
                newPriceStr = String.format("%s/%s", formatter.format(newPrice), mItem.getReplacementShopper().getUnits());
            }

            mAdapter.setPriceChanged(true);
            mAdapter.setNewPrice(newPriceStr);
            mAdapter.notifyDataSetChanged();
        }
    }

    private void drawVariantInfo() {
        List<SpreeImage> images = mVariant.getImages();
        String imageUrl = null;
        if (images.size() > 0) {
            imageUrl = images.get(0).getProductUrl();
        } else {
            images = mVariant.images();
            if (images.size() > 0) {
                imageUrl = images.get(0).getProductUrl();
            }
        }

        Picasso.with(getActivity()).cancelRequest(mHeaderItemImage);
        Picasso.with(getActivity()).load(imageUrl).placeholder(R.drawable.default_product).into(mHeaderItemImage);

        mHeaderVariantName.setText(mVariant.getName());

        mHeaderFlag.setVisibility(View.GONE);

        updatePurchasedCount();

        getListView().addHeaderView(mHeaderView);
    }

    private void drawFooter() {
        getListView().addFooterView(mFooterView);
    }

    private void updatePurchasedCount() {
        if (mItem != null) {
            mHeaderItemCounter.setText(String.valueOf(mItem.getTotalReplacedShopper()));

            if (mVariant.hasNaturalUnit()) {
                mHeaderItemAverageWeight.setText(mVariant.getDisplayAverageWeight());
                mHeaderItemAverageWeight.setVisibility(View.VISIBLE);
            } else {
                mHeaderItemAverageWeight.setVisibility(View.GONE);
            }
        }
    }

    private NumberFormat getNumberFormatter() {
        return FormatterUtils.getNumberFormatter(mShipment.getOrder().getCurrency());
    }

    private void toggleChatButton(MenuItem chatMenuItem) {
        Order order = mShipment.getOrder();
        boolean chatEnable = order != null && order.isChatEnabled();

        if (!chatEnable) {
            chatMenuItem.setVisible(false);
        }
    }
}
