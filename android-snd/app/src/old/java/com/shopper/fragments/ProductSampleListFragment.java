package com.shopper.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.Batch;
import com.happyfresh.snowflakes.hoverfly.models.ProductSampleItem;
import com.happyfresh.snowflakes.hoverfly.models.Shipment;
import com.happyfresh.snowflakes.hoverfly.models.StockLocation;
import com.shopper.activities.BatchPaymentActivity;
import com.shopper.activities.PaymentReceiptActivity;
import com.shopper.activities.ProductSampleListActivity;
import com.shopper.activities.SampleItemDetailActivity;
import com.shopper.adapters.ProductSampleListAdapter;
import com.shopper.common.Constant;
import com.shopper.common.ShopperApplication;
import com.shopper.events.FlaggedEvent;
import com.shopper.events.OrderCanceledEvent;
import com.shopper.events.ShopNowEvent;
import com.shopper.events.ShoppedEvent;
import com.shopper.interfaces.OnCreateOptionMenuListener;
import com.shopper.listeners.DataListener;
import com.shopper.listeners.RecyclerItemTouchListener;
import com.shopper.receivers.PoisonPillReceiver;
import com.shopper.utils.AppUtils;
import com.shopper.utils.LogUtils;
import com.shopper.utils.SampleUtils;
import com.shopper.utils.SharedPrefUtils;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.shopper.common.Constant.BATCH_ID;
import static com.shopper.common.Constant.SELECTED_SHOPPING_STORE;
import static com.shopper.common.Constant.STOCK_LOCATION_ID;

/**
 * Created by kharda on 9/7/16.
 */
public class ProductSampleListFragment extends BaseRecyclerFragment implements OnCreateOptionMenuListener {

    private static int REQUEST_CODE = 100;

    boolean isRefreshable = false;

    View mStartButtonContainer;

    Button mNextButton;

    View mProgressContainer;

    StockLocation mStockLocation;

    Batch mBatch;

    ProductSampleListAdapter mAdapter;

    List<ProductSampleItem> mSampleItems = new ArrayList<>();

    private boolean mIsOutOfStore = false;

    private Shipment mShipment;

    private RecyclerItemTouchListener mItemTouchListener = new RecyclerItemTouchListener(getProductSampleActivity()) {
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
        }

        @Override
        public void onTouchItem(View view, int position) {
            if (position < 0 || mSampleItems.size() < position) {
                return;
            }

            long hasActiveStore = SharedPrefUtils.getLong(getActivity(), SELECTED_SHOPPING_STORE);
            if (hasActiveStore == 0) {
                return;
            }

            Intent intent = new Intent(getProductSampleActivity(), SampleItemDetailActivity.class);
            intent.putExtra(SampleItemDetailActivity.ITEM_ID, mSampleItems.get(position).getProductId());
            intent.putExtra(SampleItemDetailActivity.ORDER_NUMBER, mSampleItems.get(position).getOrderNumber());
            startActivityForResult(intent, REQUEST_CODE);
            getProductSampleActivity().overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.stay_animation);
        }
    };

    public static ProductSampleListFragment newInstance(StockLocation stockLocation) {
        Bundle bundle = new Bundle();
        bundle.putLong(STOCK_LOCATION_ID, stockLocation.getRemoteId());

        ProductSampleListFragment fragment = new ProductSampleListFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    public ProductSampleListActivity getProductSampleActivity() {
        return (ProductSampleListActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.job_detail_recycler_list, null);
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mIsOutOfStore = getActivity().getIntent().getExtras().getBoolean(Constant.IS_OUT_OF_STORE);
        if (!mIsOutOfStore) {
            mBatch = ShopperApplication.getInstance().getBatch();
        } else {
            long batchId = getActivity().getIntent().getExtras().getLong(ProductSampleListActivity.SELECTED_BATCH);
            mBatch = Batch.findById(batchId);
            long shipmentId = getActivity().getIntent().getExtras().getLong(PaymentReceiptActivity.SHIPMENT_ID);
            mShipment = Shipment.findById(shipmentId);
        }

        setHasOptionsMenu(true);

        long stockLocationId = 0L;
        if (savedInstanceState != null) {
            stockLocationId = savedInstanceState.getLong(STOCK_LOCATION_ID, 0);
        } else {
            if (getArguments() != null) {
                stockLocationId = getArguments().getLong(STOCK_LOCATION_ID, 0);
            }
        }

        if (stockLocationId > 0) {
            mStockLocation = StockLocation.findById(stockLocationId);
        }

        if (getProductSampleActivity().getSupportActionBar() != null) {
            getProductSampleActivity().getSupportActionBar().setDisplayShowHomeEnabled(false);
        }

        // Disable app navigation as selected shipment/job is an active, or is in shopping mode
        if (mBatch != null) {
            removeBackStacks();
        }
        isRefreshable = (SharedPrefUtils.getLong(getActivity(), SELECTED_SHOPPING_STORE) > 0);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putLong(STOCK_LOCATION_ID, mStockLocation.getRemoteId());
        if (mIsOutOfStore) {
            outState.putLong(ProductSampleListActivity.SELECTED_BATCH, mBatch.getRemoteId());
            outState.putLong(PaymentFragment.SHIPMENT_ID, mShipment.getRemoteId());
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getProductSampleActivity().setupShoppingTimer(menu);

        if (mBatch != null) {
            inflater.inflate(R.menu.job, menu);

            menu.findItem(R.id.action_switch_job).setVisible(false);
            menu.findItem(R.id.action_cancel_job).setVisible(false);
        }
    }

    @Override
    public void onOptionMenuCreated() {
        getProductSampleActivity().startTimer();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return true;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mStartButtonContainer = view.findViewById(R.id.bottom);
        initNextButton(view);

        mProgressContainer = view.findViewById(R.id.progress_container);
        mProgressContainer.setVisibility(View.GONE);

        mAdapter = new ProductSampleListAdapter(getProductSampleActivity(), mSampleItems);
        int c = mAdapter.getItemCount();

        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addOnItemTouchListener(mItemTouchListener);

        getProductSampleActivity().setOnCreateOptionMenuListener(this);

        if (savedInstanceState == null) {
            fetchProductSampleList();
        }
    }

    private void initNextButton(View view) {
        mNextButton = (Button) view.findViewById(R.id.start_job_button);
        setNextButtonOnClickListener();
    }

    private void setNextButtonOnClickListener() {
        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postSampleLineItems();
            }
        });
    }

    private void postSampleLineItems() {
        if (mIsOutOfStore) {
            if (ProductSampleItem.isThereInStoreSample(mBatch.getRemoteId())) {
                postSampleLineItemInStore();
            }
            postSampleLineItemOutOfStore();
        } else {
            paymentScreen();
        }
    }

    private void postSampleLineItemInStore() {
        new SampleUtils(mBatch).setIsOutOfStore(false).withStockLocation(mStockLocation).retrieveSamples()
                .createThenPostPayload(new DataListener<Boolean>() {
                    @Override
                    public void onCompleted(boolean success, Boolean sampleExist) {
                        if (!success) {
                            if (getActivity() != null) {
                                Toast.makeText(ShopperApplication.getContext(),
                                        getString(R.string.toast_unable_post_product_sample_list), Toast.LENGTH_LONG)
                                        .show();
                                showProgress(false);
                            }
                        }
                    }
                });
    }

    private void postSampleLineItemOutOfStore() {
        new SampleUtils(mBatch).setIsOutOfStore(true).withStockLocation(mStockLocation).retrieveSamples()
                .createThenPostPayload(new DataListener<Boolean>() {
                    @Override
                    public void onCompleted(boolean success, Boolean sampleExist) {
                        if (success) {
                            backToShipmentList();
                        } else {
                            if (getActivity() != null) {
                                Toast.makeText(ShopperApplication.getContext(),
                                        getString(R.string.toast_unable_post_product_sample_list), Toast.LENGTH_LONG)
                                        .show();
                                showProgress(false);
                            }
                        }

                    }

                    @Override
                    public void onFailed(Throwable exception) {
                        backToShipmentList();
                    }
                });
    }

    private void backToShipmentList() {
        if (getActivity() != null) {
            Batch batch = ShopperApplication.getInstance().getBatch();
            if (batch.hasPayAllShopping()) {
                ProductSampleListActivity productSampleListActivity = getProductSampleActivity();
                productSampleListActivity.backToShipmentList();
            } else {
                if (AppUtils.currentUserType() == AppUtils.UserType.RANGER) {
                    if (batch.hasPayAllShoppingInStore(mShipment.getSlot().getStockLocation().getRemoteId())) {
                        SharedPrefUtils.remove(getActivity(), SELECTED_SHOPPING_STORE);
                    } else {
                        getActivity().finish();
                    }
                } else {
                    getActivity().finish();
                }
            }
        }
    }

    @Override
    public boolean isSwipeRefreshLayoutEnabled() {
        return isRefreshable;
    }

    @Override
    public void onSwipeRefresh() {
        fetchProductSampleList();
    }

    @Override
    public void onResume() {
        super.onResume();
        ShopperApplication.getInstance().getBus().register(this);

        if (mBatch == null || mBatch.shipments().size() == 0) {
            mNextButton.setVisibility(View.GONE);
            mStartButtonContainer.setVisibility(View.GONE);
            return;
        } else {
            mNextButton.setVisibility(View.VISIBLE);
            mStartButtonContainer.setVisibility(View.VISIBLE);
        }

        loadData();
    }

    @Override
    public void onPause() {
        super.onPause();
        ShopperApplication.getInstance().getBus().unregister(this);
    }

    @Subscribe
    public void onShopNowEvent(ShopNowEvent e) {
        isRefreshable = e.isEnabled();
        updateSwipeRefresh();
    }

    @Subscribe
    public void onShoppedEvent(ShoppedEvent e) {
        LogUtils.LOG("ShoppedEvent >> " + e);
        loadData();
    }

    @Subscribe
    public void onFlaggedEvent(FlaggedEvent e) {
        LogUtils.LOG("FlaggedEvent >> " + e);
        loadData();
    }

    @Subscribe
    public void onOrderCanceledEvent(OrderCanceledEvent e) {
        getProductSampleActivity().showOrderCanceledDialog(e.getDialogType());
    }

    private void fetchProductSampleList() {
        mSampleItems.clear();
        mAdapter.notifyDataSetChanged();

        mSwipeRefreshLayout.setRefreshing(true);

        loadData();

        if (getProductSampleActivity() != null) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    private void updateNavigationButton() {
        for (ProductSampleItem sampleItem : mSampleItems) {
            if (sampleItem.getFlag() == null && sampleItem.getTotalShopped() == 0) {
                setNextButtonText();
                toggleNextButton(false);
                return;
            }
        }

        setNextButtonText();
        toggleNextButton(true);
    }

    private void setNextButtonText() {
        if (mIsOutOfStore && AppUtils.currentUserType() != AppUtils.UserType.RANGER) {
            mNextButton.setText(R.string.product_sample_finalize);
        } else {
            mNextButton.setText(R.string.next_job);
        }
    }

    private void toggleNextButton(boolean enable) {
        if (enable) {
            mNextButton.setEnabled(true);
            mNextButton
                    .setBackgroundColor(ContextCompat.getColor(getProductSampleActivity(), R.color.happyfresh_green));
        } else {
            mNextButton.setEnabled(false);
            mNextButton.setBackgroundColor(
                    ContextCompat.getColor(getProductSampleActivity(), R.color.happyfresh_green_disabled_text_color));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CODE) {
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    private void removeBackStacks() {
        if (getProductSampleActivity() != null) {
            if (getProductSampleActivity().getSupportActionBar() != null) {
                getProductSampleActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getProductSampleActivity().invalidateOptionsMenu();
            }
        }

        PoisonPillReceiver.finishAll(getProductSampleActivity());
    }

    private void loadData() {
        mSampleItems.clear();
        mAdapter.notifyDataSetChanged();

        if (mBatch != null) {
            List<ProductSampleItem> finalSampleItems;
            if (!mIsOutOfStore) {
                finalSampleItems = new ArrayList<>(ProductSampleItem.findByBatchIdAndFilterByInStore(mBatch.getRemoteId()));
            } else {
                finalSampleItems = new ArrayList<>(ProductSampleItem.findByBatchIdAndFilterByOutStore(mBatch.getRemoteId()));
            }

            Collections.sort(finalSampleItems, new Comparator<ProductSampleItem>() {
                @Override
                public int compare(ProductSampleItem a, ProductSampleItem b) {
                    String a2 = (a.getTaxonName() != null) ? a.getTaxonName() : "";
                    String b2 = (b.getTaxonName() != null) ? b.getTaxonName() : "";
                    int c = a2.compareToIgnoreCase(b2);

                    if (c == 0 && a.getVariant() != null && b.getVariant() != null) {
                        c = a.getVariant().getName().compareToIgnoreCase(b.getVariant().getName());
                    }

                    return c;
                }
            });

            Collections.sort(finalSampleItems, new Comparator<ProductSampleItem>() {
                @Override
                public int compare(ProductSampleItem a, ProductSampleItem b) {
                    Integer a1 = a.getTaxonOrder();
                    Integer b1 = b.getTaxonOrder();
                    int c = b1.compareTo(a1);

                    if (c != 0) {
                        if (a1 > b1) {
                            return -1;
                        } else {
                            return 1;
                        }
                    }

                    return c;
                }
            });

            mSampleItems.addAll(finalSampleItems);

            mAdapter.notifyDataSetChanged();

            updateNavigationButton();

            try {
                LogUtils.logEvent("Batch ID", ">> " + mBatch.getRemoteId());

                for (Shipment s : mBatch.shipments()) {
                    if (s.getOrder() != null) {
                        LogUtils.logEvent("ShoppingList", "Order Number: " + s.getOrder().getNumber());
                    }
                }

                StringBuffer stringBuffer = new StringBuffer();
                for (ProductSampleItem item : finalSampleItems) {
                    if (item.getVariant() != null) {
                        stringBuffer.append(item.getVariant().getName() + "; ");
                    }
                }
                LogUtils.logEvent("ShoppingList", "Items: " + stringBuffer.toString());
            } catch (Exception e) {
            }
        }
    }

    private void paymentScreen() {
        Intent intent = new Intent(getProductSampleActivity(), BatchPaymentActivity.class);
        intent.putExtra(BATCH_ID, mBatch.getRemoteId());
        intent.putExtra(STOCK_LOCATION_ID, mStockLocation.getRemoteId());
        startActivity(intent);
    }

    public void showProgress(boolean show) {
        mProgressContainer.setVisibility(show ? View.VISIBLE : View.GONE);
    }
}
