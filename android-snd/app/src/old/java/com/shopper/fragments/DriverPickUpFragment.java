package com.shopper.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.Batch;
import com.happyfresh.snowflakes.hoverfly.models.BatchState;
import com.happyfresh.snowflakes.hoverfly.models.Shipment;
import com.happyfresh.snowflakes.hoverfly.models.Slot;
import com.happyfresh.snowflakes.hoverfly.models.StockLocation;
import com.shopper.activities.DriverAcceptanceActivity;
import com.shopper.activities.DriverListActivity;
import com.shopper.activities.FragmentActivity;
import com.shopper.adapters.ShipmentAdapter;
import com.shopper.common.Constant;
import com.shopper.common.ShopperApplication;
import com.shopper.components.CircularProgressBar;
import com.shopper.interfaces.OnCallCustomerListener;
import com.shopper.listeners.DataListener;
import com.happyfresh.snowflakes.hoverfly.models.response.ExceptionResponse;
import com.shopper.utils.ActivityUtils;
import com.shopper.utils.AppUtils;
import com.shopper.utils.LogUtils;
import com.shopper.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Isnan Franseda on 08/07/2015.
 */
public class DriverPickUpFragment extends BaseRecyclerFragment {

    final int errorNotFinished = 1;
    final int errorCancelled = 2;

    @BindView(R.id.progress_container)
    View mProgressContainer;

    @BindView(R.id.progress_indicator)
    CircularProgressBar mProgressBar;

    @BindView(R.id.progress_label)
    TextView mProgressLabel;

    @BindView(R.id.delivery_time)
    TextView deliveryTime;

    @BindView(R.id.time_left)
    TextView timeLeft;

    @BindView(R.id.pickup_button)
    Button mPickUpButton;

    @BindView(R.id.pickup_indicator)
    CircularProgressBar mPickUpIndicator;

    ShipmentAdapter mAdapter;
    List<Shipment> mShipmentList = new ArrayList();
    boolean isRefreshing;

    private Unbinder unbinder;

    public static DriverPickUpFragment newInstance() {
        return new DriverPickUpFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_driver, null);
        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public boolean isSwipeRefreshLayoutEnabled() {
        return false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mAdapter = new ShipmentAdapter(getActivity(), mShipmentList);
        mAdapter.setCallCustomerListener(new OnCallCustomerListener() {
            @Override
            public void onClick(Shipment shipment) {
                ((FragmentActivity) getActivity()).callCustomer(shipment);
            }
        });
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();

        isRefreshing = false;
        updateShipment();
    }

    public void cancelBooking() {
        LogUtils.logEvent("Cancel Booking", "Initiate cancel request");
        if (mProgressContainer != null) {
            mProgressContainer.setVisibility(View.VISIBLE);
            mProgressBar.setVisibility(View.VISIBLE);
            mProgressLabel.setText(R.string.cancelling_job);
        }

        final ShopperApplication application = ShopperApplication.getInstance();
        application.clearQueue();

        Batch batch = application.getBatch();
        if (batch == null) return;

        Long batchId = batch.getRemoteId();
        ShopperApplication.getInstance().getBatchManager().cancel(batchId, new DataListener<Batch>() {
            @Override
            public void onCompleted(boolean success, Batch data) {
                if (data != null) {
                    application.setBatch(null);
                    driverJobListScreen();

                    if (success) {
                        LogUtils.logEvent("Cancel delivery", "Cancelled successfully");
                    }
                } else {
                    if (!success) {
                        Toast.makeText(getActivity(), getString(R.string.toast_cannot_cancel_without_internet), Toast.LENGTH_LONG).show();
                    }
                }

                if (getActivity() != null) {
                    mProgressContainer.setVisibility(View.GONE);
                    mProgressBar.setVisibility(View.GONE);
                }
            }
        });
    }

    public void updateShipment() {
        if (isRefreshing) {
            return;
        }

        if (mProgressContainer != null) {
            updateView();

            mPickUpIndicator.setVisibility(View.GONE);
            mProgressContainer.setVisibility(View.VISIBLE);
            mProgressBar.setVisibility(View.GONE);

            mSwipeRefreshLayout.setRefreshing(true);
        }

        isRefreshing = true;
        ShopperApplication.getInstance().getBatchManager().fetchActive(false, new DataListener<Batch>() {
            @Override
            public void onCompleted(boolean success, Batch data) {
                isRefreshing = false;

                if (data != null) {
                    if (StockLocation.StoreType.SPECIALTY.equals(data.getSlot().getStockLocation().getStoreType())) {
                        AppUtils.changeUserType(AppUtils.UserType.RANGER);
                    } else {
                        AppUtils.changeUserType(AppUtils.UserType.DRIVER);
                    }
                }

                if (getActivity() != null) {
                    mProgressContainer.setVisibility(View.GONE);
                    mSwipeRefreshLayout.setRefreshing(false);
                }

                if (success && data != null) {
                    if (BatchState.State.AVAILABLE.equals(data.state())) {
                        driverJobListScreen();
                    } else {
                        if (getActivity() != null) {
                            mShipmentList.clear();
                            mAdapter.notifyDataSetChanged();

                            mShipmentList.addAll(data.shipments());
                            mAdapter.notifyDataSetChanged();

                            updateView();
                        }
                    }
                } else {
                    driverJobListScreen();
                }
            }

            @Override
            public void onFailed(Throwable exception) {
                isRefreshing = false;

                if (getActivity() != null) {
                    mProgressContainer.setVisibility(View.GONE);
                    mSwipeRefreshLayout.setRefreshing(false);
                }

                updateView();
            }
        });
    }

    private void updateView() {
        if (getActivity() == null) {
            return;
        }

        getActivity().invalidateOptionsMenu();

        Batch batch = ShopperApplication.getInstance().getBatch();

        if (batch == null) {
            mProgressContainer.setVisibility(View.GONE);
            mPickUpButton.setVisibility(View.GONE);
            mPickUpIndicator.setVisibility(View.GONE);
        } else {
            mProgressContainer.setVisibility(View.GONE);
            mPickUpButton.setVisibility(View.VISIBLE);
            mPickUpIndicator.setVisibility(View.GONE);

            mPickUpButton.setText(R.string.driver_pick_up);

            mShipmentList.clear();
            mAdapter.notifyDataSetChanged();

            mShipmentList.addAll(batch.shipments());
            mAdapter.notifyDataSetChanged();

            if (batch.shipments().size() > 0) {
                mPickUpButton.setEnabled(true);
            } else {
                mPickUpButton.setEnabled(false);
            }

            Slot slot = batch.getSlot();
            if (slot != null) {
                deliveryTime.setText(StringUtils.showDeliveryTime(slot.getStartTime(), slot.getEndTime()));
                timeLeft.setText(StringUtils.buildTimeDiffWithNow(getActivity(), slot.getEndTime()));
            } else {
                LogUtils.logEvent("Update View", "Slot is null");
                deliveryTime.setText("Not available");
                timeLeft.setText("-");
            }
        }
    }

    @OnClick(R.id.pickup_button)
    void pickUp() {
        final Batch batch = ShopperApplication.getInstance().getBatch();

        mPickUpButton.setText(getString(R.string.driver_wait_pick_up));
        mPickUpButton.setEnabled(false);
        mPickUpIndicator.setVisibility(View.VISIBLE);

        mProgressContainer.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);
        mProgressLabel.setVisibility(View.GONE);

        LogUtils.logEvent("Pick Up", "Batch ID: " + batch.getRemoteId());
        ShopperApplication.getInstance().getBatchManager().pickup(batch.getRemoteId(), new DataListener<Batch>() {
            @Override
            public void onCompleted(boolean success, Batch data) {
                if (success) {
                    acceptanceScreen();
                } else {
                    if (getActivity() != null) {
                        Toast.makeText(getActivity(), R.string.driver_pickup_error, Toast.LENGTH_LONG).show();
                    }
                }

                if (getActivity() != null) {
                    mPickUpButton.setEnabled(true);
                    mPickUpButton.setText(getString(R.string.driver_pick_up));
                    mPickUpIndicator.setVisibility(View.GONE);

                    mProgressContainer.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailed(Throwable exception) {
                if (getActivity() != null) {
                    ExceptionResponse response = null;
                    if (exception != null && exception.getClass().equals(ExceptionResponse.class)) {
                        response = (ExceptionResponse) exception;
                    }

                    if (response != null) {
                        if (response.getType() != null && response.getType().equalsIgnoreCase(Constant.OrderCanceledException)) {
                            showErrorDialog(getString(R.string.label_cancellation_batches_title),
                                    getString(R.string.label_cancellation_batches_content), getString(R.string.alert_button_ok), errorCancelled);
                            return;
                        }
                    }

                    if (!batch.shoppingFinished()) {
                        showErrorDialog(getString(R.string.alert_title_warning),
                                getString(R.string.shopper_alert_not_finished),
                                getString(R.string.alert_button_ok), errorNotFinished);
                        return;
                    }

                    mPickUpButton.setEnabled(true);
                    mPickUpButton.setText(getString(R.string.driver_pick_up));
                    mPickUpIndicator.setVisibility(View.GONE);
                    mProgressContainer.setVisibility(View.GONE);

                    Toast.makeText(getActivity(), R.string.driver_pickup_error, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void showErrorDialog(String title, String content, String positiveButton, final int type) {
        if (getActivity() != null) {
            MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity())
                    .title(title)
                    .content(content)
                    .positiveText(positiveButton)
                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onAny(MaterialDialog dialog) {
                            super.onAny(dialog);
                            if (type == errorNotFinished) {
                                updateShipment();
                                mPickUpButton.setEnabled(true);
                            } else if (type == errorCancelled) {
                                driverJobListScreen();
                            }
                        }
                    });
            builder.show();
        }
    }

    private void driverJobListScreen() {
        ActivityUtils.startClearTop(getActivity(), DriverListActivity.class);
    }

    private void acceptanceScreen() {
        ActivityUtils.startClearTop(getActivity(), DriverAcceptanceActivity.class);
    }
}
