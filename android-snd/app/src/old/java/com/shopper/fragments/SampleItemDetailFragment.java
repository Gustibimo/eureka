package com.shopper.fragments;

import android.app.ListFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.ProductSampleItem;
import com.happyfresh.snowflakes.hoverfly.models.SpreeImage;
import com.shopper.activities.FullImageActivity;
import com.shopper.activities.SampleItemDetailActivity;
import com.shopper.adapters.SectionAdapter;
import com.shopper.common.Constant;
import com.shopper.common.ShopperApplication;
import com.shopper.events.FlaggedEvent;
import com.shopper.events.OrderCanceledEvent;
import com.shopper.events.ShoppedEvent;
import com.shopper.managers.ShipmentManager;
import com.shopper.utils.AppUtils;
import com.shopper.utils.LogUtils;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import static com.shopper.activities.FullImageActivity.PRODUCT_IMAGES;

/**
 * Created by kharda on 9/16/16.
 */
public class SampleItemDetailFragment extends ListFragment {

    Button mFoundButton;
    private LinkedHashMap<String, String> mContent = new LinkedHashMap<String, String>();

    private AppCompatImageView mOverflowButton;

    AbsListView.OnScrollListener mOnScrollListener = new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView absListView, int i) {
        }

        @Override
        public void onScroll(AbsListView absListView, int i, int i2, int i3) {
            setActionBarColor();
        }
    };

    private View mHeaderView;
    private View mFooterView;

    private ImageView mHeaderItemImage;
    private TextView mHeaderItemCounter;
    private TextView mHeaderItemAverageWeight;
    private TextView mHeaderVariantName;
    private TextView mHeaderFlag;
    private View mHeaderMismatch;

    private RadioGroup mHeaderTabContainer;

    private Long mShipmentId;

    private Long mItemId;
    private String mItemOrderNumber;
    private ProductSampleItem mItem;

    private SectionAdapter mAdapter;

    public static SampleItemDetailFragment newInstance(Long shipmentId, Long itemId, String itemOrderNumber) {
        SampleItemDetailFragment fragment = new SampleItemDetailFragment();

        Bundle bundle = new Bundle();
        bundle.putLong(SampleItemDetailActivity.SHIPMENT_ID, shipmentId);
        bundle.putLong(SampleItemDetailActivity.ITEM_ID, itemId);
        bundle.putString(SampleItemDetailActivity.ORDER_NUMBER, itemOrderNumber);

        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        Bundle bundle = getArguments();
        if (savedInstanceState != null) {
            bundle = savedInstanceState;
        }

        mShipmentId = bundle.getLong(SampleItemDetailActivity.SHIPMENT_ID, 0);

        mItemId = bundle.getLong(SampleItemDetailActivity.ITEM_ID, 0);
        mItemOrderNumber = bundle.getString(SampleItemDetailActivity.ORDER_NUMBER);
        mItem = ProductSampleItem.findByProductIdAndOrderNumber(mItemId, mItemOrderNumber);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putLong(SampleItemDetailActivity.SHIPMENT_ID, mShipmentId);
        outState.putLong(SampleItemDetailActivity.ITEM_ID, mItemId);
        outState.putString(SampleItemDetailActivity.ORDER_NUMBER, mItemOrderNumber);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.item_detail, menu);
        onlyShowOutOfStockMenu(menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void onlyShowOutOfStockMenu(Menu menu) {
        menu.findItem(R.id.action_cancel_replacement).setVisible(false);
        menu.findItem(R.id.action_unflag_item).setVisible(false);
        menu.findItem(R.id.action_flag_item).setVisible(true);
        menu.findItem(R.id.action_price_change).setVisible(false);
        menu.findItem(R.id.action_call_operator).setVisible(false);
        menu.findItem(R.id.action_call_customer).setVisible(false);

        if (mItem != null && mItem.getTotalFlagged() > 0) {
            menu.findItem(R.id.action_unflag_item).setVisible(true);
            menu.findItem(R.id.action_flag_item).setVisible(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_flag_item:
                setOutOfStock();
                return true;

            case R.id.action_unflag_item:
                removeItemFlag();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void removeItemFlag() {
        MaterialDialog.Builder dialogBuilder = new MaterialDialog.Builder(getActivity());
        dialogBuilder.title(R.string.alert_title_unflag_item);
        dialogBuilder.content(R.string.alert_message_unflag_item);

        dialogBuilder.negativeText(R.string.alert_button_cancel);
        dialogBuilder.positiveText(R.string.alert_button_yes);

        dialogBuilder.callback(new MaterialDialog.ButtonCallback() {
            @Override
            public void onPositive(MaterialDialog dialog) {
                sendUnflagRequest();
            }
        });

        dialogBuilder.show();
    }

    private void sendUnflagRequest() {
        mItem.setFlag(null);
        mItem.setTotalShopped(0);
        mItem.setTotalFlagged(0);
        mItem.save();

        mFoundButton.setVisibility(View.VISIBLE);
        mHeaderFlag.setText(null);
        mHeaderFlag.setVisibility(View.GONE);

        updateView();

        mAdapter.notifyDataSetChanged();
        getActivity().invalidateOptionsMenu();
    }

    private void setOutOfStock() {
        final String flag = ShipmentManager.OUT_OF_STOCK;
        mItem.setTotalShopped(0);
        mItem.setFlag(flag);
        mItem.setTotalFlagged(mItem.getTotal() - mItem.getTotalShopped());
        mItem.save();
        mAdapter.notifyDataSetChanged();
        mHeaderFlag.setText(getString(R.string.item_flagged_counter,
                mItem.getFlagName(), mItem.getTotalFlagged()));

        mHeaderFlag.setVisibility(View.VISIBLE);
        mFoundButton.setVisibility(View.GONE);
        getActivity().invalidateOptionsMenu();
        getActivity().finish();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_detail, null);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupViews(view);

        if (mItem == null) {
            return;
        }

        mFoundButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemFoundDialog();
            }
        });

        if (mItem.getTotalFlagged() > 0) {
            mFoundButton.setVisibility(View.GONE);
            mHeaderFlag.setVisibility(View.VISIBLE);
            mHeaderFlag.setText(getString(R.string.item_flagged_counter, mItem.getFlagName(), mItem.getTotalFlagged()));
        } else {
            mHeaderFlag.setText("");
            mHeaderFlag.setVisibility(View.GONE);
        }

        mHeaderMismatch.setVisibility(View.GONE);
        drawVariantInfo();
        showData();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        acquireOverflowButton();
    }

    @Override
    public void onResume() {
        super.onResume();
        ShopperApplication.getInstance().getBus().register(this);

        setActionBarColor();
    }

    @Override
    public void onPause() {
        super.onPause();
        ShopperApplication.getInstance().getBus().unregister(this);
    }

    @Subscribe
    public void onShoppedEvent(ShoppedEvent e) {
        LogUtils.LOG("ShoppedEvent >> " + e);
        showData();
    }

    @Subscribe
    public void onFlaggedEvent(FlaggedEvent e) {
        LogUtils.LOG("FlaggedEvent >> " + e);
        showData();
    }

    @Subscribe
    public void onOrderCanceledEvent(OrderCanceledEvent e) {
        getSampleItemDetailActivity().showOrderCanceledDialog(e.getDialogType());
    }

    void acquireOverflowButton() {
        final String overflowDescription = getString(R.string.abc_action_menu_overflow_description);
        final ViewGroup decorView = (ViewGroup) getActivity().getWindow().getDecorView();
        final ViewTreeObserver viewTreeObserver = decorView.getViewTreeObserver();
        viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                final ArrayList<View> outViews = new ArrayList<>();
                getActivity().getWindow().getDecorView().findViewsWithText(outViews, overflowDescription,
                        View.FIND_VIEWS_WITH_CONTENT_DESCRIPTION);

                if (outViews.isEmpty()) {
                    return;
                }

                mOverflowButton = (AppCompatImageView) outViews.get(0);
            }
        });
    }

    void setActionBarColor() {
        if (getListView() == null) {
            return;
        }

        Context context = ShopperApplication.getContext();

        int scrollY = 0;
        View c = getListView().getChildAt(0);
        if (c != null) {
            scrollY = -c.getTop();
        }

        ActionBar actionBar = ((SampleItemDetailActivity) getActivity()).getSupportActionBar();
        Toolbar toolbar = ((SampleItemDetailActivity) getActivity()).getToolbar();

        Drawable closeButton = ContextCompat.getDrawable(context, R.drawable.icon_clear);
        Drawable overflowButton = ContextCompat.getDrawable(context, R.drawable.icon_more);

        int filterColor = ContextCompat.getColor(context, R.color.item_background);
        int alphaBar;

        if (scrollY >= 10) {
            alphaBar = scrollY - 10;
            if (scrollY > 255) alphaBar = 255;

            int color = R.color.happyfresh_green;
            if (AppUtils.currentUserType() == AppUtils.UserType.RANGER) {
                color = R.color.happyfresh_rangers;
            }

            ColorDrawable colorDrawable = new ColorDrawable(ContextCompat.getColor(context, color));
            colorDrawable.setAlpha(alphaBar);

            toolbar.setBackground(colorDrawable);

            if (alphaBar <= 100) {
                filterColor = ContextCompat.getColor(context, R.color.item_background);
            } else {
                filterColor = ContextCompat.getColor(context, android.R.color.white);
            }
        } else {
            toolbar.setBackground(new ColorDrawable(
                    ContextCompat.getColor(context, android.R.color.transparent)));

            filterColor = ContextCompat.getColor(context, R.color.item_background);
        }

        closeButton.setColorFilter(filterColor, PorterDuff.Mode.SRC_ATOP);
        actionBar.setHomeAsUpIndicator(closeButton);

        if (mOverflowButton != null) {
            overflowButton.setColorFilter(filterColor, PorterDuff.Mode.SRC_ATOP);
            mOverflowButton.setImageDrawable(overflowButton);
        }
    }

    void setupViews(View view) {
        // Setup all views, except replacement view
        mFoundButton = (Button) view.findViewById(R.id.found_button);
        mFoundButton.setText(getResources().getString(R.string.item_detail_found));

        mHeaderView = getActivity().getLayoutInflater().inflate(R.layout.layout_item_detail_header, null);

        mHeaderItemImage = (ImageView) mHeaderView.findViewById(R.id.item_image);
        mHeaderItemImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), FullImageActivity.class);
                intent.putParcelableArrayListExtra(PRODUCT_IMAGES, (ArrayList) mItem.getVariant().images());
                startActivity(intent);
            }
        });

        mHeaderItemCounter = (TextView) mHeaderView.findViewById(R.id.item_counter);
        mHeaderItemAverageWeight = (TextView) mHeaderView.findViewById(R.id.average_weight);
        mHeaderVariantName = (TextView) mHeaderView.findViewById(R.id.item_variant_name);
        mHeaderFlag = (TextView) mHeaderView.findViewById(R.id.item_flag_info);
        mHeaderMismatch = mHeaderView.findViewById(R.id.item_flag_mismatch);

        mHeaderTabContainer = (RadioGroup) mHeaderView.findViewById(R.id.tab_container);
        mHeaderTabContainer.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int tabId) {
                showData();
            }
        });

        mFooterView = getActivity().getLayoutInflater().inflate(R.layout.layout_item_footer, null);
        getListView().addFooterView(mFooterView);

        mAdapter = new SectionAdapter(getActivity());
        getListView().setOnScrollListener(mOnScrollListener);
    }

    private void setHeaderTarget() {
        mHeaderView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
    }

    private void itemFoundDialog() {
        mFoundButton.setEnabled(false);

        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View alertView = inflater.inflate(R.layout.dialog_item_found, null);

        final EditText itemCounter = (EditText) alertView.findViewById(R.id.item_found_count);
        final EditText itemActualWeight = (EditText) alertView.findViewById(R.id.item_actual_weight);
        final TextView amountText = (TextView) alertView.findViewById(R.id.item_found_max_amount);
        final TextView expectedText = (TextView) alertView.findViewById(R.id.item_expected_weight);
        final TextView errorMessage = (TextView) alertView.findViewById(R.id.item_error_message_text);
        final ImageView errorIcon = (ImageView) alertView.findViewById(R.id.item_error_message_icon);
        final LinearLayout errorLayout = (LinearLayout) alertView.findViewById(R.id.item_error_message);
        final RelativeLayout itemActual = (RelativeLayout) alertView.findViewById(R.id.item_actual);
        final GradientDrawable itemActualWeightBackground = (GradientDrawable) (
                (LayerDrawable) itemActualWeight.getBackground()
        ).getDrawable(1);

        final Callable<Boolean> itemActualWeightCallable = new Callable<Boolean>() {
            int mMessageColor;
            Context mContext;

            public void warn() {
                mMessageColor = ContextCompat.getColor(mContext, R.color.notification_warn);
                itemActualWeight.setTextColor(mMessageColor);
                itemActualWeightBackground.setStroke(1, mMessageColor);

                errorMessage.setText(R.string.item_actual_weight_warn_message);
                errorIcon.setImageResource(R.drawable.icon_circle_question_mark);
                errorLayout.setVisibility(View.VISIBLE);
            }

            public void error() {
                mMessageColor = ContextCompat.getColor(mContext, R.color.notification_error);
                itemActualWeight.setTextColor(mMessageColor);
                itemActualWeightBackground.setStroke(1, mMessageColor);

                errorMessage.setText(R.string.item_actual_weight_max_message);
                errorIcon.setImageResource(R.drawable.icon_circle_error);
                errorLayout.setVisibility(View.VISIBLE);
            }

            public void required() {
                mMessageColor = ContextCompat.getColor(mContext, R.color.notification_error);
                itemActualWeightBackground.setStroke(1, mMessageColor);

                itemActualWeight.setTextColor(mMessageColor);
                errorLayout.setVisibility(View.GONE);
            }

            @Override
            public Boolean call() throws Exception {
                String text = itemActualWeight.getText().toString();
                mContext = ShopperApplication.getContext();

                if (text.isEmpty()) {
                    required();

                    return false;
                }

                Double actualWeight = Double.valueOf(text);
                Double expectedWeight;
                String foundCount = itemCounter.getText().toString();
                errorIcon.setVisibility(View.VISIBLE);
                if (foundCount.isEmpty()) {
                    expectedWeight = mItem.getTotalNaturalAverageWeight();
                } else {
                    int count = Integer.valueOf(foundCount);
                    if (count == 0 && actualWeight == 0d) {
                        return true;
                    }
                    expectedWeight = mItem.getTotalNaturalAverageWeight(count);
                }

                // above 50%;
                if (actualWeight >= expectedWeight * (1 + Constant.ACTUAL_WEIGHT_MAX_FACTOR)) {
                    error();

                    return false;
                }

                // above 10%
                if (actualWeight >= expectedWeight * (1 + Constant.ACTUAL_WEIGHT_WARN_FACTOR)) {
                    warn();

                    return true;
                }

                // bellow 50%
                if (actualWeight <= expectedWeight * (1 - Constant.ACTUAL_WEIGHT_MAX_FACTOR)) {
                    error();

                    return false;
                }

                // bellow 10%
                if (actualWeight <= expectedWeight * (1 - Constant.ACTUAL_WEIGHT_WARN_FACTOR)) {
                    warn();

                    return true;
                }

                mMessageColor = ContextCompat.getColor(mContext, R.color.black);
                itemActualWeight.setTextColor(mMessageColor);
                itemActualWeightBackground.setStroke(1, mMessageColor);

                errorLayout.setVisibility(TextView.GONE);

                return true;
            }
        };


        if (mItem.hasNaturalUnit()) {

            itemActualWeight.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    try {
                        itemActualWeightCallable.call();
                    } catch (Exception ignored) {
                    }
                }
            });

            itemActualWeight.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        itemActualWeight.setHint(R.string.item_actual_weight);
                        try {
                            itemActualWeightCallable.call();
                        } catch (Exception e) {
                            LogUtils.logEvent("ActualWeight: ", e.getMessage());
                        }
                    } else {
                        itemActualWeight.setHint("");
                    }
                }
            });


            itemCounter.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    String value = itemCounter.getText().toString();
                    if (!hasFocus && !value.isEmpty()) {
                        int count = Integer.parseInt(value);
                        expectedText.setText(
                                getString(
                                        R.string.alert_item_expected_weight,
                                        mItem.getTotalNaturalAverageWeight(count),
                                        mItem.getSupermarketUnit()
                                )
                        );
                    }
                }
            });
        } else {
            itemActual.setVisibility(View.GONE);
        }

        if (mItem.getTotalShopped() > 0) {
            itemCounter.setText(String.valueOf(mItem.getTotalShopped()));
        }

        amountText.setText(getString(R.string.alert_item_found_amount, mItem.getTotal()));
        expectedText.setText(
                getString(
                        R.string.alert_item_expected_weight,
                        mItem.getTotalNaturalAverageWeight(),
                        mItem.getSupermarketUnit()
                )
        );

        final MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity());
        builder.title(R.string.alert_title_item_found);
        builder.customView(alertView, false);

        builder.negativeText(R.string.alert_button_cancel);
        builder.positiveText(R.string.alert_button_ok);
        builder.autoDismiss(false);
        builder.onNegative(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                materialDialog.dismiss();
                itemActualWeightBackground.setStroke(1, Color.BLACK);
            }
        });
        builder.onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                int foundCounter = -1;
                Double actualWeight = null;
                Boolean actualWeightValid = !mItem.hasNaturalUnit();

                String counterText = itemCounter.getText().toString();
                if (!counterText.isEmpty()) {
                    foundCounter = Integer.valueOf(counterText);
                }
                Boolean countValid = foundCounter >= 0 && foundCounter <= mItem.getTotal();

                String actualWeightText = itemActualWeight.getText().toString();
                if (!actualWeightText.isEmpty()) {
                    actualWeight = Double.valueOf(actualWeightText);
                }

                if (mItem.hasNaturalUnit()) {
                    try {
                        actualWeightValid = itemActualWeightCallable.call();
                    } catch (Exception ignored) {
                    }
                    if (actualWeightValid && countValid) {
                        itemFound(foundCounter, actualWeight);
                        itemActualWeightBackground.setStroke(1, Color.BLACK);
                        materialDialog.dismiss();
                    }
                } else if (countValid) {
                    itemFound(foundCounter, null);
                    itemActualWeightBackground.setStroke(1, Color.BLACK);
                    materialDialog.dismiss();
                }
            }
        });

        MaterialDialog inputDialog = builder.build();
        inputDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        inputDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                mFoundButton.setEnabled(true);
            }
        });
        inputDialog.show();

        itemCounter.requestFocus();
        InputMethodManager imm = (InputMethodManager) getActivity()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(itemCounter, InputMethodManager.SHOW_IMPLICIT);
        imm.showSoftInput(itemActualWeight, InputMethodManager.SHOW_IMPLICIT);
    }

    private void itemFound(final int amount, final Double actualWeight) {
        enableNavigation(false);
        if (amount == 0) {
            setOutOfStock();
        }
        else if (amount <= mItem.getTotal()) {
            mItem.setTotalShopped(amount);
            mItem.setTotalFlagged(0);
            mItem.setFlag(null);
            mItem.save();
            mAdapter.notifyDataSetChanged();
            updatePurchasedCount();
            enableNavigation(true);
            showData();
            closeOnCompleted();
        }
    }

    public void reloadData() {
        mItem = ProductSampleItem.findById(mItem.getRemoteId());
        updateView();
    }

    public void updateView() {
        setHeaderTarget();

        showData();

        if (mItem.getFlag() == null) {
            mHeaderFlag.setText(null);
            mHeaderFlag.setVisibility(View.GONE);
        } else {
            mHeaderFlag.setText(getString(R.string.item_flagged_counter, mItem.getFlagName(), mItem.getTotalFlagged()));
            mHeaderFlag.setVisibility(View.VISIBLE);
        }
    }

    private void enableNavigation(boolean enabled) {
        mFoundButton.setEnabled(enabled);

        if (getActivity() != null) {
            setHasOptionsMenu(enabled);
            getActivity().invalidateOptionsMenu();
        }
    }

    private void closeOnCompleted() {
        if (getActivity() != null) {
            getActivity().finish();
        }
    }


    public void generateData() {
        LinkedHashMap<String, String> itemDetail = new LinkedHashMap<String, String>();
        itemDetail.put(getString(R.string.item_detail_label_sku_number), mItem.getVariant().getSku());
        if (mItem.hasNaturalUnit()) { // Item with natural unit, e.g.: meat, fruit
            itemDetail.put(getString(R.string.item_detail_label_price_per_unit), String.format("%s/%s", mItem.getDisplaySupermarketUnitCostPrice(), mItem.getVariant().getUnits()));
            itemDetail.put(getString(R.string.item_detail_label_customer_price), String.format("%s/%s", mItem.getDisplayAmount(), mItem.getDisplayAverageWeight()));
        } else { // Ordinary Item
            itemDetail.put(getString(R.string.item_detail_label_price_per_unit), mItem.getDisplayCostPrice());
            itemDetail.put(getString(R.string.item_detail_label_customer_price), mItem.getDisplayAmount());
        }

        itemDetail.put(getString(R.string.item_detail_label_unit), mItem.getDisplayUnit());
        itemDetail.put(getString(R.string.item_detail_label_notes), mItem.getVariant().getDescription());
        mAdapter.setPriceChanged(false);
        mContent = itemDetail;
    }

    public void drawDetail() {
        mAdapter.clearItem();

        for (Map.Entry<String, String> content : mContent.entrySet()) {
            if (content.getKey().equals(getString(R.string.item_detail_label_notes))) {
                mAdapter.addItem(content, SectionAdapter.ViewType.TYPE_ITEM_VERTICAL);
            } else if (content.getKey().equals(getString(R.string.item_detail_label_promotions))) {
                mAdapter.addItem(content, SectionAdapter.ViewType.TYPE_ITEM_PROMOTIONS);
            } else {
                mAdapter.addItem(content);
            }
        }
        setListAdapter(mAdapter);
    }


    private void drawVariantInfo() {
        List<SpreeImage> images = mItem.getVariant().images();
        String imageUrl = null;
        if (images.size() > 0) {
            imageUrl = images.get(0).getProductUrl();
        }

        Picasso.with(getActivity()).load(imageUrl).placeholder(R.drawable.default_product).into(mHeaderItemImage);

        mHeaderVariantName.setText(mItem.getVariant().getName());

        updatePurchasedCount();

        getListView().addHeaderView(mHeaderView);
    }

    private void updatePurchasedCount() {
        if (mItem.hasNaturalUnit()) {
            mHeaderItemCounter.setText(mItem.getPurchasedAmount());
            mHeaderItemAverageWeight.setText(mItem.getDisplayAverageWeight());
            mHeaderItemAverageWeight.setVisibility(View.VISIBLE);
        } else {
            mHeaderItemCounter.setText(mItem.getPurchasedAmount());
            mHeaderItemAverageWeight.setVisibility(View.GONE);
        }
    }

    private SampleItemDetailActivity getSampleItemDetailActivity() {
        return (SampleItemDetailActivity) getActivity();
    }

    private void showData() {
        generateData();
        drawDetail();
    }
}
