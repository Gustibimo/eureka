package com.shopper.fragments;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.CashTransfer;
import com.shopper.adapters.HandoverHistoryAdapter;
import com.shopper.common.ShopperApplication;
import com.shopper.components.CircularProgressBar;
import com.shopper.listeners.DataListener;
import com.happyfresh.snowflakes.hoverfly.models.response.CashHandoverResponse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.BindView;

/**
 * Created by ifranseda on 5/18/15.
 */
public class HandoverHistoryFragment extends BaseRecyclerFragment {
    @BindView(R.id.progress_indicator)
    CircularProgressBar progressIndicator;

    @BindView(R.id.handover_no_data)
    View noData;

    HandoverHistoryAdapter mAdapter;
    List<CashTransfer> mCashTransfers = new ArrayList<>();

    @Override
    public boolean isSwipeRefreshLayoutEnabled() {
        return true;
    }

    @Override
    public void onSwipeRefresh() {
        fetchHistory();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.handover_history);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_handover_history, null);
        ButterKnife.bind(this, view);

        progressIndicator.setVisibility(View.INVISIBLE);
        noData.setVisibility(View.GONE);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mAdapter = new HandoverHistoryAdapter(getActivity(), mCashTransfers);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mRecyclerView.setAdapter(mAdapter);

        fetchHistory();
    }

    private void fetchHistory() {
        progressIndicator.setVisibility(View.VISIBLE);
        noData.setVisibility(View.GONE);

        ShopperApplication.getInstance().getHandoverManager().fetchHistory(new DataListener() {
            @Override
            public void onCompleted(boolean success, Object data) {
                progressIndicator.setVisibility(View.INVISIBLE);
                mSwipeRefreshLayout.setRefreshing(false);

                if (success && data != null) {
                    CashHandoverResponse response = (CashHandoverResponse) data;

                    mCashTransfers.clear();

                    if (response.getCashTransfers().size() > 0) {
                        Collections.reverse(response.getCashTransfers());
                        mCashTransfers.addAll(response.getCashTransfers());

                        mAdapter.notifyDataSetChanged();

                        noData.setVisibility(View.GONE);
                    } else {
                        noData.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
    }
}
