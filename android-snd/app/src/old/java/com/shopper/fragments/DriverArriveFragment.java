package com.shopper.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.Address;
import com.happyfresh.snowflakes.hoverfly.models.Batch;
import com.happyfresh.snowflakes.hoverfly.models.Job;
import com.happyfresh.snowflakes.hoverfly.models.JobState;
import com.happyfresh.snowflakes.hoverfly.models.Shipment;
import com.happyfresh.snowflakes.hoverfly.models.Slot;
import com.happyfresh.snowflakes.hoverfly.models.payload.FailDeliveryPayload;
import com.happyfresh.snowflakes.hoverfly.utils.FormatterUtils;
import com.shopper.activities.DriverArriveActivity;
import com.shopper.activities.DriverBatchDeliveryActivity;
import com.shopper.activities.DriverFinishActivity;
import com.shopper.activities.DriverListActivity;
import com.shopper.common.Constant;
import com.shopper.common.ShopperApplication;
import com.shopper.components.CircularProgressBar;
import com.shopper.listeners.DataListener;
import com.shopper.utils.ActivityUtils;
import com.shopper.utils.SharedPrefUtils;
import com.shopper.utils.StringUtils;

import java.text.NumberFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.shopper.common.Constant.CanceledDialogType.ARRIVE;
import static com.shopper.common.Constant.CanceledDialogType.FAILED_DELIVERY;

/**
 * Created by rsavianto on 12/24/14.
 */
public class DriverArriveFragment extends Fragment {

    @BindView(R.id.content)
    ScrollView mContent;

    @BindView(R.id.progress_container)
    View mProgressContainer;

    @BindView(R.id.fragment_progressbar)
    CircularProgressBar mProgressBar;

    @BindView(R.id.arrive_button)
    Button mArriveButton;

    @BindView(R.id.delivery_location_name)
    TextView deliveryLocationName;

    @BindView(R.id.delivery_location_address1)
    TextView deliveryLocationAddress1;

    @BindView(R.id.delivery_location_address2)
    TextView deliveryLocationAddress2;

    @BindView(R.id.delivery_time)
    TextView deliveryTime;

    @BindView(R.id.time_left)
    TextView timeLeft;

    @BindView(R.id.happycorporate_layout)
    View happyCorporateLayout;

    @BindView(R.id.delivery_payment_layout)
    View deliveryPaymentLayout;

    @BindView(R.id.payment_method_icon)
    ImageView paymentMethodIcon;

    @BindView(R.id.payment_method)
    TextView paymentMethodText;

    @BindView(R.id.total_order)
    TextView totalOrder;

    @BindView(R.id.order_number)
    TextView orderNumber;

    @BindView(R.id.customer_name)
    TextView customerName;

    @BindView(R.id.delivery_location_address_details)
    TextView deliveryLocationAddressDetails;

    @BindView(R.id.delivery_location_address_number)
    TextView deliveryLocationAddressNumber;

    @BindView(R.id.delivery_location_details_text)
    TextView deliveryLocationDetailsText;

    @BindView(R.id.ll_get_directions)
    LinearLayout getDirectionsLayout;

    private Slot mSlot;

    private String mDiffTime;

    private Unbinder unbinder;

    public static DriverArriveFragment newInstance() {
        DriverArriveFragment fragment = new DriverArriveFragment();

        return fragment;
    }

    public static String getTotalTimeLate(Context ctx, Date startTime, Date endTime) {
        Long startTimeInLocal = startTime.getTime() + TimeZone.getDefault().getOffset(System.currentTimeMillis());
        Long endTimeInLocal = endTime.getTime() + TimeZone.getDefault().getOffset(System.currentTimeMillis());
        Long timeDiff = startTimeInLocal - endTimeInLocal;
        return StringUtils.showDuration(ctx, timeDiff, true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            Long slotId = savedInstanceState.getLong("SLOT_ID", 0);
            mSlot = Slot.Companion.findById(slotId);

            mDiffTime = savedInstanceState.getString("DIFF_TIME");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putLong("SLOT_ID", mSlot.getRemoteId());
        outState.putString("DIFF_TIME", mDiffTime);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_driver_arrive, null);
        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mProgressBar.setVisibility(View.GONE);

        Shipment shipment = ShopperApplication.getInstance().getCurrentShipment();
        mSlot = shipment.getSlot();

        deliveryLocationName.setText(shipment.getAddress().getAddress1());
        deliveryLocationAddress1.setText(shipment.getAddress().getAddress2());
        deliveryLocationAddress2.setText(StringUtils.buildAddressCityInfo(shipment.getAddress()));

        if (shipment.getAddress().getAddressDetail() != null && !shipment.getAddress().getAddressDetail().trim()
                .isEmpty()) {
            deliveryLocationAddressDetails.setText(shipment.getAddress().getAddressDetail());

            deliveryLocationAddressDetails.setVisibility(View.VISIBLE);
            deliveryLocationDetailsText.setVisibility(View.VISIBLE);
        }
        else {
            deliveryLocationAddressDetails.setVisibility(View.GONE);
            deliveryLocationDetailsText.setVisibility(View.GONE);
        }

        if (shipment.getAddress().getAddressNumber() != null && !shipment.getAddress().getAddressNumber().trim()
                .isEmpty()) {
            deliveryLocationAddressNumber.setText(shipment.getAddress().getAddressNumber());
            deliveryLocationAddressNumber.setVisibility(View.VISIBLE);
        }
        else {
            deliveryLocationAddressNumber.setVisibility(View.GONE);
        }

        if (TextUtils.isEmpty(shipment.getAddress().getAddress2())) {
            deliveryLocationAddress1.setVisibility(View.GONE);
        }

        if (shipment.getAddress().getLat() == null) {
            getDirectionsLayout.setVisibility(View.GONE);
        }

        if (shipment.getOrder() != null && shipment.getOrder().getCompanyId() != 0) {
            happyCorporateLayout.setVisibility(View.VISIBLE);
            deliveryPaymentLayout.setVisibility(View.GONE);
        }
        else {
            if (shipment.getOrder() != null && shipment.getOrder().getPaymentMethod() != null &&
                    shipment.getOrder().getPaymentMethod().equals(Constant.CashOnDeliveryMethod)) {

                happyCorporateLayout.setVisibility(View.GONE);
                deliveryPaymentLayout.setVisibility(View.VISIBLE);

                deliveryPaymentLayout
                        .setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.payment_cod_background));
                paymentMethodIcon.setImageResource(R.drawable.icon_cod);
                paymentMethodText.setText(R.string.payment_total_cod);

                paymentMethodText.setTextColor(ContextCompat.getColor(getActivity(), android.R.color.black));
                totalOrder.setTextColor(ContextCompat.getColor(getActivity(), android.R.color.black));
            }
            else {
                happyCorporateLayout.setVisibility(View.GONE);
                deliveryPaymentLayout.setVisibility(View.VISIBLE);

                deliveryPaymentLayout
                        .setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.payment_cc_background));
                paymentMethodIcon.setImageResource(R.drawable.icon_cc);
                paymentMethodText.setText(R.string.payment_total_credit_card);

                paymentMethodText.setTextColor(ContextCompat.getColor(getActivity(), android.R.color.white));
                totalOrder.setTextColor(ContextCompat.getColor(getActivity(), android.R.color.white));
            }
        }

        String total = getNumberFormatter(shipment.getOrder().getCurrency()).format(shipment.getOrder().getTotal());
        totalOrder.setText(total);

        if (mSlot != null) {
            deliveryTime.setText(StringUtils.showDeliveryTime(mSlot.getStartTime(), mSlot.getEndTime()));
            timeLeft.setText(StringUtils.buildTimeDiffWithNow(getActivity(), mSlot.getEndTime()));
        }
        else {
            deliveryTime.setText("null");
            timeLeft.setText("null");
        }

        orderNumber.setText(
                getString(R.string.order_with_purchases_number_tail, shipment.getOrder().getNumber(),
                        shipment.getOrder().getPurchases()));
        customerName.setText(shipment.getRecipientName());
    }

    private NumberFormat getNumberFormatter(String currencyCode) {
        return FormatterUtils.getNumberFormatter(currencyCode);
    }

    @OnClick(R.id.arrive_button)
    void arriveButtonOnClick() {
        MaterialDialog.Builder inputDialog = new MaterialDialog.Builder(getActivity());
        inputDialog.title(R.string.alert_title_warning);

        inputDialog.content(R.string.alert_message_really_arrive);
        inputDialog.negativeText(R.string.alert_button_no);
        inputDialog.positiveText(R.string.alert_button_yes);
        inputDialog.callback(new MaterialDialog.ButtonCallback() {
            @Override
            public void onPositive(MaterialDialog dialog) {
                arrive();
            }
        });

        inputDialog.show();
    }

    @OnClick(R.id.ll_get_directions)
    void getDirectionsClick() {
        Shipment shipment = ShopperApplication.getInstance().getCurrentShipment();

        Address address = shipment.getAddress();
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?dirflg=t,h&daddr=" + address.getLat() + "," + address.getLon()));
        startActivity(intent);

    }

    void arrive() {
        mArriveButton.setText(getString(R.string.driver_waiting));
        mArriveButton.setEnabled(false);
        mProgressBar.setVisibility(View.VISIBLE);
        mProgressContainer.setVisibility(View.VISIBLE);

        final ShopperApplication application = ShopperApplication.getInstance();
        Batch batch = application.getBatch();
        Shipment shipment = application.getCurrentShipment();
        application.getBatchManager().arrive(batch.getRemoteId(), shipment.getRemoteId(), new DataListener() {
            @Override
            public void onCompleted(boolean success, Object data) {
                if (success) {
                    proceedCompletionScreen();
                }
                else {
                    if (getActivity() != null) {
                        Toast.makeText(getActivity(),
                                getString(R.string.alert_please_try_again), Toast.LENGTH_SHORT).show();
                    }
                }

                if (getActivity() != null) {
                    mArriveButton.setText(getString(R.string.driver_arrive));
                    mArriveButton.setEnabled(true);
                    mProgressBar.setVisibility(View.GONE);
                    mProgressContainer.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailed(Throwable exception) {
                final DriverArriveActivity activity = (DriverArriveActivity) getActivity();
                if (activity != null) {
                    if (exception != null && Constant.OrderCanceledException.equalsIgnoreCase(exception.getMessage())) {
                        activity.showOrderCanceledDialog(ARRIVE);
                    }
                    else {
                        mArriveButton.setText(getString(R.string.driver_arrive));
                        mArriveButton.setEnabled(true);
                        mProgressBar.setVisibility(View.GONE);
                        mProgressContainer.setVisibility(View.GONE);

                        Toast.makeText(ShopperApplication.getContext(),
                                getString(R.string.alert_please_try_again), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void proceedCompletionScreen() {
        Shipment shipment = ShopperApplication.getInstance().getCurrentShipment();
        if (isDriverLate(shipment)) {
            showLateDialog(new MaterialDialog.ButtonCallback() {
                @Override
                public void onPositive(MaterialDialog dialog) {
                    completionScreen();
                }
            });
        }
        else {
            completionScreen();
        }
    }

    private boolean isDriverLate(Shipment shipment) {
        boolean isLate = false;
        if (shipment != null) {
            Job job = shipment.getDeliveryJob();
            if (job != null) {
                List<JobState> states = job.states();
                for (JobState state : states) {
                    if (state.isFoundAddress() && state.getStartTime().after(shipment.getSlot().getEndTime())) {
                        mDiffTime = getTotalTimeLate(getActivity(), state.getStartTime(),
                                shipment.getSlot().getEndTime());
                        isLate = true;
                        break;
                    }
                }
            }
        }
        return isLate;
    }

    private void showLateDialog(MaterialDialog.ButtonCallback buttonCallback) {
        String msg = getString(R.string.late_arrival_dialog_msg, mDiffTime);
        MaterialDialog.Builder dialog = new MaterialDialog
                .Builder(getActivity())
                .title(R.string.late_arrival_dialog_title)
                .content(msg)
                .positiveText(R.string.alert_button_ok)
                .backgroundColor(Color.WHITE)
                .callback(buttonCallback);

        dialog.build().show();
    }

    public void showFailDelivery() {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fail_delivery_layout, null);
        final RadioButton r1 = (RadioButton) view.findViewById(R.id.radio_incorrect_shipping_address);
        final RadioButton r2 = (RadioButton) view.findViewById(R.id.radio_cannot_reach_customer);
        final RadioButton r3 = (RadioButton) view.findViewById(R.id.radio_late);

        MaterialDialog.ButtonCallback buttonCallback = new MaterialDialog.ButtonCallback() {
            @Override
            public void onPositive(MaterialDialog dialog) {
                FailDeliveryPayload payload = new FailDeliveryPayload();
                if (r1.isChecked()) {
                    payload.setReason(getString(R.string.fail_delivery_incorrect_shipping_address));
                }
                else if (r2.isChecked()) {
                    payload.setReason(getString(R.string.fail_delivery_cannot_reach_customer));
                }
                else if (r3.isChecked()) {
                    payload.setReason(getString(R.string.fail_delivery_late));
                }

                failDeliver();
            }
        };

        new MaterialDialog.Builder(getActivity())
                .title(R.string.fail_delivery)
                .customView(view, false)
                .positiveText(R.string.alert_button_ok)
                .negativeText(R.string.alert_button_cancel)
                .callback(buttonCallback)
                .show();
    }

    private void failDeliver() {
        mArriveButton.setText(getString(R.string.driver_waiting));
        mArriveButton.setEnabled(false);
        mProgressBar.setVisibility(View.VISIBLE);
        mProgressContainer.setVisibility(View.VISIBLE);

        ShopperApplication application = ShopperApplication.getInstance();
        Long batchId = application.getBatch().getRemoteId();
        Long shipmentId = application.getCurrentShipment().getRemoteId();

        application.getBatchManager().failDeliver(batchId, shipmentId, new DataListener() {
            @Override
            public void onCompleted(boolean success, Object data) {
                if (success) {
                    completeFailDelivery();
                }

                mArriveButton.setText(getString(R.string.driver_arrive));
                mArriveButton.setEnabled(true);
                mProgressBar.setVisibility(View.GONE);
                mProgressContainer.setVisibility(View.GONE);
            }

            @Override
            public void onFailed(Throwable exception) {
                mArriveButton.setText(getString(R.string.driver_arrive));
                mArriveButton.setEnabled(true);
                mProgressBar.setVisibility(View.GONE);
                mProgressContainer.setVisibility(View.GONE);

                if (Constant.OrderCanceledException.equalsIgnoreCase(exception.getMessage())) {
                    final DriverArriveActivity activity = (DriverArriveActivity) getActivity();
                    activity.showOrderCanceledDialog(FAILED_DELIVERY);
                }
                else {
                    completeFailDelivery();
                }
            }
        });
    }

    void completeFailDelivery() {
        Batch batch = ShopperApplication.getInstance().getBatch();
        if (batch != null && batch.isCompleted()) {
            ShopperApplication.getInstance().setBatch(null);
            ActivityUtils.startClearTop(getActivity(), DriverListActivity.class);
        }
        else {
            ActivityUtils.startClearTop(getActivity(), DriverBatchDeliveryActivity.class);
        }
    }

    private void completionScreen() {
        SharedPrefUtils.writeInt(getActivity(), Constant.ONE_CLICK_PAY_RETRY_COUNT, 0);
        ActivityUtils.startClearTop(getActivity(), DriverFinishActivity.class);
    }
}
