package com.shopper.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.Cash;
import com.happyfresh.snowflakes.hoverfly.models.response.CashesResponse;
import com.happyfresh.snowflakes.hoverfly.utils.FormatterUtils;
import com.shopper.activities.HandoverHistoryActivity;
import com.shopper.activities.HandoverSubmissionActivity;
import com.shopper.adapters.HandoverAdapter;
import com.shopper.common.Constant;
import com.shopper.common.ShopperApplication;
import com.shopper.components.CircularProgressBar;
import com.shopper.listeners.DataListener;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by ifranseda on 5/12/15.
 */
public class CashHandoverFragment extends BaseRecyclerFragment {
    @BindView(R.id.progress_indicator)
    CircularProgressBar progressIndicator;

    @BindView(R.id.handover_balance_container)
    View handoverBalanceContainer;

    @BindView(R.id.handover_balance)
    TextView handoverBalance;

    @BindView(R.id.handover_button)
    Button handoverButton;

    @BindView(R.id.handover_no_data)
    View noData;

    HandoverAdapter mAdapter;
    List<Cash> mCashes = new ArrayList<>();

    double cashHandoverAmount = 0;

    String cashCurrency;
    String countryIso;

    NumberFormat mNumberFormatter;

    @Override
    public boolean isSwipeRefreshLayoutEnabled() {
        return true;
    }

    @Override
    public void onSwipeRefresh() {
        fetchBalance();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.handover_title);
        }

        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.handover, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getActivity().onBackPressed();
        } else if (item.getItemId() == R.id.action_handover_history) {
            openHandoverHistory();
        }

        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_cash_handover, null);
        ButterKnife.bind(this, view);

        progressIndicator.setVisibility(View.INVISIBLE);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mAdapter = new HandoverAdapter(getActivity(), mCashes);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        handoverBalanceContainer.setVisibility(View.GONE);
        handoverButton.setEnabled(false);

        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();

        mCashes.clear();
        mAdapter.notifyDataSetChanged();

        fetchBalance();
    }

    private void openHandoverHistory() {
        Intent intent = new Intent(getActivity(), HandoverHistoryActivity.class);
        getActivity().startActivity(intent);
    }

    private String getCountryCode() {
        if (countryIso == null) {
            countryIso = ShopperApplication.getInstance().getCountryCode();
        }

        return countryIso;
    }

    private NumberFormat getNumberFormatter(String currencyCode) {
        if (mNumberFormatter == null) {
            mNumberFormatter = FormatterUtils.getNumberFormatter(currencyCode);
        }

        return mNumberFormatter;
    }

    private void fetchBalance() {
        noData.setVisibility(View.GONE);
        progressIndicator.setVisibility(View.VISIBLE);

        handoverButton.setText(R.string.please_wait);
        handoverButton.setEnabled(false);

        ShopperApplication.getInstance().getHandoverManager().fetchBalance(new DataListener() {
            @Override
            public void onCompleted(boolean success, Object data) {
                progressIndicator.setVisibility(View.INVISIBLE);
                mSwipeRefreshLayout.setRefreshing(false);

                if (success && data != null) {
                    CashesResponse response = (CashesResponse) data;

                    mCashes.clear();

                    List<Cash> tmpCashes = new ArrayList<Cash>();
                    if (response.getCashes().size() > 0) {
                        for (Cash cash : response.getCashes()) {
                            if (cash.getSourceType().equals("Spree::Job")) {
                                tmpCashes.add(cash);
                            }
                        }

                        if (tmpCashes.size() == 0) {
                            handoverBalanceContainer.setVisibility(View.GONE);
                            noData.setVisibility(View.VISIBLE);
                            handoverButton.setEnabled(false);

                            return;
                        }

                        Cash cashBalance = tmpCashes.get(0);
                        cashHandoverAmount = cashBalance.getBalance();
                        cashCurrency = cashBalance.getCurrency();

                        Collections.reverse(tmpCashes);
                        mCashes.addAll(tmpCashes);
                        mAdapter.notifyDataSetChanged();

                        handoverBalanceContainer.setVisibility(View.VISIBLE);
                        handoverBalance.setText(getNumberFormatter(cashCurrency).format(cashHandoverAmount));
                        noData.setVisibility(View.GONE);
                        handoverButton.setEnabled(true);
                    } else {
                        handoverBalanceContainer.setVisibility(View.GONE);
                        noData.setVisibility(View.VISIBLE);
                        handoverButton.setEnabled(false);
                    }
                }

                handoverButton.setText(R.string.handover);
            }
        });
    }

    @OnClick(R.id.handover_button)
    void handover() {
        Intent intent = new Intent(getActivity(), HandoverSubmissionActivity.class);
        intent.putExtra(Constant.CASH_HANDOVER_AMOUNT, cashHandoverAmount);
        intent.putExtra(Constant.CASH_HANDOVER_CURRENCY, cashCurrency);
        getActivity().startActivity(intent);
    }
}
