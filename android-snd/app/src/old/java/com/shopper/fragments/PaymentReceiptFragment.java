package com.shopper.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.happyfresh.fulfillment.R;
import com.happyfresh.fulfillment.modules.Shopper.ShoppingList.ShoppingListRouter;
import com.happyfresh.snowflakes.hoverfly.Sprinkles;
import com.happyfresh.snowflakes.hoverfly.models.Batch;
import com.happyfresh.snowflakes.hoverfly.models.ProductSampleItem;
import com.happyfresh.snowflakes.hoverfly.models.ReceiptItem;
import com.happyfresh.snowflakes.hoverfly.models.Shipment;
import com.happyfresh.snowflakes.hoverfly.models.StockLocation;
import com.happyfresh.snowflakes.hoverfly.models.payload.ReceiptCollection;
import com.happyfresh.snowflakes.hoverfly.models.response.ExceptionResponse;
import com.happyfresh.snowflakes.hoverfly.stores.ShoppingStore;
import com.shopper.activities.PaymentReceiptActivity;
import com.shopper.activities.ProductSampleListActivity;
import com.shopper.adapters.PaymentReceiptDetailAdapter;
import com.shopper.common.Constant;
import com.shopper.common.ShopperApplication;
import com.shopper.components.CircularProgressBar;
import com.shopper.events.TakePictureEvent;
import com.shopper.listeners.DataListener;
import com.shopper.utils.AppUtils;
import com.shopper.utils.LogUtils;
import com.shopper.utils.SampleUtils;
import com.shopper.utils.SharedPrefUtils;
import com.shopper.views.NonSwipeViewPager;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.shopper.activities.PaymentReceiptActivity.BATCH_ID;
import static com.shopper.activities.PaymentReceiptActivity.RECEIPT_ITEM_LIST;
import static com.shopper.activities.PaymentReceiptActivity.SHIPMENT_ID;
import static com.shopper.activities.PaymentReceiptActivity.STOCK_LOCATION_ID;
import static com.shopper.common.Constant.CanceledDialogType.PAY;
import static com.shopper.common.Constant.OrderCanceledException;
import static com.shopper.common.Constant.SELECTED_SHOPPING_STORE;

public class PaymentReceiptFragment extends Fragment {

    @BindView(R.id.next_receipt_button)
    Button nextButton;

    @BindView(R.id.progress_bar)
    CircularProgressBar progressIndicator;

    @BindView(R.id.progress_overlay)
    View progressOverlay;

    @BindView(R.id.fragment_viewpager)
    NonSwipeViewPager mViewPager;

    @BindView(R.id.item_order_number)
    TextView mOrderNumber;

    @BindView(R.id.item_customer_name)
    TextView mCustomerName;

    private long mShipmentId;

    private Shipment mShipment;

    private Batch mBatch;

    private long mBatchId;

    private StockLocation mStockLocation;

    private PaymentReceiptDetailAdapter mAdapter;

    ArrayList<ReceiptItem> mReceiptItemList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            mReceiptItemList = ReceiptItem.Companion.fromListParcel(getArguments().getParcelableArrayList(RECEIPT_ITEM_LIST));
            mShipmentId = getArguments().getLong(SHIPMENT_ID);
            mShipment = Shipment.findById(mShipmentId);

            long batchId = getArguments().getLong(BATCH_ID);
            mBatch = Batch.findById(batchId);
            mBatchId = batchId;

            long stockLocationId = getArguments().getLong(STOCK_LOCATION_ID);
            mStockLocation = StockLocation.findById(stockLocationId);
        } else {
            Long shipmentId = savedInstanceState.getLong(SHIPMENT_ID, 0);
            mShipmentId = shipmentId;
            mShipment = Shipment.findById(shipmentId);

            Long batchId = savedInstanceState.getLong(BATCH_ID, 0);
            mBatch = Batch.findById(batchId);
            mBatchId = batchId;

            Long stockLocationId = savedInstanceState.getLong(STOCK_LOCATION_ID, 0);
            mStockLocation = StockLocation.findById(stockLocationId);

            mReceiptItemList = ReceiptItem.Companion.fromListParcel(savedInstanceState.getParcelableArrayList(RECEIPT_ITEM_LIST));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (mShipment != null) {
            outState.putLong(BATCH_ID, mBatch.getRemoteId());
            outState.putLong(STOCK_LOCATION_ID, mStockLocation.getRemoteId());
            outState.putLong(SHIPMENT_ID, mShipmentId);
            outState.putParcelableArrayList(RECEIPT_ITEM_LIST, ReceiptItem.Companion.toListParcel(mReceiptItemList));
        }
    }

    private void setupViewPager() {
        mAdapter = new PaymentReceiptDetailAdapter(getChildFragmentManager(), mShipmentId, mReceiptItemList);

        mViewPager.setAdapter(mAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                ReceiptItem receiptItem = mReceiptItemList.get(position);
                nextButton.setEnabled(receiptItem.completedWithPicture());

                if (position + 1 == mReceiptItemList.size()) {
                    nextButton.setText(R.string.pay_and_save);
                } else {
                    nextButton.setText(R.string.next_job);
                }
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mViewPager.setSwipeable(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.payment_receipt_fragment, null);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mShipment = Shipment.findById(mShipmentId);

        mOrderNumber.setText(mShipment.getOrder().getNumber());
        mCustomerName.setText(mShipment.getCustomerName());

        progressIndicator.setVisibility(View.GONE);
        nextButton.setEnabled(false);

        setupViewPager();
    }

    @Override
    public void onResume() {
        super.onResume();

        int position = mViewPager.getCurrentItem();
        tryToEnableButton(mReceiptItemList.get(position));
        if (mViewPager.getCurrentItem() + 1 == mReceiptItemList.size()) {
            nextButton.setText(R.string.pay_and_save);
        } else {
            nextButton.setText(R.string.next_job);
        }

        ShopperApplication.getInstance().getBus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        ShopperApplication.getInstance().getBus().unregister(this);
    }

    @Subscribe
    public void setReceiptAfterTakePicture(TakePictureEvent e) {
        LogUtils.LOG("setReceiptAfterTakePicture >> " + e);

        if (e.getData() != null) {
            int idx = 0;
            ReceiptItem receiptItem = e.getData();
            for (ReceiptItem item : mReceiptItemList) {
                if (item.getPosition() == receiptItem.getPosition()) {
                    break;
                }
                idx++;
            }

            mReceiptItemList.remove(idx);
            mReceiptItemList.add(idx, receiptItem);

            tryToEnableButton(receiptItem);
        }
    }


    private void tryToEnableButton(ReceiptItem receiptItem) {
        if (receiptItem.completedWithPicture()) {
            nextButton.setEnabled(true);
        }
    }

    @OnClick(R.id.next_receipt_button)
    void checkPaymentComplete() {
        int currentPosition = mViewPager.getCurrentItem();
        if (currentPosition + 1 == mReceiptItemList.size()) {
            if (mBatch.hasPayAllShopping() && ProductSampleItem.isThereOutStoreSample(mBatchId)) {
                productSampleScreen();
            } else {
                MaterialDialog.Builder inputDialog = new MaterialDialog.Builder(getActivity());
                inputDialog.title(R.string.alert_title_warning);

                if (mShipment.getOrder().isEligibleForShoppingBag()) {
                    inputDialog.customView(R.layout.dialog_payment, false);
                } else {
                    inputDialog.content(R.string.alert_message_complete_payment);
                }

                inputDialog.negativeText(R.string.alert_button_no);
                inputDialog.positiveText(R.string.alert_button_yes);
                inputDialog.callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialoe) {
                        completePayment();
                    }
                });

                inputDialog.show();
            }
        } else {
            mViewPager.setCurrentItem(currentPosition + 1);
        }
    }

    private void completePayment() {
        if (ShopperApplication.getInstance().hasPendingQueue()) {
            showPendingQueueDialog();
            return;
        }

        Batch batch = ShopperApplication.getInstance().getBatch();

        nextButton.setEnabled(false);
        progressIndicator.setVisibility(View.VISIBLE);
        progressOverlay.setVisibility(View.VISIBLE);

        ReceiptCollection collection = new ReceiptCollection(mReceiptItemList);

        ShopperApplication.getInstance().getBatchManager()
                .pay(batch.getRemoteId(), mShipment.getRemoteId(), collection, new DataListener() {
                    @Override
                    public void onCompleted(boolean success, Object data) {
                        if (getActivity() != null) {
                            progressIndicator.setVisibility(View.GONE);
                            progressOverlay.setVisibility(View.GONE);
                        }

                        if (success) {
                            stopShoppingTimer();
                            if (ProductSampleItem.isThereOutStoreSample(mBatch.getRemoteId())) {
                                if (mBatch.hasPayAllShopping()) {
                                    productSampleScreen();
                                } else {
                                    backToShipmentList();
                                }
                            } else {
                                if (ProductSampleItem.isThereInStoreSample(mBatch.getRemoteId())) {
                                    postSampleInStore();
                                } else {
                                    backToShipmentList();
                                }
                            }
                        } else {
                            if (getActivity() != null) {
                                nextButton.setEnabled(true);
                                if (data == null) {
                                    showUnableToFinishDialog(getString(R.string.operation_timed_out));
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailed(Throwable exception) {
                        if (exception != null) {
                            if (exception instanceof ExceptionResponse) {
                                ExceptionResponse exceptionResponse = (ExceptionResponse) exception;
                                if (exceptionResponse.getType() != null && exceptionResponse.getType()
                                        .equalsIgnoreCase(OrderCanceledException)) {
                                    PaymentReceiptActivity paymentReceiptActivity = (PaymentReceiptActivity) getActivity();
                                    if (paymentReceiptActivity != null) {
                                        paymentReceiptActivity.showOrderCanceledDialog(PAY);
                                    }
                                } else {
                                    String message = exception.getLocalizedMessage();
                                    showUnableToFinishDialog(message);
                                }
                            } else {
                                String message = exception.getLocalizedMessage();
                                showUnableToFinishDialog(message);
                            }
                        }

                        if (getActivity() != null) {
                            nextButton.setEnabled(true);
                            progressIndicator.setVisibility(View.GONE);
                            progressOverlay.setVisibility(View.GONE);
                        }
                    }
                });
    }

    private void postSampleInStore() {
        new SampleUtils(mBatch).setIsOutOfStore(false).withStockLocation(mStockLocation).retrieveSamples()
                .createThenPostPayload(new DataListener<Boolean>() {
                    @Override
                    public void onCompleted(boolean success, Boolean sampleExist) {
                        if (success) {
                            backToShipmentList();
                        } else {
                            if (getActivity() != null) {
                                Toast.makeText(ShopperApplication.getContext(),
                                        getString(R.string.toast_unable_post_product_sample_list), Toast.LENGTH_LONG)
                                        .show();
                            }
                        }
                    }

                    @Override
                    public void onFailed(Throwable exception) {
                        Toast.makeText(ShopperApplication.getContext(), getString(R.string.something_went_wrong),
                                Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void productSampleScreen() {
        Intent intent = new Intent(getPaymentReceiptActivity(), ProductSampleListActivity.class);
        intent.putExtra(ProductSampleListActivity.SELECTED_BATCH, mBatchId);
        intent.putExtra(SHIPMENT_ID, mShipment.getRemoteId());
        intent.putExtra(Constant.IS_OUT_OF_STORE, true);
        startActivity(intent);
    }

    private void showPendingQueueDialog() {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity());
        builder.title(R.string.alert_title_warning);
        builder.content(R.string.alert_message_pending_queue);
        builder.positiveText(R.string.alert_button_ok);

        builder.show();
    }

    private void stopShoppingTimer() {
        Sprinkles.stores(ShoppingStore.class).setShoppingTimer(mBatchId, 0);
    }

    private void backToShipmentList() {
        if (getActivity() != null) {
            Batch batch = ShopperApplication.getInstance().getBatch();
            if (batch.hasPayAllShopping()) {
                PaymentReceiptActivity paymentReceiptActivity = (PaymentReceiptActivity) getActivity();
                paymentReceiptActivity.backToShipmentList();
            } else {
                if (AppUtils.currentUserType() == AppUtils.UserType.RANGER) {
                    if (batch.hasPayAllShoppingInStore(mShipment.getSlot().getStockLocation().getRemoteId())) {
                        SharedPrefUtils.remove(getActivity(), SELECTED_SHOPPING_STORE);

                        ShoppingListRouter router = new ShoppingListRouter(getActivity());
                        router.startWithBatchClearTop(batch);
                    } else {
                        getActivity().setResult(Activity.RESULT_OK);
                        getActivity().finish();
                    }
                } else {
                    getActivity().setResult(Activity.RESULT_OK);
                    getActivity().finish();
                }
            }
        }
    }

    private void showUnableToFinishDialog(String message) {
        if (getActivity() == null) {
            return;
        }

        MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity());
        builder.title(R.string.alert_title_warning);
        builder.content(message);
        builder.positiveText(R.string.alert_button_ok);

        builder.show();
    }

    public PaymentReceiptActivity getPaymentReceiptActivity() {
        return (PaymentReceiptActivity) getActivity();
    }

}