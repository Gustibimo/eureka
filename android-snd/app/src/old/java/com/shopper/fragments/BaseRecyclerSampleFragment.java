package com.shopper.fragments;

import android.content.Intent;
import android.view.View;
import android.widget.Toast;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.Batch;
import com.happyfresh.snowflakes.hoverfly.models.ProductSampleItem;
import com.happyfresh.snowflakes.hoverfly.models.StockLocation;
import com.shopper.activities.BatchPaymentActivity;
import com.shopper.activities.ProductSampleListActivity;
import com.shopper.common.ShopperApplication;
import com.shopper.listeners.DataListener;
import com.shopper.utils.SampleUtils;

import static com.shopper.common.Constant.BATCH_ID;
import static com.shopper.common.Constant.STOCK_LOCATION_ID;

/**
 * Created by numannaufal on 10/1/16.
 */
public class BaseRecyclerSampleFragment extends BaseRecyclerFragment {

    protected Batch mBatch;

    protected StockLocation mStockLocation;

    protected View progressContainer;

    @Override
    public boolean isSwipeRefreshLayoutEnabled() {
        return false;
    }

    protected void fetchProductSample() {
        showProgress(true);
        new SampleUtils().withBatch(getBatch()).withStockLocation(getStockLocation())
                .fetchProductSample(new DataListener<Boolean>() {
                    @Override
                    public void onCompleted(boolean success, Boolean sampleExist) {
                        if (success) {
                            showProgress(false);
                            if (ProductSampleItem.isThereInStoreSample(getBatch().getRemoteId())) {
                                productSampleScreen();
                            } else {
                                paymentScreen();
                            }
                        } else {
                            if (getActivity() != null) {
                                Toast.makeText(ShopperApplication.getContext(),
                                        getString(R.string.toast_unable_check_product_sample_list), Toast.LENGTH_LONG)
                                        .show();
                                showProgress(false);
                            }
                        }
                    }

                    @Override
                    public void onFailed(Throwable exception) {
                        paymentScreen();
                    }
                });
    }

    protected void productSampleScreen() {
        Intent intent = new Intent(getActivity(), ProductSampleListActivity.class);
        intent.putExtra(ProductSampleListActivity.SELECTED_BATCH, getBatch().getRemoteId());
        startActivity(intent);
    }

    public void showProgress(boolean show) {
        getProgressContainer().setVisibility(show ? View.VISIBLE : View.GONE);
    }

    protected void paymentScreen() {
        Intent intent = new Intent(getActivity(), BatchPaymentActivity.class);
        intent.putExtra(BATCH_ID, getBatch().getRemoteId());
        intent.putExtra(STOCK_LOCATION_ID, getStockLocation().getRemoteId());
        intent.putExtra(ProductSampleListActivity.SELECTED_BATCH, getBatch().getRemoteId());
        startActivity(intent);
    }

    public Batch getBatch() {
        return mBatch;
    }

    public void setBatch(Batch mBatch) {
        this.mBatch = mBatch;
    }

    public StockLocation getStockLocation() {
        return mStockLocation;
    }

    public void setStockLocation(StockLocation mStockLocation) {
        this.mStockLocation = mStockLocation;
    }

    public View getProgressContainer() {
        return progressContainer;
    }

    public void setProgressContainer(View progressContainer) {
        this.progressContainer = progressContainer;
    }

}
