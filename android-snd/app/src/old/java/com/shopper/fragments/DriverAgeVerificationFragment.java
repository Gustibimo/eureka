package com.shopper.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import com.happyfresh.fulfillment.R;
import com.shopper.components.CircularProgressBar;

import butterknife.ButterKnife;
import butterknife.BindView;

/**
 * Created by ifranseda on 2/10/16.
 */
public class DriverAgeVerificationFragment extends Fragment {

    @BindView(R.id.content)
    ScrollView content;

    @BindView(R.id.disclaimer)
    TextView disclaimer;

    @BindView(R.id.birthdayTextView)
    TextView birthdayTextView;

    @BindView(R.id.continue_button)
    Button continueButton;

    @BindView(R.id.progress_container)
    View progressContainer;

    @BindView(R.id.fragment_progressbar)
    CircularProgressBar progressbar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_driver_age_verification, null);
        ButterKnife.bind(this, view);

        return view;
    }
}
