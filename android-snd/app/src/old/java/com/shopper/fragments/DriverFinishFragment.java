package com.shopper.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.Batch;
import com.happyfresh.snowflakes.hoverfly.models.Country;
import com.happyfresh.snowflakes.hoverfly.models.Order;
import com.happyfresh.snowflakes.hoverfly.models.Shipment;
import com.happyfresh.snowflakes.hoverfly.models.Slot;
import com.happyfresh.snowflakes.hoverfly.utils.FormatterUtils;
import com.happyfresh.snowflakes.hoverfly.utils.ResourceUtils;
import com.shopper.activities.DriverBatchDeliveryActivity;
import com.shopper.activities.DriverDeliveryListActivity;
import com.shopper.activities.DriverFinishActivity;
import com.shopper.activities.DriverListActivity;
import com.shopper.activities.DriverSignatureActivity;
import com.shopper.common.Constant;
import com.shopper.common.ShopperApplication;
import com.shopper.components.CircularProgressBar;
import com.shopper.listeners.DataListener;
import com.shopper.utils.ActivityUtils;
import com.shopper.utils.AppUtils;
import com.shopper.utils.BitmapUtils;
import com.shopper.utils.LogUtils;
import com.shopper.utils.SharedPrefUtils;
import com.shopper.utils.StringUtils;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by rsavianto on 12/22/14.
 */
public class DriverFinishFragment extends Fragment {
    public static final int SIGNATURE = 999;
    public static final int FINALIZE_REQUEST_CODE = 111;
    public static final String IS_FINALIZE = "finalize";

    public static boolean finalize;

    @BindView(R.id.content)
    ScrollView mContent;

    @BindView(R.id.progress_container)
    View mProgressContainer;

    @BindView(R.id.fragment_progressbar)
    CircularProgressBar mProgressBar;

    @BindView(R.id.finish_button)
    Button mFinishButton;

    @BindView(R.id.signature_image)
    ImageView mSignatureImage;

    @BindView(R.id.signature_label)
    TextView mSignatureLabel;

    @BindView(R.id.delivery_order_receiver)
    TextView mReceiver;

    @BindView(R.id.delivery_order_receiver_label)
    TextView mReceiverLabel;

    @BindView(R.id.delivery_cod_layout)
    View mCodLayout;

    @BindView(R.id.delivery_cod_amount)
    TextView mAmountCod;

    Double mTotalInvoiced = 0.0;

    @BindView(R.id.delivery_time)
    TextView deliveryTime;

    @BindView(R.id.delivery_location)
    TextView deliveryLocation;

    @BindView(R.id.delivery_location_address1)
    TextView deliveryLocationAddress1;

    @BindView(R.id.delivery_location_address2)
    TextView deliveryLocationAddress2;

    @BindView(R.id.arrival_time)
    TextView arrivalTime;

    @BindView(R.id.arrival_duration)
    TextView arrivalDuration;

    @BindView(R.id.happycorporate_layout)
    View happyCorporateLayout;

    @BindView(R.id.delivery_payment_layout)
    View deliveryPaymentLayout;

    @BindView(R.id.payment_method_icon)
    ImageView paymentMethodIcon;

    @BindView(R.id.payment_method)
    TextView paymentMethodText;

    @BindView(R.id.total_order)
    TextView totalOrder;

    @BindView(R.id.complete_instruction)
    TextView completeInstruction;

    @BindView(R.id.order_number)
    TextView orderNumber;

    @BindView(R.id.customer_name)
    TextView customerName;

    @BindView(R.id.delivery_age_verification_layout)
    View ageVerificationLayout;

    @BindView(R.id.age_verification_checkbox)
    CheckBox ageVerificationCheckbox;

    @BindView(R.id.delivery_location_address_details)
    TextView deliveryLocationAddressDetails;

    @BindView(R.id.delivery_location_address_number)
    TextView deliveryLocationAddressNumber;

    @BindView(R.id.delivery_location_details_text)
    TextView deliveryLocationDetailsText;

    boolean customerNameEntered;

    private Location mLocation;
    private Double mLat = null;
    private Double mLon = null;

    private Unbinder unbinder;

    public static DriverFinishFragment newInstance() {
        DriverFinishFragment fragment = new DriverFinishFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getActivity().getIntent().getExtras();
        if (extras != null) {
            finalize = extras.getBoolean(IS_FINALIZE, false);
        } else {
            finalize = false;
        }

        if (savedInstanceState != null) {
            finalize = savedInstanceState.getBoolean("FINALIZED", false);
            mTotalInvoiced = savedInstanceState.getDouble("TOTAL_INVOICED", 0.0);
            customerNameEntered = savedInstanceState.getBoolean("CUSTOMER_NAME_ENTERED", false);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("FINALIZED", finalize);
        outState.putDouble("TOTAL_INVOICED", mTotalInvoiced);
        outState.putBoolean("CUSTOMER_NAME_ENTERED", customerNameEntered);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_driver_finish, null);
        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (savedInstanceState == null) {
            clearSignature();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        updateView();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!finalize) {
            openDeliveryItems();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SIGNATURE) {
            if (resultCode == Activity.RESULT_OK) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        File file = BitmapUtils.getSignatureImage(ShopperApplication.getContext());
                        if (!file.exists()) {
                            try {
                                file.createNewFile();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        Picasso.with(getActivity()).load(file).skipMemoryCache().into(mSignatureImage);
                        tryEnableButton();
                    }
                }, 500);
            } else {
                clearSignature();
                tryEnableButton();
            }

        } else if (requestCode == FINALIZE_REQUEST_CODE) {
            finalize = true;

            if (resultCode == Activity.RESULT_OK) {
                Shipment shipment = ShopperApplication.getInstance().getCurrentShipment();
                double invoiced = shipment.getOrder().getTotal();
                mTotalInvoiced = shipment.getOrder().getTotal();

                String total = "";
                if (invoiced > 0) {
                    total = getNumberFormatter(shipment.getOrder().getCurrency()).format(invoiced);
                } else {
                    total = getNumberFormatter(shipment.getOrder().getCurrency()).format(0);
                }
                totalOrder.setText(total);

                if (shipment.getOrder().getPaymentMethod() != null) {
                    if (shipment.getOrder().getCompanyId() != 0) {
                        showAsHappyCorporate();
                    }
                    else {
                        if (shipment.getOrder().getPaymentMethod().equals(Constant.CashOnDeliveryMethod)) {
                            showAsCODPayment();
                            showTotalInvoiceDialog(total, shipment.getOrder());
                        }
                        else {
                            showAsCCPayment();
                            if (shipment.getOrder().isCCPromotionExistAndNotEligible()) {
                                showCreditCardPromotionDisabledDialog();
                            }
                        }
                    }
                }

                tryEnableButton();
            } else {
                Shipment shipment = ShopperApplication.getInstance().getCurrentShipment();
                mTotalInvoiced = shipment.getOrder().getTotal();
            }

            File file = BitmapUtils.getSignatureImage(ShopperApplication.getContext());
            if (file.exists()) {
                Picasso.with(getActivity()).load(file).into(mSignatureImage);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    void showTotalInvoiceDialog(final String total, final Order order) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                View contentDialog = LayoutInflater.from(getActivity())
                        .inflate(R.layout.layout_total_confirm_dialog, null);

                TextView itemTotal = (TextView) contentDialog
                        .findViewById(R.id.dialog_confirm_total_order);

                itemTotal.setText(total);

                LinearLayout creditCardPromotionDisabled = (LinearLayout) contentDialog
                        .findViewById(R.id.dialog_confirm_cc_promo_disabled_container);

                if (order.isCCPromotionExistAndNotEligible()) {
                    creditCardPromotionDisabled.setVisibility(View.VISIBLE);
                }

                MaterialDialog.Builder dialog = new MaterialDialog
                        .Builder(getActivity())
                        .customView(contentDialog, false)
                        .positiveText(R.string.alert_button_ok)
                        .backgroundColor(Color.WHITE)
                        .cancelable(false);

                dialog.build().show();
            }
        }, 500);
    }

    void showCreditCardPromotionDisabledDialog() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                View contentDialog = LayoutInflater.from(getActivity())
                        .inflate(R.layout.layout_total_confirm_dialog, null);

                TextView dialogTitle = (TextView) contentDialog
                        .findViewById(R.id.dialog_confirm_title);

                dialogTitle.setText(getString(R.string.credit_card_promotion_disabled_title));

                LinearLayout codDialogContainer = (LinearLayout) contentDialog
                        .findViewById(R.id.dialog_confirm_body_container_cod);

                codDialogContainer.setVisibility(View.GONE);

                TextView dialogBodyCC = (TextView) contentDialog
                        .findViewById(R.id.dialog_confirm_body_cc);

                dialogBodyCC.setVisibility(View.VISIBLE);

                MaterialDialog.Builder dialog = new MaterialDialog
                        .Builder(getActivity())
                        .customView(contentDialog, false)
                        .positiveText(R.string.alert_button_ok)
                        .backgroundColor(Color.WHITE)
                        .cancelable(false);

                dialog.build().show();
            }
        }, 500);
    }

    public void openDeliveryItems() {
        Intent intent = new Intent(getActivity(), DriverDeliveryListActivity.class);
        startActivityForResult(intent, FINALIZE_REQUEST_CODE);
    }

    private void updateView() {
        mProgressContainer.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.INVISIBLE);
        mContent.setVisibility(View.VISIBLE);

        Shipment shipment = ShopperApplication.getInstance().getCurrentShipment();

        // Avoid exception on null shipment
        if (shipment == null) {
            return;
        }

        Slot slot = shipment.getSlot();

        deliveryLocation.setText(shipment.getAddress().getAddress1());
        deliveryLocationAddress1.setText(shipment.getAddress().getAddress2());
        deliveryLocationAddress2.setText(StringUtils.buildAddressCityInfo(shipment.getAddress()));

        if (shipment.getAddress().getAddressDetail() != null && !shipment.getAddress().getAddressDetail().trim()
                .isEmpty()) {
            deliveryLocationAddressDetails.setText(shipment.getAddress().getAddressDetail());

            deliveryLocationAddressDetails.setVisibility(View.VISIBLE);
            deliveryLocationDetailsText.setVisibility(View.VISIBLE);
        } else {
            deliveryLocationAddressDetails.setVisibility(View.GONE);
            deliveryLocationDetailsText.setVisibility(View.GONE);
        }

        if (shipment.getAddress().getAddressNumber() != null && !shipment.getAddress().getAddressNumber().trim()
                .isEmpty()) {
            deliveryLocationAddressNumber.setText(shipment.getAddress().getAddressNumber());
            deliveryLocationAddressNumber.setVisibility(View.VISIBLE);
        }
        else {
            deliveryLocationAddressNumber.setVisibility(View.GONE);
        }

        if (TextUtils.isEmpty(shipment.getAddress().getAddress2())) {
            deliveryLocationAddress1.setVisibility(View.GONE);
        }

        deliveryTime.setText(StringUtils.showDeliveryTime(slot.getStartTime(), slot.getEndTime()));

        completeInstruction.setText(shipment.getAddress().getDeliveryInstruction());

        String arrivedAtText = StringUtils.showInLocal(new Date(System.currentTimeMillis()));
        if (shipment.getDeliveryJob() != null && shipment.getDeliveryJob().arrivedAt() != null) {
            Date arrivedAt = shipment.getDeliveryJob().arrivedAt();
            arrivedAtText = StringUtils.showInLocal(arrivedAt);
        }
        arrivalTime.setText(arrivedAtText);

        arrivalDuration.setText(StringUtils.showDuration(getActivity(), shipment.getDeliveryJob().arrivalDuration(), false));

        mReceiver.setText(shipment.getAddress().getFullName());

        mCodLayout.setVisibility(View.GONE);

        if (shipment.getOrder() != null && shipment.getOrder().getPaymentMethod() != null &&
                shipment.getOrder().getPaymentMethod().equals(Constant.CashOnDeliveryMethod)) {
            showAsCODPayment();
        } else {
            showAsCCPayment();
        }

        String total = getNumberFormatter(shipment.getOrder().getCurrency())
                .format(shipment.getOrder().getTotal());
        totalOrder.setText(total);


        orderNumber.setText(getString(R.string.order_with_purchases_number_tail, shipment.getOrder().getNumber(), shipment.getOrder().getPurchases()));
        customerName.setText(shipment.getCustomerName());


        boolean ageVerificationRequired = SharedPrefUtils.getBoolean(getActivity(), Constant.CONFIG_STATUS.REQUIRE_AGE_VERIFICATION);
        if (ageVerificationRequired) {
            ageVerificationLayout.setVisibility(View.GONE);
        } else {
            ageVerificationLayout.setVisibility(View.VISIBLE);

            String disclaimer;
            String deviceCountryCode = getResources().getConfiguration().locale.getCountry();
            Country shipmentCountry = shipment.getSlot().getStockLocation().getCountry();

            if (shipmentCountry != null && deviceCountryCode.equalsIgnoreCase(shipmentCountry.getIsoName())) {
                disclaimer = SharedPrefUtils.getString(ShopperApplication.getContext(), Constant.CONFIG_STATUS.AGE_VERIFICATION_WORDING);
            } else {
                disclaimer = SharedPrefUtils.getString(ShopperApplication.getContext(), Constant.CONFIG_STATUS.AGE_VERIFICATION_WORDING_ENGLISH);
            }

            if (TextUtils.isEmpty(disclaimer)) {
                disclaimer = getString(R.string.default_age_verification_disclaimer);
            }

            ageVerificationCheckbox.setText(disclaimer);
        }

        tryEnableButton();
    }

    private void showAsHappyCorporate() {
        happyCorporateLayout.setVisibility(View.VISIBLE);
        deliveryPaymentLayout.setVisibility(View.GONE);
    }

    private void showAsCODPayment() {
        happyCorporateLayout.setVisibility(View.GONE);
        deliveryPaymentLayout.setVisibility(View.VISIBLE);

        deliveryPaymentLayout.setBackgroundColor(getResources().getColor(R.color.payment_cod_background));
        paymentMethodIcon.setImageResource(R.drawable.icon_cod);
        paymentMethodText.setText(R.string.payment_total_cod);

        paymentMethodText.setTextColor(getResources().getColor(android.R.color.black));
        totalOrder.setTextColor(getResources().getColor(android.R.color.black));
    }

    private void showAsCCPayment() {
        happyCorporateLayout.setVisibility(View.GONE);
        deliveryPaymentLayout.setVisibility(View.VISIBLE);

        deliveryPaymentLayout.setBackgroundColor(getResources().getColor(R.color.payment_cc_background));
        paymentMethodIcon.setImageResource(R.drawable.icon_cc);
        paymentMethodText.setText(R.string.payment_total_credit_card);

        paymentMethodText.setTextColor(getResources().getColor(android.R.color.white));
        totalOrder.setTextColor(getResources().getColor(android.R.color.white));
    }

    @OnClick(R.id.delivery_order_receiver_layout)
    void inputCustomer() {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View customerView = inflater.inflate(R.layout.dialog_customer_name, null);

        MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity());

        final EditText editText = (EditText) customerView.findViewById(R.id.dialog_customer_name);

        builder.title(R.string.driver_input_customer_name);
        builder.customView(customerView, false);

        builder.negativeText(R.string.alert_button_cancel);
        builder.positiveText(R.string.alert_button_ok);
        builder.callback(new MaterialDialog.ButtonCallback() {
            @Override
            public void onPositive(MaterialDialog dialog) {
                String text = editText.getText().toString();
                if (!TextUtils.isEmpty(text)) {
                    mReceiver.setText(editText.getText().toString());

                    mReceiver.setTextColor(ContextCompat.getColor(getActivity(), R.color.text_body));
                    mReceiverLabel.setTextColor(ResourceUtils.getAttributedColor(getActivity(), R.attr.colorTextLabel));

                    customerNameEntered = true;
                } else {
                    Shipment shipment = ShopperApplication.getInstance().getCurrentShipment();
                    if (shipment != null) {
                        mReceiver.setText(shipment.getAddress().getFullName());
                    }

                    mReceiver.setTextColor(ContextCompat.getColor(getActivity(), R.color.text_view_hint));
                    mReceiverLabel.setTextColor(ContextCompat.getColor(getActivity(), android.R.color.holo_red_dark));

                    customerNameEntered = false;
                }
            }
        });

        MaterialDialog inputDialog = builder.build();
        inputDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        inputDialog.show();
    }

    @OnClick(R.id.signature_layout)
    void inputCustomerSignature() {
        Intent intent = new Intent(getActivity(), DriverSignatureActivity.class);
        startActivityForResult(intent, SIGNATURE);
    }

    private void showPendingQueueDialog() {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity());
        builder.title(R.string.alert_title_warning);
        builder.content(R.string.alert_message_pending_queue);

        builder.positiveText(R.string.alert_button_ok);

        builder.show();
    }

    private NumberFormat getNumberFormatter(String currencyCode) {
        return FormatterUtils.getNumberFormatter(currencyCode);
    }

    private void tryEnableButton() {
        boolean completed = false;

        File file = BitmapUtils.getSignatureImage(ShopperApplication.getContext());
        if (file != null && file.exists() && file.length() > 0) {
            completed = true;
            mSignatureLabel.setTextColor(ResourceUtils.getAttributedColor(getActivity(), R.attr.colorTextLabel));
        }

        completed &= finalize;

        mFinishButton.setEnabled(completed);
    }

    @OnClick(R.id.finish_button)
    void finishButtonOnClick() {
        Shipment shipment = ShopperApplication.getInstance().getCurrentShipment();
        if (shipment == null) {
            return;
        }

        MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity());
        builder.title(R.string.alert_title_warning);
        builder.positiveText(R.string.alert_button_yes);
        builder.negativeText(R.string.alert_button_no);
        builder.callback(new MaterialDialog.ButtonCallback() {
            @Override
            public void onPositive(MaterialDialog dialog) {
                finish();
            }
        });

        if (shipment.getOrder() != null && shipment.getOrder().getCompanyId() == 0 && shipment.getOrder().getPaymentMethod() != null &&
                shipment.getOrder().getPaymentMethod().equals(Constant.CashOnDeliveryMethod)) {

            LogUtils.logEvent("Driver Finish", "Shipment ID: " + shipment.getRemoteId());
            LogUtils.logEvent("Driver Finish", "Order: " + shipment.getOrder().getNumber());

            if(shipment.getOrder().isEligibleForShoppingBag()){
                builder.customView(R.layout.dialog_driver_finish_cod, false);
            }else{
                builder.content(R.string.alert_message_cod_amount_is_collected);
            }
        } else {
            if(shipment.getOrder().isEligibleForShoppingBag()){
                builder.customView(R.layout.dialog_driver_finish, false);
            }else{
                builder.content(R.string.alert_message_driver_finish);
            }
        }
        builder.show();
    }

    void finish() {
        if (ShopperApplication.getInstance().hasPendingQueue()) {
            showPendingQueueDialog();
            return;
        }

        finalize = false;

        final ShopperApplication application = ShopperApplication.getInstance();
        Batch batch = application.getBatch();
        Shipment shipment = application.getCurrentShipment();

        String receiver = mReceiver.getText().toString();
        String cashCod = mTotalInvoiced.toString();

        File file = BitmapUtils.getSignatureImage(ShopperApplication.getContext());

        long fileSize = (file != null && file.exists()) ? file.length() : 0;
        if (fileSize == 0) {
            MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity());
            builder.cancelable(false);
            builder.title(R.string.alert_title_warning);
            builder.content(R.string.signature_is_empty);
            builder.positiveText(R.string.alert_button_ok);
            builder.show();

            return;
        }

        mProgressContainer.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.VISIBLE);
        mFinishButton.setEnabled(false);
        mFinishButton.setText(R.string.driver_waiting);

        if (mLocation != null) {
            mLat = mLocation.getLatitude();
            mLon = mLocation.getLongitude();
        }

        application.getBatchManager().finish(batch.getRemoteId(), shipment.getRemoteId(), file, receiver, cashCod, mLat, mLon, new DataListener() {
            @Override
            public void onCompleted(boolean success, Object data) {
                if (getActivity() == null) return;

                mProgressContainer.setVisibility(View.GONE);
                mProgressBar.setVisibility(View.INVISIBLE);
                mFinishButton.setEnabled(true);
                mFinishButton.setText(R.string.driver_finish);

                if (success) {
                    clearSignature();
                    driverListScreen();
                }
            }

            @Override
            public void onFailed(Throwable exception) {
                if (exception != null) {
                    boolean orderCanceled = Constant.OrderCanceledException.equalsIgnoreCase(exception.getMessage());

                    if (orderCanceled) {
                        final DriverFinishActivity activity = (DriverFinishActivity) getActivity();
                        activity.showOrderCanceledDialog(Constant.CanceledDialogType.FINISH);
                    } else {
                        boolean alreadyFinished = Constant.AlreadyFinishedException.equalsIgnoreCase(exception.getMessage());
                        boolean alreadyCancelledLegacy = Constant.AlreadyCancelledException.equalsIgnoreCase(exception.getMessage());
                        if (alreadyFinished || alreadyCancelledLegacy) {
                            driverListScreen();
                        } else {
                            MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity());
                            builder.title(R.string.alert_title_error);
                            builder.content(exception.getLocalizedMessage());
                            builder.positiveText(R.string.alert_button_ok);
                            builder.show();
                        }
                    }
                } else {
                    MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity());
                    builder.title(R.string.alert_title_error);
                    builder.content("Unknown error");
                    builder.positiveText(R.string.alert_button_ok);
                    builder.show();
                }

                mProgressContainer.setVisibility(View.GONE);
                mProgressBar.setVisibility(View.INVISIBLE);
                mFinishButton.setEnabled(true);
                mFinishButton.setText(R.string.driver_finish);
            }
        });
    }

    void driverListScreen() {
        AppUtils.changeBackUserType();

        Batch batch = ShopperApplication.getInstance().getBatch();
        if (batch == null || batch.isCompleted()) {
            ShopperApplication.getInstance().setBatch(null);
            ActivityUtils.startClearTop(getActivity(), DriverListActivity.class);
        } else {
            ActivityUtils.startClearTop(getActivity(), DriverBatchDeliveryActivity.class);
        }
    }

    void clearSignature() {
        // Delete Signature File
        File file = BitmapUtils.getSignatureImage(ShopperApplication.getContext());
        file.delete();

        mSignatureImage.setImageBitmap(null);
    }

    public void setLocation(Location location) {
        mLocation = location;
    }
}
