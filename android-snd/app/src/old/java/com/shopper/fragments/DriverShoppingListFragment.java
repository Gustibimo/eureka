package com.shopper.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.TaskStackBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.LineItem;
import com.happyfresh.snowflakes.hoverfly.models.Shipment;
import com.happyfresh.snowflakes.hoverfly.models.Variant;
import com.happyfresh.snowflakes.hoverfly.utils.FormatterUtils;
import com.shopper.activities.DriverAcceptanceActivity;
import com.shopper.activities.DriverArriveActivity;
import com.shopper.activities.DriverListActivity;
import com.shopper.adapters.DeliveryItemAdapter;
import com.shopper.common.Constant;
import com.shopper.common.ShopperApplication;
import com.shopper.components.CircularProgressBar;
import com.shopper.listeners.DataListener;
import com.shopper.utils.LogUtils;
import com.shopper.utils.SharedPrefUtils;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.BindView;
import butterknife.Unbinder;

/**
 * Created by rsavianto on 12/24/14.
 */
public class DriverShoppingListFragment extends BaseRecyclerFragment {
    @BindView(R.id.progress_container)
    View mProgressContainer;

    @BindView(R.id.progress_indicator)
    CircularProgressBar mProgressBar;

    @BindView(R.id.progress_label)
    TextView mProgressLabel;

    @BindView(R.id.happycorporate_layout)
    View happyCorporateLayout;

    @BindView(R.id.delivery_payment_layout)
    View deliveryPaymentLayout;

    @BindView(R.id.payment_method_icon)
    ImageView paymentMethodIcon;

    @BindView(R.id.payment_method)
    TextView paymentMethodText;

    @BindView(R.id.total_order)
    TextView totalOrder;

    Shipment shipment;

    DeliveryItemAdapter mAdapter;

    List<LineItem> lineItems = new ArrayList<LineItem>();

    private Unbinder unbinder;

    public static DriverShoppingListFragment newInstance(Shipment shipment) {
        DriverShoppingListFragment fragment = new DriverShoppingListFragment();
        Bundle bundle = new Bundle();
        bundle.putLong("SHIPMENT_ID", shipment.getRemoteId());
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.driver_item_recycler_list, null);
        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public boolean isSwipeRefreshLayoutEnabled() {
        return true;
    }

    @Override
    public void onSwipeRefresh() {
        loadData();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mProgressBar.setVisibility(View.INVISIBLE);

        mAdapter = new DeliveryItemAdapter(getActivity(), lineItems);
        mRecyclerView.setAdapter(mAdapter);

        Long shipmentId = getArguments().getLong("SHIPMENT_ID");
        if (shipmentId == null) {
            return;
        }

        shipment = Shipment.findById(shipmentId);
        if (shipment.getOrder().getCompanyId() != 0) {
            happyCorporateLayout.setVisibility(View.VISIBLE);
            deliveryPaymentLayout.setVisibility(View.GONE);
        }
        else {
            if (shipment.getOrder().getPaymentMethod() != null && shipment.getOrder().getPaymentMethod()
                .equals(Constant.CashOnDeliveryMethod)) {

                happyCorporateLayout.setVisibility(View.GONE);
                deliveryPaymentLayout.setVisibility(View.VISIBLE);

                deliveryPaymentLayout.setBackgroundColor(getResources().getColor(R.color.payment_cod_background));
                paymentMethodIcon.setImageResource(R.drawable.icon_cod);
                paymentMethodText.setText(R.string.payment_total_cod);

                paymentMethodText.setTextColor(getResources().getColor(android.R.color.black));
                totalOrder.setTextColor(getResources().getColor(android.R.color.black));
            }
            else {
                happyCorporateLayout.setVisibility(View.GONE);
                deliveryPaymentLayout.setVisibility(View.VISIBLE);

                    deliveryPaymentLayout.setBackgroundColor(getResources().getColor(R.color.payment_cc_background));
                paymentMethodIcon.setImageResource(R.drawable.icon_cc);
                paymentMethodText.setText(R.string.payment_total_credit_card);

                paymentMethodText.setTextColor(getResources().getColor(android.R.color.white));
                totalOrder.setTextColor(getResources().getColor(android.R.color.white));
            }
        }

        String total = getNumberFormatter(shipment.getOrder().getCurrency()).format(shipment.getOrder().getTotal());
        totalOrder.setText(total);

        loadData();
    }

    private NumberFormat getNumberFormatter(String currencyCode) {
        return FormatterUtils.getNumberFormatter(currencyCode);
    }

    private void loadData() {
        if (DriverAcceptanceActivity.isAccepting) {
            mSwipeRefreshLayout.setRefreshing(false);
            return;
        }

        mSwipeRefreshLayout.setRefreshing(true);
        mProgressContainer.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.INVISIBLE);

        ShopperApplication.getInstance().getShipmentManager().getAllItems(shipment.getRemoteId(), new DataListener() {
            @Override
            public void onCompleted(boolean success, Object data) {
                if (getActivity() != null) {
                    mProgressContainer.setVisibility(View.GONE);
                    mProgressBar.setVisibility(View.INVISIBLE);
                    mSwipeRefreshLayout.setRefreshing(false);

                    if (success) {
                        lineItems.clear();
                        mAdapter.notifyDataSetChanged();

                        if (data != null) {
                            List<LineItem> items = (List<LineItem>) data;
                            sortItems(items);
                            lineItems.addAll(new ArrayList(items));
                            shipment.setDeliveryItems(lineItems);
                            mAdapter.notifyDataSetChanged();
                            mAdapter.checkShoppingBag();
                        }
                    }
                }
            }
        });
    }

    private void sortItems(List<LineItem> items) {
        Collections.sort(items, new Comparator<LineItem>() {
            @Override
            public int compare(LineItem a, LineItem b) {
                String a2 = (a.getTaxonName() != null) ? a.getTaxonName() : "";
                String b2 = (b.getTaxonName() != null) ? b.getTaxonName() : "";
                int c = a2.compareToIgnoreCase(b2);

                if (c == 0) {
                    c = a.getVariant().getName().compareToIgnoreCase(b.getVariant().getName());
                }

                return c;
            }
        });

        Collections.sort(items, new Comparator<LineItem>() {
            @Override
            public int compare(LineItem a, LineItem b) {
                Integer a1 = a.getTaxonOrder();
                Integer b1 = b.getTaxonOrder();
                int c = b1.compareTo(a1);

                if (c != 0) {
                    if (a1 > b1) {
                        return -1;
                    } else {
                        return 1;
                    }
                }

                return c;
            }
        });
    }

    public void cancelJob() {
        LogUtils.logEvent("Cancel Booking", "Initiate cancel request");

        mProgressContainer.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.VISIBLE);
        mProgressLabel.setText(R.string.cancelling_job);

        final ShopperApplication app = ShopperApplication.getInstance();
        app.clearQueue();

        Long jobId = app.getCurrentShipment().getDeliveryJob().getRemoteId();
        ShopperApplication.getInstance().getJobManager().cancelDriver(jobId, new DataListener() {
            @Override
            public void onCompleted(boolean success, Object data) {
                if (data != null) {
//                    app.setCurrentShipment(null);
                    app.setBatch(null);

                    // Notify driver for order cancellation
                    showDialog(getString(R.string.label_cancellation_batches_title),
                            getString(R.string.label_cancellation_batches_content), "OK");

                    if (success) {
                        LogUtils.logEvent("Cancel delivery", "Cancelled successfully");
                    }
                } else {
                    if (!success) {
                        Toast.makeText(getActivity(), getString(R.string.toast_cannot_cancel_without_internet),
                                Toast.LENGTH_LONG).show();
                    }
                }

                if (getActivity() != null) {
                    mProgressContainer.setVisibility(View.GONE);
                }
            }
        });
    }

    private void showDialog(String title, String content, String button) {
        if (getActivity() != null) {
            MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity()).title(title).content(content)
                    .positiveText(button).callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {
                            super.onPositive(dialog);
                            backToDriverActivity();
                        }
                    });

            MaterialDialog dialog = builder.build();
            dialog.show();
        }
    }

    private void gotoDriverArriveActivity() {
        Intent intent = new Intent(getActivity(), DriverArriveActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void backToDriverActivity() {
        SharedPrefUtils.remove(getActivity().getApplicationContext(), Constant.SELECTED_CLUSTER);

        TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(getActivity());
        Intent intent = new Intent(getActivity(), DriverListActivity.class);
        taskStackBuilder.addNextIntent(intent);

        taskStackBuilder.startActivities();
    }

}
