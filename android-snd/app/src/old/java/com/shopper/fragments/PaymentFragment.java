package com.shopper.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.Batch;
import com.happyfresh.snowflakes.hoverfly.models.ReceiptItem;
import com.happyfresh.snowflakes.hoverfly.models.Shipment;
import com.happyfresh.snowflakes.hoverfly.models.StockLocation;
import com.shopper.activities.PaymentActivity;
import com.shopper.activities.PaymentReceiptActivity;
import com.shopper.adapters.PaymentReceiptAdapter;
import com.shopper.common.Constant;
import com.shopper.common.ShopperApplication;
import com.shopper.components.CircularProgressBar;
import com.shopper.events.AddReceiptEvent;
import com.shopper.events.ReceiptAmountEvent;
import com.shopper.events.ReceiptNumberEvent;
import com.shopper.events.ReceiptTaxEvent;
import com.shopper.events.RefreshFragmentEvent;
import com.shopper.listeners.PaymentReceiptListener;
import com.shopper.utils.LogUtils;
import com.shopper.utils.SharedPrefUtils;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.ButterKnife;
import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit.android.MainThreadExecutor;

/**
 * Created by ifranseda on 12/23/14.
 */
public class PaymentFragment extends BaseRecyclerFragment {
    public static final String SHIPMENT_ID = "com.shopper.PaymentFragment.SHIPMENT_ID";

    public static final String BATCH_ID = "com.shopper.PaymentFragment.BATCH_ID";

    public static final String STOCK_LOCATION_ID = "com.shopper.PaymentFragment.STOCK_LOCATION_ID";

    public static final String RECEIPT_LIST = "com.shopper.PaymentFragment.RECEIPT_LIST";

    private static final int TAKE_RECEIPT_CODE = 10003;

    @BindView(R.id.photo_receipt_button)
    Button nextButton;

    @BindView(R.id.progress_bar)
    CircularProgressBar progressIndicator;

    @BindView(R.id.progress_overlay)
    View progressOverlay;

    private Shipment mShipment;

    private Batch mBatch;

    private long mBatchId;

    private StockLocation mStockLocation;

    private ArrayList<ReceiptItem> mReceiptList = new ArrayList<>();

    private PaymentReceiptAdapter mAdapter;

    private Unbinder unbinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            long shipmentId = getArguments().getLong(SHIPMENT_ID);
            long batchId = getArguments().getLong(BATCH_ID);
            long stockLocationId = getArguments().getLong(STOCK_LOCATION_ID);
            mShipment = Shipment.findById(shipmentId);
            mBatch = Batch.findById(batchId);
            mBatchId = batchId;
            mStockLocation = StockLocation.findById(stockLocationId);
        } else {
            Long shipmentId = savedInstanceState.getLong(SHIPMENT_ID, 0);
            Long batchId = savedInstanceState.getLong(BATCH_ID, 0);
            Long stockLocationId = savedInstanceState.getLong(STOCK_LOCATION_ID, 0);
            mShipment = Shipment.findById(shipmentId);
            mBatch = Batch.findById(batchId);
            mBatchId = batchId;
            mStockLocation = StockLocation.findById(stockLocationId);
            mShipment = Shipment.findById(shipmentId);
            mReceiptList = (ArrayList<ReceiptItem>) savedInstanceState.getSerializable(RECEIPT_LIST);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (mShipment != null) {
            outState.putLong(SHIPMENT_ID, mShipment.getRemoteId());
            outState.putLong(BATCH_ID, mBatch.getRemoteId());
            outState.putLong(STOCK_LOCATION_ID, mStockLocation.getRemoteId());
            outState.putLong(SHIPMENT_ID, mShipment.getRemoteId());
            outState.putSerializable(RECEIPT_LIST, mReceiptList);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.payment_fragment, null);
        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        tryToEnableButton();
        ShopperApplication.getInstance().getBus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        ShopperApplication.getInstance().getBus().unregister(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public boolean isSwipeRefreshLayoutEnabled() {
        return false;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        progressIndicator.setVisibility(View.GONE);
        nextButton.setEnabled(false);

        if (savedInstanceState != null) {
            long shipmentId = savedInstanceState.getLong(SHIPMENT_ID, 0);
            if (shipmentId > 0) {
                mShipment = Shipment.findById(shipmentId);
            }
        }

        boolean required = isRequireCheckNumber();
        if (savedInstanceState == null) {
            mReceiptList.add(new ReceiptItem(required));
        } else {
            tryToEnableButton();
        }

        PaymentReceiptListener listener = new PaymentReceiptListener() {
            @Override
            public void remove(int position) {
                mReceiptList.remove(position);
                mAdapter.notifyDataSetChanged();

                tryToEnableButton();

            }
        };

        mAdapter = new PaymentReceiptAdapter(getActivity(), mShipment, mReceiptList, required, listener);
        mRecyclerView.setAdapter(mAdapter);
    }

    private boolean isRequireCheckNumber() {
        return SharedPrefUtils.getBoolean(getActivity(), Constant.CONFIG_STATUS.ASK_RECEIPT_NUMBER);
    }

    @Subscribe
    public void onFragmentRefreshed(RefreshFragmentEvent e) {
        if (e.isPay()) {
            getActivity().finish();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case TAKE_RECEIPT_CODE: {
                if (resultCode == Activity.RESULT_OK) {
                    getActivity().finish();
                }

                break;
            }

            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    @Subscribe
    public void addReceipt(AddReceiptEvent e) {
        LogUtils.LOG("AddReceiptEvent >> " + e);

        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

        mReceiptList.add(new ReceiptItem(isRequireCheckNumber()));
        mAdapter.notifyDataSetChanged();

        tryToEnableButton();
    }

    @Subscribe
    public void setReceiptNumber(ReceiptNumberEvent e) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

        mAdapter.notifyDataSetChanged();

        tryToEnableButton();
    }

    @Subscribe
    public void setReceiptAmount(ReceiptAmountEvent e) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

        mAdapter.notifyDataSetChanged();

        tryToEnableButton();
    }

    @Subscribe
    public void setReceiptTax(ReceiptTaxEvent e) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

        mAdapter.notifyDataSetChanged();

        tryToEnableButton();
    }

    private void tryToEnableButton() {
        if (nextButton == null) {
            return;
        }

        if (mReceiptList.size() > 0) {
            boolean completed = true;
            for (ReceiptItem receiptItem : mReceiptList) {
                if (!receiptItem.completed()) {
                    completed = false;
                    break;
                } else {
                    if (Double.valueOf(receiptItem.getAmount()) < Double.valueOf(receiptItem.getTax())) {
                        completed = false;
                        break;
                    }
                }
            }

            nextButton.setEnabled(completed);
        } else {
            nextButton.setEnabled(false);
        }
    }

    @OnClick(R.id.photo_receipt_button)
    void goToPaymentReceiptActivity() {
        Intent intent = new Intent(getPaymentActivity(), PaymentReceiptActivity.class);
        intent.putParcelableArrayListExtra(PaymentReceiptActivity.RECEIPT_ITEM_LIST, ReceiptItem.Companion.toListParcel(mReceiptList));
        intent.putExtra(PaymentReceiptActivity.SHIPMENT_ID, mShipment.getRemoteId());
        intent.putExtra(PaymentReceiptActivity.STOCK_LOCATION_ID, mStockLocation.getRemoteId());
        intent.putExtra(PaymentReceiptActivity.BATCH_ID, mBatch.getRemoteId());
        startActivityForResult(intent, TAKE_RECEIPT_CODE);
    }

    public PaymentActivity getPaymentActivity() {
        return (PaymentActivity) getActivity();
    }

    public static abstract class ReceiptTextWatcher implements TextWatcher {
        private final long DELAY = 500;

        public ReceiptItem receipt;

        private Timer timer = new Timer();

        public ReceiptTextWatcher(ReceiptItem receiptItem) {
            super();
            this.receipt = receiptItem;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(final Editable editable) {
            timer.cancel();
            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    new MainThreadExecutor().execute(new Runnable() {
                        @Override
                        public void run() {
                            postTextChanged(editable);
                        }
                    });
                }
            }, DELAY);
        }

        protected void postTextChanged(Editable editable) {
        }
    }

    public static class ReceiptNumberTextWatcher extends ReceiptTextWatcher {
        public ReceiptNumberTextWatcher(ReceiptItem receiptItem) {
            super(receiptItem);
        }

        protected void postTextChanged(Editable editable) {
            String receiptNumber = editable.toString();
            receipt.setNumber(receiptNumber);

            ShopperApplication.getInstance().getBus()
                    .post(new ReceiptNumberEvent(receipt, receiptNumber));
        }
    }

    public static class ReceiptAmountTextWatcher extends ReceiptTextWatcher {
        public ReceiptAmountTextWatcher(ReceiptItem receiptItem) {
            super(receiptItem);
        }

        protected void postTextChanged(Editable editable) {
            String receiptAmount = editable.toString();
            receipt.setAmount(editable.toString());

            ShopperApplication.getInstance().getBus()
                    .post(new ReceiptAmountEvent(receipt, receiptAmount));
        }
    }

    public static class ReceiptTaxTextWatcher extends ReceiptTextWatcher {
        public ReceiptTaxTextWatcher(ReceiptItem receiptItem) {
            super(receiptItem);
        }

        protected void postTextChanged(Editable editable) {
            String receiptTax = editable.toString();
            receipt.setTax(editable.toString());

            ShopperApplication.getInstance().getBus()
                    .post(new ReceiptTaxEvent(receipt, receiptTax));
        }
    }
}