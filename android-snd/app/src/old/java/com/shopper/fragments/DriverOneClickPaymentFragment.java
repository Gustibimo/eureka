package com.shopper.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.payload.OneClickPaymentPayload;
import com.shopper.common.Constant;
import com.shopper.common.ShopperApplication;
import com.shopper.listeners.DataListener;
import com.happyfresh.snowflakes.hoverfly.models.response.ExceptionResponse;
import com.shopper.utils.SharedPrefUtils;
import com.shopper.managers.tracking.TrackingUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by kharda on 10/24/16.
 */

public class DriverOneClickPaymentFragment extends Fragment {

    private String mOrderNumber;
    private boolean mCCPromotionExistAndEligible;
    private int mMaxRetry = 2;

    @BindView(R.id.horizontal_progress_bar)
    ProgressBar mProgress;

    @BindView(R.id.loading_container)
    LinearLayout mLoadingContainer;

    @BindView(R.id.loading_description)
    TextView mLoadingDescription;

    @BindView(R.id.payment_failed_desc_container)
    LinearLayout mPaymentFailedDescContainer;

    @BindView(R.id.payment_failed_button_container)
    LinearLayout mPaymentFailedButtonContainer;

    @BindView(R.id.retry_button)
    Button mRetryButton;

    private Unbinder unbinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getActivity().getIntent() != null) {
            mOrderNumber = getActivity().getIntent().getStringExtra(Constant.ORDER_NUMBER);
            mCCPromotionExistAndEligible = getActivity().getIntent().getBooleanExtra(Constant.IS_CC_PROMOTION_EXIST_AND_ELIGIBLE, false);
        }
    }

    public static DriverOneClickPaymentFragment newInstance() {
        return new DriverOneClickPaymentFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_driver_one_click_payment, null);
        unbinder = ButterKnife.bind(this, view);

        if (mProgress.getIndeterminateDrawable() != null) {
            mProgress.getIndeterminateDrawable()
                    .setColorFilter(ContextCompat.getColor(getActivity(), R.color.happyfresh_green),
                            android.graphics.PorterDuff.Mode.SRC_IN);
        }

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        processOneClickPay();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.retry_button)
    void processOneClickPay() {
        showLoadingPage(false);

        int retryCount = SharedPrefUtils.getInt(getActivity(), Constant.ONE_CLICK_PAY_RETRY_COUNT);
        if (retryCount < mMaxRetry) {
            OneClickPaymentPayload payload = new OneClickPaymentPayload(mOrderNumber);
            ShopperApplication.getInstance().getBatchManager().captureCCPayment(payload, new DataListener() {
                @Override
                public void onCompleted(boolean success, Object data) {
                    if (success) {
                        if (mCCPromotionExistAndEligible) {
                            getActiveBatch(Constant.PAYMENT_COMPLETED);
                        } else {
                            finishActivity(Constant.PAYMENT_COMPLETED);
                        }
                    } else {
                        if (data == null) {
                            showPaymentFailedPage();
                        } else {
                            ExceptionResponse response = (ExceptionResponse) data;
                            if (response != null && Constant.PaymentAlreadyCompleted.equalsIgnoreCase(response.getType())) {
                                finishActivity(Constant.ALREADY_COMPLETED);
                            } else {
                                showPaymentFailedPage();
                                int retryCount = SharedPrefUtils.getInt(getActivity(), Constant.ONE_CLICK_PAY_RETRY_COUNT);
                                if (++retryCount > (mMaxRetry - 1)) {
                                    mRetryButton.setEnabled(false);
                                }
                                SharedPrefUtils.writeInt(getActivity(), Constant.ONE_CLICK_PAY_RETRY_COUNT, retryCount);

                                TrackingUtils.oneClickPaymentFailed(getActivity());
                            }
                        }
                    }
                }
            });
        } else {
            showPaymentFailedPage();
            mRetryButton.setEnabled(false);
        }
    }

    @OnClick(R.id.switch_to_cash_button)
    void switchToCash() {
        showLoadingPage(true);

        OneClickPaymentPayload payload = new OneClickPaymentPayload(mOrderNumber);
        ShopperApplication.getInstance().getBatchManager().switchToCOD(payload, new DataListener() {
            @Override
            public void onCompleted(boolean success, Object data) {
                if (success) {
					TrackingUtils.oneClickPaymentCOD(getActivity());
                    if (mCCPromotionExistAndEligible) {
                        getActiveBatch(Constant.SWITCH_TO_COD);
                    } else {
                        finishActivity(Constant.SWITCH_TO_COD);
                    }
                } else {
                    ExceptionResponse response = (ExceptionResponse) data;
                    if (response != null && Constant.PaymentAlreadyCompleted.equalsIgnoreCase(response.getType())) {
                        finishActivity(Constant.ALREADY_COMPLETED);
                    } else {
                        showPaymentFailedPage();

                        MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity());
                        builder.title(R.string.switch_to_cash_failed_title);
                        builder.content(R.string.switch_to_cash_failed_body);
                        builder.positiveText(R.string.alert_button_ok);
                        builder.show();
                    }
                }
            }
        });
    }

    private void getActiveBatch(final int flag) {
        ShopperApplication.getInstance().getBatchManager().fetchActive(true, new DataListener() {
            @Override
            public void onCompleted(boolean success, Object data) {
                if (success) {
                    finishActivity(flag);
                } else {
                    MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity());
                    builder.title(R.string.get_new_price_failed_title);
                    builder.content(R.string.get_new_price_failed_body);
                    builder.positiveText(R.string.alert_button_try_again);
                    builder.onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            getActiveBatch(flag);
                        }
                    });
                    builder.show();
                }
            }
        });
    }

    private void showLoadingPage(boolean switchToCash) {
        mLoadingContainer.setVisibility(View.VISIBLE);
        if (switchToCash) {
            mLoadingDescription.setText(getActivity().getResources().getString(R.string.processing_switch_to_cash_payment));
        } else {
            mLoadingDescription.setText(getActivity().getResources().getString(R.string.processing_credit_card_payment));
        }
        mPaymentFailedDescContainer.setVisibility(View.GONE);
        mPaymentFailedButtonContainer.setVisibility(View.GONE);
    }

    private void showPaymentFailedPage() {
        mPaymentFailedDescContainer.setVisibility(View.VISIBLE);
        mPaymentFailedButtonContainer.setVisibility(View.VISIBLE);
        mLoadingContainer.setVisibility(View.GONE);
    }

    private void finishActivity(int flag) {
        Intent resultData = new Intent();
        resultData.putExtra(Constant.ONE_CLICK_PAY_RESULT, flag);
        getActivity().setResult(Activity.RESULT_OK, resultData);
        getActivity().finish();
    }
}
