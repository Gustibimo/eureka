package com.shopper.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.happyfresh.fulfillment.R;
import com.shopper.common.DrawingView;
import com.shopper.utils.ActivityUtils;
import com.shopper.utils.BitmapUtils;

import butterknife.ButterKnife;
import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by rsavianto on 12/24/14.
 */
public class DriverSignatureFragment extends Fragment {

    @BindView(R.id.signature_container)
    FrameLayout mContainer;

    private DrawingView mSignatureView;

    private Unbinder unbinder;

    public static DriverSignatureFragment newInstance() {
        DriverSignatureFragment fragment = new DriverSignatureFragment();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_driver_signature, null);
        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mSignatureView = new DrawingView(getActivity());
        mContainer.addView(mSignatureView);

        ActivityUtils.hideActionBar((AppCompatActivity) getActivity());
    }

    @OnClick(R.id.signature_ok)
    void ok() {
        if (mSignatureView.isDrawn()) {
            Bitmap bitmap = BitmapUtils.getBitmapFromView(mSignatureView);
            BitmapUtils.persistSignatureImage(getActivity(), bitmap);

            getActivity().setResult(Activity.RESULT_OK);
        } else {
            getActivity().setResult(Activity.RESULT_CANCELED);
        }

        getActivity().finish();
    }

    @OnClick(R.id.signature_cancel)
    void cancel() {
        getActivity().setResult(Activity.RESULT_CANCELED);
        getActivity().finish();
    }
}