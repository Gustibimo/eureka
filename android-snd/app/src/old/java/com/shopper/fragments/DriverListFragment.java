package com.shopper.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.happyfresh.fulfillment.R;
import com.happyfresh.fulfillment.modules.ChooseStore.ChooseStoreRouter;
import com.happyfresh.fulfillment.modules.Pause.PauseRouter;
import com.happyfresh.fulfillment.modules.Shopper.ShoppingList.ShoppingListRouter;
import com.happyfresh.snowflakes.hoverfly.Sprinkles;
import com.happyfresh.snowflakes.hoverfly.models.Batch;
import com.happyfresh.snowflakes.hoverfly.models.BatchState;
import com.happyfresh.snowflakes.hoverfly.models.Shipment;
import com.happyfresh.snowflakes.hoverfly.models.StockLocation;
import com.happyfresh.snowflakes.hoverfly.models.response.ExceptionResponse;
import com.happyfresh.snowflakes.hoverfly.models.response.StatusResponse;
import com.happyfresh.snowflakes.hoverfly.stores.StockLocationStore;
import com.happyfresh.snowflakes.hoverfly.stores.UserStore;
import com.shopper.activities.CashHandoverActivity;
import com.shopper.activities.DriverAcceptanceActivity;
import com.shopper.activities.DriverArriveActivity;
import com.shopper.activities.DriverBatchDeliveryActivity;
import com.shopper.activities.DriverFinishActivity;
import com.shopper.activities.DriverListActivity;
import com.shopper.activities.DriverPickUpActivity;
import com.shopper.adapters.DriverListAdapter;
import com.shopper.common.Constant;
import com.shopper.common.ShopperApplication;
import com.shopper.listeners.DataListener;
import com.shopper.utils.ActivityUtils;
import com.shopper.utils.AppUtils;
import com.shopper.utils.LogUtils;
import com.shopper.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by ifranseda on 7/8/15.
 */
public class DriverListFragment extends BaseRecyclerFragment {

    protected TextView mBaseStore;
    boolean isBooking;
    boolean isRefreshing;

    private StockLocationStore mStockLocationStore = Sprinkles.stores(StockLocationStore.class);

    List<Batch> mBatchList = new ArrayList<>();
    DriverListAdapter mAdapter;

    private DriverListAdapter.OnStartListener bookListener = new DriverListAdapter.OnStartListener() {
        @Override
        public void onClick(int position) {
            notifyJobCannotBeCanceled(position);
        }
    };

    public static DriverListFragment newInstance() {
        return new DriverListFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.driver_recycler_list, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mBaseStore = (TextView) view.findViewById(R.id.base_store);
    }

    void notifyJobCannotBeCanceled(final int position) {
        MaterialDialog.Builder inputDialog = new MaterialDialog.Builder(getActivity());
        inputDialog.title(R.string.alert_title_warning);

        int driverMessageResId = R.string.alert_message_job_cannot_be_canceled;
        if (AppUtils.currentUserType() == AppUtils.UserType.RANGER) {
            driverMessageResId = R.string.alert_message_ranger_cannot_cancel_started_job;
        }

        inputDialog.content(driverMessageResId);
        inputDialog.cancelable(false);
        inputDialog.positiveText(R.string.alert_button_continue);
        inputDialog.negativeText(R.string.alert_button_cancel);
        inputDialog.onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                initiateBooking(position);
            }
        });

        inputDialog.show();
    }

    void initiateBooking(int position) {
        try {
            Batch selected = mBatchList.get(position);
            mBatchList.clear();
            mBatchList.add(selected);

            mAdapter.setLoading(true);
            mAdapter.notifyDataSetChanged();

            if (!isBooking) {
                start(selected);
            }
        } catch (IndexOutOfBoundsException e) {
            mBatchList.clear();
            mAdapter.notifyDataSetChanged();

            updateBatches();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mAdapter = new DriverListAdapter(getActivity(), mBatchList);
        mAdapter.setOnStartClickListener(bookListener);

        mAdapter.setLoading(false);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();

        isRefreshing = false;

        updateBatches();
    }

    @Override
    public boolean isSwipeRefreshLayoutEnabled() {
        return true;
    }

    @Override
    public void onSwipeRefresh() {
        super.onSwipeRefresh();
        updateBatches();
    }

    public void updateBatches() {
        if (isBooking || isRefreshing) {
            return;
        }

        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(true);
            mNotFoundContainer.setVisibility(View.GONE);
        }

        if (mAdapter != null) {
            mBatchList.clear();
            mAdapter.setLoading(false);
            mAdapter.notifyDataSetChanged();
        }

        isRefreshing = true;
        ShopperApplication.getInstance().getBatchManager().fetchActive(true, new DataListener<Batch>() {
            @Override
            public void onCompleted(boolean success, Batch data) {
                isRefreshing = false;

                if (data != null) {
                    if (StockLocation.StoreType.SPECIALTY.equals(data.getSlot().getStockLocation().getStoreType())) {
                        AppUtils.changeUserType(AppUtils.UserType.RANGER);
                    } else {
                        AppUtils.changeUserType(AppUtils.UserType.DRIVER);
                    }
                }

                if (getActivity() != null) {
                    if (success && data != null) {
                        if (mSwipeRefreshLayout != null) {
                            mSwipeRefreshLayout.setRefreshing(false);
                        }

                        if (BatchState.State.AVAILABLE.equals(data.state())) {
                            fetchAvailableBatch();
                        } else {
                            dispatch(data);
                        }
                    }
                }
            }

            @Override
            public void onFailed(Throwable exception) {
                isRefreshing = false;
                fetchAvailableBatch();

                if (getActivity() != null) {
                    ((DriverListActivity) getActivity()).checkUpdateIfAvailable();
                }
            }
        });
    }

    public void fetchAvailableBatch() {
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(true);
            mNotFoundContainer.setVisibility(View.GONE);
        }

        ShopperApplication.getInstance().getBatchManager().fetchAvailable(new DataListener<List<Batch>>() {
            @Override
            public void onCompleted(boolean success, List<Batch> data) {
                isRefreshing = false;

                if (getActivity() != null) {
                    if (mSwipeRefreshLayout != null) {
                        mSwipeRefreshLayout.setRefreshing(false);
                    }

                    if (success) {
                        if (data != null) {
                            if (data.size() > 0) {
                                showListData(data);
                                return;
                            }
                        }
                    }

                    returnToBaseStore();
                }
            }

            @Override
            public void onFailed(Throwable exception) {
                isRefreshing = false;

                mSwipeRefreshLayout.setRefreshing(false);
                returnToBaseStore();
            }
        });
    }

    private void dispatch(Batch batch) {
        if (batch == null) {
            showListData(null);
        } else {
            switch (batch.state()) {
                default:
                case AVAILABLE:
                    updateBatches();
                    break;

                case BOOKED:
                    pickUpScreen();
                    break;

                case STARTED:
                    shoppingListScreen(batch);
                    break;

                case FINALIZING:
                    finalizeUnavailableScreen(batch);
                    break;

                case PAYMENT:
                    List<Shipment> shipments = batch.unFinishShipments();
                    if (shipments.size() > 0) {
                        shoppingListScreen(batch);
                    } else {
                        batchPaymentScreen(batch);
                    }
                    break;

                case PICKED_UP:
                case ACCEPTING:
                    acceptanceScreen();
                    break;

                case ACCEPTED:
                    deliveryBatchScreen();
                    break;

                case DELIVERING:
                    Shipment shipment = batch.activeShipment();
                    if (shipment != null) {
                        if (shipment.getDeliveryJob().isDelivering()) {
                            arriveScreen();
                        } else if (shipment.getDeliveryJob().isFoundAddress()) {
                            if (shipment.getOrder().isPaymentCompleted()) {
                                finishScreen();
                            } else {
                                completionScreen();
                            }
                        } else {
                            deliveryBatchScreen();
                        }
                    } else {
                        deliveryBatchScreen();
                    }
                    break;

                case COMPLETED:
                    deliveryBatchScreen();
                    break;
            }
        }
    }

    private void showListData(List<Batch> shipments) {
        if (!isBooking && shipments != null) {
            Collections.sort(shipments, new Comparator<Batch>() {
                @Override
                public int compare(Batch s1, Batch s2) {
                    return s1.getSlot().getStartTime().compareTo(s2.getSlot().getStartTime());
                }
            });

            mNotFoundContainer.setVisibility(View.GONE);

            mBatchList.clear();
            mAdapter.notifyDataSetChanged();

            mBatchList.addAll(shipments);
            mAdapter.setLoading(false);
            mAdapter.notifyDataSetChanged();
        }

        if (shipments == null || mBatchList.size() == 0) {
            mBatchList.clear();
            mAdapter.notifyDataSetChanged();

            returnToBaseStore();
        }
    }

    private void returnToBaseStore() {
        if (getActivity() != null) {
            mNotFoundContainer.setVisibility(View.VISIBLE);
            mBaseStore.setText(getStoreName());
        }
    }

    private void start(Batch batch) {
        if (batch.isRangerHasToShop()) {
            rangerStartShopping(batch);
        } else {
            bookDelivery(batch);
        }
    }

    private void bookDelivery(final Batch batch) {
        isBooking = true;

        LogUtils.logEvent("Booking", "Batch ID: " + batch.getRemoteId());
        final ShopperApplication application = ShopperApplication.getInstance();
        application.getBatchManager().book(batch.getRemoteId(), new DataListener<Batch>() {
            @Override
            public void onCompleted(boolean success, Batch data) {
                isBooking = false;
                if (success) {
                    dispatch(data);
                    LogUtils.logEvent("Booking", "Successful");
                } else {
                    LogUtils.logEvent("Booking", "FAILED: " + data);
                    if (getActivity() != null) {
                        Toast.makeText(getActivity(), "Booking Failed", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailed(Throwable exception) {
                isBooking = false;
                updateBatches();

                if (exception != null) {
                    if (Constant.AlreadyStartedException.equalsIgnoreCase(exception.getMessage())) {
                        Batch saved = application.getBatchManager().saveBatchToDatabase(batch);
                        if (saved != null) {
                            Batch.clear();
                            saved.getState().setName(BatchState.Companion.getBooked());
                            application.setBatch(saved);

                            pickUpScreen();
                        }
                    } else if (Constant.OrderCanceledException.equalsIgnoreCase(exception.getMessage())) {
                        // Notify driver for order cancellation
                        showDialog(getString(R.string.label_cancellation_batches_title),
                                getString(R.string.label_cancellation_batches_content), getString(R.string.alert_button_ok));
                    }
                }
            }
        });
    }

    private void rangerStartShopping(final Batch batch) {
        isBooking = true;

        LogUtils.logEvent("Booking", "Batch ID: " + batch.getRemoteId());
        final ShopperApplication application = ShopperApplication.getInstance();
        application.getBatchManager().start(batch.getRemoteId(), new DataListener<Batch>() {
            @Override
            public void onCompleted(boolean success, Batch data) {
                isBooking = false;
                if (success) {
                    AppUtils.changeUserType(AppUtils.UserType.RANGER);
                    dispatch(data);
                    LogUtils.logEvent("Booking", "Successful");
                } else {
                    LogUtils.logEvent("Booking", "FAILED: " + data);
                    if (getActivity() != null) {
                        Toast.makeText(getActivity(), "Booking Failed", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailed(Throwable exception) {
                isBooking = false;
                updateBatches();

                if (exception != null) {
                    ExceptionResponse response = null;
                    if (exception.getClass().equals(ExceptionResponse.class)) {
                        response = (ExceptionResponse) exception;
                    }

                    if (Constant.AlreadyStartedException.equalsIgnoreCase(response.getType())) {
                        Batch saved = application.getBatchManager().saveBatchToDatabase(batch);
                        if (saved != null) {
                            Batch.clear();
                            saved.getState().setName(BatchState.Companion.getBooked());
                            application.setBatch(saved);

                            dispatch(batch);
                        }
                    } else if (Constant.InvalidStateError.equalsIgnoreCase(response.getType())) {
                        if (getActivity() != null) {
                            String message = getString(R.string.shopping_already_taken);
                            if ("There are orders still processing".equalsIgnoreCase(response.getMessage())) {
                                message = response.getMessage();
                            }
                            Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                        }
                    } else if (Constant.OrderCanceledException.equalsIgnoreCase(response.getType())) {
                        // Notify driver for order cancellation
                        showDialog(getString(R.string.label_cancellation_batches_title),
                                getString(R.string.label_cancellation_batches_content), getString(R.string.alert_button_ok));
                    }
                }
            }
        });
    }

    private void showDialog(String title, String content, String button) {
        if (getActivity() != null) {
            MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity())
                    .title(title)
                    .content(content)
                    .positiveText(button)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            if (!isBooking || !isRefreshing) {
                                updateBatches();
                            }
                        }
                    });

            MaterialDialog dialog = builder.build();
            dialog.show();
        }
    }

    private String getStoreName() {
        if (mStockLocationStore != null && mStockLocationStore.getCurrentStoreName() != null) {
            return mStockLocationStore.getCurrentStoreName();
        } else {
            return getString(R.string.base_store);
        }
    }

    private void clockOut() {
        ShopperApplication.getInstance().getAppManager().clockOut(new DataListener() {
            @Override
            public void onCompleted(boolean success, Object data) {
                if (success) {
                    // set status to finish
                    ShopperApplication.getInstance().setStatus(Constant.PRESENCE.FINISHED);
                    SharedPrefUtils.remove(getActivity(), Constant.IS_STOCK_LOCATION_SELECTED);
                    LogUtils.LOG("Driver Success Clock Out!");
                    ChooseStoreRouter chooseStoreRouter = new ChooseStoreRouter(getActivity());
                    chooseStoreRouter.startClearTop();
                }
            }
        });
    }

    private void pause() {
        ShopperApplication.getInstance().getAppManager().pause(new DataListener() {
            @Override
            public void onCompleted(boolean success, Object data) {
                onDriverPause();
            }
        });
    }

    public void showClockPause(final int type) {
        if (getActivity() != null) {
            MaterialDialog.Builder inputDialog = new MaterialDialog.Builder(getActivity());
            if (type == Constant.MENU_CLOCK_OUT) {
                inputDialog.title(R.string.clock_out);
                inputDialog.content(getString(R.string.ask_to_clock_out) + " " + getStoreName() + "?");
                inputDialog.negativeText(R.string.alert_button_no);
                inputDialog.positiveText(R.string.alert_button_yes);
            } else if (type == Constant.MENU_PAUSE) {
                inputDialog.content(getString(R.string.ask_to_pause));
                inputDialog.negativeText(R.string.action_cancel);
                inputDialog.positiveText(R.string.alert_button_ok);
            }

            inputDialog.onPositive(new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    if (type == Constant.MENU_CLOCK_OUT) {
                        clockOut();
                    } else if (type == Constant.MENU_PAUSE) {
                        pause();
                    }
                }
            });
            inputDialog.show();
        }
    }

    private void pickUpScreen() {
        ActivityUtils.startClearTop(getActivity(), DriverPickUpActivity.class);
    }

    private void shoppingListScreen(Batch batch) {
        ShoppingListRouter router = new ShoppingListRouter(getActivity());
        router.startWithBatch(batch);
    }

    private void finalizeUnavailableScreen(Batch batch) {
        ShoppingListRouter router = new ShoppingListRouter(getActivity());
        router.finalizeBatch(batch);
    }

    private void batchPaymentScreen(Batch batch) {
        ShoppingListRouter router = new ShoppingListRouter(getActivity());
        router.openPaymentScreen(batch.getRemoteId(), batch.stockLocation().getRemoteId());
    }

    private void acceptanceScreen() {
        ActivityUtils.startClearTop(getActivity(), DriverAcceptanceActivity.class);
    }

    private void deliveryBatchScreen() {
        ActivityUtils.startClearTop(getActivity(), DriverBatchDeliveryActivity.class);
    }

    private void arriveScreen() {
        ActivityUtils.startClearTop(getActivity(), DriverArriveActivity.class);
    }

    private void completionScreen() {
        ActivityUtils.startClearTop(getActivity(), DriverFinishActivity.class);
    }

    private void finishScreen() {
        Bundle extras = new Bundle();
        extras.putBoolean(DriverFinishFragment.IS_FINALIZE, true);
        ActivityUtils.startClearTop(getActivity(), DriverFinishActivity.class, extras);
    }

    public void openHandover() {
        ActivityUtils.start(getActivity(), CashHandoverActivity.class, DriverListActivity.class);
    }

    public void checkPauseStatus() {
        final ShopperApplication shopperApplication = ShopperApplication.getInstance();
        shopperApplication.getAppManager().getStatus(new DataListener() {
            @Override
            public void onCompleted(boolean success, Object data) {
                StatusResponse statusResponse = (StatusResponse) data;
                if (statusResponse != null && StatusResponse.WorkingStatus.PAUSED
                        .equals(statusResponse.getWorkingStatus())) {
                    onDriverPause();
                }
            }
        });
    }

    private void onDriverPause() {
        UserStore userStore = Sprinkles.stores(UserStore.class);
        userStore.setOnPaused(true);

        PauseRouter pauseRouter = new PauseRouter(getActivity());
        pauseRouter.startPause();
        getActivity().finish();
    }
}
