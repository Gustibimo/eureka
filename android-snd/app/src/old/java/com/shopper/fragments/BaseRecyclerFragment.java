package com.shopper.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.happyfresh.fulfillment.R;
import com.shopper.utils.LogUtils;

/**
 * Created by ifranseda on 11/19/14.
 */
public abstract class BaseRecyclerFragment extends Fragment {

    protected SwipeRefreshLayout mSwipeRefreshLayout;

    protected RecyclerView mRecyclerView;

    protected View mNotFoundContainer;

    protected TextView mNotFound;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.recycler_list, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_list);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(null);

        mNotFoundContainer = view.findViewById(R.id.not_found_container);
        mNotFound = (TextView) view.findViewById(R.id.not_found);

        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setColorSchemeResources(R.color.happyfresh_accent, R.color.shopping_order_2,
                    R.color.shopping_order_1, R.color.shopping_order_5);

            mSwipeRefreshLayout.setRefreshing(false);
            mSwipeRefreshLayout.setProgressViewOffset(false, 0,
                    getResources().getDimensionPixelSize(R.dimen.refresh_indicator_end));

            updateSwipeRefresh();
        }
    }

    public abstract boolean isSwipeRefreshLayoutEnabled();

    public void showNoContent(String content) {
        mNotFoundContainer.setVisibility(View.VISIBLE);
        mNotFound.setText(content);
        mNotFound.setVisibility(View.VISIBLE);
    }

    public void hideNoContent() {
        mNotFoundContainer.setVisibility(View.GONE);
        mNotFound.setText(R.string.empty_string);
        mNotFound.setVisibility(View.GONE);
    }

    public void onSwipeRefresh() {
        LogUtils.LOG("ON REFRESH");
    }

    public void updateSwipeRefresh() {
        if (mSwipeRefreshLayout != null) {
            if (isSwipeRefreshLayoutEnabled()) {
                mSwipeRefreshLayout.setEnabled(true);
                mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        onSwipeRefresh();
                    }
                });
            } else {
                mSwipeRefreshLayout.setEnabled(false);
            }
        }
    }
}