package com.shopper.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.SpreeImage;
import com.viewpagerindicator.CirclePageIndicator;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.shopper.activities.FullImageActivity.PRODUCT_IMAGES;

/**
 * Created by rsavianto on 12/6/14.
 */
public class FullImageFragment extends Fragment {
    private static final String TAG = FullImageFragment.class.getSimpleName();

    private Unbinder unbinder;

    @BindView(R.id.pager)
    ViewPager mViewPager;

    @BindView(R.id.indicator)
    CirclePageIndicator mViewPagerIndicator;

    private List<SpreeImage> mProductImages = new ArrayList<SpreeImage>();
    private ProductPagerAdapter mProductPagerAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_image_pager, container, false);
        unbinder = ButterKnife.bind(this, rootView);

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState == null) {
            savedInstanceState = getArguments();
        }

        mProductPagerAdapter = new ProductPagerAdapter(getActivity().getFragmentManager());
        mViewPager.setAdapter(mProductPagerAdapter);
        mViewPagerIndicator.setViewPager(mViewPager);

        mProductImages = Parcels.unwrap(savedInstanceState.getParcelable(PRODUCT_IMAGES));
        mViewPager.setCurrentItem(0);

        mProductPagerAdapter.notifyDataSetChanged();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelable(PRODUCT_IMAGES, Parcels.wrap(mProductImages));
    }

    private class ProductPagerAdapter extends FragmentStatePagerAdapter {
        public ProductPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Bundle args = new Bundle();
            args.putString("IMAGE_URL", mProductImages.get(position).getProductUrl());

            ImageFragment imageFragment = new ImageFragment();
            imageFragment.setArguments(args);

            return imageFragment;
        }

        @Override
        public int getCount() {
            return mProductImages.size();
        }
    }

}
