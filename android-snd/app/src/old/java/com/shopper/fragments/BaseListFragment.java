package com.shopper.fragments;

import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.happyfresh.fulfillment.R;
import com.shopper.components.CircularProgressBar;

/**
 * Created by ifranseda on 11/19/14.
 */
public abstract class BaseListFragment extends ListFragment {
    protected CircularProgressBar mProgressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mProgressBar = (CircularProgressBar) view.findViewById(R.id.fragment_list_progressbar);
    }
}
