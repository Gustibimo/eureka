package com.shopper.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.Sprinkles;
import com.happyfresh.snowflakes.hoverfly.models.Batch;
import com.happyfresh.snowflakes.hoverfly.models.Shipment;
import com.happyfresh.snowflakes.hoverfly.models.StockLocation;
import com.happyfresh.snowflakes.hoverfly.services.BatchService;
import com.happyfresh.snowflakes.hoverfly.shared.subscribers.ServiceSubscriber;
import com.happyfresh.snowflakes.hoverfly.utils.ChatUtils;
import com.sendbird.android.SendBird;
import com.shopper.activities.PaymentActivity;
import com.shopper.adapters.ShopperBatchPaymentAdapter;
import com.shopper.common.ShopperApplication;
import com.shopper.components.CircularProgressBar;
import com.shopper.views.NonSwipeViewPager;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.client.Response;

import static com.shopper.common.Constant.BATCH_ID;
import static com.shopper.common.Constant.STOCK_LOCATION_ID;

/**
 * Created by ifranseda on 9/15/15.
 */
public class BatchPaymentFragment extends Fragment implements ViewPager.OnPageChangeListener {
    public static final String SHIPMENTS = "com.shopper.BatchPaymentFragment.SHIPMENTS";
    public static final String CURRENT_POSITION = "com.shopper.BatchPaymentFragment.CURRENT_POSITION";

    @BindView(R.id.item_order_number)
    TextView itemOrderNumber;

    @BindView(R.id.item_customer_name)
    TextView itemCustomerName;

    @BindView(R.id.item_order_checked)
    ImageView itemOrderChecked;

    @BindView(R.id.fragment_viewpager)
    NonSwipeViewPager viewPager;

    @BindView(R.id.prev_button)
    Button prevButton;

    @BindView(R.id.payment_button)
    Button paymentButton;

    @BindView(R.id.next_button)
    Button nextButton;

    @BindView(R.id.progress_container)
    RelativeLayout progressContainer;

    @BindView(R.id.progress_indicator)
    CircularProgressBar progressIndicator;

    @BindView(R.id.progress_label)
    TextView progressLabel;

    @BindView(R.id.corporate_flag)
    View corporateFlag;

    private Batch mBatch;
    private StockLocation mStockLocation;
    private int mCurrentPosition = 0;

    private ShopperBatchPaymentAdapter mAdapter;
    private List<Shipment> mShipments = new ArrayList<>();

    private SensorManager sensorMgr;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        long batchId = 0;
        if (savedInstanceState != null) {
            mShipments = (List<Shipment>) savedInstanceState.getSerializable(SHIPMENTS);

            batchId = savedInstanceState.getLong(BATCH_ID);
            mBatch = Batch.findById(batchId);

            long stockLocationId = savedInstanceState.getLong(STOCK_LOCATION_ID);
            mStockLocation = StockLocation.findById(stockLocationId);

            mCurrentPosition = savedInstanceState.getInt(CURRENT_POSITION);
        } else {
            if (getArguments() != null) {
                batchId = getArguments().getLong(BATCH_ID);
                mBatch = Batch.findById(batchId);

                long stockLocationId = getArguments().getLong(STOCK_LOCATION_ID);
                mStockLocation = StockLocation.findById(stockLocationId);
            }
        }

        if (batchId > 0) {
            flagOpenPayment(batchId);
            ChatUtils.finishChatRooms(batchId);
        }
    }

    @Override
    public void onDestroy() {
        SendBird.disconnect(() -> { });
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(SHIPMENTS, (Serializable) mShipments);
        outState.putInt(CURRENT_POSITION, mCurrentPosition);

        if (mBatch != null) {
            outState.putLong(BATCH_ID, mBatch.getRemoteId());
        }

        if (mStockLocation != null) {
            outState.putLong(STOCK_LOCATION_ID, mStockLocation.getRemoteId());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shopping_batch, null);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        loadData();
    }

    private void setupViewPager() {
        mAdapter = new ShopperBatchPaymentAdapter(getChildFragmentManager(), getActivity(), mShipments);

        viewPager.setAdapter(mAdapter);
        viewPager.addOnPageChangeListener(this);
        viewPager.setSwipeable(true);
    }

    private void loadData() {
        if (mBatch == null) {
            mBatch = ShopperApplication.getInstance().getBatch();
        }

        if (mBatch != null) {
            mShipments.clear();
            mShipments.addAll(mBatch.shipments(mStockLocation.getRemoteId()));

            if (mCurrentPosition >= mShipments.size()) {
                mCurrentPosition = 0;
            }
        }

        setupViewPager();

        viewPager.setCurrentItem(mCurrentPosition);

        update(mCurrentPosition);
        updateNavigationButton();
    }

    private void updateNavigationButton() {
        prevButton.setEnabled(true);
        nextButton.setEnabled(true);

        if (mCurrentPosition == 0) {
            prevButton.setEnabled(false);
            nextButton.setEnabled(true);
        } else if (mCurrentPosition == mShipments.size() - 1) {
            prevButton.setEnabled(true);
            nextButton.setEnabled(false);
        }

        if (mShipments.size() == 1) {
            prevButton.setEnabled(false);
            nextButton.setEnabled(false);
        }

        Shipment shipment = mShipments.get(mCurrentPosition);
        shipment.load();
        if (shipment.getShoppingJob() != null && shipment.getShoppingJob().isFinished()) {
            paymentButton.setEnabled(false);
            paymentButton.setText(getString(R.string.completed));
        } else {
            paymentButton.setEnabled(true);
            paymentButton.setText(getString(R.string.payment));
        }
    }

    public void update(int position) {
        if (mShipments.size() <= position) {
            return;
        }

        Shipment shipment = mShipments.get(position);
        itemOrderNumber.setText(shipment.getOrder().getNumber());
        itemOrderNumber.setTextColor(shipment.getColorResource());
        itemCustomerName.setText(shipment.getCustomerName());

        if (shipment.getShoppingJob() != null && shipment.getShoppingJob().isFinished()) {
            itemOrderChecked.setVisibility(View.VISIBLE);
        } else {
            itemOrderChecked.setVisibility(View.GONE);
        }

        if (shipment.getOrder() != null && shipment.getOrder().getCompanyId() != 0) {
            corporateFlag.setVisibility(View.VISIBLE);
        }
        else {
            corporateFlag.setVisibility(View.GONE);
        }
    }

    private void flagOpenPayment(long batchId) {
        BatchService batchService = Sprinkles.service(BatchService.class);
        batchService.flagOpenPayment(batchId).subscribe(new ServiceSubscriber<Response>() {
            @Override
            public void onFailure(@NotNull Throwable e) {
            }

            @Override
            public void onSuccess(Response data) {
            }
        });
    }

    @OnClick(R.id.prev_button)
    void prevButton() {
        --mCurrentPosition;
        if (mCurrentPosition < 1)
            mCurrentPosition = 0;

        viewPager.setCurrentItem(mCurrentPosition);
    }

    @OnClick(R.id.next_button)
    void nextButton() {
        ++mCurrentPosition;
        if (mCurrentPosition >= mShipments.size() - 1)
            mCurrentPosition = mShipments.size() - 1;

        viewPager.setCurrentItem(mCurrentPosition);
    }

    @OnClick(R.id.payment_button)
    void pay() {
        Shipment shipment = mShipments.get(mCurrentPosition);
        Intent intent = new Intent(getActivity(), PaymentActivity.class);
        intent.putExtra(PaymentFragment.SHIPMENT_ID, shipment.getRemoteId());
        intent.putExtra(PaymentFragment.BATCH_ID, mBatch.getRemoteId());
        intent.putExtra(PaymentFragment.STOCK_LOCATION_ID, mStockLocation.getRemoteId());
        startActivity(intent);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        mCurrentPosition = position;

        update(position);
        updateNavigationButton();
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    public void reload() {
        if (getActivity() != null) {
            loadData();

            setupViewPager();

            mCurrentPosition = 0;
            viewPager.setCurrentItem(mCurrentPosition);

            update(mCurrentPosition);
            updateNavigationButton();
        }
    }
}