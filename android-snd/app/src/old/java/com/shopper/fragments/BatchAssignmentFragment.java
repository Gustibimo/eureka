package com.shopper.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.Batch;
import com.happyfresh.snowflakes.hoverfly.models.BatchState;
import com.happyfresh.snowflakes.hoverfly.models.Slot;
import com.shopper.activities.DriverListActivity;
import com.shopper.activities.DriverPickUpActivity;
import com.shopper.adapters.BatchAssignmentAdapter;
import com.shopper.common.Constant;
import com.shopper.common.ShopperApplication;
import com.shopper.listeners.DataListener;
import com.shopper.utils.ActivityUtils;
import com.shopper.utils.LogUtils;
import com.shopper.utils.StringUtils;

import butterknife.ButterKnife;
import butterknife.BindView;

/**
 * Created by ifranseda on 11/13/15.
 */
public class BatchAssignmentFragment extends BaseRecyclerFragment {
    @BindView(R.id.progress_container)
    View progressContainer;

    @BindView(R.id.batch_number)
    TextView batchNumber;

    @BindView(R.id.delivery_time)
    TextView deliveryTime;

    @BindView(R.id.store_location_name)
    TextView storeLocationName;

    @BindView(R.id.button_no_take_job)
    Button skipButton;

    @BindView(R.id.button_yes_take_job)
    Button acceptButton;

    private Long mBatchId;
    private BatchAssignmentAdapter mAdapter;

    public static BatchAssignmentFragment newInstance(Long batchId) {
        BatchAssignmentFragment fragment = new BatchAssignmentFragment();

        Bundle bundle = new Bundle();
        bundle.putLong(Constant.EXTRAS_BATCH_ID, batchId);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_batch_assignment, null);
        ButterKnife.bind(this, view);

        progressContainer.setVisibility(View.VISIBLE);

        mBatchId = getArguments().getLong(Constant.EXTRAS_BATCH_ID);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        ShopperApplication.getInstance().getBatchManager().updateBatch(mBatchId, new DataListener<Batch>() {
            @Override
            public void onCompleted(boolean success, Batch data) {
                if (success) {
                    updateView(data);
                } else {
                    Toast.makeText(getActivity(), "Cannot load data", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailed(Throwable exception) {
                Toast.makeText(getActivity(), "Failed to load data", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean isSwipeRefreshLayoutEnabled() {
        return false;
    }

    private void updateView(final Batch batch) {
        progressContainer.setVisibility(View.GONE);

        Slot slot = batch.getSlot();

        batchNumber.setText(String.valueOf(batch.getRemoteId()));
        deliveryTime.setText(StringUtils.showDeliveryTime(slot.getStartTime(), slot.getEndTime()));
        storeLocationName.setText(String.valueOf(batch.getSlot().getStockLocation().getName()));

        mAdapter = new BatchAssignmentAdapter(getActivity(), batch.shipments());
        mRecyclerView.setAdapter(mAdapter);

        skipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                skipAssignment(batch.getRemoteId());
            }
        });

        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bookDelivery(batch);
            }
        });
    }

    private void showDialog(String title, String content, String button) {
        if (getActivity() != null) {
            MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity())
                    .title(title)
                    .content(content)
                    .positiveText(button)
                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {

                        }
                    });

            MaterialDialog dialog = builder.build();
            dialog.show();
        }
    }

    public void bookDelivery(final Batch batch) {
        progressContainer.setVisibility(View.VISIBLE);

        LogUtils.logEvent("Booking", "Batch ID: " + batch.getRemoteId());
        final ShopperApplication application = ShopperApplication.getInstance();
        application.getBatchManager().book(batch.getRemoteId(), new DataListener<Batch>() {
            @Override
            public void onCompleted(boolean success, Batch data) {
                progressContainer.setVisibility(View.GONE);

                if (success) {
                    LogUtils.logEvent("Booking", "Successful");
                    pickUpScreen();
                } else {
                    LogUtils.logEvent("Booking", "FAILED: " + data);
                    if (getActivity() != null) {
                        Toast.makeText(getActivity(), "Booking Failed", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailed(Throwable exception) {
                progressContainer.setVisibility(View.GONE);

                if (exception != null) {
                    if (Constant.AlreadyStartedException.equalsIgnoreCase(exception.getMessage())) {
                        Batch saved = application.getBatchManager().saveBatchToDatabase(batch);
                        if (saved != null) {
                            Batch.Companion.clear();
                            saved.getState().setName(BatchState.Companion.getBooked());
                            application.setBatch(saved);

                            pickUpScreen();
                        }
                    } else if (Constant.OrderCanceledException.equalsIgnoreCase(exception.getMessage())) {
                        // Notify driver for order cancellation
                        showDialog(getString(R.string.label_cancellation_batches_title),
                                getString(R.string.label_cancellation_batches_content), getString(R.string.alert_button_ok));
                    }
                }
            }
        });
    }

    public void skipAssignment(Long batchId) {
        progressContainer.setVisibility(View.VISIBLE);
        LogUtils.logEvent("DRIVER ASSIGNMENT", "SKIP ASSIGNMENT " + batchId);

        ShopperApplication.getInstance().getBatchManager().skip(batchId, new DataListener() {
            @Override
            public void onCompleted(boolean success, Object data) {
                progressContainer.setVisibility(View.GONE);

                if (success) {
                    LogUtils.logEvent("DRIVER ASSIGNMENT", "SKIP ASSIGNMENT COMPLETED");
                    driverList();
                } else {
                    
                }
            }

            @Override
            public void onFailed(Throwable exception) {
                progressContainer.setVisibility(View.GONE);

                LogUtils.logEvent("DRIVER ASSIGNMENT", "SKIP ASSIGNMENT FAILED");
                if (getActivity() != null) {
                    Toast.makeText(getActivity(), "Skip Failed", Toast.LENGTH_SHORT).show();
                }

                driverList();
            }
        });
    }

    private void pickUpScreen() {
        ActivityUtils.startClearTop(getActivity(), DriverPickUpActivity.class);
    }

    private void driverList() {
        ActivityUtils.startClearTop(getActivity(), DriverListActivity.class);
    }
}
