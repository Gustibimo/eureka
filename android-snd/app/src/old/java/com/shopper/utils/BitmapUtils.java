package com.shopper.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;

import com.happyfresh.snowflakes.hoverfly.utils.FileUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Random;

/**
 * Created by rsavianto on 12/23/14.
 */
public class BitmapUtils {

    private static final String RECEIPT_DIRECTORY = "receipts";

    private static final String RECEIPT_ORDER = "RECEIPT_%s_%d.jpg";

    private static final String PRICE_TAG_DIRECTORY = "pricetags";

    private static final String PRICE_TAG_ORDER = "PRICETAG_%s_%d.jpg";

    public static Bitmap getBitmapFromView(View view) {
        Bitmap bmp = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bmp);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);

        view.draw(canvas);

        return bmp;
    }

    public static void persistSignatureImage(Context context, Bitmap bitmap) {
        File filesDir = context.getFilesDir();
        File imageFile = new File(filesDir, "signature.jpg");
        if (imageFile.exists()) {
            imageFile.delete();
        }

        bitmap = getResizedBitmap(bitmap, 600);

        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 40, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            Log.e(BitmapUtils.class.getSimpleName(), "Error writing bitmap", e);
        }
    }

    public static File getSignatureImage(Context context) {
        File filesDir = context.getFilesDir();
        return new File(filesDir, "signature.jpg");
    }

    public static Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 0) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public static File persistInvoiceImage(Context context, Bitmap bitmap) {
        File filesDir = context.getFilesDir();
        File imageFile = new File(filesDir, "tmp.jpg");

        bitmap = getResizedBitmap(bitmap, 1500);

        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 60, os);
            os.flush();
            os.close();

            return imageFile;
        } catch (Exception e) {
            Log.e(BitmapUtils.class.getSimpleName(), "Error writing bitmap", e);
            return null;
        }
    }

    public static Bitmap versionWatermark(Bitmap src, String mark) {
        int w = src.getWidth();
        int h = src.getHeight();
        Bitmap result = Bitmap.createBitmap(w, h, src.getConfig());
        Canvas canvas = new Canvas(result);
        canvas.drawBitmap(src, 0, 0, null);

        Paint paint = new Paint();
        paint.setColor(Color.CYAN);
        paint.setTextSize(30);
        paint.setAntiAlias(true);
        canvas.drawText(mark, 36, 170, paint);

        return result;
    }

    public static String getReceiptImagePath(Context context, String orderNumber) {
        return getImagePath(context, orderNumber, RECEIPT_ORDER, RECEIPT_DIRECTORY);
    }

    public static String getPriceTagImagePath(Context context, String orderNumber) {
        return getImagePath(context, orderNumber, PRICE_TAG_ORDER, PRICE_TAG_DIRECTORY);
    }

    private static String getImagePath(Context context, String orderNumber, String formatFile, String targetDir) {
        String imageFilePath = null;

        int nextInt = new Random().nextInt();
        String filename = String.format(formatFile, orderNumber, nextInt);
        File imageDir = FileUtils.getCacheDir(context, targetDir);
        if (!imageDir.exists()) {
            imageDir.mkdirs();
        }

        File imageFile = null;
        try {
            imageFile = new File(imageDir, filename);
            imageFile.createNewFile();
        } catch (IOException e) {
            try {
                imageDir = FileUtils.getFilesDir(context, targetDir);
                if (!imageDir.exists()) {
                    imageDir.mkdirs();
                }

                imageFile = new File(imageDir, filename);
                imageFile.createNewFile();
            } catch (IOException e1) {
            }
        } finally {
            if (imageFile != null) {
                imageFilePath = imageFile.getAbsolutePath();
            }
        }

        return imageFilePath;
    }
}
