package com.shopper.utils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by ifranseda on 4/6/15.
 */
public class Serializer {

    public static void serialize(String fileName, Object object) {
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;

        try {
            fos = new FileOutputStream(fileName);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(object);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (oos != null) oos.close();
                if (fos != null) fos.close();
            } catch (IOException ex) {
            }
        }
    }

    public static Object deserialize(String fileName) {
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        Object operation = null;

        try {
            fis = new FileInputStream(fileName);
            ois = new ObjectInputStream(fis);
            operation = ois.readObject();
            ois.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }

        return operation;
    }
}
