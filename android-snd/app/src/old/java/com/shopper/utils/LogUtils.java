package com.shopper.utils;

import android.text.TextUtils;
import android.util.Log;

/**
 * Created by ifranseda on 11/17/14.
 */
public class LogUtils {
    private static final String LOG_PREFIX = "HappyFresh:";
    private static final int LOG_PREFIX_LENGTH = LOG_PREFIX.length();
    private static final int MAX_LOG_TAG_LENGTH = 23;

    public static String tagLogger(String str) {
        if (str.length() > MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH) {
            // Returns end of class name
            return LOG_PREFIX + str.substring(MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH);
        }

        return LOG_PREFIX + str;
    }

    public static void LOG(String message) {
        String className = new Throwable().getStackTrace()[1].getClassName();
        String tag = tagLogger(className);

        if (message != null) {
            Log.d(tag, message);
        }
    }

    public static void WTF(String message) {
        String className = new Throwable().getStackTrace()[1].getClassName();
        String tag = tagLogger(className);

        if (message != null) {
            Log.wtf(tag, message);
        }
    }


    public static void logEvent(String name) {
        logEvent(name, null);
    }

    public static void logEvent(String name, String message) {
        LogUtils.LOG(name + "\n" + message);
    }
}
