package com.shopper.utils;

import com.happyfresh.snowflakes.hoverfly.models.Batch;
import com.happyfresh.snowflakes.hoverfly.models.LineItem;
import com.happyfresh.snowflakes.hoverfly.models.Order;
import com.happyfresh.snowflakes.hoverfly.models.ProductSampleItem;
import com.happyfresh.snowflakes.hoverfly.models.ShopperReplacement;
import com.happyfresh.snowflakes.hoverfly.models.Shipment;
import com.happyfresh.snowflakes.hoverfly.models.StockLocation;
import com.happyfresh.snowflakes.hoverfly.models.payload.ListProductSamplePayload;
import com.happyfresh.snowflakes.hoverfly.models.payload.ProductSamplePayload;
import com.happyfresh.snowflakes.hoverfly.models.payload.SampleCampaignPayload;
import com.happyfresh.snowflakes.hoverfly.models.payload.SampleItemPayload;
import com.happyfresh.snowflakes.hoverfly.models.payload.SampleOrderPayload;
import com.shopper.common.ShopperApplication;
import com.shopper.listeners.DataListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by numannaufal on 9/30/16.
 */
public class SampleUtils {
    private Batch mBatch;

    private boolean mIsOutOfStore;

    private StockLocation mStockLocation;

    private List<ProductSampleItem> mSampleItems;

    private List<SampleOrderPayload> mSampleOrderPayloads;

    public SampleUtils() {
    }

    public SampleUtils(Batch batch) {
        mBatch = batch;
    }

    public SampleUtils withBatch(Batch batch) {
        mBatch = batch;
        return this;
    }

    public SampleUtils withStockLocation(StockLocation stocklocation) {
        mStockLocation = stocklocation;
        return this;
    }

    public SampleUtils setIsOutOfStore(boolean isOutOfStore) {
        mIsOutOfStore = isOutOfStore;
        return this;
    }

    public SampleUtils retrieveSamples() {
        if (mIsOutOfStore) {
            mSampleItems = ProductSampleItem.findByBatchIdAndFilterByOutStore(mBatch.getRemoteId());
        } else {
            mSampleItems = ProductSampleItem.findByBatchIdAndFilterByInStore(mBatch.getRemoteId());
        }
        return this;
    }

    public void createThenPostPayload(DataListener<Boolean> listener) {
        createPayload();
        postPayload(listener);
    }

    private void createPayload() {
        Set<String> campaignsAndOrders = new HashSet<String>();
        List<SampleItemPayload> sampleItemPayloads = new ArrayList<SampleItemPayload>();
        for (ProductSampleItem sampleItem : mSampleItems) {
            campaignsAndOrders.add(sampleItem.getCampaignId() + "_" + sampleItem.getOrderNumber() + "_" +sampleItem.getRuleId());
            SampleItemPayload sampleItemPayload = new SampleItemPayload(sampleItem.getProductId(), sampleItem.getTotalShopped(),
                    sampleItem.getPrice(), sampleItem.getTotalShopped() == 0, sampleItem.getCampaignId(), sampleItem.getOrderNumber(), sampleItem.getRuleId());
            sampleItemPayloads.add(sampleItemPayload);
        }

        Map<String, SampleCampaignPayload> mapSampleCampaignAndOrderPayloads = new HashMap<String, SampleCampaignPayload>();
        for (String campaignAndOrderNumber : campaignsAndOrders) {
            String[] campaignOrOrderNumber = campaignAndOrderNumber.split("_");
            int campaignId = Integer.parseInt(campaignOrOrderNumber[0]);
            String orderNumber = campaignOrOrderNumber[1];
            int ruleId = Integer.parseInt(campaignOrOrderNumber[2]);
            mapSampleCampaignAndOrderPayloads.put(campaignAndOrderNumber,
                    new SampleCampaignPayload(campaignId, orderNumber, new ArrayList<SampleItemPayload>(), ruleId));

        }

        for (SampleItemPayload sampleItemPayload : sampleItemPayloads) {
            String key = sampleItemPayload.getCampaignId() + "_" + sampleItemPayload.getOrderNumber() + "_" + sampleItemPayload.getRuleId();
            SampleCampaignPayload sampleCampaignPayload = mapSampleCampaignAndOrderPayloads.get(key);
            sampleCampaignPayload.getSampleItemPayloads().add(sampleItemPayload);
        }

        List<SampleOrderPayload> sampleOrderPayloads = new ArrayList<SampleOrderPayload>();

        for (Shipment shipment : mBatch.shipments()) {
            Order order = shipment.getOrder();
            SampleOrderPayload sampleOrderPayload = new SampleOrderPayload(order.getUserId(), order.getNumber(), order.getRemoteId(), mStockLocation.getRemoteId(),
                    new ArrayList<SampleCampaignPayload>());

            Iterator it = mapSampleCampaignAndOrderPayloads.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                String campaignAndOrderNumber = pair.getKey().toString();
                String[] campaignOrOrderNumber = campaignAndOrderNumber.split("_");
                String orderNumber = campaignOrOrderNumber[1];

                SampleCampaignPayload sampleCampaignPayload = (SampleCampaignPayload) pair.getValue();
                if (orderNumber.equals(order.getNumber())) {
                    sampleOrderPayload.getSampleCampaignPayloads().add(sampleCampaignPayload);
                }
            }
            sampleOrderPayloads.add(sampleOrderPayload);
        }

        mSampleOrderPayloads = sampleOrderPayloads;
    }

    private void postPayload(DataListener<Boolean> listener) {
        ShopperApplication.getInstance().getProductSampleManager()
                .finishSampleLineItems(mSampleOrderPayloads, listener);
    }

    public void fetchProductSample(DataListener<Boolean> listener) {
        List<ListProductSamplePayload> listProductSamplePayloads = new ArrayList<>();

        for (Shipment shipment : mBatch.shipments()) {
            Order order = shipment.getOrder();

            List<LineItem> items = shipment.getAllItems();
            List<ProductSamplePayload> productSamplePayloads = new ArrayList<>();
            for (LineItem item : items) {
                ShopperReplacement replacementItem = item.getReplacementItem();
                if (replacementItem != null && replacementItem.getLineItem().getTotalReplaced() > 0) {
                    productSamplePayloads.add(new ProductSamplePayload(replacementItem.getVariant().getSku(),
                            replacementItem.getLineItem().getTotalReplaced()));
                } else if (item.getTotalShopped() > 0) {
                    productSamplePayloads.add(new ProductSamplePayload(item.getVariant().getSku(), item.getTotalShopped()));
                }
            }

            ListProductSamplePayload listProductSamplePayload = new ListProductSamplePayload(order.getUserId(),
                    order.getNumber(), mStockLocation.getRemoteId(), productSamplePayloads);
            listProductSamplePayloads.add(listProductSamplePayload);
        }

        ShopperApplication.getInstance().getProductSampleManager()
                .getProductSample(listProductSamplePayloads, listener);
    }

}
