package com.shopper.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.happyfresh.fulfillment.R;

/**
 * Created by kharda on 28/2/17.
 */

public class DialogUtils {

    public static MaterialDialog getPriceChangeDialog(Context context, String currency, String supermarketUnit,
            final OnPriceChangeListener listener) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View alertView = inflater.inflate(R.layout.dialog_value_change, null);

        View priceChangeField = alertView.findViewById(R.id.item_new_value_input);
        final EditText priceInputField = (EditText) priceChangeField.findViewById(R.id.new_value_input_field);
        setPriceChangeDialogAttribute(priceChangeField, priceInputField, currency, supermarketUnit);

        View priceChangeReenterField = alertView.findViewById(R.id.item_reenter_new_value_input);
        final EditText priceInputReenterField = (EditText) priceChangeReenterField
                .findViewById(R.id.new_value_input_field);
        setPriceChangeDialogAttribute(priceChangeReenterField, priceInputReenterField, currency, supermarketUnit);

        final TextView priceNotMatchTextView = (TextView) alertView.findViewById(R.id.value_change_not_match);

        MaterialDialog.Builder inputDialog = new MaterialDialog.Builder(context);
        inputDialog.customView(alertView, false);
        inputDialog.cancelable(false);
        inputDialog.negativeText(R.string.alert_button_cancel);
        inputDialog.positiveText(R.string.alert_button_ok);
        inputDialog.autoDismiss(false);
        inputDialog.onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                String newPriceString = priceInputField.getText().toString();
                String newPriceReenterString = priceInputReenterField.getText().toString();

                if (!TextUtils.isEmpty(newPriceString) && !TextUtils.isEmpty(newPriceReenterString) && newPriceString
                        .equals(newPriceReenterString)) {
                    if (listener != null) {
                        listener.onPriceChange(newPriceString);
                    }
                    dialog.dismiss();
                }
                else {
                    priceNotMatchTextView.setVisibility(View.VISIBLE);
                }
            }
        });
        inputDialog.onNegative(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                dialog.dismiss();
            }
        });

        inputDialog.showListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                priceInputField.setText(null);
                priceInputReenterField.setText(null);
                priceNotMatchTextView.setVisibility(View.GONE);
            }
        });

        return inputDialog.build();
    }

    private static void setPriceChangeDialogAttribute(View priceChangeField, EditText priceInput, String currency,
            String unit) {
        int attributeSidePadding = 15;
        int unitWidth = 0;

        TextView priceCurrencyTextView = (TextView) priceChangeField.findViewById(R.id.value_currency);
        priceCurrencyTextView.setText(currency);

        if (!TextUtils.isEmpty(unit)) {
            TextView priceUnitTextView = (TextView) priceChangeField.findViewById(R.id.value_unit);
            priceUnitTextView.setText("/" + unit);
            unitWidth = ViewUtils.dpToPx(new Paint().measureText(unit) + attributeSidePadding);
        }

        int currencyWidth = ViewUtils.dpToPx(new Paint().measureText(currency) + attributeSidePadding);

        priceInput.setPadding(currencyWidth, priceInput.getPaddingTop(), unitWidth, priceInput.getPaddingBottom());
    }

    public interface OnPriceChangeListener {

        void onPriceChange(String newPrice);
    }
}
