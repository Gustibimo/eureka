package com.shopper.utils;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import com.happyfresh.snowflakes.hoverfly.Sprinkles;
import com.happyfresh.snowflakes.hoverfly.models.StockLocation;
import com.happyfresh.snowflakes.hoverfly.models.User;
import com.happyfresh.snowflakes.hoverfly.stores.UserStore;
import com.shopper.common.Constant;
import com.shopper.common.ShopperApplication;
import com.shopper.listeners.DataListener;
import com.happyfresh.snowflakes.hoverfly.models.response.VersionUpdate;
import com.shopper.receivers.DownloadReceiver;
import com.shopper.services.DownloadService;

/**
 * Created by ifranseda on 10/20/15.
 */
public class AppUtils {
    public static void checkUpdate(final DataListener listener) {
        ShopperApplication app = ShopperApplication.getInstance();
        int currentVersion = app.getBuildNumber();
        app.getAppManager().checkUpdate(currentVersion, new DataListener<VersionUpdate>() {
            @Override
            public void onCompleted(boolean success, VersionUpdate data) {
                if (success && data != null && data.getRequired()) {
                    LogUtils.LOG("Update required!!");
                    if (listener != null) {
                        listener.onCompleted(true, data);
                    }
                } else {
                    LogUtils.LOG("No update required!!");
                }
            }
        });
    }

    public static void downloadUpdate(VersionUpdate data) {
        Context app = ShopperApplication.getContext();

        Intent intent = new Intent(app.getApplicationContext(), DownloadService.class);
        intent.putExtra("version", data.getVersion());
        intent.putExtra("url", data.getApkUrl());

        intent.putExtra("receiver", new DownloadReceiver(new Handler()));

        app.startService(intent);
    }

    public static void saveUserPreferences(User user) {
        SharedPrefUtils.writeLong(ShopperApplication.getContext(), Constant.USER_ID_KEY, user.getRemoteId());

        SharedPrefUtils.writeString(ShopperApplication.getContext(), Constant.USER_NAME_KEY, user.fullName());
        SharedPrefUtils.writeString(ShopperApplication.getContext(), Constant.USER_EMAIL_KEY, user.getEmail());

        if (user.isShopper()) {
            SharedPrefUtils.writeString(ShopperApplication.getContext(), Constant.USER_TYPE_KEY, UserType.SHOPPER.name());
            SharedPrefUtils.writeString(ShopperApplication.getContext(), Constant.USER_NORMAL_ROLE, Constant.USER_ROLE.SHOPPER);
        } else if (user.isDriver()) {
            SharedPrefUtils.writeString(ShopperApplication.getContext(), Constant.USER_TYPE_KEY, UserType.DRIVER.name());
            SharedPrefUtils.writeString(ShopperApplication.getContext(), Constant.USER_NORMAL_ROLE, Constant.USER_ROLE.DRIVER);
        }
    }

    public static void changeBackUserType() {
        if (currentUserType() == UserType.RANGER) {
            AppUtils.changeUserType(AppUtils.UserType.DRIVER);
        }
    }

    public static void changeUserType(UserType userType) {
        SharedPrefUtils.writeString(ShopperApplication.getContext(), Constant.USER_TYPE_KEY, userType.name());
    }

    public static UserType currentUserType() {
        String name = SharedPrefUtils.getString(ShopperApplication.getContext(), Constant.USER_TYPE_KEY);
        if (name == null) {
            return UserType.UNKNOWN;
        } else {
            UserStore userStore = Sprinkles.stores(UserStore.class);
            if (userStore != null && userStore.isDriver() && StockLocation.StoreType.SPECIALTY.equals(userStore.currentStoreType())) {
                return UserType.RANGER;
            }
            else {
                return UserType.valueOf(name.toUpperCase());
            }
        }
    }

    public enum UserType {
        UNKNOWN,
        SHOPPER,
        DRIVER,
        RANGER
    }
}
