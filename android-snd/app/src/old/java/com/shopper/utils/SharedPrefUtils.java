package com.shopper.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.happyfresh.fulfillment.App;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ifranseda on 12/4/14.
 */
public class SharedPrefUtils {

    private static SharedPreferences sharedPrefs() {
        Context context = App.Companion.appContext();
        String name = context.getPackageName() + ".preferences";
        return context.getSharedPreferences(name, Context.MODE_PRIVATE);
    }

    public static boolean hasKey(Context context, String key) {
        SharedPreferences sharedPref = sharedPrefs();
        String value = sharedPref.getString(key, null);
        return !TextUtils.isEmpty(value);
    }

    public static boolean remove(Context context, String key) {
        if (context == null) {
            return false;
        }

        SharedPreferences sharedPref = sharedPrefs();
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.remove(key);

        return editor.commit();
    }

    public static boolean clearAll(Context context) {
        if (context == null) {
            return false;
        }

        SharedPreferences sharedPref = sharedPrefs();
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.clear();

        return editor.commit();
    }

    public static boolean writeString(Context context, String key, String value) {
        SharedPreferences sharedPref = sharedPrefs();
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        LogUtils.LOG("WRITE: SHARED PREFS >> " + key + " -- " + value);
        return editor.commit();
    }

    public static String getString(Context context, String key) {
        SharedPreferences sharedPref = sharedPrefs();
        String value = sharedPref.getString(key, null);
        LogUtils.LOG("READ: SHARED PREFS >> " + key + " -- " + value);
        return value;
    }

    public static boolean writeInt(Context context, String key, int value) {
        SharedPreferences sharedPref = sharedPrefs();
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(key, value);
        return editor.commit();
    }

    public static int getInt(Context context, String key) {
        SharedPreferences sharedPref = sharedPrefs();
        int value = sharedPref.getInt(key, 0);
        return value;
    }

    public static boolean writeLong(Context context, String key, long value) {
        SharedPreferences sharedPref = sharedPrefs();
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putLong(key, value);
        return editor.commit();
    }

    public static long getLong(Context context, String key) {
        SharedPreferences sharedPref = sharedPrefs();
        long value = sharedPref.getLong(key, 0);
        return value;
    }

    public static boolean writeBoolean(Context context, String key, Boolean value) {
        SharedPreferences sharedPref = sharedPrefs();
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(key, value);
        LogUtils.LOG("WRITE: SHARED PREFS >> " + key + " -- " + value);
        return editor.commit();
    }

    public static Boolean getBoolean(Context context, String key) {
        SharedPreferences sharedPref = sharedPrefs();
        Boolean value = sharedPref.getBoolean(key, false);
        LogUtils.LOG("READ: SHARED PREFS >> " + key + " -- " + value);
        return value;
    }

    public static boolean writeFloat(Context context, String key, Float value) {
        SharedPreferences sharedPref = sharedPrefs();
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putFloat(key, value);
        LogUtils.LOG("WRITE: SHARED PREFS >> " + key + " -- " + value);
        return editor.commit();
    }

    public static Float getFloat(Context context, String key) {
        SharedPreferences sharedPref = sharedPrefs();
        Float value = sharedPref.getFloat(key, 0);
        LogUtils.LOG("READ: SHARED PREFS >> " + key + " -- " + value);
        return value;
    }

    public static <T> boolean writeList(Context context, String key, List<T> value) {
        if (value == null) {
            return false;
        }

        SharedPreferences sharedPref = sharedPrefs();
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, TextUtils.join(",", value));
        return editor.commit();
    }

    public static List<String> getList(Context context, String key) {
        SharedPreferences sharedPref = sharedPrefs();
        String value = sharedPref.getString(key, null);

        if (value != null) {
            String[] values = value.split(",");
            return Arrays.asList(values);
        }
        else {
            return new ArrayList<>();
        }
    }
}
