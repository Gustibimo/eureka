package com.shopper.utils;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.shopper.common.ShopperApplication;

/**
 * Created by ifranseda on 11/12/15.
 */
public class LocationUtils {
    static GoogleApiClient.Builder mLocationBuilder;
    static GoogleApiClient mGoogleApiClient;

    public static void connect(final long timeout, final LocationListener listener) {
        LogUtils.LOG("CONNECTING");

        Context context = ShopperApplication.getContext();
        mLocationBuilder = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult connectionResult) {
                        LogUtils.LOG("CONNECTION FAILED");
                    }
                });

        if (mGoogleApiClient == null) {
            mGoogleApiClient = mLocationBuilder.build();
        }

        mGoogleApiClient.registerConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
            @Override
            public void onConnected(Bundle bundle) {
                LogUtils.LOG("CONNECTED");
                startRequestLocation(timeout, listener);
            }

            @Override
            public void onConnectionSuspended(int i) {
                LogUtils.LOG("SUSPENDED");
            }
        });

        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }
    }

    static void startRequestLocation(long timeout, final LocationListener listener) {
        LogUtils.LOG("START REQUEST LOCATION WITH LISTENER >> " + listener);

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(3600);
        locationRequest.setFastestInterval(60);
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, listener);

        new Handler().postDelayed(new Runnable() {
            public void run() {
                if (mGoogleApiClient.isConnected()) {
                    LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, listener);
                    mGoogleApiClient.disconnect();
                    LogUtils.LOG("DISCONNECTED");
                }
            }
        }, timeout);
    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }
}
