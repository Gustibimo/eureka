package com.shopper.utils;

import android.content.res.Resources;
import android.view.View;

/**
 * Created by anton on 12/11/14.
 */
public class ViewUtils {

    public static void setVisibilityViews(int visibility, View... views) {
        for (View view : views) {
            if (view != null) {
                view.setVisibility(visibility);
            }
        }
    }

    public static int dpToPx(double dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToDp(double px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }
}
