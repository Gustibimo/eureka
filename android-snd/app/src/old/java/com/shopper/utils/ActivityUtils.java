package com.shopper.utils;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.TaskStackBuilder;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

/**
 * Created by ifranseda on 8/12/15.
 */
public class ActivityUtils {
    public static void startClearTop(Activity activity, Class<? extends Activity> target) {
        start(activity, target, Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK, null, null);
    }

    public static void startClearTop(Activity activity, Class<? extends Activity> target, Bundle extras) {
        start(activity, target, Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK, extras, null);
    }

    public static void start(Activity activity, Class<? extends Activity> target, Class<? extends Activity>... activities) {
        start(activity, target, 0, null, null, activities);
    }

    private static void start(Activity activity, Class<? extends Activity> target, int flags,
                              Bundle extras, ActivityOptions options, Class<? extends Activity>... activities) {
        if (activity == null) {
            return;
        }

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(activity);
        Intent intent = new Intent(activity, target);
        if (flags != 0) {
            intent.setFlags(flags);
        }

        if (extras != null) {
            intent.putExtras(extras);
        }

        if (activities != null) {
            for (int i = 0; i < activities.length; i++) {
                stackBuilder.addNextIntent(new Intent(activity, activities[i]));
            }
        }

        stackBuilder.addNextIntent(intent);

        if (options != null) {
            stackBuilder.startActivities(options.toBundle());
        } else {
            stackBuilder.startActivities();
        }
    }

    public static void hideActionBar(AppCompatActivity activity) {
        final View decorView = activity.getWindow().getDecorView();
        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                    decorView.setSystemUiVisibility(
                            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                }
            }
        });

        int uiOptions = activity.getWindow().getDecorView().getSystemUiVisibility();
        int newUiOptions = uiOptions;

        if (Build.VERSION.SDK_INT >= 14) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        }

        if (Build.VERSION.SDK_INT >= 16) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_FULLSCREEN;
        }

        if (Build.VERSION.SDK_INT >= 18) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }

        decorView.setSystemUiVisibility(newUiOptions);

        ActionBar actionBar = activity.getSupportActionBar();
        actionBar.hide();
    }
}