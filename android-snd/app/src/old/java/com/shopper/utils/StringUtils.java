package com.shopper.utils;

import android.content.Context;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.Address;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by rsavianto on 12/24/14.
 */
public class StringUtils {
    private static SimpleDateFormat df = new SimpleDateFormat("dd-MM-yy hh:mma");

    public static String buildAddressCityInfo(Address address) {
        if (address != null) {
            StringBuilder builder = new StringBuilder(address.getCity());
            if (address.getZipcode() != null) {
                builder.append(", ");
                builder.append(address.getZipcode());
            }

            return builder.toString();
        }

        return "";
    }

    public static String showDuration(Context ctx, Long durationInMillis, boolean fullString) {
        Long one_minute = 60 * 1000L;
        Long one_hour = 60 * 60 * 1000L;

        if (durationInMillis < 0) {
            return "-";
        }

        if (durationInMillis < one_minute) {
            return fullString ? "1 minute" : "< 1m";
        } else {
            StringBuilder builder = new StringBuilder();
            while (durationInMillis > one_minute) {
                if (durationInMillis >= one_hour) {
                    Long hh = durationInMillis / one_hour;
                    durationInMillis = durationInMillis % one_hour;
                    builder.append(hh);
                    builder.append(fullString ? " hour " : "h");
                } else if (durationInMillis > one_minute) {
                    Long mm = durationInMillis / one_minute;
                    durationInMillis = durationInMillis % one_minute;
                    builder.append(mm);
                    builder.append(fullString ? " minute" : "m");
                }
            }

            return builder.toString();
        }
    }

    public static String buildTimeDiff(Context ctx, Date startTime, Date endTime) {
        Long startTimeInLocal = startTime.getTime() + TimeZone.getDefault().getOffset(System.currentTimeMillis());
        Long endTimeInLocal = endTime.getTime() + TimeZone.getDefault().getOffset(System.currentTimeMillis());
        Long timeDiff = endTimeInLocal - startTimeInLocal;

        if (timeDiff < 0) {
            return ctx.getString(R.string.you_missed_it);
        }

        return showDuration(ctx, timeDiff, false);
    }

    public static String buildTimeDiffWithNow(Context ctx, Date time) {
        return buildTimeDiff(ctx, new Date(System.currentTimeMillis()), time);
    }

    public static String showDeliveryTime(Date startDate, Date endDate) {
        SimpleDateFormat df1 = new SimpleDateFormat("dd-MM-yy hha");
        SimpleDateFormat df2 = new SimpleDateFormat("hha");

        return String.format("%s - %s", df1.format(startDate), df2.format(endDate));
    }

    public static String showDeliveryTime(String start, String end) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss z");

        try {
            Date startDate = dateFormat.parse(start);
            Date endDate = dateFormat.parse(end);
            return showDeliveryTime(startDate, endDate);
        } catch (ParseException e) {
            return start + " - " + end;
        }
    }

    public static String showDeliveryTime(String start) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss z");

        try {
            Date startDate = dateFormat.parse(start);
            return startDate.toString();
        } catch (ParseException e) {
            return start;
        }
    }

    public static String showDeliveryTimeNoDate(Date startDate, Date endDate) {
        SimpleDateFormat df1 = new SimpleDateFormat("hha");
        SimpleDateFormat df2 = new SimpleDateFormat("hha");

        return String.format("%s - %s", df1.format(startDate), df2.format(endDate));
    }

    public static String showInLocal(Date dateInLocal) {
        return df.format(dateInLocal);
    }

    public static String show1HourEarlierOfShoppingTime(Date startDate, Date endDate) {
        SimpleDateFormat df = new SimpleDateFormat("hh:mma");

        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);
        cal.add(Calendar.MINUTE, -60);
        Date start = cal.getTime();

        cal.setTime(endDate);
        cal.add(Calendar.MINUTE, -60);
        Date end = cal.getTime();

        return String.format("%s - %s", df.format(start), df.format(end));
    }
}
