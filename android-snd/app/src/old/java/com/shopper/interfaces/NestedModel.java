package com.shopper.interfaces;

import java.io.Serializable;

/**
 * Created by ifranseda on 11/18/14.
 */
public interface NestedModel extends Serializable {
    public Long persist();
}
