package com.shopper.interfaces;

/**
 * Created by ifranseda on 1/29/15.
 */
public interface OnCreateOptionMenuListener {
    public void onOptionMenuCreated();
}