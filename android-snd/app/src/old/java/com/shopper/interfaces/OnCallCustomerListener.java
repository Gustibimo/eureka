package com.shopper.interfaces;

import com.happyfresh.snowflakes.hoverfly.models.Shipment;

/**
 * Created by ifranseda on 8/11/15.
 */
public interface OnCallCustomerListener {
    void onClick(Shipment shipment);
}

