package com.shopper.interfaces;

import com.happyfresh.snowflakes.hoverfly.models.Variant;
import com.shopper.adapters.RejectionType;

/**
 * Created by ifranseda on 7/31/15.
 */
public interface RejectionListAction {
    void onDiscard(int position, RejectionType type);

    void onReject(int position, RejectionType type, int quantity, int reasonIndex, String reason);

    void showRejectionDialog(int position, RejectionType type, int amount, Variant variant, String price);
}