package com.shopper.interfaces;

import com.shopper.adapters.RejectionType;

/**
 * Created by ifranseda on 4/27/15.
 */
public interface OnRejectedListener {
    void onRejected(int position, RejectionType type, int quantity, String reason);

    void onDiscarded(int position, RejectionType type, int previousQuantity);
}
