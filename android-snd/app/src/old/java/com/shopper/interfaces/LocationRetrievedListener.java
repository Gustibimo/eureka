package com.shopper.interfaces;

import android.location.Location;

/**
 * Created by rsavianto on 2/9/15.
 */
public interface LocationRetrievedListener {
    public void onLocationRetrieved(Location location);
    public void onLocationNotRetrieved();
}
