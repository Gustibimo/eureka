package com.shopper.interfaces;

import android.view.View;

import com.happyfresh.snowflakes.hoverfly.models.LineItem;
import com.happyfresh.snowflakes.hoverfly.models.Shipment;

/**
 * Created by ifranseda on 9/14/15.
 */
public interface OutOfStockItemListener {
    void searchProduct(View view, LineItem lineItem);

    void showShopperReplacement(LineItem lineItem);

    void finalize(Shipment shipment, int position);

    void callCustomer(Shipment shipment);
}
