package com.shopper.tasks.converter;

import android.content.Context;

import com.google.gson.Gson;
import com.happyfresh.snowflakes.hoverfly.utils.FileUtils;
import com.shopper.tasks.NetworkTask;
import com.shopper.tasks.data.NetworkOperation;
import com.shopper.utils.LogUtils;
import com.shopper.utils.Serializer;
import com.squareup.tape.FileObjectQueue;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;

/**
 * Created by ifranseda on 4/6/15.
 */
public class NetworkTaskConverter implements FileObjectQueue.Converter {
    private final Context context;
    private final Gson gson;

    public NetworkTaskConverter(Context context, Gson gson) {
        this.context = context;
        this.gson = gson;
    }

    @Override
    public NetworkTask from(byte[] bytes) throws IOException {
        Reader reader = new InputStreamReader(new ByteArrayInputStream(bytes));
        NetworkTask task = null;

        try {
            task = gson.fromJson(reader, NetworkTask.class);
            if (task != null) {
                String fileName = FileUtils.getQueueDir(context).getAbsolutePath() + "/" + task.requestID;
                task.operation = (NetworkOperation) Serializer.deserialize(fileName);
            }
        } catch (Exception e) {
            LogUtils.logEvent("Network task queue", "Error: " + e.getLocalizedMessage());
        }

        return task;
    }

    @Override
    public void toStream(Object o, OutputStream bytes) throws IOException {
        Writer writer = new OutputStreamWriter(bytes);
        gson.toJson(o, writer);
        writer.close();

        if (o != null) {
            NetworkTask task = ((NetworkTask) o);
            String fileName = FileUtils.getQueueDir(context).getAbsolutePath() + "/" + task.requestID;
            Serializer.serialize(fileName, task.operation);
        }
    }
}
