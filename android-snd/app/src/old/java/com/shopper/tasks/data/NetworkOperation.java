package com.shopper.tasks.data;

import android.content.Context;

import com.happyfresh.snowflakes.hoverfly.models.LineItem;
import com.happyfresh.snowflakes.hoverfly.models.payload.SerializablePayload;
import com.happyfresh.snowflakes.hoverfly.utils.FileUtils;
import com.shopper.utils.LogUtils;

import java.io.File;
import java.io.Serializable;

/**
 * Created by ifranseda on 4/6/15.
 */
public class NetworkOperation<T extends SerializablePayload> implements Serializable {
    private static final long serialVersionUID = 126142781146165256L;

    public final String name;
    public final TaskManager manager;
    public final Long remoteId;
    public final Long orderId;
    public final Long stockLocationId;
    public final Long variantId;
    public final String requestID;
    public final String imagePath;

    public final T payload;

    public NetworkOperation(String name, TaskManager manager, Long remoteId, T payload) {
        this(name, manager, remoteId, null, null, null, payload, null);
    }

    public NetworkOperation(String name, TaskManager manager, LineItem lineItem, T payload, String imagePath) {
        this(name, manager, null, lineItem.getOrderId(), lineItem.getStockLocationId(), lineItem.getVariantId(), payload, imagePath);
    }

    public NetworkOperation(String name, TaskManager manager, Long remoteId, Long orderId, Long stockLocationId, Long variantId, T payload, String imagePath) {
        this.name = name;
        this.manager = manager;
        this.remoteId = remoteId;
        this.orderId = orderId;
        this.stockLocationId = stockLocationId;
        this.variantId = variantId;
        this.payload = payload;
        this.requestID = payload.getId();
        this.imagePath = imagePath;
    }

    public static void complete(Context context, String requestID) {
        String fileName = FileUtils.getQueueDir(context).getAbsolutePath() + "/" + requestID;
        (new File(fileName)).delete();
    }

    public static void failed(Context context, String requestID) {
        String fileName = FileUtils.getQueueDir(context).getAbsolutePath() + "/" + requestID;

        try {
            File failed = new File(FileUtils.getFailedQueueDir(context), requestID);
            (new File(fileName)).renameTo(failed);
        } catch (Exception e) {
            LogUtils.logEvent("Network queue cannot be moved");
            complete(context, requestID);
        }
    }
}