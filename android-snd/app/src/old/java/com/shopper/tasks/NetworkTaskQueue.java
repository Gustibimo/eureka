package com.shopper.tasks;

import android.content.Context;
import android.content.Intent;

import com.google.gson.Gson;
import com.happyfresh.snowflakes.hoverfly.utils.FileUtils;
import com.shopper.tasks.converter.NetworkTaskConverter;
import com.shopper.utils.LogUtils;
import com.squareup.tape.FileObjectQueue;
import com.squareup.tape.ObjectQueue;
import com.squareup.tape.TaskQueue;

import java.io.File;
import java.io.IOException;

/**
 * Created by ifranseda on 4/6/15.
 */
public class NetworkTaskQueue extends TaskQueue<NetworkTask> {
    private final Context context;

    public NetworkTaskQueue(ObjectQueue<NetworkTask> delegate, Context context) {
        super(delegate);
        this.context = context;

        if (size() > 0) {
            startService();
        }
    }

    public static NetworkTaskQueue create(Context context, Gson gson) {
        FileObjectQueue.Converter<NetworkTask> converter = new NetworkTaskConverter(context, gson);
        File queueFile = FileUtils.getQueueFile(context);

        FileObjectQueue<NetworkTask> delegate;
        try {
            delegate = new FileObjectQueue(queueFile, converter);
        } catch (IOException e) {
            LogUtils.logEvent("Network task failed", "Unable to create file queue >> " + e.getLocalizedMessage());
            throw new RuntimeException("Unable to create file queue.", e);
        }
        return new NetworkTaskQueue(delegate, context);
    }

    public void startService() {
        context.startService(new Intent(context, NetworkQueueService.class));
    }

    @Override
    public void add(NetworkTask task) {
        super.add(task);
        startService();
    }

    @Override
    public void remove() {
        try {
            super.remove();
        } catch (Exception e) {
            LogUtils.logEvent("NetworkTask warning", "Nothing to remove");
        }
    }
}
