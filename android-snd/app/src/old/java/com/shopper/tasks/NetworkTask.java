package com.shopper.tasks;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;

import com.happyfresh.snowflakes.hoverfly.config.Config;
import com.happyfresh.snowflakes.hoverfly.models.payload.SerializablePayload;
import com.shopper.common.ShopperApplication;
import com.shopper.listeners.DataListener;
import com.shopper.managers.BaseManager;
import com.shopper.tasks.data.NetworkOperation;
import com.shopper.tasks.data.TaskManager;
import com.shopper.utils.LogUtils;
import com.squareup.tape.Task;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ifranseda on 4/6/15.
 */
public class NetworkTask implements Task<NetworkTask.Callback> {
    private static final long serialVersionUID = 126142781146165256L;

    public final String requestID;
    public final TaskManager manager;
    public final String method;
    transient public NetworkOperation operation;

    transient private ShopperApplication app;

    public NetworkTask(NetworkOperation operation) {
        this.requestID = operation.requestID;
        this.manager = operation.manager;
        this.method = operation.name;
        this.operation = operation;
    }

    @Override
    public void execute(final Callback callback) {
        app = ShopperApplication.getInstance();

        // Do retrofit network requests
        Method method = null;
        try {
            method = getMethod();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        assert method != null;
        final Method finalMethod = method;

        checkHostReachability(new NetworkCallback() {
            @Override
            public void onNetwork() {
                invoke(finalMethod, callback);
            }

            @Override
            public void onOffline() {
                if (callback != null) {
                    callback.onOffline(requestID);
                }
            }
        });
    }

    private void invoke(Method method, final Callback callback) {
        try {
            if (this.operation == null) {
                if (callback != null) {
                    callback.onFinished(requestID, false, null);
                }
                return;
            }

            DataListener dataListener = new DataListener() {
                @Override
                public void onCompleted(boolean success, Object data) {
                    LogUtils.LOG("requestID = " + requestID + " success? = " + success + "\ndata = " + data);

                    ConnectivityManager conMgr = (ConnectivityManager) app.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo networkInfo = conMgr.getActiveNetworkInfo();
                    if (networkInfo != null && networkInfo.getState() == NetworkInfo.State.CONNECTED) {
                        if (callback != null) {
                            callback.onFinished(requestID, success, data);
                        }
                    } else {
                        if (callback != null) {
                            callback.onOffline(requestID);
                        }
                    }
                }
            };

            if (this.operation.imagePath == null) {
                method.invoke(getManager(), this.operation.remoteId, this.operation.payload, dataListener);
            } else {
                method.invoke(getManager(), this.operation.variantId, this.operation.orderId, this.operation.stockLocationId, this.operation.payload, this.operation.imagePath, dataListener);
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public static Map<String, String> getHost() {
        Pattern p = Pattern.compile("http(|s)://(.*?)(:[0-9]+|)\\/.+");
        Matcher matcher = p.matcher(Config.getBaseUrl());

        String host = "null";
        String port = "80";
        Map<String, String> output = new HashMap<>();

        try {
            while (matcher.find()) {
                host = matcher.group(2);
                if (!TextUtils.isEmpty(matcher.group(3))) {
                    port = matcher.group(3).replace(":", "");
                }
            }
        } catch (IllegalStateException e) {
        }

        output.put("host", host);
        output.put("port", port);

        return output;
    }

    public static void checkHostReachability(final NetworkCallback networkCallback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                boolean exists = false;

                Map<String, String> hostMap = NetworkTask.getHost();

                try {
                    String host = hostMap.get("host");
                    Integer port = Integer.parseInt(hostMap.get("port"));

                    SocketAddress addr = new InetSocketAddress(host, port);
                    // Create an unbound socket
                    Socket sock = new Socket();

                    // This method will block no more than timeoutMs.
                    // If the timeout occurs, SocketTimeoutException is thrown.
                    int timeoutMs = 1000;
                    sock.connect(addr, timeoutMs);

                    exists = true;
                } catch (IOException e) {
                    LogUtils.logEvent("Net error" + e.getLocalizedMessage());
                } finally {
                    if (networkCallback != null) {
                        if (exists) {
                            networkCallback.onNetwork();
                        } else {
                            networkCallback.onOffline();
                        }
                    }
                }
            }
        }).start();
    }

    private Method getMethod() throws NoSuchMethodException {
        Class cls = getManager().getClass();
        if (this.operation.imagePath == null) {
            return cls.getMethod(this.method, new Class[]{Long.class, SerializablePayload.class, DataListener.class});
        } else {
            return cls.getMethod(this.method, new Class[]{Long.class, Long.class, Long.class, SerializablePayload.class, String.class, DataListener.class});
        }
    }

    private BaseManager getManager() {
        if (this.manager == TaskManager.Shipment) {
            return app.getShipmentManager();
        } else if (this.manager == TaskManager.Job) {
            return app.getJobManager();
        }

        return null;
    }

    public interface NetworkCallback {
        void onNetwork();

        void onOffline();
    }

    public interface Callback {
        void onFinished(String requestID, boolean success, Object data);

        void onOffline(String requestID);
    }
}
