package com.shopper.tasks;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.shopper.common.ShopperApplication;
import com.shopper.tasks.data.NetworkOperation;
import com.shopper.utils.LogUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ifranseda on 4/6/15.
 */
public class NetworkQueueService extends Service implements NetworkTask.Callback {
    private NetworkTaskQueue queue;

    private boolean running = false;

    private Map<String, Integer> failedCounter = new HashMap<>();

    private int maxRetry = 1;

    @Override
    public void onCreate() {
        super.onCreate();
        this.queue = getApp().getTaskQueue();
        LogUtils.LOG("Service starting!");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        executeNext();
        return START_STICKY;
    }

    private ShopperApplication getApp() {
        return ShopperApplication.getInstance();
    }

    private void raiseQueueChange() {
        if (getApp() != null) {
            getApp().raiseQueueChanged(getApp().getTaskQueue().size());
        }
    }

    private void executeNext() {
        if (running) return; // Only one task at a time.

        try {
            NetworkTask task = queue.peek();
            if (task != null && task.operation != null) {
                running = true;
                task.execute(this);
            } else {
                queue.remove();
                running = false;
            }
        } catch (Exception e) {
            LogUtils.logEvent("Empty queue or task", "Error: " + e.getLocalizedMessage());
            running = false;
        }
    }

    @Override
    public void onFinished(String requestID, boolean success, Object data) {
        running = false;

        // If request is complete with success response, remove and call complete callback
        if (success) {
            queue.remove();
            NetworkOperation.complete(this, requestID);
            raiseQueueChange();
        } else {
            if (failedCounter.containsKey(requestID)) {
                int failed = failedCounter.get(requestID);
                if (failed < maxRetry) {
                    failedCounter.put(requestID, ++failed);
                } else {
                    queue.remove();
                    NetworkOperation.failed(this, requestID);
                    raiseQueueChange();

                    failedCounter.remove(requestID);
                }
            } else {
                failedCounter.put(requestID, 1);
            }
        }

        executeNext();
    }

    @Override
    public void onOffline(String requestID) {
        running = false;
        raiseQueueChange();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
