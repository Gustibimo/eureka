package com.shopper.tasks;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.NotificationCompat;

import com.happyfresh.fulfillment.R;
import com.shopper.common.ShopperApplication;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by rezarachman8932 on 11/5/15.
 */
public class TimerService extends Service {

    public static final long INTERVAL_TEN_MINUTES = 600000;

    private Handler mHandler = new Handler();
    private Timer mTimer = null;
    private int pauseTime = 0;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        ShopperApplication.getInstance().getWakeLock();

        if (mTimer != null) {
            mTimer.cancel();
        } else {
            mTimer = new Timer();
        }

        mTimer.scheduleAtFixedRate(new TimeDisplayTimerTask(), INTERVAL_TEN_MINUTES, INTERVAL_TEN_MINUTES);
    }

    class TimeDisplayTimerTask extends TimerTask {
        @Override
        public void run() {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    showNotification();
                }
            });
        }
    }

    private void showNotification() {
//        Intent mainIntent = new Intent(ShopperApplication.getContext(), PauseActivity.class);
//        mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//
//        PendingIntent pendingIntent = PendingIntent.getActivity(ShopperApplication.getContext(), 0, mainIntent, 0);
//        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//
//        pauseTime += 10;
//
//        NotificationCompat.Builder b = new NotificationCompat.Builder(ShopperApplication.getContext());
//        b.setAutoCancel(true)
//                .setDefaults(Notification.DEFAULT_ALL)
//                .setSmallIcon(R.drawable.ic_launcher)
//                .setContentTitle(ShopperApplication.getContext().getString(R.string.app_name))
//                .setContentText(getString(R.string.pause_for) + " " + String.valueOf(pauseTime)
//                        + " " + getResources().getString(R.string.minutes).toLowerCase() + ".")
//                .setSound(sound)
//                .setContentIntent(pendingIntent);
//
//        NotificationManager notificationManager = (NotificationManager) ShopperApplication.getContext().getSystemService(Context.NOTIFICATION_SERVICE);
//        notificationManager.notify(1, b.build());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHandler != null) {
            mHandler = null;
        }

        if (mTimer != null) {
            mTimer.cancel();
        }

        pauseTime = 0;

        ShopperApplication.getInstance().stopWakeLock();
    }

}
