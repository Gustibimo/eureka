package com.shopper.services;

import com.happyfresh.snowflakes.hoverfly.models.Batch;
import com.happyfresh.snowflakes.hoverfly.models.FreeItem;
import com.happyfresh.snowflakes.hoverfly.models.payload.ListItemRejectionPayload;
import com.happyfresh.snowflakes.hoverfly.models.payload.OneClickPaymentPayload;
import com.happyfresh.snowflakes.hoverfly.models.response.BatchShoppingListResponse;
import com.happyfresh.snowflakes.hoverfly.models.response.BatchesResponse;
import com.shopper.listeners.BackgroundCallback;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.mime.MultipartTypedOutput;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

/**
 * Created by ifranseda on 8/18/15.
 */
public interface BatchService {
    @GET("/batches/available")
    void available(Callback<BatchesResponse> callback);

    @GET("/batches/active")
    void active(@Query("show_promotions") boolean showPromotions, Callback<Batch> callback);

    @GET("/batches/{id}")
    void batch(@Path("id") Long batchId, Callback<Batch> callback);

    /*
    * DRIVER
    * */

    @POST("/batches/{id}/book")
    void book(@Path("id") Long batchId, Callback<Batch> callback);

    @POST("/batches/{id}/pickup")
    void pickup(@Path("id") Long batchId, Callback<Batch> callback);

    @POST("/batches/{id}/cancel")
    void cancel(@Path("id") Long batchId, Callback<Batch> callback);

    @POST("/batches/{id}/shipments/{shipment_id}/accept")
    void accept(@Path("id") Long batchId,
                @Path("shipment_id") Long shipmentId,
                Callback<Batch> callback);

    @POST("/batches/{id}/shipments/{shipment_id}/deliver")
    void deliver(@Path("id") Long batchId,
                 @Path("shipment_id") Long shipmentId,
                 Callback<Batch> callback);

    @POST("/payments/capture_v2")
    void captureCCPayment(@Body OneClickPaymentPayload payload, Callback<Object> callback);

    @POST("/payments/switch_to_cod")
    void switchToCOD(@Body OneClickPaymentPayload payload, Callback<Object> callback);

    @POST("/batches/{id}/shipments/{shipment_id}/arrive")
    void arrive(@Path("id") Long batchId,
                @Path("shipment_id") Long shipmentId,
                @Query("show_promotions") boolean showPromotions,
                Callback<Batch> callback);

    @Multipart
    @POST("/batches/{id}/shipments/{shipment_id}/finish")
    void finish(@Path("id") Long batchId,
                @Path("shipment_id") Long shipmentId,
                @Part("attachment") TypedFile attachment,
                @Part("receiver") TypedString receiver,
                @Part("cash_amount") TypedString cashAmount,
                @Part("lat") Double lat,
                @Part("lon") Double lon,
                Callback<Batch> callback);

    @POST("/batches/{id}/shipments/{shipment_id}/fail_delivery")
    void fail(@Path("id") Long batchId,
              @Path("shipment_id") Long shipmentId,
              Callback<Batch> callback);

    @GET("/shipments/{shipment_id}/items/rejected_free_item/{variant_id}/{quantity}")
    void rejectionsItem(@Path("shipment_id") long shipmentId,
                        @Path("variant_id") long variantId,
                        @Path("quantity") long quantity,
                        BackgroundCallback<FreeItem> callback);

    @POST("/batches/{id}/shipments/{shipment_id}/items/reject")
    void rejections(@Path("id") Long batchId,
                    @Path("shipment_id") long shipmentId,
                    @Body ListItemRejectionPayload payload,
                    Callback<Batch> callback);

    /*
    * SHOPPER
    * */

    @POST("/batches/{id}/start")
    void start(@Path("id") Long batchId, Callback<Batch> callback);

    @POST("/batches/{id}/shipments/{shipment_id}/pay")
    void pay(@Path("id") Long batchId,
             @Path("shipment_id") Long shipmentId,
             @Body MultipartTypedOutput receipts,
             Callback<Batch> callback);

    @POST("/batches/{id}/shipments/{shipment_id}/finalize")
    void finalize(@Path("id") Long batchId,
                  @Path("shipment_id") Long shipmentId,
                  Callback<Batch> callback);

    /*
    * BATCH ASSIGNMENT
    * */

    @PUT("/batches/{id}/skip")
    void skip(@Path("id") Long batchId, Callback<Batch> callback);


    /*
    * RANGERS
    * */

}