package com.shopper.services;

import com.happyfresh.snowflakes.hoverfly.models.CashTransfer;
import com.happyfresh.snowflakes.hoverfly.models.response.CashHandoverResponse;
import com.happyfresh.snowflakes.hoverfly.models.response.CashesResponse;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

/**
 * Created by ifranseda on 5/6/15.
 */
public interface CashHandoverService {
    @Multipart
    @POST("/cash_transfers")
    void submit(@Part("amount") TypedString amount,
                @Part("currency") TypedString currency,
                @Part("receiver") TypedString receiver,
                @Part("signature") TypedFile signature,
                @Part("photo") TypedFile photo,
                @Part("client_timestamp") long clientTimestamp,
                Callback<CashTransfer> callback);

    @GET("/cash_transfers")
    void history(Callback<CashHandoverResponse> callback);

    @GET("/cashes")
    void balance(Callback<CashesResponse> callback);
}
