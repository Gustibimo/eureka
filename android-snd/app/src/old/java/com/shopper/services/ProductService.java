package com.shopper.services;

import com.happyfresh.snowflakes.hoverfly.models.response.ProductResponse;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by ifranseda on 5/6/15.
 */
public interface ProductService {
    @GET("/stock_locations/{stock_location_id}/products/search")
    void searchProducts(@Path("stock_location_id") long stock_location_id,
                        @Query("q") String query,
                        @Query("page") int page,
                        @Query("per_page") int perPage,
                        @Query("use_sku") boolean useSKU,
                        Callback<ProductResponse> cb);

    @GET("/stock_locations/{stock_location_id}/products/replacement_suggestions/{product_id}")
    void getProductSuggestions(@Path("stock_location_id") long stock_location_id,
                        @Path("product_id") long product_id,
                        @Query("page") int page,
                        @Query("per_page") int perPage,
                        Callback<ProductResponse> cb);
}
