package com.shopper.services;

import com.happyfresh.snowflakes.hoverfly.models.payload.ListProductSamplePayload;
import com.happyfresh.snowflakes.hoverfly.models.payload.SampleOrderPayload;
import com.happyfresh.snowflakes.hoverfly.models.response.BatchProductSampleListResponse;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by kharda on 9/5/16.
 */
public interface ProductSampleService {
    @POST("/sampling/sample")
    void getSample(@Body List<ListProductSamplePayload> payload, Callback<BatchProductSampleListResponse> callback);

    @POST("/sampling/sampleLineItems")
    void finishSampleLineItems(@Body List<SampleOrderPayload> payload, Callback<Void> callback);
}