/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shopper.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.happyfresh.fulfillment.R;
import com.happyfresh.fulfillment.modules.Launch.LaunchActivity;
import com.happyfresh.snowflakes.hoverfly.config.Constant;
import com.happyfresh.snowflakes.hoverfly.models.payload.SendBirdNotificationPayload;
import com.shopper.receivers.NotificationWidgetService;

import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcels;

public class SNDMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (remoteMessage.getData().containsKey(Constant.SENDBIRD)) {
            handleSendBirdPushNotification(remoteMessage);
        }
        else {
            handleBrazePushNotification(remoteMessage);
        }
    }

    private void handleBrazePushNotification(RemoteMessage remoteMessage) {
        Bundle notificationBundle = new Bundle();
        String pushMessageBody = remoteMessage.getData().get("body");
        notificationBundle.putString(Constant.PUSH_MESSAGE, pushMessageBody);

        int assigned = -1;
        if (pushMessageBody != null) {
            try {
                assigned = new JSONObject(pushMessageBody).getInt("a");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (assigned != 2) {
                showNotificationWidget(getApplicationContext(), notificationBundle);
            }

            updateJobList();
        }
    }

    private void showNotificationWidget(Context context, Bundle bundle) {
        Intent intent = new Intent(context, NotificationWidgetService.class);
        intent.putExtras(bundle);
        context.startService(intent);
    }

    private void updateJobList() {
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(Constant.UPDATE_JOBS));
    }

    private void handleSendBirdPushNotification(RemoteMessage remoteMessage) {
        String message = remoteMessage.getData().get("message");

        Gson gson = new Gson();
        SendBirdNotificationPayload payload = gson.fromJson(remoteMessage.getData().get(Constant.SENDBIRD), SendBirdNotificationPayload.class);

        sendNotification(message, payload);
    }

    private void sendNotification(String message, SendBirdNotificationPayload payload) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);

        Intent intent = new Intent(this, LaunchActivity.class);

        Bundle bundle = new Bundle();
        bundle.putParcelable(Constant.SENDBIRD, Parcels.wrap(payload));
        intent.putExtras(bundle);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, payload.getNotificationId(), intent, PendingIntent.FLAG_ONE_SHOT);

        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setSmallIcon(R.drawable.hf_icon_driver);
        mBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        mBuilder.setContentTitle(getApplicationContext().getString(R.string.app_name));
        mBuilder.setContentText(message);
        mBuilder.setAutoCancel(true);

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(payload.getNotificationId(), mBuilder.build());
    }
}