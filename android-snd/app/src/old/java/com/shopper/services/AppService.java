package com.shopper.services;

import com.happyfresh.snowflakes.hoverfly.models.payload.LocationPayload;
import com.happyfresh.snowflakes.hoverfly.models.response.PauseResponse;
import com.happyfresh.snowflakes.hoverfly.models.response.StatusResponse;
import com.happyfresh.snowflakes.hoverfly.models.response.VersionUpdate;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Query;

/**
 * Created by ifranseda on 10/20/15.
 */
public interface AppService {
    @GET("/check_update")
    void checkUpdate(@Query("current_version") int version, Callback<VersionUpdate> callback);

    @PUT("/snd/location")
    void updateLocation(@Body LocationPayload payload, Callback<Object> callback);

    /*
    * CLOCK IN OUT
    * */

    @POST("/snd/clock_out")
    void clockout(Callback<StatusResponse> callback);

    /*
    * PAUSE RESUME
    * */

    @GET("/snd/status")
    void status(Callback<StatusResponse> callback);

    @POST("/snd/pause")
    void pause(Callback<PauseResponse> callback);

    @POST("/snd/resume")
    void resume(Callback<Response> responseCallback);
}
