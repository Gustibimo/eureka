package com.shopper.services;

import com.appboy.Appboy;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.happyfresh.snowflakes.hoverfly.config.Config;
import com.sendbird.android.SendBird;
import com.shopper.utils.LogUtils;

/**
 * Created by kharda on 6/30/16.
 */
public class SNDInstanceIdService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        try {
            String brazeToken = FirebaseInstanceId.getInstance().getToken(Config.getBrazeSenderId(), "FCM");
            Appboy.getInstance(getApplicationContext()).registerAppboyPushMessages(brazeToken);

            String token = FirebaseInstanceId.getInstance().getToken();
            SendBird.registerPushTokenForCurrentUser(token, (pushTokenRegistrationStatus, e) -> { });
        } catch (Exception e) {
            LogUtils.LOG("Exception while automatically registering Firebase token with Appboy");
        }
    }
}
