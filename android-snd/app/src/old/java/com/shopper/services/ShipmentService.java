package com.shopper.services;

import com.happyfresh.snowflakes.hoverfly.models.Shipment;
import com.happyfresh.snowflakes.hoverfly.models.payload.ListItemReplacementPayload;
import com.happyfresh.snowflakes.hoverfly.models.payload.ListItemShopPayload;
import com.happyfresh.snowflakes.hoverfly.models.response.ChangePriceResponse;
import com.happyfresh.snowflakes.hoverfly.models.response.LineItemResponse;
import com.happyfresh.snowflakes.hoverfly.models.response.ShipmentResponse;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

/**
 * Created by ifranseda on 11/13/14.
 */
public interface ShipmentService {

    @GET("/shipments/{id}/items")
    void items(@Path("id") long shipmentId, Callback<LineItemResponse> callback);

    /*
    Queueable request
     */
    @PUT("/shipments/{id}/items/shop")
    void replace(@Path("id") long shipmentId, @Body ListItemReplacementPayload payload,
            Callback<LineItemResponse> callback);

    @Multipart
    @POST("/orders/{order_id}/stock_locations/{stock_location_id}/variants/{variant_id}/price_amendments")
    void changePrice(@Path("order_id") long orderId,
            @Path("stock_location_id") long stockLocationId,
            @Path("variant_id") long variantId,
            @Part("attachment") TypedFile attachment,
            @Part("new_price") TypedString newPrice,
            Callback<ChangePriceResponse> callback);
}
