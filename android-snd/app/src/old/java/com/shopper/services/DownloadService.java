package com.shopper.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.ResultReceiver;

import com.shopper.common.ShopperApplication;
import com.shopper.utils.LogUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by ifranseda on 10/20/15.
 */
public class DownloadService extends IntentService {
    public static final int UPDATE_PROGRESS = 8282;
    public static final int DOWNLOAD_STATUS = 8283;

    public static final String DOWNLOAD_PROGRESS = "DOWNLOAD_PROGRESS";
    public static final String DOWNLOAD_STATE = "DOWNLOAD_STATE";
    public static final String APP_FILENAME = "APP_FILENAME";

    public static boolean isDownloading;

    public DownloadService() {
        super("ApkDownloadService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Context context = ShopperApplication.getContext();

        int current = ShopperApplication.getInstance().getBuildNumber();
        final int version = intent.getIntExtra("version", current);
        final String apkUrl = intent.getStringExtra("url");
        ResultReceiver receiver = intent.getParcelableExtra("receiver");

        if (isDownloading) {
            LogUtils.LOG("Download is in progress");
            return;
        }

        File downloadDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        String fileName = String.format("SND-%d.apk", version);

        File targetFile = new File(downloadDir, fileName);
        String targetPath = targetFile.getPath();

        isDownloading = true;

        int state = DownloadState.INITIATE;
        sendDownloadStatus(receiver, state, fileName);

        try {
            URL url = new URL(apkUrl);
            URLConnection connection = url.openConnection();
            connection.connect();

            // this will be useful so that you can show a typical 0-100% progress bar
            int fileLength = connection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(connection.getInputStream());
            OutputStream output = new FileOutputStream(targetPath);

            state = DownloadState.STARTED;
            sendDownloadProgress(receiver);

            byte data[] = new byte[1024];
            long total = 0;
            int count;
            while ((count = input.read(data)) != -1) {
                total += count;
                output.write(data, 0, count);
            }

            output.flush();
            output.close();
            input.close();

            state = DownloadState.COMPLETED;
        } catch (MalformedURLException e) {
            e.printStackTrace();

            state = DownloadState.FAILED;
        } catch (FileNotFoundException e) {
            e.printStackTrace();

            state = DownloadState.FAILED;
        } catch (IOException e) {
            e.printStackTrace();

            state = DownloadState.FAILED;
        } finally {
            sendDownloadStatus(receiver, state, fileName);
            isDownloading = false;
        }
    }

    void sendDownloadStatus(ResultReceiver receiver, int state, String fileName) {
        Bundle resultData = new Bundle();
        resultData.putInt(DOWNLOAD_STATE, state);
        resultData.putString(APP_FILENAME, fileName);
        receiver.send(DOWNLOAD_STATUS, resultData);
    }

    void sendDownloadProgress(ResultReceiver receiver) {
        Bundle resultData = new Bundle();
        receiver.send(UPDATE_PROGRESS, resultData);
    }

    public class DownloadState {
        public static final int FAILED = -1;
        public static final int INITIATE = 0;
        public static final int STARTED = 1;
        public static final int COMPLETED = 2;
    }
}