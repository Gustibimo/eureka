package com.shopper.services;

import com.happyfresh.snowflakes.hoverfly.models.Job;
import com.happyfresh.snowflakes.hoverfly.models.payload.FailDeliveryPayload;
import com.happyfresh.snowflakes.hoverfly.models.payload.StartJobPayload;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

/**
 * Created by ifranseda on 11/13/14.
 */
public interface JobService {
    @POST("/jobs")
    void start(@Body StartJobPayload job, Callback<Job> callback);

    @Multipart
    @POST("/jobs/{id}/finish")
    void finish(@Path("id") Long jobId,
                @Part("total_price") TypedString totalPrice,
                @Part("receipt_number") TypedString receiptNumber,
                @Part("attachment") TypedFile image,
                Callback<Job> callback);

    @POST("/jobs/{id}/cancel")
    void cancel(@Path("id") Long jobId, Callback<Job> callback);

    @POST("/jobs/{id}/fail_deliver")
    void failDeliver(@Path("id") Long jobId, @Body FailDeliveryPayload failDeliveryPayload, Callback<Job> callback);
}
