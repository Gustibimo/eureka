package com.shopper.managers;

import android.support.v4.content.ContextCompat;
import android.text.TextUtils;

import com.happyfresh.fulfillment.App;
import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.Batch;
import com.happyfresh.snowflakes.hoverfly.models.FreeItem;
import com.happyfresh.snowflakes.hoverfly.models.ReceiptItem;
import com.happyfresh.snowflakes.hoverfly.models.Shipment;
import com.happyfresh.snowflakes.hoverfly.models.payload.ListItemRejectionPayload;
import com.happyfresh.snowflakes.hoverfly.models.payload.OneClickPaymentPayload;
import com.happyfresh.snowflakes.hoverfly.models.payload.ReceiptCollection;
import com.happyfresh.snowflakes.hoverfly.models.response.BatchesResponse;
import com.happyfresh.snowflakes.hoverfly.models.response.ExceptionResponse;
import com.happyfresh.snowflakes.hoverfly.stores.BatchStore;
import com.happyfresh.snowflakes.hoverfly.utils.FileUtils;
import com.shopper.common.Constant;
import com.shopper.common.RestClient;
import com.shopper.common.ShopperApplication;
import com.shopper.listeners.BackgroundCallback;
import com.shopper.listeners.DataListener;
import com.shopper.listeners.SimpleCallback;
import com.shopper.services.BatchService;
import com.shopper.utils.LogUtils;
import com.shopper.utils.SharedPrefUtils;

import java.io.File;
import java.io.FileNotFoundException;

import retrofit.client.Response;
import retrofit.mime.MultipartTypedOutput;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

/**
 * Created by ifranseda on 8/19/15.
 */
public class BatchManager extends BaseManager {

    public BatchManager(ShopperApplication application) {
        super(application);

        this.application = application;
    }

    @Override
    protected BatchService getService() {
        return RestClient.createService(BatchService.class, false);
    }

    @Override
    protected BatchService getMultipartService() {
        return RestClient.createService(BatchService.class, true);
    }

    // TODO: REFACTOR THIS!!!
    public Batch saveBatchToDatabase(Batch o) {
        for (Shipment shipment : o.getRemoteShipments()) {
            shipment.setColorResource(o.getNextColor());
            if (shipment.getOrder() != null && shipment.getOrder().getCompanyId() != 0) {
                shipment.setColorResource(ContextCompat.getColor(ShopperApplication.getContext(), R.color.happycorporate_flag));
            }
        }
        o.save();

        return Batch.findById(o.getRemoteId());
    }

    public void fetchAvailable(final DataListener listener) {
        getService().available(new BackgroundCallback<BatchesResponse>(application, listener) {
            @Override
            public void afterCompletion(final BatchesResponse response) {
                if (listener != null) {
                    listener.onCompleted(true, response.getBatches());
                }
            }
        });
    }

    public void fetchActive(boolean showPromotions, final DataListener listener) {
        getService().active(showPromotions, new BackgroundCallback<Batch>(application, listener) {
            @Override
            public Batch beforeCompletion(Batch o, Response response) {
                application.setBatch(null);

                Batch batch = saveBatchToDatabase(o);
                LogUtils.logEvent("active batch >> " + batch);
                return batch;
            }

            @Override
            public void afterCompletion(final Batch o) {
                if (o.getRemoteId() != null) {
                    application.setBatch(o);

                    BatchStore store = new BatchStore(App.Companion.getContext());
                    store.saveCurrentBatch(o);
                }

                if (listener != null) {
                    listener.onCompleted(true, o);
                }
            }
        });
    }

    public void updateBatch(Long batchId, final DataListener listener) {
        getService().batch(batchId, new BackgroundCallback<Batch>(application, listener) {
            @Override
            public Batch beforeCompletion(Batch o, Response response) {
                return saveBatchToDatabase(o);
            }

            @Override
            public void afterCompletion(final Batch o) {
                if (o.getRemoteId() != null) {
                    application.setBatch(o);
                }

                if (listener != null) {
                    listener.onCompleted(true, o);
                }
            }
        });
    }

    /*
    *
    * DRIVER BATCH ENDPOINTS
    *
    * */

    public void book(Long batchId, final DataListener listener) {
        getService().book(batchId, new BackgroundCallback<Batch>(application, listener) {
            @Override
            public Batch beforeCompletion(Batch o, Response response) {
                application.setBatch(null);
                return saveBatchToDatabase(o);
            }

            @Override
            public void afterCompletion(Batch o) {
                application.setBatch(o);

                if (listener != null) {
                    listener.onCompleted(true, o);
                }
            }
        });
    }

    public void pickup(Long batchId, final DataListener listener) {
        getService().pickup(batchId, new BackgroundCallback<Batch>(application, listener) {
            @Override
            public Batch beforeCompletion(Batch o, Response response) {
                return saveBatchToDatabase(o);
            }

            @Override
            public void afterCompletion(final Batch o) {
                if (listener != null) {
                    listener.onCompleted(true, o);
                }
            }
        });
    }

    public void cancel(Long batchId, final DataListener listener) {
        getService().cancel(batchId, new BackgroundCallback<Batch>(application, listener) {
            @Override
            public Batch beforeCompletion(Batch o, Response response) {
                application.setBatch(null);
                return o;
            }

            @Override
            public void afterCompletion(final Batch o) {
                if (listener != null) {
                    listener.onCompleted(true, o);
                }
            }
        });
    }

    public void accept(Long batchId, Long shipmentId, final DataListener listener) {
        getService().accept(batchId, shipmentId, new BackgroundCallback<Batch>(application, listener) {
            @Override
            public Batch beforeCompletion(Batch o, Response response) {
                return saveBatchToDatabase(o);
            }

            @Override
            public void afterCompletion(final Batch o) {
                if (listener != null) {
                    listener.onCompleted(true, o);
                }
            }
        });
    }

    public void deliver(Long batchId, Long shipmentId, final DataListener listener) {
        getService().deliver(batchId, shipmentId, new BackgroundCallback<Batch>(application, listener) {
            @Override
            public Batch beforeCompletion(Batch o, Response response) {
                application.setBatch(null);
                return saveBatchToDatabase(o);
            }

            @Override
            public void afterCompletion(final Batch o) {
                application.setBatch(o);

                if (listener != null) {
                    listener.onCompleted(true, o);
                }
            }
        });
    }

    public void arrive(Long batchId, Long shipmentId, final DataListener listener) {
        getService().arrive(batchId, shipmentId, true, new BackgroundCallback<Batch>(application, listener) {
            @Override
            public Batch beforeCompletion(Batch o, Response response) {
                return saveBatchToDatabase(o);
            }

            @Override
            public void afterCompletion(final Batch o) {
                if (listener != null) {
                    listener.onCompleted(true, o);
                }
            }
        });
    }

    public void captureCCPayment(OneClickPaymentPayload payload, final DataListener listener) {
        getService().captureCCPayment(payload, new SimpleCallback<Object>(application, listener) {
            @Override
            public void success(Object o, Response response) {
                if (listener != null) {
                    listener.onCompleted(true, o);
                }
            }

            @Override
            public void exception(ExceptionResponse exception) {
                if (listener != null) {
                    listener.onCompleted(false, exception);
                }
            }
        });
    }

    public void switchToCOD(OneClickPaymentPayload payload, final DataListener listener) {
        getService().switchToCOD(payload, new SimpleCallback<Object>(application, listener) {
            @Override
            public void success(Object o, Response response) {
                if (listener != null) {
                    listener.onCompleted(true, o);
                }
            }

            @Override
            public void exception(ExceptionResponse exception) {
                if (listener != null) {
                    listener.onCompleted(false, exception);
                }
            }
        });
    }

    public void finish(Long batchId, Long shipmentId, File attachment,
                       String receiver, String cash, Double lat, Double lon, final DataListener listener) {
        TypedFile inAttachment = new TypedFile("image/png", attachment);
        TypedString inReceiver = new TypedString(receiver);

        TypedString inCash = null;
        if (cash != null) {
            inCash = new TypedString(cash);
        }

        getMultipartService().finish(batchId, shipmentId, inAttachment, inReceiver, inCash, lat, lon,
                new BackgroundCallback<Batch>(application, listener) {
                    @Override
                    public Batch beforeCompletion(Batch o, Response response) {
                        return saveBatchToDatabase(o);
                    }

                    @Override
                    public void afterCompletion(final Batch o) {
                        if (listener != null) {
                            listener.onCompleted(true, o);
                        }
                    }
                });
    }

    public void failDeliver(Long batchId, Long shipmentId, final DataListener listener) {
        getService().fail(batchId, shipmentId, new BackgroundCallback<Batch>(application, listener) {
            @Override
            public Batch beforeCompletion(Batch o, Response response) {
                return saveBatchToDatabase(o);
            }

            @Override
            public void afterCompletion(final Batch o) {
                if (listener != null) {
                    listener.onCompleted(true, o);
                }
            }
        });
    }

    public void rejectionsFreeItem(Long shipmentId, Long variantId, int quantity,
                                   final DataListener<FreeItem> listener) {
        getService().rejectionsItem(shipmentId, variantId, quantity,
                new BackgroundCallback<FreeItem>(application, listener) {
                    @Override
                    public void afterCompletion(FreeItem o) {
                        if (listener != null) {
                            listener.onCompleted(true, o);
                        }
                    }
                });
    }

    public void finalizeRejections(Long batchId, final Long shipmentId, ListItemRejectionPayload payload,
                                   final DataListener<Shipment> listener) {
        getService().rejections(batchId, shipmentId, payload,
                new BackgroundCallback<Batch>(application, listener) {
                    @Override
                    public void afterCompletion(final Batch o) {
                        Shipment shipment = null;
                        for (Shipment s : o.shipments()) {
                            if (shipmentId.equals(s.getRemoteId())) {
                                shipment = s;
                                break;
                            }
                        }

                        if (listener != null) {
                            listener.onCompleted(true, shipment);
                        }
                    }
                });
    }

    /*
    *
    * SHOPPER BATCH ENDPOINTS
    *
    * */

    public void start(Long batchId, final DataListener listener) {
        getService().start(batchId, new BackgroundCallback<Batch>(application, listener) {
            @Override
            public Batch beforeCompletion(Batch o, Response response) {
                SharedPrefUtils.remove(application, Constant.SELECTED_SHOPPING_STORE);

                application.setBatch(null);
                return saveBatchToDatabase(o);
            }

            @Override
            public void afterCompletion(Batch o) {
                application.setBatch(o);

                if (listener != null) {
                    listener.onCompleted(true, o);
                }
            }
        });
    }

    public void finalize(Long batchId, Long shipmentId, final DataListener listener) {
        getService().finalize(batchId, shipmentId, new BackgroundCallback<Batch>(application, listener) {
            @Override
            public Batch beforeCompletion(Batch o, Response response) {
                return saveBatchToDatabase(o);
            }

            @Override
            public void afterCompletion(Batch o) {
                application.setBatch(o);

                if (listener != null) {
                    listener.onCompleted(true, o);
                }
            }
        });
    }

    public void pay(Long batchId, Long shipmentId, ReceiptCollection collection,
                    final DataListener listener) {
        MultipartTypedOutput multipartTypedOutput = new MultipartTypedOutput();
        for (ReceiptItem item : collection.getReceipts()) {
            if (item.completedWithPicture()) {
                try {
                    for (TypedFile attachment : item.getAttachments()) {
                        if (item.getRequireNumber() && !TextUtils.isEmpty(item.getNumber())) {
                            multipartTypedOutput.addPart("receipts[][number]", new TypedString(item.getNumber()));
                        }

                        if (item.getAmount() != null) {
                            multipartTypedOutput.addPart("receipts[][total]", new TypedString(item.getAmount()));
                        }
                        if (item.getTax() != null) {
                            multipartTypedOutput.addPart("receipts[][tax]", new TypedString(item.getTax()));
                        }

                        multipartTypedOutput.addPart("receipts[][attachment]", attachment);
                    }
                } catch (FileNotFoundException e) {
                } catch (FileUtils.FileEmptyException e) {
                }
            }
        }

        getMultipartService().pay(batchId, shipmentId, multipartTypedOutput,
                new BackgroundCallback<Batch>(application, listener) {
                    @Override
                    public Batch beforeCompletion(Batch o, Response response) {
                        return saveBatchToDatabase(o);
                    }

                    @Override
                    public void afterCompletion(Batch o) {
                        application.setBatch(o);

                        if (listener != null) {
                            listener.onCompleted(true, o);
                        }
                    }
                });
    }

    /*
    *
    * DRIVER BATCH ASSIGNEMNT ENDPOINTS
    *
    * */

    public void skip(Long batchId, final DataListener listener) {
        getService().skip(batchId, new BackgroundCallback<Batch>(application, listener) {
            @Override
            public Batch beforeCompletion(Batch o, Response response) {
                return null;
            }

            @Override
            public void afterCompletion(Batch o) {
                if (listener != null) {
                    listener.onCompleted(true, o);
                }
            }
        });
    }
}
