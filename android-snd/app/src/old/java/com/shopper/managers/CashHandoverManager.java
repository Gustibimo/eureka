package com.shopper.managers;

import com.happyfresh.snowflakes.hoverfly.models.CashTransfer;
import com.happyfresh.snowflakes.hoverfly.models.response.CashHandoverResponse;
import com.happyfresh.snowflakes.hoverfly.models.response.CashesResponse;
import com.happyfresh.snowflakes.hoverfly.models.response.ExceptionResponse;
import com.shopper.common.RestClient;
import com.shopper.common.ShopperApplication;
import com.shopper.listeners.DataListener;
import com.shopper.listeners.SimpleCallback;
import com.shopper.services.CashHandoverService;

import retrofit.client.Response;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

/**
 * Created by ifranseda on 5/6/15.
 */
public class CashHandoverManager extends BaseManager {
    private CashHandoverService mCashService;
    private CashHandoverService mCashServiceWithUploader;

    public CashHandoverManager(ShopperApplication application) {
        super(application);

        this.application = application;
        mCashService = RestClient.getInstance().getHandoverService();
        mCashServiceWithUploader = RestClient.getInstance().getHandoverServiceWithUploader();
    }

    public void fetchBalance(final DataListener listener) {
        mCashService.balance(new SimpleCallback<CashesResponse>(application, listener) {
            @Override
            public void success(CashesResponse o, Response response) {
                if (listener != null) {
                    listener.onCompleted(true, o);
                }
            }

            @Override
            public void exception(ExceptionResponse exception) {
                if (listener != null) {
                    listener.onCompleted(false, null);
                }
            }
        });
    }

    public void fetchHistory(final DataListener listener) {
        mCashService.history(new SimpleCallback<CashHandoverResponse>(application, listener) {
            @Override
            public void success(CashHandoverResponse o, Response response) {
                if (listener != null) {
                    listener.onCompleted(true, o);
                }
            }

            @Override
            public void exception(ExceptionResponse exception) {
            }
        });
    }

    public void submit(double amount, String currency, String receiver, TypedFile signature, TypedFile photo, long clientTimestamp, final DataListener listener) {
        TypedString amountStr = new TypedString(String.valueOf(amount));
        TypedString currencyStr = new TypedString(currency);
        TypedString receiverStr = new TypedString(receiver);
        
        mCashServiceWithUploader.submit(amountStr, currencyStr, receiverStr, signature, photo, clientTimestamp,
                new SimpleCallback<CashTransfer>(application, listener) {
                    @Override
                    public void success(CashTransfer o, Response response) {
                        if (listener != null) {
                            listener.onCompleted(true, o);
                        }
                    }
                });
    }
}