package com.shopper.managers;

import com.happyfresh.snowflakes.hoverfly.models.Job;
import com.happyfresh.snowflakes.hoverfly.models.Shipment;
import com.happyfresh.snowflakes.hoverfly.models.payload.FailDeliveryPayload;
import com.happyfresh.snowflakes.hoverfly.models.payload.StartJobPayload;
import com.happyfresh.snowflakes.hoverfly.models.response.ExceptionResponse;
import com.shopper.common.RestClient;
import com.shopper.common.ShopperApplication;
import com.shopper.listeners.DataListener;
import com.shopper.listeners.SimpleCallback;
import com.shopper.services.JobService;
import com.shopper.utils.LogUtils;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

/**
 * Created by ifranseda on 11/25/14.
 */
public class JobManager extends BaseManager {
    private JobService mJobService;
    private JobService mJobServiceWithUploader;

    public JobManager(ShopperApplication application) {
        super(application);

        this.application = application;
        mJobService = RestClient.getInstance().getJobService();
        mJobServiceWithUploader = RestClient.getInstance().getJobServiceWithUploader();
    }

    public void start(final StartJobPayload job, final DataListener listener) {
        mJobService.start(job, new SimpleCallback<Job>(application, listener) {
            @Override
            public void success(Job job, Response response) {
                /* Workaround, B.E. does not give job type */
                job.setJobType(Job.JobType.getSHOPPING());
//                Job.persistUpdate(job);

                if (listener != null) {
                    listener.onCompleted(true, job);
                }
            }

            @Override
            public void exception(ExceptionResponse exception) {
                Shipment shipment = Shipment.findById(job.getShipmentId());

                LogUtils.logEvent("Shopping", "Start failed " + exception.toString());
                super.exception(exception);
            }

            @Override
            public void failure(RetrofitError error) {
                super.failure(error);
                LogUtils.logEvent("Shopping", "Start failed " + error.getLocalizedMessage());
            }
        });
    }

    public void cancel(Long jobId, final DataListener listener) {
        mJobService.cancel(jobId, new SimpleCallback<Job>(application, listener) {
            @Override
            public void success(Job job, Response response) {
                if (listener != null) {
                    listener.onCompleted(true, job);
                }
            }

            @Override
            public void exception(ExceptionResponse exception) {
                LogUtils.logEvent("Cancel Shopping", exception.toString());
                super.exception(exception);
            }
        });
    }

    public void finish(final Job job, TypedString pricePayload, TypedString receiptNumber, TypedFile attachment, final DataListener listener) {
        mJobServiceWithUploader.finish(job.getRemoteId(), pricePayload, receiptNumber, attachment, new SimpleCallback<Job>(application, listener) {
            @Override
            public void success(Job finishedJob, Response response) {
//                Job.persistUpdate(finishedJob);

                if (listener != null) {
                    listener.onCompleted(true, finishedJob);
                }
            }

            @Override
            public void exception(ExceptionResponse exception) {
                super.exception(exception);
            }
        });
    }

    public void driverFailDeliver(Long jobId, FailDeliveryPayload payload, final DataListener listener) {
        mJobService.failDeliver(jobId, payload, new SimpleCallback<Job>(application, listener) {
            @Override
            public void success(Job job, Response response) {
                if (listener != null) {
                    listener.onCompleted(true, job);
                }
            }

            @Override
            public void exception(ExceptionResponse exception) {
                if (listener != null) {
                    listener.onCompleted(false, null);
                }
            }
        });
    }

    public void cancelDriver(Long jobId, final DataListener listener) {
        mJobService.cancel(jobId, new Callback<Job>() {
            @Override
            public void success(Job job, Response response) {
                if (listener != null) {
                    listener.onCompleted(true, job);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (listener != null) {
                    listener.onCompleted(false, error);
                }
            }
        });
    }
}
