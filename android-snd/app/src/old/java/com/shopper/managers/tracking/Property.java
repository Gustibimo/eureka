package com.shopper.managers.tracking;

/**
 * Created by kharda on 11/1/16.
 */

public interface Property {

    String AppLanguage = "app_language";

    String FirstName = "first_name";

    String LastName = "last_name";

    String UserType = "user_type";
}
