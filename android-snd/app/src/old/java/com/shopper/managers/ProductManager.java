package com.shopper.managers;

import android.content.Context;
import android.widget.Toast;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.response.ExceptionResponse;
import com.happyfresh.snowflakes.hoverfly.models.response.ProductResponse;
import com.shopper.common.RestClient;
import com.shopper.common.ShopperApplication;
import com.shopper.listeners.DataListener;
import com.shopper.listeners.SimpleCallback;
import com.shopper.services.ProductService;

import java.util.ArrayList;

import retrofit.client.Response;

/**
 * Created by ifranseda on 5/6/15.
 */
public class ProductManager extends BaseManager {
    private ProductService mProductService;

    public ProductManager(ShopperApplication application) {
        super(application);

        this.application = application;
        mProductService = RestClient.getInstance().getProductService();
    }

    public void searchProduct(Long stockLocationId, String keyword, final DataListener listener) {
        String query = String.format("*%s*", keyword);
        int page = 1;
        int perPage = 100;
        boolean useSKU = true;

        if (query.length() < 5) {
            Context context = ShopperApplication.getContext();
            Toast.makeText(context, context.getResources().getString(R.string.minimum_3_char), Toast.LENGTH_SHORT).show();
            ProductResponse productResponse = new ProductResponse();
            productResponse.setProducts(new ArrayList<>());
            listener.onCompleted(true, productResponse);
            return;
        }

        mProductService.searchProducts(stockLocationId, query, page, perPage, useSKU, new SimpleCallback<ProductResponse>(application, listener) {
            @Override
            public void success(ProductResponse productResponse, Response response) {
                if (listener != null) {
                    listener.onCompleted(true, productResponse);
                }
            }

            @Override
            public void exception(ExceptionResponse exception) {
                super.exception(exception);
            }
        });
    }

    public void getProductSuggestion(Long stockLocationId, Long productId, final DataListener listener) {
        int page = 1;
        int perPage = 100;

        mProductService.getProductSuggestions(stockLocationId, productId, page, perPage, new SimpleCallback<ProductResponse>(application, listener) {
            @Override
            public void success(ProductResponse productResponse, Response response) {
                if (listener != null) {
                    listener.onCompleted(true, productResponse);
                }
            }

            @Override
            public void exception(ExceptionResponse exception) {
                super.exception(exception);
            }
        });
    }
}
