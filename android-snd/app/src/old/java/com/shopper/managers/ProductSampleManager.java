package com.shopper.managers;

import com.happyfresh.snowflakes.hoverfly.models.Batch;
import com.happyfresh.snowflakes.hoverfly.models.ProductSampleItem;
import com.happyfresh.snowflakes.hoverfly.models.payload.ListProductSamplePayload;
import com.happyfresh.snowflakes.hoverfly.models.payload.SampleOrderPayload;
import com.happyfresh.snowflakes.hoverfly.models.response.BatchProductSampleListResponse;
import com.happyfresh.snowflakes.hoverfly.models.response.ProductSampleListResponse;
import com.shopper.common.RestClient;
import com.shopper.common.ShopperApplication;
import com.shopper.listeners.BackgroundCallback;
import com.shopper.listeners.DataListener;
import com.shopper.services.ProductSampleService;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by kharda on 9/7/16.
 */
public class ProductSampleManager extends BaseManager {
    private ProductSampleService mProductSampleService;

    public ProductSampleManager(ShopperApplication application) {
        super(application);

        this.application = application;
        mProductSampleService = RestClient.getInstance().getProductSampleService();
    }

    public void getProductSample(List<ListProductSamplePayload> listProductSamplePayloads,
            final DataListener<Boolean> listener) {
        mProductSampleService.getSample(listProductSamplePayloads,
                new BackgroundCallback<BatchProductSampleListResponse>(application, listener) {
                    @Override
                    public BatchProductSampleListResponse beforeCompletion(BatchProductSampleListResponse o,
                            Response response) {
                        return saveShoppingListToDatabase(o.getData());
                    }

                    @Override
                    public void afterCompletion(BatchProductSampleListResponse o) {
                        if (listener != null) {
                            for (ProductSampleListResponse r : o.getData()) {
                                if (r.getItems().size() > 0) {
                                    listener.onCompleted(true, true);
                                    return;
                                }
                            }

                            listener.onCompleted(true, false);
                        }
                    }
                });
    }

    private BatchProductSampleListResponse saveShoppingListToDatabase(List<ProductSampleListResponse> responses) {
        Batch batch = ShopperApplication.getInstance().getBatch();

        BatchProductSampleListResponse batchProductSampleListResponse = new BatchProductSampleListResponse();
        batchProductSampleListResponse.setData(new ArrayList<>());

        for (ProductSampleListResponse productSampleListResponse : responses) {
            List<ProductSampleItem> savedItems = new ArrayList<>();

            for (ProductSampleItem item : productSampleListResponse.getItems()) {
                ProductSampleItem productSampleItem = ProductSampleItem
                        .findByProductIdAndOrderNumber(item.getProductId(), productSampleListResponse.getOrderNumber());
                item.setBatchId(batch.getRemoteId());
                item.setOrderNumber(productSampleListResponse.getOrderNumber());
                if (productSampleItem == null) {
                    item.save();
                    productSampleItem = item;
                }

                savedItems.add(productSampleItem);
            }

            ProductSampleListResponse sampleList = new ProductSampleListResponse();
            sampleList.setItems(savedItems);
            batchProductSampleListResponse.getData().add(sampleList);
        }

        return batchProductSampleListResponse;
    }

    public void finishSampleLineItems(List<SampleOrderPayload> sampleOrderPayloads,
            final DataListener<Boolean> listener) {
        mProductSampleService.finishSampleLineItems(sampleOrderPayloads, new Callback<Void>() {
            @Override
            public void success(Void sampleLineItemResponse, Response response) {
                listener.onCompleted(true, null);
            }

            @Override
            public void failure(RetrofitError error) {
                listener.onFailed(error);
            }
        });
    }
}
