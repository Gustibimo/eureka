package com.shopper.managers.tracking;

/**
 * Created by kharda on 11/1/16.
 */

public interface Event {

    public static String ONE_CLICK_PAYMENT_FAILED = "One Click Payment - Failed";

    public static String ONE_CLICK_PAYMENT_COD = "One Click Payment - COD";
}
