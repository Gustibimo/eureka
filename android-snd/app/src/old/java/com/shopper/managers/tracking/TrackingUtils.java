package com.shopper.managers.tracking;

import android.content.Context;

import com.happyfresh.snowflakes.hoverfly.models.User;
import com.segment.analytics.Analytics;
import com.segment.analytics.Traits;
import com.shopper.common.Constant;
import com.shopper.common.ShopperApplication;
import com.shopper.utils.SharedPrefUtils;

import java.util.Locale;

/**
 * Created by kharda on 10/27/16.
 * <p/>
 * <p/>
 * Tracking convention:
 * <p/>
 * <p/>
 * Event Name: Sentence capitalize text
 * Props name -> snake case
 * Props value -> lowercase
 * <p/>
 */

public class TrackingUtils {

    public static void identify(Context context, User user) {
        Traits traits = new Traits();

        String countryCode = SharedPrefUtils
                .getString(ShopperApplication.getContext(), Constant.STOCK_LOCATION_COUNTRY_ISO);
        Locale locale = ShopperApplication.getInstance().getLocaleByCountryCode(countryCode);

        traits.put(Property.AppLanguage, locale);

        if (user != null) {
            traits.putEmail(user.getEmail());

            if (user.getFirstName() != null && !user.getFirstName().isEmpty()) {
                traits.put(Property.FirstName, user.getFirstName());
            }

            if (user.getLastName() != null && !user.getLastName().isEmpty()) {
                traits.put(Property.LastName, user.getLastName());
            }

            if (user.isShopper()) {
                traits.put(Property.UserType, Value.Shopper);
            } else {
                traits.put(Property.UserType, Value.Driver);
            }

            Analytics.with(context).identify(String.valueOf(user.getRemoteId()), traits, null);
        } else {
            Analytics.with(context).identify(traits);
        }
    }

    public static void oneClickPaymentFailed(Context context) {
        Analytics.with(context).track(Event.ONE_CLICK_PAYMENT_FAILED);
    }

    public static void oneClickPaymentCOD(Context context) {
        Analytics.with(context).track(Event.ONE_CLICK_PAYMENT_COD);
    }
}
