package com.shopper.managers;

import com.google.gson.Gson;
import com.happyfresh.snowflakes.hoverfly.models.LineItem;
import com.happyfresh.snowflakes.hoverfly.models.PriceAmendment;
import com.happyfresh.snowflakes.hoverfly.models.Replacement;
import com.happyfresh.snowflakes.hoverfly.models.Shipment;
import com.happyfresh.snowflakes.hoverfly.models.ShopperReplacement;
import com.happyfresh.snowflakes.hoverfly.models.Variant;
import com.happyfresh.snowflakes.hoverfly.models.payload.ItemShopPayload;
import com.happyfresh.snowflakes.hoverfly.models.payload.ListItemReplacementPayload;
import com.happyfresh.snowflakes.hoverfly.models.payload.PriceItemPayload;
import com.happyfresh.snowflakes.hoverfly.models.payload.ReplacementPayload;
import com.happyfresh.snowflakes.hoverfly.models.payload.SerializablePayload;
import com.happyfresh.snowflakes.hoverfly.models.response.ChangePriceResponse;
import com.happyfresh.snowflakes.hoverfly.models.response.ExceptionResponse;
import com.happyfresh.snowflakes.hoverfly.models.response.LineItemResponse;
import com.happyfresh.snowflakes.hoverfly.models.response.PriceChangeResponse;
import com.happyfresh.snowflakes.hoverfly.utils.FileUtils;
import com.shopper.common.RestClient;
import com.shopper.common.ShopperApplication;
import com.shopper.events.OrderCanceledEvent;
import com.shopper.events.ShoppedEvent;
import com.shopper.listeners.DataListener;
import com.shopper.listeners.SimpleCallback;
import com.shopper.services.ShipmentService;
import com.shopper.tasks.NetworkTask;
import com.shopper.tasks.data.NetworkOperation;
import com.shopper.tasks.data.TaskManager;
import com.shopper.utils.LogUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

import static com.shopper.common.Constant.CanceledDialogType;
import static com.shopper.common.Constant.OrderCanceledException;

/**
 * Created by ifranseda on 11/17/14.
 */
public class ShipmentManager extends BaseManager {

    public static final String OUT_OF_STOCK = "out_of_stock";

    public static final String ON_REPLACE = "on_replace";

    private ShipmentService mShipmentService;

    private ShipmentService mShipmentMultipartService;

    public ShipmentManager(ShopperApplication application) {
        super(application);

        this.application = application;
        mShipmentService = RestClient.getInstance().getShipmentService();
    }

    @Override
    protected ShipmentService getMultipartService() {
        if (mShipmentMultipartService == null) {
            mShipmentMultipartService = RestClient.createService(ShipmentService.class, true);
        }

        return mShipmentMultipartService;
    }

    public void getAllItems(final Long shipmentId, final DataListener listener) {
        final Shipment shipment = Shipment.findById(shipmentId);
        if (shipment != null && shipment.getOrder() != null) {
            LogUtils.logEvent("Shipment", "Order number: " + shipment.getOrder().getNumber());
        }

        mShipmentService.items(shipmentId, new SimpleCallback<LineItemResponse>(application, listener) {
            @Override
            public void success(LineItemResponse itemResponse, Response response) {
                List<LineItem> savedItems = new ArrayList<LineItem>();

                LogUtils.logEvent("Shipment", "Shipment ID: " + shipmentId);
                StringBuffer stringBuffer = new StringBuffer();

                for (LineItem item : itemResponse.getItems()) {
                    try {
                        if (item.getVariant() != null) {
                            stringBuffer.append(item.getVariant().getName() + "; ");
                        }
                        else {
                            stringBuffer.append("VARIANT NULL; ");
                        }
                    } catch (Exception e) {
                        stringBuffer.append("VARIANT NULL; ");
                    }

                    item.setShipment(shipment);
                    item.save();
//                    item.persist();
                    LineItem lineItem = item;

                    List<Replacement> replacements = item.replacements();
                    for (Replacement replacement : replacements) {
                        if (replacement != null) {
                            Variant variant = replacement.getVariant();
                            if (variant != null) {
                                variant.setPrice(replacement.getPrice());
                                variant.setDisplayPrice(replacement.getDisplayPrice());
                                variant.setCostPrice(replacement.getCostPrice());
                                variant.setDisplayCostPrice(replacement.getDisplayCostPrice());
                                variant.setSupermarketUnitCostPrice(replacement.getSupermarketUnitCostPrice());
                                variant.setDisplaySupermarketUnitCostPrice(
                                        replacement.getDisplaySupermarketUnitCostPrice());
                            }

                            if (replacement.getUserId() == null || replacement.getUserId()
                                    .equals(shipment.getOrder().getUserId())) {
                                Replacement exsReplacement = Replacement.findById(replacement.getRemoteId());
                                if (exsReplacement == null) {
                                    variant.save();
//                                    variant.persist();

                                    replacement.setVariant(variant);
                                    replacement.setLineItem(lineItem);
                                    replacement.save();
//                                    replacement.persist();
                                }
                            }
                            else {
                                if (lineItem.getReplacementShopper() == null && lineItem
                                        .getTotalReplacedShopper() > 0) {
                                    ShopperReplacement exsShprReplacmnt = ShopperReplacement
                                            .findByReplacementId(replacement.getRemoteId());
                                    if (exsShprReplacmnt == null) {
                                        variant.save();
//                                        variant.persist();

                                        ShopperReplacement shprReplacmnt = new ShopperReplacement(
                                                lineItem.getVariantId(), lineItem.getOrderId(),
                                                lineItem.getStockLocationId(), variant.getRemoteId(),
                                                replacement.getVariantId());
                                        shprReplacmnt.setUserId(replacement.getUserId());
                                        shprReplacmnt.save();
//                                        shprReplacmnt.persist();
                                    }
                                }
                            }
                        }
                    }

                    if (item.getPriceChanges() != null) {
                        for (PriceChangeResponse priceResponse : item.getPriceChanges()) {
                            if (priceResponse != null) {
                                PriceAmendment priceAmendment = PriceAmendment.createFromResponse(priceResponse);
                                priceAmendment.save();
                            }
                        }
                    }

                    savedItems.add(lineItem);
                }

                LogUtils.logEvent("Shopping item", "Items: " + stringBuffer.toString());

                if (listener != null) {
                    listener.onCompleted(true, savedItems);
                }
            }
        });
    }

    public void shopperReplaceItem(final Long shipmentId, LineItem lineItem, Replacement replacement, int quantity,
            Double actualWeight, final DataListener.AwaitListener awaits) {

        ListItemReplacementPayload payload = new ListItemReplacementPayload();
        payload.setItems(new ArrayList<>());

        LineItem item = LineItem.findByVariantIdOrderIdAndStockLocationId(lineItem.getVariantId(), lineItem.getOrderId(), lineItem.getStockLocationId());

        ReplacementPayload replacementPayload = new ReplacementPayload();
        replacementPayload.setVariantId(item.getVariant().getRemoteId());
        replacementPayload.setQuantity(item.getTotalShopped() - item.getTotalReplaced());
        replacementPayload.setReplacements(new ArrayList<>());

        ItemShopPayload itemShopPayload = new ItemShopPayload();
        itemShopPayload.setVariantId(replacement.getRemoteId());
        itemShopPayload.setQuantity(quantity);
        itemShopPayload.setActualWeight(actualWeight);
        replacementPayload.getReplacements().add(itemShopPayload);

        payload.getItems().add(replacementPayload);

        NetworkTask task = new NetworkTask(
                new NetworkOperation("submitReplaceItem", TaskManager.Shipment, shipmentId, payload));
        getApplication().getTaskQueue().add(task);

        if (awaits != null) {
            awaits.promise();
        }
    }

    public void submitReplaceItem(final Long shipmentId, SerializablePayload payload, final DataListener listener) {
        ListItemReplacementPayload p = (ListItemReplacementPayload) payload;
        mShipmentService.replace(shipmentId, p, new SimpleCallback<LineItemResponse>(application, listener) {
            @Override
            public void success(LineItemResponse itemResponse, Response response) {
                List<LineItem> lineItemList = new ArrayList<LineItem>();
                for (LineItem item : itemResponse.getItems()) {
                    LineItem lineItem = LineItem.findByLineItem(item);

                    if (lineItem == null) {
                        lineItem = item;
                    }

                    lineItem.setTotalShopped(item.getTotalShopped());
                    lineItem.setTotalReplaced(item.getTotalReplaced());
                    lineItem.setTotalReplacedCustomer(item.getTotalReplacedCustomer());
                    lineItem.setTotalReplacedShopper(item.getTotalReplacedShopper());
                    lineItem.setTotalFlagged(item.getTotalFlagged());
                    lineItem.setFlag(item.getFlag());

                    lineItem.save();

                    Shipment shipment = Shipment.findById(shipmentId);

                    List<Replacement> replacements = item.getReplacements();
                    for (Replacement replacement : replacements) {
                        Variant variant = replacement.getVariant();
                        if (variant != null) {
                            variant.setPrice(replacement.getPrice());
                            variant.setDisplayPrice(replacement.getDisplayPrice());
                            variant.setCostPrice(replacement.getCostPrice());
                            variant.setDisplayCostPrice(replacement.getDisplayCostPrice());
                            variant.setSupermarketUnitCostPrice(replacement.getSupermarketUnitCostPrice());
                            variant.setDisplaySupermarketUnitCostPrice(
                                    replacement.getDisplaySupermarketUnitCostPrice());
                        }

                        if (replacement.getUserId() == null || (shipment != null && shipment
                                .getOrder() != null && replacement.getUserId()
                                .equals(shipment.getOrder().getUserId()))) {
                            Replacement exsReplacement = Replacement.findById(replacement.getRemoteId());
                            if (exsReplacement == null) {
                                if (variant != null) {
                                    variant.save();

                                    lineItem.setReplacementShopper(variant);
                                    lineItem.save();
                                }

                                replacement.setVariant(variant);
                                replacement.setLineItem(lineItem);
                                replacement.save();
                            }
                        }
                        else {
                            if (lineItem.getReplacementShopper() == null && lineItem.getTotalReplacedShopper() > 0) {
                                ShopperReplacement existingShopperReplacement = ShopperReplacement
                                        .findByReplacementId(replacement.getRemoteId());
                                if (existingShopperReplacement == null) {
                                    if (variant != null) {
                                        variant.save();

                                        lineItem.setReplacementShopper(variant);
                                        lineItem.save();
                                    }

                                    replacement.setVariant(variant);
                                    replacement.setLineItem(lineItem);
                                    replacement.save();

                                    ShopperReplacement shopperReplacement = new ShopperReplacement(
                                            lineItem.getVariantId(), lineItem.getOrderId(),
                                            lineItem.getStockLocationId(), variant.getRemoteId(),
                                            replacement.getVariantId());
                                    shopperReplacement.setUserId(replacement.getUserId());
                                    shopperReplacement.save();
                                }
                            }
                        }
                    }

                    lineItemList.add(lineItem);
                }

                ShoppedEvent e = new ShoppedEvent(lineItemList);
                application.getBus().post(e);

                if (listener != null) {
                    if (lineItemList.size() > 0) {
                        listener.onCompleted(true, lineItemList.get(0));
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                handleCanceledOrder(error);

                if (listener != null) {
                    listener.onCompleted(false, null);
                }
            }
        });
    }

    public void changePrice(LineItem lineItem, Long replacementId, double newPrice, String imagePath,
            final DataListener.AwaitListener awaits) {
        PriceItemPayload payload = new PriceItemPayload();
        payload.setNewPrice(newPrice);
        if (replacementId != null) {
            payload.setReplacementId(replacementId);
        }

        NetworkTask task = new NetworkTask(
                new NetworkOperation("submitPriceChange", TaskManager.Shipment, lineItem, payload, imagePath));
        getApplication().getTaskQueue().add(task);

        if (awaits != null) {
            awaits.promise();
        }
    }

    public void submitPriceChange(final Long variantId, final Long orderId, final Long stockLocationId,
            SerializablePayload payload, String imagePath,
            final DataListener listener) {
        final PriceItemPayload priceItemPayload = (PriceItemPayload) payload;

        File imageFile = null;
        try {
            imageFile = FileUtils.getFile(imagePath);
        } catch (FileNotFoundException | FileUtils.FileEmptyException e) {
            e.printStackTrace();
            return;
        }

        TypedFile inAttachment = new TypedFile("image/jpg", imageFile);
        TypedString inNewPrice = new TypedString(Double.toString(priceItemPayload.getNewPrice()));

        getMultipartService()
                .changePrice(orderId, stockLocationId, variantId, inAttachment, inNewPrice,
                        new SimpleCallback<ChangePriceResponse>(application, listener) {
                            @Override
                            public void success(ChangePriceResponse changePriceResponse, Response response) {
                                LineItem mItem = LineItem.findByVariantIdOrderIdAndStockLocationId(variantId, orderId, stockLocationId);

                                if (mItem == null) {
                                    return;
                                }

                                PriceAmendment priceAmendment = new PriceAmendment();
                                if (mItem.getPriceAmendment() != null) {
                                    priceAmendment = mItem.getPriceAmendment();
                                }

                                priceAmendment.setRemoteId(changePriceResponse.getId());
                                priceAmendment.setVariantId(changePriceResponse.getVariantId());
                                priceAmendment.setOrderId(changePriceResponse.getOrderId());
                                priceAmendment.setStockLocationId(changePriceResponse.getStockLocationId());
                                priceAmendment.setReplacementId(changePriceResponse.getReplacementId());
                                priceAmendment.setUserId(changePriceResponse.getUser());

                                if (priceItemPayload.getReplacementId() != 0) {
                                    priceAmendment.setReplacement(changePriceResponse.getNewPrice());
                                }
                                else {
                                    priceAmendment.setVariant(changePriceResponse.getNewPrice());
                                }

                                priceAmendment.save();
//                                priceAmendment.persist();

                                mItem.setPriceAmendment(priceAmendment);
                                mItem.save();

                                ShoppedEvent e = new ShoppedEvent(mItem);
                                application.getBus().post(e);

                                if (listener != null) {
                                    listener.onCompleted(true, changePriceResponse);
                                }
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                handleCanceledOrder(error);

                                error.printStackTrace();
                                LogUtils.LOG(error.getLocalizedMessage());
                                if (listener != null) {
                                    listener.onCompleted(false, null);
                                }
                            }
                        });
    }

    private void handleCanceledOrder(RetrofitError error) {
        if (error != null) {
            try {
                String jsonStr = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                ExceptionResponse exceptionResponse = new Gson().fromJson(jsonStr, ExceptionResponse.class);

                if (exceptionResponse != null) {
                    if (exceptionResponse.getType().equalsIgnoreCase(OrderCanceledException)) {
                        application.getBus().post(new OrderCanceledEvent(CanceledDialogType.SHOP));
                    }
                }
            } catch (Exception e) {
            }
        }
    }
}
