package com.shopper.managers;

import android.content.Context;

import com.happyfresh.snowflakes.hoverfly.Sprinkles;
import com.happyfresh.snowflakes.hoverfly.models.payload.LocationPayload;
import com.happyfresh.snowflakes.hoverfly.models.response.ExceptionResponse;
import com.happyfresh.snowflakes.hoverfly.models.response.PauseResponse;
import com.happyfresh.snowflakes.hoverfly.models.response.StatusResponse;
import com.happyfresh.snowflakes.hoverfly.models.response.VersionUpdate;
import com.happyfresh.snowflakes.hoverfly.stores.UserStore;
import com.shopper.common.Constant;
import com.shopper.common.RestClient;
import com.shopper.common.ShopperApplication;
import com.shopper.listeners.BackgroundCallback;
import com.shopper.listeners.DataListener;
import com.shopper.listeners.SimpleCallback;
import com.shopper.services.AppService;
import com.shopper.utils.LogUtils;
import com.shopper.utils.SharedPrefUtils;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ifranseda on 10/20/15.
 */
public class AppManager extends BaseManager {

    private AppService mService;

    public AppManager(ShopperApplication application) {
        super(application);
        mService = RestClient.getInstance().getAppService();
    }

    public void checkUpdate(int version, final DataListener listener) {
        mService.checkUpdate(version, new Callback<VersionUpdate>() {
            @Override
            public void success(VersionUpdate versionUpdate, Response response) {
                if (listener != null) {
                    listener.onCompleted(true, versionUpdate);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (listener != null) {
                    listener.onCompleted(false, null);
                }
            }
        });
    }

    public void updateLocation(final double latitude, final double longitude, final double accuracy) {
        LocationPayload payload = new LocationPayload(latitude, longitude, accuracy);
        mService.updateLocation(payload, new Callback<Object>() {
            @Override
            public void success(Object o, Response response) {
                LogUtils.logEvent("Location Update",
                        "Updated >> " + latitude + ", " + longitude + ". Accuracy >>" + accuracy);
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
    }

    public void getStatus(final DataListener listener) {
        mService.status(new BackgroundCallback<StatusResponse>(application, listener) {
            @Override
            public void afterCompletion(StatusResponse status) {
                if (status != null) {
                    updateConfiguration(application, status);
                }

                if (listener != null) {
                    listener.onCompleted(true, status);
                }
            }
        });
    }

    public void clockOut(final DataListener listener) {
        mService.clockout(new SimpleCallback<StatusResponse>(application, listener) {
            @Override
            public void success(StatusResponse status, Response response) {
                if (status != null) {
                    updateConfiguration(application, status);
                }

                SharedPrefUtils.remove(application, Constant.SELECTED_SHOPPING_STORE);

                if (listener != null) {
                    listener.onCompleted(true, status);
                }
            }

            @Override
            public void exception(ExceptionResponse exception) {
                if (exception != null && Constant.UserIsClockedOutException.equals(exception.getType())) {
                    SharedPrefUtils.remove(application, Constant.SELECTED_SHOPPING_STORE);

                    if (listener != null) {
                        listener.onCompleted(true, null);
                    }
                }
            }
        });
    }

    private void updateConfiguration(Context context, StatusResponse statusResponse) {
        if (context == null) {
            return;
        }

        UserStore userStore = Sprinkles.stores(UserStore.class);
        userStore.saveStatus(statusResponse);
    }

    public void pause(final DataListener listener) {
        mService.pause(new BackgroundCallback<PauseResponse>(application, listener) {
            @Override
            public void afterCompletion(PauseResponse o) {
                if (listener != null) {
                    listener.onCompleted(true, o);
                }
            }
        });
    }

    public void resume(final DataListener listener) {
        mService.resume(new BackgroundCallback<Response>(application, listener) {
            @Override
            public void afterCompletion(Response o) {
                if (listener != null) {
                    listener.onCompleted(true, o);
                }
            }
        });
    }
}
