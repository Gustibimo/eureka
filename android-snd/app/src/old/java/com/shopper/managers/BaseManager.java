package com.shopper.managers;

import com.shopper.common.ShopperApplication;

/**
 * Created by ifranseda on 11/17/14.
 */
public abstract class BaseManager {
    protected ShopperApplication application;

    public BaseManager(ShopperApplication app) {
        application = app;
    }

    protected ShopperApplication getApplication() {
        return application;
    }

    protected <T> T getService() {
        return null;
    }

    protected <T> T getMultipartService() {
        return null;
    }
}
