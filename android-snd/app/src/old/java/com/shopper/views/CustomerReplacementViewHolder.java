package com.shopper.views;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.Job;
import com.happyfresh.snowflakes.hoverfly.models.LineItem;
import com.happyfresh.snowflakes.hoverfly.models.RejectionItem;
import com.happyfresh.snowflakes.hoverfly.models.SpreeImage;
import com.happyfresh.snowflakes.hoverfly.models.Variant;
import com.shopper.adapters.RejectionType;
import com.shopper.interfaces.RejectionListAction;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.BindView;

/**
 * Created by ifranseda on 7/31/15.
 */
public class CustomerReplacementViewHolder extends DeliveryItemViewHolder {
    @BindView(R.id.replacement_container)
    View replacementContainer;

    @BindView(R.id.replacement_indicator)
    View replacementIndicator;

    @BindView(R.id.replacement_item_total)
    TextView replacementItemTotal;

    @BindView(R.id.replacement_item_image)
    ImageView replacementItemImage;

    @BindView(R.id.replacement_item_container)
    View replacementItemContainer;

    @BindView(R.id.replacement_item_variant_name)
    TextView replacementItemName;

    @BindView(R.id.replacement_item_price)
    TextView replacementItemPrice;

    @BindView(R.id.replacement_item_price_calculated)
    TextView replacementItemPriceCalculated;

    @BindView(R.id.replacement_item_price_after_rejected)
    TextView replacementItemPriceAfterRejected;

    @BindView(R.id.replacement_reject_button_container)
    View replacementRejectButtonContainer;

    @BindView(R.id.replacement_reject_button)
    Button replacementRejectButton;

    @BindView(R.id.replacement_reject_counter_container)
    View replacementRejectCounterContainer;

    @BindView(R.id.replacement_cancel_reject)
    ImageButton replacementCancelReject;

    @BindView(R.id.replacement_update_rejected)
    View replacementRejectUpdate;

    @BindView(R.id.replacement_reject_counter)
    TextView replacementRejectCounter;

    @BindView(R.id.replacement_customer_overlay)
    View replacementCustomerOverlay;

    public CustomerReplacementViewHolder(View itemView, Context ctx, RejectionListAction action) {
        super(itemView, ctx, action);
        ButterKnife.bind(this, itemView);
    }

    protected void showCustomerReplacement(final LineItem item, final int position) {
        replacementContainer.setVisibility(View.VISIBLE);

        NumberFormat formatter = getNumberFormatter(item.getShipment().getOrder().getCurrency());

        final Variant replacement = item.getReplacementCustomer();
        String replacementProductUrl = null;

        if (replacement != null) {
            List<SpreeImage> replacementImages = replacement.images();
            if (replacementImages.size() > 0) {
                replacementProductUrl = replacementImages.get(0).getProductUrl();
            }

            Picasso.with(mContext).cancelRequest(replacementItemImage);
            Picasso.with(mContext).load(replacementProductUrl).placeholder(R.drawable.default_product).into(replacementItemImage);

            replacementItemTotal.setText(String.format("%d", item.getTotalReplacedCustomer()));
            replacementItemName.setText(replacement.getName());

            final String replacementPriceStr = String.valueOf(replacement.getDisplayPrice());
            replacementItemPrice.setText(replacementPriceStr);

            double replacementTotalPrice;
            String calculatedReplace;
            if (replacement.hasNaturalUnit()) {
                replacementTotalPrice = replacement.getPrice() * item.getTotalReplacedCustomer() + replacement.getActualWeightAdjustmentTotal();
                calculatedReplace = formatter.format(replacementTotalPrice);
                calculatedReplace = String.format("%s /%s", calculatedReplace, replacement.getDisplayActualWeight());
            } else {
                replacementTotalPrice = replacement.getPrice() * item.getTotalReplacedCustomer();
                calculatedReplace = formatter.format(replacementTotalPrice);
            }

            replacementItemPriceCalculated.setText(calculatedReplace);
            replacementItemPriceAfterRejected.setText(calculatedReplace);

            final String finalReplacedPriceStr = replacementPriceStr;
            replacementRejectButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mRejectionAction.showRejectionDialog(position, RejectionType.REPLACEMENT_CUSTOMER, item.getTotalReplacedCustomer(), replacement, finalReplacedPriceStr);
                }
            });

            replacementRejectUpdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mRejectionAction.showRejectionDialog(position, RejectionType.REPLACEMENT_CUSTOMER, item.getTotalReplacedCustomer(), replacement, finalReplacedPriceStr);
                }
            });

            replacementCancelReject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    replacementItemPriceAfterRejected.setVisibility(View.GONE);
                    mRejectionAction.onDiscard(position, RejectionType.REPLACEMENT_CUSTOMER);
                }
            });
        }

        if (item.getTotalReplacedCustomer() == 0) {
            replacementCustomerOverlay.setVisibility(View.VISIBLE);
        } else {
            replacementCustomerOverlay.setVisibility(View.GONE);
        }

        replacementItemPriceCalculated.setAllCaps(false);
        replacementItemPriceCalculated.setTextColor(mContext.getResources().getColor(R.color.delivery_item_total_price));
        replacementRejectButtonContainer.setVisibility(View.VISIBLE);

        if (item.rejectionItems() != null && item.rejectionItems().size() > 0) {
            for (RejectionItem rejected : item.rejectionItems()) {
                if (replacement != null && rejected.getVariantId().equals(item.getReplacementCustomer().getRemoteId())) {
                    if (rejected.getQuantity() > 0) {
                        replacementRejectCounterContainer.setVisibility(View.VISIBLE);
                        replacementRejectButton.setVisibility(View.GONE);

                        replacementRejectCounter.setText(String.valueOf(rejected.getQuantity()));

                        double itemWeightChange  = 0d;
                        int qnt = item.getTotalReplacedCustomer() - rejected.getQuantity();
                        if (item.getReplacementCustomer().hasNaturalUnit()) {
                            itemWeightChange = item.getReplacementCustomer().getActualWeightAdjustmentTotal() / item.getTotalReplacedCustomer();
                            itemWeightChange = qnt * itemWeightChange;
                        }

                        String replacementTotalPriceAfterReject = formatter.format(item.getReplacementCustomer().getPrice() * qnt + itemWeightChange);
                        replacementItemPriceCalculated.setText(replacementTotalPriceAfterReject);

                        replacementItemPriceAfterRejected.setVisibility(View.VISIBLE);
                        replacementItemPriceAfterRejected.setPaintFlags(replacementItemPriceAfterRejected.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    } else {
                        replacementItemPriceAfterRejected.setVisibility(View.GONE);
                        replacementRejectButton.setVisibility(View.VISIBLE);
                        replacementRejectCounterContainer.setVisibility(View.GONE);
                    }
                }
            }
        } else {
            replacementItemPriceAfterRejected.setVisibility(View.GONE);
            replacementRejectButton.setVisibility(View.VISIBLE);
            replacementRejectCounterContainer.setVisibility(View.GONE);
        }


        Drawable closeButton = mContext.getResources().getDrawable(R.drawable.btn_close_dark);
        closeButton.setColorFilter(mContext.getResources().getColor(R.color.delivery_cancel_rejection), PorterDuff.Mode.SRC_ATOP);
        replacementCancelReject.setImageDrawable(closeButton);

        // Hide all rejection button, if current shipment state is not accepted yet
        if (!Job.Progress.getFOUND_ADDRESS().equalsIgnoreCase(item.getShipment().getDeliveryJob().getState())) {
            replacementRejectButtonContainer.setVisibility(View.GONE);
            replacementRejectButton.setVisibility(View.GONE);
        }
    }

    public void showData(LineItem item, int position) {
        super.showData(item, position);
        showCustomerReplacement(item, position);
    }
}
