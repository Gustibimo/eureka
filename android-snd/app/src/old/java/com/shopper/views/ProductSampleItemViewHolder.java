package com.shopper.views;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.ProductSampleItem;
import com.happyfresh.snowflakes.hoverfly.models.SpreeImage;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.BindView;

/**
 * Created by kharda on 9/15/16.
 */
public class ProductSampleItemViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.category_container)
    public LinearLayout categoryContainer;

    @BindView(R.id.category_name)
    public TextView categoryName;

    @BindView(R.id.order_number)
    public TextView orderNumber;

    @BindView(R.id.customer_name)
    public TextView customerName;

    @BindView(R.id.item_status)
    public TextView shoppingStatus;

    @BindView(R.id.item_image)
    public ImageView itemImage;

    @BindView(R.id.item_counter_total)
    public TextView itemCounterTotal;

    @BindView(R.id.item_counter)
    public TextView itemCounter;

    @BindView(R.id.item_variant_name)
    public TextView itemVariantName;

    @BindView(R.id.item_price)
    public TextView itemPrice;

    @BindView(R.id.item_replacement_status)
    public TextView itemReplacementStatus;

    @BindView(R.id.customer_replacement)
    public RelativeLayout customerReplacementLayout;

    @BindView(R.id.shopper_replacement)
    public RelativeLayout shopperReplacementLayout;

    @BindView(R.id.item_display_banner)
    public TextView itemDisplayBanner;

    @BindView(R.id.shopper_note_container)
    LinearLayout shopperNotesContainer;

    Context context;

    public ProductSampleItemViewHolder(Context ctx, View itemView) {
        super(itemView);
        context = ctx;
        ButterKnife.bind(this, itemView);
    }

    public void setCategory(String category) {
        if (TextUtils.isEmpty(category)) {
            categoryContainer.setVisibility(View.GONE);
        } else {
            categoryContainer.setVisibility(View.VISIBLE);
            categoryName.setText(category);
        }
    }

    public void showItemDetail(ProductSampleItem item) {
        showOrderIndicator(item);
        showPurchaseStatus(item);

        showOriginalItem(item);
        hideUnusedLayout();
    }

    private void showOrderIndicator(ProductSampleItem item) {
        orderNumber.setText(item.getOrderNumber());
        customerName.setText(item.getCustomerName());
    }

    private void showPurchaseStatus(ProductSampleItem item) {
        ProductSampleItem.PurchaseStatus purchaseStatus = item.getStatus();
        if (purchaseStatus == ProductSampleItem.PurchaseStatus.NOT_STARTED) {
            shoppingStatus.setVisibility(View.GONE);
        } else {
            shoppingStatus.setVisibility(View.VISIBLE);
            shoppingStatus.setText(purchaseStatus.statusName(context));
            shoppingStatus.setTextColor(purchaseStatus.textColor(context));
            shoppingStatus.setBackgroundColor(purchaseStatus.backgroundColor(context));
        }
    }

    private void showOriginalItem(ProductSampleItem item) {
        List<SpreeImage> images = item.getVariant().images();
        String productUrl = null;
        if (images.size() > 0) {
            productUrl = images.get(0).getProductUrl();
        }

        Picasso.with(context).cancelRequest(itemImage);
        Picasso.with(context).load(productUrl).placeholder(R.drawable.default_product).into(itemImage);

        String shoppedCount = context.getString(R.string.item_shopped_counter, item.getTotalShopped(), item.getTotal());

        itemCounter.setText(String.valueOf(item.getTotalShopped()));
        itemCounterTotal.setText(shoppedCount);
        itemVariantName.setText(item.getVariant().getName().trim());
        itemPrice.setText(getOriginalPrice(item));
    }

    private void hideUnusedLayout() {
        shopperReplacementLayout.setVisibility(View.GONE);
        customerReplacementLayout.setVisibility(View.GONE);
        itemDisplayBanner.setVisibility(View.GONE);
        itemReplacementStatus.setVisibility(View.GONE);
        shopperNotesContainer.setVisibility(View.GONE);
    }

    private String getOriginalPrice(ProductSampleItem item) {
        String price = item.getDisplayCostPrice();

        if (item.hasNaturalUnit()) {
            return String.format("%s/%s", price, item.getDisplayAverageWeight());
        } else {
            return price;
        }
    }
}
