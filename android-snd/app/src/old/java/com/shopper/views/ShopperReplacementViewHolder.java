package com.shopper.views;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.Job;
import com.happyfresh.snowflakes.hoverfly.models.LineItem;
import com.happyfresh.snowflakes.hoverfly.models.RejectionItem;
import com.happyfresh.snowflakes.hoverfly.models.SpreeImage;
import com.happyfresh.snowflakes.hoverfly.models.Variant;
import com.shopper.adapters.RejectionType;
import com.shopper.interfaces.RejectionListAction;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.BindView;

/**
 * Created by ifranseda on 7/31/15.
 */
public class ShopperReplacementViewHolder extends DeliveryItemViewHolder {
    @BindView(R.id.replacement_shopper_container)
    View replacementShopperContainer;

    @BindView(R.id.replacement_shopper_indicator)
    View replacementShopperIndicator;

    @BindView(R.id.replacement_shopper_item_total)
    TextView replacementShopperItemTotal;

    @BindView(R.id.replacement_shopper_item_image)
    ImageView replacementShopperItemImage;

    @BindView(R.id.replacement_shopper_item_container)
    View replacementShopperItemContainer;

    @BindView(R.id.replacement_shopper_item_variant_name)
    TextView replacementShopperItemName;

    @BindView(R.id.replacement_shopper_item_price)
    TextView replacementShopperItemPrice;

    @BindView(R.id.replacement_shopper_item_price_calculated)
    TextView replacementShopperItemPriceCalculated;

    @BindView(R.id.replacement_shopper_item_price_after_rejected)
    TextView replacementShopperItemPriceAfterRejected;

    @BindView(R.id.replacement_shopper_reject_button_container)
    View replacementShopperRejectButtonContainer;

    @BindView(R.id.replacement_shopper_reject_button)
    Button replacementShopperRejectButton;

    @BindView(R.id.replacement_shopper_reject_counter_container)
    View replacementShopperRejectCounterContainer;

    @BindView(R.id.replacement_shopper_cancel_reject)
    ImageButton replacementShopperCancelReject;

    @BindView(R.id.replacement_shopper_update_rejected)
    View replacementShopperRejectUpdate;

    @BindView(R.id.replacement_shopper_reject_counter)
    TextView replacementShopperRejectCounter;

    @BindView(R.id.replacement_shopper_overlay)
    View replacementShopperOverlay;

    public ShopperReplacementViewHolder(View itemView, Context ctx, RejectionListAction action) {
        super(itemView, ctx, action);
        ButterKnife.bind(this, itemView);
    }

    protected void showShopperReplacement(final LineItem item, final int position) {
        replacementShopperContainer.setVisibility(View.VISIBLE);

        NumberFormat formatter = getNumberFormatter(item.getShipment().getOrder().getCurrency());

        final Variant replacement = item.getReplacementShopper();
        String replacementProductUrl = null;

        if (replacement != null) {
            List<SpreeImage> replacementImages = replacement.images();
            if (replacementImages.size() > 0) {
                replacementProductUrl = replacementImages.get(0).getProductUrl();
            }

            Picasso.with(mContext).cancelRequest(replacementShopperItemImage);
            Picasso.with(mContext).load(replacementProductUrl).placeholder(R.drawable.default_product).into(replacementShopperItemImage);

            replacementShopperItemTotal.setText(String.format("%d", item.getTotalReplacedShopper()));
            replacementShopperItemName.setText(replacement.getName());

            final String replacementPriceStr = String.valueOf(replacement.getDisplayPrice());
            replacementShopperItemPrice.setText(replacementPriceStr);

            double replacementTotalPrice;
            String calculatedReplace;
            if (replacement.hasNaturalUnit()) {
                replacementTotalPrice = replacement.getPrice() * item.getTotalReplacedShopper() + replacement.getActualWeightAdjustmentTotal();
                calculatedReplace = formatter.format(replacementTotalPrice);
                calculatedReplace = String.format("%s /%s", calculatedReplace, replacement.getDisplayActualWeight());
            } else {
                replacementTotalPrice = replacement.getPrice() * item.getTotalReplacedShopper();
                calculatedReplace = formatter.format(replacementTotalPrice);
            }
            replacementShopperItemPriceCalculated.setText(calculatedReplace);
            replacementShopperItemPriceAfterRejected.setText(calculatedReplace);

            final String finalReplacedPriceStr = replacementPriceStr;
            replacementShopperRejectButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mRejectionAction.showRejectionDialog(position, RejectionType.REPLACEMENT_SHOPPER, item.getTotalReplacedShopper(), replacement, finalReplacedPriceStr);
                }
            });

            replacementShopperRejectUpdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mRejectionAction.showRejectionDialog(position, RejectionType.REPLACEMENT_SHOPPER, item.getTotalReplacedShopper(), replacement, finalReplacedPriceStr);
                }
            });

            replacementShopperCancelReject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    replacementShopperItemPriceAfterRejected.setVisibility(View.GONE);
                    mRejectionAction.onDiscard(position, RejectionType.REPLACEMENT_SHOPPER);
                }
            });
        }

        if (item.getTotalReplacedShopper() == 0) {
            replacementShopperOverlay.setVisibility(View.VISIBLE);
        } else {
            replacementShopperOverlay.setVisibility(View.GONE);
        }

        replacementShopperItemPriceCalculated.setAllCaps(false);
        replacementShopperItemPriceCalculated.setTextColor(mContext.getResources().getColor(R.color.delivery_item_total_price));
        replacementShopperRejectCounterContainer.setVisibility(View.GONE);

        if (item.rejectionItems() != null && item.rejectionItems().size() > 0) {
            for (RejectionItem rejected : item.rejectionItems()) {
                if (replacement != null && rejected.getVariantId().equals(item.getReplacementShopper().getRemoteId())) {
                    if (rejected.getQuantity() > 0) {
                        replacementShopperRejectCounterContainer.setVisibility(View.VISIBLE);
                        replacementShopperRejectButton.setVisibility(View.GONE);

                        replacementShopperRejectCounter.setText(String.valueOf(rejected.getQuantity()));

                        double itemWeightChange  = 0d;
                        int qnt = item.getTotalReplacedShopper() - rejected.getQuantity();
                        if (item.getReplacementShopper().hasNaturalUnit()) {
                            itemWeightChange = item.getReplacementShopper().getActualWeightAdjustmentTotal() / item.getTotalReplacedShopper();
                            itemWeightChange = qnt * itemWeightChange;
                        }

                        String replacementTotalPriceAfterReject = formatter.format(item.getReplacementShopper().getPrice() * qnt + itemWeightChange);
                        replacementShopperItemPriceCalculated.setText(replacementTotalPriceAfterReject);

                        replacementShopperItemPriceAfterRejected.setVisibility(View.VISIBLE);
                        replacementShopperItemPriceAfterRejected.setPaintFlags(replacementShopperItemPriceAfterRejected.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    } else {
                        replacementShopperItemPriceAfterRejected.setVisibility(View.GONE);
                        replacementShopperRejectButton.setVisibility(View.VISIBLE);
                        replacementShopperRejectCounterContainer.setVisibility(View.GONE);
                    }
                }
            }
        } else {
            replacementShopperItemPriceAfterRejected.setVisibility(View.GONE);
            replacementShopperRejectButton.setVisibility(View.VISIBLE);
            replacementShopperRejectCounterContainer.setVisibility(View.GONE);
        }

        Drawable closeButton = mContext.getResources().getDrawable(R.drawable.btn_close_dark);
        closeButton.setColorFilter(mContext.getResources().getColor(R.color.delivery_cancel_rejection), PorterDuff.Mode.SRC_ATOP);
        replacementShopperCancelReject.setImageDrawable(closeButton);

        // Hide all rejection button, if current shipment state is not accepted yet
        if (!Job.Progress.getFOUND_ADDRESS().equalsIgnoreCase(item.getShipment().getDeliveryJob().getState())) {
            replacementShopperRejectButtonContainer.setVisibility(View.GONE);
            replacementShopperRejectButton.setVisibility(View.GONE);
        }
    }

    public void showData(LineItem item, int position) {
        super.showData(item, position);
        showShopperReplacement(item, position);
    }
}
