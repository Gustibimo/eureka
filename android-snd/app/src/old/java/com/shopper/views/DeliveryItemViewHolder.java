package com.shopper.views;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.Job;
import com.happyfresh.snowflakes.hoverfly.models.LineItem;
import com.happyfresh.snowflakes.hoverfly.models.RejectionItem;
import com.happyfresh.snowflakes.hoverfly.models.Shipment;
import com.happyfresh.snowflakes.hoverfly.models.SpreeImage;
import com.happyfresh.snowflakes.hoverfly.utils.FormatterUtils;
import com.shopper.adapters.RejectionType;
import com.shopper.interfaces.RejectionListAction;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringUtils;

import java.text.NumberFormat;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.BindView;

/**
 * Created by ifranseda on 7/31/15.
 */
public class DeliveryItemViewHolder extends RecyclerView.ViewHolder {
    protected Context mContext;

    protected RejectionListAction mRejectionAction;

    @BindView(R.id.original_container)
    View originalContainer;

    @BindView(R.id.item_total)
    TextView itemTotal;

    @BindView(R.id.item_image)
    ImageView itemImage;

    @BindView(R.id.origin_item_container)
    View originItemContainer;

    @BindView(R.id.item_variant_name)
    TextView itemName;

    @BindView(R.id.item_price)
    TextView itemPrice;

    @BindView(R.id.item_price_calculated)
    TextView itemPriceCalculated;

    @BindView(R.id.item_price_after_rejected)
    TextView itemPriceAfterRejected;

    @BindView(R.id.reject_button_container)
    View rejectButtonContainer;

    @BindView(R.id.reject_button)
    Button rejectButton;

    @BindView(R.id.reject_counter_container)
    View rejectCounterContainer;

    @BindView(R.id.cancel_reject)
    ImageButton cancelReject;

    @BindView(R.id.update_rejected)
    View rejectUpdate;

    @BindView(R.id.reject_counter)
    TextView rejectCounter;

    @BindView(R.id.original_overlay)
    View originalOverlay;

    @BindView(R.id.shopper_note_container)
    View shopperContainer;

    @BindView(R.id.shopper_note)
    public TextView shopperNote;

    public DeliveryItemViewHolder(View itemView, Context ctx, RejectionListAction action) {
        super(itemView);
        mContext = ctx;
        mRejectionAction = action;

        ButterKnife.bind(this, itemView);
    }

    protected NumberFormat getNumberFormatter(String currencyCode) {
        return FormatterUtils.getNumberFormatter(currencyCode);
    }

    protected void showOriginal(final LineItem item, final int position) {
        List<SpreeImage> images = item.getVariant().images();
        String productUrl = null;
        if (images.size() > 0) {
            productUrl = images.get(0).getProductUrl();
        }

        Picasso.with(mContext).cancelRequest(itemImage);
        Picasso.with(mContext).load(productUrl).placeholder(R.drawable.default_product).into(itemImage);

        itemName.setText(item.getVariant().getName());

        NumberFormat formatter = getNumberFormatter(item.getShipment().getOrder().getCurrency());

        String priceStr = item.getSingleDisplayAmount();
        if (item.hasNaturalUnit()) {
            priceStr = String.format("%s / %s", item.getSingleDisplayAmount(), item.getDisplayActualWeight());
        }

        itemPrice.setText(priceStr);

        int shoppedCounter = item.getTotalShopped() - item.getTotalReplacedCustomer() - item.getTotalReplacedShopper();

        String itemTotalPrice;
        if (item.hasNaturalUnit()) {
            itemTotalPrice = formatter.format(item.getPrice() * shoppedCounter + item.getActualWeightAdjustmentTotal());
            itemTotalPrice = String.format("%s /%s", itemTotalPrice, item.getDisplayActualWeight());
        } else {
            itemTotalPrice = formatter
                    .format(item.getPrice() * (shoppedCounter - (item.getTotalFreeItem() + item.getTotalRejectedFreeItem())));
        }

        itemPriceCalculated.setText(itemTotalPrice);
        itemPriceAfterRejected.setText(itemTotalPrice);

        itemTotal.setText(String.format("%d/%d", shoppedCounter, item.getTotal()));

        if (shoppedCounter == 0) {
            originalOverlay.setVisibility(View.VISIBLE);
            itemPriceCalculated.setText(R.string.action_flag_item);
            itemPriceCalculated.setAllCaps(true);
            itemPriceCalculated.setTextColor(mContext.getResources().getColor(R.color.delivery_item_reject));

            rejectButtonContainer.setVisibility(View.GONE);
        } else {
            originalOverlay.setVisibility(View.GONE);
            itemPriceCalculated.setAllCaps(false);
            itemPriceCalculated.setTextColor(mContext.getResources().getColor(R.color.delivery_item_total_price));

            rejectButtonContainer.setVisibility(View.VISIBLE);
        }

        if (item.rejectionItems() != null && item.rejectionItems().size() > 0) {
            for (RejectionItem rejected : item.rejectionItems()) {
                if (rejected.getVariantId().equals(item.getVariant().getRemoteId())) {
                    if (rejected.getQuantity() > 0) {
                        rejectCounterContainer.setVisibility(View.VISIBLE);
                        rejectButton.setVisibility(View.GONE);

                        rejectCounter.setText(String.valueOf(rejected.getQuantity()));

                        double itemWeightChange = 0d;
                        int qnt = shoppedCounter - (rejected.getQuantity() - item.getTotalRejectedFreeItem()) - (item.getTotalFreeItem() + item.getTotalRejectedFreeItem());
                        if (item.hasNaturalUnit() && qnt > 0) {
                            itemWeightChange = item.getActualWeightAdjustmentTotal() / shoppedCounter;
                            itemWeightChange = qnt * itemWeightChange;
                        }

                        String itemTotalPriceAfterReject = formatter.format(item.getPrice() * qnt + itemWeightChange);
                        itemPriceCalculated.setText(itemTotalPriceAfterReject);

                        itemPriceAfterRejected.setVisibility(View.VISIBLE);
                        itemPriceAfterRejected
                                .setPaintFlags(itemPriceAfterRejected.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    } else {
                        itemPriceAfterRejected.setVisibility(View.GONE);
                        rejectButton.setVisibility(View.VISIBLE);
                        rejectCounterContainer.setVisibility(View.GONE);
                    }
                }
            }
        } else {
            itemPriceAfterRejected.setVisibility(View.GONE);
            rejectButton.setVisibility(View.VISIBLE);
            rejectCounterContainer.setVisibility(View.GONE);
        }

        final int finalShoppedCounter = shoppedCounter;
        final String finalPriceStr = priceStr;
        rejectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mRejectionAction
                        .showRejectionDialog(position, RejectionType.ORIGINAL, finalShoppedCounter, item.getVariant(),
                                finalPriceStr);
            }
        });

        rejectUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mRejectionAction
                        .showRejectionDialog(position, RejectionType.ORIGINAL, finalShoppedCounter, item.getVariant(),
                                finalPriceStr);
            }
        });

        cancelReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemPriceAfterRejected.setVisibility(View.GONE);
                mRejectionAction.onDiscard(position, RejectionType.ORIGINAL);
            }
        });

        Drawable closeButton = mContext.getResources().getDrawable(R.drawable.btn_close_dark);
        closeButton.setColorFilter(mContext.getResources().getColor(R.color.delivery_cancel_rejection),
                PorterDuff.Mode.SRC_ATOP);
        cancelReject.setImageDrawable(closeButton);

        // Hide all rejection button, if current shipment state is not accepted yet
        if (item.getShipment().getDeliveryJob() == null || !Job.Progress.getFOUND_ADDRESS()
                .equalsIgnoreCase(item.getShipment().getDeliveryJob().getState())) {
            rejectButtonContainer.setVisibility(View.GONE);
            rejectButton.setVisibility(View.GONE);
        }

        if (StringUtils.isEmpty(item.getNotes())) {
            shopperContainer.setVisibility(View.GONE);
            shopperNote.setText(null);
        } else {
            shopperContainer.setVisibility(View.VISIBLE);
            shopperNote.setText(item.getNotes());
        }
    }

    public void showShoppingBag(Shipment shipment, int position) {
        itemPriceCalculated.setVisibility(View.INVISIBLE);
        itemPriceAfterRejected.setVisibility(View.INVISIBLE);
        itemPrice.setVisibility(View.INVISIBLE);
        rejectButtonContainer.setVisibility(View.GONE);
        shopperContainer.setVisibility(View.GONE);
        itemTotal.setVisibility(View.INVISIBLE);
        itemName.setText(this.mContext.getString(R.string.shopping_bag));
        Picasso.with(mContext).cancelRequest(itemImage);
        Picasso.with(mContext).load(R.drawable.icon_bag_with_logo).into(itemImage);
    }

    public void showData(final LineItem item, final int position) {
        assert (mRejectionAction != null);
        showOriginal(item, position);
    }
}
