package com.shopper.views;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.ReceiptItem;
import com.happyfresh.snowflakes.hoverfly.models.Shipment;
import com.happyfresh.snowflakes.hoverfly.utils.FormatterUtils;

import java.text.NumberFormat;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentReceiptHeaderViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.receipt_number)
    TextView mReceiptNumberTextView;

    @BindView(R.id.receipt_amount)
    TextView mReceiptAmountTextView;

    @BindView(R.id.receipt_tax)
    TextView mReceiptTaxTextView;

    @BindView(R.id.receipt_number_container)
    LinearLayout mReceiptNumberContainer;

    NumberFormat numberFormatter;

    public PaymentReceiptHeaderViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void showData(Shipment shipment, final ReceiptItem receiptItem) {
        if (receiptItem.getRequireNumber()) {
            mReceiptNumberContainer.setVisibility(View.VISIBLE);
        } else {
            mReceiptNumberContainer.setVisibility(View.GONE);
        }
        String amount = getNumberFormatter(shipment.getOrder().getCurrency()).format(Double.valueOf(receiptItem.getAmount()));
        String tax = getNumberFormatter(shipment.getOrder().getCurrency()).format(Double.valueOf(receiptItem.getTax()));

        mReceiptNumberTextView.setText(receiptItem.getNumber());
        mReceiptAmountTextView.setText(amount);
        mReceiptTaxTextView.setText(tax);

    }

    private NumberFormat getNumberFormatter(String currencyCode) {
        if (numberFormatter == null) {
            numberFormatter = FormatterUtils.getNumberFormatter(currencyCode);
        }

        return numberFormatter;
    }

}