package com.shopper.views;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.Shipment;

import butterknife.ButterKnife;
import butterknife.BindView;

/**
 * Created by ifranseda on 11/25/15.
 */
public class PaymentHeaderViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.item_order_number)
    TextView itemOrderNumber;

    @BindView(R.id.item_customer_name)
    TextView itemCustomerName;

    public PaymentHeaderViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void showData(Shipment shipment) {
        itemOrderNumber.setText(shipment.getOrder().getNumber());
        itemCustomerName.setText(shipment.getCustomerName());
    }
}
