package com.shopper.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by ifranseda on 1/19/16.
 */
public class CollapsibleImageView extends ImageView {

    public CollapsibleImageView(Context context) {
        super(context);
    }

    public CollapsibleImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CollapsibleImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int width = getMeasuredWidth();
        setMeasuredDimension(width, (int) (width / 1.8));
    }
}
