package com.shopper.views;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.happyfresh.fulfillment.R;
import com.shopper.common.ShopperApplication;
import com.shopper.components.CircularProgressBar;
import com.shopper.events.OpenCameraEvent;
import com.happyfresh.snowflakes.hoverfly.models.PhotoReceiptItem;
import com.happyfresh.snowflakes.hoverfly.utils.FileUtils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentPhotoReceiptViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.open_camera_button)
    View openCameraButton;

    @BindView(R.id.camera_result_image)
    ImageView cameraResultImage;

    @BindView(R.id.camera_indicator)
    ImageView cameraIndicator;

    @BindView(R.id.hint_capture)
    View hintOpenCamera;

    @BindView(R.id.progress_bar)
    CircularProgressBar progressBar;

    private Context mContext;

    public PaymentPhotoReceiptViewHolder(Context context, View itemView) {
        super(itemView);
        mContext = context;
        ButterKnife.bind(this, itemView);

        cameraIndicator.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
    }

    public void showData(final String imagePath) {
//    public void showData(final PhotoReceiptItem photoReceiptItem) {
//        String imagePath = photoReceiptItem.imagePath;

        openCameraButton.setEnabled(true);
        progressBar.setVisibility(View.GONE);
        hintOpenCamera.setVisibility(View.VISIBLE);

        if (!TextUtils.isEmpty(imagePath)) {
            try {
                File file = FileUtils.getFile(imagePath);
                Picasso.with(mContext).load(file).into(cameraResultImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        progressBar.setVisibility(View.GONE);
                    }
                });

                hintOpenCamera.setVisibility(View.GONE);

                cameraResultImage.setVisibility(View.VISIBLE);
                cameraIndicator.setVisibility(View.VISIBLE);
            } catch (FileNotFoundException e) {
                cameraResultImage.setVisibility(View.GONE);
                cameraIndicator.setVisibility(View.GONE);
                e.printStackTrace();
            } catch (FileUtils.FileEmptyException e) {
                cameraResultImage.setVisibility(View.GONE);
                cameraIndicator.setVisibility(View.GONE);
                e.printStackTrace();
            }
        }
    }

    public void setCameraOnClick(final PhotoReceiptItem photoReceiptItem) {
        openCameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCameraButton.setEnabled(false);

                ShopperApplication.getInstance().getBus().post(new OpenCameraEvent(photoReceiptItem));
                progressBar.setVisibility(View.VISIBLE);
                hintOpenCamera.setVisibility(View.GONE);
            }
        });
    }

}