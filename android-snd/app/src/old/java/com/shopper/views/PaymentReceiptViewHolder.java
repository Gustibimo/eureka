package com.shopper.views;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.ReceiptItem;
import com.shopper.fragments.PaymentFragment;
import com.shopper.listeners.PaymentReceiptListener;

import butterknife.ButterKnife;
import butterknife.BindView;

/**
 * Created by ifranseda on 11/25/15.
 */
public class PaymentReceiptViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.receipt_number_container)
    View receiptNumberContainer;

    @BindView(R.id.receipt_number)
    EditText receiptNumber;

    @BindView(R.id.receipt_amount)
    EditText receiptAmount;

    @BindView(R.id.receipt_tax)
    EditText receiptTax;

    @BindView(R.id.receipt_text)
    TextView receiptText;

    @BindView(R.id.remove_receipt_container)
    RelativeLayout removeReceiptContainer;

    PaymentReceiptListener listener;

    public PaymentReceiptViewHolder(View itemView, PaymentReceiptListener listener) {
        super(itemView);
        ButterKnife.bind(this, itemView);

        this.listener = listener;
    }

    public void showData(final ReceiptItem receipt, final boolean requireNumber, Context context, final int receiptPosition) {
        final PaymentFragment.ReceiptNumberTextWatcher receiptNumberTextWatcher = new PaymentFragment.ReceiptNumberTextWatcher(receipt);
        final PaymentFragment.ReceiptAmountTextWatcher receiptAmountTextWatcher = new PaymentFragment.ReceiptAmountTextWatcher(receipt);
        final PaymentFragment.ReceiptTaxTextWatcher receiptTaxTextWatcher = new PaymentFragment.ReceiptTaxTextWatcher(receipt);

        receiptText.setText(context.getString(R.string.receipt_text, String.valueOf(receiptPosition+1)));
        if (receiptPosition == 0) {
            removeReceiptContainer.setVisibility(View.INVISIBLE);
        } else {
            removeReceiptContainer.setVisibility(View.VISIBLE);
        }

        removeReceiptContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.remove(receiptPosition);
                }
            }
        });

        if (requireNumber) {
            receiptNumberContainer.setVisibility(View.VISIBLE);
        } else {
            receiptNumberContainer.setVisibility(View.GONE);
        }

        receiptNumber.setText(receipt.getNumber());
        if (requireNumber) {
            receiptNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean focused) {
                    if (focused) {
                        receiptNumber.addTextChangedListener(receiptNumberTextWatcher);
                    } else {
                        receiptNumber.removeTextChangedListener(receiptNumberTextWatcher);
                    }
                }
            });
        }

        receiptAmount.setText(receipt.getAmount());
        receiptAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean focused) {
                if (focused) {
                    receiptAmount.addTextChangedListener(receiptAmountTextWatcher);
                } else {
                    receiptAmount.removeTextChangedListener(receiptAmountTextWatcher);
                }
            }
        });

        receiptTax.setText(receipt.getTax());
        receiptTax.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean focused) {
                if (focused) {
                    receiptTax.addTextChangedListener(receiptTaxTextWatcher);
                } else {
                    receiptTax.removeTextChangedListener(receiptTaxTextWatcher);
                }
            }
        });

    }
}
