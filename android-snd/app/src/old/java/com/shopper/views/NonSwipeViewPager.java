package com.shopper.views;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by ifranseda on 12/17/14.
 */
public class NonSwipeViewPager extends ViewPager {
    private boolean mSwipeable = true;

    public NonSwipeViewPager(Context context) {
        super(context);
    }

    public NonSwipeViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (mSwipeable) {
            return super.onInterceptTouchEvent(event);
        } else {
            return false;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (mSwipeable) {
            return super.onTouchEvent(event);
        } else {
            return false;
        }
    }

    public void setSwipeable(boolean swipeable) {
        mSwipeable = swipeable;
    }
}