package com.shopper.views;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.happyfresh.fulfillment.R;
import com.shopper.common.ShopperApplication;
import com.shopper.events.AddReceiptEvent;

import butterknife.ButterKnife;
import butterknife.BindView;

/**
 * Created by ifranseda on 11/25/15.
 */
public class PaymentAddReceiptButton extends RecyclerView.ViewHolder {
    @BindView(R.id.add_receipt_button)
    Button addReceiptButton;

    public PaymentAddReceiptButton(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

        final AddReceiptEvent event = new AddReceiptEvent();
        addReceiptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShopperApplication.getInstance().getBus().post(event);
            }
        });
    }
}
