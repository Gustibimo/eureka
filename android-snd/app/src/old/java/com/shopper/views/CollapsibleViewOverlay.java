package com.shopper.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Created by ifranseda on 1/23/16.
 */
public class CollapsibleViewOverlay extends RelativeLayout {

    public CollapsibleViewOverlay(Context context) {
        super(context);
    }

    public CollapsibleViewOverlay(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CollapsibleViewOverlay(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public CollapsibleViewOverlay(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int width = getMeasuredWidth();
        setMeasuredDimension(width, (int) (width / 1.8));
    }
}

