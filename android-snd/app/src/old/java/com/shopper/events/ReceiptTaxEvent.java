package com.shopper.events;

import com.happyfresh.snowflakes.hoverfly.models.ReceiptItem;

public class ReceiptTaxEvent extends UIEvent<ReceiptItem> {
    public String tax;

    public ReceiptTaxEvent(ReceiptItem o, String tax) {
        super(o);
        this.tax = tax;
    }

    public ReceiptItem getData() {
        return data;
    }
}