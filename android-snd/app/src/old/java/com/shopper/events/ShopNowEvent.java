package com.shopper.events;

/**
 * Created by ifranseda on 2/16/16.
 */
public class ShopNowEvent extends UIEvent<Boolean> {
    public ShopNowEvent(Boolean enabled) {
        super(enabled);
    }

    public boolean isEnabled() {
        return data;
    }
}

