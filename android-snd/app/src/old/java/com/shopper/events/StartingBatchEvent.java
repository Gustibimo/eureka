package com.shopper.events;

import com.happyfresh.snowflakes.hoverfly.models.Batch;

/**
 * Created by ifranseda on 25/09/2017.
 */

public class StartingBatchEvent extends UIEvent<Batch> {

    public StartingBatchEvent(Batch o) {
        super(o);
    }

    public Batch getBatch() {
        return data;
    }
}
