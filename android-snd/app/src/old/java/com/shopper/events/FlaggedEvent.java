package com.shopper.events;

import com.happyfresh.snowflakes.hoverfly.models.LineItem;

import java.util.List;

/**
 * Created by ifranseda on 9/25/15.
 */
public class FlaggedEvent extends BusEvent<LineItem> {
    public FlaggedEvent(List<LineItem> l) {
        super(l);
    }
}
