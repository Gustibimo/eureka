package com.shopper.events;

import com.happyfresh.snowflakes.hoverfly.models.ReceiptItem;

/**
 * Created by ifranseda on 11/27/15.
 */
public class ReceiptNumberEvent extends UIEvent<ReceiptItem> {

    public String text;

    public ReceiptNumberEvent(ReceiptItem o, String text) {
        super(o);
        this.text = text;
    }

    public ReceiptItem getData() {
        return data;
    }
}
