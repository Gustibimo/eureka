package com.shopper.events;

import com.happyfresh.snowflakes.hoverfly.models.Shipment;

import java.util.List;

/**
 * Created by ifranseda on 9/7/15.
 */
public class ShipmentEvent extends BusEvent<Shipment> {
    public ShipmentEvent(Shipment o) {
        super(o);
    }

    public ShipmentEvent(List<Shipment> l) {
        super(l);
    }
}
