package com.shopper.events;

/**
 * Created by rsavianto on 12/10/15.
 */
public class StatusEvent {
    private String mStatus;

    public StatusEvent(String status) {
        this.mStatus = status;
    }

    public String getStatus() {
        return mStatus;
    }
}
