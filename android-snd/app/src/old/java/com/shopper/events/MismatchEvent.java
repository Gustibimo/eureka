package com.shopper.events;

import com.happyfresh.snowflakes.hoverfly.models.LineItem;

/**
 * Created by ifranseda on 07/04/2017.
 */

public class MismatchEvent extends BusEvent<LineItem> {

    public MismatchEvent(LineItem l) {
        super(l);
    }
}
