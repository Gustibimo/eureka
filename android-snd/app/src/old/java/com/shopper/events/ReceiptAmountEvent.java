package com.shopper.events;

import com.happyfresh.snowflakes.hoverfly.models.ReceiptItem;

public class ReceiptAmountEvent extends UIEvent<ReceiptItem> {
    public String amount;

    public ReceiptAmountEvent(ReceiptItem o, String amount) {
        super(o);
        this.amount = amount;
    }

    public ReceiptItem getData() {
        return data;
    }
}