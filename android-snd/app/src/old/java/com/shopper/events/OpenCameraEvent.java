package com.shopper.events;

import com.happyfresh.snowflakes.hoverfly.models.PhotoReceiptItem;

/**
 * Created by ifranseda on 11/26/15.
 */
public class OpenCameraEvent extends UIEvent<PhotoReceiptItem> {

    public OpenCameraEvent() {
        super(null);
    }

    public OpenCameraEvent(PhotoReceiptItem o) {
        super(o);
    }

    public PhotoReceiptItem getData() {
        return data;
    }
}
