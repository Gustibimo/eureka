package com.shopper.events;

import com.happyfresh.snowflakes.hoverfly.models.ReceiptItem;

public class TakePictureEvent extends UIEvent<ReceiptItem> {
    public TakePictureEvent() {
        super(null);
    }

    public TakePictureEvent(ReceiptItem o) {
        super(o);
    }

    public ReceiptItem getData() {
        return data;
    }
}