package com.shopper.events;

import static com.shopper.common.Constant.CanceledDialogType;

public class OrderCanceledEvent {
    private CanceledDialogType dialogType;

    public CanceledDialogType getDialogType() {
        return dialogType;
    }

    public OrderCanceledEvent(CanceledDialogType dialogType) {
        this.dialogType = dialogType;
    }

}
