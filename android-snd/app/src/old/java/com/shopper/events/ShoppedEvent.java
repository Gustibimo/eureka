package com.shopper.events;

import com.happyfresh.snowflakes.hoverfly.models.LineItem;

import java.util.List;

/**
 * Created by ifranseda on 9/7/15.
 */
public class ShoppedEvent extends BusEvent<LineItem> {
    public ShoppedEvent(LineItem o) {
        super(o);
    }

    public ShoppedEvent(List<LineItem> l) {
        super(l);
    }
}
