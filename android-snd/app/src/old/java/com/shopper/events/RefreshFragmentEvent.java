package com.shopper.events;

import com.shopper.common.Constant.RefreshFragmentType;

public class RefreshFragmentEvent {
    private RefreshFragmentType refreshType;

    public RefreshFragmentEvent(RefreshFragmentType refreshType) {
        this.refreshType = refreshType;
    }

    public boolean isOOS() {
        return refreshType == RefreshFragmentType.OOS;
    }

    public boolean isPay() {
        return refreshType == RefreshFragmentType.PAY;
    }
}
