package com.shopper.events;

import java.io.Serializable;

/**
 * Created by ifranseda on 11/26/15.
 */
public abstract class UIEvent<T extends Serializable> {
    protected T data;

    public UIEvent(T o) {
        this.data = o;
    }
}
