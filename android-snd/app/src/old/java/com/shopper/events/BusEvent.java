package com.shopper.events;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ifranseda on 9/7/15.
 */
public abstract class BusEvent<T> {
    protected T data;
    protected List<T> dataList;

    protected boolean mListData = false;

    public BusEvent(T o) {
        this.data = o;
    }

    public BusEvent(List<T> l) {
        mListData = true;
        this.dataList = l;
    }

    public boolean isList() {
        return mListData;
    }

    public T getData() {
        return data;
    }

    public List<T> getList() {
        if (dataList == null && data != null) {
            dataList = new ArrayList<T>();
            dataList.add(data);
        }

        return dataList;
    }
}
