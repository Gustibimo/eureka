package com.shopper.events;

import com.happyfresh.snowflakes.hoverfly.models.Batch;

/**
 * Created by ifranseda on 9/28/15.
 */
public class FinalizedEvent extends BusEvent<Batch> {
    public FinalizedEvent(Batch o) {
        super(o);
    }
}
