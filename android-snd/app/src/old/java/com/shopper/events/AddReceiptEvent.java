package com.shopper.events;

import com.happyfresh.snowflakes.hoverfly.models.ReceiptItem;

/**
 * Created by ifranseda on 11/27/15.
 */
public class AddReceiptEvent extends UIEvent<ReceiptItem> {

    public AddReceiptEvent() {
        super(null);
    }

    public AddReceiptEvent(ReceiptItem o) {
        super(o);
    }
}