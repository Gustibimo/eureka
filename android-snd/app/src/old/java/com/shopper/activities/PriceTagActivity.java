package com.shopper.activities;

import android.app.Fragment;
import android.support.annotation.NonNull;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.happyfresh.fulfillment.R;
import com.shopper.fragments.PriceTagFragment;

/**
 * Created by kharda on 03/01/16.
 */

public class PriceTagActivity extends FragmentActivity {

    private PriceTagFragment mFragment;

    @Override
    protected Fragment createFragment() {
        mFragment = PriceTagFragment.newInstance();
        return mFragment;
    }

    @Override
    protected void restoreFragment(Fragment fragment) {
        mFragment = (PriceTagFragment) fragment;
    }

    @Override
    public void onBackPressed() {
        showDiscardNewPriceDialog();
    }

    private void showDiscardNewPriceDialog() {
        MaterialDialog.Builder dialogBuilder = new MaterialDialog.Builder(this);
        dialogBuilder.title(R.string.alert_title_warning);
        dialogBuilder.content(R.string.discard_new_price);
        dialogBuilder.negativeText(R.string.alert_button_cancel);
        dialogBuilder.positiveText(R.string.alert_button_discard);
        dialogBuilder.onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                PriceTagActivity.super.onBackPressed();
            }
        });
        dialogBuilder.build().show();
    }
}
