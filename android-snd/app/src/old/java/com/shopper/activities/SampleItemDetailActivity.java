package com.shopper.activities;

import android.app.Fragment;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.MenuItem;

import com.happyfresh.fulfillment.R;
import com.shopper.fragments.SampleItemDetailFragment;
import com.shopper.utils.AppUtils;

/**
 * Created by kharda on 9/16/16.
 */
public class SampleItemDetailActivity extends FragmentActivity {
    public static final String ITEM_ID = ".shopper.fragments.SampleItemDetailFragment.ITEM_ID";
    public static final String ORDER_NUMBER = ".shopper.fragments.SampleItemDetailFragment.ORDER_NUMBER";
    public static final String SHIPMENT_ID = ".shopper.fragments.SampleItemDetailFragment.SHIPMENT_ID";

    Long mShipmentId;
    Long mItemId;
    String mItemOrderNumber;

    SampleItemDetailFragment mDetailFragment;

    @Override
    protected Fragment createFragment() {
        mShipmentId = getIntent().getLongExtra(SHIPMENT_ID, 0);
        mItemId = getIntent().getLongExtra(ITEM_ID, 0);
        mItemOrderNumber = getIntent().getStringExtra(ORDER_NUMBER);

        mDetailFragment = SampleItemDetailFragment.newInstance(mShipmentId, mItemId, mItemOrderNumber);
        return mDetailFragment;
    }

    @Override
    protected void restoreFragment(Fragment fragment) {
        mDetailFragment = (SampleItemDetailFragment) fragment;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (AppUtils.currentUserType() == AppUtils.UserType.RANGER) {
            setTheme(R.style.HappyFreshTheme_Rangers_Transparent);
        } else {
            setTheme(R.style.HappyFreshTheme_Shoppers_Transparent);
        }

        setContentView(getLayoutResId());
        setupToolbar();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            Drawable closeButton = ContextCompat.getDrawable(this, R.drawable.icon_clear);
            assert closeButton != null;

            closeButton.setColorFilter(ContextCompat.getColor(this, android.R.color.darker_gray), PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(closeButton);
            getSupportActionBar().setTitle(null);
        }

        if (savedInstanceState != null) {
            mShipmentId = savedInstanceState.getLong(SHIPMENT_ID, 0);
            mItemId = savedInstanceState.getLong(ITEM_ID, 0);
        }
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_fragment_transparent;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putLong(SHIPMENT_ID, mShipmentId);
        outState.putLong(ITEM_ID, mItemId);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    public void finish() {
        setResult(RESULT_OK);
        super.finish();
        overridePendingTransition(R.anim.stay_animation, R.anim.slide_out_to_bottom);
    }
}
