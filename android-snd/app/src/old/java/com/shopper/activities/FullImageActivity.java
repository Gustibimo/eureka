package com.shopper.activities;

import android.app.Fragment;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.view.MenuItem;
import android.view.Window;

import com.happyfresh.fulfillment.R;
import com.shopper.fragments.FullImageFragment;

/**
 * Created by rsavianto on 12/6/14.
 */
public class FullImageActivity extends FragmentActivity {

    public static String PRODUCT_IMAGES = "com.shopper.activities.FullImageActivity.PRODUCT_IMAGES";
    public static String IMAGE_POSITION = "com.shopper.activities.FullImageActivity.IMAGE_POSITION";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.HappyFreshTheme_Transparent);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);

        super.onCreate(savedInstanceState);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            Drawable closeButton = ContextCompat.getDrawable(getBaseContext(), R.drawable.icon_clear);
            closeButton.setColorFilter(ContextCompat.getColor(getBaseContext(), android.R.color.darker_gray), PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(closeButton);

            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getSupportActionBar().setTitle(null);

            getSupportActionBar().setStackedBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected Fragment createFragment() {
        Parcelable imagesParcel = getIntent().getParcelableExtra(PRODUCT_IMAGES);

        Bundle args = new Bundle();
        args.putParcelable(PRODUCT_IMAGES, imagesParcel);
        args.putInt(IMAGE_POSITION, 0);

        Fragment fragment = new FullImageFragment();
        fragment.setArguments(args);

        return fragment;
    }
}
