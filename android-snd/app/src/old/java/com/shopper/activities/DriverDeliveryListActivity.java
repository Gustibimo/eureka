package com.shopper.activities;

import android.app.Fragment;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.happyfresh.fulfillment.R;
import com.shopper.fragments.DriverDeliveryListFragment;

/**
 * Created by ifranseda on 4/27/15.
 */
public class DriverDeliveryListActivity extends FragmentActivity {
    private DriverDeliveryListFragment mFragment;

    @Override
    protected Fragment createFragment() {
        mFragment = DriverDeliveryListFragment.newInstance();
        return mFragment;
    }

    @Override
    protected void restoreFragment(Fragment fragment) {
        mFragment = (DriverDeliveryListFragment) fragment;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.driver, menu);

        MenuItem refresh = menu.findItem(R.id.action_refresh_job);
        refresh.setVisible(true);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_refresh_job) {
            mFragment.reload();
        } else {
            super.onOptionsItemSelected(item);
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(this, getString(R.string.must_finalize_rejection), Toast.LENGTH_LONG).show();
        return;
    }
}
