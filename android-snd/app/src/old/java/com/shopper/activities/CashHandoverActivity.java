package com.shopper.activities;

import android.app.Fragment;

import com.shopper.fragments.CashHandoverFragment;

/**
 * Created by ifranseda on 5/12/15.
 */
public class CashHandoverActivity extends FragmentActivity {
    @Override
    protected Fragment createFragment() {
        return new CashHandoverFragment();
    }
}