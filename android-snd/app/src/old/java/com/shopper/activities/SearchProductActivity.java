package com.shopper.activities;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;

import com.happyfresh.snowflakes.hoverfly.models.LineItem;
import com.shopper.common.Constant;
import com.shopper.fragments.SearchProductFragment;

import org.parceler.Parcels;

/**
 * Created by julianagalag on 5/6/15.
 */
public class SearchProductActivity extends FragmentActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(null);
        }
    }

    @Override
    protected Fragment createFragment() {
        Long shipmentId = getIntent().getExtras().getLong(Constant.SHIPMENT_ID);
        LineItem lineItem = Parcels.unwrap(getIntent().getExtras().getParcelable(SearchProductFragment.LINE_ITEM));
        return SearchProductFragment.newInstance(shipmentId, lineItem);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        setResult(Activity.RESULT_OK);
        finish();
    }
}