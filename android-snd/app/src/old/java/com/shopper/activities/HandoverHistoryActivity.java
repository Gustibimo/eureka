package com.shopper.activities;

import android.app.Fragment;
import android.view.MenuItem;

import com.shopper.fragments.HandoverHistoryFragment;

/**
 * Created by ifranseda on 5/18/15.
 */
public class HandoverHistoryActivity extends FragmentActivity {
    @Override
    protected Fragment createFragment() {
        return new HandoverHistoryFragment();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
        }

        return true;
    }
}
