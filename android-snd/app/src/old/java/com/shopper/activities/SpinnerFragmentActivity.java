package com.shopper.activities;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;

import com.happyfresh.fulfillment.R;

/**
 * Created by ifranseda on 11/18/14.
 */
public abstract class SpinnerFragmentActivity extends BaseActivity {
    protected abstract void actionBarListModeNavigation(int position);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        setNavigationModeList();
    }

    protected void setNavigationModeList() {
        // Default value for spinner item: android.R.layout.simple_spinner_dropdown_item
        SpinnerAdapter mSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.action_menu, R.layout.layout_spinner_menu);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);

//        getSupportActionBar().setListNavigationCallbacks(mSpinnerAdapter, new ActionBar.OnNavigationListener() {
//            @Override
//            public boolean onNavigationItemSelected(int position, long itemId) {
//                actionBarListModeNavigation(position);
//                return true;
//            }
//        });
    }

    protected String getSpinnerMenu(int position) {
        String[] strings = getResources().getStringArray(R.array.action_menu);
        return strings[position];
    }
}
