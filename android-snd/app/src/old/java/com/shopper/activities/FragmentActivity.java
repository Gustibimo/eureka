package com.shopper.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.Shipment;
import com.happyfresh.snowflakes.hoverfly.utils.PermissionUtils;
import com.shopper.common.ShopperApplication;
import com.shopper.listeners.NetworkQueueChangedListener;
import com.shopper.utils.LogUtils;

/**
 * Created by ifranseda on 11/14/14.
 */
public abstract class FragmentActivity extends BaseActivity {
    protected View fragmentPendingQueue;
    protected TextView pendingQueueText;
    protected Button editQueueButton;

    protected Fragment createFragment() {
        return null;
    }

    protected void restoreFragment(Fragment fragment) {
    }

    protected int getLayoutResId() {
        return R.layout.activity_fragment;
    }

    protected boolean isQueueListener() {
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId());
        setupToolbar();

        fragmentPendingQueue = findViewById(R.id.fragment_pending_queue);

        pendingQueueText = (TextView) findViewById(R.id.queue_text);
        editQueueButton = (Button) findViewById(R.id.edit_queue_button);
        editQueueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                retrySyncTask();
            }
        });

        if (savedInstanceState == null) {
            FragmentManager fm = getFragmentManager();
            Fragment fragment = fm.findFragmentById(R.id.fragment_container);
            if (fragment == null) {
                fragment = createFragment();

                if (fragment != null) {
                    fm.beginTransaction()
                            .add(R.id.fragment_container, fragment)
                            .commit();
                }
            } else {
                restoreFragment(fragment);
            }
        } else {
            FragmentManager fm = getFragmentManager();
            Fragment fragment = fm.findFragmentById(R.id.fragment_container);
            if (fragment != null) {
                restoreFragment(fragment);
            } else {
                fragment = createFragment();
                if (fragment != null) {
                    fm.beginTransaction()
                            .add(R.id.fragment_container, fragment)
                            .commit();
                }
            }
        }
    }

    public void callCustomer(final Shipment shipment) {
        MaterialDialog.Builder inputDialog = new MaterialDialog.Builder(this);
        inputDialog.title(R.string.alert_title_call_customer);

        final String phoneNumber = shipment.getAddress().getPhoneNumber();
        String message = getString(R.string.alert_message_call_customer, shipment.getRecipientName(), phoneNumber);
        inputDialog.content(Html.fromHtml(message));

        inputDialog.negativeText(R.string.alert_button_cancel);
        inputDialog.positiveText(R.string.alert_button_call);
        inputDialog.callback(new MaterialDialog.ButtonCallback() {
            @Override
            public void onPositive(MaterialDialog dialog) {
                if (PermissionUtils.callPhone(FragmentActivity.this)) {
                    ShopperApplication.getInstance().callPhone(phoneNumber);
                } else {
                    PermissionUtils.requestPermissionIfNeeded(FragmentActivity.this);
                }
            }
        });

        inputDialog.show();
    }

    public void callCustomer() {
        final Shipment shipment = ShopperApplication.getInstance().getBatch().activeShipment();
        callCustomer(shipment);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (isQueueListener()) {
            ShopperApplication app = ShopperApplication.getInstance();
            app.registerQueueChangedListener(queueChangedListener);
            app.raiseQueueChanged(app.getTaskQueue().size());
        }
    }

    @Override
    protected void onPause() {
        if (isQueueListener()) {
            ShopperApplication.getInstance().unregisterQueueChangedListener(queueChangedListener);
        }

        super.onPause();
    }

    NetworkQueueChangedListener queueChangedListener = new NetworkQueueChangedListener() {
        @Override
        public void onUpdated(final int size) {
            if (size == 0) {
                fragmentPendingQueue.setVisibility(View.GONE);
            } else {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        fragmentPendingQueue.setVisibility(View.VISIBLE);
                        pendingQueueText.setText(size + " " + getString(R.string.sync_task_pending));
                    }
                });
            }
        }
    };

    private void retrySyncTask() {
        LogUtils.logEvent("Retry sync task");
        ShopperApplication.getInstance().getTaskQueue().startService();
    }
}