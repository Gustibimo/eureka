package com.shopper.activities;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.Menu;
import android.view.MenuItem;

import com.happyfresh.fulfillment.R;
import com.shopper.common.Constant;
import com.shopper.fragments.DriverListFragment;

/**
 * Created by ifranseda on 7/8/15.
 */
public class DriverListActivity extends FragmentActivity {
    private DriverListFragment mFragment;

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            getFragment().updateBatches();
        }
    };

    @Override
    protected Fragment createFragment() {
        mFragment = DriverListFragment.newInstance();

        Bundle args = new Bundle();
        long stockLocationId = getIntent().getLongExtra(Constant.STOCK_LOCATION_ID, 0);
        if (stockLocationId != 0) {
            args.putLong(Constant.STOCK_LOCATION_ID, stockLocationId);
        }
        mFragment.setArguments(args);

        return mFragment;
    }

    private DriverListFragment getFragment() {
        if (mFragment == null) {
            mFragment = DriverListFragment.newInstance();
        }

        return mFragment;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String stockLocName = getIntent().getStringExtra(Constant.STOCK_LOCATION_NAME);
        if (stockLocName == null) {
            stockLocName = getString(R.string.action_jobs);
        }

        setTitle(stockLocName);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mFragment != null) {
            mFragment.checkPauseStatus();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        requestPermissionToDrawOverlay();
        LocalBroadcastManager.getInstance(this).registerReceiver(mBroadcastReceiver, new IntentFilter(Constant.UPDATE_JOBS));
    }

    @Override
    protected void onPause() {
        super.onPause();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.driver, menu);

        menu.findItem(R.id.action_refresh_job).setVisible(true);
        menu.findItem(R.id.action_settings).setVisible(true);
        menu.findItem(R.id.action_logout).setVisible(true);
        menu.findItem(R.id.action_handover).setVisible(true);
        menu.findItem(R.id.action_clock_out).setVisible(true);
        menu.findItem(R.id.action_pause).setVisible(true);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_refresh_job) {
            getFragment().updateBatches();
        } else if (item.getItemId() == R.id.action_handover) {
            getFragment().openHandover();
        } else if (item.getItemId() == R.id.action_clock_out) {
            getFragment().showClockPause(Constant.MENU_CLOCK_OUT);
        } else if (item.getItemId() == R.id.action_pause) {
            getFragment().showClockPause(Constant.MENU_PAUSE);
        } else {
            super.onOptionsItemSelected(item);
        }
        return true;
    }

    public void checkUpdateIfAvailable() {
        super.checkUpdate();
    }
}
