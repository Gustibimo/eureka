package com.shopper.activities;

import android.app.Fragment;
import android.widget.Toast;

import com.happyfresh.fulfillment.R;
import com.shopper.fragments.DriverOneClickPaymentFragment;

/**
 * Created by kharda on 10/24/16.
 */

public class DriverOneClickPaymentActivity extends FragmentActivity {

    private DriverOneClickPaymentFragment mFragment;

    @Override
    protected Fragment createFragment() {
        mFragment = DriverOneClickPaymentFragment.newInstance();
        return mFragment;
    }

    @Override
    protected void restoreFragment(Fragment fragment) {
        mFragment = (DriverOneClickPaymentFragment) fragment;
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(this, getString(R.string.must_finalize_payment), Toast.LENGTH_LONG).show();
        return;
    }
}
