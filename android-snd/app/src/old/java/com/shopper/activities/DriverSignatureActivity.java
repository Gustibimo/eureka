package com.shopper.activities;

import android.app.Fragment;

import com.shopper.fragments.DriverSignatureFragment;

/**
 * Created by rsavianto on 12/24/14.
 */
public class DriverSignatureActivity extends FragmentActivity {

    @Override
    protected Fragment createFragment() {
        return DriverSignatureFragment.newInstance();
    }
}
