package com.shopper.activities;

import android.app.Fragment;

import com.shopper.common.Constant;
import com.shopper.common.ShopperApplication;
import com.shopper.fragments.HandoverSubmissionFragment;
import com.shopper.utils.SharedPrefUtils;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by ifranseda on 5/18/15.
 */
public class HandoverSubmissionActivity extends FragmentActivity {
    @Override
    protected Fragment createFragment() {
        String countryCode = SharedPrefUtils.getString(ShopperApplication.getContext(),
                Constant.STOCK_LOCATION_COUNTRY_ISO);
        Locale locale = ShopperApplication.getInstance().getLocaleByCountryCode(countryCode);
        NumberFormat numberFormatter = NumberFormat.getCurrencyInstance(locale);
        String currencyCode = numberFormatter.getCurrency().getCurrencyCode();

        double amount = getIntent().getExtras().getDouble(Constant.CASH_HANDOVER_AMOUNT, 0);
        String currency = getIntent().getExtras().getString(Constant.CASH_HANDOVER_CURRENCY, currencyCode);
        return HandoverSubmissionFragment.newInstance(amount, currency);
    }
}
