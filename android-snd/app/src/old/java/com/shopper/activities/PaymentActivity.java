package com.shopper.activities;

import android.app.Fragment;
import android.os.Bundle;
import android.view.MenuItem;

import com.happyfresh.fulfillment.R;
import com.shopper.fragments.PaymentFragment;

/**
 * Created by ifranseda on 12/23/14.
 */
public class PaymentActivity extends FragmentActivity {

    @Override
    protected Fragment createFragment() {
        Fragment fragment = new PaymentFragment();

        Long shipmentId = getIntent().getExtras().getLong(PaymentFragment.SHIPMENT_ID);
        Long batchId = getIntent().getExtras().getLong(PaymentFragment.BATCH_ID);
        Long stockLocationId = getIntent().getExtras().getLong(PaymentFragment.STOCK_LOCATION_ID);

        Bundle bundle = new Bundle();
        bundle.putLong(PaymentFragment.SHIPMENT_ID, shipmentId);
        bundle.putLong(PaymentFragment.BATCH_ID, batchId);
        bundle.putLong(PaymentFragment.STOCK_LOCATION_ID, stockLocationId);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    protected boolean isQueueListener() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle(R.string.payment_screen_title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_material);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}