package com.shopper.activities;

import android.app.Fragment;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.MenuItem;
import android.view.WindowManager;

import com.happyfresh.fulfillment.R;
import com.shopper.common.Constant;
import com.shopper.fragments.BatchAssignmentFragment;

/**
 * Created by ifranseda on 11/13/15.
 */
public class DriverBatchAssignmentActivity extends FragmentActivity {
    BatchAssignmentFragment mFragment;

    private Long batchId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            Drawable closeButton = ContextCompat.getDrawable(getBaseContext(), R.drawable.icon_clear);
            closeButton.setColorFilter(ContextCompat.getColor(getBaseContext(), android.R.color.white), PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(closeButton);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nm.cancelAll();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mFragment.skipAssignment(batchId);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected Fragment createFragment() {
        batchId = getIntent().getLongExtra(Constant.EXTRAS_BATCH_ID, -1);
        mFragment = BatchAssignmentFragment.newInstance(batchId);
        return mFragment;
    }

    @Override
    public void onBackPressed() {
    }
}
