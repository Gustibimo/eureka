package com.shopper.activities;

import android.app.Fragment;
import android.os.Bundle;
import android.view.MenuItem;

import com.happyfresh.fulfillment.R;
import com.shopper.fragments.PaymentReceiptFragment;
import com.happyfresh.snowflakes.hoverfly.models.ReceiptItem;

import java.util.ArrayList;

public class PaymentReceiptActivity extends FragmentActivity {
    public static final String RECEIPT_ITEM_LIST = "com.shopper.activities.PaymentReceiptActivity.RECEIPT_ITEM_LIST";
    public static final String SHIPMENT_ID = "com.shopper.activities.PaymentReceiptActivity.SHIPMENT_ID";
    public static final String STOCK_LOCATION_ID = "com.shopper.PaymentReceiptActivity.STOCK_LOCATION_ID";
    public static final String BATCH_ID = "com.shopper.PaymentReceiptActivity.BATCH_ID";

    @Override
    protected Fragment createFragment() {
        Fragment fragment = new PaymentReceiptFragment();

        ArrayList<ReceiptItem> receiptItemList = ReceiptItem.Companion.fromListParcel(getIntent().getExtras().getParcelableArrayList(RECEIPT_ITEM_LIST));
        Long shipmentId = getIntent().getExtras().getLong(SHIPMENT_ID);
        Long batchId = getIntent().getExtras().getLong(BATCH_ID);
        Long stockLocationId = getIntent().getExtras().getLong(STOCK_LOCATION_ID);

        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(RECEIPT_ITEM_LIST, ReceiptItem.Companion.toListParcel(receiptItemList));
        bundle.putLong(SHIPMENT_ID, shipmentId);
        bundle.putLong(BATCH_ID, batchId);
        bundle.putLong(STOCK_LOCATION_ID, stockLocationId);

        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    protected boolean isQueueListener() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle(R.string.payment_screen_title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // TODO: MERGE FROM release/1.30
//        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}