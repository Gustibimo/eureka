package com.shopper.activities;

import android.app.Fragment;
import android.os.Bundle;
import android.view.MenuItem;

import com.happyfresh.fulfillment.R;
import com.shopper.fragments.BatchPaymentFragment;

/**
 * Created by ifranseda on 9/15/15.
 */
public class BatchPaymentActivity extends FragmentActivity {

    @Override
    protected Fragment createFragment() {
        BatchPaymentFragment fragment = new BatchPaymentFragment();

        if (getIntent() != null) {
            fragment.setArguments(getIntent().getExtras());
        }

        return fragment;
    }

    @Override
    protected boolean isQueueListener() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle(R.string.payment);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_material);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
