package com.shopper.activities;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.Batch;
import com.happyfresh.snowflakes.hoverfly.models.BatchState;
import com.happyfresh.snowflakes.hoverfly.models.Job;
import com.happyfresh.snowflakes.hoverfly.models.Shipment;
import com.happyfresh.snowflakes.hoverfly.models.payload.DriverStatePayload;
import com.shopper.adapters.DriverShoppingListBatchAdapter;
import com.shopper.common.Constant;
import com.shopper.common.ShopperApplication;
import com.shopper.components.CircularProgressBar;
import com.shopper.fragments.DriverAcceptanceFragment;
import com.shopper.listeners.DataListener;
import com.shopper.utils.ActivityUtils;
import com.shopper.utils.LogUtils;
import com.shopper.views.NonSwipeViewPager;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by rsavianto on 12/24/14.
 */
public class DriverAcceptanceActivity extends FragmentActivity implements ViewPager.OnPageChangeListener {
    public static boolean isAccepting;

    @BindView(R.id.progress_container)
    View mProgressContainer;

    @BindView(R.id.progress_indicator)
    CircularProgressBar mProgressBar;

    @BindView(R.id.progress_label)
    TextView mProgressLabel;

    @BindView(R.id.fragment_viewpager)
    NonSwipeViewPager viewPager;

    @BindView(R.id.accept_button)
    Button acceptButton;

    @BindView(R.id.prev_button)
    Button prevButton;

    @BindView(R.id.next_button)
    Button nextButton;

    private DriverAcceptanceFragment mFragment;

    private DriverShoppingListBatchAdapter mAdapter;

    private List<Shipment> mShipments = new ArrayList<>();

    private int mCurrentPosition = 0;

    private Unbinder unbinder;

    @Override
    protected Fragment createFragment() {
        mFragment = new DriverAcceptanceFragment();
        return mFragment;
    }

    @Override
    protected void restoreFragment(Fragment fragment) {
        mFragment = (DriverAcceptanceFragment) fragment;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_fragment_viewpager;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        unbinder = ButterKnife.bind(this);

        mAdapter = new DriverShoppingListBatchAdapter(getFragmentManager(), getBaseContext(), mShipments);
        viewPager.setAdapter(mAdapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    protected void onResume() {
        super.onResume();

        isAccepting = false;

        viewPager.addOnPageChangeListener(this);
        viewPager.addOnPageChangeListener(mFragment);
        viewPager.setSwipeable(true);

        reloadData();

        updateNavigationButton();
    }

    private void reloadData() {
        Batch batch = ShopperApplication.getInstance().getBatch();
        mShipments.clear();
        mAdapter.notifyDataSetChanged();

        mShipments.addAll(batch.shipments());
        mAdapter.notifyDataSetChanged();

        mFragment.setShipmentList(mShipments);
    }

    @Override
    protected void onPause() {
        super.onPause();

        isAccepting = false;

        viewPager.removeOnPageChangeListener(this);
        viewPager.removeOnPageChangeListener(mFragment);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.driver, menu);

        MenuItem refresh = menu.findItem(R.id.action_refresh_job);
        refresh.setVisible(false);

        MenuItem cancel = menu.findItem(R.id.action_cancel_book);
        cancel.setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_cancel_book) {
            cancelDialog();
        }

        return true;
    }

    @OnClick(R.id.prev_button)
    void prevButton() {
        --mCurrentPosition;
        if (mCurrentPosition < 1) {
            mCurrentPosition = 0;
        }

        viewPager.setCurrentItem(mCurrentPosition);
    }

    @OnClick(R.id.next_button)
    void nextButton() {
        ++mCurrentPosition;
        if (mCurrentPosition >= mShipments.size() - 1) {
            mCurrentPosition = mShipments.size() - 1;
        }

        viewPager.setCurrentItem(mCurrentPosition);
    }

    public void cancelDialog() {
        MaterialDialog.Builder inputDialog = new MaterialDialog.Builder(this);
        inputDialog.title(R.string.alert_title_warning);

        inputDialog.content(R.string.alert_message_cancel);
        inputDialog.negativeText(R.string.alert_button_no);
        inputDialog.positiveText(R.string.alert_button_yes);
        inputDialog.callback(new MaterialDialog.ButtonCallback() {
            @Override
            public void onPositive(MaterialDialog dialog) {
                cancelJob();
            }
        });

        inputDialog.show();
    }

    public void cancelJob() {
        LogUtils.logEvent("Cancel Booking", "Initiate cancel request");

        mProgressContainer.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.VISIBLE);
        mProgressLabel.setText(R.string.cancelling_job);

        final ShopperApplication app = ShopperApplication.getInstance();
        app.clearQueue();

        Long batchId = app.getBatch().getRemoteId();
        ShopperApplication.getInstance().getBatchManager().cancel(batchId, new DataListener<Batch>() {
            @Override
            public void onCompleted(boolean success, Batch data) {
                if (data != null) {
                    app.setBatch(null);
                    driverListScreen();

                    if (success) {
                        LogUtils.logEvent("Cancel delivery", "Cancelled successfully");
                    }
                } else {
                    if (!success) {
                        Toast.makeText(DriverAcceptanceActivity.this,
                                getString(R.string.toast_cannot_cancel_without_internet), Toast.LENGTH_LONG).show();
                    }
                }

                mProgressContainer.setVisibility(View.GONE);
            }
        });
    }

    @OnClick(R.id.accept_button)
    void acceptJob() {
        isAccepting = true;

        acceptButton.setText(getString(R.string.driver_waiting));
        acceptButton.setEnabled(false);
        prevButton.setEnabled(false);
        nextButton.setEnabled(false);

        viewPager.setSwipeable(false);

        ShopperApplication application = ShopperApplication.getInstance();
        Long batchId = application.getBatch().getRemoteId();

        try {
            Shipment currentShipment = mShipments.get(mCurrentPosition);
            Long shipmentId = currentShipment.getRemoteId();

            final String state = Job.Progress.getACCEPTED();
            DriverStatePayload driverStatePayload = new DriverStatePayload();
            driverStatePayload.setState(state);

            application.getBatchManager().accept(batchId, shipmentId, new DataListener() {
                @Override
                public void onCompleted(boolean success, Object data) {
                    isAccepting = false;
                    gotoDriverBatchDeliveryActivity();

                    if (viewPager != null) {
                        viewPager.setSwipeable(true);
                    }
                }

                @Override
                public void onFailed(Throwable exception) {
                    if (Constant.OrderCanceledException.equalsIgnoreCase(exception.getMessage())) {
                        showOrderCanceledDialog(Constant.CanceledDialogType.ACCEPT);
                    }

                    isAccepting = false;

                    if (viewPager != null) {
                        viewPager.setSwipeable(true);
                    }
                }
            });

        } catch (IndexOutOfBoundsException ie) {
            reloadData();
        }
    }

    private void updateNavigationButton() {
        prevButton.setEnabled(true);
        nextButton.setEnabled(true);

        if (mCurrentPosition == 0) {
            prevButton.setEnabled(false);
            nextButton.setEnabled(true);
        } else if (mCurrentPosition == mShipments.size() - 1) {
            prevButton.setEnabled(true);
            nextButton.setEnabled(false);
        }

        if (mShipments.size() <= 1) {
            prevButton.setEnabled(false);
            nextButton.setEnabled(false);
        }

        if (mShipments.size() == 0) {
            return;
        }
        Shipment shipment = mShipments.get(mCurrentPosition);
        shipment.load();
        if (shipment.getDeliveryJob() != null && shipment.getDeliveryJob().isAccepted()) {
            acceptButton.setEnabled(false);
            acceptButton.setText(getString(R.string.driver_accepted));
        } else {
            acceptButton.setEnabled(true);
            acceptButton.setText(getString(R.string.driver_accept));
        }
    }

    private void gotoDriverBatchDeliveryActivity() {
        updateNavigationButton();
        mFragment.update(viewPager.getCurrentItem());

        Batch batch = ShopperApplication.getInstance().getBatch();
        if (batch.state().equals(BatchState.State.DELIVERING)) {
            ActivityUtils.startClearTop(this, DriverBatchDeliveryActivity.class);
        }
    }

    private void driverListScreen() {
        ActivityUtils.startClearTop(this, DriverListActivity.class);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        mCurrentPosition = position;
        updateNavigationButton();
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }
}
