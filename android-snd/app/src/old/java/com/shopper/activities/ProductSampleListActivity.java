package com.shopper.activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.Batch;
import com.happyfresh.snowflakes.hoverfly.models.StockLocation;
import com.shopper.adapters.ProductSamplePagerAdapter;
import com.shopper.common.Constant;
import com.shopper.common.ShopperApplication;
import com.shopper.events.ShopNowEvent;
import com.shopper.interfaces.OnCreateOptionMenuListener;
import com.shopper.utils.AppUtils;
import com.shopper.utils.LogUtils;
import com.shopper.utils.SharedPrefUtils;
import com.shopper.utils.ViewUtils;
import com.shopper.views.CollapsibleImageView;
import com.shopper.views.NonSwipeViewPager;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.shopper.common.Constant.SELECTED_SHOPPING_STORE;

/**
 * Created by kharda on 9/7/16.
 */
public class ProductSampleListActivity extends FragmentActivity {

    public static final String SELECTED_BATCH = "ProductSampleListActivity.SELECTED_SHIPMENT_ID";

    public static final String SHIPMENT_TIMER = "ProductSampleListActivity.SHIPMENT_TIMER";

    @BindView(R.id.toolbar_image)
    CollapsibleImageView toolbarImageView;

    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;

    @BindView(R.id.store_name)
    TextView storeNameTextView;

    @BindView(R.id.store_address)
    TextView storeAddressTextView;

    @BindView(R.id.shop_now_button)
    Button shopNowButton;

    @BindView(R.id.navigation_container)
    View navigationContainer;

    @BindView(R.id.prev_button)
    Button prevButton;

    @BindView(R.id.next_button)
    Button nextButton;

    @BindView(R.id.fragment_viewpager)
    NonSwipeViewPager viewPager;

    @BindView(R.id.view_pager_indicator)
    CirclePageIndicator viewPagerIndicator;

    @BindView(R.id.progress_container)
    View progressContainer;

    //Timer
    private long startTime = 0;

    private Handler timerHandler = new Handler();

    private int mCurrentPosition = 0;

    private long mSelectedStore;

    private long mBatchId;

    private Batch mBatch;

    private View mProgressBar;

    private TextView mTimerTextView;

    Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {
            long millis = System.currentTimeMillis() - startTime;
            int seconds = (int) (millis / 1000);
            int minutes = seconds / 60;
            seconds = seconds % 60;

            mTimerTextView.setText(String.format("%d:%02d", minutes, seconds));

            timerHandler.postDelayed(this, 500);
        }
    };

    private List<StockLocation> mStockLocations = new ArrayList<>();

    private OnCreateOptionMenuListener listener;

    private ProductSamplePagerAdapter mAdapter;

    private boolean mIsOutOfStore = false;

    private Unbinder unbinder;

    @Override
    protected boolean isQueueListener() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        unbinder = ButterKnife.bind(this);

        if (AppUtils.currentUserType() == AppUtils.UserType.RANGER) {
            setTheme(R.style.HappyFreshTheme_Rangers_Transparent);
        } else {
            setTheme(R.style.HappyFreshTheme_Shoppers_Transparent);
        }

        setupToolbar();

        collapsingToolbarLayout.setExpandedTitleColor(ContextCompat.getColor(this, android.R.color.transparent));
        collapsingToolbarLayout.setCollapsedTitleTextColor(ContextCompat.getColor(this, android.R.color.white));

        if (savedInstanceState != null) {
            mBatchId = savedInstanceState.getLong(SELECTED_BATCH);
            mSelectedStore = savedInstanceState.getLong(SELECTED_SHOPPING_STORE);
            mIsOutOfStore = savedInstanceState.getBoolean(Constant.IS_OUT_OF_STORE);
        } else {
            if (getIntent().getExtras() != null) {
                mBatchId = getIntent().getExtras().getLong(SELECTED_BATCH);
                mIsOutOfStore = getIntent().getExtras().getBoolean(Constant.IS_OUT_OF_STORE, false);
            }

            mSelectedStore = SharedPrefUtils.getLong(getBaseContext(), SELECTED_SHOPPING_STORE);
        }

        if (mIsOutOfStore) {
            setTitle(R.string.product_sample_list_out_of_store);
        } else {
            setTitle(R.string.product_sample_list_in_store);
        }

        if (mBatchId != 0) {
            mBatch = Batch.findById(mBatchId);
        } else {
            mBatch = ShopperApplication.getInstance().getBatch();
        }
        mStockLocations = mBatch.allUniqueStockLocations();
        mAdapter = new ProductSamplePagerAdapter(getFragmentManager(), mStockLocations);
        viewPager.setAdapter(mAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                mCurrentPosition = position;
                setHeaderContent(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        if (mSelectedStore > 0) {
            int i = 0;
            for (StockLocation stockLocation : mStockLocations) {
                if (stockLocation.getRemoteId() == mSelectedStore) {
                    mSelectedStore = stockLocation.getRemoteId();
                    mCurrentPosition = i;
                    break;
                }
                i++;
            }
            setHasActiveStore();
        }

        setHeaderContent(mCurrentPosition);
        viewPager.setCurrentItem(mCurrentPosition);
        viewPagerIndicator.setViewPager(viewPager);
        updateShoppingListViewState();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putLong(SELECTED_BATCH, mBatchId);
        outState.putLong(SELECTED_SHOPPING_STORE, mSelectedStore);
        outState.putBoolean(Constant.IS_OUT_OF_STORE, mIsOutOfStore);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_shopping_list;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();

        pauseTimer();
    }

    public void setTitle(int resId) {
        if (getSupportActionBar() != null) {
            collapsingToolbarLayout.setTitle(getString(resId));
        }
    }

    private void setHeaderContent(int position) {
        try {
            StockLocation stockLocation = mStockLocations.get(position);
            String url = "";

            if (stockLocation != null) {
                storeNameTextView.setText(stockLocation.getName());
                storeAddressTextView.setText(stockLocation.getAddress1() + "\n" + stockLocation.getAddress2());

                url = stockLocation.getStorePhoto();
            } else {
                storeNameTextView.setText(null);
                storeAddressTextView.setText(null);
            }

            updateNavigationButton();

            Picasso.with(this).cancelRequest(toolbarImageView);
            Picasso.with(this).load(url).into(toolbarImageView, new Callback() {
                @Override
                public void onSuccess() {
                    applyToolbarPalette();
                }

                @Override
                public void onError() {
                }
            });

        } catch (IndexOutOfBoundsException ie) {
            updateNavigationButton();
        }
    }

    @OnClick(R.id.prev_button)
    void previousStore() {
        --mCurrentPosition;
        setHeaderContent(mCurrentPosition);
        viewPager.setCurrentItem(mCurrentPosition);
    }

    @OnClick(R.id.next_button)
    void nextStore() {
        ++mCurrentPosition;
        setHeaderContent(mCurrentPosition);
        viewPager.setCurrentItem(mCurrentPosition);
    }

    void updateNavigationButton() {
        prevButton.setVisibility(View.VISIBLE);
        nextButton.setVisibility(View.VISIBLE);

        if (mCurrentPosition == 0) {
            prevButton.setVisibility(View.GONE);
            nextButton.setVisibility(View.VISIBLE);
        } else if (mCurrentPosition == mStockLocations.size() - 1) {
            prevButton.setVisibility(View.VISIBLE);
            nextButton.setVisibility(View.GONE);
        }

        if (mStockLocations.size() <= 1) {
            prevButton.setVisibility(View.GONE);
            nextButton.setVisibility(View.GONE);
        }
    }

    private void applyToolbarPalette() {
        int color = ContextCompat.getColor(this, R.color.happyfresh_green);
        if (AppUtils.currentUserType() == AppUtils.UserType.RANGER) {
            color = ContextCompat.getColor(this, R.color.happyfresh_rangers);
        }

        collapsingToolbarLayout.setContentScrimColor(color);
        collapsingToolbarLayout.setStatusBarScrimColor(color);
    }

    @OnClick(R.id.shop_now_button)
    void shopNow() {
        StockLocation stockLocation = mStockLocations.get(mCurrentPosition);
        mSelectedStore = stockLocation.getRemoteId();
        SharedPrefUtils.writeLong(getBaseContext(), SELECTED_SHOPPING_STORE, mSelectedStore);

        setHasActiveStore();
    }

    void setHasActiveStore() {
        viewPager.setSwipeable(false);
        viewPagerIndicator.setVisibility(View.GONE);
        navigationContainer.setVisibility(View.GONE);
        shopNowButton.setVisibility(View.GONE);

        updateShoppingListViewState();
    }

    void updateShoppingListViewState() {
        ShopNowEvent event = new ShopNowEvent(false);

        if (SharedPrefUtils.getLong(getBaseContext(), SELECTED_SHOPPING_STORE) > 0) {
            viewPager.setAlpha(1f);
            event = new ShopNowEvent(true);
        } else {
            viewPager.setAlpha(0.5f);
        }
        ShopperApplication.getInstance().getBus().post(event);
    }

    public void setupShoppingTimer(Menu menu) {
        View container = LayoutInflater.from(this).inflate(R.layout.layout_shipment_timer, null);
        mTimerTextView = (TextView) container.findViewById(R.id.timer);
        mProgressBar = container.findViewById(R.id.progress_timer);

        ViewUtils.setVisibilityViews(View.GONE, mProgressBar, mTimerTextView);

        menu.add(Menu.NONE, Menu.NONE, Menu.FIRST, R.string.tab_items).setActionView(container)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        if (listener != null) {
            listener.onOptionMenuCreated();
        }
    }

    public void startTimer() {
        startTime = SharedPrefUtils.getLong(getApplicationContext(), SHIPMENT_TIMER);
        if (startTime == 0) {
            startTime = System.currentTimeMillis();
            SharedPrefUtils.writeLong(getApplicationContext(), SHIPMENT_TIMER, startTime);
        }

        timerHandler.postDelayed(timerRunnable, 0);
        ViewUtils.setVisibilityViews(View.VISIBLE, mProgressBar, mTimerTextView);
    }

    public void stopTimer() {
        SharedPrefUtils.remove(getApplicationContext(), SHIPMENT_TIMER);
        pauseTimer();
    }

    public void pauseTimer() {
        if (timerHandler != null) {
            ViewUtils.setVisibilityViews(View.GONE, mProgressBar, mTimerTextView);
            timerHandler.removeCallbacks(timerRunnable);
        }
    }

    public void cancel() {
        LogUtils.logEvent("Cancel Shopping", "Show cancel dialog");

        MaterialDialog.Builder inputDialog = new MaterialDialog.Builder(this);
        inputDialog.title(R.string.alert_title_warning);

        inputDialog.content(R.string.alert_message_cancel_shopping);
        inputDialog.negativeText(R.string.alert_button_no);
        inputDialog.positiveText(R.string.alert_button_yes);
        inputDialog.callback(new MaterialDialog.ButtonCallback() {
            @Override
            public void onPositive(MaterialDialog dialog) {
                //cancelShoppingBatch();
            }
        });

        inputDialog.show();
    }

    public void setOnCreateOptionMenuListener(OnCreateOptionMenuListener listener) {
        this.listener = listener;
    }

    @Override
    public void onBackPressed() {
        int backStackEntry = getFragmentManager().getBackStackEntryCount();
        if (backStackEntry == 0) {
            super.onBackPressed();
        } else {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getFragmentManager().popBackStackImmediate();
            setTitle(R.string.shopping_list);
        }
    }

    private void showProgress(boolean show) {
        progressContainer.setVisibility(show ? View.VISIBLE : View.GONE);
    }
}
