package com.shopper.activities;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.payload.FailDeliveryPayload;
import com.happyfresh.snowflakes.hoverfly.models.response.ExceptionResponse;
import com.shopper.common.Constant;
import com.shopper.common.ShopperApplication;
import com.shopper.fragments.DriverArriveFragment;
import com.shopper.interfaces.LocationRetrievedListener;
import com.shopper.listeners.DataListener;
import com.shopper.utils.LogUtils;

/**
 * Created by rsavianto on 12/24/14.
 */
public class DriverArriveActivity extends FragmentActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private DriverArriveFragment mDriverArriveFragment;
    private GoogleApiClient mGoogleApiClient;
    private LocationRetrievedListener mLocationRetrievedListener;

    private Runnable mRunnable;
    private Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        invalidateOptionsMenu();

        /* Check if google play service connected */
        servicesConnected();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    @Override
    protected Fragment createFragment() {
        mDriverArriveFragment = DriverArriveFragment.newInstance();

        return mDriverArriveFragment;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.driver, menu);

        MenuItem refreshItem = menu.findItem(R.id.action_refresh_job);
        refreshItem.setVisible(false);

        MenuItem failDeliveryItem = menu.findItem(R.id.action_fail_delivery);
        failDeliveryItem.setVisible(true);

        MenuItem callCustomerItem = menu.findItem(R.id.action_call_customer);
        callCustomerItem.setVisible(true);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_call_customer) {
            super.callCustomer();
        } else if (item.getItemId() == R.id.action_fail_delivery) {
            mDriverArriveFragment.showFailDelivery();
        } else {
            super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void showNotificationFailDelivery() {
        View view = LayoutInflater.from(this).inflate(R.layout.fail_delivery_layout, null);
        final RadioButton r1 = (RadioButton) view.findViewById(R.id.radio_incorrect_shipping_address);
        final RadioButton r2 = (RadioButton) view.findViewById(R.id.radio_cannot_reach_customer);
        final RadioButton r3 = (RadioButton) view.findViewById(R.id.radio_late);

        MaterialDialog.ButtonCallback buttonCallback = new MaterialDialog.ButtonCallback() {
            @Override
            public void onPositive(MaterialDialog dialog) {
                ShopperApplication application = ShopperApplication.getInstance();
                Long jobId = application.getCurrentShipment().getDeliveryJob().getRemoteId();

                FailDeliveryPayload payload = new FailDeliveryPayload();
                if (r1.isChecked()) {
                    payload.setReason(getString(R.string.fail_delivery_incorrect_shipping_address));
                } else if (r2.isChecked()) {
                    payload.setReason(getString(R.string.fail_delivery_cannot_reach_customer));
                } else if (r3.isChecked()) {
                    payload.setReason(getString(R.string.fail_delivery_late));
                }

                application.getJobManager().driverFailDeliver(jobId, payload, new DataListener() {
                    @Override
                    public void onCompleted(boolean success, Object data) {
                        if (success) {
                            doFinishDriverJob();
                        } else {
                            ExceptionResponse response = (ExceptionResponse) data;
                            if (response != null && response.getType().equalsIgnoreCase(Constant.AlreadyFinishedException)) {
                                doFinishDriverJob();
                            }
                        }
                    }
                });
            }
        };

        new MaterialDialog.Builder(this)
                .title(R.string.fail_delivery)
                .customView(view, false)
                .positiveText(R.string.alert_button_ok)
                .negativeText(R.string.alert_button_cancel)
                .callback(buttonCallback)
                .show();
    }

    void doFinishDriverJob() {
        // SET current shipment to nil
//        ShopperApplication.getInstance().setCurrentShipment(null);
        ShopperApplication.getInstance().setBatch(null);

        Intent intent = new Intent(this, DriverListActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    private boolean servicesConnected() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == resultCode) {
            LogUtils.LOG("Google Play services is available.");
            return true;
        } else {
            LogUtils.LOG("Cannot use Google Play services");
            return false;
        }
    }

    public void requestLocation(LocationRetrievedListener locationRetrievedListener) {
        mLocationRetrievedListener = locationRetrievedListener;

        mRunnable = new Runnable() {
            public void run() {
                if (mLocationRetrievedListener != null) {
                    mLocationRetrievedListener.onLocationNotRetrieved();
                    if (mGoogleApiClient.isConnected()) {
                        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, DriverArriveActivity.this);
                    }
                }
            }
        };

        mHandler.postDelayed(mRunnable, 10000L);

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(3600);
        locationRequest.setFastestInterval(60);

        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, this);
        }
    }

    public boolean checkLocationService() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) &&
                !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            return false;
        }

        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }

        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (checkLocationService()) {
            requestLocation(new LocationRetrievedListener() {
                @Override
                public void onLocationRetrieved(Location location) {
                    if (DriverArriveActivity.this == null) return;

                    Double lat = location.getLatitude();
                    Double lon = location.getLongitude();

                    LogUtils.logEvent("Current Location", lat + "," + lon);
                }

                @Override
                public void onLocationNotRetrieved() {
                    LogUtils.logEvent("Current Location", "Not available");
                }
            });
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        mHandler.removeCallbacks(mRunnable);
        if (mLocationRetrievedListener != null) {
            mLocationRetrievedListener.onLocationNotRetrieved();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mHandler.removeCallbacks(mRunnable);
        if (mLocationRetrievedListener != null) {
            mLocationRetrievedListener.onLocationRetrieved(location);
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            }
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        mHandler.removeCallbacks(mRunnable);
        if (mLocationRetrievedListener != null) {
            mLocationRetrievedListener.onLocationNotRetrieved();
        }
    }
}
