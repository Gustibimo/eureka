package com.shopper.activities;

import android.app.Fragment;

import com.shopper.fragments.DriverBatchDeliveryFragment;

/**
 * Created by ifranseda on 8/10/15.
 */
public class DriverBatchDeliveryActivity extends FragmentActivity {
    @Override
    protected Fragment createFragment() {
        return DriverBatchDeliveryFragment.newInstance();
    }
}