package com.shopper.activities;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.utils.PermissionUtils;
import com.shopper.fragments.DriverFinishFragment;
import com.shopper.interfaces.LocationRetrievedListener;
import com.shopper.utils.LocationUtils;
import com.shopper.utils.LogUtils;

/**
 * Created by rsavianto on 12/22/14.
 */
public class DriverFinishActivity extends FragmentActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private DriverFinishFragment mFragment;

    private GoogleApiClient mGoogleApiClient;
    private LocationRetrievedListener mLocationRetrievedListener;

    private Runnable mRunnable;
    private Handler mHandler = new Handler();

    @Override
    protected Fragment createFragment() {
        mFragment = DriverFinishFragment.newInstance();
        return mFragment;
    }

    @Override
    protected void restoreFragment(Fragment fragment) {
        mFragment = (DriverFinishFragment) fragment;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!LocationUtils.isLocationEnabled(this)) {
            showTurnOnLocationDialog();
        }

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.driver, menu);

        MenuItem refresh = menu.findItem(R.id.action_refresh_job);
        refresh.setVisible(false);

        MenuItem finalizeItem = menu.findItem(R.id.action_finalize_item);
        finalizeItem.setVisible(true);

        MenuItem callCustomerItem = menu.findItem(R.id.action_call_customer);
        callCustomerItem.setVisible(true);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_call_customer) {
            super.callCustomer();
        } else if (item.getItemId() == R.id.action_finalize_item) {
            mFragment.openDeliveryItems();
        } else {
            super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void showTurnOnLocationDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage(getResources().getString(R.string.driver_turn_on_location));
        dialog.setPositiveButton(getResources().getString(R.string.alert_button_ok), null);
        dialog.show();
    }

    private void getLocation() {
        if (checkLocationService()) {
            requestLocation(new LocationRetrievedListener() {
                @Override
                public void onLocationRetrieved(Location location) {
                    Double lat = location.getLatitude();
                    Double lon = location.getLongitude();

                    LogUtils.logEvent("Current Location", lat + "," + lon);
                    mFragment.setLocation(location);
                }

                @Override
                public void onLocationNotRetrieved() {
                    LogUtils.logEvent("Current Location", "Not available");
                }
            });
        }
    }

    private boolean checkLocationService() {
        if (!PermissionUtils.accessLocation(this)) {
            PermissionUtils.requestPermissionIfNeeded(this);
            return false;
        }

        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) &&
                !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            return false;
        }

        return true;
    }

    private void requestLocation(LocationRetrievedListener locationRetrievedListener) {
        mLocationRetrievedListener = locationRetrievedListener;

        mRunnable = new Runnable() {
            public void run() {
                if (mLocationRetrievedListener != null) {
                    mLocationRetrievedListener.onLocationNotRetrieved();
                    if (mGoogleApiClient.isConnected()) {
                        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, DriverFinishActivity.this);
                    }
                }
            }
        };

        mHandler.postDelayed(mRunnable, 10000L);

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(3600);
        locationRequest.setFastestInterval(60);

        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, this);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }

        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(Bundle bundle) {
        getLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mHandler.removeCallbacks(mRunnable);
        if (mLocationRetrievedListener != null) {
            mLocationRetrievedListener.onLocationNotRetrieved();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mHandler.removeCallbacks(mRunnable);
        if (mLocationRetrievedListener != null) {
            mLocationRetrievedListener.onLocationRetrieved(location);
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            }
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        mHandler.removeCallbacks(mRunnable);
        if (mLocationRetrievedListener != null) {
            mLocationRetrievedListener.onLocationNotRetrieved();
        }
    }
}
