package com.shopper.activities;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;

import com.afollestad.materialdialogs.MaterialDialog;
import com.happyfresh.fulfillment.R;
import com.happyfresh.fulfillment.modules.Login.LoginRouter;
import com.happyfresh.fulfillment.modules.Shopper.JobList.ShopperJobListActivity;
import com.happyfresh.fulfillment.modules.Shopper.ShoppingList.ShoppingListRouter;
import com.happyfresh.snowflakes.hoverfly.models.Batch;
import com.shopper.common.Constant;
import com.shopper.common.Constant.CanceledDialogType;
import com.shopper.common.Constant.RefreshFragmentType;
import com.shopper.common.ShopperApplication;
import com.shopper.events.RefreshFragmentEvent;
import com.shopper.fragments.UpdateFragment;
import com.shopper.listeners.DataListener;
import com.shopper.listeners.NetworkStateListener;
import com.happyfresh.snowflakes.hoverfly.models.response.StatusResponse;
import com.happyfresh.snowflakes.hoverfly.models.response.VersionUpdate;
import com.shopper.services.DownloadService;
import com.shopper.utils.ActivityUtils;
import com.shopper.utils.AppUtils;
import com.shopper.utils.LogUtils;
import com.shopper.utils.SharedPrefUtils;

import static com.shopper.common.Constant.CanceledDialogType.ACCEPT;
import static com.shopper.common.Constant.CanceledDialogType.ARRIVE;
import static com.shopper.common.Constant.CanceledDialogType.DELIVER;
import static com.shopper.common.Constant.CanceledDialogType.FAILED_DELIVERY;
import static com.shopper.common.Constant.CanceledDialogType.FINISH;
import static com.shopper.common.Constant.CanceledDialogType.PAY;
import static com.shopper.common.Constant.CanceledDialogType.REJECT;
import static com.shopper.common.Constant.CanceledDialogType.REPLACEMENT;
import static com.shopper.common.Constant.CanceledDialogType.SHOP;

/**
 * Created by ifranseda on 11/17/14.
 */
public abstract class BaseActivity extends AppCompatActivity {

    NetworkStateListener stateListener = new NetworkStateListener() {
        @Override
        public void onStateChanged(NetworkInfo.State state) {
            if (state == NetworkInfo.State.DISCONNECTED) {
                showDisconnected();
            }
            else {
                showConnected();
            }
        }
    };

    protected MaterialDialog mUpdateDialog;

    protected MaterialDialog mCanceledDialog;

    protected Toolbar mToolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        if (AppUtils.currentUserType() == AppUtils.UserType.RANGER) {
            setTheme(R.style.HappyFreshTheme_Rangers);
        }
        else {
            setTheme(R.style.HappyFreshTheme_Shoppers);
        }

        super.onCreate(savedInstanceState);
    }

    private static boolean mActive = false;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;

            case R.id.action_logout:
                logout();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void setupToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
        }
    }

    public Toolbar getToolbar() {
        return mToolbar;
    }

    private void logout() {
        MaterialDialog.Builder inputDialog = new MaterialDialog.Builder(this);
        inputDialog.title(R.string.alert_title_warning);

        inputDialog.content(R.string.alert_message_logout);
        inputDialog.negativeText(R.string.alert_button_no);
        inputDialog.positiveText(R.string.alert_button_yes);
        inputDialog.callback(new MaterialDialog.ButtonCallback() {
            @Override
            public void onPositive(MaterialDialog dialog) {
                ShopperApplication.getInstance().getUnauthorizedCallback().onUnauthorized();

                // TODO: Fix this
                LoginRouter router = new LoginRouter(BaseActivity.this);
                router.startClearTop();
            }
        });

        inputDialog.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        mActive = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        mActive = false;
    }

    protected boolean isActive() {
        return mActive;
    }

    @Override
    protected void onResume() {
        super.onResume();

        ShopperApplication.getInstance().registerNetworkStateListener(stateListener);

        checkNetworkConnectivity();
    }

    @Override
    protected void onPause() {
        ShopperApplication.getInstance().unregisterNetworkStateListener(stateListener);

        if (mUpdateDialog != null) {
            mUpdateDialog.dismiss();
        }

        super.onPause();
    }

    public boolean checkNetworkConnectivity() {
        ConnectivityManager manager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            showConnected();
            return true;
        }
        else {
            showDisconnected();
            return false;
        }
    }

    protected void checkPresenceStatus() {
        boolean hasUserToken = SharedPrefUtils.hasKey(ShopperApplication.getContext(), Constant.USER_ACCESS_TOKEN_KEY);
        // return if not login
        if (!hasUserToken) {
            return;
        }

        final ShopperApplication shopperApplication = ShopperApplication.getInstance();
        shopperApplication.getAppManager().getStatus(new DataListener() {
            @Override
            public void onCompleted(boolean success, Object data) {
                StatusResponse statusResponse = (StatusResponse) data;
                if (statusResponse != null) {
                    shopperApplication.setStatus(statusResponse.getWorkingStatus());
                    checkIfPause();
                }
            }
        });
    }

    protected void checkIfPause() {
    }

    private void showDisconnected() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.RED));
        }
    }

    private void showConnected() {
        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = getTheme();
        theme.resolveAttribute(R.attr.colorPrimary, typedValue, true);
        int colorPrimary = typedValue.data;

        if (getSupportActionBar() != null) {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorPrimary));
        }
    }

    protected void checkUpdate() {
        if (mUpdateDialog != null) {
            mUpdateDialog.dismiss();
        }

        AppUtils.checkUpdate(new DataListener<VersionUpdate>() {
            @Override
            public void onCompleted(boolean success, VersionUpdate data) {
                if (!isDestroyed()) {
                    showUpdateDialog(data);
                }
            }
        });
    }

    protected MaterialDialog.Builder requireUpdateDialog() {
        MaterialDialog.Builder inputDialog = new MaterialDialog.Builder(this);
        inputDialog.title(R.string.alert_title_update_info);

        inputDialog.content(R.string.alert_message_update_required);
        inputDialog.cancelable(false);
        inputDialog.positiveText(R.string.alert_button_ok);

        return inputDialog;
    }

    protected void showUpdateDialog(final VersionUpdate data) {
        if (!DownloadService.isDownloading) {
            addUpdateOverlay();

            MaterialDialog.Builder dialog = requireUpdateDialog();
            dialog.callback(new MaterialDialog.ButtonCallback() {
                @Override
                public void onPositive(MaterialDialog dialog) {
                    AppUtils.downloadUpdate(data);
                }
            });

            if (mUpdateDialog != null) {
                mUpdateDialog.dismiss();
            }

            mUpdateDialog = dialog.build();
            mUpdateDialog.show();
        }
    }

    public void showOrderCanceledDialog(final CanceledDialogType dialogType) {
        if (!(mCanceledDialog != null && mCanceledDialog.isShowing())) {

            MaterialDialog.Builder inputDialog = new MaterialDialog.Builder(this);

            inputDialog.title(R.string.alert_title_warning);
            inputDialog.content(R.string.order_already_canceled);
            inputDialog.positiveText(R.string.alert_button_ok);
            inputDialog.callback(new MaterialDialog.ButtonCallback() {
                @Override
                public void onAny(final MaterialDialog dialog) {

                    final Batch batch = ShopperApplication.getInstance().getBatch();
                    if (batch.shipments().size() <= 1) {
                        if (dialogType == SHOP || dialogType == PAY || dialogType == REPLACEMENT) {
                            backToShipmentList();
                        }
                        else if (dialogType == ACCEPT || dialogType == DELIVER || dialogType == ARRIVE || dialogType == REJECT || dialogType == FINISH || dialogType == FAILED_DELIVERY) {
                            ShopperApplication.getInstance().setBatch(null);
                            ActivityUtils.startClearTop(BaseActivity.this, DriverListActivity.class);
                        }
                    }
                    else {
                        ShopperApplication.getInstance().getBatchManager()
                                .updateBatch(batch.getRemoteId(), new DataListener<Batch>() {
                                    @Override
                                    public void onCompleted(boolean success, Batch data) {
                                        if (success) {
                                            switch (dialogType) {
                                                case SHOP:
                                                    openJobDetail(data);
                                                    LogUtils.logEvent("Shopping", "Refreshing shopping list");
                                                    break;
                                                case REPLACEMENT:
                                                    LogUtils.logEvent("Shopping", "Refreshing OOS list");
                                                    ShopperApplication.getInstance().getBus()
                                                            .post(new RefreshFragmentEvent(RefreshFragmentType.OOS));
                                                    break;
                                                case PAY:
                                                    LogUtils.logEvent("Shopping", "Refreshing payment list");
                                                    ShopperApplication.getInstance().getBus()
                                                            .post(new RefreshFragmentEvent(RefreshFragmentType.PAY));
                                                    break;
                                                case ACCEPT:
                                                    LogUtils.logEvent("Delivery", "Refreshing accept list");
                                                    ActivityUtils.startClearTop(BaseActivity.this,
                                                            DriverAcceptanceActivity.class);
                                                    break;
                                                default:
                                                    if (dialogType == DELIVER || dialogType == ARRIVE || dialogType == REJECT || dialogType == FINISH || dialogType == FAILED_DELIVERY) {
                                                        LogUtils.logEvent("Delivery", "Refreshing delivery list");
                                                        ActivityUtils.startClearTop(BaseActivity.this,
                                                                DriverBatchDeliveryActivity.class);
                                                    }
                                                    break;
                                            }
                                            return;
                                        }
                                    }
                                });

                    }
                }
            });

            mCanceledDialog = inputDialog.build();
            mCanceledDialog.setCanceledOnTouchOutside(false);
            mCanceledDialog.show();
        }
    }

    protected void addUpdateOverlay() {
        if (!isDestroyed()) {
            UpdateFragment updateFragment = UpdateFragment.newInstance();

            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.fragment_container, updateFragment, "Update Required");
            fragmentTransaction.commitAllowingStateLoss();
        }
    }

    public void backToShipmentList() {
        ShopperApplication.getInstance().clearQueue();

        Class activity = ShopperJobListActivity.class;

        AppUtils.UserType userType = AppUtils.currentUserType();
        if (userType == AppUtils.UserType.RANGER) {
            AppUtils.changeBackUserType();
            activity = DriverListActivity.class;
        }

        ActivityUtils.startClearTop(this, activity);
    }

    private void openJobDetail(Batch batch) {
        ShoppingListRouter router = new ShoppingListRouter(this);
        router.startWithBatch(batch);
    }

    protected void requestPermissionToDrawOverlay() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, Constant.OVERLAY_PERMISSION_REQUEST_CODE);
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constant.OVERLAY_PERMISSION_REQUEST_CODE) {
            if (!Settings.canDrawOverlays(this)) {
                LogUtils.WTF("Settings.canDrawOverlays not granted");
            }
        }
        else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}