package com.shopper.activities;

/**
 * Created by rsavianto on 2/9/15.
 */

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.utils.PermissionUtils;
import com.shopper.common.Constant;
import com.shopper.common.ShopperApplication;
import com.shopper.interfaces.LocationRetrievedListener;
import com.shopper.receivers.LocationUpdater;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by rsavianto on 11/25/14.
 */
public class LocationActivity extends BaseActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {
    protected static final String TAG = LocationActivity.class.getSimpleName();

    protected static final long TIMEOUT = 15000L;
    private static final int REQUEST_CODE = 1;
    @BindView(R.id.progress_layout)
    RelativeLayout mProgressLayout;

    @BindView(R.id.footer_button_container)
    RelativeLayout mRefreshButton;

    private GoogleApiClient mGoogleApiClient;
    private LocationRetrievedListener mLocationRetrievedListener;

    private Runnable mRunnable;
    private Handler mHandler = new Handler(Looper.getMainLooper());

    private boolean mRedirected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_location);
        ButterKnife.bind(this);

        /* Check if google play service connected */
        servicesConnected();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }

        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "Location service connected");
        checkAndRequestLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "Location service disconnected");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "Location service connection failed");
    }

    public void startRequestLocation(LocationRetrievedListener locationRetrievedListener) {
        mLocationRetrievedListener = locationRetrievedListener;

        mRunnable = new Runnable() {
            public void run() {
                if (mLocationRetrievedListener != null) {
                    mLocationRetrievedListener.onLocationNotRetrieved();
                    if (mGoogleApiClient.isConnected()) {
                        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, LocationActivity.this);
                    }
                }
            }
        };

        mHandler.postDelayed(mRunnable, TIMEOUT);

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(3600);
        locationRequest.setFastestInterval(60);

        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, this);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mHandler.removeCallbacks(mRunnable);
        if (mLocationRetrievedListener != null) {
            mLocationRetrievedListener.onLocationRetrieved(location);
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        break;
                }
        }
    }

    private boolean servicesConnected() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == resultCode) {
            Log.d(TAG, "Google Play services is available.");
            return true;
        } else {
            Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(resultCode, this, REQUEST_CODE);

            if (errorDialog != null) {
                ErrorDialogFragment errorFragment = new ErrorDialogFragment();
                errorFragment.setDialog(errorDialog);
                errorFragment.show(getFragmentManager(), "Location Updates");
            }

            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Constant.PERMISSIONS_REQUEST_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    requestLocation();
                }

                return;
            }

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                return;
        }
    }

    public boolean checkLocationService() {
        if (!PermissionUtils.accessLocation(this)) {
            PermissionUtils.requestPermissionIfNeeded(this);
            return false;
        }

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) &&
                !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            showErrorGettingLocation();
            return false;
        }

        return true;
    }

    public void requestLocation() {
        startRequestLocation(new LocationRetrievedListener() {
            @Override
            public void onLocationRetrieved(Location location) {
                if (LocationActivity.this == null) return;

                if (!mRedirected) {
                    mRedirected = true;

                    Double lat = location.getLatitude();
                    Double lon = location.getLongitude();

                    final Bundle bundle = new Bundle();
                    bundle.putDouble("LATITUDE", lat);
                    bundle.putDouble("LONGITUDE", lon);

                    LocationUpdater.startSchedule(ShopperApplication.getContext());
                }
            }

            @Override
            public void onLocationNotRetrieved() {
                if (LocationActivity.this == null) return;
                showErrorGettingLocation();
            }
        });
    }


    private void checkAndRequestLocation() {
        if (checkLocationService()) {
            requestLocation();
        }
    }

    private void showErrorGettingLocation() {
        mProgressLayout.setVisibility(View.INVISIBLE);
        mRefreshButton.setVisibility(View.VISIBLE);

        Toast.makeText(LocationActivity.this, R.string.cannot_retrieve_location, Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.refresh_button)
    void refreshGettingLocation() {
        mProgressLayout.setVisibility(View.VISIBLE);
        mRefreshButton.setVisibility(View.INVISIBLE);

        checkAndRequestLocation();
    }

    public static class ErrorDialogFragment extends DialogFragment {
        private Dialog mDialog;

        public ErrorDialogFragment() {
            super();
            mDialog = null;
        }

        public void setDialog(Dialog dialog) {
            mDialog = dialog;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return mDialog;
        }
    }
}