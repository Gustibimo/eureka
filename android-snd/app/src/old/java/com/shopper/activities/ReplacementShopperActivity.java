package com.shopper.activities;

import android.app.Fragment;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MenuItem;

import com.happyfresh.fulfillment.R;
import com.happyfresh.snowflakes.hoverfly.models.LineItem;
import com.happyfresh.snowflakes.hoverfly.models.Variant;
import com.shopper.fragments.ReplacementShopperFragment;
import com.shopper.fragments.SearchProductFragment;

import org.parceler.Parcels;

/**
 * Created by julianagalag on 5/13/15.
 */
public class ReplacementShopperActivity extends FragmentActivity {

    @Override
    protected Fragment createFragment() {
        long shipmentId = getIntent().getLongExtra(SearchProductFragment.SHIPMENT_ID, 0);
        LineItem lineItem = Parcels.unwrap(getIntent().getParcelableExtra(SearchProductFragment.LINE_ITEM));
        Variant variant = Parcels.unwrap(getIntent().getParcelableExtra(SearchProductFragment.VARIANT));

        return ReplacementShopperFragment.newInstance(shipmentId, lineItem, variant);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.HappyFreshTheme_Transparent);
        super.onCreate(savedInstanceState);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            Drawable closeButton = getResources().getDrawable(R.drawable.btn_close_dark);
            closeButton.setColorFilter(getResources().getColor(android.R.color.white), PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(closeButton);

            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getSupportActionBar().setTitle(null);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }

    @Override
    public void finish() {
        setResult(RESULT_OK);
        super.finish();
        overridePendingTransition(R.anim.stay_animation, R.anim.slide_out_to_bottom);
    }
}
