package com.shopper.activities;

import android.app.Fragment;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.happyfresh.fulfillment.R;
import com.shopper.fragments.DriverPickUpFragment;

import org.apache.commons.lang3.text.WordUtils;

/**
 * Created by rsavianto on 12/22/14.
 */
public class DriverPickUpActivity extends FragmentActivity {
    private DriverPickUpFragment mDriverPickUpFragment;

    @Override
    protected Fragment createFragment() {
        return getDriverFragment();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        invalidateOptionsMenu();
        setTitle(WordUtils.capitalize(getString(R.string.driver_pick_up).toLowerCase()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.driver, menu);

        MenuItem logoutItem = menu.findItem(R.id.action_logout);
        logoutItem.setVisible(false);

        MenuItem refresh = menu.findItem(R.id.action_refresh_job);
        refresh.setVisible(true);

        MenuItem handover = menu.findItem(R.id.action_handover);
        handover.setVisible(false);

        MenuItem cancel = menu.findItem(R.id.action_cancel_book);
        cancel.setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
        } else if (item.getItemId() == R.id.action_refresh_job) {
            getDriverFragment().updateShipment();
        } else if (item.getItemId() == R.id.action_cancel_book) {
            getDriverFragment().cancelBooking();
        } else {
            super.onOptionsItemSelected(item);
        }
        return true;
    }

    private DriverPickUpFragment getDriverFragment() {
        if (mDriverPickUpFragment == null) {
            mDriverPickUpFragment = DriverPickUpFragment.newInstance();
        }

        return mDriverPickUpFragment;
    }
}
